import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

/**
A dummy component implementation. Return a failure exit code
if a boolean parameter "fail" exists and has true value; otherwise,
return a success exit code.
*/
public class Dummy extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        for (int i=1; i<10; i++) {
            File outFile = cf.getOutput("out"+i);
            if (outFile != null) {
                Tools.writeString(outFile, "");
            }
        }
        
        double delay = Double.parseDouble(cf.getParameter("delay"));
        if (delay > 0) Thread.currentThread().sleep((long)delay*1000);
        
        final String failValue = cf.getParameter("fail");
        if (failValue != null && failValue.equals("true")) {
            return ErrorCode.ERROR;
        } else {
            return ErrorCode.OK;
        }
    }
    
    public static void main(String[] args) {
        new Dummy().run(args);
    }
}
