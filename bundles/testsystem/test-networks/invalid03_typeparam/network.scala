import anduril.builtin._
import anduril.testsystem._

object invalid03 {
    def main(args: Array[String]): Unit = {
        val in1 = INPUT(path="x1")
        val numberSum = Add(in1, in1)
        val matrixSum = AddMatrix(in1, in1)
    }
}
