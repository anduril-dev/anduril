
import anduril.builtin._
import anduril.testsystem._

/**
 * Network that illustrates the use of private functions.
 */
object network02 {

	/**
	 * Square a Number
	 * @return Number squared
	 */
    def Square(a: Number): Number = {
        val squared = Multiply(a, a)
        squared.product
    }

    /**
     * Add squared Numbers
     * @param x Number
     * @param y Number
     * @return Number x^2+y^2
     */
    def SumSquare(x: Number, y: Number): Number = {
        val xSquared = Square(x)
        val ySquared = Square(y)
        val sumSquare = Add(x1=xSquared, x2=ySquared)
        sumSquare.sum
    }

    def main(args: Array[String]): Unit = {
        val x1 = INPUT(path="x1") // Value: 3
        val x2 = INPUT(path="x2") // Value: 4
        val x3 = INPUT(path="x3") // Value: 6
        val x4 = INPUT(path="x4") // Value: 7

        val square1 = Square(x1.out) // Result: 9
        OUTPUT(square1)

        val square2 = Square(x2.out) // Result: 16
        OUTPUT(square2)

        val sumSquare1 = SumSquare(x1.out, x2.out) // Result: 25
        OUTPUT(sumSquare1)

        val sumSquare2 = SumSquare(x3.out, x4.out) // Result: 85
        OUTPUT(sumSquare2)
    }
}
