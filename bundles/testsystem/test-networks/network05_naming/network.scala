
import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime.ErrorCode
import org.anduril.runtime.IgnoreName

/* This test is a class instead of an object because it
 * has a val field. Static fields in objects don't work properly
 * in test networks because the same class is executed twice. */
class network05 {
    def GetInput(path: String) = {
        // Generate multiple instances to check unique naming; return last
        INPUT(path=path)
        INPUT(path=path)
    }

    def Square(a: Number, expectedNamePrefix: String): Option[Number] = {
        // dummy instance to check unique naming
        assert(INPUT(path="x1")._fullName.startsWith(expectedNamePrefix))

        val squared = Multiply(a, a)
        assert(squared._name == "squared")
        assert(squared._fullName == expectedNamePrefix+"-squared")
        Some(squared.product)
    }

    def SumSquare(x: Number, y: Number, expectedNamePrefix: String): Number = {
        val xSquared = Square(x, expectedNamePrefix+"-xSquared")
        val ySquared = Square(y, expectedNamePrefix+"-ySquared")
        val sumSquare = Add(x1=xSquared.get, x2=ySquared.get, _name="renamedSumSquare")
        assert(sumSquare._name == "renamedSumSquare")
        assert(sumSquare._fullName == expectedNamePrefix+"-renamedSumSquare")
        sumSquare.sum
    }

    val x4 = INPUT(path="x4")
    assert(x4._fullName == "x4", x4._fullName)

    val fieldSumSquare = SumSquare(x4.out, x4.out, "fieldSumSquare")

    def run(): Unit = {
        val x1 = INPUT(path="x1")
        assert(x1._name == "x1")
        assert(x1._fullName == "x1")

        val x2 = INPUT(
            path="x2")
        assert(x2._fullName == "x2")

        val x3 = GetInput(path="x3")
        assert(x3._fullName.startsWith("x3-"))

        val square1 = Square(x1.out, "square1")
        val square2 = Square(x2.out, "square2")
        val sumSquare1 = SumSquare(x1.out, x2.out, "sumSquare1")
        val sumSquare2 = SumSquare(x3.out, x4.out, "sumSquare2")
        OUTPUT(square1.get)
        OUTPUT(square2.get)
        OUTPUT(sumSquare1)
        OUTPUT(sumSquare2)

        val collection = Seq(INPUT(path="x1"), INPUT(path="x2", _name="c2"), INPUT(path="x3"))
        assert(collection(1)._fullName == "c2")

        val nested = Add(INPUT(path="x1"), INPUT(path="x3"))
        //assert(nested._fullName == "nested") // TODO: doesn't work; "x1" gets the "nested" name

        val withMap = INPUT(Map("path" -> "x1"))
        assert(withMap._fullName == "withMap", withMap._fullName)

        // Check that unique names are generated and that non-relevant
        // variables don't affect naming.
        val dummy1 = "dummy"
        val dummy2 = ErrorCode.OK
        assert(!INPUT(path="x1")._name.startsWith("dummy"))
        INPUT(path="x1")
        INPUT(path="x1")
        assert(!INPUT(path="x1")._name.startsWith("dummy"))
        val dummy3 = "dummy"
        val dummy4 = ErrorCode.OK
    }
}

object network05 {
    @IgnoreName
    def main(args: Array[String]): Unit = {
        new network05().run()
    }
}
