
import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object network09 {

    def main(args: Array[String]): Unit = {
        val x1 = INPUT(path="x1") // Value: 3
        val x2 = INPUT(path="x2") // Value: 4
        val x3 = INPUT(path="x3") // Value: 6
        x1._keep = false

        val sum1 = Add(x1, x2) // Result: 7
        sum1._keep = false

        val sum2 = Add(x1, x3) // Result: 9
        sum2._keep = false

        val array = makeArray(x1, sum1, sum2)

        // Dynamic breakpoint 1
        x2.content

        val extracted1 = array("1") // Result: 3
        val sum3 = Add(extracted1, sum1) // Result: 3+7 = 10
        sum3._keep = false

        OUTPUT(x1)
        OUTPUT(sum1)

        // Dynamic breakpoint 2
        x3.content

        val extracted2 = array("3") // Result: 9

        OUTPUT(sum3)
        OUTPUT(extracted2)
    }
}
