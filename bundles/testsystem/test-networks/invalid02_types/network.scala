import anduril.builtin._
import anduril.testsystem._

object invalid02 {
    def main(args: Array[String]): Unit = {
        val in1 = INPUT(path="x1")
        val in2 = INPUT(path="x2")
        val numberSum = Add(in1, in2)
        /* Results in workflow-level type error. */
        val matrixSum = AddMatrix(numberSum.sum.forceSoft(), numberSum.sum.forceSoft())
    }
}
