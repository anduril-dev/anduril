import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object network14 {

    def main(args: Array[String]): Unit = {

        val in1 = StringInput(content = "2")
        val in2 = StringInput(content = "5")

        val add = Add(x1=in1.out, x2=in2)
        val (a,b) = ( in1, add )
        val add2 = Add(a, b)

        val rec = Record[Any]("rec", ("1", add), ("2", add2), ("3", 3), ("4", AddInteger(in1,in1)), ("5", AddInteger(in1,in2)), ("6", add), ("7", add.sum))
        

        val allAdds = rec.collect[Add]
        val add3 = Add(allAdds("1").sum, allAdds("2").sum)

        val addInts = rec[AddInteger]
        val add4 = Add(addInts("4"), add3.sum)

        val ports = rec.collect[Number]
        val add5 = Add(add, ports("7"))

        val x = rec.collect[AddInteger].apply("4")
        val add6 = Add(x, x.sum)

        val chain = Chain[Any]("chain", add, AddInteger(in1,in1), 3.14, add.sum)
        val cis = chain.collect[AddInteger]
        val num = chain[Number]
        val ctt = chain[Double]
        val add7 = Add(x1 = cis(0), x2 = num(0), constant = ctt(0))

    }

}
