
import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime.table.Table
import org.anduril.runtime._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.io.Source
import sys.process._

object network03 {
    def readMatrix(path: String, instanceName: String): NumMatrix = {
        INPUT(path=path, _name=instanceName).out
    }
    
    def diff(p1: Port, p2: Port): String = {
        val sumPath = p1.content.getAbsolutePath 
        val sum2Path = p2.content.getAbsolutePath 
        (s"diff $sumPath $sum2Path" lineStream_!).mkString("\n")
    }

    def main(args: Array[String]): Unit = {
        val biasConstant = 5
        val biasInput = StringInput(content=biasConstant.toString)
        val bias = Source.fromFile(biasInput.out.content).mkString.toDouble
        assert(bias == biasConstant)

        val matrixCSV = INPUT(path="matrices.csv")
        val matrixMap = Map[String, NumMatrix]()
        for (row <- Table.reader(matrixCSV.out.content)) {
            row.rowNumber match {
                case 0 => assert(row("Filename") == "m1")
                case 1 => assert(row("Filename") == "m2")
                case _ => assert(false, "Too many rows in matrices.csv")
            }

            val name = "matrix_"+row("Filename")
            matrixMap(name) = readMatrix(path=row("Filename"), name)
        }

        val sum = AddMatrix(matrixMap("matrix_m1"), matrixMap("matrix_m2"), bias=bias)
        OUTPUT(sum.sum)        
        
        val bias2 = StringInput(content="5").out.textRead.toDouble
        val inM = iterCSV(matrixCSV.out.content).map (row => INPUT(path=row("Filename"))).toArray
        val sum2 = AddMatrix(inM(0), inM(1), bias = bias2)
        OUTPUT(sum2.sum)            
        
        val d = diff(sum, sum2)
        if (d isEmpty) println("Identical sum and sum2 !!") else println(d)
        
        val sum3 = AddMatrix(inM(0), inM(1))
        val d3 = diff(sum, sum3)
        if (d3 isEmpty) println("Identical sum and sum3 !!") else println(d3)
    }

}
