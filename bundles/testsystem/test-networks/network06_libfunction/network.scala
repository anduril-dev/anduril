
import anduril.builtin._
import anduril.testsystem._

object network06 {
    def Func(x: Number, y: Number): (Number, Number) = {
        val funcSumSquare = LibSumSquare(x, y)
        (funcSumSquare.result, funcSumSquare.ysqr)
    }

    def main(args: Array[String]): Unit = {
        val x1 = INPUT(path="x1") // Value: 3
        val x2 = INPUT(path="x2") // Value: 4
        val x3 = INPUT(path="x3") // Value: 6
        val x4 = INPUT(path="x4") // Value: 7

        val sumSquare1 = LibSumSquare(x1, x2, failEnvironmentVariable="ANDURIL_TESTSYSTEM_LIBSUMSQUARE_FAIL") // Result: (25, 16)
        OUTPUT(sumSquare1.result)
        OUTPUT(sumSquare1.ysqr)
        assert(sumSquare1.result eq sumSquare1._1)
        assert(sumSquare1.result eq sumSquare1("result"))

        val sumSquare2 = LibSumSquare(x3, x4) // Result: (85, 49)
        OUTPUT(sumSquare2.result)
        OUTPUT(sumSquare2.ysqr)

        val sumSquare3 = Func(x1, x3) // Result: (45, 36)
        OUTPUT(sumSquare3._1)
        OUTPUT(sumSquare3._2)

        val sumSquare4 = Func(x1, x4) // Result: (58, 49)
        OUTPUT(sumSquare4._1)
        OUTPUT(sumSquare4._2)
    }
}
