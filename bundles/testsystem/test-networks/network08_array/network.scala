
import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._
import scala.collection.mutable.LinkedHashMap

object network08 {
    def main(args: Array[String]): Unit = {
        val x1 = INPUT(path="x1") // Value: 1
        val x2 = INPUT(path="x2") // Value: 2
        val x3 = INPUT(path="x3") // Value: 3
        val inputArray = INPUT(path="inputArray") // {"x4"=4, "x5"=5}

        val x4 = inputArray.out("x4")
        val x5 = inputArray.out("x5")
        assert(x4.component._fullName == "x4")
        assert(x5.component._fullName == "x5")

        val sum1 = Add(x1, x4) // Result: 5
        val sum2 = Add(x2, x5) // Result: 7
        OUTPUT(sum1.sum)
        OUTPUT(sum2.sum)

        val myArray1 = makeArray(x1, x4, x2, x5)
        assert(myArray1.component._fullName.startsWith("myArray1"))
        val myArray1_1 = myArray1("1") // Result: 1
        val myArray1_2 = myArray1("2") // Result: 4
        val myArray1_3 = myArray1("3") // Result: 2
        val myArray1_4 = myArray1("4") // Result: 5
        OUTPUT(myArray1_1)
        OUTPUT(myArray1_2)
        OUTPUT(myArray1_3)
        OUTPUT(myArray1_4)

        val myArray2 = makeArray("key_x2" -> x2, x5, 9 -> x3, "key_x4" -> x4)
        assert(myArray2.component._fullName.startsWith("myArray2"))
        val myArray2_1 = myArray2("key_x2") // Result: 2
        val myArray2_2 = myArray2("2")      // Result: 5
        val myArray2_3 = myArray2("9")      // Result: 3
        val myArray2_4 = myArray2("key_x4") // Result: 4
        OUTPUT(myArray2_1)
        OUTPUT(myArray2_2)
        OUTPUT(myArray2_3)
        OUTPUT(myArray2_4)

        val implicitArraySum = Add(array = Seq(x1, x2, x3)) // Result: 6
        OUTPUT(implicitArraySum.sum)

        val rec = array2Rec("rec2", myArray2)
        rec.print

        assert( (rec("9"): Port).name == "file1" )

        for (((key, file), i) <- new ArrayIndex(myArray2.contentArray).zipWithIndex) {
            val expectedKey: String = expectedArray(i)
            assert(key == expectedKey)
        }        

        for (((key, file), i) <- iterArray(myArray2).zipWithIndex) {
            val expectedKey: String = expectedArray(i)
            assert(key == expectedKey)
        }

        checkArray(myArray2)

        val makeArrayTestSizes = Seq(1, 99, 100, 20*99 + 1)
        for (numItems <- makeArrayTestSizes) {
            val keyPortMap = LinkedHashMap[String, Component]()
            for (i <- 0 until numItems) {
                /* Inputs x1, x2 and x3 are cycled within the array. */
                val input = (i % 3) match {
                    case 0 => x1
                    case 1 => x2
                    case 2 => x3
                }
                keyPortMap("item" + i) = input
            }
            var array = makeArray(keyPortMap)

            val lastKey = numItems - 1
            val extractedLast = array("item" + lastKey, _name = "makeArray_" + numItems + "_last")
            OUTPUT(extractedLast)
            /* Expected last items, when numItems =
             * 1:       x1 (0 % 3 == 0)
             * 99:      x3 (98 % 3 == 2)
             * 100:     x1 (99 % 3 == 0)
             * 20*99+1: x1 (20*99 % 3 == 0)
             */
        }
    }

    val emptyArray = makeArray()

    def checkArray[T <: CSV]( array: T ) {
        for ( ((key,file), idx) <- new ArrayIndex(array.contentArray).zipWithIndex ) {
            val expectedKey: String = expectedArray(idx)
            assert(key == expectedKey)
        }
    }

    def expectedArray(i:Int): String = {
        i match {
            case 0 => "key_x2"
            case 1 => "2"
            case 2 => "9"
            case 3 => "key_x4"
        }
    }

}
