import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

class TestClass {
    val testX1 = INPUT(path="x1")
    network11.assertLocation(testX1, "TestClass", "<init>", 6)

    def Sub(): Unit = {
        val testX2 = INPUT(path="x2")
        network11.assertLocation(testX2, "TestClass", "Sub", 10)
    }

    Sub()
}

object network11 {

    def assertLocation(comp: Component, className: String, methodName: String, line: Int): Unit = {
        val location = comp._componentInstance.getSourceLocation()
        assert(location.getClassName() == className, location.getClassName())
        assert(location.getMethodName() == methodName, location.getMethodName())
        assert(location.getLine() == line, location.getLine())
    }

    def Square(a: Number): Number = {
        val squared = Multiply(a, a)
        assertLocation(squared, "network11$", "Square", 27)
        squared.product
    }

    def main(args: Array[String]): Unit = {
        val x1 = INPUT(path="x1")           // Value: 3
        val x2 = INPUT(Map("path" -> "x2")) // Value: 4
        assertLocation(x1, "network11$", "main", 33)
        assertLocation(x2, "network11$", "main", 34)

        val square = Square(x1.out) // Result: 9
        OUTPUT(square)

        val testClass = new TestClass()
        val sum = Add(x2, testClass.testX1) // Result: 4+3 = 7
        assertLocation(sum, "network11$", "main", 42)
        OUTPUT(sum)
    }
}
