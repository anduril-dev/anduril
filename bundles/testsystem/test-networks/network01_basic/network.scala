
import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object network01 {

    def main(args: Array[String]): Unit = {

        info("Fetching input...")

        /* Numeric inputs */
        val in1 = INPUT(path="in1")
        val in2 = INPUT(path="in2")

        /* Matrix inputs */
        val m1 = INPUT(path="m1")
        val m2 = INPUT(path="m2")

        info("Performing basic arithmetic on input...")

        val add = Add(x1=in1.out, x2=in2)

        val madd = AddMatrix(m1=m1._1, m2=m2("out"), failEnvironmentVariable="ANDURIL_TESTSYSTEM_ADDMATRIX_FAIL")
        val mstats = MatrixStats(m1=madd.sum)
        
        val mul = Multiply(x1=add.sum, x2=in2.out, x3=mstats.max)

        /* Test contravariance in input ports for generic component */
        val addInteger = AddInteger(in1.out, in2.out)
        val generic = Xor(add.sum, addInteger.sum)

        val inlineSum = Add("3", "7", _name="myInlineSum")
        if (inlineSum != null) {
            info("The inlinesum worked !")
        }

        info("Outputting results...")

        OUTPUT(add.sum)
        OUTPUT(mul.product)
        OUTPUT(madd.sum)
        OUTPUT(mstats.min)
        OUTPUT(mstats.max)
        OUTPUT(mstats.mean)
        OUTPUT(mstats.custom1)
        OUTPUT(mstats.custom2)
        OUTPUT(generic.out)
        OUTPUT(inlineSum.sum)

        assert(in1.out eq in1._1)
        assert(in1.out eq in1("out"))

        val (a,b) = ( INPUT(path="in1"), add )
        val add2 = Add(a, b)
    }

}
