import anduril.builtin._
import anduril.testsystem._

import org.anduril.runtime.table.Table
import org.anduril.runtime.Port
import org.anduril.runtime.Chain
import org.anduril.runtime.Component
import org.anduril.runtime.Record
import org.anduril.runtime.Record._

import scala.collection.mutable.ListBuffer
import scala.io.Source

object network13 {

    def main(args: Array[String]): Unit = {
    	
    	val matrixCSV = INPUT(path="in.csv")
        
        // Record that collects output from loop and renames ci's.
        val rec1 = Record[Add]("rec1") 
        for (row <- Table.reader(matrixCSV.out.content)) {
        	rec1(row("h1")) = Add(constant=1)
        }        

        // More on ci renaming based in row number.
        for (row <- Table.reader(matrixCSV.out.content)) {
        	rec1("same"+row.rowNumber) = Add(constant=1)
        }

        println("Third element in rec1 is " + rec1.nValue(2))
        rec1.print
        
        val rec2 = Record[Any]("rec2")  // "Cartesian" Record (x, y, f(x,y)).
        rec2("ctt") = 1.2
        rec2("x1") = StringInput(content="7.5").out  // rec2.x1 is a port
        rec2("ci") = Add(x1=rec2("x1").asInstanceOf[Number], constant=rec2("ctt").asInstanceOf[Double])
        rec2.print        

        val rec4 = Record[Any]("rec4", ("1","a"))
        rec4 += ("k","v")
        rec4("k2") = "value2"
        rec4 ++= rec1
        rec4("k3") = 3
        rec4.print

        // Record of records.
        val recAll = Record[Record[_]]("recursive")  
        recAll("a") = rec1
        recAll("b") = rec2
        recAll.print

		// No Chain name prepending on component instances.
        val lb = new ListBuffer[Any]()  
        lb += Add(constant=2.71)
        lb += Add(constant=3.14)

		val chain = Chain[String]("chain")
        chain ++= Chain("_","a", "b", "c") 
        chain.print

		// Chain name prepending on component instances.
        val c = Chain[Any]("chain1")  
        c += Add(constant=2.71)
        c += Add(constant=3.14)
        c += chain  // append whole given chain as single entry
        c ++= chain  // append each item in given chain
        c += 1
        c += 2
        c += 3
        c += 4
        c ++= Range(10,14)

		// No Chain name prepending on component instances, direct collection manipulation.
        c.chain += Add(constant=0)  

        c.print 

        val c2: Chain[Component] = Chain("c2")
        c2 ++= c.chain.collect { case v: Component => v }
        c2.print        

    }
}