
import anduril.builtin._
import anduril.testsystem._

object network04 {

    def main(args: Array[String]): Unit = {
        val x1 = INPUT(path="x1") // Value: 3
        val x2 = INPUT(path="x2") // Value: 4
        val x3 = INPUT(path="x3") // Value: 6

        val sum = Add(x1.out, x2.out, _name="mySum") // Result: 7

        sum._bind(x3)

        sum._enabled = true
        assert(sum._enabled)

        sum._execute = "always"
        assert(sum._execute == "always")

        sum._filename("sum", "test.csv")

        sum._keep = true
        assert(sum._keep)

        sum._priority = 10
        assert(sum._priority == 10)

        sum._require("x1", true)

        // Extra annotations

        sum._cpu = 5
        assert(sum._cpu == 5)
        assert(sum._custom("cpu") == "5")

        sum._doc = "Test"
        assert(sum._doc == "Test")

        sum._host = "local"
        assert(sum._host == "local")

        sum._memory = 100
        assert(sum._memory == 100)

        sum._custom("customkey") = "customvalue"
        assert(sum._custom("customkey") == "customvalue")

        val output = OUTPUT(sum.sum)
        output._filename("out", "mySum-sum.customext")

        // Another instance

        val sum2 = Add(x2.out, x3.out, _name="mySum2") // Result: 10

        sum2._enabled = false
        assert(!sum2._enabled)

        sum2._keep = false
        assert(!sum2._keep)

        // Should be disabled
        Compare(sum2.sum, sum2.sum)
    }
}
