
import anduril.builtin._
import anduril.testsystem._

object network10 {
    def main(args: Array[String]): Unit = {
        val in1 = INPUT(path="x1") // Value: 3
        val in2 = INPUT(path="x2") // Value: 4
        val numberSum = Add(in1.force(), in2) // Result: 7
        val matrixSum = AddMatrix(in1, numberSum.sum.force())
        OUTPUT(numberSum.sum)
    }
}
