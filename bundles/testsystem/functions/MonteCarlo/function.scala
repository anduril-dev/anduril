def MonteCarlo(n: Int): Number = { 
    import scala.util.Random
    def in() = math.pow(Random.nextDouble,2) + math.pow(Random.nextDouble,2) <= 1  
    val pi = (1 to n).map(_ => in()).count( _ == true) * 4.0 / n 
    StringInput(content = pi.toString) 
}
