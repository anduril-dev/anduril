def NumberSum(x1: Port, x2: Port, extra: CSVTest): Number = {
    val value = Add(x1, x2)
    value.sum
}
