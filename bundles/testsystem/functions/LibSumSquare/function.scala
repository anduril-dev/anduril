def square(a: Number) = {
    val squared = Multiply(a, a)
    squared.product
}

def LibSumSquare(x: Number, y: Number, skip: Boolean, failEnvironmentVariable: String): (Number, Number) = {
    if (!failEnvironmentVariable.isEmpty()) {
        if (sys.env.contains(failEnvironmentVariable)) {
            sys.error("Failed conditionally based on environment variable %s".format(failEnvironmentVariable))
        }
    }

    if (skip) {
        (x, y)
    } else {
        val xSquared = square(x)
        val ySquared = square(y)
        val sumSquare = Add(x1=xSquared, x2=ySquared)
        (sumSquare.sum, ySquared)
    }
}
