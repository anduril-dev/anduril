def GenericOutput(path: String): Port = {
    val in = INPUT(path=path)
    in.out
}
