import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class Compare extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        double x = Double.valueOf(cf.readInput("x"));
        double y = Double.valueOf(cf.readInput("y"));
        double epsilon = Double.valueOf(cf.getParameter("epsilon"));
        
        String choice = null;
        if (Math.abs(x-y) < epsilon) choice = "equal";
        else if (x<y) choice = "less";
        else choice = "greater";
        
        cf.writeLog("Choices output is "+cf.getOutput("_choices"));
        
        Tools.writeString(cf.getOutput("_choices"), choice);
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new Compare().run(args);
    }
}
