from anduril.args import *
from anduril import table
import os

CSVReaderColumnTypes=eval(CSVReaderColumnTypes)    


def csv_reader_compare(csv1,csv2,reader_type):
    csv1=table.TableReader(csv1,type=reader_type,column_types=CSVReaderColumnTypes)
    csv2=table.TableReader(csv2,type=reader_type,column_types=CSVReaderColumnTypes)

    if reader_type=="List":
        assert csv1.get_fieldnames()==csv2.get_fieldnames()
    
    csv2_iterator=iter(csv2)
    for d1 in csv1:
        d2=csv2_iterator.next()
        assert d1==d2, "Mismatch in elements '%s' and '%s' when reading tables as %s" % (d1,d2,reader_type)

if testBias:
    #Reading input
    x=float(open(x,'r').read())
    write_log('x = %.3f' % x)
    write_log('bias = %.3f' % bias)
    
    #Writing ouput
    output_port=open(output_port,'w')
    output_port.write("Bias test result: "+str(x+bias)+"\n")
    output_port.close()


#--- CSV related tests
if testCSVCompare:
    csv_reader_compare(csv1,csv2,"Dict")
    csv_reader_compare(csv1,csv2,"List")
    assert (table.TableReader(csv1,column_types=CSVReaderColumnTypes).get_numpy_array()==table.TableReader(csv2,column_types=CSVReaderColumnTypes).get_numpy_array()).all()

if testCSVSum:
    s=0
    for row in table.TableReader(csv1,type="List",column_types=CSVReaderColumnTypes):
        s+=sum(row)
    write_log("Sum test result: "+str(s))
    assert s==CSVSum

if testCSVWriter:
    #Write to output port with dict type
    csv1_table=table.TableReader(csv1,type="Dict",column_types=CSVReaderColumnTypes)
    out_csv=table.TableWriter(output_port,csv1_table.get_fieldnames())
    for row in csv1_table:
        out_csv.writerow(row)
    out_csv.close()

    #Write to temp with list type and check that result is equal to 
    #file written with dict type
    temp_csv_file=os.path.join(tempdir,"temp_csv_file.csv")
    csv1_table=table.TableReader(csv1,type="List",column_types=CSVReaderColumnTypes)
    temp_csv=table.TableWriter(temp_csv_file,csv1_table.get_fieldnames())
    for row in csv1_table:
        temp_csv.writerow(row)    
    temp_csv.close()

    #compare
    f1,f2=list(open(output_port,'r')),list(open(temp_csv_file,'r'))
    assert f1==f2, "There is a difference between files written with list and dict writers."

