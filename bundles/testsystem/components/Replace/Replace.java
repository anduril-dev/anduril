import java.io.*;
import java.util.regex.*;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

import org.anduril.core.utils.IOTools; 

public class Replace extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {

        File original = cf.getInput("original");

        String match = cf.getParameter("match");
        String replace = cf.getParameter("replace");

        System.out.println("match: " + match);
        System.out.println("replace: " + replace);

	for (File[] fileMap: Tools.mapMultifilePaths(original, cf.getOutput("replaced"))) {
	    File fileIn = fileMap[0];
	    File output = fileMap[1];
	    String strLine = "";
	    StringBuffer sb = new StringBuffer();

	    try {
		FileInputStream fstream = new FileInputStream(fileIn);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		System.out.println("out file: " + output);

		while ((strLine = br.readLine()) != null) {
		    System.out.println("original: " + strLine);

		    Pattern pattern = Pattern.compile( match );
		    final Matcher matcher = pattern.matcher( strLine );
		    strLine = matcher.replaceAll( replace );

		    System.out.println("replaced: " + strLine);
		    sb.append(strLine + "\n");
		}

		Tools.writeString(output, "" + sb.toString());
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new Replace().run(args);
    }
}
