import java.io.File;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class Xor extends SkeletonComponent {
    public static final int MAX_INPUTS  = 3;
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File input = null;
        boolean forceExclusive = cf.getParameter("forceExclusive").equals("true");
        
        for (int i=1; i<=MAX_INPUTS; i++) {
            String s = "x"+i;
            if (cf.inputDefined(s)) {
                if (input != null) {
                    if (forceExclusive) {
                        cf.writeError("Several inputs were set");
                        return ErrorCode.INVALID_INPUT;
                    } else {
                        cf.writeLog("Warning: several inputs were set; selecting the first: "+input);
                        break;
                    }
                }
                input = cf.getInput(s);
            }
        }
        
        if (input == null) {
            cf.writeError("No input is set");
            return ErrorCode.INVALID_INPUT;
        }
        
        File output = cf.getOutput("out");
        Tools.copyFile(input, output);
        
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new Xor().run(args);
    }
}
