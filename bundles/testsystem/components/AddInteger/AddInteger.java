import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class AddInteger extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        int x1 = Double.valueOf(cf.readInput("x1").trim()).intValue();
        int x2 = Double.valueOf(cf.readInput("x2").trim()).intValue();
        int x3 = 0;
        if (cf.inputDefined("x3")) {
            x3 = Double.valueOf(cf.readInput("x3").trim()).intValue();
        }
        int sum = x1+x2+x3;
        Tools.writeString(cf.getOutput("sum"), ""+sum);
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new AddInteger().run(args);
    }
}
