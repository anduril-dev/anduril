library(componentSkeleton)

execute <- function(cf) {
    m1 <- NumMatrix.read(get.input(cf, 'm1'))
    
    result.custom1 <- 0
    if (delegate.defined(cf, 'custom1')) {
        delegate.in <- get.delegate.input(cf, 'custom1', 'matrix')
        file.copy(get.input(cf, 'm1'), delegate.in)
        write.delegate.parameter(cf, 'custom1', 'testparam', 'test-value')
        status <- launch.delegate(cf, 'custom1')
        result.custom1 <- Number.read(get.delegate.output(cf, 'custom1', 'value'))
    }
    
    result.custom2 <- 0
    
    write.log(cf, sprintf('custom1 = %s', result.custom1))
    write.log(cf, sprintf('custom2 = %s', result.custom2))
    
    Number.write(get.output(cf, 'min'), min(m1, na.rm=TRUE))
    Number.write(get.output(cf, 'max'), max(m1, na.rm=TRUE))
    Number.write(get.output(cf, 'mean'), mean(m1, na.rm=TRUE))
    Number.write(get.output(cf, 'custom1'), result.custom1)
    Number.write(get.output(cf, 'custom2'), result.custom2)
}	

main(execute)
