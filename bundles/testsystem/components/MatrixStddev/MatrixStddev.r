library(componentSkeleton)

execute <- function(cf) {
    m <- NumMatrix.read(get.input(cf, 'matrix'))
    Number.write(get.output(cf, 'value'), sd(c(m), na.rm=TRUE))
}	

main(execute)
