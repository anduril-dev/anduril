library(componentSkeleton)

execute <- function(cf) {
    fail.env <- get.parameter(cf, 'failEnvironmentVariable')
    if (nchar(fail.env) > 0) {
        if (nchar(Sys.getenv(fail.env)) > 0) {
            stop(sprintf("Failed conditionally based on environment variable %s", fail.env))
        }
    }

    m1 <- NumMatrix.read(get.input(cf, 'm1'))
    m2 <- NumMatrix.read(get.input(cf, 'm2'))
    result <- m1+m2
    
    verbose <- get.parameter(cf, 'verbose', 'boolean')
    
    if (verbose) {
        cat('m1:\n')
        print(m1)
        cat('m2:\n')
        print(m2)
    }
    
    if (input.defined(cf, 'm3')) {
        m3 <- NumMatrix.read(get.input(cf, 'm3'))
        result <- result + m3
        if (verbose) {
            cat('m3:\n')
            print(m3)
        }
    }
    
    result <- result + get.parameter(cf, 'bias', 'float')

    if (verbose) {
        cat(paste('Test output lines', 1:50, '\n'), file=stdout())
        cat(paste('Test output lines', 51:100, '\n'), file=stderr())
        cat(paste('Test output lines', 101:105, '\n'), file=stdout())
    }
    
    NumMatrix.write(get.output(cf, 'sum'), result)
}

main(execute)
