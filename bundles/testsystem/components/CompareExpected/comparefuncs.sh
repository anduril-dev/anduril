
function comparefolder {
    eFiles=( $( ls "$1" | sort ) )
    tFiles=( $( ls "$2" | sort ) )
    expectedNum=${#eFiles[@]}
    testNum=${#tFiles[@]}
    # Allow expected folder to miss some files
    [[ $expectedNum -gt $testNum ]] && {
        writeerror "Expected folder contains different number of files"
        paste "$input_expected" "$input_in" | tee -a "${output_difference}"/differing_folders.txt
        exit 1
    }
    for (( i=0; i<${#eFiles[@]};i++ )); do
        efile="${eFiles[$i]}"
        tfile="$2"/"$efile"
        [[ -e "$tfile" ]] || { 
             writeerror "Expected file does not exist: $tfile"
             EC=$(( $EC + 1 ))
             exit 1
        }
        comparefile "$1/${efile}" "${tfile}" 
    done
}


function comparefile {
    echo "\"${1}\"  \"${2}\""
    local filetype=$( file "$1" | cut -d: -f2- )
    echo "$filetype" | grep -q image && {
        compareimage "$1" "$2"
        return $?
    }
    echo "$filetype" | grep -q text && {
        compareregex "$1" "$2"
        return $?
    }
    comparebinary "$1" "$2"
    return $?
}

function comparebinary {
    he=$( md5sum "${1}" | sed 's, .*,,' )
    ht=$( md5sum "${2}" | sed 's, .*,,' )
    bn=$( basename "${1}" )
    [[ "$he" = "$ht" ]] || {
        echo Binary files different | tee -a "${output_difference}"/"$bn"
        EC=$(( $EC + 1 ))
        exit 1
    }
    return 0
}

function comparediff {
    bn=$( basename "${1}" )
    diff "${1}" "${2}" | tee -a "${output_difference}"/"$bn"
    EC=${PIPESTATUS[0]}
    [[ $EC -ne 0 ]] && exit 1
    return $EC
}

function compareregex {
    local bn=$( basename "${1}" )
    local elines=$( cat "$1" | wc -l )
    local tlines=$( cat "$2" | wc -l )
    local f1
    local f2
    [[ $elines -ne $tlines ]] && {
        echo Expected $elines lines, got $tlines lines | tee -a "${output_difference}"/"$bn"
        EC=$(( $EC + 1 ))
        exit 1
    }
    local i=1
    paste -d$'\r' "$1" "$2" | while IFS=$'\r' read -r f1 f2; do
        printf '%s' "$f2" | grep -xqvEe "$f1"
        [[ $? -eq 0 ]] && {
            echo On line $i | tee -a "${output_difference}"/"$bn"
            echo "Expected: $f1" | tee -a "${output_difference}"/"$bn"
            echo "Got     : $f2" | tee -a "${output_difference}"/"$bn"
            EC=$(( $EC + 1 ))
            exit 1
        }
        i=$(( $i + 1 ))
    done
}

function compareimage {
 
    iscmd convert || exit 1
    iscmd compare || exit 1
    iscmd bc || exit 1

    tempdir=$( gettempdir )
    local bn=$( basename "${1}" )
    convert "${1}[0]" "${tempdir}/expected.miff" 2>&1 >> "$logfile" 
    convert "${2}[0]" "${tempdir}/image.miff" 2>&1 >> "$logfile" 
    #convert "${1}[0]" "${tempdir}/expected.png" 2>&1 >> "$logfile" 
    #convert "${2}[0]" "${tempdir}/image.png" 2>&1 >> "$logfile" 
    compare "${tempdir}/expected.miff" "${tempdir}/image.miff" "${output_difference}"/"${bn%.png}".png

    correlation=$( compare -verbose -metric NCC "${tempdir}/expected.miff" \
     "${tempdir}/image.miff" null: 2>&1 | grep all: | sed 's@.*: \([0-9\.]\+\).*@\1@' )

    greater=$( echo $correlation'>'0.9 | bc -l  )

    if [ "$greater" = "0" ]
    then writelog "Expecting / Got: file://${1} file://${2}"
         writelog "Image correlation $correlation lesser than limit 0.9" 
         writelog "Images not similar"
         EC=$(( $EC + 1 ))
         exit 1
    else
        writelog "Image correlation $correlation" 
    fi
    return 0
}


