source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
WD=$( getmetadata componentPath )
source "$WD"/comparefuncs.sh
#set -x
#set -e
mkdir "$output_difference" 
echo '< Expected'
echo '> Tested'
test -d "${input_expected}"
eDir=$?
test -d "${input_in}"
tDir=$?
[[ $eDir -ne $tDir ]] && {
    writeerror "One of Expected or Test inputs is a folder, and the other is not"
    exit 1
}
EC=0
# compare single files
[[ $eDir = 1 ]] && {
    echo "Comparting two files: \"${input_expected}\" \"${input_in}\""
    comparefile "${input_expected}" "${input_in}"
    exit ${EC}
}
# compare folders
echo "Comparting two folders: \"${input_expected}\" \"${input_in}\""
comparefolder "${input_expected}" "${input_in}"
exit ${EC}

