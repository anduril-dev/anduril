import java.io.File;

import org.anduril.component.CommandFile;
import org.anduril.component.DirectoryType;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class MatrixCombine extends SkeletonComponent {
    public static final int MAX_MATRICES = 4;
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        DirectoryType dir = new DirectoryType(cf.getOutput("list"));
        
        for (int i=1; i<=MAX_MATRICES; i++) {
            String in = "m"+i;
            if (cf.inputDefined(in)) {
                File input = cf.getInput(in);
                File output = dir.getNextFile();
                Tools.copyFile(input, output);
            }
        }
        
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new MatrixCombine().run(args);
    }
}
