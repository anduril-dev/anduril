library(componentSkeleton)

execute <- function(cf) {
    matrix.dir <- get.input(cf, 'list')
    filenames <- dirtype.get.files(matrix.dir)
    if (length(filenames) == 0) {
        write.error(cf, 'Matrix list is empty')
        return(1)
    }
    
    result <- NULL
    for (filename in filenames) {
        matr <- NumMatrix.read(filename)
        if (is.null(result)) result <- matr
        else {
            if (dim(result) != dim(matr)) {
                write.error('Dimensions of matrices are not consistent')
                return(1)
            }
            bigger <- matr > result
            result[bigger] <- matr[bigger]
        }
    }
    
    NumMatrix.write(get.output(cf, 'max'), result)
}

main(execute)
