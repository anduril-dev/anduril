import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class Add extends SkeletonComponent {
    
    static public final String INPUT_ARRAY = "array";

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
		    
		double sum = 0D;
		
		cf.writeLog("Input x1 is "+cf.getInput("x1"));
        if (cf.inputDefined("x1")) {
            sum = sum + Double.valueOf(cf.readInput("x1"));
        }
        
        cf.writeLog("Input x2 is "+cf.getInput("x2"));
        if (cf.inputDefined("x2")) {
            sum = sum + Double.valueOf(cf.readInput("x2"));
        }
        
        cf.writeLog("Input x3 is "+cf.getInput("x3"));
        if (cf.inputDefined("x3")) {
            sum = sum + Double.valueOf(cf.readInput("x3"));
        }
        
        if (cf.inputDefined(INPUT_ARRAY)) {
            IndexFile index = cf.readInputArrayIndex(INPUT_ARRAY);
            for (String key : index) {
                File inputFile = index.getFile(key);
                BufferedReader br = new BufferedReader(new FileReader(inputFile));
                String line = br.readLine();
                double val = Double.valueOf(line);
                sum = sum + val;
                br.close();
            }
        }
        
        double ctt = cf.getDoubleParameter("constant");
        sum = sum + ctt;        
        
        Tools.writeString(cf.getOutput("sum"), ""+sum);
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new Add().run(args);
    }
}
