source "$ANDURIL_HOME/lang/bash/functions.sh"
export_command
source ../CompareExpected/comparefuncs.sh

echo '< Expected'
echo '> Tested'
expectedKeys=( $( getarraykeys expected ) )
expectedFiles=( $( getarrayfiles expected ) )
testKeys=( $( getarraykeys in ) )
testFiles=( $( getarrayfiles in ) )
expectedNum=${#expectedKeys[@]}
testNum=${#testKeys[@]}
mkdir "${output_difference}"
[[ $expectedNum -ne $testNum ]] && {
    writeerror "Expected array contains different number of rows"
    paste "$( getinput _index_expected )" "$( getinput _index_in )" | tee -a "${output_difference}"/array_difference.txt
    exit 1
}
EC=0
for (( i=0; i<${#expectedKeys[@]};i++ )); do
    ekey="${expectedKeys[$i]}"
    efile="${expectedFiles[$i]}"
    tkey="${testKeys[$i]}"
    tfile="${testFiles[$i]}"
    [[ "$ekey" = "$tkey" ]] || { 
         writeerror "Expecting Key: $ekey, got: $tkey"
         EC=$(( $EC + 1 ))
         exit 1
    }
    [[ -d "${efile}" ]] && comparefolder "${efile}" "${tfile}" 
    [[ -f "${efile}" ]] && comparefile "${efile}" "${tfile}" 
    EC=$(( $EC + ${PIPESTATUS[0]} ))
done


exit ${EC}

