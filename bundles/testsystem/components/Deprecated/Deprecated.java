import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class Deprecated extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        double x1 = Double.valueOf(cf.readInput("x1"));
        double x2 = Double.valueOf(cf.readInput("x2"));
        double x3 = 0;
        cf.writeLog("Input x3 is "+cf.getInput("x3"));
        if (cf.inputDefined("x3")) {
            x3 = Double.valueOf(cf.readInput("x3"));
        }
        cf.writeLog("x1 is "+x1);
        cf.writeLog("x2 is "+x2);
        double sum = x1+x2+x3;
        Tools.writeString(cf.getOutput("sum"), ""+sum);
        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new Deprecated().run(args);
    }
}
