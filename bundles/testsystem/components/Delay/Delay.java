import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

public class Delay extends SkeletonComponent {
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        double delayMs = Double.valueOf(cf.readInput("delay"));
        cf.writeLog("Requested delay of " + delayMs);

        long start = System.currentTimeMillis();
        Thread.currentThread().sleep((long)delayMs);    
        long end = System.currentTimeMillis();

        Tools.writeString(cf.getOutput("start"), "" + start);
        Tools.writeString(cf.getOutput("end"),  "" + end);

        return ErrorCode.OK;
    }
    
    public static void main(String[] args) {
        new Delay().run(args);
    }
}
