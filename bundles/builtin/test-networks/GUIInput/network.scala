import anduril.builtin._
import org.anduril.runtime._
import java.io._
object GUIInputNetwork {
    
    val template = StringInput(content=
"""
fileIn=File File for something\nNext line explaining more
dirIn=Dir Use this for reading data folders
strwithDef=String This parameter has a default value
noComments=Boolean A yes or now question
comment=Boolean Are you commenting on this matter?
number=Double a numeric
""")

val default = StringInput(content=
"""strwithDef=a predefined value
number=6
comment=False
fileIn=../../default/out.txt
dirIn=../../default/
strnoDef=actually it has
noComments=True
""")

    val saveLocation = "/tmp/savedValues.txt"

    val interactive = GUIInput(template=template, 
                       defaultValues=default,
                       skipDefaults=true)
   
    var strd=interactive.out("strwithDef").textRead
    var number=interactive.out("number").textRead.toDouble
    var comment=interactive.out("number").textRead=="true"
    var dataFile=interactive.out("fileIn").content.getAbsolutePath
    val fileIn=INPUT(path=dataFile)
    
    info( "User gave these values")
    info( "strd: "+ strd )
    info( "number: "+ number.toString )
    info( "user giving comments?: "+ comment.toString )    
    info( "Data to use: "+ dataFile )    
    
    /* --------------------------
     * GUIInput can be used as iterative tool, asking parameters
     * on the first run, then remembering them for the second
     * as defaults */
     
    // Read defaults from file, if it exists
    val fileCheck2=new File(saveLocation)
    val default2:TextFile = if (fileCheck2.isFile()) {
        info("Saved values exist!")
        INPUT(path=saveLocation);
    } else {
        info("No previously saved values")
        interactive.saved
    }

    val interactive2 = GUIInput(template=template, 
                       defaultValues=default2,
                       skipDefaults=true)

    // Save defaults for next run 
    CopyOutput(in=interactive.saved, path=saveLocation)
    OUTPUT(interactive.saved)
}

