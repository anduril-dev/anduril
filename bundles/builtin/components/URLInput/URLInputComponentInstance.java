import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;


/**
 * This class enables users to use files defined by a URL (i.e., not located in
 * the present file system) as input files to networks.
 * 
 * @author Riku Louhimo
 *
 */
public class URLInputComponentInstance extends SkeletonComponent {

    private String PARAM_NAME = "url";
    private String OUTPUT_FILE = "out";
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final String value = cf.getParameter(PARAM_NAME);
        int tries=cf.getIntParameter("tries");
        
        if (value == null) {
            cf.writeError("Parameter not set: "+PARAM_NAME);
            return ErrorCode.PARAMETER_ERROR;
        }
        URLConnection connection;
   
        File outFile = cf.getOutput(OUTPUT_FILE);
        boolean downloaded=false;
        int c=0;
        do{
            try {
                connection = new URL(value).openConnection();
                // Open connection
                InputStream fis = connection.getInputStream();
                float inputLength = connection.getContentLength();
                // Open file writer
                FileOutputStream fos = new FileOutputStream(outFile);
                byte[] buf = new byte[1024*10];
                int    got;
                float  gotSum=0;
                int    gotPerc=0;
                if (inputLength!= -1) {
                    System.out.println("Download file size: "+humanReadableByteCount((long)inputLength, false));
                } else {
                    System.out.println("Download file size undetermined.");
                }
                while ((got=fis.read(buf)) >= 0) {
                    if (got > 0) fos.write(buf, 0, got);
                    gotSum+=got;
                    if ((gotPerc != (int)(10*gotSum/inputLength)) && (inputLength!= -1)) {
                        gotPerc=(int)(10*gotSum/inputLength);
                        System.out.format("%d%% %s%n",10*gotPerc,humanReadableByteCount((long)gotSum, false));
                    }
                }
                fis.close();
                fos.close(); 
                downloaded=true;
            } catch (MalformedURLException me){
                cf.writeError("Parameter URL malformed: "+value);
                cf.writeError(me);
                return ErrorCode.PARAMETER_ERROR;
            } catch (IOException ie){
//              cf.writeError("Parameter URL unavailable: "+PARAM_NAME);
//              return ErrorCode.INPUT_IO_ERROR;
                ie.printStackTrace();
                cf.writeLog("WARNING: failed downloading "+value+"! Retrying...");
            }
            c++;
        }while((!downloaded)&&(c<tries));
        if(!downloaded){
            cf.writeError("Connection failed!");
            return ErrorCode.INPUT_IO_ERROR;
        }
        return ErrorCode.OK;
     }

    public static void main(String[] args) {
        new URLInputComponentInstance().run(args);
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

}


