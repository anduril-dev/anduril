#!/usr/bin/env python3
import anduril, sys, os
from anduril.args import *
from anduril import *

cf = anduril.CommandFile.from_file(sys.argv[1])
writer = TableWriter(output_out, fieldnames=["Key", "File"])
for key, filename in input_in.items():
    if input_relative:
        filename = os.path.relpath(filename, cf.get_input("relative"))
    writer.writerow([key, filename])
