#!/usr/bin/env python3
import sys
import anduril
from anduril.args import *
import csv
import re
import os.path

keystring = re.compile(r"(.*)_(\d*)")


def uniquename(testList, newitem):
    """ Returns a unique new list item, in the fashion of  "string"_1 ... """
    if not newitem in testList:
        return newitem
    i = 0
    matches = keystring.match(newitem)
    if matches == None:
        i = 1
        newitem = newitem + "_" + str(i)

    while newitem in testList:
        matches = keystring.match(newitem)
        i += 1
        newitem = matches.group(1) + "_" + str(i)
    return newitem


method = keys.lower().strip()

if method not in ["numbers", "column", "file", "hash"]:
    write_error("Key generation method " + method + " not recognized.")
    sys.exit(1)

reader = anduril.TableReader(input_in, column_types=str)
fields = reader.get_fieldnames()
if fileColumn not in fields:
    write_error('Column "' + fileColumn + '" not found.')
    sys.exit(1)
if failOnMissing:
    for row in reader:
        if not os.path.exists(row.get(fileColumn)):
            write_error('File "' + row.get(fileColumn) + '" not found.')
            sys.exit(1)
    reader = anduril.TableReader(input_in, column_types=str)

if method == "numbers":
    i = 1
    for row in reader:
        output_out.write(str(i), row.get(fileColumn))
        i += 1
    sys.exit(0)

if method == "column":
    if keyColumn not in fields:
        write_error('Column "' + keyColumn + '" not found.')
        sys.exit(1)
    outFiles = []
    outUnique = []
    for row in reader:
        outUnique.append(uniquename(outUnique, row.get(keyColumn)))
        outFiles.append(row.get(fileColumn))

if method == "file":
    outFiles = []
    outUnique = []
    for row in reader:
        basename = os.path.basename(row.get(fileColumn))
        outUnique.append(uniquename(outUnique, basename))
        outFiles.append(row.get(fileColumn))

if method == "hash":
    import hashlib

    outFiles = []
    outUnique = []
    for row in reader:
        basename = os.path.basename(row.get(fileColumn))
        md5 = hashlib.md5()
        with open(row.get(fileColumn), "rb") as f:
            for chunk in iter(lambda: f.read(8192), ""):
                md5.update(chunk)
        outUnique.append(uniquename(outUnique, md5.hexdigest()))
        outFiles.append(row.get(filecolumn))

for k, f in zip(outUnique, outFiles):
    output_out.write(k, f)
