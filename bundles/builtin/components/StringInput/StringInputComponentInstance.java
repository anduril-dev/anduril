import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

/**
 * This class enables users to use strings in AndurilScript as input files to networks.
 * 
 * @author Lauri Lyly
 *
 */
public class StringInputComponentInstance extends SkeletonComponent {

    private String PARAM_NAME = "content";
    private String OUTPUT_FILE = "out";
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        String value = cf.getParameter(PARAM_NAME);
        
        if (value == null) {
            cf.writeError("Parameter not set: "+PARAM_NAME);
            return ErrorCode.PARAMETER_ERROR;
        }
   
        File outFile = cf.getOutput(OUTPUT_FILE);
        try {
            FileWriter writer = new FileWriter(outFile);
            writer.write(value);
            writer.close();
        } catch (IOException ie) {
            cf.writeError("Problem writing to file: " + outFile.getAbsolutePath());
            return ErrorCode.INPUT_IO_ERROR;
        }
        return ErrorCode.OK;
     }

    public static void main(String[] args) {
        new StringInputComponentInstance().run(args);
    }
}
