import java.io.File;
import java.util.HashSet;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.core.network.Port;

public class ArrayConstructor extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        File outDir = cf.getOutput("out");
        outDir.mkdirs();

        IndexFile outputIndex = new IndexFile();
        HashSet<String> seenKeys = new HashSet<String>(Port.ARRAY_CONSTRUCTOR_PORTS);

        for (int i=1; i<=Port.ARRAY_CONSTRUCTOR_PORTS; i++) {
            String inPort = "file"+i;
            if (!cf.inputDefined(inPort)) continue;

            String key = cf.getParameter("key"+i);
            if (seenKeys.contains(key)) {
                cf.writeError("Duplicate key: "+key);
                return ErrorCode.PARAMETER_ERROR;
            }
            seenKeys.add(key);

            outputIndex.add(key, cf.getInput(inPort).getAbsoluteFile());
        }

        outputIndex.write(cf, "out");

        return ErrorCode.OK;
    }

    public static void main(String[] args) {
        new ArrayConstructor().run(args);
    }
}
