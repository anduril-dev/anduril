from anduril.args import *
import os as operatingsystem

exec(preprocess)  # there is a possiblity of name collisions!

new_array_list = []
new_index = 0
for index, (key, path) in enumerate(input_in.items()):
    if not eval(remove):
        filename = operatingsystem.path.split(path)[-1]
        new_key = str(eval(rename))
        order_index = eval(order)
        new_array_list.append((order_index, new_key, path))
        new_index += 1

new_array_list.sort(key=lambda x: x[0])
for order_index, new_key, path in new_array_list:
    output_out.write(new_key, path)
