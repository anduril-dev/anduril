#!/usr/bin/env python3
from anduril.args import *
import shutil, sys, os, re, anduril


def copytree(src, dst):
    """Recursively copy a directory tree using shutil.copy2().

    Modified from shutil.copytree to overcome copystat -problems
    on NTFS / CIFS / other non-octect permission media
     ( uses Anduril logger )

    """
    names = os.listdir(src)
    if not os.path.isdir(dst):
        os.makedirs(dst)
    errors = []
    permission_errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if os.path.isdir(srcname):
                copytree(srcname, dstname)
            else:
                # Will raise a SpecialFileError for unsupported file types
                shutil.copyfile(srcname, dstname)
                try:
                    shutil.copystat(srcname, dstname)
                except:
                    # stats may not copy.
                    permission_errors.append(dstname)
                    pass
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except Error, err:
            errors.extend(err.args[0])
        except EnvironmentError, why:
            errors.append((srcname, dstname, str(why)))
    try:
        shutil.copystat(src, dst)
    except Exception:
        # If permissions cannot be set, skip this
        permission_errors.append(dst)
        pass

    if len(permission_errors) > 0:
        write_log("Could not change permissions: " + ", ".join(permission_errors))

    if errors:
        raise Error(errors)


if param_path.startswith(os.sep):
    # target path is absolute
    targetFile = param_path
else:
    if param_relativeToScript:
        # target path is relative to script
        scriptFile = re.sub(":[0-9]+$", "", meta_sourceLocation)
        relativeTo = os.path.abspath(os.path.dirname(scriptFile))
    else:
        # target path is relative to execution folder
        relativeTo = os.path.abspath(os.path.dirname(os.path.dirname(tempdir)))
    targetFile = os.path.join(relativeTo, param_path)
write_log(
    "Reading from: "
    + os.sep.join(input_in.split(os.sep)[-2:])
    + " Writing to: "
    + targetFile
)

# Delete target if requested, or source is a file. links are always regenerated
if param_delete and os.path.isdir(targetFile):
    shutil.rmtree(targetFile)
if os.path.isfile(targetFile):
    os.remove(targetFile)
if os.path.islink(targetFile):
    os.remove(targetFile)

if param_link:
    # Create link and exit
    os.symlink(input_in, targetFile)
    sys.exit(0)

if os.path.isdir(input_in):
    # Copy folder
    copytree(input_in, targetFile)
else:
    # Copy file
    if not os.path.isdir(os.path.dirname(targetFile)):
        os.makedirs(os.path.dirname(targetFile))
    shutil.copyfile(input_in, targetFile)
    try:
        shutil.copystat(input_in, targetFile)
    except Exception:
        # stats may not copy.
        write_log("Could not change permissions: " + targetFile)
        pass
