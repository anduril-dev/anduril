import java.io.*;
import java.util.*;
import java.util.regex.Matcher;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;
import org.anduril.asser.AsserUtil;

/**
 * This Anduril component is a wrapper for command line executables.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 2.38
 */
public class StandardProcess extends SkeletonComponent {

    /** Waiting time (in milliseconds) that allows threads to complete their tasks. */
    static final private int JOIN_WAIT = 60*1000;

    protected ErrorCode runImpl(CommandFile cf) throws IOException,
                                                       InterruptedException {
        File     fileIn   = cf.getInput("input");
        File     fileEnv  = cf.getInput("env");
        File     fileOut  = cf.getOutput("stdout");
        File     fileErr  = cf.getOutput("stderr");
        String   cmdParam = cf.getParameter("command");
        String   pDelim   = cf.getParameter("delim");
        String   wdParam  = cf.getParameter("wd");
        String   myName   = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        String[] env      = null;

        Map<String,String> optPorts = new HashMap<String,String>(20);

        for (int a=1; a<10; a++) {
        	String argN = "arg"+a;
        	String argK = '@'+argN+'@';
        	File   argF = cf.getInput(argN);
        	int    pos  = cmdParam.indexOf(argK);

        	if (argF == null) {
        		if (pos >= 0) {
        			cf.writeError("Input "+argN+" is missing but the command refers to it.");
        			return ErrorCode.INVALID_INPUT;
        		}
        	} else {
                if (pos >= 0) {
                	optPorts.put(argK, Matcher.quoteReplacement(argF.getPath()));
        	    } else {
        		    cf.writeError("Input "+argN+" is not used within the given command: "+cmdParam);
        		    return ErrorCode.PARAMETER_ERROR;
        	    }
            }
        }

        for (int o=1; o<4; o++) {
        	String outN = "optOut"+o;
        	String outK = '@'+outN+'@';
        	File   outF = cf.getOutput(outN);
        	int    pos  = cmdParam.indexOf(outK);

        	if (pos < 0) {
        		Tools.writeString(outF, "DO NOT USE THIS OUTPUT!\n"+
        				                "This component ("+myName+") does not produce output for "+outN+'.');
        	} else {
            	optPorts.put(outK, Matcher.quoteReplacement(outF.getPath()));
            }
        }

        if (fileEnv != null) {
            Properties envP = new Properties();
            envP.load(new FileReader(fileEnv));

            env = new String[envP.size()];
            int pos = 0;
            Enumeration<Object> keys = envP.keys();
            while (keys.hasMoreElements()) {
                String key   = keys.nextElement().toString();
                String value = envP.getProperty(key, AsserUtil.EMPTY_STRING);
                env[pos++]   = key+'='+value;
            }
        }

        File wd = AsserUtil.onlyWhitespace(wdParam) ? null : new File(wdParam);

        String[] cmd = prepareCommand(cmdParam, pDelim, optPorts); 
        cf.writeLog("Executing: "+AsserUtil.collapse(cmd, " "));

        Thread inputTh = null;
        final Process exec = Runtime.getRuntime()
                                    .exec(cmd, env, wd);
        Thread stdoutTh = bindStreams(exec.getInputStream(),
                                      new FileOutputStream(fileOut),
                                      fileOut+" writer",
                                      false);
        Thread stderrTh = bindStreams(exec.getErrorStream(),
                                      new FileOutputStream(fileErr),
                                      fileErr+" writer",
                                      false);
        OutputStream inStream = exec.getOutputStream();
        InputStream  sIn      = null;
        if (fileIn == null) {
            inStream.close();
        } else {
            sIn     = new FileInputStream(fileIn);
            inputTh = bindStreams(sIn, inStream, fileIn+" feed", true);
        }
        int status = exec.waitFor();
        if (status != 0)
            throw new RuntimeException("Execution failed and returned "+status+'.');
        if (inputTh != null) inputTh.join(JOIN_WAIT);
        stdoutTh.join(JOIN_WAIT);
        stderrTh.join(JOIN_WAIT);
        if (sIn != null) sIn.close();

        return ErrorCode.OK;
    }

    static private String[] prepareCommand(String             template,
                                           String             delim,
                                           Map<String,String> ports) {
    	List<String>    cmd    = new LinkedList<String>();
    	StringTokenizer tokens = delim.isEmpty() ? new StringTokenizer(template) :
    	                                           new StringTokenizer(template, delim);

    	while (tokens.hasMoreTokens()) {
    		String token = tokens.nextToken();
    		int    at    = token.indexOf('@');
    		while ((at >= 0) && (at < token.length()-3)) {
    			int end = token.indexOf('@', at+1);
    			if (end < 2) break;
    			String port = token.substring(at, end+1);
    			String file = ports.get(port);
    			if (file != null)
    			   token = token.replaceAll(port, file);
    			at = token.indexOf('@', end+1);
    		}
    		cmd.add(token);
    	}
    	return cmd.toArray(new String[cmd.size()]);
    }

    /**
     * Copies data from the given source to the given sink.
     * This task is carried out by the thread returned. The thread is started
     * automatically.
     *
     * The virtual machine is turned off if this execution fails!
     *
     * @param acceptTermination Do not worry if the sink does not accept the whole source data.
     */
    static private Thread bindStreams(final InputStream  in,
                                      final OutputStream out,
                                      final String       name,
                                      final boolean      acceptTermination) {
        Thread outputThread = new Thread() {
            public void run() {
                try {
                  long   use = 0;
                  byte[] buf = new byte[4096];
                  int    got;

                  while ((got=in.read(buf)) >= 0) {
                      if (got > 0) {
                         try { out.write(buf, 0, got); }
                         catch (IOException e) {
                             System.err.println("Warning: target stream was closed before the end of the content. Only "+
                                                use+" bytes were written by "+name+'.');
                             if (acceptTermination)
                                return;
                             throw e;
                         }
                         use += got;
                      } else {
                         System.err.println("Zero bytes obtained.");
                      }
                  }
                  try { out.flush(); }
                  catch (IOException e) {
                      System.err.println("Warning: target stream was closed before the end of the content. Only "+
                                         use+" bytes were written by "+name+'.');
                      if (!acceptTermination)
                         throw e;
                  }
                  out.close();
                } catch (Exception e) {
                    System.err.println("I/O error in "+name+'.');
                    e.printStackTrace();
                    System.exit(ErrorCode.ERROR.getCode());
                }
            }
        };
        outputThread.setName(name);
        outputThread.setDaemon(true);
        outputThread.start();
        return outputThread;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new StandardProcess().run(argv);
    }

}
