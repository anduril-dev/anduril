# /usr/bin/env python3
import sys
import anduril
from anduril.args import *
import math


def portorder(inArray, outArrays, N, subset):
    """ Write array elements in port order """
    outidx = 0
    for row in inArray.items():
        if subset == 0:
            outArrays[outidx].write(row[0], row[1])
        else:
            if subset == outidx + 1:
                outArrays[0].write(row[0], row[1])
        outidx = outidx + 1
        if outidx + 1 > N:
            outidx = 0


def arrayorder(inArray, outArrays, N, subset):
    """ Write array elements in input array order """

    bins = [int(math.floor(float(len(inArray)) / float(N)))] * int(N)
    binidx = 0
    while sum(bins) < len(inArray):
        bins[binidx] += 1
        binidx += 1
    offsets = list(offset(bins))
    offsets.insert(0, 0)
    inItems = list(inArray.items())
    if subset == 0:
        for outidx in range(N):
            for f in range(offsets[outidx], offsets[outidx] + bins[outidx]):
                outArrays[outidx].write(inItems[f][0], inItems[f][1])
    else:
        outidx = subset - 1
        for f in range(offsets[outidx], offsets[outidx] + bins[outidx]):
            outArrays[0].write(inItems[f][0], inItems[f][1])


def offset(it):
    total = 0
    for x in it:
        total += x
        yield total


""" Splits an array input in N outputs """
outArrays = []
for x in range(9):
    outArrays.append(eval("output_out" + str(x + 1)))

method = order.lower().strip()
if method not in ("port", "array"):
    write_error('Writing order: "' + method + '" not recognized.')
    sys.exit(1)
## limit of 9 outputs, happens only when subset is not used
if subset == 0:
    if N > 9:
        N = 9

if method == "port":
    portorder(input_in, outArrays, N, subset)
if method == "array":
    arrayorder(input_in, outArrays, N, subset)
