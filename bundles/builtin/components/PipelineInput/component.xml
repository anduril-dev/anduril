<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>PipelineInput</name>
    <version>1.1</version>
    <doc>Read a previous Anduril pipeline execution folder as an input.

    This component provides convenience when pipelines are split up to
    separate tasks. With it you can access a component instance outside
    the current pipeline namespace.

    With conventional INPUT:
    <pre>
    val data_from_earlier=INPUT(path="execution_folder"+"instance_name"+"file_name")
    </pre>
    With PipelineInput:
    <pre>
    val instance_from_earlier=PipelineInput(exec="execution_folder",instance="instance_name")
    val data_from_first_port=instance_from_earlier.n(1)
    val data_from_named_port=instance_from_earlier.name("csv")
    </pre>

    </doc>
    <author email="ville.rantanen@helsinki.fi">Ville Rantanen</author>
    <category>Internal</category>
    <launcher type="python3">
       <argument name="file" value="PipelineInput.py" />
    </launcher>
    <type-parameters>
        <type-parameter name="T" />
    </type-parameters>
    <outputs>
        <output name="name" type="T" array="true">
            <doc>A generic output array of ports. The keys are the output port names of the selected instance.</doc>
        </output>
        <output name="n" type="T" array="true">
            <doc>A generic output array of ports. The keys are the number of the port (natural sorting).
                 For the single output component, you can always refer: "instance.n(1)"</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="exec" type="string">
            <doc>Path to execution folder.</doc>
        </parameter>
        <parameter name="instance" type="string">
            <doc>Name of the component instance. Does not work with function instance names, has to be an existing instance folder name.
            Use value "output" to create an array of files/folders in the execution output folder.
            You may also use "@[regex]" to include all instances that match the regex. If grouping used in the regular expression,
            the first group is used for naming instances - NOTE: group must produce unique names. Use "@.*" to export
            all instances.
            </doc>
        </parameter>
        <parameter name="expandArrays" type="boolean" default="false">
            <doc>Expand array output ports.</doc>
        </parameter>
    </parameters>
</component>
