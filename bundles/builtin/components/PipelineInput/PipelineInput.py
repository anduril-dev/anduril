import anduril
from anduril.args import *
import os, sys, string, re


def natural_sorted(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split("([0-9]+)", key)]
    return sorted(l, key=alphanum_key)


def check_for_paths(p, i, fname):
    # search for the instance path either relative to the running path
    path1 = os.path.abspath(os.path.join(start_path, p, i, fname))
    # or absolute path
    path2 = os.path.abspath(os.path.join(p, i, fname))
    # or above current execution
    path3 = os.path.abspath(
        os.path.join(os.path.dirname(os.path.dirname(my_path)), p, i, fname)
    )

    # Relative path is preferred
    new_path = None
    if os.path.exists(path3):
        new_path = path3
    if os.path.exists(path2):
        new_path = path2
    if os.path.exists(path1):
        new_path = path1

    if new_path == None:
        write_error("Path not found: " + " nor ".join([path1, path2, path3]))
        sys.exit(1)
    else:
        write_log("Path found: " + new_path)
    return new_path


def output_array(out_path, prefix="", expand_arrays=False):
    # Write output files as array
    new_list = os.listdir(out_path)
    if len(new_list) == 0:
        write_error("Folder empty: " + out_path)
    i = 1
    if prefix != "":
        prefix = prefix + "."
    for op in natural_sorted(new_list):
        nop = op.replace("-", "_")
        output_file = os.path.join(out_path, op)
        if expand_arrays:
            index_file = os.path.join(output_file, "_index")
            if os.path.exists(index_file):
                new_array = anduril.arrayio.ArrayReader(folder=output_file)
                for new_item in new_array:
                    new_file = anduril.arrayio.get_abs_file(
                        output_file, new_item["File"]
                    )
                    name.write(prefix + op + "." + new_item["Key"], new_file)
                    n.write(prefix + str(i) + "." + new_item["Key"], new_file)
                i += 1
                continue
        name.write(prefix + nop, output_file)
        n.write(prefix + str(i), output_file)
        write_log(
            "Added output: "
            + meta_name
            + '.name("'
            + prefix
            + nop
            + '")'
            + " / "
            + meta_name
            + '.n("'
            + prefix
            + str(i)
            + '")'
        )
        i += 1


def component_array(new_path, prefix="", expand_arrays=False):
    # Write component ports as array

    new_cf = anduril.CommandFile.from_file(new_path)
    cf_base = os.path.dirname(new_path)
    i = 1
    if prefix != "":
        prefix = (prefix + ".").replace("-", "_")
    for op in natural_sorted(new_cf.get_output_ports()):
        if not op.startswith("_"):
            output_file = os.path.abspath(new_cf.get_output(op))
            if not os.path.exists(output_file):
                write_log(
                    "Port missing by absolute path: '"
                    + output_file
                    + "' using _command directory."
                )
                out_base = os.path.basename(output_file)
                output_file = os.path.join(cf_base, out_base)
            if not os.path.exists(output_file):
                write_log("Port missing (@keep=false?): '" + prefix + op + "'")
                continue
            if expand_arrays:
                index_file = os.path.join(output_file, "_index")
                if os.path.exists(index_file):
                    new_array = anduril.arrayio.ArrayReader(folder=output_file)
                    for new_item in new_array:
                        new_file = anduril.arrayio.get_abs_file(
                            output_file, new_item["File"]
                        )
                        name.write(prefix + op + "." + new_item["Key"], new_file)
                        n.write(prefix + str(i) + "." + new_item["Key"], new_file)
                    i += 1
                    continue
            name.write(prefix + op, output_file)
            n.write(prefix + str(i), output_file)
            write_log(
                "Added output: "
                + meta_name
                + '.name("'
                + prefix
                + op
                + '")'
                + " / "
                + meta_name
                + '.n("'
                + prefix
                + str(i)
                + '")'
            )
            i += 1


cf = anduril.CommandFile.from_file(sys.argv[1])
meta_name = cf.get_metadata("instanceName")
start_path = os.environ["PWD"]
my_path = os.path.dirname(name.get_folder())
# Collect OUTPUTs
if param_instance == "output":
    new_path = check_for_paths(param_exec, param_instance, "")
    output_array(new_path, prefix="", expand_arrays=param_expandArrays)

# Collect all instances in one array (that match a regex)
if param_instance.startswith("@"):
    # Check the _state file exists
    exec_path = os.path.dirname(check_for_paths(param_exec, "", "_state"))
    reg = re.compile(param_instance[1:])
    for d in natural_sorted(os.listdir(exec_path)):
        # match for a rexeg
        if not reg.match(d):
            continue
        # Add output folder
        if d == "output":
            if len(os.listdir(os.path.join(exec_path, d))) > 0:
                output_array(
                    os.path.join(exec_path, d),
                    prefix="output",
                    expand_arrays=param_expandArrays,
                )
        # Add all folders with _command
        try:
            prefix = reg.match(d).group(1)
        except:
            prefix = reg.match(d).group(0)
        if os.path.isfile(os.path.join(exec_path, d, "_command")):
            component_array(
                os.path.join(exec_path, d, "_command"),
                prefix=prefix,
                expand_arrays=param_expandArrays,
            )

# Collect ports of one instance
if param_instance != "output" and not param_instance.startswith("@"):
    new_path = check_for_paths(param_exec, param_instance, "_command")
    component_array(new_path, prefix="", expand_arrays=param_expandArrays)

# Close and flush the array
name.close()
arrayreader = anduril.ArrayReader(folder=name.get_folder())
# Fail the component if no elements written in array
if len(list(arrayreader)) == 0:
    write_error("Could not find any outputs!")
