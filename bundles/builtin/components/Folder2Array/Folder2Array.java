import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.asser.AsserUtil;

public class Folder2Array extends SkeletonComponent {
    public static final int NUM_PORTS = 9;
    
    private IndexFile outputIndex;
    private Pattern filePattern;
    private Pattern folderPattern;
    private Pattern excludePattern;
    private Set<String> seenKeys;
    private String  keyMode;
    private boolean timeStampPrefix;
    private boolean includeFolders;
    
    private CommandFile cf;
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        this.cf=cf;
        cf.getOutput("out").mkdirs();
        this.outputIndex = new IndexFile();
        this.filePattern = Pattern.compile(cf.getParameter("filePattern"));
        this.folderPattern = Pattern.compile(cf.getParameter("folderPattern"));
        this.excludePattern = Pattern.compile(cf.getParameter("excludePattern"));
        this.keyMode = cf.getParameter("keyMode").toLowerCase();
        this.timeStampPrefix = cf.getBooleanParameter("timeStampPrefix");
        this.includeFolders = cf.getBooleanParameter("includeFolders");
        
        this.seenKeys = new HashSet<String>();
        
        for (int i=1; i<=NUM_PORTS; i++) {
            String portName = "folder"+i;
            if (!cf.inputDefined(portName)) continue;
            File input = cf.getInput(portName);
            if (input.isDirectory()) scanDirectory(input,portName);
            else processFile(input,portName);
        }
        
        this.outputIndex.write(cf, "out");
        
        return ErrorCode.OK;
    }
    
    private void scanDirectory(File directory,String    portName) {
        if ((!this.folderPattern.matcher(directory.getName()).matches())||
                (this.excludePattern.matcher(directory.getName()).matches())) {
            return;
        }
        
        File[] files = directory.listFiles();
        Arrays.sort(files);
        
        for (File f: files) {
            if (f.isFile()) processFile(f,portName);
            if (includeFolders) if (f.isDirectory()) processFile(f,portName);
        }
        if (includeFolders) return;
        for (File f: files) {
            if (f.isDirectory()) scanDirectory(f,portName);
        }
    }

    private void processFile(File file,String portName) {
        Matcher matcher = this.filePattern.matcher(file.getName());
        Matcher excludeMatcher = this.excludePattern.matcher(file.getName());
        if (matcher.matches()) {
//          String  key="key"+Math.abs(file.hashCode());
            String  key;
//          String  key=file.getAbsolutePath().replaceAll("[^a-zA-Z0-9_]", "_");
//          String  key=file.getAbsolutePath().replace(File.separator,"_");
            if(!excludeMatcher.matches()){
                
                if (keyMode.equals("pattern")) {
                    key = matcher.groupCount() > 0 ? matcher.group(1) : matcher.group();
                } else if (keyMode.equals("filename")) {
                    key=getRelativePath(cf.getInput(portName),file).replaceAll("[^a-zA-Z0-9_]", "_");
                } else if (keyMode.equals("hashcode")) {
                    key="key"+Math.abs(file.hashCode());
                } else if (keyMode.equals("number")) {
                    key=Integer.toString(this.seenKeys.size()+1);
                } else {    
                    key="";
                    cf.writeError("Key generation mode not recognized");
                    cf.writeError("keyMode:\t"+keyMode);
                    System.exit(1);
                }        

                if(timeStampPrefix)
                    key=Long.toString(file.lastModified())+"_"+key;
//              System.out.println("Debug: key="+key);
                if (!this.seenKeys.contains(key)) {
                    this.outputIndex.add(key, file.getAbsoluteFile());
                    this.seenKeys.add(key);
                }
            }
            
        }
    }
    
    private String  getRelativePath(File    baseDir,File    file)
    {
        int count,fCount;
        File[]  basePath=new    File[count=AsserUtil.split(baseDir.getAbsolutePath(), File.separator).length];
        File[]  filePath=new    File[fCount=AsserUtil.split(file.getAbsolutePath(), File.separator).length];
        
        File    curFile=baseDir;
        for(int i=count-1;(i>=0)&&(curFile!=null);i--){
            basePath[i]=curFile;
            curFile=curFile.getParentFile();
        }
        curFile=file;
        for(int i=fCount-1;(i>=0)&&(curFile!=null);i--){
            filePath[i]=curFile;
            curFile=curFile.getParentFile();
        }
        if(fCount<=count)
        {
            cf.writeError("Internal error in getRelativePath: file is not nested in the baseDir");
            cf.writeError("file:\t"+file.getAbsolutePath());
            cf.writeError("baseDir:\t"+baseDir.getAbsolutePath());
            System.exit(1);
        }
        for(int i=0;i<count;i++)
        {
            if(!basePath[i].equals(filePath[i])){
                cf.writeError("Internal error in getRelativePath: file is not nested in the baseDir");
                cf.writeError("file:\t"+file.getAbsolutePath());
                cf.writeError("baseDir:\t"+baseDir.getAbsolutePath());
                System.exit(1);
            }
        }
        String  relativePath="";
        for(int i=count;i<fCount;i++)
            relativePath+=(relativePath.length()>0?File.separator:"")+filePath[i].getName();
        return  relativePath;
    }
    
    public static void main(String[] args) {
        new Folder2Array().run(args);
    }

}


