import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.IndexFile;
import org.anduril.component.SkeletonComponent;
import org.anduril.core.network.Port;

public class ArrayCombiner extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        final HashSet<String> selectedKeys = new HashSet<String>();
        final ArrayList<String> orderedKeys = new ArrayList<String>();
        final HashMap<String,String> fileMap = new HashMap<String, String>();

        final String combineType = cf.getParameter("combineType");
        final Pattern subsetPattern = Pattern.compile(cf.getParameter("subset"));

        boolean firstInput = true;
        for (int i=1; i<=Port.ARRAY_COMBINER_PORTS; i++) {
            String inputPort = "array"+i;
            if (!cf.inputDefined(inputPort)) continue;

            final IndexFile inputIndex = cf.readInputArrayIndex(inputPort);

            HashSet<String> currentKeys = new HashSet<String>();
            for (String key: inputIndex) {
                if (!subsetPattern.matcher(key).matches()) continue;

                if (!fileMap.containsKey(key)) {
                    File elementFile = inputIndex.getFile(key);
                    fileMap.put(key, elementFile.getAbsolutePath());
                    orderedKeys.add(key);
                }
                currentKeys.add(key);
            }

            if (combineType.equals("union")) {
                selectedKeys.addAll(currentKeys);
            } else if (combineType.equals("intersection")) {
                if (firstInput) {
                    selectedKeys.addAll(currentKeys);
                } else {
                    selectedKeys.retainAll(currentKeys);
                }
            } else {
                cf.writeError("Invalid 'type' parameter: "+combineType);
                return ErrorCode.PARAMETER_ERROR;
            }

            firstInput = false;
        }

        orderedKeys.retainAll(selectedKeys);
        cf.getOutput("out").mkdirs();

        final IndexFile outputIndex = new IndexFile();
        for (String key: orderedKeys) {
            outputIndex.add(key, new File(fileMap.get(key)));
        }
        outputIndex.write(cf, "out");

        return ErrorCode.OK;
    }

    public static void main(String[] args) {
        new ArrayCombiner().run(args);
    }
}
