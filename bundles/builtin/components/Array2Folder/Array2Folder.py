# /usr/bin/env python3
import sys, shutil, os
import anduril
from anduril.args import *


def copytree(src, dst, symlinks=False):
    """Recursively copy a directory tree using shutil.copy2().

    Modified from shutil.copytree to overcome copystat -problems
    on NTFS / CIFS / other non-octect permission media
     ( uses Anduril logger )

    """
    names = os.listdir(src)
    if not os.path.exists(dst):
        os.makedirs(dst)
    errors = []
    permission_errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks)
            else:
                # Will raise a SpecialFileError for unsupported file types
                shutil.copyfile(srcname, dstname)
                try:
                    shutil.copystat(srcname, dstname)
                except Exception:
                    # stats may not copy.
                    permission_errors.append(dstname)
                    pass
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except Error as err:
            errors.extend(err.args[0])
        except EnvironmentError as why:
            errors.append((srcname, dstname, str(why)))
    try:
        shutil.copystat(src, dst)
    except Exception:
        # If permissions cannot be set, skip this
        permission_errors.append(dst)
        pass

    if len(permission_errors) > 0:
        write_log("Could not change permissions: " + ", ".join(permission_errors))

    if errors:
        raise Error(errors)


def copyfile(src, dst, symlinks=False):
    if not os.path.exists(os.path.dirname(dst)):
        os.makedirs(os.path.dirname(dst))
    if symlinks:
        os.symlink(src, dst)
    else:
        shutil.copyfile(src, dst)


def copypath(src, dst, symlinks=False):
    if os.path.isdir(src):
        copytree(src, dst, symlinks=symlinks)
    else:
        copyfile(src, dst, symlinks=symlinks)


def replace_tags(name, key, fileName):
    foo, ext = os.path.splitext(fileName)
    return (
        name.replace("@key@", key)
        .replace("@file@", fileName)
        .replace("@ext@", ext)
        .replace("@instance@", meta_instanceName)
    )


os.makedirs(output_out)
template = {}
if input_template:
    reader = anduril.TableReader(input_template, column_types=str)
    fields = reader.get_fieldnames()
    if param_fileCol not in fields:
        write_error('Column "' + param_fileCol + '" not found.')
        sys.exit(1)
    if param_keyCol not in fields:
        write_error('Column "' + param_keyCol + '" not found.')
        sys.exit(1)
    for row in reader:
        template[row.get(param_keyCol)] = row.get(param_fileCol)

for row in input_in.items():
    dst = replace_tags(param_fileMode, row[0], os.path.basename(row[1]))
    if input_template:
        if row[0] in template:
            # force relative to output
            dst = template[row[0]].lstrip("/.")
    if dst in ["", "."]:
        dst = ""
    copypath(row[1], os.path.join(output_out, dst), param_link)
