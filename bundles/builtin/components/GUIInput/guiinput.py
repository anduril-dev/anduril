from anduril.args import * 
import easygui,sys,os,re

def typeValue(t,v):
    if t in ('double','float','int','string','str','file','dir'):
        return str(v)
    if t == 'boolean':
        if v.lower() == 'true': 
            return 'true'
        if v.lower() == 'false': 
            return 'false'
        write_error("Error parsing boolean: "+v)
        sys.exit(1)
    #if t == 'int':
    #    return int(v)
    #if t in ('float','double'):
    #    return float(v)
    write_error("Unknown type: '"+t+"'")
    sys.exit(1)

def correctType(t,v):
    if t in ('string','str'):
        return True
    if t == 'file':
        return os.path.isfile(v)
    try:
        if t == 'int':
            foo=int(v)
        if t in ('float','double'):
            foo=float(v)
    except ValueError:
        return False
    return True

def templateParse(fileIn,fileDefaults):
    defaults={}
    if fileDefaults is not None:
        defaults=defaultsParse(fileDefaults)
    reader=open(fileIn,'rb')
    values={}
    order=[]
    for l in reader:
        if l.strip()=='':
            continue
        parts=l.split('=',1)
        if len(parts) is not 2:
            write_error('Error reading template row: '+l)
            sys.exit(1)
        key=parts[0].strip()
        info=parts[1].split(' ',1)
        if len(info)==1:
            info.append('')
        info=[x.strip().lower() for x in info]
        info=[ info[0], info[1].replace('\\n','\n') ]
        if key in defaults:
            info.append( typeValue(info[0], defaults[key]))
        else:
            if info[0] in ('file', 'dir'):
                info.append( DEF_PATH )
            else:
                info.append( None )
        values[key]=info
        order.append(key)
    return (values,order)
    
def defaultsParse(fileIn):
    reader=open(fileIn,'rb')
    values={}
    for l in reader:
        if l.strip()=='':
            continue
        parts=l.split('=',1)
        if len(parts) is not 2:
            write_error('Error reading defaults row: '+l)
            sys.exit(1)
        values[parts[0].strip()]=parts[1].rstrip('\n')
    return values
    
def askQuestions(values,order):
    for key in order:
        if values[key][2] != None and SKIP:
            if values[key][0] in ('file','dir'):
                if values[key][2] != DEF_PATH:
                    continue
            else:
                continue
        if values[key][0]=='boolean':
            if values[key][2]=='true' or values[key][2] == None:
                choices=('true', 'false','cancel')
            else:
                choices=('false', 'true','cancel')
            values[key][2]=easygui.buttonbox(msg=values[key][1], title=newTitle(key), choices=choices)
            if values[key][2] == 'cancel':
                values[key][2]=None
        if values[key][0] in ('int','float','double','str','string'):
            if values[key][2] == None:
                values[key][2]=''
            return_value=None
            enter_value=str(values[key][2])
            while True:
                return_value=easygui.enterbox(msg=values[key][1], title=newTitle(key), default=enter_value, strip=True)
                if not correctType(values[key][0], return_value):
                    enter_value=return_value
                    easygui.msgbox(msg='Your entry is not of type: '+values[key][0], title=newTitle(key))
                else:
                    break
            values[key][2]=return_value
        if values[key][0] =='file':
            if values[key][2] == None:
                values[key][2]=DEF_PATH
            enter_value=str(values[key][2])
            return_value=easygui.enterbox(msg=values[key][1]+"\nPress OK to browse if entry is a folder.", title=newTitle(key), default=enter_value, strip=True)
            if return_value == None:
                write_error('User canceled the component.')
                sys.exit(1)
            if return_value == '':
                return_value=DEF_PATH
            if not correctType(values[key][0], return_value):
                return_value=easygui.fileopenbox(msg=values[key][1], title=newTitle(key), default=return_value)    
            values[key][2]=return_value
        if values[key][0] =='dir':
            if values[key][2] == None:
                values[key][2]=DEF_PATH
            values[key][2]=easygui.diropenbox(msg=values[key][1], title=newTitle(key), default=str(values[key][2]))
        if values[key][2] == None:
            write_error('User canceled the component.')
            sys.exit(1)
    return values

def writeValues(values):
    for key in values:
        if values[key][0] in ('file','dir'):
            output_out.write(key, values[key][2])
        else:
            with open(os.path.join(output_out.get_folder(), key),'w') as f:
                f.write(values[key][2])
                f.close()
            output_out.write(key, key)
    with open(output_saved, 'a') as f:
        for key in values:
            f.write(key+'='+values[key][2]+'\n')
        f.close()

def newTitle(k):
    return TITLE+' ['+k+']'


TITLE=param_title
SKIP=param_skipDefaults
if param_defaultPath.startswith(os.sep):
    DEF_PATH=os.path.abspath(param_defaultPath)+os.sep
else:
    sourceFile=re.sub(":[0-9]+$","",meta_sourceLocation)
    sourceFolder=os.path.abspath(os.path.dirname( sourceFile ))
    DEF_PATH=os.path.join(sourceFolder, param_defaultPath)+os.sep

(values,order)=templateParse(input_template, input_defaultValues)
arrayValues=askQuestions(values,order)
writeValues(arrayValues)
