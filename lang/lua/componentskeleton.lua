--- Command file object.
-- @class table
-- @name commandfile
-- @field getinput Method cf:getinput(port) returns file name of
--   given input port.
-- @field getinputindex Method cf:getinputindex(port) returns file name
--   of index file of given input array port.
-- @field getoutput Method cf:getoutput(port) returns file name of
--   given output port.
-- @field getoutputindex Method cf:getoutputindex(port) returns file name
--   of index file of given output array port.
-- @field getparameter Method cf:getparameter(port, [type]) returns value
--   of given parameter. If type is omitted, a string is returned;
--   type may also be "boolean", "float" or "int".
-- @field getmetadata Method cf:getmetadata(name) returns value of
--   given metadata item.
-- @field err Method cf:err(message) writes to Anduril error stream.
-- @field log Method cf:log(message) writes to Anduril log stream.
-- @field haserrors Boolean attribute that indicates whether a
--   message has been written to error stream.

-- Master list is in ErrorCode.java
STATUS_OK = 0
GENERIC_ERROR = 1
NO_COMMAND_FILE = 20
COMMAND_FILE_IO_ERROR = 21
INPUT_IO_ERROR = 22
INVALID_ERRORSTREAM = 23
PARAMETER_ERROR = 24
UNCAUGHT_EXCEPTION = 25
INVALID_INPUT = 26
INVALID_COMMAND_FILE = 27
OUTPUT_IO_ERROR = 30

--- Return a string with whitespace removed from start and end.
-- @param s Input string
function trim(s)
  return s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
end

--- Return true if the file path is an absolute path.
function isabs(path)
    first = path:sub(1, 1)
    return first == "/" or first == "\\" or path:sub(2, 3) == ":\\"
end

--- Return concatenation of two file paths.
function pathjoin(path1, path2)
    first = path2:sub(1, 1)
    if first == "/" or first == "\\" then
        path2 = path2:sub(2)
    elseif path2:sub(2, 3) == ":\\" then
        path2 = path2:sub(4)
    end
    
    last = path1:sub(#path1)
    if last ~= "/" and last ~= "\\" then
        return path1 .. "/" .. path2
    else
        return path1 .. path2
    end
end

--- Split the path into dirname and basename components.
-- Return these two values. The first component may be
-- empty.
function pathsplit(path)
    i = #path - 1
    while i > 0 and path:sub(i, i) ~= "/" and path:sub(i, i) ~= "\\" do
        i = i - 1
    end
    return path:sub(1, i), path:sub(i+1, #path)
end

-- Command file handing --

local function cf_getinput(cf, port)
    return cf.inputs[port]
end

local function cf_getinputindex(cf, port)
    return cf.inputs["_index_" .. port]
end

local function cf_getoutput(cf, port)
    return cf.outputs[port]
end

local function cf_getoutputindex(cf, port)
    return cf.outputs["_index_" .. port]
end

local function cf_getmetadata(cf, metadata)
    return cf.metadata[metadata]
end

local function cf_getparameter(cf, param, ptype)
    value = cf.parameters[param]
    if value == nil or ptype == nil or ptype == "string" then
        return value
    end

    if ptype == "boolean" then
        if value == "true" then
            return true
        elseif value == "false" then
            return false
        else
            error(string.format("Parameter %s is not valid boolean: %s", param, value))
        end
    elseif ptype == "float" then
        return tonumber(value)
    elseif ptype == "int" then
        numeric = tonumber(value)
        if (numeric % 1) ~= 0 then
            error(string.format("Parameter %s is not integer: %f", param, numeric))
        end
        return numeric
    else
        error(string.format("Invalid parameter type arg: %s", ptype))
    end
end

local function cf_write_log(cf, message)
    filename = cf.outputs["_log"]
    if filename == nil then
        error("Log file is not defined")
    end
    
    fileobj = io.open(filename, "a+")
    fileobj:write(message)
    fileobj:write("\n")
    fileobj:close()
end

local function cf_write_error(cf, message)
    cf.haserrors = true

    filename = cf.outputs["_errors"]
    if filename == nil then
        error("Error file is not defined")
    end
    
    fileobj = io.open(filename, "a+")
    fileobj:write(message)
    fileobj:write("\n")
    fileobj:close()
end

--- Parses command file and returns a command file object.
-- Normally not called manually.
-- @param filename File name of the command file
-- @see commandfile
function read_command_file(filename)
    cf = {inputs={}, outputs={}, parameters={}, metadata={},
        haserrors=false,
        getinput=cf_getinput, getinputindex=cf_getinputindex,
        getoutput=cf_getoutput, getoutputindex=cf_getoutputindex,
        getparameter=cf_getparameter,
        err=cf_write_error, log=cf_write_log}

    for line in io.lines(filename) do
        if line:sub(1, 1) ~= '#' then
            pos = line:find("=")
            if pos == nil then
                error(string.format("Invalid line: %s", line))
            end
            key = trim(line:sub(1, pos-1))
            value = trim(line:sub(pos+1))
            
            pos = key:find(".", 1, true)
            if pos == nil then
                error(string.format("Invalid key on line: %s", line))
            end
            key_class = key:sub(1, pos-1)
            key_entry = key:sub(pos+1)
            
            if #key_entry == 0 then
                error(string.format("Invalid key on line: %s", line))
            end
            
            if key_class == "input" then
                if #value == 0 then
                    value = nil
                end
                cf.inputs[key_entry] = value
            elseif key_class == "metadata" then
                if #value == 0 then
                    value = nil
                end
                cf.metadata[key_entry] = value
            elseif key_class == "output" then
                if #value == 0 then
                    error(string.format("Missing output file on line: %s", line))
                end
                cf.outputs[key_entry] = value
            elseif key_class == "parameter" then
                cf.parameters[key_entry] = value
            else
                error(string.format("Invalid key on line: %s", line))
            end
        end
    end
    
    return cf
end

--- Entry point for Anduril components.
-- This functions parses the command file and invokes given function with
-- the command file object as argument. Afterwards, calls os.exit to exit
-- lua.
-- @param execfunc A function with a single argument (command file object)
--   that performs the actual work of the component. Must return an Anduril
--   status code (constant STATUS_OK, GENERIC_ERROR, INPUT_IO_ERROR,
--   PARAMETER_ERROR, INVALID_INPUT or OUTPUT_IO_ERROR). Returning nil is
--   interpreted as STATUS_OK.
-- @see commandfile
function componentmain(execfunc)
    if #arg == 0 then
        print("No command file given")
        os.exit(NO_COMMAND_FILE)
    end
    
    ok, cf = pcall(read_command_file, arg[1])
    if not ok then
        print(string.format("Error reading command file %s: %s", arg[1], cf))
        os.exit(INVALID_COMMAND_FILE)
    end
    
    status = execfunc(cf)
    if status == nil then
        status = STATUS_OK
    end
    if cf.haserrors and status == STATUS_OK then
        status = GENERIC_ERROR
    end
    os.exit(status)
end
