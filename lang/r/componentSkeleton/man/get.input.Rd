\name{get.input}
\alias{get.input}
\title{Get input filename}
\description{
Return the input filename or NULL if the input name is invalid.
Input filename may be the empty string if the input port is optional.
}
\usage{
get.input(command.file, input.name)
}
\arguments{
  \item{command.file}{Usually produced by \code{\link{main}}.}
  \item{input.name}{Input port name.}
}
\author{ Kristian Ovaska (\email{kristian.ovaska@helsinki.fi}) }
\keyword{input}