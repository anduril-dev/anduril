\name{SetList.new}
\alias{SetList.new}
\title{Initialize a new SetList instance}
\description{
Initialize a SetList instance, optionally filling it with
values. The return value is a data frame with two columns,
ID and Members (both character). This constructor does not
support annotation columns. To create annotation columns,
the data frame must be constructed manually.

SetList instances can be written to CSV files using \code{\link{CSV.write}}.
}
\usage{
SetList.new(set.names=NULL, set.contents=NULL)
}
\arguments{
	\item{set.names}{If non-NULL, this is a character vector
	that contains set identifiers. Duplicated set identifiers
    are not allowed. If NULL, the initialized SetList is empty.}
	\item{set.contents}{If non-NULL, this is a list of character
	vectors. Each vector is the contents of one set. NA can be
    used to represent an empty set. The list must have as many 
    elements as \code{set.names}. If NULL, the sets are initialized
    to empty sets. It is legal to have non-NULL \code{set.names} 
    and NULL \code{set.contents}.}
}
\author{ Kristian Ovaska (\email{kristian.ovaska@helsinki.fi}) }
