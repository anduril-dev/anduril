\name{SampleGroupTable.write}
\alias{SampleGroupTable.write}
\title{Write a SampleGroupTable object}
\description{
Write a SampleGroupTable object to a file.
}
\usage{
SampleGroupTable.write(filename, group.table)
}
\arguments{
  \item{filename}{Target filename.}
  \item{group.table}{SampleGroupTable object.}
}
\author{ Kristian Ovaska (\email{kristian.ovaska@helsinki.fi}) }
\keyword{misc}