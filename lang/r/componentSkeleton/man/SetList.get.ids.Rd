\name{SetList.get.ids}
\alias{SetList.get.ids}
\title{Return the set identifiers of a SetList}
\description{
Return the set identifiers (names) as a character vector.
}
\usage{
SetList.get.ids(sets)
}
\arguments{
	\item{sets}{SetList instance as a data frame with (at least) columns
	ID and Members.}
}
\author{ Kristian Ovaska (\email{kristian.ovaska@helsinki.fi}) }
