\name{Array.new}
\alias{Array.new}
\title{Initialize a new Array instance}
\description{
Initialize an Array instance, optionally filling it with
values.
}
\usage{
Array.new(keys=NULL, files=NULL)
}
\arguments{
	\item{keys}{If non-NULL, this is a character vector
	that contains array keys. Duplicated array keys
    are not allowed.}
	\item{files}{If non-NULL, this is a character vector that
    contains the files associated to the keys. The character vector
    must have as many elements as non-NULL \code{keys}. If NULL, the array is
    initialized empty. It is legal to have NULL \code{keys} 
    and non-NULL \code{files}. In this case the keys will be initialized
    to interger values (as character strings) starting from 1.}
}
\value{
A data frame with two columns, Key and File (both character).
}
\author{ Erkka Valo (\email{erkka.valo@helsinki.fi}) }
