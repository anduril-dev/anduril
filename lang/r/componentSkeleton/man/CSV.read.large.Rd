\name{CSV.read.large}
\alias{CSV.read.large}
\title{Read a CSV file}
\description{
Read a CSV file as a data frame. The data frame has column names
but no row names. This function is especially useful for large CSV files where it can 
load the data much faster than the CSV.read function.
}
\usage{
CSV.read(filename, expected.columns = NULL)
}
\arguments{
  \item{filename}{Source filename.}
  \item{expected.columns}{if non-NULL, the column names are checked against
  this vector of expected column names and if they don't match, an
  error is generated. The order of column names is relevant.}
}
\author{ Amjad Alkodsi (\email{amjad.alkodsi@helsinki.fi}) }
\keyword{file}
