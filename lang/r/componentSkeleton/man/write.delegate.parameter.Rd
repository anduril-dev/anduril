\name{write.delegate.parameter}
\alias{write.delegate.parameter}
\title{Write an entry into delegate parameter file}
\description{
Write a line with format \code{key=value} into the delegate parameter file.
}
\usage{
write.delegate.parameter(command.file, delegate.name, param.name, value)
}
\arguments{
  \item{command.file}{Usually produced by \code{\link{main}}.}
  \item{delegate.name}{Name of delegate.}
  \item{param.name}{Name of delegate's parameter.}
  \item{value}{Value of the parameter.}
}
\value{
Raise an error if the delegate is not defined.
}
\author{ Kristian Ovaska (\email{kristian.ovaska@helsinki.fi}) }
\keyword{file}