\name{latex.tabular}
\alias{latex.tabular}
\title{LaTeX code for inserting a tabular environment}
\description{
Create LaTeX code for a LaTeX tabular environment, corresponding to a
\code{\\begin\{tabular\}} statement.
This is not a float; to create a floating table, use latex.table.
Return a character vector that contains LaTeX code lines.
}
\usage{
latex.tabular(table.obj, column.alignment, long = FALSE, use.row.names = FALSE, escape = TRUE, escape.content = TRUE, numberFormat = "\%.2f")
}
\arguments{
  \item{table.obj}{Data frame or matrix. If column names are defined, they
    are used as a header line.}
  \item{column.alignment}{Column alignment string. Must contain as many entries
    as there are columns.}
  \item{long}{If TRUE, use the longtable environment that enables multi-page
    tables.}
  \item{use.row.names}{If TRUE and row names are defined in \code{table.obj},
    add a column that contains row names. This column must be included in
    \code{column.alignment}.}
  \item{escape}{Tells if the column names should be converted into LaTeX format}
  \item{escape.content}{Tells if the content of the table cells should be converted into LaTeX format}
  \item{numberFormat}{Formatting pattern for the numeric values.}
}
\author{ Kristian Ovaska \email{kristian.ovaska@helsinki.fi},
         Marko Laakso \email{Marko.Laakso@Helsinki.FI} }
\keyword{}
