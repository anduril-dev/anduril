\name{get.output.array.index}
\alias{get.output.array.index}
\title{Get the array index file}
\description{
Get the array index file associated to an array output port.
}
\usage{
get.output.array.index(command.file, array.port.name)
}
\arguments{
	\item{command.file}{Usually produced by main.}
	\item{array.port.name}{Output port name.}
}

\author{ Erkka Valo (\email{erkka.valo@helsinki.fi}) }
