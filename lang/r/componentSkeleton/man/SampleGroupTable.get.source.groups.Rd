\name{SampleGroupTable.get.source.groups}
\alias{SampleGroupTable.get.source.groups}
\title{Get vector of source group IDs}
\description{
Return the source groups of given target group as a vector.
If the target group is not found, return NULL.
}
\usage{
SampleGroupTable.get.source.groups(group.table, group.id)
}
\arguments{
  \item{group.table}{SampleGroupTable object.}
  \item{group.id}{Group ID. This refers to the first column of
  the table.}
}
\author{ Kristian Ovaska (\email{kristian.ovaska@helsinki.fi}) }
\keyword{misc}