\name{Array.getFiles}
\alias{Array.getFiles}
\title{Get the files contained in the array}
\description{
Get the files contained by the array.
}
\usage{
Array.getFiles(array)
}

\arguments{
	\item{array}{Array instance as a data frame with at least
    columns Key and File.}
}

\value{
A vector of character strings representing the files contained
in the array.
}
\author{ Erkka Valo (\email{erkka.valo@helsinki.fi}) }
