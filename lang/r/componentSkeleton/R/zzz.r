.First.lib <- function(libname, pkgname) {
    pkgdir <- file.path(libname, pkgname)
    componentSkeleton.set.package.dir(pkgdir)
}
.onAttach <- function(libname,pkgname){
    invisible(NULL)
}

.onUnload <- function(libpath){
    invisible(NULL)
}


### Package tools ######################################################

if (!exists('.componentSkeleton.package.dir')) {
    .componentSkeleton.package.dir <- NULL
}

componentSkeleton.set.package.dir <- function(directory) {
    .componentSkeleton.package.dir <<- directory
}

componentSkeleton.get.package.dir <- function() {
    return(.componentSkeleton.package.dir)
}

componentSkeleton.package.file.path <- function(...) {
    pkg.dir <- componentSkeleton.get.package.dir()
    if (is.null(pkg.dir)) {
        stop(sprintf('Directory for package componentSkeleton is not set'))
    }
    return(file.path(pkg.dir, ...))
}
