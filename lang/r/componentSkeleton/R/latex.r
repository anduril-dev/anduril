LATEX.SECTIONS <- c('section', 'subsection', 'subsubsection')

# Write the main document (document.tex) by concatenating text
# lines.
# Input:
# - cf: command file object
# - output.name: name of LaTeX output port
# - latex.lines: vector of strings
latex.write.main <- function(cf, output.name, latex.lines) {
    stopifnot(is.character(latex.lines))
    
    out.dir <- get.output(cf, output.name)
    if (!file.exists(out.dir)) {
        dir.create(out.dir, recursive=TRUE)
    }
    docfile <- file.path(out.dir, LATEX.DOCUMENT.FILE)
    textfile.write(docfile, paste(latex.lines, collapse='\n'))
}

latex.quote <- function(string) {
    stopifnot(is.character(string))
    string <- gsub('\\',           'tmpBackslash',  string, fixed=TRUE)
    string <- gsub('_',            '\\_',           string, fixed=TRUE)
    string <- gsub('$',            '\\$',           string, fixed=TRUE)
    string <- gsub('%',            '\\%',           string, fixed=TRUE)
    string <- gsub('#',            '\\#',           string, fixed=TRUE)
    string <- gsub('&',            '\\&',           string, fixed=TRUE)
    string <- gsub('^',            '\\^{}',         string, fixed=TRUE)
    string <- gsub('~',            '\\~{}',         string, fixed=TRUE)
    string <- gsub('{',            '\\{',           string, fixed=TRUE)
    string <- gsub('}',            '\\}',           string, fixed=TRUE)
    string <- gsub('<',            '$<$',           string, fixed=TRUE)
    string <- gsub('>',            '$>$',           string, fixed=TRUE)
    string <- gsub('|',            '$|$',           string, fixed=TRUE)
    string <- gsub('tmpBackslash', '$\\backslash$', string, fixed=TRUE)
    return(string)
}

# Create text lines for a LaTeX graphics figure.
latex.figure <- function(filename,
                         caption      = NULL,
                         position     = '!ht',
                         image.scale  = NULL,
                         image.width  = NULL,
                         image.height = NULL,
                         quote.capt   = TRUE,
                         fig.label    = NULL) {
    stopifnot(is.character(filename))
    stopifnot(length(filename) == 1)
    stopifnot(is.character(position))
    stopifnot(length(position) == 1)
    stopifnot(is.null(caption)      || (is.character(caption)    && length(caption)      == 1))
    stopifnot(is.null(image.scale)  || (is.numeric(image.scale)  && length(image.scale)  == 1))
    stopifnot(is.null(image.width)  || (is.numeric(image.width)  && length(image.width)  == 1))
    stopifnot(is.null(image.height) || (is.numeric(image.height) && length(image.height) == 1))
    
    tex <- character()
    tex <- c(tex, sprintf('\\begin{figure}[%s]', position))
    tex <- c(tex, '\\begin{center}')
    
    params <- NULL
    
    if (!is.null(image.scale)) {
        params <- sprintf('scale=%s', image.scale)
    }
    else if (!is.null(image.width) && !is.null(image.height)) {
        params <- sprintf('width=%scm,height=%scm,keepaspectratio=true', image.width, image.height)
    }
    else if (!is.null(image.width)) {
        params <- sprintf('width=%scm', image.width)
    }
    else if (!is.null(image.height)) {
        params <- sprintf('height=%scm', image.height)
    }
    
    if (is.null(params)) {
        tex <- c(tex, sprintf('\\includegraphics{%s}', filename))
    }
    else {
        stopifnot(is.character(params))
        stopifnot(length(params) == 1)
        tex <- c(tex, sprintf('\\includegraphics[%s]{%s}', params, filename))
    }
    
    tex <- c(tex, '\\end{center}')
    if (!is.null(caption) && nchar(caption) > 0) {
        if (quote.capt) {
            caption = latex.quote(caption)
        }
        tex <- c(tex, sprintf('\\caption{%s}', caption))
    }
    if (!is.null(fig.label) && nchar(fig.label) > 0) {
        tex <- c(tex, sprintf('\\label{%s}', fig.label))
    }
    tex <- c(tex, '\\end{figure}')
    tex <- c(tex, '')
    return(tex)
}

# Create text lines for a LaTeX tabular environment (\begin{tabular}).
# This is not a float; to create a floating table, use latex.table. 
# Input:
# - table.obj: data frame or matrix. If column names are defined, they
#   are used as header.
# - column.alignment: the second parameter to \begin{tabular}
# - use.row.names: if TRUE, place row names from table.obj to the first column
latex.tabular <- function(table.obj,
                          column.alignment,
                          long           = FALSE,
                          use.row.names  = FALSE,
                          escape         = TRUE,
                          escape.content = TRUE,
                          numberFormat   = '%.2f') {
    stopifnot(is.data.frame(table.obj) || is.matrix(table.obj))
    stopifnot(is.character(column.alignment) && length(column.alignment) == 1)

    conv <- function(x) {
        if (is.na(x))       return('')
        if (is.numeric(x))  return(sprintf(numberFormat, x))
        if (escape.content) return(latex.quote(x))
        return(x)
    }

    if (long) {
        tex <- c(sprintf('\\begin{longtable}{%s}', column.alignment))
    } else {
        tex <- c(sprintf('\\begin{tabular}{%s}', column.alignment))	
    }

    if (!is.null(colnames(table.obj))) {
        if (escape) {
            values <- paste(latex.quote(colnames(table.obj)), collapse=' & ')
        } else {
            values <- paste(colnames(table.obj), collapse=' & ')
        }
        if (use.row.names && !is.null(rownames(table.obj))) {
            values <- c(' & ', values)
        }
        tex <- c(tex, values, '\\\\', '\\hline')
    }

    for (i in 1:nrow(table.obj)) {
        values <- sapply(table.obj[i,], conv)
        if (use.row.names && !is.null(rownames(table.obj))) {
            values <- latex.quote(c(rownames(table.obj)[i], values))
        }
        tex <- c(tex, paste(values, collapse=' & '), '\\\\')
    }

    if(long){
        tex <- c(tex, '\\end{longtable}')
    } else{
        tex <- c(tex, '\\end{tabular}')
    }
    return(tex)
}

# Create text lines for a LaTeX table environment (\begin{table}
# and a \begin{tabular} inside it).
latex.table <- function(table.obj,
                        column.alignment,
                        caption        = NULL,
                        position       = '!ht',
                        long           = FALSE,
                        use.row.names  = FALSE,
                        escape         = TRUE,
                        escape.content = TRUE,
                        label          = NULL,
                        numberFormat   = '%.2f') {
    stopifnot(is.character(position))
    stopifnot(length(position) == 1)
    stopifnot(is.null(caption) || (is.character(caption) && length(caption) == 1))

    if (!is.null(caption) && nchar(caption) > 0) {
        if (escape) {
            caption <- sprintf('\n\\caption{%s}', latex.quote(caption))
        } else {
            caption <- sprintf('\n\\caption{%s}', caption)
        }
    } else {
        caption = ''
    }
 
    if (long) {
        conv <- function(x) {
            if (is.na(x))       return('')
            if (is.numeric(x))  return(sprintf(numberFormat, x))
            if (escape.content) return(latex.quote(x))
            return(x)
        }
        tex <- sprintf('\\begin{longtable}{%s}%s\\\\', column.alignment, caption)
        if (!is.null(colnames(table.obj))) {
            if (escape) {
                values <- paste(latex.quote(colnames(table.obj)), collapse=' & ')
            } else {
                values <- paste(colnames(table.obj), collapse=' & ')
            }
            if (use.row.names && !is.null(rownames(table.obj))) {
                values <- c(' & ', values)
            }
            tex <- c(tex, values, '\\\\\\hline')
        }
        for (i in 1:nrow(table.obj)) {
            values <- sapply(table.obj[i,], conv)
            if (use.row.names && !is.null(rownames(table.obj))) {
                values <- latex.quote(c(rownames(table.obj)[i], values))
            }
            tex <- c(tex, paste(values, collapse=' & '), '\\\\')
        }
        tex <- c(tex, '\\end{longtable}')
    } else {
        tex <- c(sprintf('\\begin{table}[%s]', position),
                 latex.tabular(table.obj, column.alignment, FALSE, use.row.names, escape, escape.content,
                               numberFormat=numberFormat),
                 caption,
                 '\\end{table}')
    }
    return(tex)
}
