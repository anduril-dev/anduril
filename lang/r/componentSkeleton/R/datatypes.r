### Number #############################################################

Number.read <- function(filename) {
    stopifnot(is.character(filename))
    f <- file(filename)
    contents <- readLines(f)
    close(f)
    return(as.numeric(contents[[1]]))
}

Number.write <- function(filename, number) {
    stopifnot(is.character(filename))
    stopifnot(is.numeric(number))
    cat(number, file=filename)
}

### NumMatrix ##########################################################

NumMatrix.read <- function(filename) {
    stopifnot(is.character(filename))
    fr <- read.table(filename, sep='\t', stringsAsFactors=FALSE, colClasses='numeric')
    return(as.matrix(fr))
}

NumMatrix.write <- function(filename, matrx) {
    stopifnot(is.character(filename))
    stopifnot(is.matrix(matrx))
    
    outfile <- file(filename, 'wb')
    write.table(matrx, outfile, sep='\t', eol='\n', row.names=FALSE, col.names=FALSE)
    close(outfile)
}

### SampleGroupTable ##################################################

SAMPLE.GROUP.TABLE.MEAN <- 'mean'
SAMPLE.GROUP.TABLE.MEDIAN <- 'median'
SAMPLE.GROUP.TABLE.RATIO <- 'ratio'
SAMPLE.GROUP.TABLE.SAMPLE <- 'sample'

SAMPLE.GROUP.TABLE.CENTRAL.MEASURE <- c(
    SAMPLE.GROUP.TABLE.MEAN,
    SAMPLE.GROUP.TABLE.MEDIAN
)

is.SampleGroupTable <- function(group.table) {
    return(is.data.frame(group.table)
        && !is.null(colnames(group.table))
        && 'ID' %in% colnames(group.table)
        && 'Members' %in% colnames(group.table)
        && 'Type' %in% colnames(group.table)
        && is.character(group.table$ID)
        && is.character(group.table$Members)
        && is.character(group.table$Type))
}

# Create a new, empty SampleGroupTable.
SampleGroupTable.new <- function() {
    fr <- data.frame(ID=character(0), Members=character(0), Type=character(0),
        Description=character(0), stringsAsFactors=FALSE)
    stopifnot(is.SampleGroupTable(fr)) # Sanity check
    return(fr)
}

# Read a sample group table from a file.
SampleGroupTable.read <- function(filename) {
    stopifnot(is.character(filename))
    fr <- CSV.read(filename)
    stopifnot(is.SampleGroupTable(fr))
    return(fr)
}

# Read the sample group table into a file.
SampleGroupTable.write <- function(filename, group.table) {
    stopifnot(is.SampleGroupTable(group.table))
    CSV.write(filename, group.table)
}

# Add a sample group to an existing SampleGroupTable.
# Return the updated SampleGroupTable object.
SampleGroupTable.add.group <- function(group.table, group.id, type, sample.ids, description) {
    stopifnot(is.SampleGroupTable(group.table))
    stopifnot(is.character(group.id))
    stopifnot(length(group.id) == 1)
    stopifnot(is.character(sample.ids))
    stopifnot(length(sample.ids) >= 1)
    stopifnot(length(description) == 1)
    
    sample.id.str <- paste(sample.ids, collapse=',')
    if (ncol(group.table) == 3) group.row <- c(group.id, sample.id.str, type)
    else group.row <- c(group.id, sample.id.str, type, description)
    group.table[nrow(group.table)+1,] <- group.row
    return(group.table)
}

# Return all group IDs in order.
# Types must be NULL or a subset SAMPLE.GROUP.TABLE.TYPES.
# If types is not NULL, return only groups that match the type.
SampleGroupTable.get.groups <- function(group.table, types=NULL) {
    stopifnot(is.SampleGroupTable(group.table))
    
    if (!is.null(types)) {
        good <- group.table$Type %in% types
        return(group.table$ID[good])
    }
    else {
        return(group.table$ID)
    }
}

# Return group type for given group.
# If the group is not found, return NULL.
SampleGroupTable.get.type <- function(group.table, group.id) {
    stopifnot(is.SampleGroupTable(group.table))
    stopifnot(is.character(group.id))
    stopifnot(length(group.id) == 1)

    type <- group.table$Type[group.table$ID == group.id]
    if (length(type) == 0) return(NULL)
    else return(type)
}

# Return source group IDs as a character vector for given group.
# If the group is not found, return NULL.
SampleGroupTable.get.source.groups <- function(group.table, group.id) {
    stopifnot(is.SampleGroupTable(group.table))
    stopifnot(is.character(group.id))
    stopifnot(length(group.id) == 1)
    
    sample.id.str <- group.table$Members[group.table$ID == group.id]
    if (length(sample.id.str) == 0) return(NULL)
    else return(strsplit(sample.id.str, ',', fixed=TRUE)[[1]])
}

# Return group description for given group.
# If the group is not found, return NULL.
SampleGroupTable.get.description <- function(group.table, group.id) {
    stopifnot(is.SampleGroupTable(group.table))
    stopifnot(is.character(group.id))
    stopifnot(length(group.id) == 1)
    
    desc <- group.table$Description[group.table$ID == group.id]
    if (length(desc) == 0) return(NULL)
    else return(desc)
}

# Return the sample group table as a list. The list has a named element
# for each group. The element has the items GroupID, SourceGroups (as a vector)
# and Description.
SampleGroupTable.as.list <- function(group.table) {
    stopifnot(is.SampleGroupTable(group.table))
    
    li <- list()
    for (group.id in SampleGroupTable.get.groups(group.table)) {
        type <- SampleGroupTable.get.type(group.table, group.id)
        sources <- SampleGroupTable.get.source.groups(group.table, group.id)
        desc <- SampleGroupTable.get.description(group.table, group.id)
        group.list <- list(ID=group.id, Members=sources, Type=type, Description=desc)
        li[[group.id]] <- group.list
    }
    
    return(li)
}

SampleGroupTable.find.group <- function(group.table, sample.ids, types=NULL, any.order=FALSE) {
    stopifnot(is.SampleGroupTable(group.table))
    stopifnot(is.character(sample.ids))
    stopifnot(length(sample.ids) >= 1)
    stopifnot(is.logical(any.order))
    stopifnot(length(any.order) == 1)
    
    if (any.order) sample.ids <- sort(sample.ids)
    
    group.list <- SampleGroupTable.as.list(group.table)
    for (i in 1:length(group.list)) {
        this.type <- group.list[[i]]$Type
        if (!is.null(types) && !(this.type %in% types)) next
        
        this.samples <- group.list[[i]]$Members
        if (any.order) this.samples <- sort(this.samples)
        if (identical(sample.ids, this.samples)) return(group.list[[i]]$ID)
    }
    
    return(NULL)
}

### SetList ############################################################

SetList.read <- function(filename) {
    stopifnot(is.character(filename))
    fr <- CSV.read(filename)
    stopifnot('ID' %in% colnames(fr))
    stopifnot('Members' %in% colnames(fr))
    fr$Members[is.na(fr$Members)] <- ""
    # Remove duplicate values inside sets.    
    set.list <- SetList.new(set.names=fr$ID, set.contents=strsplit(fr$Members, ","))
    fr$ID <- set.list$ID
    fr$Members <- set.list$Members
    return(fr)
}

SetList.new <- function(set.names=NULL, set.contents=NULL) {
    stopifnot(is.list(set.contents) || is.null(set.contents))
    if (!is.null(set.names) && !is.null(set.contents)) {
        stopifnot(length(set.names) == length(set.contents))
        stopifnot(all(!duplicated(set.names)))
    }
    
    if (is.null(set.names)) set.names <- character(0)
    
    set.list <- data.frame(ID=character(), Members=character(), stringsAsFactors=F)
    if (is.null(set.contents)) {
        members <- rep(NA, length(set.names))
    }else {
        for(i in 1:length(set.names)) {
            set.list <- SetList.assign(set.list, set.names[i], set.contents[[i]])
        }
    }
    return(set.list)
}

SetList.get.ids <- function(sets) {
    return(sets$ID)
}

SetList.get.members <- function(sets, set.id) {
    index <- match(set.id, sets$ID)
    if(is.na(index)) {
        return(NULL)
    } else {
        members <- sets$Members[index]
        return(split.trim(members, ','))
    }
}

SetList.assign <- function(sets, id, contents) {
    if(!(length(contents) == 1 && is.na(contents))) {    
        stopifnot(is.character(contents) && all(!is.na(contents)))
        contents <- unique(contents)        
        contents <- paste(contents, collapse=',')
    }
    index <- match(id, sets$ID)
    if (is.na(index)) {
        sets[nrow(sets)+1,] <- c(id, contents)
    } else {
        sets$Members[index] <- contents
    }
    return(sets)
}

### Array #########################################################
ARRAY.INDEX.FILE.PSEUDO.PORTNAME.PREFIX <- '_index_'
ARRAY.INDEX.FILE.KEY.COL <- "Key"
ARRAY.INDEX.FILE.FILENAME.COL <- "File"

Array.new <- function(keys=NULL, files=NULL) {
    stopifnot(length(keys) == length(files) || length(keys) == 0)
    stopifnot(all(!duplicated(keys)))
    array <- data.frame()
    array[[ARRAY.INDEX.FILE.KEY.COL]] <- character(0)
    array[[ARRAY.INDEX.FILE.FILENAME.COL]] <- character(0)

    if(length(files) > 0) {
        for(i in 1:length(files)) {
            if(is.null(keys)) {
                key <- as.character(i)
            }else {
                key <- keys[i]
            }
            array <- Array.add(array, key, files[i])
        }
    }
    return(array)
}

Array.size <- function(array) {
    stopifnot(is.data.frame(array))
    nrow(array)
}

Array.add <- function(array, key, file) {
    stopifnot(is.data.frame(array))
    stopifnot(is.character(key))
    stopifnot(is.character(file)) 
    index <- match(key, array[,ARRAY.INDEX.FILE.KEY.COL])
    if(is.na(index)) {
        array[nrow(array)+1, c(ARRAY.INDEX.FILE.KEY.COL, ARRAY.INDEX.FILE.FILENAME.COL)] <- c(key, file)
    } else {
        array[[ARRAY.INDEX.FILE.FILENAME.COL]][index] <- file
    }
    return(array)
}

Array.getKey <- function(array, i) {
    stopifnot(is.data.frame(array))
    stopifnot(i > 0, i <= Array.size(array))    
    array[i, ARRAY.INDEX.FILE.KEY.COL]
}

Array.getFile <- function(array, key) {
    stopifnot(is.data.frame(array))
    stopifnot(is.character(key) | is.numeric(key))
    if(is.numeric(key)) {
        stopifnot(key > 0, key <= Array.size(array))    
        array[key, ARRAY.INDEX.FILE.FILENAME.COL]
    }else {
        index <- match(key, array[,ARRAY.INDEX.FILE.KEY.COL])
        if(is.na(index)) {
            return(NULL)
        } else {
            return(array[[ARRAY.INDEX.FILE.FILENAME.COL]][index])
        }
    }
}

Array.getFiles <- function(array) {
    stopifnot(is.data.frame(array))
    return(array[[ARRAY.INDEX.FILE.FILENAME.COL]])
}

Array.remove <- function(array, key) {
    stopifnot(is.data.frame(array))
    stopifnot(is.character(key))
    index <- match(key, array[,ARRAY.INDEX.FILE.KEY.COL])
    if(is.na(index)) {
        return(array)
    } else {
        return(array[-index,])
    }
}

Array.read <- function(command.file, array.port.name) {
    stopifnot(is.character(array.port.name))
    expected.cols <- c(ARRAY.INDEX.FILE.KEY.COL, ARRAY.INDEX.FILE.FILENAME.COL)
    array <- CSV.read(get.input.array.index(command.file, array.port.name), expected.cols)
    # Make sure the columns are of type character.
    array[,ARRAY.INDEX.FILE.KEY.COL]      <- as.character(array[,ARRAY.INDEX.FILE.KEY.COL])
    array[,ARRAY.INDEX.FILE.FILENAME.COL] <- as.character(array[,ARRAY.INDEX.FILE.FILENAME.COL])
    array.dir <- get.input(command.file, array.port.name)
    
    if(nrow(array) != 0) {
        for(i in 1:nrow(array)) {
            filename <- array[i, ARRAY.INDEX.FILE.FILENAME.COL]
            # Make relative file paths absolute.
            if(length(grep("^/|^\\w:\\\\", filename)) == 0) {
                filename <- file.path(array.dir, filename)
            }
            array[i, ARRAY.INDEX.FILE.FILENAME.COL] <- filename
        }
    }
    return(array)
}

Array.write <- function(command.file, array, array.port.name) {
    stopifnot(is.data.frame(array))
    stopifnot(is.character(array.port.name))
    array.dir <- get.output(command.file, array.port.name)
    
    # Check that the files exist.
    if(Array.size(array) != 0) {
        for(i in 1:Array.size(array)) {
            filename <- array[i, ARRAY.INDEX.FILE.FILENAME.COL]
            if(!file.exists(filename)) {
                if(!file.exists(paste(array.dir, filename, sep="/"))) {
                    write.error(command.file, sprintf("File '%s' in the array index file does not exist.", filename))
                }
            }
        }
    }

    # Create array output dir.
    array.out.dir <- get.output(command.file, array.port.name)
    if (!file.exists(array.out.dir)) {
        dir.create(array.out.dir, recursive=TRUE)
    }
    
    CSV.write(get.output.array.index(command.file, array.port.name), array)
}
