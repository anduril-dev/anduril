### Utility functions ##################################################

trim <- function(string, trim.chars=' \t\n\r') {
    stopifnot(!is.null(string))
    string <- sub(paste('^[', trim.chars, ']+', sep=''), '', string)
    string <- sub(paste('[', trim.chars, ']+$', sep='') , '', string)
    return(string)
}

split.trim <- function(string, split, ...) {
    stopifnot(!is.null(string))
    stopifnot(!is.null(split))
    return(trim(unlist(strsplit(string, split, ...))))
}

# Recursively copy source.directory to dest.directory.
copy.dir <- function(source.directory, dest.directory, allow.duplicates=FALSE, exclude.names=character()) {
    if (!file.exists(dest.directory)) dir.create(dest.directory, recursive=TRUE)
    
    for (filename in list.files(source.directory)) {
        if (filename %in% exclude.names) next
        
        full.source <- file.path(source.directory, filename)
        full.dest <- file.path(dest.directory, filename)
        
        if (file.exists(full.source) && file.info(full.source)$isdir) {
            copy.dir(full.source, full.dest, allow.duplicates, exclude.names)
        }
        else {
            if (!allow.duplicates && file.exists(full.dest)) {
                stop(sprintf('Duplicate file: %s', filename))
            }
            file.copy(full.source, full.dest)
        }
    }
}

textfile.read <- function(filename) {
    return(paste(readLines(filename, warn=FALSE), collapse='\n'))
}

textfile.write <- function(filename, string, append=FALSE) {
    outfile <- file(filename, 'wb')
    last <- substr(string, nchar(string), nchar(string))
    if (nchar(string) > 0 && !(last %in% c('\n', '\r'))) string <- paste(string, '\n', sep='')
    cat(string, file=outfile, append=append)
    close(outfile)
}

touch.output <- function(cf, output.name) {
    stopifnot(is.command.file(cf))
    f <- file(get.output(cf, output.name), 'w')
    close(f)
}

# Create a hashed lookup environment of type value -> index.
# Input:
# - values: sequence of strings.
create.lookup <- function(values) {
    stopifnot(is.character(values))
    values <- unlist(values)
    lookup <- new.env(hash=TRUE)
    if (length(values) == 0) return(lookup)
    
    for (i in 1:length(values)) {
        if (nchar(values[[i]]) > 0) lookup[[ values[[i]] ]] <- i
    }
    return(lookup)	
}

### Graphics ###########################################################

# Open a PNG device. Use this instead of a raw call to png() since png()
# doesn't work without GUI.
png.open <- function(filename, width=800, height=600) {
    stopifnot(is.character(filename))
    stopifnot(length(filename) == 1)
    
    stopifnot(is.numeric(width))
    stopifnot(length(width) == 1)
    stopifnot(width >= 0)
    
    stopifnot(is.numeric(height))
    stopifnot(length(height) == 1)
    stopifnot(height >= 0)
    
    # Don't warn if png is not succesful since we try bitmap after that.
    prev.warn <- getOption('warn')
    options(warn=-1)  
    result <- try(png(filename, width=width, height=height), silent=TRUE)
    options(warn=prev.warn)
    
    if (class(result) == 'try-error') {
        resolution <- 100
        bitmap(filename, type='png16m', width=width/resolution, height=height/resolution, res=resolution)
    }
}

png.close <- function() {
    dev.off()
}
