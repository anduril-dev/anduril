get.input <- function(command.file, input.name) {
    if (!is.command.file(command.file)) stop('command.file is not a command file object')
    stopifnot(is.character(input.name))
    
    return(command.file$input[[input.name]])
}

input.defined <- function(command.file, input.name) {
    if (!is.command.file(command.file)) stop('command.file is not a command file object')
    stopifnot(is.character(input.name))
    
    value <- command.file$input[[input.name]]
    return(!is.null(value) && nchar(value) > 0)
}

get.output <- function(command.file, output.name) {
    if (!is.command.file(command.file)) stop('command.file is not a command file object')
    stopifnot(is.character(output.name))
    
    return(command.file$output[[output.name]])
}

get.parameter <- function(command.file, param.name, type='string', allowed.values=NULL) {
    if (!is.command.file(command.file)) stop('command.file is not a command file object')
    stopifnot(is.character(param.name))
    
    value <- command.file$params[[param.name]]
    if (is.null(value)) return(value)
    
    if(!is.null(allowed.values)) {
        if(!value %in% allowed.values) {
            stop(sprintf("%s not in (\"%s\")", value, paste(allowed.values, collapse="\", \"")))
        }
    }

    if (type == 'string') return(value)
    else if (type == 'boolean') return(as.logical(value))
    else if (type == 'float') return(as.numeric(value))
    else if (type == 'int') return(as.integer(value))
    else stop(sprintf('Invalid "type" argument: %s', type))
}

get.metadata <- function(command.file, metadata.name) {
    if (!is.command.file(command.file)) stop('command.file is not a command file object')
    stopifnot(is.character(metadata.name))
    return (command.file$metadata[[metadata.name]])
}

get.input.array.index <- function(command.file, array.port.name) {
    if (!input.defined(command.file, array.port.name)) stop(paste('Input', array.port.name, 'is not defined.', sep=" "))
    stopifnot(is.character(array.port.name))
    array.index.port.name <- paste(ARRAY.INDEX.FILE.PSEUDO.PORTNAME.PREFIX, array.port.name, sep="")
    if(!exists(array.index.port.name, envir=command.file$input))
        stop(sprintf('No array index input \'%s\' for input port \'%s\'.', array.index.port.name, array.port.name))
    return(command.file$input[[array.index.port.name]])
}

get.output.array.index <- function(command.file, array.port.name) {
    if (!is.command.file(command.file)) stop('command.file is not a command file object')
    stopifnot(is.character(array.port.name))
    array.index.port.name <- paste(ARRAY.INDEX.FILE.PSEUDO.PORTNAME.PREFIX, array.port.name, sep="")
    if(!exists(array.index.port.name, envir=command.file$output))
        stop(sprintf('No array index output \'%s\' for output port \'%s\'.', array.index.port.name, array.port.name))
    return(command.file$output[[array.index.port.name]])
}

### Delegates ##########################################################

delegate.defined <- function(command.file, delegate.name) {
    return (!is.null(get.delegate(command.file, delegate.name)))
}

get.delegate.input <- function(command.file, delegate.name, input.name) {
    stopifnot(is.character(delegate.name))
    stopifnot(is.character(input.name))
    
    dele <- get.delegate(command.file, delegate.name)
    if (is.null(dele)) return(NULL)
    else return(dele$input[[input.name]])
}

get.delegate.output <- function(command.file, delegate.name, output.name) {
    stopifnot(is.character(delegate.name))
    stopifnot(is.character(output.name))
    
    dele <- get.delegate(command.file, delegate.name)
    if (is.null(dele)) return(NULL)
    else return(dele$output[[output.name]])
}

write.delegate.parameter <- function(command.file, delegate.name, param.name, value) {
    filename <- get.delegate.parameters(command.file, delegate.name)
    if (is.null(filename)) stop(sprintf('Parameters file for delegate %s is not defined', delegate.name))
    x <- sprintf('parameter.%s=%s\n', param.name, value)
    cat(x, file=filename, append=TRUE)
}

launch.delegate <- function(command.file, delegate.name) {
    launcher <- get.delegate.launcher(command.file, delegate.name)
    if (is.null(launcher)) return(BAD.DELEGATE)
    status <- system(sprintf('bash "%s"', launcher))
    return(status)
}

write.error <- function(command.file, message) {
    .componentSkeleton.has.errors <<- TRUE
    cat(paste(message, '\n', sep=''), file=get.error.file(command.file), append=TRUE)
}

write.log <- function(command.file, message) {
    cat(paste(message, '\n', sep=''), file=get.log.file(command.file), append=TRUE)
}

has.errors <- function() {
    return(.componentSkeleton.has.errors)
}

### Helpers ############################################################

# Delegate structure is a list with the following fields:
# - launcher: filename of launcher shell script
# - parameters: filename of delegate component parameters file
# - input: environment portname -> filename
# - output: environment portname -> filename

.new.delegate <- function() {
    return(list(
        launcher=NULL,
        parameters=NULL,
        input=new.env(hash=TRUE),
        output=new.env(hash=TRUE)))
}

is.command.file <- function(command.file) {
    return(is.list(command.file)
        && is.environment(command.file$input)
        && is.environment(command.file$output)
        && is.environment(command.file$params)
        && is.environment(command.file$metadata))
}

parse.command.file <- function(filename) {
    input <- new.env(hash=TRUE)
    output <- new.env(hash=TRUE)
    params <- new.env(hash=TRUE)
    metadata <- new.env(hash=TRUE)
    delegates <- new.env(hash=TRUE)
    
    for (line.str in readLines(filename, warn=FALSE)) {
        line.str <- trim(line.str)
        if (nchar(line.str) == 0) next
        
        sep <- regexpr('=', line.str, fixed=TRUE)[[1]]
        if (sep < 0) stop(sprintf('Command file: invalid line: %s', line.str))
        key <- trim(substr(line.str, 1, sep-1))
        value <- trim(substr(line.str, sep+1, nchar(line.str)))
        
        key.tokens <- strsplit(key, '.', fixed=TRUE)[[1]]
        if (length(key.tokens) < 2) stop(sprintf('Invalid key: %s', key))
        
        key.type <- key.tokens[[1]]
        key.name <- key.tokens[[2]]
        if (key.type == 'input') input[[key.name]] <- value
        else if (key.type == 'output') {
#            if (key.name == '_tempdir') {
#                if (file.exists(value) == FALSE) dir.create(value)
#            }
            output[[key.name]] <- value
        }
        else if (key.type == 'parameter') {
            # Converting strings with escape characters into R format.
            value <- gsub("\\\\","DouBlEWTFqUOt3S",value,fixed=TRUE)
            value <- gsub("^\\\\ "," ",value)
            value <- gsub("\\n","\n",value,fixed=TRUE)
            value <- gsub("\\r","\r",value,fixed=TRUE)
            value <- gsub("\\t","\t",value,fixed=TRUE)
            #value <- gsub("\\\\\\\\","\\",value,fixed=TRUE)
            #value <- gsub("\\\"","\\\"",value,fixed=TRUE)
                        
            # Filtering out escape syntax from java.utils.Properties.store().
            value <- gsub("\\\\#","#",value)
            value <- gsub("\\\\!","!",value)
            value <- gsub("\\\\=","=",value)
            value <- gsub("\\\\:",":",value)
            
            value <- gsub("DouBlEWTFqUOt3S","\\",value, fixed=TRUE)
            
            params[[key.name]] <- value
        }
        else if (key.type == 'metadata') metadata[[key.name]] <- value
        else if (key.type == 'delegate') {
            dele <- delegates[[key.name]]
            if (is.null(dele)) dele <- .new.delegate()
            
            sub.type <- key.tokens[[3]]
            sub.name <- NULL
            if (length(key.tokens) == 4) sub.name <- key.tokens[[4]]
            
            if (sub.type == 'launcher') dele$launcher <- value
            else if (sub.type == 'parameters') dele$parameters <- value
            else if (sub.type == 'input') dele$input[[sub.name]] <- value
            else if (sub.type == 'output') dele$output[[sub.name]] <- value
            else stop(sprintf('Invalid delegate key: %s', key))
            
            delegates[[key.name]] <- dele
        }
        else stop(sprintf('Invalid key: %s', key))
    }
    
    return(list(input=input, output=output, metadata=metadata, params=params, delegates=delegates))
}

get.error.file <- function(command.file) {
    return(command.file$output[[ERRORS.FILE]])
}

get.log.file <- function(command.file) {
    return(command.file$output[[LOG.FILE]])
}

get.temp.dir <- function(command.file) {
    value <- command.file$output[[TEMP.DIR]]
    if (file.exists(value) == FALSE) dir.create(value)
    return(command.file$output[[TEMP.DIR]])
}    

get.delegate <- function(command.file, delegate.name) {
    return(command.file$delegates[[delegate.name]])
}

get.delegate.parameters <- function(command.file, delegate.name) {
    dele <- get.delegate(command.file, delegate.name)
    if (is.null(dele)) return(NULL)
    else return(dele$parameters)
}

get.delegate.launcher <- function(command.file, delegate.name) {
    dele <- get.delegate(command.file, delegate.name)
    if (is.null(dele)) return(NULL)
    else return(dele$launcher)
}
