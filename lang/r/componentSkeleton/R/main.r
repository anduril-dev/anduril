.command.file <<- NULL # Set by main, used by error.handler

exit <- function(status) {
    quit(save='no', status=status, runLast=FALSE)
}

error.handler <- function() {
    dump.frames('frames')
    
    write.error(.command.file, geterrmessage())
    
    cat('Stack trace (oldest to newest)\n')
    for (i in 1:(length(frames)-1)) {
        env.name <- names(frames)[i]
        cat(sprintf('\nEnvironment %s\n', env.name))
        
        if (substr(env.name, 1, 5) == 'stop(') break
        if (substr(env.name, 1, 10) == 'stopifnot(') break
         
        try(print(ls.str(frames[[i]])))
    }
    
    exit(UNCAUGHT_EXCEPTION)
}

main <- function(execute.func) {
    argv <- commandArgs(TRUE)
    if (length(argv) == 0) exit(NO_COMMAND_FILE)
    cf <- try(parse.command.file(argv[[1]]), TRUE)
    if (class(cf) == 'try-error') exit(COMMAND_FILE_IO_ERROR)
    
    if (!is.function(execute.func)) {
        write.error(cf, 'main: execute.func is not a function')
        exit(1)
    }
    
    .componentSkeleton.has.errors <<- FALSE
    .command.file <<- cf
    
    options(error=error.handler)
    result <- execute.func(cf)
    # Print imported packages and their versions    
    session.info<-sessionInfo()
    session.packages<-c()
    for (session.package in session.info$otherPkgs) {
        session.packages=append(session.packages, paste(session.package$Package, 
                                       session.package$Version,sep=": v"))
    }
    write.log(cf, paste(
        session.info$R.version$version.string,
        session.info$platform,
        session.info$running,
        sep=", "))
    if (length(session.packages)>0) {
        write.log(cf, paste(session.packages,sep="", collapse=", "))
    }
    
    if (is.numeric(result)) exit(result)
    else exit(0)
}
