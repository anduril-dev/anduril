CSV.read <- function(filename, expected.columns=NULL) {
    stopifnot(is.character(filename))
    stopifnot(length(filename) == 1)
    stopifnot(is.null(expected.columns) || is.character(expected.columns))
    
    csv <- read.table(filename, sep='\t', header=TRUE, check.names=FALSE,
        stringsAsFactors=FALSE, row.names=NULL, quote='"')  

    if (!is.null(expected.columns)) {
        actual <- colnames(csv)
        if (is.null(actual) || !identical(actual, expected.columns)) {
            stop(sprintf('Invalid column names in %s: expected %s, got %s',
                filename, paste(expected.columns, collapse=', '), paste(actual, collapse=', ')))
        }
    }
    
    return(csv)
}

CSV.read.first.cell <- function(filename) {
    return(scan(filename, what=character(), n=1, quiet=TRUE))
}

CSV.write <- function(filename, csv, append=FALSE, first.cell='RowName') {
    stopifnot(is.character(filename))
    stopifnot(length(filename) == 1)
    
    if (append) file.mode <- 'ab'
    else file.mode <- 'wb'
    outfile <- file(path.expand(filename), file.mode)
    
    if (ncol(csv) > 0) {
        if (is.matrix(csv) && !is.null(rownames(csv))) {
            csv <- data.frame(RowName=rownames(csv), csv)
            colnames(csv)[1] <- first.cell
        }
        write.table(csv, outfile, sep='\t', eol='\n', row.names=FALSE, col.names=TRUE)
    }
    
    close(outfile)
}

CSV.read.large <- function (filename, expected.columns = NULL) 
{
  require("sqldf")
  stopifnot(is.character(filename))
  stopifnot(length(filename) == 1)
  stopifnot(is.null(expected.columns) || is.character(expected.columns))
  csv <- read.csv.sql(filename, header = FALSE, sep="\t", skip = 1,
                      filter = list('gawk -f prog', prog= '{ gsub(/"/, "");gsub("\tNA\t", "\t-9999.5555\t"); print}'))
  header = scan(filename, nmax=ncol(csv), what=character(), sep="\t")
  colnames(csv) <- header
  csv[csv==-9999.5555]<- NA
  if (!is.null(expected.columns)) {
    actual <- colnames(csv)
    if (is.null(actual) || !identical(actual, expected.columns)) {
      stop(sprintf("Invalid column names in %s: expected %s, got %s", 
                   filename, paste(expected.columns, collapse = ", "), 
                   paste(actual, collapse = ", ")))
    }
  }
  return(csv)
}
### Matrix #############################################################

Matrix.read <- function(filename, expected.columns=NULL) {
    matr <- CSV.read(filename, expected.columns=expected.columns)
    if (nrow(matr) == 0) return(matr)
    names.row <- matr[,1]
    matr <- as.matrix(matr[,2:ncol(matr),drop=FALSE])
    if (!is.numeric(matr)) {
        stop(sprintf('Matrix is not numeric: %s', filename))
    }
    rownames(matr) <- names.row
    return(matr)
}

### LogMatrix ##########################################################

LOG.MATRIX.LIMIT <- 100

LogMatrix.read <- function(filename) {
    stopifnot(is.character(filename))
    stopifnot(length(filename) == 1)

    expr <- Matrix.read(filename)

    if (nrow(expr) == 0) return(expr)
    if (max(expr, na.rm=TRUE) > LOG.MATRIX.LIMIT) {
        stop(sprintf('LogMatrix.read: %s has too large numeric values (max: %s)',
            filename, max(expr, na.rm=TRUE)))
    }
    if (min(expr, na.rm=TRUE) < -LOG.MATRIX.LIMIT) {
        stop(sprintf('LogMatrix.read: %s has too small numeric values (min: %s)',
            filename, min(expr, na.rm=TRUE)))
    }
    
    dup <- duplicated(rownames(expr))
    if (any(dup)) {
        stop(sprintf('LogMatrix (%s) contains duplicate row names: %s', filename, rownames(expr)[dup])) 
    }
    
    return(expr)
}

### AnnotationTable ####################################################

AnnotationTable.read <- function(filename) {
    # TODO: check uniqueness
    return(CSV.read(filename))
}

AnnotationTable.get.vector <- function(csv, column.name, key.values=NULL) {
    stopifnot(is.data.frame(csv))
    stopifnot(is.character(column.name))
    stopifnot(length(column.name) == 1)
    
    if (!(column.name %in% colnames(csv))) {
        stop(sprintf("Table doesn't have column %s", column.name))
    }
    
    values <- csv[[column.name]]
    names(values) <- csv[,1]
    if (is.null(key.values)) return(values)
    else return(values[key.values])
}
