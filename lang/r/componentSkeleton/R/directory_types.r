dirtype.get.files <- function(directory, full.names=TRUE, only.numeric=FALSE) {
    if (only.numeric) pattern <- '[0-9]+'
    else pattern <- NULL
    return(list.files(directory, pattern=pattern, full.names=full.names))
}

### Numeric file names #################################################

dirtype.get.file <- function(directory, index) {
    stopifnot(index >= 0)
    return(file.path(directory, as.character(index)))
}

dirtype.get.indices <- function(directory) {
    filenames <- list.files(directory, pattern='[0-9]+')
    return(as.integer(filenames))
}

dirtype.get.max.index <- function(directory) {
    indices <- dirtype.get.indices(directory)
    if (length(indices) == 0) return(0)
    else(return(max(indices))) 	
}

dirtype.get.next.file <- function(directory) {
    return(dirtype.get.file(directory, dirtype.get.max.index(directory)+1))	
}
