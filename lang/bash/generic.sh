##  Non-Anduril, generic functions
# These functions may come handy, even in your interactive shell

function calc {
# Simple calculator using bc.
# Second argument is the scale (number of decimals)
#  - 0 for integer
#  - defaults to 4
# Usage: calc "(4+5)/4"
# Usage: calc "4%5" 0
# Usage: calc 'b=2; for (i=1;i<=5;i++) { (b+b)^b-i }'

    local scale=4
    [[ -z "$2" ]] || scale=$2
    echo -e 'scale='$scale';'"$1" | bc -l
}

function csvcoln {
# Gets a column number from a csv file header
# If column not found, echos 0
# Usage: coln=$( csvcoln path/to/file.csv "ColName" )
# (to extract that column: cut -f $coln path/to/file.csv)

    local c
    local IFS
    c=1
    IFS=$'\t'
    for col in $( head -n 1 "$1" | tr -d '\n\r' )
    do if (( $( echo ${col//\"/} | grep -c ^${2}$ ) ))
       then echo $c
            return 0
       fi
       c=$(( $c + 1 ))
    done
    echo 0
    echo "Column $2 not found" 1>&2
    return 1
}

function filesize {
# Return a human readable size from integer of bytes
# Usage:  filesize 10000

    [ "$1"  = "0" ] && {
        echo "0 B"
        return 0
    }

    awk 'BEGIN{ x = '$1'
        split("B KB MB GB TB PB",type)
        for(i=5;y < 1;i--)
            y = x / (2^(10*i))
        str=int(y*10)/10 " " type[i+2] 
        if (x==0) { str = "0 B" }
        print str
    }' || return $?
}

function iscmd {
# Checks whether an executable is available, 
# if not, prints error message, and returns 1
# Usage: iscmd wget || exit 1

    which "$1" > /dev/null || {
        echo Command: $1 not found >&2
        return 1
    }
}

function stringtomap {
# Exports a record converted in to a string.
# The reverse function of std.recordToString
# Usage: stringtomap "key1=value1,key2=value2";  echo $key1

    local IFS
    local i
    IFS=',' read -ra PAIR <<< "$1"
    for i in "${PAIR[@]}"; do
        eval export \"$i\"
    done
}
