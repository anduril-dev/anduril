#!/bin/bash


# Documentation generation follows this simple pattern:
# functions start the line with string "function".
# documentation follows it with lines starting with "#".
# One line must be empty after documentation.
# "Usage:" row must be a single row.
# Each function file must start with a ## NAME

if [ $# -ne 2 ]
then echo "Give the path to functions.bash and the path to write HTML"
    exit 0
fi


IFS=$'\n'
filelist=( $( ls -1 "$1" | grep -v generate_docs | grep -v bash_tests ) )
mkdir -p "$2"
touch "$2"
echo '<html>
<head>
<style>
PRE { background-color: lightgray; 
      padding: 2px; }
#toc {
    position: fixed;
    padding: 10px;
    right: 0px;
    top: 0px;
    background-color: white;
    border: solid black 1px;
}
</style>
<script language="javascript">

function link(id, text) {
    var el=document.createElement("a");
    el.setAttribute("href", "#f" + id);
    el.setAttribute("id", "l"+id);
    el.onclick=function(){ lightUp(id); };
    el.innerHTML=text;
    return el;
}
function lightUp(id) {
    for (var i=0; i<document.body.childNodes.length; ++i) {
        var el=document.body.childNodes[i];
        if ((el.nodeName.toUpperCase() == "H3" ) || (el.nodeName.toUpperCase() == "H2" )) {
            el.style.backgroundColor = "transparent";
        }
    }
    el=document.getElementById("f"+id);
    el.style.backgroundColor = "yellow";
}
function TOC() {
    toc=document.getElementById("toc");
    for (var i=0; i<document.body.childNodes.length; ++i) {
        var el=document.body.childNodes[i];
        if (el.nodeName.toUpperCase() == "H3") {
            el.setAttribute("id", "f"+i);
            toc.appendChild(document.createElement("div")).appendChild(link(i, el.innerHTML));
        } else if (el.nodeName.toUpperCase() == "H2") {
            el.setAttribute("id", "f"+i);
            var name=el.innerHTML.replace(/:.*/,"").trim().toUpperCase();
            toc.appendChild(lel=document.createElement("div")).appendChild(link(i, name));
            lel.style.fontWeight="bold";
        }
    }
}

</script>
</head>
<body onload="TOC();"><div id="toc"></div>' > "$2/index.html"
echo '<h1>Documentation for Anduril Bash API functions</h1>' >> "$2/index.html"
date "+<pre>%c</pre>" >> "$2/index.html"
for (( i=0; i<${#filelist[@]}; i++ ))
do
    if [ -d "$1/${filelist[$i]}" ]
    then
        continue
    fi
    if [ -f "$1/${filelist[$i]}" ]
    then
        # Header of file
        awk '/^##[^_]/,/^$/ { print }' "$1/${filelist[$i]}" | \
          sed -e 's,^##\(.*\),<hr/><h2>'${filelist[$i]}': \1</h2>,' \
              -e 's,^#\(.*\),<p>\1</p>,' >> "$2/index.html"
        # function documentation
        awk '/^function [^_]/,/^$/ { print }' "$1/${filelist[$i]}" | \
        sed -e 's,^function,<hr/><h3>,' -e 's,{$,</h3><ul>,' \
        -e 's,^#,<br>,' -e 's,^$,</ul>,' -e 's,\(Usage:\)\(.*\),\1<pre>\2</pre>,' \
        >> "$2/index.html"
    fi
done
date "+<div>Generated: %c</div>" >> "$2/index.html"
echo '</body></html>' >> "$2/index.html"
