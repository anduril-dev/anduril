## Anduril related functions.
# These functions make it easy to create BASH components in the Anduril framework.

SELFDIR="$( dirname "${BASH_SOURCE[0]}" )"
# import generic functions
. "$SELFDIR"/generic.sh
. "$SELFDIR"/tsvkit.sh

# When sourced with no arguments, will not try to solve log files
[[ -z "$1" ]] || {
[[ -a "$1" ]] && { 
    # Write to log:  echo "Hello World!" >> "$logfile"
    logfile="$( grep ^output._log= "$1" | sed s,^output._log=,, )"
    # Write to error log:  echo "Error!" >> "$errorfile"
    errorfile="$( grep ^output._errors= "$1" | sed s,^output._errors=,, )"
    # save the command file location for internal use
    commandfile="$1"
} || echo _command file not available
}

function addarray {
# Writes an entry in the array index
# Usage: addarray outputportname key file

    local portpath="$( getoutput "$1" )"
    local portindex="$( getoutput "_index_"${1} )"
    if [ -z "$portpath" ]
    then echo "Port $1 is not a valid output port" >&2
         return 1
    fi
    mkdir -p "$portpath"
    if [ ! -f "$portindex" ]
    then printf '"Key"\t"File"\n' > "${portindex}"
    fi
    printf '"%s"\t"%s"\n' "$2" "$3" >> "${portindex}"
    return 0
}

function createarrayindex {
# Appends index in the array output port with keys matching 
# the basenames of th files
# Usage: createarrayindex outputportname

    local  IFS
    local portpath="$( getoutput "$1" )"
    local portindex="$( getoutput "_index_""${1}" )"
    if [ ! -f "$portindex" ]
    then printf '"Key"\t"File"\n' > "${portindex}"
    fi
    IFS=$'\n'
    find "$portpath" -mindepth 1 -maxdepth 1 -printf '%P\t%P\n' | sort | grep -v ^_index >> "${portindex}" || return 0
    return 0
}

function export_command {
# Exports the _command file as environment variables.
# Ports and parameters are prefixed with their type, 
# i.e. input_ output_ parameter_ metadata_
# Usage: export_command

    local type
    local name
    local value
    local niceName
    while read line
    do 
        type="${line%%.*}" # remove all characters after dot (long match)
        name="${line%%=*}" # remove all characters after the first =
        name="${name#*.}" # remove all characters before .
        niceName="${name//./_}" # remove dots from name
        value="$( get${type} ${name} )"
        eval 'export ${type}_${niceName}="${value}"'
    done < "$commandfile"
    
}

function exportarraykeys {
# Exports array values into variables named after the corresponding key
# The only parameter is an array input
# all non a-zA-Z0-9_ characters are replaced with _.
# If the key starts with a number, it is prefixed with "key"
# Usage: exportarraykeys inputportname

    OLDIFS="$IFS"
    local k validkey
    local portindex="$( getinput "_index_""${1}" )"
    IFS=$'\n'
    if [ -f "$portindex" ]; then
        local myFunkyKeys=( $( getarraykeys "$1" ) )
        local myFunkyFiles=( $(getarrayfiles "$1" ) )
        for (( k=0;k<${#myFunkyKeys[@]};k++)); do
            validkey="$( echo -n "${myFunkyKeys[$k]}" | tr -c '[a-zA-Z0-9_]' _ | sed -e 's,^\([0-9]\),key\1,' )"
            export $validkey="${myFunkyFiles[$k]}"
        done
    fi
    IFS="$OLDIFS"
}

function forarray {
# Run a script for each file in an array, writing the output to
# another array. This function replaces the Anduril function
# ArrayBashEvaluate. The function uses BashEvaluate input and output 
# array ports named "array1" and "arrayOut1", unless arguments inArray 
# and outArray are set. If outArray is a plain folder output port, no index is generated.
# The first argument is the script sourced for each array file. Its stdout
#   is used for the content of the output array file. Refer to the
#   key and file in the loop with "$key" and "$file". Running index: $i
# The second (optional) argument states how the output filenames 
#   are generated: either "keys" or "files". Defaults to "keys".
#   The "files" mode may result in non-unique file names!
# Usage: forarray scriptfile [keys] [inArray] [outArray]

    local oldIFS="$IFS"
    local inArray="$3"
    local outArray="$4"
    IFS=$'\n'
    [ -z "$3" ] && inArray="array1"
    [ -z "$4" ] && outArray="arrayOut1"
    local arraykeys=( $( getarraykeys "$inArray" ) )
    local arrayfiles=( $( getarrayfiles "$inArray" ) )
    local i
    local key
    local file
    local fileout
    local dirout="$( getoutput $outArray )"
    local filemode="keys"
    [ "$2" = "files" ] && filemode="files"
    for (( i=0; i<${#arraykeys[@]};i++ ))
    do key="${arraykeys[$i]}"
       file="${arrayfiles[$i]}"
       [ $filemode = "keys" ] && {
           fileout="$key"
       } || {
           fileout="$( basename "${arrayfiles[$i]}" )"
       }
       . "$1" > "$dirout"/"$fileout"
       [ -n "$( getoutput "_index_$outArray" )" ] && addarray "$outArray" "$key" "$fileout"
    done
    IFS="$oldIFS"
}

function getarrayfiles {
# Return a newline separated list of array files
# Usage: files=( $( getarrayfiles inputportname ) )

    portpath="$( getinput "$1" )"
    portindex="$( getinput "_index_""${1}" )"
    if [ ! -f "$portindex" ]
    then echo "No array index found" 1>&2
        return 1
    fi
    keyf=$( csvcoln "$portindex" File )
    if (( $? ))
    then echo "No File column found" 1>&2
         return 1
    fi
    IFS=$'\n'
    files=( $( cut -f "$keyf" "$portindex" | tail -n +2 | sed -e 's,\r,,' -e 's,^",,' -e 's,"$,,' ) )
    # returns the absolute path to file, even if File content is relative
    for (( i=0;  i<${#files[@]};i++ ))
    do if [ -e "${portpath}/${files[$i]}" ]
       then echo "${portpath}/${files[$i]}"
       else echo "${files[$i]}"
       fi
    done
    return 0
}

function getarrayfile {
# Return a file for a key
# Usage: file=$( getarrayfile inputportname keyname )

    local i keys
    local files
    local IFS=$'\n'
    keys=( $( getarraykeys $1 ) )
    files=( $( getarrayfiles $1 ) )
    for (( i=0; i<${#keys[@]}; i++ )); do
       [[ "${keys[$i]}" = "$2" ]] && { 
           echo "${files[$i]}"
           return 0
        }
    done
    echo "No key $2 found" 1>&2
    return 1
}

function getarraykeys {
# Returns a newline separated list of array keys
# Usage: keys=( $( getarraykeys inputportname ) )

    local portpath="$( getinput "$1" )"
    local portindex="$( getinput "_index_""${1}" )"
    if [ ! -f "$portindex" ]
    then echo "No array index found in \"$portindex\"" 1>&2
        return 1
    fi
    local keyc="$( csvcoln "$portindex" Key )"
    if (( $? ))
    then echo "No Key column found" 1>&2
         return 1
    fi
    cut -f "$keyc" "$portindex" | tail -n +2 | sed -e 's,\r,,' -e 's,^",,' -e 's,"$,,'  
    return 0
}

function getarraykeyindex {
# Gets an index number for a specific key
# Usage: index=( $( getarraykeyindex inputportname key ) )

    local portindex="$( getinput "_index_""${1}" )"
    if [ ! -f "$portindex" ]
    then echo "No array index found in \"$portindex\"" 1>&2
        return 1
    fi
    local keyc="$( csvcoln "$portindex" Key )"
    if (( $? ))
    then echo "No Key column found" 1>&2
         return 1
    fi
    local value="$( cut -f $keyc "$portindex" | tail -n +2 | grep -n ^"${2}"$ | head -n 1 | sed 's,^\([0-9]*\)[:].*,\1,' )"
    echo $(( $value - 1 ))
    return 0
}

function getbundlepath {
# Returns bundle path, if found in ANDURIL_BUNDLES
# Usage: tools_path=$( getbundlepath tools )

    [[ -z "$ANDURIL_HOME" ]] && {
        echo "No ANDURIL_HOME set" 1>&2
        return 1
    }
    [[ -z "$ANDURIL_BUNDLES" ]] && {
        ANDURIL_BUNDLES="$ANDURIL_HOME"/bundles
    }
    PYTHONPATH="$ANDURIL_HOME/lang/python:$PYTHONPATH" python <( echo '
import anduril.bundlerepository,sys
repo=anduril.bundlerepository.Repository.from_env()
for b in repo.get_bundle_names():
    if sys.argv[1]==b:
        print(repo.get_bundle(b).get_path())
        sys.exit(0)
sys.exit(1)
' ) "$1" || { echo "Bundle $1 not found" 1>&2;false; }
}


function getinput {
# Returns input port from the _command file.
# Usage: var=$( getinput input_name )

    grep ^input."$1"= "$commandfile" | sed s,^input."$1"=,, 
}

function getmetadata {
# Returns metadata from the _command file.
# Usage: var=$( getmetadata "metadata_name" )

    grep ^metadata."$1"= "$commandfile" | sed s,^metadata."$1"=,,
}

function getoutput {
# Returns output port from the _command file.
# Usage: var=$( getoutput output_name )

    grep ^output."$1"= "$commandfile" | sed s,^output."$1"=,, 
}

function getparameter {
# Returns parameter from the _command file.
# Usage: var=$( getparameter "parameter_name" )

    param="$( grep ^parameter."$1"= "$commandfile" | sed s,^parameter."$1"=,, | \
    sed -e 's,\\#,#,g' -e 's,\\!,!,g' -e 's,\\:,:,g' -e 's,\\=,=,g' -e 's,^\\ , ,g' )"
    echo "$param"
}

function gettempdir {
# Create and get the temporary dir path:  
# Usage: tmpdir=$( gettempdir )

    local tempdir
    tempdir="$( grep ^output._tempdir= "$commandfile" | sed s,^output._tempdir=,, )"
    mkdir -p "$tempdir" 
    echo "$tempdir"
}

function gettempdir-local {
# Create and get a local disk temporary dir path:
# Usage: tmpdir=$( gettempdir-local )

    mktemp -d -t anduril.tmp.XXXXXXXXXX
}


function writeerror {
# Writes a string in the error file, and on the stdErr
# Usage:  writeerror "Something went wrong"

    echo "$@" >&2
    echo "$@" >> "$errorfile"
}

function writelog {
# Writes a string in the log file, and on the stdOut
# Usage: writelog "This is a message"

    echo "$@" 
    echo "$@" >> "$logfile"
}
