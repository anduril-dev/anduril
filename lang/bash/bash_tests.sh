#!/bin/bash

# Anduril BASH API unit tests.

### Init ###############################################################

if [ $# -lt 1 ]; then
    echo "Usage: $0 TEMP-DIRECTORY TEST-FUNCTIONS"
    echo "TEST-FUNCTIONS: Space-separated list of test case names. 'list' to list, 'all' to run all, All is default."
    exit 1
fi

[[ -e "${ANDURIL_HOME}/lang/bash/functions.sh" ]] || {
    echo "ANDURIL_HOME not set!"
    exit 1
}

TMP=$( readlink -f "$1" )
EXECUTE=$TMP/execute
LOG=$TMP/log
OUTPUT=$TMP/out
FUNCTIONS="${ANDURIL_HOME}/lang/bash/functions.sh"
GENERIC="${ANDURIL_HOME}/lang/bash/generic.sh"

mkdir -p "$TMP"

EXAMPLECOMMAND="metadata.instanceName=mask
metadata.Anduril=Engine 1.2.19
metadata.componentName=ImageSegment
metadata.componentPath=$TMP/anduril-bundles/anima/components/ImageSegment
metadata.ANDURIL_HOME=/usr/share/anduril/
metadata.bundleName=anima
metadata.bundlePath=$TMP/anduril-bundles/anima
input.dir=$TMP/gray
input.dir2=
input.seed=
input.array=$TMP/exec/mask/array
input._index_array=$TMP/exec/mask/array/_index
output._errors=$TMP/exec/mask/_errors
output._log=$TMP/exec/mask/_log
output._tempdir=$TMP/exec/mask/_tempdir
output.mask=$TMP/exec/mask/mask
output.masklist=$TMP/exec/mask/masklist
output.numbers=$TMP/exec/mask/numbers
output.perimeter=$TMP/exec/mask/perimeter
output.array=$TMP/exec/mask/array
output._index_array=$TMP/exec/mask/array/_index
parameter.clearborders=true
parameter.clusters=2
parameter.constant=0.5
parameter.corr=1
parameter.fillholes=true
parameter.masklist=false
parameter.maxecc=1
parameter.maxround=1
parameter.maxsize=2000
parameter.method=shape
parameter.minecc=0
parameter.minintensity=0
parameter.minround=0.8
parameter.minsize=500
parameter.numbers=false
parameter.snakeparam=0.7,0.65,1,0.7,0.65,0.65,0.65,100
parameter.command=\n                             -tile x2 @in2@ @in3@ -geometry +2+2 @out@\n"
COMMANDFILE="$TMP/_command"


## Anduril functions
function get-log-file() {
    # read log file location
    EXPECTING="$TMP/exec/mask/_log"
    . "$FUNCTIONS" "$COMMANDFILE"
    _equal-test "$EXPECTING" "$logfile" || return 1
}

function get-error-file() {
    # read error file location
    EXPECTING="$TMP/exec/mask/_errors"
    . "$FUNCTIONS" "$COMMANDFILE"
    _equal-test "$EXPECTING" "$errorfile" || return 1
}
function add-array() {
    # add array entry
    . "$FUNCTIONS" "$COMMANDFILE"
    local arr
    local arrin
    arr=$( getoutput array )
    arrin=$( getoutput _index_array )
    mkdir -p "$arr"
    [[ -e "$arrin" ]] && rm "$arrin"
    addarray array key1 file1
    _equal-test '"Key"	"File"
"key1"	"file1"' "$( cat "$arrin" )" || return 1
}
function create-array-index() {
    # add array entries by filenames
    . "$FUNCTIONS" "$COMMANDFILE"
    local arr
    local arrin
    arr=$( getoutput array )
    arrin=$( getoutput _index_array )
    mkdir -p "$arr"
    touch "$arr"/file_{1..6}
    [[ -e "$arrin" ]] && rm "$arrin"
    createarrayindex array
    _equal-hash 'ebcb06de1d7afd44d21805bfe6d237f4' "$arrin" || return 1
}
function export-command() {
    # export command file as variables
    . "$FUNCTIONS" "$COMMANDFILE"
    export_command
    _equal-test "$TMP/exec/mask/mask" "$output_mask" || return 1
    _equal-test "false" "$parameter_masklist" || return 1
}
function export-array-keys() {
    # add array entries by filenames
    . "$FUNCTIONS" "$COMMANDFILE"
    local arr
    local arrin
    arr=$( getinput array )
    arrin=$( getinput _index_array )
    mkdir -p "$arr"
    touch "$arr"/file_{1..6}
    [[ -e "$arrin" ]] && rm "$arrin"
    createarrayindex array
    
    exportarraykeys array
    _equal-test "$file_1" "$TMP/exec/mask/array/file_1" || return 1
    _equal-test "$file_2" "$TMP/exec/mask/array/file_2" || return 1
    _equal-test "$file_5" "$TMP/exec/mask/array/file_5" || return 1
}
function get-input() {
    # get a inputs from command file
    . "$FUNCTIONS" "$COMMANDFILE"
    _equal-test "$TMP/gray" "$( getinput dir )" || return 1
    _equal-test "" "$( getinput seed )" || return 1
}
function get-metadata() {
    # get a metadata from command file
    . "$FUNCTIONS" "$COMMANDFILE"
    _equal-test "anima" "$( getmetadata bundleName )" || return 1
    _equal-test "ImageSegment" "$( getmetadata componentName )" || return 1
}
function get-output() {
    # get a outputs from command file
    . "$FUNCTIONS" "$COMMANDFILE"
    _equal-test "$TMP/exec/mask/mask" "$( getoutput mask )" || return 1
    _equal-test "$TMP/exec/mask/perimeter" "$( getoutput perimeter )" || return 1
}
function get-parameter() {
    # get a parameters from command file
    . "$FUNCTIONS" "$COMMANDFILE"
    local STATUS=0
    _equal-test "1" "$( getparameter maxround )" || STATUS=1
    _equal-test "500" "$( getparameter minsize )" || STATUS=1 
    _equal-test "shape" "$( getparameter method )" || STATUS=1
    return $STATUS
}
function get-temp-dir() {
    # create a temporary folder
    . "$FUNCTIONS" "$COMMANDFILE"
    [[ -d "$TMP/exec/mask/_tempdir" ]] && rm -rf "$TMP/exec/mask/_tempdir"
    tmpdir=$( gettempdir )
    _equal-test "$TMP/exec/mask/_tempdir" "$tmpdir" || return 1
    [[ -d "$TMP/exec/mask/_tempdir" ]] && { 
        return 0 
    } || { 
        echo "Folder $TMP/exec/mask/_tempdir was not created"
        return 1
    }
}
function write-error() {
    # write an error
    . "$FUNCTIONS" "$COMMANDFILE"
    [[ -e "$errorfile" ]] && rm "$errorfile"
    writeerror "This is an error."
    _equal-hash "b2dfa0c9f896f3944e749e05102bb209" "$TMP/exec/mask/_errors" || return 1
}

## Generic functions
function test-calc() {
    # test the calc function
    . "$GENERIC"
    _equal-test "4" $( calc "(1+1)*2" ) || return 1
    _equal-test "3.1428" $( calc "22/7" 4 ) || return 1
}
function test-csvcoln() {
    # test the csv column function
    . "$GENERIC"
    _equal-test "3" $( csvcoln <( echo -e "Key\tFile\tValue" ) Value ) || return 1
    
    csvcoln <( echo -e "Key\tFile\tValue" ) NotColumn || { 
        # Fails as planned
        return 0 
    } && {
        echo "Column found when it's not there"
        return 1
    }
    return $STATUS
}
function test-filesize() {
    # test the filesize function
    . "$GENERIC"
    local STATUS=0
    _equal-test "3 B" "$( filesize 3 )" || STATUS=1
    _equal-test "2.9 KB" "$( filesize 3000 )" || STATUS=1
    _equal-test "27.9 GB" "$( filesize 30000000000 )" || STATUS=1
    # fail with strings
    filesize string && STATUS=1
    return $STATUS
}
function test-iscmd() {
    # test the filesize function
    . "$GENERIC"
    local STATUS=0
    iscmd bash || STATUS=1
    iscmd hope__this__is__not__a__command && STATUS=1
    return $STATUS
}
function test-stringtomap() {
    # test the stringtomap function
    . "$GENERIC"
    local STATUS=0
    stringtomap "map1=value1,map5=value3" || STATUS=1
    _equal-test "value1" "$map1" || STATUS=1
    
    return $STATUS
    
}


## functions starting with _ are not tested. They are helper functions for the testing framework
function _fail-msg() {
    # fail a function
    echo -e "failed: \n Expecting: $1 \n Got:       $2"
    return 1
}
function _equal-test() {
    # test for equality of strings or fail
    [[ "$1" = "$2" ]] || _fail-msg "$1" "$2"
}
function _equal-hash() {
    # test for equality of file hashes or fail
    newhash=$( cat "$2" | md5sum | cut -f1 -d' ' )
    [[ "$1" = "$newhash" ]] || _fail-msg "md5sum: $1" "md5sum: $newhash $2"
}

### Run tests ##########################################################

if [ "$2" = '' ]; then
    TESTS=( $( grep "^function " $0 | grep -v "^function _" | sed 's,.* \(.*\)().*,\1,' | xargs echo ) )
elif [ "$2" = 'all' ]; then
    TESTS=( $( grep "^function " $0 | grep -v "^function _" | sed 's,.* \(.*\)().*,\1,' | xargs echo ) )
elif [ "$2" = 'list' ]; then
    grep "^function " $0 | grep -v "^function _" | sed 's,.* \(.*\)().*,\1,' | xargs echo
    exit 0
else
    TESTS="${@:2}"
fi

echo "$EXAMPLECOMMAND" > "$COMMANDFILE"
echo Testing functions in $FUNCTIONS
FAILURES=()
for (( i=0; i<${#TESTS[@]};i++ )) ; do
    TEST=${TESTS[$i]}
    OUT="$TMP/$TEST.out"
    ERR="$TMP/$TEST.err"
    echo "----- $TEST -----"
    $TEST 2> "$ERR" > "$OUT"
    STATUS=$?

    if [ $STATUS -ne 0 ]; then
        echo "--- BEGIN LOG ---"
        tail -n500 $OUT
        tail -n500 $ERR
        echo "--- END LOG ---"
        echo Failed: $TEST
        FAILURES+=( $TEST )
    else echo OK: $TEST
    fi
done

echo '-----'
for (( j=0; i<${#FAILURES[@]};j++ )) ; do
    TEST=${FAILURES[$j]}
    echo "---- FAILED: $TEST -----"
done | grep --color=auto "FAILED"
[[ $j -eq 0 ]] && { 
    echo OK. All test results are as expected.
    exit 0
} || {
    exit 1
}
