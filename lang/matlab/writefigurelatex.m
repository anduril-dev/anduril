function writefigurelatex(outdir,figureid,figurename,figureformat,caption,suffix,componentname)
% Writes a figure on disk, and generates LaTeX -code that refers to it.
% WRITEFIGURELATEX(OUTDIR,FIGUREID,FIGURENAME,FIGUREFORMAT,CAPTION,SUFFIX,COMPONENTNAME)
%   OUTDIR        = Directory to write to
%   FIGUREID      = matlab figure number or 'gcf'
%   FIGURENAME    = filename to use for the image file
%   FIGUREFORMAT  = format for the image file 'pdf'/'png'/'jpg'
%   CAPTION       = caption for figure
%   SUFFIX        = latex label string for refering eg.  'text'  ->   'fig:text'
%   COMPONENTNAME = optional name for the component. appears in the subsection field of the LaTeX document.
%                   if left out or '', no subsection will be generated.
% The TeX document is appended every time you call this function.
% 
% Authors:
% anna-maria.lahesmaa@helsinki.fi
% ville.rantanen@helsinki.fi


if ~exist('suffix','var'), suffix = ''; end
if ~exist('componentname','var'), componentname = ''; end

directoryname=outdir;

if ~exist(directoryname, 'dir')
    mkdir(directoryname)
end

savefigure = [directoryname filesep figurename];
switch lower(figureformat)
    case 'png'
        %saveas(figureid,savefigure,'png');
        print(figureid,'-dpng','-r90',savefigure);
    case 'jpg'
        %saveas(figureid,savefigure,'jpg');
        print(figureid,'-djpeg99','-r90',savefigure);
    case 'pdf'
        print(figureid,'-dpdf','-r90',savefigure);
end

%% write to file

filename = [directoryname filesep 'document.tex'];
fid=fopen(filename,'at');
if ~strcmp(componentname, '')
    fprintf(fid, ['\\subsection{' componentname  '}\n']);
end
fprintf(fid, ['\\begin{figure}[!ht]\n\\begin{center}\n']);
fprintf(fid, ['\\includegraphics[width=15cm,height=10cm,keepaspectratio=true]{' figurename '}\n']);
fprintf(fid, '\\end{center}\n');
fprintf(fid, ['\\caption{' caption '}\n']);
fprintf(fid, ['\\label{fig:' suffix '}\n']);
fprintf(fid, '\\end{figure}');
if ~strcmp(componentname, '')
    fprintf(fid, '\\clearpage\n');
end


fclose(fid);

