function value=getinput(cf, var)
% Retrieves an input port from the commandfile variable set.
% VALUE=GETINPUT(CF, VAR)
%
%   CF  = pipeline commadfile properties (see readcommandfile)
%   VAR = the input variable name as a string
%   VALUE = value of the variable
%
% Author: ville.rantanen@helsinki.fi 2011 01

var=['input_' var];

if (~isfield(cf,var))
    writeerror(cf,['Variable ' var ' not found!']);
    value=nan();
    return
end    
value=getfield(cf,var);

