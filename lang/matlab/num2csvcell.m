function data=num2csvcell(numdata,format)
% Converts a numerical data matrix in to string cell array,
% suitable for WRITEFILECSV function
% DATA=NUM2CSVCELL(NUMDATA,FORMAT)
%   NUMDATA = Array of numbers (may include nan's)
%   FORMAT  = format of numerical conversion, defaults to '%g'
%
% Author: ville.rantanen@helsinki.fi   2011 01

if ~exist('format','var')
    format='%g';
end
if any(size(numdata)==0)
    data=cell(size(numdata));
    return
end

data=cellfun(@(x) sprintf(format,x), num2cell(numdata),'UniformOutput',false);
