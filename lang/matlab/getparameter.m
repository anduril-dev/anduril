function value=getparameter(cf, var, vtype)
% Retrieves a parameter value from the commandfile variable set.
% VALUE=GETPARAMETER(CF, VAR, VTYPE)
%
%   CF    = pipeline commadfile properties (see readcommandfile)
%   VAR   = the variable name as a string
%   VTYPE = the expected variable type as a string:
%            'string','float','boolean','guess'
%           Defaults to 'float'
%           'guess' will return a string or float.
%   VALUE = value of the variable
%
% Author: ville.rantanen@helsinki.fi 2011 01

var=['parameter_' var];

if (~isfield(cf,var))
    writeerror(cf,['Variable ' var ' not found!']);
    value=nan();
    return
end
if ~exist('vtype','var')
    vtype='float';
end
value=getfield(cf,var);
value=getvariabletype(value,vtype);



