function cf=readcommandfile(commandfile)
% Loads the Anduril commandfile contents and converts it to a string struct.
%
% CF=READCOMMANDFILE(COMMANDFILE)
%
%   COMMANDFILE = string with the command file name (usually '_command')
%   CF          = struct of strings. 
%    struct names are the variable names and the values are the variable values as strings.
%    CF variables can be accessed through functions GETPARAMETER, GETINPUT and GETOUTPUT
%
% EXAMPLE:
%  cf=readcommandfile('_command');
%  getparameter(cf,'var1','boolean')
%
% author: ville.rantanen@helsinki.fi

cf=struct();
fid=fopen(commandfile,'r');  % open the file for reading
while 1
    tline=fgetl(fid); % read a line
    if ~ischar(tline) % if line is not a string, end loop (i.e. EOF) 
        break
    end
    cparts=regexp(tline, '=', 'split','once'); % split the variable name and value
    fieldname=strrep(char(cparts{1}),'.','_');
    cf=setfield(cf,fieldname,cparts{2});
end
fclose(fid);  % close the file

