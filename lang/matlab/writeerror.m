function writeerror(cf, message)
% Writes a string to the error log.
% WRITEERROR(CF, message)
%   CF      = pipeline commadfile properties
%   message = the string to write to the error file
%
% EXAMPLE:
% if ~exist('parameter','var')
%   writeerror(cf,'Variable PARAMETER not found')
% end
% Author: ville.rantanen@helsinki.fi   2008 10

logfile=getoutput(cf,'_errors');

if strcmp(logfile,'NA')
    disp(message)
else

fid=fopen(logfile, 'at');
fprintf(fid, '%s\n',message);

fclose(fid);
end
