#!/bin/bash

[[ ${#@} -ne 1 ]] && {
    echo Usage:  $0 "[target directory]"
    exit
}

if [ -z $( which matlab ) ]
then 
   mkdir -p "$1"
   echo "Matlab was not available during documentation generation." > "$1"/index.html
else
    set -x
    SRCDIR=$( dirname $( readlink -f $0 ) ) 
    TGTDIR=$( readlink -f $1 )
    cd "$SRCDIR"/../
    matlab -nosplash -nojvm -nodesktop -r "addpath('matlab/m2html','.'); m2html('mFiles','matlab','recursive','off','htmlDir','$TGTDIR','source','off'); exit;"
fi
