function tempdir=gettempdir(cf)
% Creates a temporary folder for the component and returns it's path.
% TEMPDIR=GETTEMPDIR(CF)
%   CF        = pipeline commadfile properties
%   TEMPDIR   = string containing the path to the folder
%
% EXAMPLE:
% tempdir=gettempdir(cf)
% dlmwrite([tempdir filesep 'file.txt'],rand(5,5),'delimiter','\t','precision','%.6f')
%
% Author: ville.rantanen@helsinki.fi   2010 12

tempdir=getoutput(cf,'_tempdir'); 
mkdir(tempdir);
end
