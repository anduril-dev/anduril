function writefilecsv(filename,table)
% Writes a pre-formatted CSV file
% WRITEFILECSV(FILENAME,TABLE)
%   FILENAME = string variable, file to write
%   TABLE    = a struct, containing fields:
%       TABLE.COLUMNHEADS = cell array of strings for heading row {1xM}
%       TABLE.DATA        = cell array of strings containing the data. 
%                           number of columns must match! {NxM}
%
% EXAMPLE
% table.columnheads={'Col1','Col2'};
% table.numdata=num2cell(eye(2));
% table.data=cellfun(@(x) sprintf('%g',x), table.numdata,'UniformOutput',false);
% writefilecsv('test.csv',table)
%
% Author: ville.rantanen@helsinki.fi   2009 11

if and(size(table.columnheads,2)~=size(table.data,2),numel(table.data)>0)
    error('anduril:WrongSize','Header and data column numbers do not match.' );
end

%% replace NaNs to NA
table.data=cellfun(@(x) regexprep(x, '^NaN$', 'NA'), table.data,'UniformOutput', false); 

%% write to file
fid=fopen(filename,'wt');
if size(table.columnheads,2)>1, fprintf(fid, '"%s"\t', table.columnheads{1:(end-1)}); end
fprintf(fid, '"%s"', table.columnheads{end});
fprintf(fid, '\n');
cols=size(table.data,2);
rowstr=[repmat('%s\t',[1 cols-1]) '%s\n'];
table.data=table.data';
fprintf(fid,rowstr, table.data{:});
fclose(fid);

