% This script runs a debugging loop. You may interact Matlab by
% writing commands in [debugger] file
%  Example:
% Add these lines to your code:
%    debugfile='path_to_a_file'
%    debugshell
% Then give commands:
%    echo whos > path_to_a_file
%    echo quit > path_to_a_file

if ~exist('debugfile','var')
    debugfile='debugger';
end

try
    debugger.file=fopen(debugfile,'w');
    fclose(debugger.file);
    if exist([pwd filesep debugfile],'file')==2
        debugger.path=[pwd filesep debugfile];
    elseif exist(debugfile,'file')==2
        debugger.path=debugfile;
    else
        error(['Could not find where I wrote "' debugfile '"']);
    end
    disp(['Your debugger input file is at: ' debugger.path]);
    disp('Delete the input file to exit debugger loop');
    disp('NOTE: using multiple instances of debugger from the same input file is not recommended.');
    debugger.end=false;
    while ~debugger.end
        debugger.file=fopen(debugger.path,'rt');
        if debugger.file~=-1 % file exists
            % read scriptfile
            debugger.script=textscan(debugger.file,'%s','Delimiter','','EndOfLine','');
            fclose(debugger.file);
            debugger.script=char(debugger.script{1});
            % empty the scriptfile
            debugger.file=fopen(debugger.path,'w');
            fclose(debugger.file);
            if strcmp(debugger.script,'quit')
                %The quit command has been given
                disp('You exited the debugger. Continuing the script');
                debugger.end=true;
            else
                % Run the given commands 
                try
                    eval(debugger.script);
                catch debugger_me
                    disp(debugger_me.message);
                    for st=1:size(debugger_me.stack,1)
                        disp(debugger_me.stack(st).file);
                        disp(num2str(debugger_me.stack(st).line));
                    end
                end
            end
        else % file has been deleted
            disp('You have deleted the debugger file. Continuing the script');
            debugger.end=true;
        end
        % re-check every second
        pause(1);
    end
    
catch me
    disp(me.message);
    for st=1:size(me.stack,1)
        disp(me.stack(st).file);
        disp(num2str(me.stack(st).line));
    end
end
