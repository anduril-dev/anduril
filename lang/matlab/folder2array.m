function folder2array(cf,arrayname,regex)
% Creates an anduril array index file in a folder of files.
% FOLDER2ARRAY(CF,ARRAYNAME,REGEX);
%   CF=pipeline commadfile properties (see readcommandfile)
%   ARRAYNAME=string name of output array
%   REGEX is a regular expression for matching files (case insensitive)
%
% Author: ville.rantanen@helsinki.fi

    if (nargin<2)
        error('anduril:ParameterMissing','You have to give the output array name' );
    end
    if (nargin<3)
        regex='.*'
    end
    fdir=getoutput(cf,arrayname);    
    if ~exist(fdir,'dir')
        error('anduril:FileMissing',['Folder "' fdir '" not found']);
    end
    
    fdir=regexprep(fdir,[filesep '$'],'');
    array.columnheads={'Key','File'};
    array.data=listcontents(fdir,regex);
    writearray(cf,arrayname,array);
end
function fulllist=listcontents(fdir,regex)
    filelist=struct2cell(dir(fdir));
    filelist=filelist([1 4],:); % Only save name and dir-status
    validlist=regexp(filelist(1,:), '^_|\.$|\.\.$');
    validlist=cellfun(@(x) isempty(x),validlist);
    uservalidlist=regexpi(filelist(1,:), regex);
    uservalidlist=not(cellfun(@(x) isempty(x),uservalidlist));
    filelist=filelist(:,and(validlist,uservalidlist));
    dirlist=filelist(1,cellfun(@(x) x==1,filelist(2,:)));
    filelist=filelist(1,cellfun(@(x) x==0,filelist(2,:)));
    
    dirlist=sort(dirlist);
    filelist=sort(filelist);
    fulllist=repmat([dirlist filelist]',[1 2]);
    fulllist(:,2)=cellfun(@(x) [fdir filesep x],fulllist(:,2),'UniformOutput',false);

end
