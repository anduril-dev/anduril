function writeerrorstack(cf, me)
% Writes the error stack to the error log file. 
% WRITEERRORSTACK(CF, ME)
%   CF = pipeline commadfile properties
%   ME = the stack retrieved with the "try ... catch me ... end" routine
%
% EXAMPLE:
% try
%   f=fopen('non_existing_file.ext');
% catch ME
%   writeerrorstack(cf,ME)
% end

% ville.rantanen@helsinki.fi 2008 10

logfile=getoutput(cf,'_errors'); 
if strcmp(logfile,'NA')
    disp(me.message)
else

fid=fopen(logfile, 'at');
fprintf(fid, '%s\n', me.message);
for st=1:size(me.stack,1)
    fprintf(fid, '%s\n', me.stack(st).file);
    fprintf(fid, 'line: %s\n', num2str(me.stack(st).line));
end

fclose(fid);
end
