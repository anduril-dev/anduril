function data=csvcell2num(celldata)
% Converts a string cell array to numerical data matrix,
% suitable for conversion after READCSVCELL function
% NUMDATA=CSVCELL2NUM(CELLDATA)
%   CELLDATA = Cell array of numerical strings
%   NUMDATA = Array of numbers (may include nan's)
%
% Author: ville.rantanen@helsinki.fi   2011 02

data=str2double(celldata);

