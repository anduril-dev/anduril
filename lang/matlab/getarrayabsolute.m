function path=getarrayabsolute(dir,file)
% PATH=GETARRAYABSOLUTE(DIR,FILE)
% Returns the absolute path to an existing file in
% either  DIR/FILE  if the file is relative 
% or      FILE      if the file is absolute
% An error is created when the file does not exist.
%
% Ville.Rantanen@helsinki.fi 2011


if exist([filesep file],'file')
   path=file;
   return
end
if exist([dir filesep file],'file')
   path=[dir filesep file];
   return
end

error(['The file ' file ' or ' [dir filesep file] ' do not exist'])



