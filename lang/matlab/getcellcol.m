function [c,iscell]=getcellcol(celmat,str,vtype,delim)
% Searches struct CSV.COLUMNHEADS for STRING and returns the corresponding COLUMN from CSV.DATA.
%
% [COLUMN,ISCELL]=GETCELLCOL(CSV,STRING,VTYPE,DELIM)
%
% VTYPE is the expected variable type,  'guess','string','float' or 'boolean'. Defaults to 'guess'.
% DELIM is the decimal separator. defaults to '.'
%
% The variable CSV must be in the format of READCSVCELL output.
%
% When using 'guess' type, the cell is returned as a [Mx1] double vector
% if the column appears to be numeric,
% otherwise it is returned as a {Mx1} cell string vector.
%
% ISCELL equals 1, when the result was not converted to double. Valid only when 'guess' type used.
%
% Author: ville.rantanen@helsinki.fi

if ~exist('delim','var')
    delim='.';
end
if ~exist('vtype','var')
    vtype='guess';
end
colnum=strcmp(celmat.columnheads,str);
if sum(colnum)~=1
    error('Anduril:error',[ '"' str '" Column name not found'])
    return
end

if ~strcmp(delim,'.')
    celmat.data(:,colnum)=regexprep(celmat.data(:,colnum), delim, '.');
end
iscell=0;
switch vtype
    case 'string'
        c=celmat.data(:,colnum);
        iscell=1;
    case 'guess'
        coldata=str2double(celmat.data(:,colnum));
        listofnans=isnan(coldata); % find any non numerical rows
        if sum(listofnans)>0 % if there are any non numbers
            if or(strcmpi('Na', celmat.data(:,colnum)),strcmpi('NaN', celmat.data(:,colnum)))==listofnans
                c=coldata; % Non-numericals were Na or NaN
            else
                c=celmat.data(:,colnum); % Other non-numericals detected, return a string cell array.
                iscell=1;
            end
        else
            c=coldata;
        end
    case 'float'
        c=str2double(celmat.data(:,colnum));
    case 'boolean'
        c=str2double(celmat.data(:,colnum));
        c(strcmpi('true', celmat.data(:,colnum)))=1;
        c(strcmpi('false', celmat.data(:,colnum)))=1;
        c=logical(c);
end
