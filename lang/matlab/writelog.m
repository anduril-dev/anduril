function writelog(cf, string_in)
% Writes a string to the log file.
% WRITELOG(CF, STRING_IN)
%
%   CF        = pipeline commadfile properties (see readcommandfile)
%   STRING_IN = the string to write in the log
%
% EXAMPLE
%   writelog(cf,'component works')
%
% Author: ville.rantanen@helsinki.fi   2008 10

logfile=getoutput(cf,'_log'); % new (2011) style CF variable
fid=fopen(logfile, 'at');
fprintf(fid, '%s\n', string_in);

fclose(fid);
