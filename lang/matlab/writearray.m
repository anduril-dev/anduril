function writearray(cf,arrayname,arraydata)
% Write an anduril array data type file.
% WRITEARRAY(CF,ARRAYNAME,ARRAYDATA);
%   CF = pipeline commadfile properties (see readcommandfile)
%   ARRAYNAME = string name of input array
%   ARRAYDATA = A struct with cell string arrays. Column heads must be "Key" and "File".
%               see readcsvcell.m
%
% Author: ville.rantanen@helsinki.fi
    
    outfolder=getoutput(cf,arrayname);
    if ~exist(outfolder,'dir')
        mkdir(outfolder);
    end
    outindex=getoutput(cf,['_index_' arrayname]);
    writefilecsv(outindex,arraydata);
    
end
