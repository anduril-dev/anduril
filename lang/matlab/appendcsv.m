function appendcsv(filename,table)
% Appends to a pre-formatted CSV file. If the file does not exist, the
% header is written.
% APPENDCSV(FILENAME,TABLE)
%   FILENAME = string variable, file to write (with path)
%   TABLE = a struct, containing fields:
%       TABLE.COLUMNHEADS = cell array of strings for heading row {1xM}
%       TABLE.DATA        = cell array of strings containing the data. 
%                           column number must match! {NxM}
%
% EXAMPLE
% table.columnheads={'Col1','Col2'};
% table.numdata=num2cell(eye(2));
% table.data=cellfun(@(x) sprintf('%g',x), table.numdata,'UniformOutput',false);
% appendcsv('test.csv',table)
%
% NOTE: the function does not know the earlier column count of the file,
% so you may end up having wrongly constructed table.
%
% Author: ville.rantanen@helsinki.fi   2011 04

if and(size(table.columnheads,2)~=size(table.data,2),numel(table.data)>0)
    error('anduril:WrongSize','Header and data column numbers do not match.' );
end

if (~exist(filename,'file')) 
    writefilecsv(filename,table);
    return
end
%% replace NaNs to NA
table.data=cellfun(@(x) regexprep(x, '^NaN$', 'NA'), table.data,'UniformOutput', false); 
%% append data to file

cols=size(table.data,2);
rowstr=[repmat('%s\t',[1 cols-1]) '%s\n'];
table.data=table.data';
fid=fopen(filename,'at');
fprintf(fid,rowstr, table.data{:});
fclose(fid);

