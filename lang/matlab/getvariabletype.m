function value=getvariabletype(string,vtype)
% Converts a string value to wanted data type, or if the string 
% is not convertable returns the same string and a warning.
%   VALUE=GETVARIABLETYPE(STRING,VTYPE)
%
%   STRING = string variable
%   VTYPE  = the expected variable type as a string:
%            'string','float','boolean','guess'
%            Defaults to 'guess'
%            'guess' will return a 'string' or 'float'.
%            'string' will return an escaped string, use sprintf to 
%                format any possible \n or \t characters,
%                or regexp(s,'\\n','split') to split into array.
%            All other methods will fail if conversion is not possible.
%   VALUE  = value of the variable
%
% Author: ville.rantanen@helsinki.fi

if ~exist('vtype','var')
    vtype='guess';
end
if ~isstr(string)
    error('anduril:ParameterType','parameter is not string')
end

value=regexprep(string,...
      {'\\#','\\!','\\=','\\:','^\\ '},...
      {'#',  '!',  '=',  ':',' '});

switch vtype
    case 'string'

    case 'float'
        vfloat=str2double(string);
        if isnan(vfloat)
            error('anduril:VariableType',['"' string '" could not be converted to float.']);
        else
            value=vfloat;
        end
    case 'boolean'
        vbool=strcmpi(string,{'true','false','1','0'});
        if any(vbool)
            if vbool(1)
                value=true;
            elseif vbool(2)
                value=false;
            elseif vbool(3)
                value=true;
            elseif vbool(4)
                value=false;                
            end
        else
            error('anduril:VariableType',['"' string '" could not be converted to boolean. ']);
        end
    case 'guess'

        vfloat=str2double(string);
        if any(strcmpi({'Na','NaN'}, string))
            value=nan();
        elseif ~isnan(vfloat)
            value=vfloat;
        end
        
    otherwise
        error('anduril:ParameterError',['Variable type "' vtype '" not recognized']);
        return
end
