function value=getmetadata(cf, var)
% Retrieves a metadata value from the commandfile variable set.
% VALUE=GETMETADATA(CF, VAR)
%
%   CF  = pipeline commadfile properties (see readcommandfile)
%   VAR = the metadata variable name as a string
%   VALUE = value of the variable, NaN if variable not set
%
% Author: ville.rantanen@helsinki.fi 2011 01

var=['metadata_' var];

if (~isfield(cf,var))
    %writeerror(cf,['Variable ' var ' not found!']);
    value=nan();
    return
end    
value=getfield(cf,var);


