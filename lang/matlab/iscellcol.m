function iscol=iscellcol(celmat,str)
% Searches struct CSV.COLUMNHEADS for STRING and returns true if column name found
%
% ISCOL=ISCELLCOL(CSV,STRING)
%
% The variable CSV must be in the format of READCSVCELL output.
%
%
% Author: ville.rantanen@helsinki.fi

colnum=strcmp(celmat.columnheads,str);
if sum(colnum)~=1
    iscol=false;
else
    iscol=true;
end
