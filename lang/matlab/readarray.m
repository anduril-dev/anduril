function data=readarray(cf,arrayname)
% Read an anduril array data type file.
% Returns a cell string table of keys and values.
% TABLE=READARRAY(CF,ARRAYNAME);
%   CF         = pipeline commadfile properties (see readcommandfile)
%   ARRAYNAME  = string name of input array
%   TABLE.DATA = Nx2 matrix with keys and files
%
% Author: ville.rantanen@helsinki.fi
    
    infolder=getinput(cf,arrayname);
    inindex=getinput(cf,['_index_' arrayname]);
    if ~exist(inindex,'file')
        error('anduril:FileMissing',['File "' inindex '" not found']);
    end
    data=readcsvcell(inindex);
    for row=1:size(data.data,1)
        data.data{row,2}=getarrayabsolute(infolder,data.data{row,2});
    end
    
end
