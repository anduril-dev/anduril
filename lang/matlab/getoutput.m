function value=getoutput(cf, var)
% Retrieves an output value from the commandfile variable set.
% VALUE=GETOUTPUT(CF, VAR)
%
%   CF  = pipeline commadfile properties (see readcommandfile)
%   VAR = the output variable name as a string
%   VALUE = value of the variable
%
% Author: ville.rantanen@helsinki.fi 2011 01

var=['output_' var];

if (~isfield(cf,var))
    writeerror(cf,['Variable ' var ' not found!']);
    value=nan();
    return
end    
value=getfield(cf,var);

