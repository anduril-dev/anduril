function value=inputdefined(cf,var)
% Checks whether an optional input is defined.
% VALUE=INPUTDEFINED(CF, VAR)
%
%   CF  = pipeline commadfile properties (see readcommandfile)
%   VAR = the input variable name as a string
%   VALUE = true or false
%
% Author: ville.rantanen@helsinki.fi 2011 08

var=['input_' var];

if (~isfield(cf,var))
    writeerror(cf,['Input ' var ' not found!']);
    value=nan();
    return
end
if strcmp(getfield(cf,var),'')
    value=false;
else
    value=true;
end

