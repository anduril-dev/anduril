function out=getvariable(cf, var)
% Retrieves a value from the commandfile variable set.
% OUT=GETVARIABLE(CF, VAR)
%
%   CF  = pipeline commadfile properties (see commandfileparser)
%   VAR = the variable name as a string
%   OUT = value of the variable
%
% Author: ville.rantanen@helsinki.fi 2008 10

found=false;
for varn=1:size(cf,1)
   if strcmp(char(cf(varn,1)), var)
       out=cf{varn,2};
       found=true;
       break
   end
end

if found==false
    out='NA';
    error(['Variable ' var ' not found!']);
end
