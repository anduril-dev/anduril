function data=createarray()
% Create an empty array structure
% ARRAY=CREATEARRAY();
%   ARRAY = A struct with fields 'columnheads' and 'data'. 
%
% Author: ville.rantanen@helsinki.fi
    
    data.columnheads={'Key','File'};
    data.data=cell(0);
   
end
