#########################################################################
#                      Perl componentSkeleton                           #
# Ping Chen (ping.chen@helsinki.fi)                                     #
#########################################################################

package componentSkeleton;
use strict;
require Exporter;

our $version = 0.01;
our @ISA = qw(Exporter);
our @EXPORT = qw(&main &get_input &get_output &get_parameter &parse_command_file &read_table &read_property &FetchColumnContent &write_log &input_defined &get_InputArrayIndex);
#our @EXPORT_OK = qw(&func4);
our %EXPORT_TAGS= ();

our %cf;
our @argv;
our %result;

################
# Command file #
################

sub input_defined{
	my ($cf_ref, $name) = @_;
	$name = "input.".$name;
	my $path = $cf_ref->{$name};
	if(length($path)>0){
		return(1);
	}else{
		return(0);
	}
}

sub get_input{
	my ($cf_ref,$name) = @_;
	$name = "input.".$name;
	my $path = $cf_ref->{$name};
	return($path);
}

# Read array index file and 
# store key and absolute path in hash
sub get_InputArrayIndex{
	my ($cf_ref, $name) = @_;
	$name = "input.".$name;
	my $port = $cf_ref->{$name};
        my $path = $port."/_index";

	my %hash_arrayIdx;
	open(FILEIN, $path);
	my @lines = <FILEIN>;
	my $nline = @lines;
	for(my $i=1;$i<$nline;$i++){
		chomp($lines[$i]);
		$lines[$i] =~ s/\"//g; # Remove quotes
		my @tmp = split(/\t/, $lines[$i]); 
		if($tmp[1]=~/^\//){
			$hash_arrayIdx{$tmp[0]} = $tmp[1];
		}else{
			$hash_arrayIdx{$tmp[0]} = $port."/".$tmp[1];
		}
	}
	close(FILEIN);
	return(\%hash_arrayIdx);
}


sub get_output{
	my ($cf_ref,$name) = @_;
	$name = "output.".$name;
	my $path = $cf_ref->{$name};
	return($path);
}

sub get_parameter{
	my ($cf_ref,$name) = @_;
	$name = "parameter.".$name;

	$name =~ s/"\\\\n"/"\\n"/g;
	$name =~ s/"\\\\r"/"\\r"/g;
	$name =~ s/"\\\\t"/"\\t"/g;
	$name =~ s/"\\\\\\\\"/"\\\\"/g;
	$name =~ s/"\\\\\""/"\\\""/g;

	$name =~ s/"\\\\#"/"#"/g;
	$name =~ s/"\\\\!"/"!"/g;
	$name =~ s/"\\\\="/"="/g;
	$name =~ s/"\\\\:"/":"/g;

	my $path = $cf_ref->{$name};
	return($path);
}

sub parse_command_file{
	my ($filepath) = @_;
	open(FILE,$filepath);
	my @lines = <FILE>;
	my $line;
	my @val;
	my %hash;
	foreach $line (@lines){
		chomp($line);
		@val= split(/=/,$line);
		$hash{$val[0]} = $val[1];
	}
	close(FILE);
	return(%hash);
}

sub write_log{
	my ($cf_ref,$message) = @_;
	open(FILE,">>".$cf_ref->{"output._log"});
	print FILE $message."\n";
	close(FILE);
}

##################
# Perl functions #
##################

# Read table function
sub read_table{
	my($file,$sep)=@_;
	open(FILE, $file);
	my @ref_array;
	my $i = 0;
	while(<FILE>){
		chomp($_);
		$_=~s/["']//g;
		my @tmp=split(/$sep+/,$_);
		push @ref_array, \@tmp;
	}
	close(FILE);
	return(@ref_array);
}

# Read property file
sub read_property{
	my($file) = @_;
	my %hash = {};
	open(FILE, $file);
	while(<FILE>){
		chomp($_);
		$_=~s/["']//g;
		my @tmp = split(/ *= */,$_);
		$hash{$tmp[0]} = $tmp[1];
	}
	close(FILE);
	return(%hash);
}

# Read one column from table (if table contains header, $header should be set to TRUE)
sub FetchColumnContent{
	my($ref_ref_array,$column,$header) = @_;
	$column--;
	my $row = @{$ref_ref_array};
	my @elements = qw();
	my $start = 0;
	
	if($header){
		$start = 1;
	}
	
	for(my $i=$start;$i<$row;$i++){
		push @elements, $$ref_ref_array[$i]->[$column];
	}
	return(@elements);
}










1;
__END__
