function writeerrorstack(cf, me)
% function writeerrorstack(cf, me)
% Writes the error stack to the error log file. 
%   cf = pipeline commadfile properties (see commandfileparser)
%   me = the stack retrieved with the "try ... catch me ... end" routine

% ville.rantanen@helsinki.fi 2008 10

constants;

logfile=getvariable(cf,'output._errors');

% disp(['writing: ' string_in]);
fid=fopen(logfile, 'at');
fprintf(fid, [me.message '\n']);
for st=1:size(me.stack,1)
    fprintf(fid, [me.stack(st).file '\n']);
    fprintf(fid, ['line: ' num2str(me.stack(st).line) '\n']);

end
fprintf(fid, ['\n' stringlistseparator '\n'] );

fclose(fid);
