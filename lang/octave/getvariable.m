function out=getvariable(cf, var)
% function out=getvariable(cf, var)
% Retrieves a value from the commandfile variable set.
%   cf  = pipeline commadfile properties (see commandfileparser)
%   var = the variable name as a string
%   out = value of the variable

% ville.rantanen@helsinki.fi 2008 10

found=false;
for varn=1:size(cf,1)
   if strcmp(cf{varn,1}, var)
       out=cf{varn,2};
       found=true;
       break
   end
end

if found==false
    out='NA';
    disp(['Variable ' var ' not found!']);
end
