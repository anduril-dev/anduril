function cf=componentskeletonmain(commandfile)
% cf=componentskeletonmain(commandfile)
% this function loads the commandfile contents and puts the in to a string
% array.
%   commandfile = string with the command file name
%   cf = Nx2 cell array. first column is the variable name and the second column the variable value
% cf variables can be accessed through the function getvariable(component,'variablename')

% ville.rantanen@helsinki.fi

%   disp('Starting matlab component');
    
cf=commandfileparser(commandfile);


	

