function table=readfilecsv(cf,jump,delim)
% function table=readfilecsv(cf,filevar, jump,delim,dl)
% Reads a CSV filein to a table structure.
%   cf      = file name
%   table   = struct, containing fields:
%       table.columnheads = cell array of strings for header row
%       table.data     = cell array of data
%       table.numdata  = only the numerical part of the data
%       table.isnum    = shows which columns are numerical
%
% jump = jump to row number or first row containing text (int or string) (defaults to 0)
% delim = csv delimiter (char(9) or ',', defaults to char(9)=tab)


% ville.rantanen@helsinki.fi   2010 02

filename=cf;

%fid=fopen(filename,'rt');

%% get the column number
cols=0;
if ~exist('jump','var')
    jump=0;
end
if ~exist('delim','var')
        delim=char(9);
end
data=csv2cell(filename,delim);

table.columnheads=data(1+jump,:);
table.data=data((2+jump):end,:);
valueisnum=cellfun(@(x) isnumeric(x),table.data);
table.isnum=max(valueisnum,[],1);
table.numdata=nan(size(table.data));

table.numdata(valueisnum)=cellfun(@(x) (x),table.data(valueisnum));


