function component=commandfileparser(commandfile)
% function component=commandfileparser(commandfile)
% Does the actual reading of the command file.
%   commadfile = a string containing the command file path where input file
%                locations and parameters are read from.
%   component  = a string array of variables, which can be
%                accessed through the function getvariable(component,'variablename')

% ville.rantanen@helsinki.fi   2008 10 

i=1;
component=cell(1);
fid=fopen(commandfile,'r');  % open the file for reading
while 1
    tline=fgetl(fid); % read a line
% disp(tline);
    if ~ischar(tline) % if line is not a string, end loop (i.e. EOF) 
        break
    end
    cparts=split(tline, '=', 2); % split the variable name and value
    component{i,1} =strtrim(cparts(1,:));
    cpart2=str2num(cparts(2,:));
    if numel(cpart2)~=0
        component{i,2} = cpart2; % numeric values are saved as such
% disp(' is number');
    else 
        component{i,2} =strtrim(cparts(2,:)); % string values saved as character arrays
    end

    i=i+1;
    
end
fclose(fid);  % close the file

