function writefilecsv(filename,table)
% function writefilecsv(filename,table)
%   filename= file to write to
%   table = a struct, containing fields:
%       table.columnheads = cell array of strings for heading row {1xM}
%       table.data        = cell array of strings containing the data. 
%                           column number must match! {NxM}
% Example of creating table.data
% table.numdata=num2cell(datamatrix);
% table.data=cellfun(@(x) sprintf('%g',x), table.numdata,'UniformOutput',false);

% ville.rantanen@helsinki.fi   2009 11


if size(table.columnheads,2)~=size(table.data,2)
%    writeerror(cf,'Header and data column numbers do not match.');
    error('anduril:WrongSize','Header and data column numbers do not match.' );
    %return
end

%% replace NaNs to NA
table.data=cellfun(@(x) regexprep(x, '^NaN$', 'NA'), table.data,'UniformOutput', false); 

%% write to file, not an efficient way, but matlab cell arrays are not my
% thing..  
cell2csv(filename,[table.columnheads; table.data],char(9));

