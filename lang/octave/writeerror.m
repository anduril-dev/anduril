function writeerror(cf, string_in)
% function writeerror(cf, string_in)
% Writes a string to the error log.
%   cf        = pipeline commadfile properties (see commandfileparser)
%   string_in = the string to write to the error file

% ville.rantanen@helsinki.fi   2008 10

constants; % read the stringlistseparator

logfile=getvariable(cf,'output._errors');

% disp(['writing: ' string_in]);
fid=fopen(logfile, 'at');
fprintf(fid, string_in);
fprintf(fid, ['\n' stringlistseparator '\n'] );

fclose(fid);