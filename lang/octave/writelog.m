function writelog(cf, string_in)
% function writelog(cf, string_in)
% Writes a string to the pipeline log.
%   cf        = pipeline commadfile properties (see commandfileparser)
%   string_in = the string to write to the pipeline log

% ville.rantanen@helsinki.fi   2008 10

constants;  % get the stringlistseparator

logfile=getvariable(cf,'output._log');

fid=fopen(logfile, 'at');
fprintf(fid, string_in);
fprintf(fid, ['\n' stringlistseparator '\n'] );

fclose(fid);
