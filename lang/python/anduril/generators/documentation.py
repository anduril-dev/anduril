import textwrap

def component_markdown(component, short, bundle=None):
    """ Print component documentation with markdown syntax
        
        returns string
        
        args:
            - component   Anduril ComponentFile object 
            - short       Boolean, if True, returns short listing
            - bundle      optional string, written as Bundle name
    
    """
    output_rows=[]
    output_rows.append("# {:^40}".format(component.get_name()))
    if not short: 
        output_rows.append("\n* Version: {:}".format(component.get_version()))
        output_rows.append(u"* Author: {:} <{:}>".format(*component.get_author()))
        output_rows.append("* Category: {:}".format(", ".join(component.get_category())))
        if bundle!=None:
            output_rows.append("* Bundle: {:}".format(bundle))
        output_rows.append("* Launcher: {:}".format(component.get_launcher().title()))
        output_rows.append("\n"+component.get_doc())
    output_rows.append("\n## {:^20}\n".format("Inputs"))    
    for input in sorted(component.get_inputs()):
        fmtstr="* {:<30} *{:}*"
        if component.input_is_array(input): fmtstr="* {:<30} *Array<{:}>*"
        if component.input_is_optional(input): fmtstr+=" (opt)"
        output_rows.append(fmtstr.format(input, component.get_input_type(input)))
        if not short:
            output_rows.append("    * "+"\n      ".join(textwrap.wrap(component.get_input_doc(input),70)))

    output_rows.append("\n## {:^20}\n".format("Parameters"))
    for parameter in sorted(component.get_parameters()):
        fmtstr="* {:<30} *{:<10} {:<20}"
        if component.get_parameter_default(parameter)==None:
            default="[]"
        else:
            default=component.get_parameter_default(parameter)
            if component.get_parameter_type(parameter)=="string":
                default='["'+default+'"]'
            else:
                default='['+default+']'
        output_rows.append(fmtstr.format(parameter, component.get_parameter_type(parameter)+"*", default))#"["+component.get_parameter_default(parameter)+"]"))
        if not short:
            output_rows.append("    * "+"\n      ".join(textwrap.wrap(component.get_parameter_doc(parameter),70)))

    output_rows.append("\n## {:^20}\n".format("Outputs"))    
    for output in sorted(component.get_outputs()):
        fmtstr="* {:<30} *{:}*"
        if component.output_is_array(output): fmtstr="* {:<30} *Array<{:}>*"
        output_rows.append(fmtstr.format(output, component.get_output_type(output)))
        if not short:
            output_rows.append("    * "+"\n      ".join(textwrap.wrap(component.get_output_doc(output),70)))
    return "\n".join(output_rows)
