import os,sys
from anduril.componentfile import ComponentFile as Component

def head_foot(name=""):
    """ Generate header and footer for an anduril script """
    return ( "object anduril"+name+" { \n", "\n}" )

def component_usage(component):
    """Return string of component example usage"""
    indentlen = 2
    items = []
    for i in component.get_inputs():
        fmtstr = "{:<15} = {:}"
        if component.input_is_array(i): fmtstr = "{:<15} = makeArray({:})"
        if component.input_is_optional(i): fmtstr="// "+fmtstr.replace("<15","<12")
        items.append(fmtstr.format(i, component.get_input_type(i)))
    for i in component.get_parameters():
        if component.get_parameter_default(i) == None:
            default = "<{s}>".format(s = component.get_parameter_type(i))
        else:
            default = component.get_parameter_default(i)
            if component.get_parameter_type(i) == "string":
                default = '"' + default + '"'
            if component.get_parameter_type(i) == "float":
                if "." not in default:
                    default = default + ".0"
        items.append("{:<15} = {:}".format(i, default))
    return component.get_name() + \
           "(\n{s:{l}}".format(s = " ", l = indentlen) + \
           ",\n{s:{l}}".format(s = " ", l = indentlen).join(items) + \
           "\n)"

def copy_to_clipboard(s):
    """Use command `xclip` to copy string to clip board"""
    os.system("echo '%s' | xclip 2> /dev/null" % s)

def code_input_replace(code,repository,bundle=None):
    """ Replace input files with a INPUT component instance """
    name=code.split("(",1)[0]
    (component,bundle)=repository.get_component(name,bundle=None)
    inputs=[]
    args=[]
    code=")".join(code.split("(",1)[1].split(")")[:-1])
    for arg in code.split(","):
        keyValue=arg.strip().split("=",1)
        keyValue=[ x.strip() for x in keyValue ]
        if len(keyValue)>1:
            if keyValue[0] in component.get_inputs():
                inputs.append("val {:} = INPUT(path=\"{:}\")".format(keyValue[0], os.path.abspath(keyValue[1])))
                keyValue[1]=keyValue[0]
        args.append(" = ".join(keyValue))
    header=["import anduril."+bundle+"._"]
    if bundle != "builtin":
        header.append("import anduril.builtin._")
    header.append("object runComponent"+name+" { \n")
    footer=") }"
    return "\n".join(header)+"\n".join(inputs)+"\nval component="+name+"("+",".join(args)+footer


def testcase(bundle, component, testcase):
    """ Return Anduril script to run a testcase for a component.

    example:
        - repository=Repository.from_env()
        - bundle=repository.get_bundle("my_bundle_name")
        - print(testcase(bundle, "component_name", "case1"))

    """
    b=bundle.get_name()
    comp=bundle.get_component(component)
    c=comp.get_name()
    case=comp.get_testcases().get_case(testcase)
    s=[]
    for i in sorted(case['inputs']):
        s.append("__".join([ b,c,testcase,'input',i] )+" = anduril.builtin.INPUT(path=\""+case['inputs'][i]+"\")")
    s.append("__".join( [b,c,testcase] )+"="+".".join(["anduril",b,c])+"(")
    s=["    val "+x for x in s]
    sp=[]
    for i in sorted(case['inputs']):
        sp.append("{:12}{:<12} = {:}.out".format(" ",i,"__".join([ b,c,testcase,'input',i])))
    for p in sorted(case['parameters']):
        if comp.get_parameter_type(p)=="string":
            sp.append("{:12}{:<12} = \"{:}\"".format(" ",p,case['parameters'][p].replace('"','\\"')))
        else:
            sp.append("{:12}{:<12} = {:}".format(" ",p,case['parameters'][p]))

    sp=",\n".join(sp)

    s="\n".join(s)+"\n"+sp+"\n    )"
    op=["    "]
    for o in sorted(case['outputs']):
        if comp.output_is_array(o):
            op.append("    val {:}=anduril.testsystem.CompareExpectedArray(".format("__".join([b,c,testcase,'output',o])))
        else:
            op.append("    val {:}=anduril.testsystem.CompareExpected(".format("__".join([b,c,testcase,'output',o])))
        op.append("{:12}expected=anduril.builtin.INPUT(path=\"{:}\"),".format(" ",case['outputs'][o]))
        op.append("{:12}in      ={:}.{:}\n    )".format(" ","__".join([b,c,testcase]),o ))


    return s+"\n".join(op)

def validate_testcases(bundle_name=None):
    """ Return a list of broken test cases.
        Creates the bundle repository from env (ANDURIL_BUNDLES) """

    import anduril.bundlerepository
    repo=anduril.bundlerepository.Repository.from_env()
    invalids=[]
    if bundle_name:
        bundles=[bundle_name]
    else:
        bundles=repo.get_bundle_names()
    for b in bundles:
        bundle=repo.get_bundle(b)
        components=bundle.get_components()
        for c in sorted(components):
            if not components[c].get_testcases().get_names():
                continue
            for case in components[c].get_testcases().get_names():
                valid=components[c].get_testcases().is_valid(case)
                if not valid[0]:
                    invalids.extend(valid[1])

    return invalids

