
import os, anduril

''' _state file object model '''

class NetworkNode:
    def __init__(self,row,path):
        self.row=row
        self.path=path
        self.outputnames=[]
        self.inputs=[]
        self.inputnames=[]
        self.children=[]
        self.parents=[]
        self.parameters={}
        self.disabled=False
        self.version=''
        self.cf=False
        self.parserow()
        self.outputsizes=False
        self.inputsizes=False
    
    def parserow(self):
        value=self.row.strip()
        unescaped = "#!=:tnr\\"
        replace_escaped = "#!=:\t\n\r\\"
        escaped = ["\\"+x for x in unescaped]
        new_value = ""
        while len(value):
            if len(value)==1:
                new_value+=value[0]
                break
            if value[0:2] in escaped:
                new_value+=replace_escaped[escaped.index(value[0:2])]
                value=value[2:]
            else:
                new_value+=value[0]
                value=value[1:]
        value = new_value
        rowlist=value.split('	')
        self.name=rowlist[0]
        self.disabled=rowlist[2]=='DISABLED'
        arguments=rowlist[3].split(' ')
        self.component=arguments[0]
        for argpos in range(len(arguments)):
            if (arguments[argpos] == 'IN'):
                argin=arguments[argpos+1].split('=',1)
                self.inputnames.append(argin[0])
                self.inputs.append(argin[1])
            if (arguments[argpos] == 'OUT'):
                self.outputnames.append(arguments[argpos+1])
            if (arguments[argpos] == 'VER'):
                self.version=arguments[argpos+1]
            if (arguments[argpos] == 'P'):
                break
        parameters=rowlist[3].split(' P ')
        for parpos in range(len(parameters)-1):
            argin=parameters[parpos+1].split('=',1)
            self.parameters[argin[0]]=argin[1]
        return

    def get_disabled(self):
        return self.disabled
    
    def get_version(self):
        return self.version

    def get_path(self):
        return self.path
        
    def get_outputnames(self):
        return self.outputnames
        
    def get_inputs(self):
        return self.inputs
    
    def get_input_instances(self):
        return [i.split(':')[0] for i in self.inputs]
    
    def get_input_values(self):
        return [i.split(':')[1] for i in self.inputs]
    
    def get_inputnames(self):
        return self.inputnames
        
    def get_name(self):
        return self.name
        
    def get_component(self):
        return self.component
    
    def get_parameters(self):
        keys=self.parameters.keys()
        keys.sort()
        indent=len(self.name)+3
        if indent>20:
            indent=20
        strout=""
        for key in keys:
            if len(self.parameters[key])>0:
                strout+=' '*(indent)+key+'='+self.parameters[key]+"\n"
        return strout
    
    def get_parameter_dict(self):
        return self.parameters
    
    def set_cf(self):
        filename=os.path.join(self.path,self.name,'_command')
        if not os.path.exists(filename):
            self.cf=False
        else:
            self.cf=anduril.CommandFile()
            self.cf=self.cf.from_file(filename)
    
    def get_input(self,name):
        if not self.cf:
            self.set_cf()
            if not self.cf:
                return ""
        return self.cf.get_input(name)
    
    def get_output(self,name):
        if self.component=='anduril.builtin.INPUT':
            return self.parameters['path'].split(' TS ',1)[0]
        if self.component=='anduril.builtin.URLInput':
            return self.parameters['url']
        if self.component=='anduril.builtin.OUTPUT':
            return os.path.join(self.path,'output')
        if not self.cf:
            self.set_cf()
            if not self.cf:
                return ""
        return self.cf.get_output(name)

    def set_port_sizes(self):
        self.set_input_sizes()
        self.set_output_sizes()
    
    def set_input_sizes(self):
        self.inputsizes=[self.get_port_size(self.get_input(i)) for i in self.get_inputnames()]
        
    def set_output_sizes(self):
        self.outputsizes=[self.get_port_size(self.get_output(i)) for i in self.get_outputnames()]
    
    def get_input_size(self,name,numeric=False):
        if not self.inputsizes:
            self.set_input_sizes()
        index=self.get_inputnames().index(name)
        value=self.inputsizes[index]
        if numeric:
            return value
        else:
            return humanize_size(value)
    
    def get_output_size(self,name,numeric=False):
        if not self.outputsizes:
            self.set_output_sizes()
        index=self.get_outputnames().index(name)
        value=self.outputsizes[index]
        if numeric:
            if value==None:
                value=0
            return value
        else:
            return humanize_size(value)
    
    def get_port_size(self,path):
        if path==None or (not os.path.exists(path)):
            return None
        try:
            size=0
            if os.path.isdir(path):
                for dirfile in os.listdir(path):
                    size+=os.path.getsize(os.path.join(path,dirfile))
            else:
                size=os.path.getsize(path)
            return size
        except:
            return None
    
    def get_logfile(self):
        if not self.cf:
            self.set_cf()
            if not self.cf:
                return False
        logfilename=self.cf.get_output(anduril.constants.LOG_PORT)
        if os.path.exists(logfilename):
            return logfilename
        else:
            return False
    
    def get_errorfile(self):
        if not self.cf:
            self.set_cf()
            if not self.cf:
                return False
        errfilename=self.cf.get_output(anduril.constants.ERRORS_PORT)
        if os.path.exists(errfilename):
            return errfilename
        else:
            return False
        
    def get_children(self):
        return self.children
    
    def get_parents(self):
        return self.parents
    
    def get_parents_list(self,others):
        parentlist=[]
        allinstances=[i.get_name() for i in others]
        self.set_parents(others)
        parentlist.extend(self.get_parents())
        myparents=self.get_parents()
        if len(myparents)>0:
            for p in myparents:
                pidx=allinstances.index(p[0])
                pparents=others[pidx].get_parents_list(others)
                parentlist.extend(pparents)
        
        return parentlist
        
    
    def delete_family(self):
        self.children=[]
        self.parents=[]
        
    def set_family(self,others):
        self.delete_family()
        self.set_children(others)
        self.set_parents(others)
    
    def set_children(self,others):
        #if len(self.children)>0:
        #    return
        ch=[]
        allinstances=[i.get_name() for i in others]
        for inst in others:
            instances=inst.get_input_instances()
            ports=inst.get_input_values()
            inputs=inst.get_inputnames()
            if self.name in instances:
                selfidx=instances.index(self.name)
                ch.append((inst.get_name(),ports[selfidx],inputs[selfidx],inst.get_component()))
        ch.sort(key=i_sort)
        self.children=ch
    
    def set_parents(self,others):
        #if len(self.parents)>0:
        #    return
        pa=[]
        instances=self.get_input_instances()
        inputports=self.get_inputnames()
        ports=self.get_input_values()
        allinstances=[i.get_name() for i in others]
        p=0
        for parent in instances:
            if parent in allinstances:
                idx=allinstances.index(parent)
                #inputs=others[idx].get_outputnames()
                #print(self.get_name(),parent,ports[p],inputports[p])
                pa.append((parent,ports[p],inputports[p],others[idx].get_component()))
            p+=1
        self.parents=pa
        
    def set_stats(self):
        try:
            self.runtime
            return
        except AttributeError:
            pass
        errfile=self.get_errorfile()
        logfile=self.get_logfile()
        self.runtime=0
        self.endtime=False
        self.starttime=False
        self.suretime=False
        self.numfiles=0
        if self.cf==False:
            return
        commandfile=os.path.join(self.path,self.name,'_command')
        starttime=os.path.getmtime(commandfile)
        outfiles=[]
        outtimes=[]
        for output in self.outputnames:
            outfile=self.get_output(output)
            if outfile is None:
                continue
            if os.path.exists(outfile):
                outtimes.append(os.path.getmtime(outfile))
            if os.path.isdir(outfile):
                for dirfile in os.listdir(outfile):
                    outtimes.append(os.path.getmtime(os.path.join(outfile,dirfile)))
        self.numfiles=len(outtimes)
        if errfile:
            outtimes.append(os.path.getmtime(errfile))
        if logfile:
            outtimes.append(os.path.getmtime(logfile))
        self.starttime=int(starttime)
        if len(outtimes)==0:
            self.endtime=self.starttime
        else:
            self.endtime=int(max(outtimes))
            self.suretime=True
        self.runtime=self.endtime-self.starttime
        
    def get_runtime(self):
        return self.runtime
    def get_endtime(self):
        return self.endtime
    def get_starttime(self):
        return self.starttime
    def get_suretime(self):
        return self.suretime
        
    def get_numfiles(self):
        return self.numfiles

def i_sort(s):
    ''' Instace name sorter '''
    if s[0]=='_':
        return s[1:]
    else:
        return s

def humanize_size(size,precision=1):
    if size==None:
        return 'nan'
    suffixes=['B','KB','MB','GB','TB']
    suffixIndex = 0
    defPrecision=0
    while size > 1024:
        suffixIndex += 1 #increment the index of the suffix
        size = size/1024.0 #apply the division
        defPrecision=precision
    return "%.*f%s"%(defPrecision,size,suffixes[suffixIndex])
