import sys
import os
import re
import scipy.misc
from PIL import Image as pimg
import anduril.tifffile as tfff
import anduril.png as png
import itertools
# ~ import component_skeleton.main
import numpy as n
import warnings

def get_imagelist(directory):
     """Return the filenames of images in a directory as a list.
     Image formats not known are removed (based on the extension.)
     """
     imglist=[]
     accepted='tif$|jpg$|jpeg$|tiff$|png$|gif$'
     for filename in os.listdir(directory):
          imtype=re.search(accepted,filename.lower())
          if imtype:
               imglist.append(filename)
     imglist.sort()
     return imglist

def load_image(path,flatten=0,keepalpha=False,keeptype=False):
     """ Load image and return as array, float32 type, scaled 0-1.
         Tries to handle different image formats...
         When flatten=1, grayscale is forced.
         keepalpha = keep alpha channel
         keeptype  = keep original bit depth
     """
     extension=path.split('.')
     if re.search('jpg$|jpeg$|gif$',extension[-1].lower()):

               im = pimg.open(path)
               origmode=im.mode
               im=scipy.misc.fromimage(im,flatten=flatten)

               if not keeptype:
                   if [origmode in ['1','RGB','RGBA','L']]:
                       im=im.astype('float')/255
                   if origmode=='I':
                       im=im.astype('float')/65535

               if len(im.shape)>2:
                    if (im.shape[2]==2 and not keepalpha):
                         im=im[:,:,0]
                    if (im.shape[2]==4 and not keepalpha):
                         im=im[:,:,0:2]

     if re.search('tif$|tiff$',extension[-1].lower()):

               tif = tfff.TIFFfile(path)
               im = tif.asarray(squeeze=True,rgbonly=False)
               # currently we rid of the alpha channel.
               if not keeptype:
                   if im.dtype == 'uint16':
                       im = (im.astype('float'))/65535
                   if im.dtype == 'uint8':
                       im = (im.astype('float'))/255
               if len(im.shape)==3:
                    row_count,column_count,plane_count=im.shape
               else:
                    row_count,column_count=im.shape
                    plane_count=1
               tif.close()

               if flatten:
                    if plane_count==3:
                         im=im.mean(2)
                    if plane_count==4:
                         if keepalpha:
                              imt=n.zeros((row_count,column_count,2))
                              imt[:,:,0]=im[:,:,0:2].mean(2)
                              imt[:,:,1]=im[:,:,3]
                              im=imt
                         else:
                              im=im[:,:,0:2].mean(2)
               if not keepalpha:
                    if plane_count==2:
                         im=im[:,:,0]
                    if plane_count==4:
                         if len(im.shape)==3:
                              im=im[:,:,0:2]

     if re.search('png$',extension[-1].lower()):
          #try:
               pngReader=png.Reader(filename=path)
               row_count, column_count, pngdata, meta = pngReader.asDirect()
               bitdepth=meta['bitdepth']
               plane_count=meta['planes']
               #print bitdepth
               #print plane_count
               if bitdepth==16:
                    image_2d = n.vstack(itertools.imap(n.uint16, pngdata))
               if (bitdepth==8 or bitdepth==1):
                    image_2d = n.vstack(itertools.imap(n.uint8, pngdata))
               if plane_count>1:
                    im = n.reshape(image_2d,
                         (column_count,row_count,plane_count))
               else:
                    im = n.reshape(image_2d,
                         (column_count,row_count))
               if not keeptype:
                   if bitdepth==16:
                        im=im.astype('float')/65535
                   if (bitdepth==8):
                        im=im.astype('float')/255
                   if bitdepth==1:
                        im=im.astype('float')
               if flatten:
                    if plane_count==3:
                         im=im.mean(2)
                    if plane_count==4:
                         if keepalpha:
                              imt=n.zeros((row_count,column_count,2))
                              imt[:,:,0]=im[:,:,0:2].mean(2)
                              imt[:,:,1]=im[:,:,3]
                              im=imt
                         else:
                              im=im[:,:,0:2].mean(2)
               if not keepalpha:
                    if plane_count==2:
                         im=im[:,:,0]
                    if plane_count==4:
                         if len(im.shape)==3:
                              im=im[:,:,0:2]

          #except:
          #     print "Unexpected error:", sys.exc_info()[0]
          #     return

     return im

def save_png(name,arr,grayscale=True,alpha=False,bitdepth=16):
     """ save array as png.
         Returns 1 when file could not be saved.
     """
     warnings.filterwarnings('ignore')#,'.*',DeprecationWarning,'png')
     try:
          if arr.ndim==2:
               row_count, column_count = arr.shape
               planes=1
          if arr.ndim==3:
               row_count, column_count, planes = arr.shape

          pngfile = open(name, 'wb')
          try:
               pngWriter = png.Writer(column_count, row_count,
                                     greyscale=grayscale,
                                     alpha=alpha,
                                     bitdepth=bitdepth)

               pngWriter.write(pngfile,
                              n.reshape(arr, (-1, column_count*planes)))

          finally:
               pngfile.close()
     except:
          return 1

     return 0
