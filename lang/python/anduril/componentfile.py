import xml.etree.ElementTree as ET
import os,sys
class ComponentFile:
    """A class representing component.xml files.
    """
    def __init__(self,filename):

        try:
            tree = ET.parse(filename)
        except Exception as e:
            raise Exception("Error opening %s: %s" % (filename, e))
        self._name=tree.find('name').text
        self._doc=self._finddoc(tree.getroot())

        self._path=os.path.dirname(filename)
        self._version=self._findpass(tree.find("version"))
        self._author=(self._findpass(tree.find("author")),self._findpass(tree.find("author"),a="email"))
        self._category=[c.text for c in tree.findall("category")]
        self._launcher=self._findpass(tree.find("launcher"),a="type")
        if self._launcher==None:
            self._launcher='function'
        self._launcher_attribs = []
        for launcher in tree.findall("launcher"):
            for arg in launcher.findall("argument"):
                self._launcher_attribs.append((arg.attrib['name'],arg.attrib['value']))
        self._parameters=[]
        self._parameterTypes={}
        self._parameterDoc={}
        self._parameterDefault={}
        for elem in tree.iter('parameter'): #.iter in Python 2.7
            self._parameterTypes[elem.attrib["name"]]=elem.attrib["type"]
            self._parameterDoc[elem.attrib["name"]]=self._finddoc(elem)
            self._parameterDefault[elem.attrib["name"]]=self._findpass(elem,a="default")
            self._parameters.append(elem.attrib["name"])
        self._inputs=[]
        self._inputIsArray={}
        self._inputIsOptional={}
        self._inputType={}
        self._inputDoc={}
        for elem in tree.iter('input'): #.iter in Python 2.7
            if "array" in elem.attrib:
                if elem.attrib["array"]=="false":
                    self._inputIsArray[elem.attrib["name"]]=False
                elif elem.attrib["array"]=="true":
                    self._inputIsArray[elem.attrib["name"]]=True
                elif elem.attrib["array"]=="generic": #generics set to false
                    self._inputIsArray[elem.attrib["name"]]=False
                else:
                    raise Exception("Invalid value for attribute 'array' for input '"+elem.attrib["name"]+"': '"+elem.attrib["array"]+"'")
            else:
                self._inputIsArray[elem.attrib["name"]]=False #default value is false
            if "optional" in elem.attrib:
                if elem.attrib["optional"]=="false":
                    self._inputIsOptional[elem.attrib["name"]]=False
                elif elem.attrib["optional"]=="true":
                    self._inputIsOptional[elem.attrib["name"]]=True
                else:
                    raise Exception("Invalid value for attribute 'optinal' for input '"+elem.attrib["name"]+"': '"+elem.attrib["optional"]+"'")
            else:
                self._inputIsOptional[elem.attrib["name"]]=False
            self._inputType[elem.attrib["name"]]=elem.attrib["type"]
            self._inputDoc[elem.attrib["name"]]=self._finddoc(elem)
            self._inputs.append(elem.attrib["name"])

        self._outputs=[]
        self._outputIsArray={}
        self._outputDoc={}
        self._outputType={}
        for elem in tree.iter('output'): #.iter in Python 2.7
            if "array" in elem.attrib:
                if elem.attrib["array"]=="false":
                    self._outputIsArray[elem.attrib["name"]]=False
                elif elem.attrib["array"]=="true":
                    self._outputIsArray[elem.attrib["name"]]=True
                elif elem.attrib["array"]=="generic": #generics set to false
                    self._outputIsArray[elem.attrib["name"]]=False
                else:
                    raise Exception("Invalid value for attribute 'array' for output '"+elem.attrib["name"]+"': '"+elem.attrib["array"]+"'")
            else:
                self._outputIsArray[elem.attrib["name"]]=False #default value is false
            self._outputType[elem.attrib["name"]]=elem.attrib["type"]
            self._outputDoc[elem.attrib["name"]]=self._finddoc(elem)
            self._outputs.append(elem.attrib["name"])

        self._testcases=Testcases(filename,self)
        self._requirements=[]
        for r in tree.findall("requires"):
            self._requirements.append((
                                r.text,
                                self._findpass(r,a="type"),
                                self._findpass(r,a="optional"),
                                self._findpass(r,a="name"),
                                self._findpass(r.find("resource")),
                                self._findpass(r.find("resource"), a="type"),
                                ))

    def _findpass(self,e,a=False):
        """ Try to return an xml element text, or its attribute 'a', return empty string if not found """
        try:
            if a: return e.attrib[a]
            return e.text
        except AttributeError: return None
        except KeyError: return None

    def _finddoc(self,e):
        """ Try to return a full doc text, if found """
        if len(list(e))>0:
            if 'doc' in [x.tag for x in list(e)]:
                return "\n".join([r.strip() for r in ET.tostring(e.find('doc'),
                              method="text",encoding="utf-8").decode("utf-8").split("\n")]).rstrip()
        return ""

    def get_parameter_type(self,parameter):
        if parameter in self._parameterTypes:
            return self._parameterTypes[parameter]
        if parameter not in self._parameters:
            raise Exception("Component.xml doesn't contain parameter named '"+str(parameter)+"'")

    def output_is_array(self,output):
        if output in self._outputIsArray:
            return self._outputIsArray[output]
        else:
            raise Exception("Component.xml doesn't contain output named '"+str(output)+"'")

    def input_is_array(self,input):
        if input in self._inputIsArray:
            return self._inputIsArray[input]
        else:
            raise Exception("Component.xml doesn't contain input named '"+str(input)+"'")

    def input_is_optional(self,input):
        if input in self._inputIsOptional:
            return self._inputIsOptional[input]
        else:
            raise Exception("Component.xml doesn't contain input named '"+str(input)+"'")

    def iter_inputs(self):
        for inp in self._inputs:
            yield inp

    def iter_outputs(self):
        for output in self._outputs:
            yield output

    def iter_parameters(self):
        for parameter in self._parameters:
            yield parameter

    def get_author(self):
        return self._author

    def get_category(self):
        return self._category

    def get_name(self):
        return self._name

    def get_inputs(self):
        return self._inputs

    def get_input_doc(self, input):
        if input in self._inputDoc:
            return self._inputDoc[input]
        else:
            return ""

    def get_input_type(self, input):
        if input in self._inputType:
            return self._inputType[input]
        else:
            return ""

    def get_outputs(self):
        return self._outputs

    def get_output_doc(self, output):
        if output in self._outputDoc:
            return self._outputDoc[output]
        else:
            return ""

    def get_output_type(self, output):
        if output in self._outputType:
            return self._outputType[output]
        else:
            return ""

    def get_parameters(self):
        return self._parameters

    def get_parameter_doc(self, parameter):
        if parameter in self._parameterDoc:
            return self._parameterDoc[parameter]
        else:
            return ""

    def get_parameter_default(self, parameter):
        if parameter in self._parameterDefault:
            return self._parameterDefault[parameter]
        else:
            return None

    def get_doc(self):
        return self._doc

    def get_documentation(self):
        s="Anduril component "+self.get_name()+".\n\n"
        s+=self.get_doc()
        return self._doc

    def get_launcher(self):
        return self._launcher

    def get_launcher_argument(self, name):
        for arg in self._launcher_attribs:
            if arg[0] == name:
                return arg[1]
        return None

    def get_path(self):
        return self._path

    def get_requirements(self):
        return self._requirements

    def get_testcases(self):
        return self._testcases

    def get_version(self):
        return self._version

    def is_function(self):
        return self._launcher=='function'

class Testcases:
    """ class that lazily returns test cases """
    def __init__(self, filename,component):
        parent=os.path.dirname(filename)
        name=os.path.basename(parent)
        self._path=os.path.join(parent,'testcases')
        self._test_network=os.path.abspath( os.path.join( parent,"..","..","test-networks", name, "network.scala" ) )
        self._cases=None
        self._component=component

    def _valid_ports(self, folder,port_type):
        if port_type=="input":
            for f in os.listdir( os.path.join(folder, 'input')):
                if os.path.splitext(f)[0] in self._component.get_inputs():
                    yield (os.path.splitext(f)[0], os.path.join(folder,'input',f))
        if port_type=="output":
            for f in os.listdir( os.path.join(folder, 'expected-output')):
                if os.path.splitext(f)[0] in self._component.get_outputs():
                    yield (os.path.splitext(f)[0], os.path.join(folder,'expected-output',f))

    def get_names(self):
        if self._cases==None:
            if os.path.isdir( self._path ):
                self._cases=[ d for d in sorted(os.listdir( self._path )) if not d==".hidden"]
        return self._cases

    def get_case(self,name):
        self.get_names()
        if name not in self._cases:
            raise Exception("No testcase named '"+str(name)+"'")
        case={}
        case['inputs']={}
        case['parameters']={}
        case['metadata']={}
        case['outputs']={}
        if os.path.isdir(os.path.join(self._path,name,'input')):
            case['inputs'].update([(p,f) for (p,f) in self._valid_ports(os.path.join(self._path,name),'input')])
        if os.path.isdir(os.path.join(self._path,name,'expected-output')):
            case['outputs'].update([(p,f) for (p,f) in self._valid_ports(os.path.join(self._path,name),'output')])
        case['fail']=os.path.isfile(os.path.join(self._path,name,'failure'))
        case['disabled']=False
        if os.path.isfile(os.path.join(self._path,name,'component.properties')):
            with open(os.path.join(self._path,name,'component.properties'), 'r') as f:
                for l in f:
                    if "=" not in l:
                        continue
                    key_value=l.split(".",1)
                    if len(key_value)==1:
                        case['parameters']["invalid_entry"]=key_value
                        continue
                    (key,value)=l.split(".",1)[1].split("=",1)
                    key=key.strip()
                    value=value.rstrip("\n\r").lstrip(" ")
                    if l.startswith("component"):
                        case['parameters'][key]=value
                    if l.startswith("metadata"):
                        case['metadata'][key]=value
        if 'timeout' in case['metadata']:
            if case['metadata']['timeout']=="0":
                case['disabled']=True

        return case

    def get_test_network(self):

        if os.path.isfile( self._test_network ):
            return self._test_network
        else:
            return None

    def is_valid(self,name):
        case=self.get_case(name)
        is_valid=True
        in_files=[]
        out_files=[]
        if os.path.isdir(os.path.join(self._path,name,'input')):
            in_files=[ f for f in os.listdir( os.path.join(self._path,name, 'input')) if not f.startswith(".") ]
        if os.path.isdir(os.path.join(self._path,name,'expected-output')):
            out_files=[ f for f in os.listdir( os.path.join(self._path,name, 'expected-output')) if not f.startswith(".") ]
            out_files=[ f for f in out_files if not f.startswith("_") ]
        errors=[]
        for i in in_files:
            i_p=os.path.splitext(i)[0]
            if i_p not in (self._component.get_inputs()):
                errors.append("Found incorrect input: %s.%s.%s"% (self._component.get_name(), name, i_p))
                is_valid=False
        for p in case['parameters']:
            if p not in self._component.get_parameters():
                errors.append("Found incorrect parameter: %s.%s.%s"% (self._component.get_name(), name, p))
                is_valid=False
        for o in out_files:
            o_p=os.path.splitext(o)[0]
            if o_p not in (self._component.get_outputs()):
                errors.append("Found incorrect output: %s.%s.%s"% (self._component.get_name(), name, o_p))
                is_valid=False
        if case['fail']:
            if os.path.isdir(os.path.join(self._path,name,'expected-output')):
                errors.append("Expecting failure, but found expected-output %s.%s"% (self._component.get_name(), name))
                is_valid=False
        return (is_valid, errors)



