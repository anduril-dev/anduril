import types
if "StringTypes" not in dir(types): types.StringTypes=(str,)
import os,sys,re
from anduril import constants

from anduril.utils import *
from anduril.componentfile import ComponentFile as Component

__all__=["Repository","Bundle"]

class Repository:
    """Provides access to the bundles."""

    def __init__(self):
        """Create an empty command file instance. Usually,
        an instance is created with the from_env function.
        """
        self._bundles = {}

    def get_bundle(self, name):
        """Return the Bundle corresponding to given name,
        or None if the bundle does not exist."""
        if type(name) not in types.StringTypes:
            raise TypeError('name must be a string')
        if name not in self._bundles:
            raise Exception('No such bundle: '+name)
        return self._bundles.get(name)

    def get_bundles(self):
        """Return the Bundles."""
        return self._bundles.values()

    def get_bundle_names(self):
        """Return the Bundle names as a list."""
        return self._bundles.keys()

    def set_bundle(self, name, bundle):
        """Set a new bundle in the Repository."""
        if type(name) not in types.StringTypes:
            raise TypeError('name must be a string')
        self._bundles[name] = bundle

    def get_component(self, name, bundle=None):
        """Return component and the bundle it is found in.
           If multiple components with same name, first returned.
           Help search by providing bundle name."""
        if type(name) not in types.StringTypes:
            raise TypeError('name must be a string')
        if type(bundle) not in types.StringTypes and bundle is not None:
            raise TypeError('bundle must be a string')
        for b in self._bundles:
            if bundle!=None and b!=bundle:
                continue
            bundle_object=self.get_bundle(b)
            for component in bundle_object.get_components():
                if component==name:
                    return (bundle_object.get_component(component),b)
        raise Exception("Component %s not found" % name)

    def get_components(self, name):
        """Return the components of a named bundle."""
        if type(name) not in types.StringTypes:
            raise TypeError('name must be a string')
        return self._bundles[name].get_components()

    def get_all_component_names(self):
        """Return all components as a list of bundle and component."""
        components=[]
        for bundle in sorted(self._bundles):
            components.extend([(bundle,c) for c in sorted(self._bundles[bundle].get_components())])
        return components

    def search_component(self, key):
        """Return a list of tuples of Components with match type."""
        matches=[]
        searcher=re.compile(key, re.IGNORECASE)
        for bundle in self._bundles:
            bundle_object=self.get_bundle(bundle)
            components=bundle_object.get_components()
            for c in components:
                if searcher.search( components[c].get_name() ):
                    matches.append((c,'name'))
                else:
                    if searcher.search( components[c].get_doc() ):
                        matches.append((c,'docs'))

        return matches

    def search_component_close_match(self, key):
        """ Return a list of Components whose name closely matches key """
        import difflib as dl
        match=[]
        for comp in self.get_all_component_names():
            s=dl.SequenceMatcher(None, comp[1].lower(), key.lower())
            s_ratio=s.ratio()
            if s_ratio > 0:
                match.append(( s_ratio, comp[1] ))
        match.sort(key=lambda x:x[0], reverse=True)
        best_matches=[x[1] for x in match[0:5]]
        return best_matches

    @classmethod
    def from_env(cls):
        """Parse environment and return a corresponding
        Repository instance.
        Raise ValueError if there are errors in the Bundles.
        """

        repo = Repository()
        path_list=os.getenv("ANDURIL_BUNDLES",
                            os.path.join(os.getenv("ANDURIL_HOME",""),"bundles")
                           ).split(":")
        for location in path_list:
            if not os.path.isdir(location):
                continue
            for folder in os.listdir(location):
                if os.path.isfile( os.path.join(location, folder, 'bundle.xml') ):
                    bundle=Bundle.from_folder(os.path.join(location, folder))
                    repo.set_bundle( bundle.get_name(), bundle )
        return repo

class Bundle:
    """Provides access to the components."""

    def __init__(self):
        """Create an empty command file instance. Usually,
        an instance is created with the from_env function.
        """
        self._components = {}
        self._functions = {}
        self._name = ""
        self._path = ""
        self._depends = []

    def get_name(self):
        """ return bundle name """
        return self._name

    def set_name(self, name):
        """ set bundle name """
        if type(name) not in types.StringTypes:
            raise TypeError('name must be a string')
        self._name=name

    def get_path(self):
        """ return bundle path """
        return self._path

    def set_path(self, path):
        """ set bundle name """
        if type(path) not in types.StringTypes:
            raise TypeError('path must be a string')
        self._path=path

    def get_component(self, name):
        """Return the Component corresponding to given name,
        or None if the Component does not exist."""
        if type(name) not in types.StringTypes:
            raise TypeError('name must be a string')
        return self._components[name]

    def set_component(self, component):
        """Set a new component in the Bundle."""

        self._components[component.get_name()]=component

    def get_components(self):
        """Return the components."""
        return self._components

    def search_component(self, key):
        """Return a list of tuples of Components with match type."""
        matches=[]
        searcher=re.compile(key, re.IGNORECASE)
        for c in sorted(self._components):
            if searcher.search( c ):
                matches.append((c,'name'))
            else:
                if searcher.search( self._components[c].get_doc() ):
                    matches.append((c,'docs'))
        return matches

    def set_depends(self, name):
        """ Add a dependency """
        if name not in self._depends:
            self._depends.append(name)
    def get_depends(self):
        """ Get a list of dependencies """

        return self._depends

    @classmethod
    def from_folder(cls, path):
        """Parse environment and return a corresponding
        Repository instance.
        Raise ValueError if there are errors in the Bundles.
        """

        bundle = Bundle()
        bundle.set_path(path)
        import xml.etree.ElementTree as ET
        try:
            tree = ET.parse(os.path.join( path, "bundle.xml"))
        except Exception as e:
            raise Exception("Error opening %s: %s" % (path, e))
        for elem in tree.iter('name'):
            bundle.set_name(elem.text)
        for elem in tree.iter('depends'):
            bundle.set_depends(elem.text)

        if os.path.isdir(os.path.join( path, "components")):
            folder_list=os.listdir(os.path.join( path, "components"))
            for folder in folder_list:
                if os.path.isfile( os.path.join(path, "components", folder, 'component.xml') ):
                    bundle.set_component( Component(os.path.join(path, "components", folder, 'component.xml')) )
        if os.path.isdir(os.path.join( path, "functions")):
            folder_list=os.listdir(os.path.join( path, "functions"))
            for folder in folder_list:
                if os.path.isfile( os.path.join(path, "functions", folder, 'component.xml') ):
                    bundle.set_component( Component(os.path.join(path, "functions", folder, 'component.xml')) )
        return bundle





