#!/usr/bin/env python

from __future__ import print_function
from itertools import *

import os, errno, inspect

__all__=["memoize","trace","Struct","mkdir_p", "get_console_size", "install_requirements"]

import types
if "StringTypes" not in dir(types): types.StringTypes=(str,)
if "DictType" not in dir(types): types.DictType=dict
class Struct:
    """ Transforms a nested dictionary into an object hierarchy.
    Pass keyword arguments as **your_dictionary. You can reconstruct
    the dictionary with to_dict, and pretty printing should work
    automatically.

    Caveat: Dictionaries with keys that aren't valid variable names
    will throw.

    Example:
        >>> b = Struct(a=1, b={'a':1})
        >>> b.b.a
        1


    Constructor from: http://stackoverflow.com/questions/5290715/complex-transforming-nested-dictionaries-into-objects-in-python
    """
    def __init__(self, **entries):
        self.__dict__.update(entries)
        for k,v in self.__dict__.items():
            if type(v) == types.DictType:
                setattr(self, k, Struct(**v))

    def to_dict(self):
        """ Reconstruct the original dictionary. """
        d={}
        for k,v in self.__dict__.items():
            if isinstance(v, self.__class__):
                d[k] = v.to_dict()
            else:
                d[k] = v
        return d

    def __repr__(self):
        return "Struct: %r"%self.to_dict()

    def update(self, u):
        import collections
        """ Takes in a dictionary, and recursively updates this Struct as if it were an analoguous to a nested dictionary. Everything except nested Struct instances is overwritten. """
        for k, v in u.items():
            own=None if not hasattr(self, k) else getattr(self, k)
            if isinstance(v, collections.Mapping) and isinstance(own, self.__class__):
                own.update(v)
            else:
                setattr(self, k, u[k])

def memoize(f):
    """ Memoization decorator for a function taking one or more arguments by Martin Miller. From: http://code.activestate.com/recipes/578231-probably-the-fastest-memoization-decorator-in-the-/ , meaning it has MIT license? """
    class memodict(dict):
        def __getitem__(self, *key):
            return dict.__getitem__(self, key)

        def __missing__(self, key):
            ret = self[key] = f(*key)
            return ret
    return memodict().__getitem__

@memoize
def innertrace(*args, **kw):
    """ Needed to implement trace function because stack inspection doesn't work from decorators. """
    # Needs: from __future__ import print_function
    print(*args, **kw)
    return inspect.stack()[1][3]

def trace(*args, **kw):
    """ Behaves like the print(function, except never prints the same thing twice unless called from a different location. Prepends the calling function name. """
    return innertrace(inspect.stack()[1][3], *args, **kw)

def main():
    b = Struct(a=1, b={'a':1, })
    print(b.b.a)

    b = Struct(**{ 'LOL':set((12,)),'foo': {'bar': "baz", "l":4 }})
    print(b.foo.bar)
    print(b)

    b.update({'tiredof': ["this"], 'foo': {'hakan': 5, 'bar': { 'UUS': 6 } }})
    print("Updated", b)

    print(b.to_dict())
    #print(Struct(**b.to_dict()))

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def get_console_size():
    return os.popen('stty size', 'r').read().split()

def install_requirements(c,sudo=False,printOnly=False,optional=True):
    ''' Function that mimicks the Anduril1 InstallRequirements component.
    The input is a Component from anduril.componentFile module '''
    import subprocess
    def install_debian(c,r,s):
        return """ dpkg -s {r} 2>/dev/null | grep -q "Status:.*installed" || {s} apt-get install -y --allow-unauthenticated --no-install-recommends {r} """.format(
            r=r[0], s=s)
    def install_r(c,r,s):
        return """{s:} R --vanilla -q  <<< "
          if (try(require('{r}', quietly = TRUE))) {{{{ }}}} else {{{{
            print('Installing {r}')
            if ( version['major'] >= 3 && version['minor'] >= 5 ) {{
              if (requireNamespace('BiocManager')) {{{{ }}}} else {{{{
                install.packages('BiocManager', repos = 'http://cloud.r-project.org');
              }}}}
              BiocManager::install(
                '{r}',
                update = F,
                siteRepos = c('http://anduril.org/pub/R','http://r-forge.r-project.org/')
              );
           }} else {{
             source('http://bioconductor.org/biocLite.R')
             biocLite('{r}',siteRepos=c('http://anduril.org/pub/R','http://r-forge.r-project.org/'))
           }}
         }}}}" """.format(
            r = r[0],
            s = s
        )
    def install_make(c,r,s):
        print("Not Implemented Make")
        """ pushd "{p}"; {s} make {t} ; popd """.format(
             p=c.get_path(),
             s=s, t=r[4])
    def install_python(c,r,s):
        return """python -c "import {r}" || {s} pip install {r}""".format(
                r=r[0], s=s)
    def install_python3(c,r,s):
        return """python3 -c "import {r}" || {s} pip3 install {r}""".format(
                r=r[0], s=s)
    def install_bash(c,r,s):
        print("Not Implemented Bash")
    def install_component_provided(c,r,s):
        if r[5]=='ant':
            return """ pushd "{p}"; ant {t}; popd """.format(
             p=c.get_path(),
             t=r[4] if not r[4]==None else "")
        if r[5]=='bash':
            return """ pushd "{p}"; bash {f} ; popd """.format(
             p=c.get_path(),
             f=r[4])
        return ""

    req=c.get_requirements()
    s=""
    if sudo: s="sudo -i"
    print("# Component: {:}".format(c.get_name()))
    for r in req:
        isopt=r[2]=='true'
        reqName=r[0] if r[0]!=None else ""
        strType=r[5] if r[5]!=None else r[1]
        print("## Requirement: {:}, type: {:}, optional: {:}".format(
                    reqName.strip(),strType,isopt))
        if not optional:
            if isopt:
                continue
        cmd=""
        if r[1]=='R-bioconductor':
            cmd=install_r(c,r,s)
        if r[1]=='R-package':
            cmd=install_r(c,r,s)
        if r[1]=='DEB':
            cmd=install_debian(c,r,s)
        if r[1]=='make':
            cmd=install_make(c,r,s)
        if r[1]=='python':
            cmd=install_python(c,r,s)
        if r[1]=='python3':
            cmd=install_python3(c,r,s)
        if r[5]!=None:
            cmd=install_component_provided(c,r,s)
        if cmd!="":
            if printOnly:
                print(cmd)
            else:
                subprocess.call(cmd, shell=True, executable="/bin/bash")

    return req


if __name__=="__main__":
    main()

