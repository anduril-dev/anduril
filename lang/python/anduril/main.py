from __future__ import print_function
import sys

import anduril.commandfile
import anduril.constants

def main(execute_func):
    """Entry point for the component. execute_func is a function
    that implements the main functionality of the component. The
    function takes an instance of CommandFile as parameter and
    returns an integer exit code. Constants for exit codes are
    in the contants module.

    Example component:

        import anduril.main
        def execute(cf):
            some_input = cf.get_input('input_port')
            some_parameter = cf.get_parameter('some_param', 'float')
            cf.write_log('Log message')
            cf.write_error('Error message')
            # ...
            some_output = open(cf.get_output('some_output'), 'wt')
            # Write result to some_output
            return 0
        anduril.main(execute)
    """

    args = sys.argv
    if len(args) < 2:
        print >>sys.stderr, 'No command file given as a command line argument'
        sys.exit(constants.NO_COMMAND_FILE)

    try:
        cf = anduril.commandfile.CommandFile.from_file(args[1])
    except IOError as e:
        print >>sys.stderr, 'I/O error when reading command file: %s' % e
        sys.exit(constants.COMMAND_FILE_IO_ERROR)
    except ValueError as e:
        print >>sys.stderr, 'Parse error when reading command file: %s' % e
        sys.exit(constants.COMMAND_FILE_IO_ERROR)

#    try:
    status = execute_func(cf)
#    except:
#        e_type, e_value, e_traceback =  sys.exc_info()
#        print >>sys.stderr, 'Uncaught exception: %s' % e_value
#        sys.exit(constants.UNCAUGHT_EXCEPTION)

    if type(status) != int:
        print >>sys.stderr, 'execute_func did not return an integer return code; returned %s' % status
        sys.exit(constants.GENERIC_ERROR)
    sys.exit(status)


