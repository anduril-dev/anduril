import types
if "StringTypes" not in dir(types): types.StringTypes=(str,)
from os import mkdir
from anduril import constants
from anduril.utils import *

__all__=["CommandFile","string_record_to_dict"]

class CommandFile:
    """Provides access to the filenames of input and
    output ports as well as parameters values."""

    def __init__(self):
        """Create an empty command file instance. Usually,
        an instance is created with the from_file function.
        This, in turn, is usually done by the main module."""
        self._inputs = {} # {port name: file name}
        self._outputs = {} # {port name: file name}
        self._parameters = {} # {name: value}
        self._metadata = {} # {name: value}
        self.filename = None

    def get_input(self, port_name):
        """Return the filename corresponding to given input port,
        or None if the port does not exist or if the filename is not
        defined."""
        if type(port_name) not in types.StringTypes:
            raise TypeError('port_name must be a string')
        return self._inputs.get(port_name)

    def get_input_ports(self):
        """Return a list of input port names."""
        return self._inputs.keys()

    def get_input_array_index(self, port_name):
        """Return the filename corresponding to given input port index file,
        or None if the port does not exist or if the filename is not
        defined."""
        if type(port_name) not in types.StringTypes:
            raise TypeError('port_name must be a string')
        return self._inputs.get('_index_'+port_name)

    def set_input(self, port_name, filename):
        """Set the filename corresponding to given input port."""
        if type(port_name) not in types.StringTypes:
            raise TypeError('port_name must be a string')
        if not (filename is None or type(filename) in types.StringTypes):
            raise TypeError('filename must be a string or None')
        self._inputs[port_name] = filename

    def get_output(self, port_name):
        """Return the filename corresponding to given output port,
        or None if the port does not exist or if the filename is not
        defined."""
        if type(port_name) not in types.StringTypes:
            raise TypeError('port_name must be a string')
        return self._outputs.get(port_name)

    def get_output_ports(self):
        """Return a list of output port names."""
        return self._outputs.keys()

    def get_output_array_index(self, port_name):
        """Return the filename corresponding to given output port index file,
        or None if the port does not exist or if the filename is not
        defined."""
        if type(port_name) not in types.StringTypes:
            raise TypeError('port_name must be a string')
        return self._outputs.get('_index_'+port_name)

    def set_output(self, port_name, filename):
        """Set the filename corresponding to given output port."""
        if type(port_name) not in types.StringTypes:
            raise TypeError('port_name must be a string')
        if not (filename is None or type(filename) in types.StringTypes):
            raise TypeError('filename must be a string or None')
        self._outputs[port_name] = filename

    def get_parameter(self, param_name, param_type='string'):
        """Return the value of an parameter, or None if the parameter
        is not defined. For string parameters, the empty string is
        a possible output value. The type of the returned object
        depends on param_type. Legal values are "string", "int",
        "float" and "boolean". For example, if param_type is "boolean",
        the parameter is converted to a Python bool object.
        Raise ValueError if a numeric or Boolean conversion fails."""

        if type(param_name) not in types.StringTypes:
            raise TypeError('param_name must be a string')
        value = self._parameters.get(param_name)
        if param_type == 'string':
            return value
        elif param_type == 'int':
            return int(value, 10)
        elif param_type == 'float':
            return float(value)
        elif param_type == 'boolean':
            if value == 'true':
                return True
            elif value == 'false':
                return False
            else:
                raise ValueError('Cannot return parameter %s as boolean: invalid value: "%s"' % (param_name, value))
        else:
            raise ValueError('Invalid param_type: %s' % param_type)

    def get_parameter_list(self):
        """Return a list of parameter names."""
        return self._parameters.keys()

    def set_parameter(self, param_name, value):
        """Set the value of a parameter."""
        if type(param_name) not in types.StringTypes:
            raise TypeError('param_name must be a string')
        self._parameters[param_name] = value

    def get_metadata(self, metadata_name):
        """Return the value of a metadata item, or None if the item
        is not defined. The value is always a string, if defined."""
        if type(metadata_name) not in types.StringTypes:
            raise TypeError('metadata_name must be a string')
        return self._metadata.get(metadata_name)

    def get_tempdir(self):
        """Create a temporary directory or reuse one, and return the location as a string."""
        import os
        pathname = self.get_output(constants.TEMPDIR_PORT)
        mkdir_p(pathname)
        return self._outputs.get(constants.TEMPDIR_PORT)

    def set_metadata(self, metadata_name, value):
        """Set the value of a metadata item."""
        if type(metadata_name) not in types.StringTypes:
            raise TypeError('metadata_name must be a string')
        if not (value is None or type(value) in types.StringTypes):
            raise TypeError('value must be a string or None')
        self._metadata[metadata_name] = value

    def write_error(self, message):
        """Write a message to the error stream."""
        if not message.endswith('\n'):
            message += '\n'
        filename = self.get_output(constants.ERRORS_PORT)
        outfile = open(filename, 'at')
        outfile.write(message)
        if not message.endswith('\n'):
            outfile.write('\n')
        outfile.close()

    def write_log(self, message):
        """Write a message to the log stream."""
        if not message.endswith('\n'):
            message += '\n'
        filename = self.get_output(constants.LOG_PORT)
        outfile = open(filename, 'at')
        outfile.write(message)
        if not message.endswith('\n'):
            outfile.write('\n')
        outfile.close()


    def __str__(self):
        s=""
        for field_name,data in self._metadata.items():
            s+="metadata.%s=%s\n" % (field_name,data)
        for port,filename in self._inputs.items():
            s+="input.%s=%s\n" % (port,filename)
        for port,filename in self._outputs.items():
            s+="output.%s=%s\n" % (port,filename)
        for parameter_name,data in self._parameters.items():
            s+="parameter.%s=%s\n" % (parameter_name,data)

        return s

    def set_filename(self, filename):
        self.filename = filename

    def get_filename(self):
        return self.filename

    def write_to_file(self,filename = None):
        if not filename:
            filename == self.filename
        else:
            self.filename = filename
        thefile=open(filename,'w')
        thefile.write(str(self))
        thefile.close()

    @classmethod
    def from_file(cls, filename):
        """Parse a command file and return a corresponding
        CommandFile instance.
        Raise ValueError if there are errors in the command file.
        Raise IOError if the command file can not be read.
        Raise TypeError if filename is of incorrect type."""

        if type(filename) not in types.StringTypes:
            raise TypeError('filename must be a string')

        cf = CommandFile()
        cf.set_filename(filename)
        infile = open(filename, 'rt')
        for line in infile:
            line = line.strip()
            if not line:
                continue

            tokens = line.split('=', 1)
            if len(tokens) != 2:
                raise ValueError('Invalid line "%s"' % line)
            key, value = tokens
            value = value.strip()
            key_tokens = key.split('.')
            if len(key_tokens) < 2:
                raise ValueError('Invalid key "%s" on line "%s"' % (key, line))
            key_type = key_tokens[0]
            key_name = key_tokens[1]

            if key_type == 'input':
                if len(value) == 0:
                    value = None
                cf.set_input(key_name, value)
            elif key_type == 'output':
                if len(value) == 0:
                    value = None
                cf.set_output(key_name, value)
            elif key_type == 'parameter':
                # Filtering out escape syntax from java.utils.Properties.store().
                if value[:2] == "\\ ":
                    value = " "+value[2:]
                unescaped = "#!=:tnr\\"
                replace_escaped = "#!=:\t\n\r\\"
                escaped = ["\\"+x for x in unescaped]
                new_value = ""
                while len(value):
                    if len(value)==1:
                        new_value+=value[0]
                        break
                    if value[0:2] in escaped:
                        new_value+=replace_escaped[escaped.index(value[0:2])]
                        value=value[2:]
                    else:
                        new_value+=value[0]
                        value=value[1:]
                value = new_value

                cf.set_parameter(key_name, value)
            elif key_type == 'metadata':
                cf.set_metadata(key_name, value)
            elif key_type == 'delegate':
                pass # not yet supported
            else:
                raise ValueError('Invalid key type "%s" on line "%s"' % (key_type, line))
        return cf


def string_record_to_dict(s):
    ''' Converts a  std.recordToString() string into a Dict of
        key=value pairs. The types are guessed.
        Returns either bool, int, float or str type values.

        >>> record_dict = anduril.string_record_to_dict("key1=value1,key2=value2");
        >>> print(record_dict['key1'])
        'value1'
        '''

    def boolify(s):
        if s.lower() == 'true':
            return True
        if s.lower() == 'false':
            return False
        raise ValueError('Not Boolean Value!')

    def type_guess(s):
        '''guesses the str representation of the variables type'''
        s=str(s)
        for caster in (boolify, int, float):
            try:
                return caster(s)
            except ValueError:
                pass
        return s

    values={}
    for pair in s.split(','):
        [k,v]=pair.strip().split('=',1)
        values[k]=type_guess(v)

    return values


