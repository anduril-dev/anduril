""" Module for easy importing of Anduril component arguments 
(input/output ports and parameters).

The args module is written to help writing Anduril components in Python.
Args module loads all the information of the input/output ports and the
parameter of the component when it is imported at the Python script that
is run by Anduril. In addition, it provides an interface for writing 
errors and other information to the log files, and for accessing the
temporary directory.

The arguments are read to namespace of the module as they appear in the
component.xml file. In case of collision with the arguments of different
types, the arguments can be accessed with names starting with 'param_', 
'input_', and 'output_'. Typical use of the Args module is to import
all the arguments to the script namespace at the begining of the script
with 'from anduril.args import *'.

The parameters are automatically converted to the data type indicated in
the component.xml file. That is, strings are Python string, bools are
Python boolean objects etc.

Input and output ports are given as file names. However, if the input
port is an array, it is given as Python OrderedDict object, where the 
key values are array indices and values are the file names. For output
array, an AnduriOutputArray object (from arrayio.py) is given.

The auxiliary functions provided by the module are:
- write_log
- write_error 
- tempdir
"""

from __future__ import print_function
import sys,os

try:
    from collections import OrderedDict
except ImportError:
    # Python 2.6 or earlier
    from ordereddict import OrderedDict
from anduril.componentfile import ComponentFile
from anduril import CommandFile,get_array,ArrayWriter,AndurilOutputArray
from anduril import constants
from anduril import utils

__all__=["component"]

def iteroutputs(cf):
    defaultOutputs=['_tempdir',  '_errors', '_log']
    for output in cf._outputs:
        if output not in defaultOutputs:
            yield output
def iterinputs(cf):
    for input in cf._inputs:
        yield input

def iterparameters(cf):
    for parameter in cf._parameters:
        yield parameter

def itermetadata(cf):
    for meta in cf._metadata:
        yield meta

def get_iop_dict(cf):
    """ Can be used to get a dict to easily print all the values input to the component. Even easier to use the function after this one. """
    import itertools
    return { key: value for (key, value) in [ ( key, globals()[key] ) for key in itertools.chain(
            [ "param_"+a for a in iterparameters(cf) ],
            [ "input_"+a for a in iterinputs(cf)  if not a.startswith("_index_")],
            [ "output_"+a for a in iteroutputs(cf) if not a.startswith("_index_")]
    ) ] }

def component_object(cf):
    """ Using an utility function, build a hierarchy of objects in which,
    inputs, outputs and parameters each have their own attribute, as in
    
    component.input.myinput

    This is stored in the global called "component" elsewhere.
    """
    d = {
            "input": { key:globals()["input_"+key] for key in iterinputs(cf) if not key.startswith("_index_") } ,
            "output": { key:globals()["output_"+key] for key in iteroutputs(cf) if not key.startswith("_index_") },
            "param": { key:globals()["param_"+key] for key in iterparameters(cf) },
            "meta": { key:globals()["meta_"+key] for key in itermetadata(cf) }
    }
    component=utils.Struct(**d)
    return component

args = sys.argv
if len(args) < 2:
    print('No command file given as a command line argument', file=sys.stderr)
    sys.exit(constants.NO_COMMAND_FILE)

try:
    cf = CommandFile.from_file(args[1])
except IOError as e:
    print >>sys.stderr, 'I/O error when reading command file: %s' % e
    sys.exit(constants.COMMAND_FILE_IO_ERROR)
except ValueError as e:
    print >>sys.stderr, 'Parse error when reading command file: %s' % e
    sys.exit(constants.COMMAND_FILE_IO_ERROR)


#We need to parse the type of the parameters and ports from component.xml
_componentXMLFile=os.path.join(os.path.dirname(sys.argv[0]),"component.xml") #assume that xml-file in the same dir as the .py file
#cf.write_log(_componentXMLFile)
_componentXML=ComponentFile(_componentXMLFile)

for parameter in iterparameters(cf):
    paramObject=cf.get_parameter(parameter,_componentXML.get_parameter_type(parameter))
    globals()[parameter]=paramObject; __all__.append(parameter)
    globals()["param_"+parameter]=paramObject; __all__.append("param_"+parameter)
    del paramObject
for meta in itermetadata(cf):
    metaObject=cf.get_metadata(meta)
    globals()[meta]=metaObject; __all__.append(meta)
    globals()["meta_"+meta]=metaObject; __all__.append("meta_"+meta)
    del metaObject
for output in iteroutputs(cf):
    #Check if this input is index to an array
    if not output.startswith("_index_"):
        if _componentXML.output_is_array(output):
            outputObject=AndurilOutputArray(cf,output)
        else:
            outputObject=cf.get_output(output)        

        globals()[output]=outputObject; __all__.append(output)
        globals()["output_"+output]=outputObject; __all__.append("output_"+output)
        del outputObject
for input in iterinputs(cf):
    #Check if this input is index to an array
    if not input.startswith("_index_"):
        #If input is an array, make a Python dict out of it.
        if _componentXML.input_is_array(input) and cf.get_input(input):
            inputObject=OrderedDict(get_array(cf,input))
        else:
            inputObject=cf.get_input(input)
    
        globals()[input]=inputObject; __all__.append(input)
        globals()["input_"+input]=inputObject; __all__.append("input_"+input)
        del inputObject

component=component_object(cf)

#cf.write_log(outputCsv)

#request for temporary directory
tempdir=cf.get_tempdir()
__all__.append("tempdir")

#add the write_log function
write_log = cf.write_log
__all__.append("write_log")


#add the write_error function
write_error = cf.write_error
__all__.append("write_error")



#cf.write_log(str(list(iteroutputs(cf))))
#cf.write_log(str(list(iterinputs(cf))))
#cf.write_log(str(list(iterparameters(cf))))
