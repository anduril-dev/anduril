""" Module for reading and writing Anduril style CSV files.

An ambiguous definition of the CSV format used by Anduril components can
be found from the datatypes.xml file of the builting component. This
module implements that definition, and tries to fill in the ambiguous
parts.
"""

import csv
import types
if "StringTypes" not in dir(types): types.StringTypes=(str,)
import os
import os.path
import math

from anduril.constants import DEFAULT_NAN_OBJECT,NAN_STRING,is_nan_object

__all__ = [
    "TableReader",
    "TableWriter",
    "PandasReader",
    "PandasWriter"
]

class TableWriter():
    ''' Writes CSV files. Automatically quotes strings but leaves
    everything else unquoted. None, NA and nan objects are printed
    as NA strings without quoting as is standard in Anduril.'''
    def __init__(self,filename,fieldnames=[]):
        """Create an array writer instance."""
        self.filename=filename
        self.fieldnames=fieldnames
        self.open()

    def open(self):
        ''' Opens a new file for writing. '''
        self.csvfile=open(self.filename,'wt')

        #This is bit of a hack since we don't want to write the writer
        #again. We do not want the writer to do any automatic quoting
        #since NA is a string and should be unquoted. We do the quoting
        #manually before the row is given to the writer.
        self.writer=csv.DictWriter(self.csvfile,
                               self.fieldnames,
                               delimiter='\t',
                               quotechar='',
                               quoting=csv.QUOTE_NONE)

        if self.fieldnames!=None: #if None, no headers.
            self.writerow(dict(zip(self.fieldnames,self.fieldnames)))

    def close(self):
        '''Close the file.'''
        self.csvfile.close()

    def _element_to_string(self,element):
        if isinstance(element,str) or isinstance(element,unicode):
            return '"'+element+'"'
        elif element==None or math.isnan(element) or is_nan_object(element):
            return NAN_STRING
        else:
            return str(element)

    def writerow(self,row):
        ''' Write a row in the array file. The row can be either
        dict or list type. Dictionary should have the keys as
        field names and values as data elements. List should have
        the data elements in the same order as fields in
        self.fieldnames.'''
        if isinstance(row,dict):
            nrow=dict(map(lambda x:(x[0],self._element_to_string(x[1])),row.items()))
        elif isinstance(row,list):
            nrow=dict(zip(self.fieldnames,map(self._element_to_string,row)))
        else:
            raise TypeError("Row object must be a 'dict' or a 'list', received: "+str(type(row)))
        self.writer.writerow(nrow)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

    def set_fieldnames(self,fieldnames):
        ''' Set the field names for the table.
            Note that existing file will be erased '''
        self.fieldnames=fieldnames
        self.open()

class TableReader:
    """ Class for reading Anduril style CSV files.
    Parameters:
        filename : Path to the CSV-file
        type : Return type for the rows, 'dict' or 'list'
        column_types : Data type for the columns, can be any type object,
                       'guess' or 'strict'. Guessing ignores all the
                        quatations and tries to convert all the values
                        to floats or booleans. Strict mode returns all
                        quoted values as strings and tries to convert
                        all unquoted values to floats returning NAN
                        when failing.
    """
    def __init__(self,filename,type='dict',column_types="guess"):
        """ Create an array reader instance
        Parameters:
        filename : Path to the CSV-file
        type : Return type for the rows, 'dict' or 'list'
        column_types : Data type for the columns, can be any type object,
                       'guess' or 'strict'. Guessing ignores all the
                        quatations and tries to convert all the values
                        to floats or booleans. Strict mode returns all
                        quoted values as strings and tries to convert
                        all unquoted values to floats returning NAN
                        when failing.
        """
        self.filename=filename
        self.file=open(self.filename,'rt')
        assert type in ["Dict","dict","List","list"], "Invalid type: '%s', use 'Dict' or 'List'" % type
        self.row_type=type.lower()
        self.column_types=column_types
        self.open()

    def open(self):
        ''' Open the file for reading. If file is already open, it is
        reset such that the iterators start from the begining of the file.

        Note that this function is already called at the constructor!'''
        self.file.seek(0)
        if self.column_types=="strict":
            quoting=csv.QUOTE_NONE
        else:
            quoting=csv.QUOTE_MINIMAL
        if self.row_type=="dict":
            self.reader=csv.DictReader(self.file,
                                   delimiter='\t',
                                   quotechar='"',
                                   quoting=quoting)
            if type(self.column_types) is type({}):
                self._parse_row=self._parse_row_dict
            else:
                self._parse_row=self._parse_row_dict_default
            self.fieldnames=self.reader.fieldnames
        else: #its a list
            self.reader=csv.reader(self.file,
                                   delimiter='\t',
                                   quotechar='"',
                                   quoting=quoting)
            if type(self.column_types) is type([]):
                self._parse_row=self._parse_row_list
            else:
                self._parse_row=self._parse_row_list_default
            self.fieldnames=next(self.reader)


    def _parse_element(self,element,column_type):
        if column_type=="strict":
            if element.startswith('"') and element.endswith('"'):
                return element.strip('"')
            else:
                try:
                    return float(element)
                except ValueError:
                    return DEFAULT_NAN_OBJECT
        elif column_type=="guess":
            if element==NAN_STRING:
                return DEFAULT_NAN_OBJECT
            try:
                return float(element)
            except:
                if element.lower() in ["true","false"]:
                    return bool(element)
                else:
                    return element

        return column_type(element)

    def _parse_row_dict(self,row):
        pass
    def _parse_row_dict_default(self,row):
        return dict(map(lambda x:(x[0],self._parse_element(x[1],self.column_types)),row.items()))

    def _parse_row_list(self,row):
        pass
    def _parse_row_list_default(self,row):
        return map(lambda v:self._parse_element(v,self.column_types),row)

    def next(self):
        ''' Returns the next row in the table'''
        return self._parse_row(next(self.reader))

    def __iter__(self):
        for row in self.reader:
            yield self._parse_row(row)

    def read(self):
        ''' Returns the whole file contents as a list of row Dicts/lists depeding
        on the default type.'''
        values=list(self)
        self.open()
        return values

    def get_fieldnames(self):
        ''' Returns the list of field names '''
        return self.fieldnames

    def get_column(self,col_name):
        ''' Returns the column as a list of values '''
        if col_name not in self.fieldnames:
            raise ValueError('Column name "'+col_name+'" not found in table '+self.filename)
        values=[row[col_name] for row in self.reader]
        self.open()
        return values

    def get_dict(self,keyColumn=None,allowDuplicateKeys=False):
        """Returns the content of the table as a dictionary, where elements in keyColumn are used as keys.

        Kwargs:
           keyColumn (str): The name of the column which is used as the key for the row. If None, the first column is used.
           allowDuplicatedKeys (bool): If duplicate keys are not allowed and one is encountered an expetion is raised. If they
                                       are allowed the latest row is returned in the dict.

        Returns:
           Dictionary object containing each row as an item where key is from the keyColumn and value contains the data in the
           rest of the columns. Note that the type of value depends on the type of the table instance.
        """
        assert len(self.fieldnames)>0, "There should be at least one column in the table."
        return_dict={}
        if self.row_type=="list":
            if keyColumn==None:
                keyColumn=0
        else:
            if keyColumn==None:
                keyColumn=self.get_fieldnames()[0]
        for row in self:
            key=row[keyColumn]
            if not allowDuplicateKeys:
                assert key not in return_dict, "Key '"+str(key)+"' appears multiple times in the table."
            if self.row_type=="list":
                return_dict[key]=row[:keyColumn]+row[keyColumn+1:]
            else:
                return_dict[key]=row
                return_dict[key].pop(keyColumn)
        return return_dict

    def get_numpy_array(self,types=None,numeric=False):
        """ Reads the table to a Numpy Record Array object."""
        import numpy
        self.open() #reset the reader

        if types==None:
            types={}

        fieldNames=self.get_fieldnames()
        fieldNameSet=set(fieldNames)
        assert len(fieldNames)==len(fieldNameSet), "The file contains repeated field names."
        for columnName in types.keys():
            assert columnName in fieldNameSet,"The input types contains key: '"+str(columnName)+"' which is not a field name."

        #Count the number of rows
        self.file.seek(0)
        for nRows,dummy in enumerate(self.file):
            pass
        self.open()

        #Infer the missing types by reading the first line
        if nRows>0:
            for field,value in self.next().items():
                if field not in types:
                    if value.__class__ == float:
                        types[field]=float
                    else:
                        types[field]=object
            self.open()
        else:
            for field in fieldNames:
                if field not in types:
                    types[field]=object

        fieldNumbers=["f%u"%i for i in range(len(fieldNames))]
        fieldTypes=[types[fieldName] for fieldName in fieldNames]

        array=numpy.empty(nRows,dtype={"names":fieldNumbers,"formats":fieldTypes,"titles":fieldNames})

        for rowNumber,row in enumerate(self):
            for field,value in row.items():
                try:
                    array[field][rowNumber]=value
                except ValueError:
                    raise Exception("Value of column '"+field+"': '"+str(value)+"' cannot be converted to float")

        if numeric:
            numeric_fields=map(lambda x:array.dtype.names[x],filter(lambda x:array.dtype[x].kind=="f",range(len(array.dtype))))
            narray=array[numeric_fields]
            narray=narray.view(float).reshape(len(narray),len(numeric_fields))

            non_numeric_fields=map(lambda x:array.dtype.descr[x][0][0],filter(lambda x:array.dtype[x].kind!="f",range(len(array.dtype))))
            non_numeric_data=dict(map(lambda f:(f,array[f]),non_numeric_fields))

            return narray,map(lambda x:array.dtype.fields[x][2],numeric_fields),non_numeric_data

        return array


def PandasReader(filename):
    """ Returns a pandas data frame, from Anduril flavor TSV file """

    import pandas
    return pandas.read_csv(
        filename,
        sep = '\t',
        header = 0
    )

def PandasWriter(dataframe, filename):
    """ Writes a data frame as TSV file """

    import pandas
    assert type(dataframe) == pandas.core.frame.DataFrame, "Input is not a pandas.DataFrame"
    dataframe.to_csv(
        filename,
        sep = '\t',
        index = False,
        header = True
    )
