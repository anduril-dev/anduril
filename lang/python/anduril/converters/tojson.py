import csv as c
from itertools import *
import io

def decorate(field):
    if isfloat(field):
        return float(field)
    else:
        return '"'+field+'"'

def isfloat(s):
    try:
        i = float(s)
    except (ValueError, TypeError):
        return False
    else:
        return True

def from_csv_iterator_plot(csv,columnsFieldName="columns",dataFieldName="data"):
    if isinstance(csv,str):
        csv=open(csv,'r')

    reader=c.reader(csv,
                    delimiter="\t",
                    quotechar='"',
                    quoting=c.QUOTE_MINIMAL)

    yield '{'
    yield '"'+columnsFieldName+'" : ['+','.join(map(lambda f:str(decorate(f)),next(reader)))+'],'

    #Write the data
    yield '"'+dataFieldName+'" : ['
    first = True
    s=""
    for fields in reader:
        if not first:
            yield s+','
            s=""
        first=False
        s+='['
        s+=','.join(map(lambda f:str(decorate(f)),fields))
        s+=']'
    yield s+']'
    yield '}'

def from_csv_iterator_table(csv):
    import json
    if isinstance(csv,str):
        csv=open(csv,'r')

    reader=c.reader(csv,
                    delimiter="\t",
                    quotechar='"',
                    quoting=c.QUOTE_MINIMAL)

    #Write the data
    yield '['
    first = True
    s=""
    for fields in reader:
        if not first:
            yield s+','
            s=""
        first=False
        s+=json.dumps(fields)
    yield s
    yield ']'


def from_csv_iterator(csv,format="plot",**kwargs):
    if format=="plot":
        return from_csv_iterator_plot(csv,**kwargs)
    if format=="table":
        return from_csv_iterator_table(csv,**kwargs)


def from_csv(csv,output='',**kwargs):
    """Converts a CSV file (or other iterable object) to a JSON format.

    Args:
        csv: The csv file, iterator to lines in the csv or a file name as a string.
        output: Defines how the output is produced, options are:
                Empty string (default) - returns the JSON as a string.
                File name - opens the file, write the JSON to it and returns it.
                Open file - writes the JSON to the file, returns the file.
        **kwargs: These are passsed to from_csv_iterator.
    """
    if isinstance(output,io.IOBase): #its an open file
        for line in from_csv_iterator(csv,**kwargs): output.write(line+"\n")
        return output
    elif isinstance(output,str) and len(output)>0: #its a filename
        output=open(output,'w')
        for line in from_csv_iterator(csv,**kwargs): output.write(line+"\n")
        output.close()
    elif isinstance(output,str): #default, return string
        return "\n".join(from_csv_iterator(csv,**kwargs))
    else:
        raise Exception("Invalid type for the argument output: "+str(type(output)))


#def formatter(self,format)

    #for row in self.reader:
        #outrow={}
        #for col in range(len(row)):
            #if isfloat(row[col]):
                #outrow[col]=float(format%float(row[col]))
            #else:
                #outrow[col]=row[col]



