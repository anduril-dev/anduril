import csv as c
from itertools import *
import anduril
import io

def decorate(field):
    if isfloat(field):
        return float(field)
    else:
        return '"'+field+'"'

def isfloat(s):
    try:
        i = float(s)
    except (ValueError, TypeError):
        return False
    else:
        return True

def from_csv_iterator_html(csv,t_class="",t_id=""):

    reader=anduril.TableReader(csv,
                    type='list',
                    column_types=str)
    tab="\t"
    yield '<table id="'+t_id+'" class="'+t_class+'">'
    yield '<thead><tr>'
    for name in reader.get_fieldnames():
        yield tab+'<th>'+name+'</th>'
    yield '</tr></thead>'
    yield '<tbody>'
    for row in reader:
        yield '<tr>'
        for value in row:
            yield tab+'<td>'+value+'</td>'
        yield '</tr>'
    yield '</tbody>'
    yield '</table>'

    return

def from_csv(csv,output='',**kwargs):
    """Converts a CSV file (or other iterable object) to a HTML format.

    Args:
        csv: The csv file, iterator to lines in the csv or a file name as a string.
        **kwargs: These are passsed to from_csv_iterator.
    """
    if isinstance(output,io.IOBase): #its an open file
        for line in from_csv_iterator_html(csv,**kwargs): output.write(line+"\n")
        return output
    elif isinstance(output,str) and len(output)>0: #its a filename
        output=open(output,'w')
        for line in from_csv_iterator_html(csv,**kwargs): output.write(line+"\n")
        output.close()
    elif isinstance(output,str): #default, return string
        return "\n".join(from_csv_iterator_html(csv,**kwargs))
    else:
        raise Exception("Invalid type for the argument output: "+str(type(output)))


