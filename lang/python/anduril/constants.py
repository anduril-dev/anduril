import math

ERRORS_PORT = '_errors'
LOG_PORT = '_log'
TEMPDIR_PORT = '_tempdir'

# Master list is in ErrorCode.java
OK = 0
GENERIC_ERROR = 1
NO_COMMAND_FILE = 20
COMMAND_FILE_IO_ERROR = 21
INPUT_IO_ERROR = 22
INVALID_ERRORSTREAM = 23
PARAMETER_ERROR = 24
UNCAUGHT_EXCEPTION = 25
INVALID_INPUT = 26

LATEX_DOCUMENT_FILE = 'document.tex'

DEFAULT_NAN_OBJECT=float("nan")
NAN_STRING="NA"


def is_nan_object(obj):
    return isinstance(obj,float) and math.isnan(obj)
