import csv
import types
if "StringTypes" not in dir(types): types.StringTypes=(str,)
import os
import os.path

__all__=["ArrayWriter","ArrayReader","get_array","AndurilOutputArray"]


class ArrayWriter:
    ''' Writes Anduril array files'''
    def __init__(self,commandfile=None,port_name=None,folder=None):
        """Create an array writer instance. Give either commandfile and
        port_name, or folder."""
        self.index=None
        if commandfile!=None and port_name!=None:
            self.folder = commandfile.get_output(port_name)
            self.index_file = commandfile.get_output_array_index(port_name)
        else:
            if folder==None:
                raise ValueError("Give either commandfile and port_name or folder.")
            else:
                self.folder=folder
                self.index_file=os.path.join(folder,"_index")
        if not os.path.exists(self.folder):
            os.mkdir(self.folder)
        #self.open()

    def open(self):
        ''' Opens a new array file for writing. '''
        try:
            self.index=open(self.index_file,'wt')
            self.writer=csv.writer(self.index,
                                   delimiter='\t',
                                   quotechar='"',
                                   quoting=csv.QUOTE_NONNUMERIC)
            self.writer.writerow(['Key','File'])
        except:
            raise IOError('Can not write to file '+self.index_file)

    def close(self):
        self.index.close()

    def writerow(self,row):
        ''' Write a row in the array file '''
        self.writer.writerow(row)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class ArrayReader:
    def __init__(self,commandfile=None,port_name=None,folder=None):
        """ Create an array reader instance. Give either commandfile and
        port_name, or folder. """
        if commandfile!=None and port_name!=None:
            self.folder = commandfile.get_input(port_name)
            self.index_file = commandfile.get_input_array_index(port_name)
        else:
            if folder==None:
                raise ValueError("Give either commandfile and port_name or folder.")
            else:
                self.folder=folder
                self.index_file=os.path.join(folder,"_index")
        try:
            self.reader=csv.DictReader(open(self.index_file,'rt'),
                               delimiter='\t',
                               quotechar='"',
                               quoting=csv.QUOTE_ALL)
        except:
            raise IOError('Can not read file '+self.index_file)

    def next(self):
        ''' Returns the next value in the array '''
        return next(self.reader)

    def __iter__(self):
        return self.reader

    def get_folder(self):
        return self.folder


def get_array(cf,array_name,relativeTo=None):
    """Return a list of tuples containing the key and filename of an
    Anduril array data type item, or None if the item
    is not defined. The value is always a string, if defined.
    cf is the CommandFile object created by Anduril component skeleton."""
    #import os.path
    if type(array_name) not in types.StringTypes:
        raise TypeError('array_name must be a string')
    array_path=cf.get_input(array_name)
    arrayreader=ArrayReader(cf,array_name)
    array=[]
    for row in arrayreader:
        if relativeTo==None:
            arrayrow=(row['Key'],get_abs_file(array_path,row['File']))
        else:
            arrayrow=(row['Key'],get_rel_file(relativeTo,get_abs_file(array_path,row['File'])))
        array.append(arrayrow)
    return array

def get_abs_file(pathName,fileName):
    ''' Returns a string absolute file name.
    The returned string is either "fileName" or "pathName"/"fileName" '''
    if os.path.isabs(fileName):
        return fileName
    else:
        return os.path.join(pathName,fileName)

def get_rel_file(startPath,absPath):
    ''' Returns a string relative path compared to dirPath.
    '''
    return os.path.relpath(absPath,startPath)

class AndurilOutputArray(object):
    """ AndurilOutputArray automatically creates file names
    and writes them to the array file in disk when asked
    of keys with __getitem__. E.g.

    >>> filename = andurilOutputArrayObject["myKey"]

    You can also explicitely give the file name:

    >>> andurilOutputArrayObject.write("myKey","path/to/my/file.txt")
    """

    def __init__(self,commandfile=None,port_name=None,filetype="",folder=None):
        """Give either commandfile and port_name, or folder."""
        self.keys=set()
        self.writer=ArrayWriter(commandfile,port_name,folder)
        self.writer.open()
        self.filetype=filetype

    def set_type(self,newtype):
        self.filetype=newtype

    def write(self,key,filename):
        if key not in self.keys:
            self.keys.add(key)
            self.writer.writerow([key,filename])
        else:
            raise Exception("Key '%s' already in array." % key)

    def close(self):
        self.writer.close()

    def get_folder(self):
        return self.writer.folder

    def __getitem__(self,key):
        self.write(key,self._get_filename(key))
        return self._get_path_to_file(key)

    def _get_filename(self,key):
        if len(self.filetype)>0:
            return key+"."+self.filetype
        else:
            return key

    def _get_path_to_file(self,key):
        return os.path.join(self.writer.folder,self._get_filename(key))

    def __del__(self):
        #self.writer.close()
        pass
