import csv
import types
import os
import os.path

class ArrayWriter:
    ''' Writes Anduril array files'''
    def __init__(self,commandfile,port_name):
        """Create an array writer instance."""
        self.folder = commandfile.get_output(port_name)
        self.index_file = commandfile.get_output_array_index(port_name)
        if not os.path.exists(self.folder):
            os.mkdir(self.folder)
        #self.open()
    
    def open(self):
        ''' Opens a new array file for writing. '''
        try:
            self.writer=csv.writer(open(self.index_file,'wt'),
                                   delimiter='\t',
                                   quotechar='"', 
                                   quoting=csv.QUOTE_NONNUMERIC)
            self.writer.writerow(['Key','File'])
        except:
            raise IOError('Can not write to file '+self.index_file)
        
    def writerow(self,row):
        ''' Write a row in the array file '''
        self.writer.writerow(row)
    
    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class ArrayReader:
    def __init__(self,commandfile,port_name):
        """ Create an array reader instance """ 
        self.folder = commandfile.get_input(port_name)
        self.index_file = commandfile.get_input_array_index(port_name)
        try:
            self.reader=csv.DictReader(file(self.index_file,'rt'),
                               delimiter='\t',
                               quotechar='"', 
                               quoting=csv.QUOTE_ALL)
        except:
            raise IOError('Can not read file '+self.index_file)

    def next(self):
        ''' Returns the next value in the array '''
        return self.reader.next()
            
    def __iter__(self):
        return self.reader

    
def get_array(cf,array_name,relativeTo=None):
    """Return a list of tuples containing the key and filename of an 
    Anduril array data type item, or None if the item
    is not defined. The value is always a string, if defined.
    cf is the CommandFile object created by Anduril component skeleton."""
    #import os.path
    if type(array_name) not in types.StringTypes:
        raise TypeError('array_name must be a string')
    array_path=cf.get_input(array_name)
    arrayreader=ArrayReader(cf,array_name)
    array=[]
    for row in arrayreader:
        if relativeTo==None:
            arrayrow=(row['Key'],get_abs_file(array_path,row['File']))
        else:
            arrayrow=(row['Key'],get_rel_file(relativeTo,get_abs_file(array_path,row['File'])))
        array.append(arrayrow)
    return array

def get_abs_file(pathName,fileName):
    ''' Returns a string absolute file name.
    The returned string is either "fileName" or "pathName"/"fileName" '''
    if os.path.isabs(fileName):
        return fileName
    else:
        return os.path.join(pathName,fileName)

def get_rel_file(startPath,absPath):
    ''' Returns a string relative path compared to dirPath.
    '''
    return os.path.relpath(absPath,startPath)
    
