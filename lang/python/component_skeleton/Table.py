import csv
import types
import os
import os.path
import numpy

class writer():
    ''' Writes CSV files'''
    def __init__(self,filename,fieldnames=[]):
        """Create an array writer instance."""
        self.filename=filename
        self.fieldnames=fieldnames
        self.open()
    
    def open(self):
        ''' Opens a new file for writing. '''
        try:
            self.writer=csv.DictWriter(open(self.filename,'wt'),
                                   self.fieldnames,
                                   delimiter='\t',
                                   quotechar='"', 
                                   quoting=csv.QUOTE_NONNUMERIC)
            self.writer.writeheader()
        except:
            raise IOError('Can not write to file '+self.index_file)
        
    def writerow(self,row):
        ''' Write a row in the array file '''
        self.writer.writerow(row)
    
    def writerows(self, rows):
        for row in rows:
            self.writerow(row)
            
    def set_fieldnames(self,fieldnames):
        ''' Set the field names for the table. 
            Note that existing file will be erased '''
        self.fieldnames=fieldnames
        self.open()

class reader:
    def __init__(self,filename,type='Dict'):
        """ Create an array reader instance """ 
        self.filename=filename
        self.file=file(self.filename,'rt')
        self.type=type
        self.open()
    
    def open(self):
        ''' Open the file for reading '''
        try:
            self._set_reader()
        except ValueError:
            self._set_reader(quoting=csv.QUOTE_MINIMAL)
        except:
            raise IOError('Can not read file '+self.filename)
    
    def _set_reader(self,quoting=csv.QUOTE_NONNUMERIC):
        ''' Try to guess the quoting, and set the reader accordingly '''
        self.file.seek(0)
        self.reader=csv.DictReader(self.file,
                               delimiter='\t',
                               quotechar='"', 
                               quoting=quoting)
        self.fieldnames=self.reader.fieldnames
        
    
    def next(self):
        ''' Returns the next value in the array '''
        return self.reader.next()
            
    def __iter__(self):
        return self.reader
        
    def read(self):
        ''' Returns the whole file contents as a list of row Dicts '''
        values=list(self.reader)
        self.open()
        return values

    def get_fieldnames(self):
        ''' Returns the list of field names '''
        return self.reader.fieldnames
    
    def get_column(self,col_name):
        ''' Returns the column as a list of values '''
        if col_name not in self.fieldnames:
            raise ValueError('Column name "'+col_name+'" not found in table '+self.filename)
        values=[row[col_name] for row in self.reader]
        self.open()
        return values
    
    def get_numpy_array(self,types=None):
        """ Reads the table to a Numpy Record Array object."""
        self.open() #reset the reader

        if types==None:
            types={}

        fieldNames=self.get_fieldnames()
        fieldNameSet=set(fieldNames)
        assert len(fieldNames)==len(fieldNameSet), "The file contains repeated field names."
        for columnName in types.keys():
            assert columnName in fieldNameSet,"The input types contains key: '"+str(columnName)+"' which is not a field name."

        #Count the number of rows
        self.file.seek(0)
        for nRows,dummy in enumerate(self.file):
            pass

        #Infer the missing types by reading the first line
        self.open()
        for field,value in self.next().iteritems():
            if field not in types:
                if value.__class__ == float:
                    types[field]=float
                else:
                    types[field]=object
        self.open()


        fieldNumbers=["f%u"%i for i in range(len(fieldNames))]
        fieldTypes=[types[fieldName] for fieldName in fieldNames]
                
        array=numpy.empty(nRows,dtype={"names":fieldNumbers,"formats":fieldTypes,"titles":fieldNames})

        for rowNumber,row in enumerate(self):
            for field,value in row.iteritems():
                try:
                    array[field][rowNumber]=value
                except ValueError:
                    raise Exception("Value of column '"+field+"': '"+str(value)+"' cannot be converted to float")
        return array
