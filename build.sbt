/*
SBT build file for Anduril core.

Commands:
- sbt assembly = Compile stand-alone anduril.jar (for deployment)
- sbt doc      = Generate Scaladocs for org.anduril.runtime
- sbt test     = Run unit tests

Requirements:
- $ANDURIL_HOME must be defined
*/

lazy val commonSettings = Seq(
    scalaVersion := "2.11.7",
    version := "2.0.0-beta.1"
)

val ANDURIL_HOME = file(sys.env("ANDURIL_HOME"))

lazy val core = (project in file("core")).
    settings(commonSettings: _*).
    settings(
        name := "anduril",

        scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation"),

        libraryDependencies ++= Seq(
            "commons-cli" % "commons-cli" % "1.3.1",
            "commons-collections" % "commons-collections" % "3.2.2",
            "commons-lang" % "commons-lang" % "2.6",
            "commons-logging" % "commons-logging" % "1.2",
            "commons-primitives" % "commons-primitives" % "1.0",
            "com.googlecode.java-diff-utils" % "diffutils" % "1.2.1",
            "com.google.code.gson" % "gson" % "2.6.2",
            "junit" % "junit" % "4.12",
            "net.sf.supercsv" % "super-csv" % "2.4.0",
            "org.apache.commons" % "commons-math3" % "3.6.1",
            "org.apache.velocity" % "velocity" % "1.7",
            "org.ow2.asm" % "asm" % "7.0",
            "org.scala-lang" % "scala-compiler" % scalaVersion.value,
            "org.scala-lang" % "scala-reflect"  % scalaVersion.value
        ),

        libraryDependencies ++= Seq(
            "org.scalatest" % "scalatest_2.11" % "2.2.6" % Test
        ),
        unmanagedJars in Test ++= Seq(
            ANDURIL_HOME / "bundles" / "builtin" / "builtin.jar",
            ANDURIL_HOME / "bundles" / "techtest" / "techtest.jar"
        ),
        fork in Test := true,

        mainClass in Compile := Some("org.anduril.core.commandline.CLI"),
        packageOptions += Package.ManifestAttributes(
            "Class-Path" -> (fullClasspath in Compile).value.map { _.data }.mkString(" ")
        ),

        assemblyJarName in assembly := "anduril.jar",
        test in assembly := {},

        scalacOptions in (Compile, doc) ++= Opts.doc.title("Anduril Scala API"),
        scalacOptions in (Compile, doc) ++= Seq(
            "-skip-packages",
            "org.anduril.asser:org.anduril.core:org.apache"
        ),
        scalacOptions in (Compile, doc) ++= Seq(
            "-doc-root-content",
            (baseDirectory.value / "src" / "scaladoc-root-content.txt").getPath
        ),

        crossTarget := baseDirectory.value / "..",
        artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
            artifact.name + "-dev" + "." + artifact.extension
        }
    )

lazy val root = (project in file(".")).
    settings(commonSettings: _*).
    aggregate(core)
