# ![Anduril workflow platform](img/anduril-logo-w200px.png)

Anduril is a workflow platform for analyzing large data sets.

Anduril provides [facilities](http://www.anduril.org/anduril/bundles/all/doc/) for analyzing high-thoughput data in biomedical research, and the platform is fully [extensible](http://anduril.org/userguide/extend/overview/) by third parties. Ready-made tools support data visualization, DNA/RNA/ChIP-sequencing, DNA/RNA microarrays, cytometry and image analysis.

Workflows are [constructed](http://anduril.org/userguide/workflows/hello-world/) using Scala 2.11 and executed in parallel using a workflow engine optimized for iterative development. External libraries (e.g., R, Python) and command-line tools can be integrated into workflows. Anduril can be deployed locally or in a [Linux cluster](http://anduril.org/userguide/install/cluster/).

Anduril development began in 2007 and the platform has been used for analyzing [several](resources/publications.md) large molecular biology data sets.

## Getting started

1. [Install](download/) using Docker or natively on Linux
2. Read [User Guide](http://anduril.org/userguide/)
3. Create and execute a workflow

## License

Anduril is developed by Systems Biology Laboratory, University of Helsinki, Finland. Anduril 2 (current version) is available under the [BSD 2-clause license](https://bitbucket.org/anduril-dev/anduril/raw/anduril2/doc/LICENSE). Anduril 1 (legacy version) is available under the GNU General Public License v2 (GPL 2).

## Citing

Please cite this publication when you use Anduril 2.x.

* Cervera, A., Rantanen, V., Ovaska, K., Laakso, M., Nuñez-Fontarnau, J., Alkodsi, A., ... Hautaniemi, S. [Anduril 2: Upgraded large-scale data integration framework](https://doi.org/10.1093/bioinformatics/btz133)
Bioinformatics 2019, 35(19), 3815-3817

Please cite this publication when you use Anduril 1.x.

* Kristian Ovaska, Marko Laakso et al. [Large-scale data integration
framework provides a comprehensive view on glioblastoma multiforme.](http://genomemedicine.com/content/2/9/65)
Genome Medicine 2010, 2(9):65.

