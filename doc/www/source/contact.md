# Contact information

> Systems Biology Laboratory

> Biomedicum Helsinki, B524b  
> P.O.Box 63 (Haartmaninkatu 8)  
> 00014 University of Helsinki  
> Finland  

> e-mail: anduril-dev(at)helsinki.fi  
> phone: +358 9 191 25407  
> WWW: [http://research.med.helsinki.fi/gsb/hautaniemi/](http://research.med.helsinki.fi/gsb/hautaniemi/)

# Links

- [Source code @ Bitbucket](http://source.anduril.org)
- [Anduril discussion forum](https://groups.google.com/forum/m/?fromgroups#!forum/anduril-dev)
