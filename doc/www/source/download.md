# Download

Anduril works on Linux. The easiest way to get started is using [Docker](https://hub.docker.com/u/anduril/). Alternatively, you can install natively.

More detailed installation instructions: [User Guide](http://anduril.org/userguide/install/install/).

## Docker

`docker run -ti --rm anduril/anduril anduril -h`

This prints the help message of the Anduril command line tool. Next step: [Hello world](http://anduril.org/userguide/workflows/hello-world/).

## Native installation: Ubuntu

*Install time: 30-60 min*

```sh
sudo apt-get install ant mercurial git default-jdk python r-base-dev
export ANDURIL_HOME=~/anduril
git clone https://bitbucket.org/anduril-dev/anduril --branch stable "$ANDURIL_HOME"
export PATH="$ANDURIL_HOME/bin:$PATH"
anduril build
anduril install tools
```

## Native installation: Other Linux

Replace the `sudo apt-get install` command above with commands that install the following mandatory dependencies:

- Java 8, 10 or 11
- Apache Ant
- Mercurial
- Python 2.7
- R 3.0+

## Additional bundles

After a basic installation, you may want to install additional bundles using `anduril install BUNDLE-NAME`.

Bundle name                                                | Purpose
-----------------------------------------------------------|---------------------------------
[anima](https://bitbucket.org/anduril-dev/anima)           | Image analysis
[flowand](https://bitbucket.org/anduril-dev/flowand)       | Cytometry analysis
[microarray](https://bitbucket.org/anduril-dev/microarray) | DNA/RNA microarray analysis
[sequencing](https://bitbucket.org/anduril-dev/sequencing) | DNA/RNA sequencing analysis (NGS)
[tools](https://bitbucket.org/anduril-dev/tools)           | Commonly used tools. Already installed above.
