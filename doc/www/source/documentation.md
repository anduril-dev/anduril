# Documentation

## Essentials

- [User Guide](http://anduril.org/userguide/)
- [Component documentation](http://www.anduril.org/anduril/bundles/all/doc) (for all bundles)

##  Language APIs

The following APIs are used when implementing new components for Anduril.

- [Bash API](http://anduril.org/pub/api/bash)
- [MATLAB API](http://anduril.org/pub/api/matlab)
- [Python API](http://anduril.org/pub/api/python)
- [R API](http://anduril.org/pub/api/r/00Index.html)
- [Scala API](http://anduril.org/pub/api/scala)

## Other

- [ChangeLog](https://bitbucket.org/anduril-dev/anduril/src/anduril2/doc/ChangeLog.txt#cl-0)
- [Anduril discussion forum](https://groups.google.com/forum/m/?fromgroups#!forum/anduril-dev)

