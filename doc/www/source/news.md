# News


## Anduril gets a new web site!
Posted: 2016/01/04

We are changing our web site to a new server, and changing many ways we work.
The main site for Anduril gets a facelift! Expect to see further changes here.

-- ville


## Presentation about Anduril and Docker
Posted: 2015/10/12

<iframe src="https://www.slideshare.net/slideshow/embed_code/key/bdxgGTIkYUqbq8" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>
[Reproducible bioinformatics pipelines with Docker and Anduril](https://www.slideshare.net/ChristianFrech/reproducible-bioinformatics-pipelines-with-docker-and-anduril)
by [Christian Frech](http://www.slideshare.net/ChristianFrech)

Thanks Christian for the excellent job!

-- ville

----

## Anduril source code repositories moved
Posted: 2015/04/11 

Since Google Code is shutting down, we have moved our public source code repositories to be hosted at Bitbucket.
Find the Anduril Core and bundles at [https://bitbucket.org/anduril-dev](https://bitbucket.org/anduril-dev)

-- ville

----

## Anduril celebrates its 7th birthday!

Posted: 2015/02/05 

I don’t know what procrastination devil prompted me to check our 
versioning system today but I noticed that the Anduril project 
started seven years ago (give or take a few weeks) –- so happy 
birthday, big-A! Incidentally, the comment on the first commit is an 
excited “Moved everything to trunk”. But hey, I guess you never know 
when you are about to start something big.

-- riku

----

## Anima published
Posted: 2014/08/06

The image analysis bundle for Anduril was published in Frontiers in Bioengineering and Biotechnology.

Read it, and start using the imaging capabilities of Anduril! Anima provides several components for processing, segmenting, and feature extraction written in Matlab. Moreover, Anima integrates other platforms, for example ImageMagick and Fiji in the workflow.

The full title of the publication:  
Anima: Modular workflow system for comprehensive image data analysis.
Ville Rantanen, Miko Valori and Sampsa Hautaniemi,  
Front. Bioeng. Biotechnol.   
doi: 10.3389/fbioe.2014.00025

-- ville

----

## The Cancer Genome Atlas data download made easy
Posted: 2013/03/04

We’ve updated the list of Anduril bundles with the TCGA bundle. TCGA, or the Cancer Genome Atlas, is a massive repository of open-access cancer genomics data from some 25 cancer types. Accessing and downloading the data can be quite tricky, so we’ve constructed our own accessor components that make not only downloading but also sample matching between data types as straight-forward as it can be. We’ve also included some small examples in the bundle subpage.

-- riku

----

## Two new Anduril “helper apps”
Posted: 2013/03/01

The Anduril distribution contains two new helper apps `anduril-log-cleaner` and `anduril-result-browser`.

We all know how – erhm – informative Anduril execution logs and the stdout stream can be. You can now beautify the stream with anduril-log-cleaner. There are several parameters that you can use to tweak levels of verbosity and color code your favorite std streams. And there’s more: the cleaner also prints the numbers of succesful, failed and queued component instances and the elapsed time of execution. Pretty neat, huh? Just add `$ANDURIL_HOME/bin` to your $PATH and pipe your run command to the cleaner: anduril run Script.and | anduril-log-cleaner.

Talking about verbose logs, an execution directory of Anduril can be excessive as well. Sometimes you still have to dive into its depths to hunt for that one bug. The `anduril-result-browser` makes this a whole lot simpler. The syntax is `anduril-result-browser myExecDir/instanceName`.

The next Anduril update will surface next week.

-- riku

----

## AndurilScript joins programming language community
Posted: 2012/06/08 

We’ve recently (or rather, Ville from our group has) added AndurilScript to the catalogue of programming languages capable of producing the ’99 bottles of beer’-song. This comical version of a ‘Hello world’-program has its roots in Donald Knuth’s 1977 article on computational complexity theory called ‘The Complexity of Songs’.

[Check out our implementation.](http://www.99-bottles-of-beer.net/language-anduril-2699.html)

-- riku

----
