# References

## Publications

Please cite this publication when you use Anduril.

-   Kristian Ovaska, Marko Laakso *et al.* [Large-scale data integration
    framework provides a comprehensive view on glioblastoma
    multiforme](http://genomemedicine.com/content/2/9/65). Genome
    Medicine 2010, 2:65.\
     [Supplementary
    document](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/summaryReport-document.pdf),
    [Genome Medicine
    commentary](http://genomemedicine.com/content/2/9/67), [TCGA
    commentary](http://cancergenome.nih.gov/researchhighlights/researchbriefs/newdatabase),
    [original case study
    results](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/tcga-gbm/index.html), [updated
    glioblastoma results](http://csbi.ltdk.helsinki.fi/gbm/).

-   Alejandra Cervera, Ville Rantanen *et al.* [Anduril 2: upgraded large-scale data
    integration framework](https://doi.org/10.1093/bioinformatics/btz133),
    *Bioinformatics*, btz133, 2019

## Learning material

-   Marko Laakso, Kristian Ovaska, Sampsa Hautaniemi. *Component-based
    workflow framework for data analysis.*BREW2009. [Workshop
    handout](http://csbi.ltdk.helsinki.fi/pub/anduril/BREW2009Anduril.pdf),
    [slides](http://csbi.ltdk.helsinki.fi/pub/anduril/BREW2009ASlides.pdf)
-   Kristian Ovaska. [*Component-based workflow framework for gene
    expression microarray
    analysis*](http://csbi.ltdk.helsinki.fi/pub/anduril_static/gradu.pdf).
    MSc Thesis, 2008. This provides a detailed description of Anduril
    architecture. Not fully up-to-date: refer to User Guide and
    Maintenance Guide for latest information.
-   Kristian Ovaska, Marko Laakso, Sampsa Hautaniemi. [*Component-based
    framework for bioinformatics
    analysis*](http://csbi.ltdk.helsinki.fi/pub/anduril_static/htb2008-poster.pdf).
    Poster at [High Throughput Biology
    2008](http://research.med.helsinki.fi/HTB-2008/).

## Related publications

-   Karinen S, Saarinen S, Lehtonen R, Rastas P, Vahteristo P, Aaltonen
    L A, Hautaniemi S. [Rule-based induction method for haploptype
    comparison and identification of candidate disease
    loci](http://genomemedicine.com/content/4/3/21). *Genome Medicine*,
    2012, 4:2.
-   Louhimo R, Lepikhova T, Monni O, Hautaniemi S. [Comparative analysis
    of algorithms for integration of copy number and expression
    data](http://www.nature.com/nmeth/journal/v9/n4/full/nmeth.1893.html).*Nature
    Methods*, 2012 Feb; 9(4):351-5.
-   Anna-Maria Lahesmaa-Korpinen, Sari E. Jalkanen *et al.* [FlowAnd:
    Comprehensive Computational Framework for Flow Cytometry Data
    Analysis](http://www.omicsonline.org/0974-276X/JPB-04-245.php).
    *Journal of Proteomics & Bioinformatics*, 2011, 4(197): 245-249,
    [website](http://csbi.ltdk.helsinki.fi/flowand)
-   Biswajyoti Sahu, Marko Laakso *et al.* [Dual role of FoxA1 in
    androgen receptor binding to chromatin, androgen signaling and
    prostate
    cancer](http://www.nature.com/emboj/journal/vaop/ncurrent/full/emboj2011328a.html).
    *EMBO Journal*, 2011, 30(19): 3962-3976
-   Päivi Pihlajamaa, FP Zhang, *et al.* [The phytoestrogen genistein is
    a tissue-specific androgen receptor
    modulator](http://endo.endojournals.org/content/early/2011/08/24/en.2011-0221.abstract).
    *Endocrinology*, 2011
-   Hans Blom, Daniel Rönnlund *et al.* [Nearest neighbor analysis of
    dopamine D1 receptors and Na+-K+-ATPases in dendritic spines
    dissected by STED
    microscopy](http://onlinelibrary.wiley.com/doi/10.1002/jemt.21046/full).
    *Microscopy Research and Technique*, 2011
-   Pauliina I. Ehlers, Anne S. Kivimäki *et al.* [High blood
    pressure-lowering and vasoprotective effects of milk products in
    experimental
    hypertension](http://journals.cambridge.org/action/displayAbstract?fromPage=online&aid=8272199).
    *British Journal of Nutrition*, 2011, [in print]
-   Pilvi Maliniemi and Emilia Carlsson *et al.* [NAV3 copy number
    changes and target genes in basal and squamous cell
    cancers](http://onlinelibrary.wiley.com/doi/10.1111/j.1600-0625.2011.01358.x/abstract).
    *Experimental Dermatology*, 2011
-   Ping Chen, Tatiana Lepikhova *et al.* [Comprehensive Exon Array Data
    Processing Method for Quantitative Analysis of Alternative Spliced
    Variants](http://nar.oxfordjournals.org/content/early/2011/07/09/nar.gkr513.full).
    *Nucleic Acids Research*, 2011, 39(18):
    e123, [website](http://csbi.ltdk.helsinki.fi/meap)
-   D.P. Mathiasen, C. Egebjerg *et al.* [Identification of a c-Jun
    N-terminal kinase 2 dependent signal amplification cascade that
    regulates c-Myc levels in Ras
    transformation](http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2011230a.html).
    *Oncogene*, 2011
-   Sirkku Karinen, Tuomas Heikkinen *et al.* [Data Integration Workflow
    for Search of Disease Driving Genes and Genetic
    Variants](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0018636).
    *PLoS ONE*, 2011, 6(4),
    [website](http://csbi.ltdk.helsinki.fi/pub/CANGES/)
-   Mira Heinonen, Annabrita Hemmes *et al.* [Role of RNA binding
    protein HuR in ductal carcinoma *in situ* of the
    breast](http://onlinelibrary.wiley.com/doi/10.1002/path.2889/abstract).
    *The Journal of Pathology*, 2011, 224(6): 529-539
-   Riku Louhimo, Sampsa Hautaniemi, [CNAmet: an R package for
    integrating copy number, methylation and expression
    data](http://bioinformatics.oxfordjournals.org/content/27/6/887). *Bioinformatics*,
    2011, 27(6): 887-888, [website](http://csbi.ltdk.helsinki.fi/CNAmet)
-   Marko Laakso, Sampsa Hautaniemi, [Integrative Platform to Translate
    Gene Sets to
    Networks](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/26/14/1802). *Bioinformatics*,
    2010,
    26(14):1802-1803, [website](http://csbi.ltdk.helsinki.fi/moksiskaan)

-   [Full list of
    publications](http://research.med.helsinki.fi/gsb/hautaniemi/publications.html).


## Case study

Anduril scripts used in the [case
study](http://genomemedicine.com/content/2/9/65) (see Publications) for
the glioblastoma multiforme dataset (source: [Cancer Genome
Atlas](http://cancergenome.nih.gov/)) are listed below.

Results can be browsed at [Case Study
Website](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/tcga-gbm/index.html)
(the search function requires JavaScript).

-   [Main
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/Main.and)
-   [Settings](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/Settings.and)
-   [Array CGH
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/ACGHanalysis.and)
-   [Array CGH analysis function collection
    1](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/ACGHfrequencies.and)
-   [Array CGH analysis function collection
    2](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/ACGHSegments.and)
-   [Array CGH analysis function collection
    3](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/ACGHSegmentAnalysis.and)
-   [Array CGH integration to
    expression](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/ACGHexonIntegration.and)
-   [Gene expression
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/GeneExpression.and)
-   [Transcript expression analysis based on median exon
    expression](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/MedianExonExpression.and)
-   [Methylation status
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/Methylation.and)
-   [MicroRNA expression
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/MiRNAAnalysis.and)
-   [Genotype specific survival
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/SNPSurvival.and)
-   [Transcript expression
    analysis](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/TranscriptExpression.and)
-   [Summary website
    generator](http://csbi.ltdk.helsinki.fi/pub/anduril_casestudy/SummaryTable.and)

## TCGA analysis results

We have analyzed four cancer data sets from the Cancer Genome Atlas. The
following cancer result websites are currently available and more will
be added.

-   [Breast invasive carcinoma
    (BRCA)](http://csbi.ltdk.helsinki.fi/brca/)
-   [Colorectal adenocarcinoma
    (COAD)](http://csbi.ltdk.helsinki.fi/pub/coad/)
-   [Glioblastoma multiforme (GBM)](http://csbi.ltdk.helsinki.fi/gbm/)
-   [Temozolomide treated subpopulation of
    GBM](http://csbi.ltdk.helsinki.fi/camda/) and
-   [Ovarian serous cystadenocarcinoma
    (OV)](http://csbi.ltdk.helsinki.fi/ov/).

With the exception of methylation data, we have used raw data (TCGA
level 1, e.g., CEL-files) for all the different analysis platforms.

