# Readme


This is the contents of anduril.org site. Online site is updated daily.

# Requirements

* python3 package mkdocs==1.0.4

Install with `pip3 install --user --upgrade mkdocs`.
You may encounter trouble with pyyaml libraries, install with
`sudo apt-get install libyaml-cpp-dev`

* Some markdown extensions are separate packages, like

`pip3 install --user markdown-include`

# Show the website

run `mkdocs serve`  and open the link the command prints


# Other notes

Check for broken links:

`wget --spider -r -nd -nv -H -l 1 -w 2 -o run1.log  http://your_server_ip/`

