Anduril logo (anduril-logo*.svg) uses the "Lovelo" font
- Author: Hans Renzler
- License: free for commercial use
- URL: http://www.fontfabric.com/lovelo-font/

- Light orange: http://www.color-hex.com/color/ffb546
- Dark orange: http://www.color-hex.com/color/eb6000


