#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object ComponentNaming {

    // 1. Name: data
    val data = INPUT(path = "data.csv")

    val mySequence = NamedSeq[INPUT]("dataSeq")
    val myMap = NamedMap[INPUT]("dataMap")

    for (key <- Seq("data1", "data2")) {
        withName(key) {
            // 2. Names: data1-encapsulated, data2-encapsulated
            val encapsulated = INPUT(path = key+".csv")
        }

        // 3. (Seq) Names: dataSeq_0, dataSeq_1
        mySequence += INPUT(path = key+".csv")

        // 3. (Map) Names: dataMap_data1, dataMap_data2    
        myMap(key) = INPUT(path = key+".csv")
    }

    // 4. Names: embedded (CSVSort), embedded-in (INPUT)
    val embedded = CSVSort(
        INPUT(path = "data.csv")
    )

    // 5. Name: explicitName
    CSVSort(data, _name = "explicitName")

    // 6. Name: fromFunction-sorted
    def subFunction() = {
        val sorted = CSVSort(data)
        // Or any other pattern from above
    }
    val fromFunction = subFunction()
}
