# 10. Reference: Component naming

When using Scala to insert components on a workflow, you need to follow basic
code patterns that ensure that each component gets a consistent and human-readable
name in the workflow. This allows tracing components between Scala
code and workflow results (log messages and execution folder), and is required
by Anduril to correctly determine if a component has been changed and needs to
be re-executed.

Summary of supported Scala patterns:

Code | Component name(s)
---- | -----------------
`val simple = INPUT("data.csv")`                                            | simple
`val map = NamedMap[INPUT]("parent")`<br>`map("child") = INPUT("data.csv")` | parent_child
`val seq = NamedSeq[INPUT]("parent")`<br>`map += INPUT("data.csv")`         | parent_0
`withName("parent")`<br>`{ val child = INPUT("data.csv") }`                 | parent-child
`val parent = CSVSort(INPUT("data.csv"))`                                   | parent, parent_in
`def f(x) = { val child = INPUT("data.csv") }`<br>`val parent = f()`        | parent-child
`def f(x) = { val child = INPUT("data.csv") }`<br>`f()`                     | child
`INPUT("data.csv", _name = "explicit")`                                     | explicit

These patterns are illustrated in the following code:

```scala
{!docs/workflows/component-naming/component-naming.scala!}
```

## Explanation of Scala patterns

### Basic pattern: val

The most basic pattern in `val`. The component constructor `val name =
Component(` must be on the same line as `val`, but the argument list can
extend over multiple lines.

### Iteration: NamedMap, NamedSeq, withName

[NamedMap](http://anduril.org/pub/api/scala/#org.anduril.runtime.NamedMap),
[NamedSeq](http://anduril.org/pub/api/scala/#org.anduril.runtime.NamedSeq) and `withName`
are used in [iterative structures](../iteration/).

### Embedded calls

Embedded calls like `f(g(x))` are supported supported to *one* level deep.
`g(x)` does not have to be on the same line as `f`. `f(g(h(x)))` (two levels)
is not supported.

### Function calls

Arbitrarily nested function calls are supported, and result in names like
parent1-parent2-child. Inside functions, the hierarchical prefix (such as
parent1-parent2) is inserted to all generated names. All supported naming
patterns are available in functions.

### Explicit naming

The `_name` annotation can be used in the component constructor to assign
explicit names. This always gives proper names, but should be avoided due to
the manual work involved.

## Invalid solutions

The following code demonstrates some antipatterns that do **not** result in
consistent names. The problems are:

1. Orphan component that has no `val` or `var`, or `_name` annotation. Correcting: use `val` or `_name`.
2. `val` is on a different line than component definitions. Correcting: use `_name` or fit on one line.
3. Name is reused in a `for` loop. Correcting: use `withName`, `NamedSeq` or `NamedMap`.
4. Component is inserted into a plain Scala collection (`Seq`, `Map`, etc.). Correcting: use `NamedSeq` or `NamedMap`.

```scala
{!docs/workflows/component-naming/component-naming-bad.scala!}
```
