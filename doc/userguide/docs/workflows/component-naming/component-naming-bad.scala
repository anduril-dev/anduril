#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object ComponentNamingBad {

    // 1. BAD EXAMPLE: orphan
    INPUT(path = "data.csv")

    // 2. BAD EXAMPLE: different line
    val flag = true
    val conditional = if (flag) {
        INPUT(path = "data1.csv")
    } else {
        INPUT(path = "data2.csv")
    }

    // 3. BAD EXAMPLE: name reused
    for (key <- Seq("data1", "data2")) {
        val data = INPUT(path = key+".csv")
    }

    // 4. BAD EXAMPLE: plain collection
    val plainSeq = scala.collection.mutable.Seq[INPUT]
    for (key <- Seq("data1", "data2")) {
        plainSeq += INPUT(path = key+".csv")
    }
}
