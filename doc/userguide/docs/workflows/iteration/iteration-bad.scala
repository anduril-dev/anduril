#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._
import scala.collection.mutable.Map

object IterationBad {
    val samples = Map("sample1" -> "data1.csv", "sample2" -> "data2.csv")
    val filteredMap = Map[String, CSV]()

    for ((sampleID, filename) <- samples) {
        val input = INPUT(path = filename)
        val filtered = CSVFilter(input, regexp = "QualityOK=1")
        filteredMap(sampleID) = filtered.out
    }

    val joined = CSVListJoin(in = filteredMap)
}
