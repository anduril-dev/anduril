#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._
import scala.collection.mutable.Map

object IterationCSV {
    val filteredMap = Map[String, CSV]()

    for (row <- iterCSV("metadata.csv")) {
        val sampleID = row("SampleID")
        val filename = row("Filename")
        withName(sampleID) {
            val input = INPUT(path = filename)
            val filtered = CSVFilter(input, regexp = "QualityOK=1")
            filteredMap(sampleID) = filtered
        }
    }

    val joined = CSVListJoin(in = filteredMap)
}
