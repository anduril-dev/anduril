#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object IterationNamedMap {
    val samples = Map("sample1" -> "data1.csv", "sample2" -> "data2.csv")
    val inputMap = NamedMap[INPUT]("input")
    val filteredMap = NamedMap[CSVFilter]("filtered")

    for ((sampleID, filename) <- samples) {
        inputMap(sampleID) = INPUT(path = filename)
        filteredMap(sampleID) = CSVFilter(inputMap(sampleID), regexp = "QualityOK=1")
    }

    val joined = CSVListJoin(in = filteredMap)
}
