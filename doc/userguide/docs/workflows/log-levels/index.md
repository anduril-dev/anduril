# 9. Log levels

Anduril has three levels of logging. They can be used to display messages in the workflow, but also warnings and errors.
In addition, error message also stops the workflow from running.

The loggers are found in package `org.anduril.runtime` - make sure you import it.

## Info

The info() works like println(), but it adds a prefix for the log pager. The
prefix allows the log pager to color the output white.

The info() function can print any object, which makes it a good debugging tool.

## Warning

The warning() function differs from info() only by the color in the log pager.
The pager colors it yellow.

## Error

The error function, in addition to being colored red in the pager, also 
throws an exception. It stops the current workflow, and exits with error messages.

The following example shows an example of these functions.

```scala
{!docs/workflows/log-levels/log-levels.scala!}
```
