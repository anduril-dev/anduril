#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object ErrorHandling {
  var volume = 11

  info(s"Volume selected: $volume")

  volume match {
    case x if x > 10 => {
      // This will stop the workflow. The component instance at the end will not run.
      error("Volume can not go higher than 10")
    }
    case x if x > 8 => {
      warning(s"Volume $volume is dangerously high!")
    }
    case _ => {}
  }

  val setVolume = QuickBash(script=s"echo set volume $volume > out")

}
