#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object DynamicArrayPort {
  val data1 = INPUT(path = "data1.csv")
  val data2 = INPUT(path = "data2.csv")
  val myData = Map(
    "sample1" -> data1, 
    "sample2" -> data2
  )

  val array = makeArray(myData)

  for ((key, file) <- iterArray(array)) {
    info("%s = %s".format(key, file))
  }

  val filtered = CSVFilter(
    array("sample1"), 
    regexp = "QualityOK=1"
  )
}
