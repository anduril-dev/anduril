#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Dynamic {
  info("Beginning workflow construction")

  val metadata = INPUT(path = "metadata.csv")
  val selectScript = INPUT(path = "select-samples.sh")
  val filteredMetadata = BashEvaluate(
    command=selectScript, 
    var1=metadata
  )

  for (row <- iterCSV(filteredMetadata.stdOut)) {
    withName(row("SampleID")) {
      val input = INPUT(path = row("Filename"))
        // Further processing omitted
    }
  }

  info("Ending workflow construction")
}
