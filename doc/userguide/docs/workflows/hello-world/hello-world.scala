#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object HelloWorld {
  info("Beginning workflow construction")
  val helloWorld = BashEvaluate(script = "echo Hello world!")
  info("Ending workflow construction")
}
