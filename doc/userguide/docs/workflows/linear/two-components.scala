#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object TwoComponents {
    val input = INPUT(path = "data.csv")
    val sorted = CSVSort(input.out, types = "Gene=string")
}
