def CSVSort(in: anduril.builtin.CSV, types: String = ""): CSVSort = { ... }

class CSVSort extends org.anduril.runtime.Component {
    val out: anduril.builtin.CSV = ...
    val status: anduril.builtin.TextFile = ...
    ...
}

type anduril.builtin.CSV = org.anduril.runtime.Port
type anduril.builtin.TextFile = org.anduril.runtime.Port
