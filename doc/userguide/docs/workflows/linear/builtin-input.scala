/** Create an instance of the INPUT component, and place it on the workflow. */
def INPUT(path: String, recursive: Boolean = true): INPUT = {
    // 1. Create an instance of INPUT class using given parameters
    // 2. Place the instance on the workflow
    // 3. Return the instance
}

/** Encapsulate the implementation of the INPUT component. Provide access
 * to output ports of the component. */
class INPUT extends org.anduril.runtime.Component {
    /** Handle to the imported file or folder. */
    val out: org.anduril.runtime.Port = ...
    ...
}
