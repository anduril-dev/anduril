#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object ArrayPort {
  val data1 = INPUT(path = "data1.csv")
  val data2 = INPUT(path = "data2.csv")
  val myData = Map("sample1" -> data1, "sample2" -> data2)

  // Implicitly create an array index
  val joined = CSVListJoin(in = subData)
  // Explicit alternative
  val joinedExplicit = CSVListJoin(in = makeArray(myData))
}
