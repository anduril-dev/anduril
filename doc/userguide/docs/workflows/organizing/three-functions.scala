#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object JoinFunctions {
    def importData(): NamedMap[CSV] = {
        val allData = NamedMap[CSV]("data")
        allData("data1") = INPUT(path = "data1.csv")
        allData("data2") = INPUT(path = "data2.csv")
        allData("data3") = INPUT(path = "data3.csv")
        allData
    }

    def joinCSV(file1: CSV, file2: CSV): CSV = {
        val joined = CSVJoin(file1, file2, intersection = false)
        val sorted = CSVSort(joined.out, types = "Gene=string")
        return sorted.out
    }

    val data = importData()
    val joined12 = joinCSV(data("data1"), data("data2"))
    val joined23 = joinCSV(data("data2"), data("data3"))
    OUTPUT(joined12)
    OUTPUT(joined23)
}
