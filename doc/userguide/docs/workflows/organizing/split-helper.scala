import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object JoinLibrary {
    def joinCSV(file1: CSV, file2: CSV): CSV = {
        val joined = CSVJoin(file1, file2, intersection = false)
        val sorted = CSVSort(joined.out, types = "Gene=string")
        return sorted.out
    }
}
