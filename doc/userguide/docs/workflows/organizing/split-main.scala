#!/usr/bin/env anduril
//$OPT -s split-helper.scala

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object SplitMain {
    def importData(): NamedMap[CSV] = {
        val allData = NamedMap[CSV]("data")
        allData("data1") = INPUT(path = "data1.csv")
        allData("data2") = INPUT(path = "data2.csv")
        allData("data3") = INPUT(path = "data3.csv")
        allData
    }

    val data = importData()
    val joined12 = JoinLibrary.joinCSV(data("data1"), data("data2"))
    val joined23 = JoinLibrary.joinCSV(data("data2"), data("data3"))
    OUTPUT(joined12)
    OUTPUT(joined23)
}
