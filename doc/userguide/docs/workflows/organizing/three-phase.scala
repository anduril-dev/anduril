#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object ThreePhase {
    // Import
    val data1 = INPUT(path = "data1.csv")
    val data2 = INPUT(path = "data2.csv")

    // Processing
    val joined = CSVJoin(data1, data2, intersection = false)

    // Output
    OUTPUT(joined.out)
}
