#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Annotations {
  val input1 = INPUT(path = "data1.csv")
  input1._enabled = false

  val input2 = INPUT(path = "data2.csv")

  val sorted = CSVSort(input1.out, types = "Gene=string", _name = "mySorted")
  sorted._bind(input2)
  sorted._custom("cpu") = "4"
  sorted._execute = "once"
  sorted._filename("out", "mySortedValue.csv")
  sorted._priority = 1
}
