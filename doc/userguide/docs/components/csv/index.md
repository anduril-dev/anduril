# CSV/TSV processing

*Click component name to see manual page*

Component                                                                                          | Bundle | Description
---------------------------------------------------------------------------------------------------|--------|-------------------------------------
[CSV2Excel](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSV2Excel)           | tools  | Export a CSV file to Excel
[CSVCleaner](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVCleaner)         | tools  | Convert between CSV formats
[CSVFilter](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVFilter)           | tools  | Filter rows and columns; rename columns
[CSVJoin](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVJoin)               | tools  | Join CSV files using a key column
[CSVListJoin](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVListJoin)       | tools  | Append CSV files without using keys
[CSVSort](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVSort)               | tools  | Sort CSV file
[CSVSplit](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVSplit)             | tools  | Partition a CSV file into a CSV array
[CSVTransformer](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.CSVTransformer) | tools  | Transform CSV file using an R expression
[Excel2CSV](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.Excel2CSV)           | tools  | Import a CSV file from Excel
[PythonEvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.PythonEvaluate) | tools  | Process CSV file using streaming
[ScalaEvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.ScalaEvaluate)   | tools  | Process CSV file using streaming (high performance)
[TableQuery](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.TableQuery)         | tools  | Invoke SQL query on CSV files

## Overview

Anduril has extensive facilities for working with tabulated files. The tools bundle specifies a CSV file with a certain format: fields are delimited by tabs, the first row contains column names, and values are optionally quoted with `"`.

## Import/export

CSVCleaner can be used to import/export files in different CSV/TSV dialects. CSV2Excel and Excel2CSV provide two-way conversion with Microsoft Excel.

## Filtering and transforming

There are multiple components for processing CSV files: some with a limited purpose but simple interface, and some with flexible options but more complex interface. Most flexibility is provided using [external scripts](../external/).

Simple components: CSVFilter, CSVJoin, CSVListJoin, CSVSort, CSVSplit

Flexible components: CSVTransformer, PythonEvaluate, ScalaEvaluate, TableQuery
