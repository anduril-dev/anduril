#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Excel {
    val data1 = INPUT("data1.csv")
    val data2 = INPUT("data2.csv")
    val style = INPUT("style.csv")
    val report = CSV2Excel(data1, data2, style = style, sheetNames = "Data 1,Data 2")
}
