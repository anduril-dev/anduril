#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object PDF {
    val data = Randomizer(distribution = "normal", rows = 20, columns = 5)
    val boxPlot = BoxPlot(data)
    val barPlot = Plot2D(data, plotType = "bar")

    val template = LatexTemplate(title = "Anduril report", printTOC = true)
    val document = LatexCombiner(boxPlot.out, barPlot.out, sectionTitle = "Plots")
    val pdf = LatexPDF(document, header = template.header, footer = template.footer)
}
