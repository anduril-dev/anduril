#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object PythonInline {
    val data = INPUT("data.csv")
    val result = python"""|
                          |import anduril
                          |value_sum = 0
                          |for row in anduril.TableReader(${data}):
                          |    value_sum += row['Value']
                          |tableout.set_fieldnames(['ValueSum'])
                          |tableout.writerow([value_sum])"""
}
