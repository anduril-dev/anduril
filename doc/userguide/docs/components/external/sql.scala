#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object SQL {
    val data1 = INPUT("data1.csv")
    val data2 = INPUT("data2.csv")

    val qualityCondition = 1
    val joined = sql"""
        SELECT data1."Gene",
               (data1."Value"+data2."Value")/2 AS "MeanValue"
        FROM ${data1} AS data1,
             ${data2} AS data2
        WHERE data1."Gene" = data2."Gene"
          AND data1."QualityOK" = ${qualityCondition}
          AND data2."QualityOK" = ${qualityCondition}"""
}
