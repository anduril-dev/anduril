#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Python {
    val data = INPUT("data.csv")
    val script = INPUT("script.py")
    val result = PythonEvaluate(scriptIn = script, table1 = data)
}
