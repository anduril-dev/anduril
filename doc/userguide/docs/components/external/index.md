# External scripts

*Click component name to see manual page*

Component                                                                                          | Shorthand     | Bundle | Description
---------------------------------------------------------------------------------------------------|---------------|--------|------------
[BashEvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.BashEvaluate)     | `bash"..."`   | tools  | Invoke Bash script
[PythonEvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.PythonEvaluate) | `python"..."` | tools  | Invoke Python script
[MatlabEvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.MatlabEvaluate) |               | tools  | Invoke Matlab script
[QuickBash](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.QuickBash)           |               | tools  | Invoke Bash script with fewer inputs
[REvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.REvaluate)           | `R"..."`      | tools  | Invoke R script
[ScalaEvaluate](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.ScalaEvaluate)   | `scala"..."`  | tools  | Invoke Scala script
[TableQuery](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=tools.TableQuery)         | `sql"..."`    | tools  | Invoke SQL query on CSV files

## Overview

Anduril provides facilities to invoke scripts written in external languages. The components that provide this all have similar interfaces: they take a script and a number of data files as input, and produce a number of data files as output.

There are two ways to call these components. First, they can be invoked as regular components using the function call syntax of Scala. Second, they have shorthand syntax using Scala [string interpolation](http://docs.scala-lang.org/overviews/core/string-interpolation.html) that makes common use case more convenient. Regular invocation is needed if you want to place the external script in its own file or need to modify parameters; otherwise, shorthand syntax can be used.

The general form of the shorthand syntax is `prefix"script"` or `prefix"""long script"""` (for multi-line strings). Here, `prefix` is a language-dependent identifier such as `bash` or `python`. You can use Scala variables inside the strings using `${variable}`: they are expanded to the value of the variable. Scala expressions such as `${component.port}` are also supported. When expanding output ports of components, Anduril also inserts dependencies to the workflow.

These components are in the tools bundle, so remember to put `import anduril.tools._` in your scripts.

## Bash

The script below shows three equivalent ways of filtering a CSV file using grep to obtain lines that contain "gene01". The shorthand syntax `bash"..."` expands the script inside the quotes using the variables `grepArguments` and `data` to produces a final command like `grep gene01 /home/user/data/data.csv`. Dependencies are properly configured: `filtered1` depends on `data` in the workflow.

QuickBash is a simplified interface with fewer ports and is suitable when you have one input (visible in Bash as `$in`) and one output (`$out`). BashEvaluate is the verbose version of the shorthand syntax, in which parameter substitution is done using templates of the form `@arg@`.

The `multiple` component demonstrates chaining Bash calls together, having multiple statements in the script, and writing to multiple output ports.

```scala
{!docs/components/external/bash.scala!}
```

## SQL

In the following example, we have two CSV files with columns Gene, Value and QualityOK, and want to compute the mean Value for genes that are present in both files and have QualityOK = 1. TableQuery and `sql"..."` construct a temporary in-memory database that can be used for executing a query. We use the multi-line form of string interpolation with triple quotation marks. The `sql` shorthand syntax expands file references like `data1` into table names like `table1`; we can rename them using `AS` to be explicit. The default SQL engine is [HSQLDB](http://hsqldb.org/).

```scala
{!docs/components/external/sql.scala!}
```

## Python

In this example, we compute the sum of Value columns in a CSV file, and write the result into a CSV file. PythonEvaluate provides access to an [Anduril API](http://anduril.org/pub/api/python/) that can be used for such tasks, and pre-populates certain variables. Alternatively, the Python standard library (such as `csv`) or external libraries can be used.

Here, we chose to place the Python script into an external file and invoke PythonEvaluate using the verbose syntax. The Python script is:

```python
{!docs/components/external/script.py!}
```

Workflow configuration:

```scala
{!docs/components/external/python.scala!}
```

We can also use the shorthand syntax. Here, we have to be take care of proper indentation, because Python uses whitespace to mark code structure. The `python"..."` syntax supports the Scala `stripMargin` feature, in which whitespace before initial `|` is removed. Note that this feature is specific to the Python shorthand syntax. Also, the data file is imported as a generic file, not a CSV table, so we manually construct a CSV iterator.

```scala
{!docs/components/external/python-inline.scala!}
```
