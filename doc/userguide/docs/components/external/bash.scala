#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Bash {
    val data = INPUT("data.csv")
    val grepArguments = "gene01"
    val filtered1 = bash"grep ${grepArguments} ${data.out}"
    val filtered2 = QuickBash(script = "grep " + grepArguments + " $in > $out",
                              in = data)
    val filtered3 = BashEvaluate(script = "grep @param1@ @var1@",
                                 var1 = data,
                                 param1 = grepArguments)

    val multiple = bash"""
        cat ${filtered1.stdOut} ${filtered2.out} ${filtered3.stdOut} > @out1@
        echo OK > @out2@"""
}
