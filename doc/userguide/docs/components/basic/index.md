# Basic components

*Click component name to see manual page*

Component                                                                                            | Bundle  | Description
-----------------------------------------------------------------------------------------------------|---------|-------------------------------------
[ArrayExtractor](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.ArrayExtractor) | builtin | Extract files from an array
[Array2Folder](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.Array2Folder)     | builtin | Convert an Anduril array into a folder
[Folder2Array](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.Folder2Array)     | builtin | Convert a folder into an Anduril array
[INPUT](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.INPUT)                   | builtin | Import a local data file to workflow
[OUTPUT](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.OUTPUT)                 | builtin | Export a workflow result file to `output/`
[StringInput](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.StringInput)       | builtin | Import contents of a string as a file
[URLInput](http://www.anduril.org/anduril/bundles/all/doc/index.html?c=builtin.URLInput)             | builtin | Import a remote data file to workflow

## Input & output

All data files need to be explicitly imported to workflows. Usually this is done using INPUT. StringInput is useful when a component takes a text file as input, and the contents of that file can succintly be written inline in the Scala code.

OUTPUT helps locating the most important result files by copying them to the `output/` folder in the execution folder.

See [Organizing workflows](../../workflows/organizing/) for common patterns of using INPUT and OUTPUT.

## Folder/array conversion

[Anduril arrays](../../workflows/array/) are structurally close to regular folders, and two-way conversions between them are available. Folder2Array is useful to import a folder of data files into an array, and Array2Folder can export an array for an external program that takes a folder as an input.
