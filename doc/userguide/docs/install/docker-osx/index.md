# Installation with OS X and Docker

This is a short instruction on how to install Anduril on OS X using Docker and how to test file sharing between OS X and Docker container and how to test the Anduril installation.

## Install Docker Desktop

 1. Create yourself a Docker user account in [Docker Hub](https://hub.docker.com/signup).
 2. Download [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac).
 3. Follow the instructions from [Docker documentation](https://docs.docker.com/docker-for-mac/install/) to install Docker Desktop.
 4. Sing in to Docker Desktop using your username and password.

## Configure Docker Desktop

If you want to use port forwarding using local IP such as 127.0.0.1, set the host ip for port forwarding in Docker's ** Preferences > Daemon > Advanced** by adding line "ip" : "<ip for port forwarding\>" to the configuration file, for example:

```sh
{
  "debug" : true,
  "ip" : "127.0.0.1",
  "experimental" : true
}
```

## Start the Anduril container

Container can be started with command

```sh
	docker run -p 8000:8000 -td -v $(pwd)/shared:/opt/shared anduril/core bash
```

* **-p** parameter sets port forwarding from host to container using ports 8000 and the IP configured above.
* **-v** parameter connects host computer's folder ~/shared to the Anduril container's /opt/shared folder.

This should start downloading Anduril container if it is not installed and show the following messages:

```sh
	Unable to find image 'anduril/core:latest' locally
	latest: Pulling from anduril/core
	6cf436f81810: Downloading [=========>
	...
```

The name and ID of the running container can be obtained with command

```sh
	docker container ls
```

You should obtain a list with following headers:

```sh
	CONTAINER ID        IMAGE           ...   PORTS                      NAMES
	3bd695dbc87d        anduril/core    ...   127.0.0.1:8000->8000/tcp   kind_yonath
```

## Connect to a running Anduril container

Now you can connect from terminal to Anduril container with the container name (or id):

```sh
	docker exec -it kind_yonath /bin/bash
```

You should be connected to the container and see a command prompt such as:

```sh
	root@7a7652bd7e82:/anduril#
```

## Test the file sharing:

In your local machine, create a file, for example *workflow.scala* in **~/shared** folder with the following contents:

```sh
#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object HelloWorld {
  info("Beginning workflow construction")
  val helloWorld = BashEvaluate(script = "echo Hello world!")
  info("Ending workflow construction")
}
```
In Anduril container, check if the file exists:

```sh
	cd /opt/shared
	ls
```

## Test the Anduril:

Run the following command in **/opt/shared** in Anduril container:

```sh
	anduril run workflow.scala
```

This should yeald result as described in [Getting started: workflows](http://anduril.org/userguide/workflows/hello-world/). The same result can be obtained when the file is made executable:

```sh
	chmod +x workflow.scala
```

and run with command.

```sh
	./workflow.scala
```
