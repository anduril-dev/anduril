# Command-line usage

The Anduril command line tool is `anduril`. It uses a sub-command syntax: `anduril COMMAND args`.

To see all commands, type: `anduril -h`. It prints the following commands. Some commands are for end users and some for developers who extend Anduril.

```text
- build: Build Anduril engine and/or component bundles
- build-doc: Generate component and API documentation in HTML format.
- doc: Print component documentation
- install: Install and build Anduril engine, environment and/or external component bundles
- install-dep: Install libraries and external resources for Anduril and bundles.
- run: Run workflow
- shell: Run extra shell commands in an Anduril environment (for advanced usage)
- test: Run test cases for components
- test-workflow: Run test cases for test workflows
```

To see options for a specific sub-command (such as `run`), type: `anduril run -h`.

## Running workflows

How to construct workflows is covered in the Section [Creating workflows](../../workflows/hello-world/). For now, let's assume we have the following workflow in the file workflow.scala:
```scala
{!docs/workflows/hello-world/hello-world.scala!}
```

To run a workflow stored in workflow.scala, type: `anduril run workflow.scala`.

If the workflow source file starts with a Unix shebang line `#!/usr/bin/env anduril` (as above), you can alternatively just run: `./workflow.scala`. Ensure that the .scala file is executable (`chmod +x workflow.scala`).

You can pass several options to `anduril run` that modify workflow execution. For example:

- Set the number of concurrent components: `--threads 16` (default: 4)
- Use a custom execution folder: `-d my-execution-folder`
- Store and re-use compiled JAR bytecode file: `-j workflow.jar`
- Pipe Anduril log messages through a formatter: `--pipe 'tee saved-log'`
- Run Anduril components through a wrapper such as Slurm/srun: `--wrapper srun`

## Installing and updating bundles

To install a new component bundle, type `anduril install BUNDLE-NAME`. Example: `anduril install tools`. To see available bundles, run `anduril install -h`. By default, a stable release version is installed. To install a potentially unstable development version, add the `--dev` flag.

To install external dependencies of the bundles you have installed, use `anduril install-dep -b BUNDLE-NAME`. As this command installs global dependencies, you need to provide either `--sudo` (if your user account has sudo privileges) or `--admin` (if you are running as root). You can specify component names with `-c`. Example: `anduril install-dep -b tools -c Rsync -c KaplanMeier --sudo` installs dependencies for the Rsync and KaplanMeier components, and invokes `sudo` when needed.

To update an installed bundle, type: `anduril build -u BUNDLE-NAME`. Example: `anduril build -u tools`. You can update the Anduril core using: `anduril build -u anduril`.

## Generating component documentation

To generate component documentation locally in HTML format, type: `anduril build-doc --component bundle-doc`.
