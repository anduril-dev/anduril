# Installation

Anduril is developed and tested on Linux. This User Guide includes example commands for Ubuntu, but other Linux distributions (e.g., Red Hat Enterprise Linux) work equally well.

## Using Docker

The easiest way to get started is using [Docker](https://www.docker.com/).

```sh
docker run -ti --rm anduril/core anduril -h
```

The above command prints the help message of the Anduril command line tool.

To make the Docker environment more useful, you can import a host folder (`./shared`) into the container (`/opt/shared`) using the following:

```sh
docker run -ti --rm -v $(pwd)/shared:/opt/shared anduril/core bash
```

## Native installation

Native installation may be preferred for production environments, such as for cluster deployment.

External dependencies of Anduril core and important components are:

Name        | Ubuntu 16.04 package
------------|---------------------
Apache Ant  | ant
Java 7 or 8 | openjdk-8-jdk
Mercurial   | mercurial
Python 2.7  | python
R 3.0+      | r-base-dev

### Installation on Ubuntu

Paste the following script into a Bash shell. To install into a different folder than `~/anduril`, modify lines two and three.

```sh
sudo apt-get install ant mercurial git default-jdk python r-base-dev
git clone https://bitbucket.org/anduril-dev/anduril --branch stable ~/anduril
export ANDURIL_HOME=~/anduril
export PATH="$ANDURIL_HOME/bin:$PATH"
anduril build
anduril install tools
```

To get started, see the help message of the Anduril command line tool:
```sh
anduril -h
```

### Troubleshooting

#### Error message: `git: command not found`

You do not have Git installed. Install it prior to invoking `git`.

#### Error message: `anduril: command not found`

You must add $ANDURIL_HOME/bin to your PATH, where $ANDURIL_HOME is the installation folder. Alternatively, you can call $ANDURIL_HOME/bin/anduril using the full path.

#### Error message: `Error: Unable to access jarfile anduril.jar`

(1) Your Anduril installation folder is not found correctly. Set the $ANDURIL_HOME environment variable to point to the folder that contains files such as anduril.jar. Ensure the environment variable is setup when you log in (e.g., by placing it into `~/.bashrc`).

(2) Your Anduril engine is not built. Build with `anduril build`.

## Installing additional component bundles

In addition to the core system that provides workflow facilities, Anduril includes component bundles ("modules") for various analysis tasks. You can start with a basic system and add features if you need them.

Anduril developers distribute the following component bundles. In addition, Anduril is fully extensible by third parties, so you can write your own component bundles or find third-party distributed bundles.

Bundle                                                     | Purpose
-----------------------------------------------------------|---------------------------------
[anima](https://bitbucket.org/anduril-dev/anima)           | Image analysis
[flowand](https://bitbucket.org/anduril-dev/flowand)       | Flow cytometry (FACS) analysis
[microarray](https://bitbucket.org/anduril-dev/microarray) | DNA/RNA microarray analysis
[sequencing](https://bitbucket.org/anduril-dev/sequencing) | DNA/RNA sequencing analysis (NGS)
[tools](https://bitbucket.org/anduril-dev/tools)           | Commonly used tools. Already installed above.

Bundles are installed using `anduril install BUNDLE-NAME(S)`. For example, to install the microarray and sequencing bundles, type:
```sh
anduril install microarray sequencing
```

This installs the Anduril-specific code, but many components depend on external libraries and command-line tools as well. External dependencies are installed using `anduril install-dep`. External dependencies can be large, so you can narrow the selection using bundle (`-b` flag) and component (`-c` flag) names. For example, to install all external depedencies of the tools bundle, run:
```sh
anduril install-dep -b tools
```

# Environment variables

Anduril recognizes the following environment variables:

- `$ANDURIL_HOME`: Points to the Anduril installation folder. This folder contains files such as anduril.jar and build.sbt.
- `$ANDURIL_BUNDLES` (usually not needed): List of folders (separated by `:`) that are searched for Anduril components. `$ANDURIL_HOME/bundles` is always searched, so it is not needed on `$ANDURIL_BUNDLES`. This variable is needed if you have multiple bundle locations, or want to override a bundle with your own version (such as when developing a bundle).

The `anduril` tool uses `$ANDURIL_HOME` to locate the installation folder, but `$ANDURIL_HOME` is not mandatory in the following cases:

- `anduril` is invoked using an absolute or relative path, and not obtained from `PATH`.
- `anduril` is obtained from `PATH`, but it is a symbolic link to the installation folder (`bin/anduril`). In this case, `anduril` can determine the installation folder by following the symbolic link
