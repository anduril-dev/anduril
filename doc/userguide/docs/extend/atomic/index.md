# Implementation: Atomic

Atomic components are implemented by writing a script or program in the language defined by the `<launcher>` element of `component.xml`. When the component is executed, it receives a properties file called a _command file_ as an argument that contains values and paths for input files, parameters and output files, as well as various metadata. This properties file is parsed by a language-specific Anduril library, and the values are exposed to the component code.

Provided language-specific libraries and links to their documentation are:

Language | Source code folder
---------|-------------------
[Bash](http://anduril.org/pub/api/bash)               | `lang/bash`
Java                                                  | `core/src/main/java/org/anduril/component`
Lua                                                   | `lang/lua`
[Matlab](http://anduril.org/pub/api/matlab)           | `lang/matlab`
Octave                                                | `lang/octave`
Perl                                                  | `lang/perl`
[Python](http://anduril.org/pub/api/python)           | `lang/python`
[R](http://anduril.org/pub/anduril/rdoc/00Index.html) | `lang/r`
[Scala](http://anduril.org/pub/api/scala)             | `core/src/main/scala/org/anduril/runtime`

## Example: R component

To implement the SimpleCSVFilter component whose [interface](../interface/) we defined, we write the following R script (`~/my-bundles/demo/components/SimpleCSVFilter/SimpleCSVFilter.r`):

```r
{!bundles/demo/components/SimpleCSVFilter/SimpleCSVFilter.r!}
```

Here we used the `componentSkeleton` R library that is provided by Anduril (`lang/r`). It parses the properties files in `main` into an object called `cf` and provides read/write functions for the CSV format defined in the tools bundle.
