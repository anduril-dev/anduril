# Component bundles

A bundle is a collection of related components, such as analysis tools for DNA sequencing or microscopy image analysis. It can be shared online, or used privately within your organization to implement custom workflows. Physically, a bundle is a folder structure that contains implementations of components and various metadata. Many parts of the folder structure are optional.

When you extend Anduril, you can either add components to an existing bundle (in coordination with the bundle maintainer), or create your own bundle. Let's create a new bundle.

## Placing a new bundle on the search path

Each bundle needs to have a (unique) name; we name our bundle `demo`. We need to create a location for the bundle that Anduril can find. Let's place our `demo` bundle in `~/my-bundles/demo`. Anduril recognizes the environment variable `ANDURIL_BUNDLES`, which contains root folders of bundle directories. We add the root folder `~/my-bundles` to the search path by executing the following in the shell:

```bash
export ANDURIL_BUNDLES=$ANDURIL_BUNDLES:~/my-bundles
```

## Basic metadata

Minimally, a bundle must contain a metadata file `bundle.xml` in the bundle main folder. This metadata specifies the names of the bundle and other bundle that this bundle depends on. The contents of `~/my-bundles/demo/bundle.xml` are:

```xml
{!bundles/demo/bundle.xml!}
```

Our bundle depends on the `tools` bundle because we use components from that bundle. You need to declare all bundle dependencies (using multiple `<depends>` elements). The bundle `builtin` is included automatically and does not need to be declared.

## Building a bundle

Bundles need to be built to generate Scala code that can be executed from workflows. We build our bundle with `anduril build demo`. Building the bundle creates the compiled archive `~/my-bundles/demo/demo.jar`. You need to rebuild the bundle when you make certain changes to the bundle.

## Using a bundle in a workflow

To use a bundle in workflow Scala code, insert `import anduril.demo._` in the header section of the code. The code generated for the bundle is located in the Scala package anduril.NAME. The tailing `_` part of the import statement imports all symbols of the package directly into the workflow scope for convenience.
