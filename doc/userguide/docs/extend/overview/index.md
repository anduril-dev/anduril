# Overview

Anduril can be extended by implementing new components. A component is executable code that encapsulates some functionality used in workflows, such as preprocessing data in a certain format or a certain statistical algorithm. A component is packaged so that it can be reused between projects, and distributed to other users if needed.

Ready-made components provide a large amount of functionality, so you usually don't need to implement your own components when you start using Anduril. In particular, components that invoke [external scripts](../../components/external/) allow embedding custom scripts in your workflows without encapsulating them as components. However, if you use certain code in several projects, you obtain the following benefits from implementing it as a component:

- A component is implemented once and used several times, which reduces work and increases quality
- Components are found on the Anduril search path so it is easy to import code into new workflows
- Components have a standard and documented interface, which allows sharing them between users
- Components can be systematically tested
- Components can specify their external dependencies, which can be automatically installed
- Related components can be packaged in bundles that can be distributed online

# Example scenario

In the following sections, we illustrate extending Anduril by creating a custom bundle that contains one atomic component (implemented using R) and one composite component (Scala function). The finished bundle is available as a [ZIP archive](demo.zip).
