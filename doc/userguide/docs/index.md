# Introduction

[Anduril](http://anduril.org) is a workflow platform for analyzing large data sets. It provides ready-made analysis tools for molecular biology, and is extensible to other applications. Anduril provides a powerful workflow environment for analysts, and automated facilities for generating visual reports for end users of analytics.

Workflows are constructed using [Scala 2.11](http://www.scala-lang.org/) and executed in parallel using a workflow engine optimized for iterative development. External libraries (e.g., R, Python) and command-line tools can be integrated into workflows. Anduril can be flexibly deployed locally or in a Linux cluster.

## Table of Contents

- Installation & Usage
    - [Installation](install/install/)
    - [Command-line usage](install/usage/)
    - [Cluster & Docker deployment](install/cluster/)
    - [Example Pipelines](examples/qc/)
- Workflows
    - [Getting started: Hello world](workflows/hello-world/)
    - [Linear workflow](workflows/linear/)
    - [Branching and merging](workflows/branch-merge/)
    - [Array ports](workflows/array/)
    - [Iteration](workflows/iteration/)
    - [Dynamic workflows](workflows/dynamic/)
    - [Organizing workflows](workflows/organizing/)
    - [Annotations](workflows/annotations/)
    - [Log levels](workflows/log-levels/)
    - [Reference: Component naming](workflows/component-naming/)
- Components
    - [Basic components](components/basic/)
    - [External scripts](components/external/)
    - [CSV/TSV processing](components/csv/)
    - [Visualization](components/visualization/)
- Extending (for developers)
     - [Overview](extend/overview/)
     - [Component bundle](extend/bundle/)
     - [Component types](extend/component-types/)
     - [Component interface](extend/interface/)
     - [Implementing: Atomic](extend/atomic/)
     - [Implementing: Composite](extend/composite/)
     - [Testing](extend/testing/)
