def PreprocessGeneCSV(in: CSV, excludeBadQuality: Boolean, includeGenes: String): (CSV,CSV) = {
    val hasFilter = (includeGenes != ".*") || excludeBadQuality;
    var filteredCSV: CSV = in
    if (hasFilter) {
        val regexp: String =
            "Gene=%s".format(includeGenes) +
            (if (excludeBadQuality) ",QualityOK=1" else "")
        val filtered = CSVFilter(in, regexp = regexp)
        filteredCSV = filtered.out
    }

    val statisticsScript = INPUT(path = "statistics.r")
    val statistics = REvaluate(statisticsScript, in)

    (filteredCSV, statistics.table)
}
