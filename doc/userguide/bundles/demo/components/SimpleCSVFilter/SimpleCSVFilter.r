library(componentSkeleton)

execute <- function(cf) {

    input <- CSV.read(get.input(cf, 'in'))
    include <- get.parameter(cf, 'include', 'boolean')

    if (input.defined(cf, 'columnNames')) {
        column.names <- CSV.read(get.input(cf, 'columnNames'))[,1]
        if (include) {
            output <- input[, column.names, drop = FALSE]
        } else {
            output <- input[, !(colnames(input) %in% column.names), drop = FALSE]
        }
    } else {
        output <- input
    }

    CSV.write(get.output(cf, 'out'), output)
    return(0)
}

main(execute)
