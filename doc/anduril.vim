" Vim syntax file
" Language:     AndurilScript
" Maintainer:   Kristian Ovaska <kristian.ovaska@helsinki.fi>
" Last Change:  2013-03-01

" Installation:
" 1. Copy to ~/.vim/anduril.vim
" 2. Put the following into your .vimrc:
"    au! Syntax anduril source ~/.vim/anduril.vim 
"    au BufRead,BufNewFile *.and set filetype=anduril

if exists("b:current_syntax")
  finish
endif

syn keyword conditionalKW case else if optional switch
syn keyword statementKW include return
syn keyword repeatKW for
syn keyword otherKW delegate force function
syn keyword typeKW boolean float int string record 
syn keyword booleanKW false true
syn keyword constantKW null

hi link conditionalKW Conditional
hi link statementKW Statement
hi link repeatKW Repeat
hi link otherKW Keyword
hi link typeKW Type
hi link booleanKW Boolean
hi link constantKW Constant

syn match annotation "@\(bind\|doc\|enabled\|execute\|filename\|host\|in\|keep\|name\|out\|par\|priority\|require\)\>"
hi link annotation Special

syn match stdFunction "\<std.\(concat\|echo\|enumerate\|exists\|fail\|fRead\|iterArray\|itercsv\|iterdir\|length\|lookup\|makeArray\|metadata\|mod\|nRows\|pow\|quote\|range\|recordToString\|registerJava\|split\|strReplace\|substring\)\>"
hi link stdFunction Special

syn match envvar "$\w\+"
hi link envvar Special

syn match escape "\\\(n\|r\|t\|\"\|\\\|$\)" contained
syn region string1 start='"' end='"' end='$' contains=escape
syn region string2 start="'" end="'" end="$"
syn region string3 start='"""' end='"""' contains=escape
syn region string4 start="'''" end="'''"

hi link escape SpecialCharacter
hi link string1 String
hi link string2 String
hi link string3 String
hi link string4 String

" Number patterns from simula.vim (Haakon Riiser)
syn match number1 "-\=\<\d\+\>"
syn match number2 "-\=\<\d\+\(\.\d\+\)\=\(&&\=[+-]\=\d\+\)\=\>"
hi link number1 Number
hi link number2 Number

syn match error1 "[#%;?^`~\\]"
syn match error2 "[+-/*&|]="
syn match error3 "<<\|>>\|[*][*]\|::\|[.][.]\|\[\["
syn match error4 "=[><]"
syn match error5 ",\s*,"
hi link error1 Error
hi link error2 Error
hi link error3 Error
hi link error4 Error
hi link error5 Error

syn region comment1 start="/\*" end="\*/"
syn match comment2 "//.*"
syn region comment3 start="/\*\*" end="\*/"
" Start-of-file hashbang
syn match comment4 "\%^#!.*"
hi link comment1 Comment
hi link comment2 Comment
hi link comment3 SpecialComment
hi link comment4 SpecialComment

syn match funcCall "\w\+\ze("
hi link funcCall Function

syn sync minlines=80

let b:current_syntax = "anduril"
