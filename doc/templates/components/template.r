library(componentSkeleton)

execute <- function(cf) {
	# Read inputs/output ports and parameters
	csv.input <- CSV.read( get.input( cf, 'in' ) )

	boolean.param <- get.parameter( cf, 'boolean_parameter', type='boolean' )
	string.param <- get.parameter( cf, 'string_parameter' )

	# Read xlsx files from a BinaryFolder
	folder <- get.input( cf, 'in' )
	filenames <- list.files( folder, pattern = "*.xlsx", full.names = TRUE )
	# Do something (summary()) on each file in the folder
	list.of.data.frames <- lapply( filenames, summary)
	names( list.of.data.frames ) <- filenames

	# Read array of csv from input port in
	in.array <- Array.read( cf, 'in' )
    list.of.data.frames <- list()
    for( i in 1:Array.size( in.array ) ) {
        key <- Array.getKey( in.array, i )
        filename <- Array.getFile( in.array, key )
   		list.of.data.frames[[ key ]] <- CSV.read( filename )
    }

	# Check for file existance
	if( input.defined( cf, 'optional_input' ) ) {
    	optional.input.data.frame <- CSV.read( get.input( cf, 'optional_input' ) )

	# Printing log/error messages
	write.log(cf, 'This message will appear in the log file')
	write.error(cf, 'This message will appear in the log file as an error')
	return(1) # After an error message the component should exit with non-zero 

	# TODO: write to temporary directory

	# Write array of csv files to output port named 'out'
	result[['file1']] <- data.frame()
	result[['file2']] <- data.frame()

	# Create directory for output out if we want to store files (from array) or plots in it.
	out.dir <- get.output(cf, 'out')
	if (!file.exists(out.dir)) {
		dir.create(out.dir, recursive=TRUE)
	}

	array.out.object <- Array.new()
	for(i in 1:length( result )) {
		key = names( result )[i]
		filename = paste(key, ".csv", sep="")
		CSV.write(paste(out.dir, "/", filename, sep=""), result[[i]] )
		array.out.object <- Array.add(array.out.object, key, filename)
	}
	Array.write(cf, array.out.object, 'out')

	# Store plots
	myName <- get.metadata(cf, 'instanceName')
	base.name <- paste( myName, 'plot', sep="-")
    filename  <- file.path( out.dir, base.name )
    png.open( paste0(filename, '.png') )
    plot( csv.input )
    png.close()

	# Always end the component with return(0), any other number will be an error
    return(0)
}

main(execute)
