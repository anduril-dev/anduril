% Template for using Matlab in Anduril

try % Everything inside "try" to catch any errors
    cf=readcommandfile(commandfile);  % this is needed to get the input and outputs
    % cf = Anduril command file struct

    % Read outputs,inputs and parameters
    outdir=getoutput(cf,'dir');
    mkdir(outdir);
    % input and output ports are named 'file' in the component.xml
    outfile=getoutput(cf,'file');
    inputfile=getinput(cf,'file');
    
    parameter=getparameter(cf,'param1','string');  % note, parameter can be string, float or boolean. you can also make the function 'guess'

    if strcmp(parameter,'fail') %i.e. param1="fail"
        writeerror(cf,'You have commanded the component to throw error');
        exit
    end
    % write to log
    writelog(cf,['reading ' inputfile ])
    % read CSV file
    csvdata=readcsvcell(inputfile);
    % create a struct from two columns, named col1 and col2
    data.col1=getcellcol(csvdata,'col1','float');  % Force both columns numeric (string,boolean,guess exist)
    data.col2=getcellcol(csvdata,'col2','float');
    
    % example calculation
    covariance=cov(data.col1,data.col2);
    
    % write result as CSV:
    %  create the header for the file
    dataout.columnheads={'Column 1','Column 2'};
    %  convert the numeric matrix in to cell of strings
    dataout.data=num2csvcell(covariance);

    writefilecsv(outfile,dataout);    

catch me  % error handling
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;  % the script must exit matlab at the end!
