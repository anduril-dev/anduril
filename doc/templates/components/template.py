#!/usr/bin/python
from anduril.args import * #import all the Anduril component related stuff to the global namespace


######################################################################
######  Inputs/outputs and parameters of different type ######
######################################################################

if verbose: #verbose is a bool type parameter.
    print "Hello!"

inFile=open(inputPort,'rU') #open non-array input port named inputPort
outFile=open(outputPort,'w') #open non-array output port named outputPort

for line in inFile:
    outFile.write(line)

for i in range(paramInt): #paramInt is a integer type parameter
    outFile.write(paramString) #paramString is a string type parameter

outFile.close()



##########################################################
###### Printing to stdout/stderr/log/error messages ######
##########################################################

import sys
print "Hello stdout!"
sys.stderr.write("Hello stderr!")
write_log("Hello log!")
write_error("Hello error messages!") #will cause the exection to fail



#####################################
###### CSV reading and writing ######
#####################################

import csv 

# read CSV
reader = csv.DictReader(open(inputCSV,'rb'),delimiter='\t')
for row in reader:
    print(row)
    
#write CSV
reader = csv.DictReader(open(inputCSV,'rb'),delimiter='\t')
writer = csv.DictWriter(file(outputCSV,'wb'),
                        header,
                        delimiter='\t',
                        quotechar='"',
                        quoting=csv.QUOTE_MINIMAL)

for row in reader:
    writer.writerow(row)



############################################
###### Writing to temporary directory ######
############################################

import os
tempfile=open(os.path.join(tempdir,"tmpfile.txt"),'w')
tempfile.write("Temporary data.")
tempfile.close()



###################################
###### Array reading/writing ######
###################################

#this loop will create a (deep) copy of inputArrayPort to outputArrayPort
for arrayKey in inputArrayPort: # array type input ports are automatically read in as ordered dicts
    inputFilename=inputArrayPort[arrayKey] #values of the dict are the file names
    
    #output arrays are automatically read as AndurilOutputArray objects
    outputFilename=outputArrayPort[arrayKey] #generate output filename based on array key
    
    import shutil 
    shutil.copyfile(inputFilename,outputFilename)
    
