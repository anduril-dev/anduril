
# set -x  # Used for debugging, prints each line before executing it
# set -e  # Script exits at first error, otherwise the next line is 
          # executed even after a command fails 

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/lang/bash/functions.sh"

# use these functions to read command file
infile=$( getinput datain ) 
outfile=$( getoutput dataout )
parameter=$( getparameter param1 )
# or use the exporter function
export_command
echo param1 $parameter_param1
echo datain $input_datain


GREP=$( which grep )
if ( $GREP --version | grep -i gnu )
    then echo "GNU Grep found."
else
    msg="GNU Grep not found. ( $GREP )"
    writeerror "$msg"
    exit 1
fi

# Check for file existance
if [ ! -f "$infile" ]
then echo "If infile is an optional input, check for it's existence"
fi

if [ "$parameter" = "true" ]
then echo "boolean parameters are read as strings!"
fi

# errors may be handled with the || operator
$GREP "$parameter" "$infile" > $outfile || writelog "Error in grepping." 

# Typical case: if output file doesnt exist, exit with error
# NOTE: Anduril fails the component automatically, if an output port
# is missing!
if [ -f $outfile ]
then writelog "Successful run."
else
     writeerror "Output missing!"
     exit 1
fi

# You can use a special temporary directory:
# Temporary folder is not automatically deleted if component exits with error.
tempdir=$( gettempdir )
echo "Hello" > "$tempdir"/tempfile

# Reading arrays

# Writing arrays
