#!/bin/bash
# Template for --exec-mode prefix  --prefix prefix_template
# This file is actually three templates, it does not work as such.

if [ -z "$1" ]
then echo No args, no action.
     exit 1
fi
# The following environment variables are set if @cpu, @memory, @host annotations are used:
echo CPUS $ANDURIL_PREFIX_CPU MEM $ANDURIL_PREFIX_MEMORY HOSTS $ANDURIL_PREFIX_HOST
# By default, the variables are empty.

###################################
## 1. The minimum implementation ##
###################################
# prefix script collects the arguments and runs them.
# In the case of R launcher, the stdin is used for
# passing the script:
"$@"  < /dev/stdin
# or shorter way of doing the same:
# cat - | "$@"
#    And thats it!

######################################
### 2. Passing commands to a script ##
###################################### 
CPUS=1
if [ ! -z "$ANDURIL_PREFIX_CPU" ]
then CPUS=$ANDURIL_PREFIX_CPU
fi
srun -c $CPUS "$@" < /dev/stdin
# The 'srun' process must exit ONLY when the
# processes it starts exit. No backgrounding here, 
# since when this prefix script exists, Anduril
# will go on with the next component.

##########################################################
### 3. Saving commands in a script file, running it     ##
###    in the background, and waiting for exit          ##
##########################################################

# Save temporary scripts in user home folder 
JOBROOT="$HOME/.scripts"
mkdir -p "$JOBROOT"
# Create unique name for the script file
JOBNAME=job_$( date +"%y%m%d_%H%M%S" )_$( echo $@ | md5sum | cut -f1 -d" " )
JOBPATH="$JOBROOT/$JOBNAME"
mkdir -p "$JOBPATH"
JOBFILE="$JOBPATH/job.sh"
STREAMFILE="$JOBPATH/stdin"
# The DONEFILE is used to indicate the process is finished
DONEFILE="$JOBPATH/done"

# Create the script to run
echo '#!/bin/sh' > "$JOBFILE"
# re-quote arguments, just for the sake of.. 
for (( i=1; i<=$#; i++ ))
do echo -n "\"${!i}\" "  >> "$JOBFILE"
done
# Catch the input stream (for R launcher)
cat - >> "$STREAMFILE"
echo -n ' < "'$STREAMFILE'"' >> "$JOBFILE"
# Create the DONEFILE if the process fails
echo -n ' || touch "'$DONEFILE'"' >> "$JOBFILE"
echo -e "\n" >> "$JOBFILE"
# you have to create the file anyway:
echo 'touch "'$DONEFILE'"' >> "$JOBFILE"

# and execute the job ( sends to background )
qsub "$JOBFILE"

# wait for the DONEFILE to appear
while [ ! -e "$DONEFILE" ]
do sleep 5
done
##  now the script can finish

exit 0

