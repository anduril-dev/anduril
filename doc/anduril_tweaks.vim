" Adds syntax highlighting for languages other than AndurilScript
" when they are embedded in AndurilScript strings
"
" Author: Lauri Lyly
" Version: 0.1
"
" For the real author, see the function TextEnableCodeSnip below
"
" Installation:
" 1. Put the following into your .vimrc:
"    source /opt/anduril/doc/anduril_tweaks.vim
"
" You should likely change the path to match your ANDURIL_HOME

" This function is what is really enabling this functionality
" Author: Ivan Tishchenko
" URL: http://vim.wikia.com/wiki/Different_syntax_highlighting_within_regions_of_a_file
" Enables different syntax highlight between certain tags
function! TextEnableCodeSnip(filetype,start,end,textSnipHl) abort
  let ft=toupper(a:filetype)
  let group='textGroup'.ft
  if exists('b:current_syntax')
    let s:current_syntax=b:current_syntax
    " Remove current syntax definition, as some syntax files (e.g. cpp.vim)
    " do nothing if b:current_syntax is defined.
    unlet b:current_syntax
  endif
  execute 'syntax include @'.group.' syntax/'.a:filetype.'.vim'
  try
    execute 'syntax include @'.group.' after/syntax/'.a:filetype.'.vim'
  catch
  endtry
  if exists('s:current_syntax')
    let b:current_syntax=s:current_syntax
  else
    unlet b:current_syntax
  endif
  execute 'syntax region textSnip'.ft.'
  \ matchgroup='.a:textSnipHl.'
  \ start="'.a:start.'" end="'.a:end.'"
  \ contains=@'.group
endfunction

" Syntax will look horrible for BASH files without this.
" see :h ft-sh-syntax
let g:is_bash=1

" Color anduril embedded python in python syntax... only for variables ending with _py
autocmd BufRead *.and :call TextEnableCodeSnip('python', ".*_py.*=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('python', '.*_py.*=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

" Color anduril embedded python in python syntax... only for variables ending with _py
autocmd BufRead *.and :call TextEnableCodeSnip('python', ".*_py.*StringInput(content=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('python', '.*_py.*StringInput(content=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

" Color anduril embedded matlab in matlab syntax... only for variables ending with _mat
autocmd BufRead *.and :call TextEnableCodeSnip('matlab', ".*_mat.*StringInput(content=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('matlab', '.*_mat.*StringInput(content=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

" Color anduril embedded bash in bash syntax... only for variables ending with _sh
autocmd BufRead *.and :call TextEnableCodeSnip('sh', ".*_sh.*StringInput(content=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('sh', '.*_sh.*StringInput(content=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

" Color anduril embedded bash in bash syntax... only for variables ending with _sh
autocmd BufRead *.and :call TextEnableCodeSnip('sh', ".*_sh.*BashEvaluate(.*script=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('sh', '.*_sh.*BashEvaluate(.*script=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

" Color anduril embedded bah in bash syntax... only for variables ending with _sh
autocmd BufRead *.and :call TextEnableCodeSnip('sh', ".*_sh.*=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('sh', '.*_sh.*=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

" Color anduril embedded R in R  syntax... only for variables ending with _sh
autocmd BufRead *.and :call TextEnableCodeSnip('r', ".*_r.*StringInput(content=[^\']*'''", "'''", 'SpecialComment')
autocmd BufRead *.and :call TextEnableCodeSnip('r', '.*_r.*StringInput(content=[^\"]*\"\"\"', '\"\"\"', 'SpecialComment')

