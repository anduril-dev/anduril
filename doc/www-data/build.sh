#!/bin/bash

echo "***** Updating home page images..."
PDFS="ConfigurationReport GOClustering GraphVisualizer SQL2Latex VennDiagram"
for pdf in $PDFS; do
    echo "Converting $pdf to a PNG format..."
    inkscape -z $pdf.pdf --export-png=$pdf.png --export-area-drawing \
             --export-background=ffffff --export-width=1000
done
montage \
  ConfigurationReport.png GOClustering.png    VennDiagram.png \
  Logo.png                GraphVisualizer.png SQL2Latex.png   \
  -tile 3x2 -geometry 400x400+0+0 \
  ../www/demoMontage.png
for pdf in $PDFS; do
    rm $pdf.png
done
