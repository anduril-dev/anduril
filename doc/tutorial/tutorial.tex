\documentclass{article}

\usepackage{graphicx}
\graphicspath{{diagrams/}}
\usepackage{fancyhdr}
\usepackage{fancyvrb}
\usepackage{listings}
\usepackage{mathptmx} % Change font to Times
\usepackage{url}
\usepackage[pdfpagemode=None,colorlinks=true,urlcolor=red,linkcolor=black,citecolor=black,pdfstartview=FitH]{hyperref}
\usepackage{fullpage}


%\hyphenation{
%}

\begin{document}
\pagenumbering{arabic}
%\vspace*{5cm}
\begin{center}
\begin{LARGE}
Anduril Tutorial \\
\end{LARGE}
\end{center}

%\vspace*{0.5cm}
\tableofcontents{}
%\pagebreak{}

%\setlength{\baselineskip}{1.2\baselineskip}
\setlength{\parindent}{0pt}
%\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

\section{Anduril script basics}
\subsection{Resources for examples}
First, you will have to find the home folder of Anduril.
The home folder should be assigned to environment variable \$ANDURIL\_HOME. If not, depending on the installation
it may be for example \textbf{/usr/share/anduril}. The home folder will contain the file ``anduril.jar''.
All of the files used in this tutorial can be found in \textbf{doc/tutorial/examples/} folder,
under the Anduril home folder.

\subsection{Normalizing data, a simple example}
The files for this example are in the \textbf{doc/tutorial/examples/01-Normalize2Excel/} folder.

\lstset{frame=single,language=,caption=matrix.csv}
\lstinputlisting{examples/01-Normalize2Excel/matrix.csv}
Consider you need to normalize two sets of data. The sets are placed in a table as seen above. 
To normalize the two columns separately 
to zero-mean and unit variance, and provide the results in an Excel friendly format, the following simple four step
Anduril script can be used. The steps are:
\begin{enumerate}
  \item Assign data
  \item Normalize data
  \item Convert data to Excel
  \item Write the result
\end{enumerate}
\pagebreak

\lstset{frame=single,language=Java,caption=normalizer.scala}
\lstinputlisting{examples/01-Normalize2Excel/normalizer.scala}
Explanation of the script: The data is stored in the CSV format in file ``matrix.csv''. The file location
is saved in the instance ``values''. Next, the file location is used as an
input for a component called LinearNormalizer\footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/LinearNormalizer}}.
Note the parameter ``method'' given to the component. This controls the method of normalization.
See the microarray documentation for possible values.

The normalized values are assigned to the instance ``valuesNorm'', which is again an
input to the next component: CSV2Excel\footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/CSV2Excel}}.
This time, two inputs are used. The inputs serve as separate sheets in the produced Excel file. Finally the Excel file is copied to a special
folder reserved for final results with the component OUTPUT. Note that you should consult to the component manuals to find out which input
and output ports each component provides. 

To run the script go to the \textbf{examples/01-Normalize2Excel/} folder and type:
\begin{lstlisting}[frame=single,caption=,language=]
anduril run normalizer.scala
\end{lstlisting}
If you get errors, your Anduril installation is probably not complete. In that case, refer to the User Guide for installation procedures.

In the running folder there are now several new subfolders and a new file: 
\begin{itemize}
 \item \textbf{log}: folder for log files
 \item \textbf{excelOut, valuesNorm}: intermediate result folders for the instances
 \item \textbf{output}: final results directed with the OUTPUT component.
 \item \textbf{\_state}: file used internally by Anduril to check the status of previously run instances.
\end{itemize}

Running more complex scripts will populate the current folder with many more subfolders, and therefore you may want to
change the execution folder of the script. Try running:
\begin{lstlisting}[frame=single,caption=,language=]
anduril run normalizer.scala -d results
\end{lstlisting}
You should see that the log folder is still created in the current folder, but the script results, both intermediate
and the final, are now under subfolder \textbf{results}.

Look at the intermediate result of the normalized values by typing:
\begin{lstlisting}[frame=single,caption=,language=]
cat results/valuesNorm/matrix.csv
\end{lstlisting}
The excel file is located both in the \textbf{results/excelOut} and the \textbf{results/output} folders. Open the Excel file
and see how the data is placed in two different sheets.


\subsection{Plotting, a simple example}

The files for this example are in the \textbf{doc/tutorial/examples/01-plotting/} folder.

\lstset{frame=single,language=,caption=matrix.csv}
\lstinputlisting{examples/01-plotting/matrix.csv}
Consider you need to make a plot of the above data. To make a scatter plot of the data, the following 
Anduril script can be used. The steps are:
\begin{enumerate}
  \item Assign data
  \item Plot data
  \item Write the result
\end{enumerate}
%\pagebreak

\lstset{frame=single,language=Java,caption=plot.scala}
\lstinputlisting{examples/01-plotting/plot.scala}
Explanation of the script: The data is stored in the CSV format in file ``matrix.csv''. The file location
is saved in the instance ``values''. Next, the file location is used as an
input for a component called Plot2D\footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/Plot2D}}.
Note the parameters ``title'' and ``caption'' given to the component. They contain ``\%s'' placeholders, which are replaced with the column names in input files. See the microarray documentation for other parameters.

Outputs of the Plot2D-component are assigned to the instance ``plot'', which contains a Latex document file and an image file. Finally the plot folder is copied to a special folder reserved for final results with the component OUTPUT. Note that you should consult to the component manuals to find out which input
and output ports each component provides. 

To run the script go to the \textbf{examples/01-plotting/} folder and type:
\begin{lstlisting}[frame=single,caption=,language=]
anduril run plotting.and
\end{lstlisting}

In the running folder there are now several new subfolders and a new file: 
\begin{itemize}
 \item \textbf{log}: folder for log files
 \item \textbf{plot}: intermediate result folder for the instance
 \item \textbf{output}: final results directed with the OUTPUT component.
 \item \textbf{\_state}: file used internally by Anduril to check the status of previously run instances.
\end{itemize}

\subsection{Statistical tests, t-test as an example}

The files for this example are in the \textbf{doc/tutorial/examples/01-StatisticalTest/} folder.

\lstset{frame=single,language=,caption=matrix.csv}
\lstinputlisting{examples/01-StatisticalTest/matrix.csv}
Consider you need to make a statistical test for the above data. To compute, for example, a t-test for the data, the following 
Anduril script can be used. The steps are:
\begin{enumerate}
  \item Assign data
  \item Compute the test
  \item Write the result
\end{enumerate}

\lstset{frame=single,language=Java,caption=tTest.scala}
\lstinputlisting{examples/01-StatisticalTest/tTest.scala}
Explanation of the script: The data is stored in the CSV format in file ``matrix.csv''. The file location
is saved in the instance ``values''. Next, the file location is used as an
input for a component called StatisticalTest\footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/StatisticalTest}}.
Note the parameter ``test'' is used to control the type of the statistical test and t-test is set as a default value. See the microarray documentation for other parameters.

Outputs of the StatisticalTest-component are assigned to the instance ``tTest'', which contains two output files: ``pvalues'' and ``idlist''. ``pvalues'' contains all computed p-values in CSV format. ``idlist'' contains the IDs of those tests whose p-value is below the threshold. In this example default value 0.05 for ``threshold'' parameter is used. Finally the pvalues file is copied to a special folder reserved for final results with the component OUTPUT. Note that you should consult to the component manuals to find out which input
and output ports each component provides. 

To run the script go to the \textbf{examples/01-StatisticalTest/} folder and type:
\begin{lstlisting}[frame=single,caption=,language=]
anduril run tTest.scala
\end{lstlisting}

In the running folder there are now new subfolders and a new file: 
\begin{itemize}
 \item \textbf{log}: folder for log files
 \item \textbf{tTest}: intermediate result folder for the instance
 \item \textbf{output}: final results directed with the OUTPUT component.
 \item \textbf{\_state}: file used internally by Anduril to check the status of previously run instances.
\end{itemize}


\subsection{Creating and applying your own function}
The files for this example are in the \textbf{doc/tutorial/examples/02-SimpleFunction/} folder.

Often you would like to perform the same task involving several components to more than one set of data. In such a case it is useful to create a function that can be called with different parameters. In this example we have a matrix where we want to plot two subsets of the original data. This also shows the use of the REvaluate component. Inside the function:

\begin{enumerate}
  \item Filter data with CSVFilter
  \item Create custom heatmap plot with REvaluate
\end{enumerate}

This function is then run with two parameter sets.

\lstset{frame=single,language=Java,caption=useSimpleFunction.scala}
\lstinputlisting{examples/02-SimpleFunction/useSimpleFunction.scala}

\textbf{Add more information here}

\lstset{frame=single,language=Java,caption=customPlot.r}
\lstinputlisting{examples/02-SimpleFunction/customPlot.r}


To run the script go to the \textbf{examples/02-SimpleFunction/} folder and type:

\begin{lstlisting}[frame=single,caption=,language=]
anduril run useSimpleFunction.scala -d results
\end{lstlisting}

In the results folder there are now new subfolders and a new file: 
\begin{itemize}
 \item \textbf{log}: folder for log files
 \item \textbf{diff1-fMatrix}: intermediate result folder for the first CSVFilter component instance
 \item \textbf{diff1-matrixOut}: intermediate result folder for the first REvaluate component instance
 \item \textbf{diff2-fMatrix}: intermediate result folder for the second CSVFilter component instance
 \item \textbf{diff2-matrixOut}: intermediate result folder for the second REvaluate component instance
 \item \textbf{output}: final results directed with the OUTPUT component.
 \item \textbf{\_state}: file used internally by Anduril to check the status of previously run instances.
\end{itemize}

The plots can be found in the output-folder, with a document.tex file and the .png-file that was created.

\subsection{difficult example}
for loop ? 

std.* family of commands?
\subsection{Running Anduril scripts}
command line related things.
\begin{itemize}
\item \$ANDURIL\_HOME
\item anduril clean
\item anduril build-doc
\item switches -b, --force, --force-all, --min-space
\end{itemize}

\section{Applications}
\subsection{Agilent chip analysis}
we need concrete applications. (with relatively small datasets, and so that the running can happen outside Univ. network)


\subsection{Common CSV processing examples}
The files for this tutorial are in the \textbf{doc/tutorial/examples/01-CSVProcessing/} folder.

\lstset{frame=single,language=,caption=csv1.csv}
\lstinputlisting{examples/01-CSVProcessing/csv1.csv}

\lstset{frame=single,language=,caption=csv2.csv}
\lstinputlisting{examples/01-CSVProcessing/csv2.csv}

Often analysis starts with combining clinical data from different sources; quite possibly the data differ in their format, just like csv1 and csv2 do, but we want to use them both. Most Anduril components only understand certain data formats, so cleaning up data is essential. Also, joining data can be achieved several ways in Anduril. For instance, simple join operations have specific components, while more complex operations can be carried out by using SQL. Thus, we have four goals which we want achieve with our Anduril script.

\begin{enumerate}
  \item Clean up data
  \item Filter data
  \item Join data using CSVJoin
  \item Join data using TableQuery
\end{enumerate}

%\pagebreak
\lstset{frame=single,language=Java,caption=csvProcessor.scala}
\lstinputlisting{examples/01-CSVProcessing/csvProcessor.scala}


The script has two input files, csv1 and csv2, of which csv1 is comma separated. This format is not readily understood by most Anduril components, so we need to first change the commas into tabs. Some components are also unable to handle quotation marks, which csv2 contains, so we also clean those away. More often, though, stripping quotation marks comes handy when formatting output files. These steps can be done with the CSVCleaner component \footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/CSVCleaner}}. The double backslashes for the delimSymbol parameter in the csv1clean instance are needed because, without them, the Anduril execution environment assumes that the first backslash is an escape character \footnote{If you're unfamiliar with this concept, see \url{http://en.wikipedia.org/wiki/Escape_character}}, and ignores it.

Both inputs contain distinct columns that we want to get rid of before joining the two files. CSVFilter \footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/CSVFilter}} is our component for this as the name suggests. The component has a parameter \emph{negate} to reverse the include options, which comes handy for instance here, when we only want to filter out one column from a (possibly) big input file.

Now we have two csv files which have the same columns, and we can join them (concatenate, set join) with the CSVJoin component. Setting \emph{useKeys} false makes the component simply concatenate the files.

The two left over columns of csv1 and csv2 (colX, colY), that we filtered out in step 2, might contain interesting data. Therefore, joining the rest of the clinical data for the samples might be handy. This is needed quite often, too: different parts of an analysis need different parts of the input data i.e., columns. Therefore, an SQL left join using the TableQuery \footnote{See full manual page: \url{http://csbi.ltdk.helsinki.fi/anduril/microarray/doc/components/TableQuery}} component does the trick. Just like in the CSVCleaner example, the backslashes are needed to "escape" the quotation marks within the string. Also notice the plus-signs in the query string, which indicate string concatenation in Anduril.

\end{document}
