% Compile:
% pdflatex CSVsInAnduril.tex && pdflatex CSVsInAnduril.tex

\documentclass[]{beamer}
\mode<presentation>
\usepackage[english]{babel}
\usepackage{listings}
\usepackage{hyperref}
\hypersetup{
 pdfauthor    ={Marko.Laakso@Helsinki.FI},
 pdftitle     ={CSV files in Anduril},
 pdfnewwindow =true,
 pdfview      =FitB,
 pdftoolbar   =true,
 pdfmenubar   =true,
 pdfwindowui  =false,
 pagecolor    =white,
 pdfhighlight =/N,
 pdffitwindow =true,pagebordercolor={1 1 1},
 pdfpagelayout={SinglePage}
}
\usepackage{geometry}

\title{CSV files in Anduril}
\author{Marko Laakso}
\institute{\href{http://www.ltdk.helsinki.fi/sysbio/csb/default.htm}{CSBL}}
\date{}

\usetheme{Warsaw}
\usecolortheme{crane}

\begin{document}

\selectlanguage{english}

\begin{frame}
\begin{center}
Anduril tutorial series\\
\begin{huge}
CSV files in Anduril\\
\end{huge}
\vspace{5mm}
Marko Laakso
\vspace{5mm}
\\\textit{
 Computational systems biology laboratory,
 Institute of biomedicine, University of Helsinki,
 Research program in genome-scale biology,
 Biomedicum\/
}
\end{center}
\end{frame}

\section{Introduction to the data type}
\subsection{Anduril expectations}

\begin{frame}[t]
\frametitle{What are CSV files}
\begin{itemize}
  \item CSV stands for Comma Separated Values but the values are \textcolor{red}{separated with tabs} in Anduril
  \item CSV file represents a data matrix with rows and columns
  \item The first row of each file consists of the column names
  \item Number of columns is equal for each row
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{Test case support}
\begin{itemize}
  \item CSV files are validated by\\
        {\footnotesize\texttt{fi.helsinki.ltdk.csbl.anduril.component.CSVTypeFunctionality}}
  \item Syntax check for the column structure
  \item Expected outputs accept regular expressions withing \{\{ \}\}
  \item Floating points tolerate minor variation in roundings ($10^{-5}$)
\end{itemize}
\end{frame}

\section{Language supports}
\subsection{R}

\begin{frame}[t]
\frametitle{R componentSkeleton}
\begin{itemize}
  \item Install \texttt{componentSkeleton} package\\
        {\tiny\texttt{(ant r; sudo R CMD INSTALL r/componentSkeleton\_1.0.tar.gz)}}
  \item Specific functions to access CSV subtypes (read/edit/write):
  \begin{itemize}
    \item AnnotationTable
    \item CSV
    \item SampleGroupTable
    \item SetList
  \end{itemize}
\end{itemize}
\end{frame}

\subsection{Java}

\frame[containsverbatim]{\frametitle{Reading in Java}
\begin{itemize}
  \item Asser.jar provides utilities for CSV files
  \item \texttt{fi.helsinki.ltdk.csbl.asser.io.CSVParser} can be used to read files
\end{itemize}
\lstset{language=Java,numbers=left,numberstyle=\footnotesize,frame=shadowbox}
\footnotesize\begin{lstlisting}
CSVParser in       = new CSVParser(inputfile);
String[]  colnames = in.getColumnNames();
for (String[] row : in) {
    System.out.println(in.getLineNumber()+": "+
                       in.Arrays.toString(row));
}
in.close();
\end{lstlisting}}

\frame[containsverbatim]{\frametitle{Writing in Java}
\begin{itemize}
  \item \texttt{fi.helsinki.ltdk.csbl.asser.io.CSVWriter} can be used to write files
  \item Quotations, column and line separators, and missing values are handled automatically
\end{itemize}
\lstset{language=Java,numbers=left,numberstyle=\footnotesize,frame=shadowbox}
\footnotesize\begin{lstlisting}
CSVWriter out = new CSVWriter(colnames, outputfile);
for (String[] row : in) {
    for (String cell : row) {
        out.write(cell);
    }
}
out.close();
\end{lstlisting}}

\subsection{MATLAB}


\begin{frame}[t,containsverbatim]
\frametitle{Reading in MATLAB}
\lstset{language=Matlab,numbers=none,numberstyle=\footnotesize,frame=shadowbox}
\footnotesize\begin{lstlisting}
TABLE=READFILECSV(FILENAME,JUMP,DELIM,DL)
 FILENAME = file name
 JUMP     = skips no. of rows or to first 
            occurence of string (int or string)
 DELIM    = csv delimiter
 TABLE    = struct, containing fields:
   TABLE.COLUMNHEADS = strings for header row
   TABLE.DATA        = cell matrix of data

EXAMPLE:
 table=readcsvfile('data.csv');
 column=getcellcol(table,'Col 1')
\end{lstlisting}
Note, conversion from strings to numbers is done in getcellcol.
\end{frame}

\begin{frame}[t,containsverbatim]
\frametitle{Writing in MATLAB}
\lstset{language=Matlab,numbers=none,numberstyle=\footnotesize,frame=shadowbox}
\footnotesize\begin{lstlisting}
WRITEFILECSV(FILENAME,TABLE)
 FILENAME = file to write
 TABLE    = a struct, containing fields:
   TABLE.COLUMNHEADS = cell array of strings for 
                       header row {1xM}
   TABLE.DATA        = cell array of strings. 
                       Column number must match! {NxM}

 EXAMPLE
 table.columnheads={'Col1','Col2'};
 table.numdata=num2cell(eye(2));
 table.data=cellfun(@(x) sprintf('\%g',x), ...
     table.numdata,'UniformOutput',false);
 writefilecsv('test.csv',table)
\end{lstlisting}
Note, conversion from numbers to strings must be done prior to writing.
\end{frame}

\section{Microarray components}
\subsection{Transformations}

\begin{frame}[t,containsverbatim]
\frametitle{CSVFilter}
\begin{itemize}
  \item Select subset of rows and columns
  \item Renaming columns
\end{itemize}
\footnotesize\begin{verbatim}
                                  // intersection with the status
fenn = FennPathway(force red    = CSVFilter(degsA.gIDs, status), 
                   force green  = CSVFilter(degsB.gIDs, status),
                   force blue   = CSVFilter(degsC.gIDs, status),
                   force status = status,
                   organism     = Organism_Homo_sapiens,
                   linkTypes    = linkTypes)
\end{verbatim}
\end{frame}

\begin{frame}[t]
\frametitle{CSV2IDList}
\begin{itemize}
  \item Produces a list of unique values
  \item Multiple input files (columns) may be used
  \item AndurilScript values may be exported to the IDList
        using the \textit{constants} parameter
\end{itemize}
\tiny
\begin{tabular}{|lll|}\hline
col1 & \textcolor{red}{col2}  & col4\\\hline
A    & \textcolor{red}{s1,s4} & j1\\
B    & \textcolor{red}{s1}    & j2\\
C    & \textcolor{red}{s2}    & j1\\
D    & \textcolor{red}{s2}    & j2\\
E    & \textcolor{red}{s3}    & j1\\\hline
\end{tabular}
$+$
\begin{tabular}{|lll|}\hline
col1 & col2  & \textcolor{red}{col4}\\\hline
A    & s1,s4 & \textcolor{red}{j1}\\
B    & s1    & \textcolor{red}{j2}\\
C    & s2    & \textcolor{red}{j1}\\
D    & s2    & \textcolor{red}{j2}\\
E    & s3    & \textcolor{red}{j1}\\\hline
\end{tabular}
$\Longrightarrow$
\begin{tabular}{|l|}\hline
col2\\\hline
s1\\
s4\\
s2\\
s3\\
j1\\
j2\\\hline
\end{tabular}
\end{frame}

\begin{frame}[t]
\frametitle{CSVTransformer}
\begin{itemize}
  \item 
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{ExpandCollapse}
\begin{itemize}
  \item Converts between the two possible representations of a relations with multivalued columns
\end{itemize}
\tiny
\begin{tabular}{|llll|}\hline
col1 & col2 & col3  & col4\\\hline
A    & s1   & koivu & j1\\
A    & s1   & koivu & j2\\
A    & s2   & koivu & j1\\
A    & s2   & koivu & j2\\
A    & s3   & koivu & j1\\
A    & s3   & koivu & j2\\
B    & s1   & kuusi & j3\\
C    & s4   & paju  & j2\\
C    & s4   & paju  & j3\\
D    & s2   & tammi & j1\\
D    & s3   & tammi & j1\\
D    & s4   & tammi & j1\\\hline
\end{tabular}
$\Longleftrightarrow$
\begin{tabular}{|llll|}\hline
col1 & col2     & col3  & col4\\\hline
A    & s1,s2,s3 & koivu & j1,j2\\
B    & s1       & kuusi & j3\\
C    & s4       & paju  & j2,j3\\
D    & s2,s3,s4 & tammi & j1\\\hline
\end{tabular}
\end{frame}

\begin{frame}[t]
\frametitle{IDConvert}
\begin{itemize}
  \item 
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{REvaluate}
\begin{itemize}
  \item 
\end{itemize}
\end{frame}

\subsection{Combining files}

\begin{frame}[t]
\frametitle{CSVJoin}
\begin{itemize}
  \item 
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{CSVListJoin}
\begin{itemize}
  \item Concatenate multiple CSV files into a long file
  \item Union of all columns is used in output
  \item An optional column to describe the source file of each row
\end{itemize}
\end{frame}

\begin{frame}[t,containsverbatim]
\frametitle{TableQuery}
\begin{itemize}
  \item Provides a relational database (HyperSQL) for the CSV files
  \item Each input table represents a relation (table1, table2, \ldots)
  \item Results of the given SQL select statement are written to the
        output CSV
\end{itemize}
\footnotesize\begin{verbatim}
common = TableQuery(table1 = degsA,
                    table2 = degsB,
                    query  = "SELECT DISTINCT A.\".GeneId\" "+
                             "FROM   table1 A, table2 B "+
                             "WHERE  (A.\".GeneId\"=B.\".GeneId\") "+
                             "ORDER  BY 1")
\end{verbatim}
\end{frame}

\subsection{Reporting}

\begin{frame}[t]
\frametitle{CSV2Excel}
\begin{itemize}
  \item Converts your CSVs into a Microsoft Office Excel compatible spreadsheet
  \item Each CSV input (csv, csv1, csv2, \ldots) represents an individual sheet
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{CSV2Latex}
\begin{itemize}
  \item Generates a \LaTeX{} table for the selected columns of the input
  \item Table cells may be hyperlinks or anchors
\end{itemize}
\end{frame}

\begin{frame}[t]
\frametitle{HTMLReport}
\begin{itemize}
  \item Generates a semi-static (includes some JavaScript) WWW site that
        represents the data of the input CSVs interconnected by the columns
        with common values
\end{itemize}
\includegraphics[width=0.6\linewidth]{gliomaWeb.png}
\end{frame}

\begin{frame}
\begin{center}
  \begin{huge}Thank you!\end{huge}\\
  Anduril home page is
  \href{http://csbi.ltdk.helsinki.fi/anduril/}{http://csbi.ltdk.helsinki.fi/anduril/}
\end{center}
\end{frame}

\end{document}
