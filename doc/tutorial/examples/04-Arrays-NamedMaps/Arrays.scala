#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._
import org.anduril.runtime.table.Table

object scalaTools {

    /** Input data */
    val csv = INPUT(path="csv.csv")
    // Create an empty Map to store instances of Randomizer
    // The string "random" gives Anduril a hint how to name execution folders
    val random=NamedMap[Randomizer]("random")
    for (row <- iterCSV(csv.out)) {
        // Create a random set of data, by using the CSV file as 
        // parametrization source
        random(row("values"))=Randomizer(columns=1,rows=50,distribution="normal",
                                         mean=row("colA").toDouble)
    }
    // An Anduril Array is a list structure that is created by Components, and written
    // as a file. The file contains key/file pairs.
    // i.e. you can not create an Array in the scala pipeline configuration environment.
    // There is a function makeArray, that calls components, like ArrayConstructor
    // Array should only contain one data type
    val instanceArray=makeArray(random)
    
    // Many components accept arrays to handle multiple lists
    
    // Let's produce a mean of each file as a CSV table
    val process_many_files=PythonEvaluate(inArray=instanceArray,
                                     script="""
tableout.set_fieldnames(['File','Mean'])
outrow={}
for row in input_inArray:
    outrow['File']=input_inArray[row]
    col_data=anduril.table.TableReader(outrow['File']).get_column("Column1")
    outrow['Mean']=sum([float(x) for x in col_data])/float(len(col_data))
    tableout.writerow(outrow)
                                     """)

    // You can access individual Array elements by name
    val large_values=CSVFilter(instanceArray("row1"), lowBound="Column1=7")

    // Arrays can be iterated
    val small_values=NamedMap[CSV]("small_values")
    for ((key,port)<-iterArray(instanceArray)){
        info(key)
        small_values(key)=CSVFilter(instanceArray(key), highBound="Column1=5")
    }
    

}
