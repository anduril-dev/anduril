#!/usr/bin/env anduril

import anduril.builtin._
import org.anduril.runtime._
import org.anduril.runtime.table.Table

object scalaTools {

    /** Input data */
    val csv = INPUT(path="csv.csv")

    // Print a string
    info("Hello World!")

    // Multiple string printing
    val i = 10
    info(i + " bottles" + " on" + " the" + " wall")

    // Converting a CSV to string
    for (row <- Table.reader(csv.out.content)) info(row)
    
    // For loop over delimited string
    val listString = "one,two,three,four,five"
    val mylist = listString.split(",")
    // One way
    mylist.foreach { info }
    // Another way
    for (word <- mylist) {
        // String concatenation, and string length
        val message = List("String", word, "is", word.length, "characters long")
        val messageColon = message.mkString(":")
        info(messageColon)
        // String replace (two ways)
        info(messageColon.replace(':', ' '))
        info(messageColon.map(c => if(c == ':') ' ' else c))
        // Convert string to valid instance name (not handling 1. number!)
        info("Valid variable name: "+messageColon.replaceAll("[^\\w]", "_"))
    }

    // Iterate over CSV
    for (row <- iterCSV(csv.out)) {
        info("Column contents")
        // Access row data
        info(row)
        info(row("colA"))
    }

}
