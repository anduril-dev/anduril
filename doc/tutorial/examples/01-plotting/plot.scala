#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Plotting {

	/** The input file */
	val values = INPUT(path="matrix.csv")

	/** Use the Plot2D component to 
	plot a scatter plot */
	val plot = Plot2D(y=values,
					  x=values,
					  xColumns="S1",
					  yColumns="S2",
					  title="%s vs. %s",
					  caption="Scatter plot (%s vs. %s)")

	/** Direct the plot folder
	to the output folder */
	OUTPUT(plot)
	     
}






