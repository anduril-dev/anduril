library(gplots)
param1<-split.trim(param1, ',')
data <- table1[,param1]

fname    <- paste(myName, "heatmapExample.png", sep="")
plotname <- file.path(document.dir, fname)
document.out <- ''
document.out <- c(document.out, sprintf('\\%s{%s}', "section", "Heatmap"))
pdf(file=plotname, title="heatmap")

heatmap.2(as.matrix(data), main="", xlab="", ylab="", cexRow=1, cexCol=1)
dev.off()

fig.tex <- latex.figure(fname,
                        caption      = "Heatmap",
                        quote.capt   = FALSE,
                        fig.label    = sprintf('fig:%s', myName))

document.out <- c(document.out, fig.tex)
table.out <- data
