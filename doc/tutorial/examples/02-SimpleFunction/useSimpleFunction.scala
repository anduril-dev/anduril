#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object useSimpleFunction {
    
    /** Input data */
    val csv = INPUT(path="csv.csv")

    /** Script for REvaluate */
    val calculatediffscript = INPUT(path="customPlot.r")

    /* Function that has four inputs, one output and uses two components */
    def calculateDiff(matrixIn: CSV, includeCols: String, operation: String): Latex = { 
        /** Select only rows where value of colA is greater than 3 */
        val fMatrix = CSVFilter(in = matrixIn,
                includeColumns = "*",
                lowBound="colA=3")

        /** Calculate difference of fMatrix and  */
        val matrixOut = REvaluate(table1 = fMatrix,
                  script = calculatediffscript,
                  param1 = includeCols,
                  param2 = operation)

        return matrixOut.document
    }

    /** Use function with colA and colB */
    val diff1 = calculateDiff(matrixIn = csv,
                  includeCols = "colA,colB",
                  operation = "max")

    /** Use function with colC, colD and colE */
    val diff3 = calculateDiff(matrixIn = csv,
                  includeCols = "colC,colD,colE",
                  operation = "max")

    OUTPUT(diff1)
    OUTPUT(diff3)
    
    /* Functions can output multiple objects. The easiest way is to return a tuple */
    def tupleOutput(input:CSV): (CSV,CSV) = { 
        val operation1=CSVJoin(input).out
        val operation2=CSVJoin(input).out
        return (operation1,operation2)
    }
    val two_ops=tupleOutput(csv)
    /* Tuple elements are referred to with numbers */
    OUTPUT(two_ops._1)
    OUTPUT(two_ops._2)

    /* To use names for returned values, use a NamedMap 
     * The outputs have to be the same data type!
     * */
    def hashMapOutput(input:CSV): NamedMap[CSV] = { 
        val map=NamedMap[CSV]("map")
        map("first")=CSVJoin(input).out
        val operation2=CSVJoin(input).out
        map("second")=operation2
        return map
    }
    val named_ops=hashMapOutput(csv)
    /* Elements are referred to with strings */
    OUTPUT(named_ops("first"))
    OUTPUT(named_ops("second"))
    
    /* To name the outputs, and have different data types, we must
     * create a new class that implements those types */
    
    case class myOutputType(firstPort: CSV, secondPort: BinaryFolder){}
    def differentDataTypes(input:CSV): myOutputType = {
        val operation1=CSVJoin(input)
        val operation2=QuickBash(in=input, script="mkdir $out")
        return new myOutputType(firstPort=operation1.out,
                                 secondPort=operation2.out)
    }
    val ops_with_diff_dTypes=differentDataTypes(csv)
    OUTPUT(ops_with_diff_dTypes.firstPort)
    OUTPUT(ops_with_diff_dTypes.secondPort)
}

