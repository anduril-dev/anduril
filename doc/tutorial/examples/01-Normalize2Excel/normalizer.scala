#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object Normalizer {

	/** The input files */
	val values = INPUT(path="matrix.csv")

	/** Use the z normalization*/
	val valuesNorm = LinearNormalizer(in=values,
									  method="z")

	/** Create an Excel file from the input
	and the normalized data */ 
	val excelOUT = CSV2Excel(csv=values,
							 csv2=valuesNorm.out,
							 sheetNames="input data,normalized data")

	/** Direct the Excel file (the output)
	to the output folder */
	OUTPUT(excelOUT.excelFile)
	     
}




