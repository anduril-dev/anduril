#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object DirectingOutput {

	/** Input files */
	val input = INPUT(path="DirectingOutput.csv")
	val aux   = INPUT(path="DirectingOutput.aux")
	/** Filter input based on first column of aux */
	val csv1 = CSVFilter(in=input.out,aux=aux.out)

	/** Let's do a nice latex table from the csv1 */
	val outputCSV = CSV2Latex(tabledata = csv1,
							  colFormat = "p{1cm}p{1cm}", // force all columns to be 1cm wide
							  columns   = "Col1,Col2") // Only take columns one and two

	/** Every pdf file needs a header and a footer */
	val template = LatexTemplate(authors = "Anduril Team",
								 title   = "Simple guide to making a pdf with Anduril")

	/** Create a pdf with header and footer and csv file */
	val outputPDF = LatexPDF(in = outputCSV,
							 header   = template.header,
							 footer   = template.footer)

	/** Directing output to output folder and outputting the PDF document */
	OUTPUT(outputPDF.out)
        
}

