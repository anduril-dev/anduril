#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object tTest {

    /** The input file */
    val values = INPUT(path="matrix.csv")

    /** Use the StatisticalTest component to 
    compute a t-test  */
    val tTest = StatisticalTest(matrix=values,
                                targetColumns="S1,S2,S3",
                                referenceColumns="C1,C2,C3",
                                correction="fdr")   

    /** Direct the plot folder
    to the output folder */
    OUTPUT(tTest.pvalues)
         
}



