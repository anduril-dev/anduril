#!/usr/bin/env anduril
//$OPT -d test_results
//$OPT --threads 50
//$OPT --pipe "tee $ANDURIL_EXECUTION_DIR/_log"
//$OPT --pipe "anduril-pager --ls"

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._
import anduril.microarray._
import anduril.sequencing._

/*
 * TEST pipeline for doing differential expression and survival analysis with public TCGA data
 * HTseq counts for TCGA Ovarian cancer patients was downloaded from GDC portal on 20181106 with 
   clinical data and sample info
 * The individual count files have been combined into a single expression matrix file and the "-" 
   in the TCGA ids have been replace by "_"
 * The clinical data did not include much information so the differential expression is done between 
   patients diagnosed before age 45 and patients diagnosed at ages 65 to 70 yrs
*/

object pipeline {

  val inputExpr = INPUT(path="input_matrix.csv")
  val clinicalInfo = INPUT(path="clinical.tsv")
   
  //remove samples with no age_at_diagnosis
  val prepClin1 = CSVFilter(in=clinicalInfo,regexp="age_at_diagnosis=--",negate=true,includeColumns="")

  //replace missing info on days_to_last_follow_up with zero
  val prepClin2 = CSVCleaner(in=prepClin1.out,replace="days_to_last_follow_up\n--\n0")

  //fix ids, retain important columns
  val prepClin3 = CSVCleaner(in=prepClin2.out, 
                             replace="submitter_id\n-\n_", 
                             columnsOut="submitter_id,age_at_diagnosis,vital_status,days_to_last_follow_up")
  
  /* compare primary samples of patients diagnosed before 45 yrs old 
   * and patients diagnosed between 65 and 70 */

  //subset ids for "young" group from clinical data 
  val young = CSVFilter(in=prepClin3.out,
                        highBound="age_at_diagnosis=16425",
                        lowBound="days_to_last_follow_up=730")

  //subset ids for "old" group from clinical data
  val old = CSVFilter(in=prepClin3.out,
                      lowBound="age_at_diagnosis=23725,days_to_last_follow_up=730",
                      highBound="age_at_diagnosis=25550")

  //prepare condition table for differential expression analysis
  val conditionTable = BashEvaluate(var1=young.out,
                                    var2=old.out,
                                    script="""tsvecho ID Treatment > @out1@
                                    awk 'NR>1 {print $1,"young"}' OFS="\t" @var1@ >> @out1@
                                    awk 'NR>1 {print $1,"old"}' OFS="\t" @var2@ >> @out1@""")

  //prepare expression matrix retaining only the ids of the patients we want to compare
  //we can use the conditionTable we created to keep only the columns matching those ids
  //we also need to keep the gene_id column so we do so with includeColumns parameter
  //the colOrder parameter lets us choose if we want first the columns from the includeColumns paramter or from the file
  val countMatrix = CSVFilter(in=inputExpr,
                              includeColumnsFile=conditionTable.out1, 
                              includeColumns="gene_id",
                              colOrder=true)
  
  //we are running the differential expression with deseq2, but you could choose edgeR as well
  val diff = DEan(expr=countMatrix.out,
                  treatment=conditionTable.out1,
                  options="deseq2",
                  DE_pairwise=false)

  //ensembl ids without the version part match better to entrez genes
  val fixIds = BashEvaluate(var1=diff.array("deseq2_oldvsyoung"),
                            var2=diff.diffExpr("deseq2_normalized_counts"),
                            script="""tr -d '"' < @var1@ | awk -F"\t" '{split($1,a,"."); $1=a[1]; print $0}' OFS="\t" > @out1@
                                      tr -d '"' < @var2@ | awk -F"\t" '{split($1,a,"."); $1=a[1]; print $0}' OFS="\t" > @out2@
                                    """)

  //call biomart for getting entrez ids from our ensembl gene ids
  val biomart = BiomartAnnotator( filter=fixIds.out1,
                                attributes="ensembl_gene_id,hgnc_symbol,entrezgene",
                                batchSize=500,
                                filterTypes="ensembl_gene_id",
                                martHost="www.ensembl.org", 
                                martPath="/biomart/martservice",
                                uniq=false,
                                listLayout=false)

  //remove genes that could not be annotated with entrez ids
  val cleanAnnot = CSVFilter(in=biomart.annotations, nonMissing=0.9)

  //join the entrez ids with the degs resultds
  val degsEntrezLFC = CSVJoin(in1 = fixIds.out1, in2 = cleanAnnot.out)

  //let's add an absolute log fold change column to make sorting easier
  val addAbsLFC = PythonEvaluate(var1=degsEntrezLFC.out,
                                  script="""
import pandas as pd
df = anduril.PandasReader(var1)
df['absLFC'] = df['log2FoldChange'].abs()
anduril.PandasWriter(df, out1File)""")

  //select only genes that have expression >= 50, absLFC >= 1, adjusted p-value <= 0.05
  val degs = CSVFilter(	in=addAbsLFC.out1,
                     		lowBound = "baseMean=50,absLFC=1",
                     		highBound="padj=0.05",
												includeColumns="entrezgene,hgnc_symbol,id,log2FoldChange")

  //prepare reference gene list for SPIA
  val reference = CSVFilter(in=degsEntrezLFC.out,
														includeColumns="entrezgene")

  //run SPIA with degs (first subset the 2 columns required by SPIA) and reference
  //here we are running SPIA with the built-in KEGG pathways but if you plan to use SPIA in your analysis
  //you should download the newest KEGG pathways and supply the path to those as a parameter to this component
  val pathEnrich = SPIA(deg=CSVFilter(in=degs.out,includeColumns="entrezgene,log2FoldChange").out.force(),
                        reference = reference.out.force(),
                        foldChangeColumn = "log2FoldChange",
                        nboot = 2000,
                        verbose = true)
  
  //subset the expression matrix to include only the degs we want to visualize in a heatmap
  val exprDegs = CSVJoin( in1=degs.out,
                          in2=fixIds.out2,
                          keyColumnNames="id,id")

  //Create a heatmap, for that we subset the expression matrix to include the same samples that in our condition table
  //sorted by condition. You don't need to sort them if you use the dendrogram option.
  val ygVSold = HeatMapReport( in=CSVFilter(in=exprDegs.out,
                                         includeColumnsFile=conditionTable.out1.force(),
                                         colOrder=true,
                                         includeColumns="hgnc_symbol").out.force(),
                             colLabels = CSVCleaner(in=conditionTable.out1,rename="Treatment=Type").out,
                             custom ="main='Differentially expressed genes: age of diagnosis < 45 and 65 to 70 yrs old'",
                             drawLegends = true,
                             scale = "row",
                             distanceMetric ="euclidean,NA",
                             drawRownames=false,
                             drawColnames=false)
                             //plotHeight = 10) 

  //make a folder with the relevant results from this pipeline
  val RESULTS = FolderCombiner( in1=ygVSold.out,
                                files=makeArray("degs.txt" -> degs.out,
																								"pathways.txt" -> pathEnrich.pathways),
                                exclude="document.tex",
          										  keysAsNames=true)
 
} 
