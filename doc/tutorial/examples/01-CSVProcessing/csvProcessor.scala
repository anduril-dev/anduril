#!/usr/bin/env anduril

import anduril.builtin._
import anduril.tools._
import org.anduril.runtime._

object csvProcessor {

        /** CSV with comma separated columns. */
        val csv1 = INPUT(path="csv1.csv")
        /** CSV with tab separated columns. */
        val csv2 = INPUT(path="csv2.csv")
        
        /** Transform comma separated file to tab separated. */
        val csv1clean = CSVCleaner(csv1, delimIn = ",", delimSymbol = "\\t")
        /** Strip quotation marks. */
        val csv2clean = CSVCleaner(csv2, skipQuotes = "*")
        
        /** Filter extra columns, negate chooses all columns except colX. */
        val csv1filter = CSVFilter(csv1clean, includeColumns = "colX", negate = true)
        /** Filter extra columns, negate chooses all columns except colY. */
        val csv2filter = CSVFilter(csv2clean, includeColumns = "colY", negate = true)
        
        /** Join filtered csv files; useKeys=false equals set union. */
        val csvJoined = CSVJoin(in1 = csv1filter,
                                in2 = csv2filter,
                                useKeys = false)
        
        /** Join colX and colY using SQL. */
        val csvXY = TableQuery(table1 = csv1clean,
                               table2 = csv2clean,
                               query  = "SELECT table1.\"sample\", "+
                                        "table1.\"colX\", "+
                                        "table2.\"colY\" "+
                                        "FROM table1 "+
                                        "LEFT JOIN table2 ON "+
                                        "(table1.\"sample\" = table2.\"sample\") ")
        
        OUTPUT(csvJoined)
        OUTPUT(csvXY)

}

