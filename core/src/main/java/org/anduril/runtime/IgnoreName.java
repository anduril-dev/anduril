package org.anduril.runtime;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation that indicates that the method should
 * be ignored when determining hierarchical names
 * of components. Can be applied to individual
 * methods, or classes (which applies the annotation
 * to all methods of the class).
 */
@Retention(RetentionPolicy.CLASS)
public @interface IgnoreName {
}
