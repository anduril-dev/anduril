package org.anduril.component;

/**
 * Component exit codes.
 */
public enum ErrorCode {

    /** Successful execution. */
    OK (0, "OK"),

    /** Generic failed execution. */
    ERROR (1, "Generic error"),

    /** No command file was given as command line argument. */
    NO_COMMAND_FILE (20, "No command file given"),
    
    /** The command file could not be read. */
    COMMAND_FILE_IO_ERROR (21, "Command file read error"),

    /** An input file could not be read. */
    INPUT_IO_ERROR (22, "Input file read error"),

    /** The error stream could not be written to. */
    INVALID_ERRORSTREAM (23, "Can't write to error stream"),
    
    /** A parameter is missing or has an invalid value. */
    PARAMETER_ERROR (24, "Parameter is missing or has invalid value"),
    
    /** An uncaught exception was thrown during component execution. */
    UNCAUGHT_EXCEPTION (25, "Exception that was not caught in the component"),
    
    /** An input file is badly formatted or semantically invalid. */
    INVALID_INPUT (26, "Input is invalid"),

    /** The command file contains syntax errors. */
    INVALID_COMMAND_FILE (27, "Command file is badly formatted"),
    
    /** An output port cannot be written. */
    OUTPUT_IO_ERROR (30, "Output port cannot be used"),
    ;

    private int code;
    private String desc;
    
    private ErrorCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * Return the numeric the for the exit status.
     */
    public int getCode() {
        return code;
    }
    
    /**
     * Return a human-readable description of the exit status.
     */
    public String getDesc() {
        return desc;
    }
    
    /**
     * Return an ErrorCode instance based on numeric exit status.
     * @return The ErrorCode instance, or null if code is not found.
     */
    public static ErrorCode getErrorCode(int code) {
        for (ErrorCode err: ErrorCode.values()) {
            if (err.getCode() == code) return err;
        }
        return null;
    }

}
