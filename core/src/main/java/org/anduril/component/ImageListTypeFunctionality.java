package org.anduril.component;

import org.anduril.core.network.DataType;
import org.anduril.core.network.DataTypeFunctionality;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

public class ImageListTypeFunctionality extends DataTypeFunctionality {
	
    public ImageListTypeFunctionality(DataType type) {
        super(type);
    }
    
    public boolean compare(File file1, File file2, StringBuffer difference)
            throws IOException {

        if (file1.isFile()) {
            difference.append(file1.getAbsolutePath())
                      .append(" is not a folder and thus it makes an invalid reference output.\n");
        } else
        if (file2.isFile()) {
            difference.append(file2.getAbsolutePath())
                      .append(" is not a folder.\n");
        } else {
            for (File f : file1.listFiles()) {
            	String name = f.getName();
            	if (name.charAt(0) != '.')
                   if (!super.compare(f, new File(file2, name), difference)) return false;
            }
        }
        return true;
    }

    @Override
    protected boolean compareFiles(File file1, File file2, StringBuffer difference)
            throws IOException {
        return compareImageFiles(file1, file2, difference);
    }

    static public boolean compareImageFiles(File file1, File file2, StringBuffer difference)
    throws IOException {

	if (file1.length() == 0 && file2.length() == 0) return true;

        try {
            int width=512;
            int height=512;
            float[] kernelmatrix = {
                0.111f, 0.111f, 0.111f, 
                0.111f, 0.111f, 0.111f, 
                0.111f, 0.111f, 0.111f, 
            };
            BufferedImageOp op = new ConvolveOp( new Kernel(3, 3, kernelmatrix) );
            // Resize image 1
            // returns a width x height buffer convolved with op  (box blur)
            DataBuffer db1=preprocess(file1.getAbsolutePath(),width,height,op);
              // Resize image 2
            DataBuffer db2=preprocess(file2.getAbsolutePath(),width,height,op);
              // calculate difference
            double sum_difference=0;
            for (int i=0; i< width*height; i++ ) {
                sum_difference=sum_difference+((double)db1.getElem(i)-(double)db2.getElem(i))*((double)db1.getElem(i)-(double)db2.getElem(i));
            }
            double log_difference=Math.max(0,Math.log10(sum_difference));
            System.out.println(String.format("  Difference score: %2.2f (%s)",log_difference,file1.getName()));
            // the following difference treshold is just a guess.
            if (log_difference > 16 ) {
                String msg = String.format(
                            "\n  Images are not similar: (%s %s)",
                            file2.getAbsolutePath(), file1.getAbsolutePath());
                difference.append(msg);
                return false;
            }
            
        
        } 
        catch (Exception e) {
            String msg = String.format(
                    "Exception %s while comparing images %s and %s ",
                    e.getMessage(), file1.getAbsolutePath(), file2.getAbsolutePath());
            difference.append(msg);
            return false;
        }
        
        return true;
    }    
    public static DataBuffer preprocess(String file, int width, int height, BufferedImageOp op) throws IOException {
        // Load image as BufferedImage
        BufferedImage src = ImageIO.read(new File(file));
        // Create empty standard sized image
        BufferedImage bdest = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bdest.createGraphics();
        // Defince transformation to resize original image to standard size one
        AffineTransform at =
        AffineTransform.getScaleInstance((double)width/src.getWidth(),
              (double)height/src.getHeight());
        // Set rendering to Bicubic interpolation. (nearest interpolation would make this more sensitive)
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        // Draw resized image
        g.drawRenderedImage(src,at);
        // new empty image for averaged version of the resized image
        BufferedImage blurredImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // average the resized image
        op.filter(bdest,blurredImage);
        // Get raster format data from the averaged image
        Raster r=blurredImage.getData();
        // Return data buffer
        DataBuffer db=r.getDataBuffer();
        return db;
    }
    public static final boolean isGreyscaleImage(PixelGrabber aPixelGrabber) {
        return aPixelGrabber.getPixels() instanceof byte[];
    }

}
