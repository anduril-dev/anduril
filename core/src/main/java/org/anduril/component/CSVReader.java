package org.anduril.component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

/**
 * Parse comma/tab-separated files using the SuperCSV library.
 * The CSV files must always contain a header that gives column
 * names. Lines can be iterated as lists of strings (next) or as
 * maps that bind column names to cell values (nextMap). These
 * two methods may be mixed, so it is legal to call next() after
 * nextMap().
 */
public class CSVReader implements Iterator<List<String>>, Iterable<List<String>> {
    public static final String DEFAULT_NA = "NA";
    public static final char DEFAULT_DELIMITER = '\t';
    public static final char DEFAULT_QUOTE = '"';
    
    public enum ColumnType {
        BOOLEAN,
        FLOAT,
        INT,
        STRING
    }
    
    final private String na;
    final private CsvListReader csvReader;
    final private ArrayList<String> header;
    private List<String> nextLine;
    
    /**
     * Initialized.
     * @param reader Reader from which rows are read.
     * @param skip Number of lines to skip from the beginning
     *  of the stream.
     * @param na String that denotes a missing value, represented
     *  as null at Java level.
     * @param delimiter Character that separates cells on a row.
     * @param quote Quote character. Inside quotes, delimiter
     *  characters can be used freely without separating cells.
     */
    public CSVReader(Reader reader, int skip, String na, char delimiter, char quote) throws IOException {
        if (skip > 0) {
            if (!(reader instanceof BufferedReader)) {
                reader = new BufferedReader(reader);
            }
            BufferedReader bufReader = (BufferedReader)reader;
            for (int i=0; i<skip; i++) bufReader.readLine();
        }
        this.na = na;
        this.csvReader = new CsvListReader(reader,
                new CsvPreference.Builder(quote, delimiter, "\n").build());
        
        String[] firstLine = csvReader.getHeader(false);
        if (firstLine == null) throw new IOException("Header could not be read.");
        this.header = new ArrayList<String>(firstLine.length);
        for (String col: firstLine) {
            this.header.add(col);
        }
        this.header.trimToSize();
    }
    
    /**
     * Init with no skipped lines and default settings.
     * @param reader Reader from which rows are read.
     */
    public CSVReader(Reader reader) throws IOException {
        this(reader, 0, DEFAULT_NA, DEFAULT_DELIMITER, DEFAULT_QUOTE);
    }
    
    /**
     * Init with no skipped lines and default settings.
     * @param csvFile File from which rows are read.
     */
    public CSVReader(File csvFile) throws IOException {
        this(new FileReader(csvFile), 0, DEFAULT_NA, DEFAULT_DELIMITER, DEFAULT_QUOTE);
    }
    
    private List<String> readLine() throws IOException {
        final List<String> line = this.csvReader.read();
        if (line == null) return null;
        if (this.na != null) {
            for (int i=0; i<line.size(); i++) {
                if (Objects.equals(this.na, line.get(i))) line.set(i, null);
            }
        }
        return line;
    }

    @Override
    /**
     * Return true if the input stream has one or more
     * unread rows.
     */
    public boolean hasNext() {
        if (this.nextLine == null) {
            try {
                this.nextLine = readLine();
            } catch (IOException e) {
                return false;
            }
        }
        return this.nextLine != null;
    }

    @Override
    /**
     * Return the next unread row, or null if there
     * are no more unread rows. Missing values are
     * returned as null. Note: the List object may
     * be reused by the underlying library, so after
     * another call to next(), the list may be modified.
     * Create a copy of the list if the contents of
     * the row need to be preserved.
     */
    public List<String> next() {
        if (this.nextLine == null) {
            try {
                this.nextLine = readLine();
            } catch (IOException e) {
                return null;
            }
        }
        List<String> line = this.nextLine;
        this.nextLine = null;
        return line;
    }

    @Override
    /** 
     * Do nothing.
     */
    public void remove() {}

    @Override
    /**
     * Return an iterator over the rows.
     */
    public Iterator<List<String>> iterator() {
        return this;
    }
    
    /**
     * Close the underlying reader.
     */
    public void close() throws IOException {
        this.csvReader.close();
    }
    
    public int getLineNumber() {
        return this.csvReader.getLineNumber();
    }
    
    /**
     * Return the next row as a map from column names
     * to cell values.
     * @param includeColumns List of column names that
     *  are included in the map, or null if all columns
     *  are to be included.
     */
    public Map<String,String> nextMap(Set<String> includeColumns) {
        if (this.nextLine == null) {
            try {
                this.nextLine = readLine();
            } catch (IOException e) {
                return null;
            }
        }
        if (this.nextLine == null) return null;
        
        Map<String,String> map = new HashMap<String, String>();
        for (int i=0; i<this.nextLine.size(); i++) {
            final String colName = this.header.get(i);
            if (includeColumns != null && !includeColumns.contains(colName)) continue;
            map.put(colName, this.nextLine.get(i));
        }
        this.nextLine = null;
        return map;
    }
    
    /**
     * Return the next row as a map from column names
     * to cell values. All columns are included.
     */
    public Map<String,String> nextMap() {
        return nextMap(null);
    }
    
    /**
     * Parse successive cells of a line as integers.
     * @param line The line read from the CSV file.
     * @param beginIndex Index (0 to N-1) of the first integer column, inclusive.
     * @param endIndex Index (0 to N-1) of the last integer column, inclusive.
     * @param includeColumns If non-null, gives column names between
     *  beginIndex and endIndex that are included in the array. If null, all
     *  indices are used.
     * @param missingValue For cell containing a missing value, this numeric
     *  value is inserted to the array.
     * @return Integer array containing integer values of the cells.
     */
    public int[] parseInt(List<String> line, int beginIndex, int endIndex, Set<String> includeColumns, int missingValue) {
        if (endIndex >= line.size()) endIndex = line.size() - 1;
        final int size = includeColumns == null ?
                endIndex - beginIndex + 1 : includeColumns.size();
        int[] array = new int[size];
        
        int dest = 0;
        for (int i=beginIndex; i<=endIndex; i++) {
            final String colName = header.get(i);
            if (includeColumns != null && !includeColumns.contains(colName)) {
                continue;
            }
            final String value = line.get(i);
            if (value == null) array[dest] = missingValue;
            else {
                array[dest] = Integer.parseInt(value);
            }
            dest++;
        }
        return array;
    }

    /**
     * Parse successive cells of a line as doubles.
     * @param line The line read from the CSV file.
     * @param beginIndex Index (0 to N-1) of the first double column, inclusive.
     * @param endIndex Index (0 to N-1) of the last double column, inclusive.
     * @param includeColumns If non-null, gives column names between
     *  beginIndex and endIndex that are included in the array. If null, all
     *  indices are used.
     * @param missingValue For cell containing a missing value, this numeric
     *  value is inserted to the array. Double.NaN can be used.
     * @return Double array containing double values of the cells.
     */
    public double[] parseDouble(List<String> line, int beginIndex, int endIndex, Set<String> includeColumns, double missingValue) {
        if (endIndex >= line.size()) endIndex = line.size() - 1;
        final int size = includeColumns == null ?
                endIndex - beginIndex + 1 : includeColumns.size();
        double[] array = new double[size];
        
        int dest = 0;
        for (int i=beginIndex; i<=endIndex; i++) {
            final String colName = header.get(i);
            if (includeColumns != null && !includeColumns.contains(colName)) {
                continue;
            }
            final String value = line.get(i);
            if (value == null) array[dest] = missingValue;
            else {
                array[dest] = Double.parseDouble(value);
            }
            dest++;
        }
        return array;
    }
    
    /**
     * Return the column names of the CSV file.
     */
    public List<String> getHeader() {
        return this.header;
    }
    
    /**
     * Return the index (0 to N-1) of given column name.
     * @param name Column name
     * @param mandatory If true, IllegalArgumentException is
     *  throws if the column is not found. If false, -1 is
     *  returned if the column is not found.
     */
    public int getColumnIndex(String name, boolean mandatory) {
        int index = header.indexOf(name);
        if (mandatory && index < 0) {
            throw new IllegalArgumentException("Column not found: "+name);
        }
        return index;
    }
    
    /**
     * Return the index (0 to N-1) of given column name.
     * Throw IllegalArgumentException if the column is not found.
     * @param name Column name
     */
    public int getColumnIndex(String name) {
        return getColumnIndex(name, true);
    }
    
    /**
     * Helper for inferColumnTypes: create a boolean
     * array filled with true's.
     */
    private static boolean[] getTrueBooleanArray(final int size) {
        boolean[] array = new boolean[size];
        for (int i=0; i<size; i++) array[i] = true;
        return array;
    }
    
    /**
     * Infer column types of the CSV file by iterating over rows.
     * Recognized types are boolean ("true" or "false"), integer,
     * float and string. For example, a column with contents
     * "1.5", "NA", "-2e5" is a float column (missing values are
     * allowed for all types), while "1", "-5", "NA" is an integer
     * column. A column with only missing values is a string column.
     * 
     * <p>
     * Note: This method iterates over the complete stream,
     * consuming all rows. A new reader is needed if rows are to
     * be iterated again.
     * </p>
     * 
     * @return A list with one entry for each column. The type
     *  is never null. 
     */
    public List<ColumnType> inferColumnTypes() throws IOException {
        final int NUM_COLUMNS = this.getHeader().size();
        boolean[] isBoolean = getTrueBooleanArray(NUM_COLUMNS);
        boolean[] isInt = getTrueBooleanArray(NUM_COLUMNS);
        boolean[] isFloat = getTrueBooleanArray(NUM_COLUMNS);

        for (List<String> row: this) {
            for (int col=0; col<NUM_COLUMNS; col++) {
                final String value;
                try { value = row.get(col); }
                catch (IndexOutOfBoundsException err) {
                	throw new IllegalStateException("Expected "+NUM_COLUMNS+
                			                        " columns but got "+row.size()+
                			                        ":\n"+row);
                }
                if (value == null) continue;
                
                if (isBoolean[col]) {
                    if (!(value.equals("true") || value.equals("false"))) {
                        isBoolean[col] = false;
                    }
                }

                if (isInt[col]) {
                    try {
                        Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        isInt[col] = false;
                    }
                }

                if (isFloat[col]) {
                    if (!value.startsWith("0x") && (value.endsWith("d") || value.endsWith("f"))) {
                    	/* Numeric with d/f suffix: treated as string */
                    	isFloat[col] = false;
                    } else {
                    	try {
                    		Double.parseDouble(value);
                    	} catch (NumberFormatException e) {
                    		isFloat[col] = false;
                    	}
                    }
                }
            }
        }

        List<ColumnType> types = new ArrayList<ColumnType>(NUM_COLUMNS);
        for (int col=0; col<NUM_COLUMNS; col++) {
            ColumnType type;
            if (isBoolean[col] && isInt[col]) type = ColumnType.STRING; // All NAs
            else if (isBoolean[col]) type = ColumnType.BOOLEAN;
            else if (isInt[col]) type = ColumnType.INT;
            else if (isFloat[col]) type = ColumnType.FLOAT;
            else type = ColumnType.STRING;
            types.add(type);
        }
        return types;    
    }
}
