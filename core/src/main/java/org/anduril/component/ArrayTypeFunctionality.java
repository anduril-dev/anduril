package org.anduril.component;

import org.anduril.core.network.DataType;
import org.anduril.core.network.DataTypeFunctionality;
import org.anduril.core.network.Port;

import java.io.File;
import java.io.IOException;

public class ArrayTypeFunctionality extends DataTypeFunctionality {
    public ArrayTypeFunctionality(DataType elementType) {
        super(elementType);
    }
    
    @Override
    public boolean compare(File file1, File file2, StringBuffer difference)
    throws IOException {
        if (!file1.isDirectory()) {
            throw new IOException("file1 is not directory: "+file1.getAbsolutePath());
        }
        if (!file2.isDirectory()) {
            throw new IOException("file2 is not directory: "+file2.getAbsolutePath());
        }
        
        File indexFile1 = new File(file1, Port.ARRAY_INDEX_FILE);
        File indexFile2 = new File(file2, Port.ARRAY_INDEX_FILE);
        
        if (!indexFile1.exists()) {
            throw new IOException("Array index does not exist: "+indexFile1);
        }
        if (!indexFile2.exists()) {
            throw new IOException("Array index does not exist: "+indexFile2);
        }
        
        DataTypeFunctionality elementFunc = getType().makeFunctionalityInstance(false);
        
        final IndexFile index1 = IndexFile.read(indexFile1);
        final IndexFile index2 = IndexFile.read(indexFile2);
        
        if (index1.size() != index2.size()) {
            difference.append(String.format("Array sizes are different: %d vs %d",
                    index1.size(), index2.size()));
            return false;
        }
        
        final int N = index1.size();
        for (int i=0; i<N; i++) {
            final String key1 = index1.getKey(i);
            final String key2 = index2.getKey(i);
            if (!key1.equals(key2)) {
                difference.append(String.format("Keys differ at position %d: %s vs %s", i+1, key1, key2));
                return false;
            }

            final File element1 = index1.getFile(i);
            final File element2 = index2.getFile(i);
            boolean equal = elementFunc.compare(element1, element2, difference);
            if (!equal) return false;
        }
        
        return true;
    }
}
