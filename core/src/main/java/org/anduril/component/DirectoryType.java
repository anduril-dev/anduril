package org.anduril.component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for implementing directory data types.
 * This class supports directory data types where filenames
 * are numeric (match the regular expression <code>[0-9]+</code>).
 *
 */
public class DirectoryType {
    private File dir;
    
    /**
     * Initialize.
     * @param dir Directory of the data item.
     */
    public DirectoryType(File dir) {
        if (dir == null) throw new NullPointerException("dir is null");
        if (dir.isFile()) {
            throw new IllegalArgumentException("Given dir is a file: "+dir);
        }
        if (!dir.exists()) dir.mkdirs();
        this.dir = dir;
    }
    
    /**
     * Return the directory.
     */
    public File getDirectory() {
        return dir;
    }
    
    public File getFile(int index) {
        if (index < 0) throw new IllegalArgumentException("index is negative");
        return new File(dir, String.valueOf(index));
    }
    
    /**
     * Return numeric values for the files that exist in the
     * directory.
     */
    public List<Integer> getIndices() {
        List<Integer> indices = new ArrayList<Integer>();
        for (File f: dir.listFiles()) {
            String re = "[0-9]+";
            if (f.getName().matches(re)) {
                indices.add(Integer.valueOf(f.getName()));
            }
        }
        return indices;
    }
    
    /**
     * Return the maximum index of the files that exist in the
     * directory.
     */
    public int getMaxIndex() {
        int max = 0;
        for (int index: getIndices()) {
            if (index > max) max = index;
        }
        return max;
    }
    
    /**
     * Return the filename for the first index that doesn't
     * currently exist. This is used to add new files. If the
     * directory is empty, the filename is "1". 
     */
    public File getNextFile() {
        return getFile(getMaxIndex()+1);
    }
}
