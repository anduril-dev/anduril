package org.anduril.component;

import org.anduril.core.network.DataType;
import org.anduril.core.network.DataTypeFunctionality;
import org.anduril.core.network.PresentationType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TextFileTypeFunctionality extends DataTypeFunctionality {

    public TextFileTypeFunctionality(DataType type) {
        super(type);
    }
    
    @Override
    public boolean compare(File file1, File file2, StringBuffer difference) throws IOException {
    	if (getType().getPresentationType() == PresentationType.MULTIFILE) {
    		File[][] fileMap = Tools.mapMultifilePaths(file1, file2);
    		for (File[] files: fileMap) {
    			final boolean ok;
    			if (fileMap.length == 1 || files[0].getName().endsWith(".txt") || files[0].getName().endsWith(".csv")) {
    				ok = compareTextFiles(files[0], files[1], difference);
    			} else {
    				ok = compareFiles(files[0], files[1], difference);
    			}
    			if (!ok) return false;
    		}
    		return true;
    	} else {
    		return compareTextFiles(file1, file2, difference);
    	}
    }
    
    static public boolean compareTextFiles(File file1, File file2, StringBuffer difference)
            throws IOException {
        if (file1.length() == 0 && file2.length() == 0) return true;
        BufferedReader reader1 = new BufferedReader(new FileReader(file1));
        BufferedReader reader2 = new BufferedReader(new FileReader(file2));
        try {
            int lineNumber = 0;
            while (true) {
                String line1 = reader1.readLine();
                String line2 = reader2.readLine();
                lineNumber++;
                if (line1 == null && line2 == null) break;
                if (line1 == null || line2 == null) {
                    /* One has EOF but another has not */
                    String msg = "The files have different number of rows";
                    difference.append(msg);
                    return false;
                }
                
                final boolean equal = DataTypeFunctionality.compareRegexp(line1, line2);
                if (!equal) {
                    String msg = String.format(
                            "Values differ at row %d: %s vs. %s",
                            lineNumber, line1, line2);
                    difference.append(msg);
                    return false;
                }
            }
        } finally {
            reader1.close();
            reader2.close();
        }
        
        return true;
    }    
}
