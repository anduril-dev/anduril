package org.anduril.component;

import org.anduril.core.utils.TextTools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LatexTools {

    public static final String DOCUMENT_FILE = "document.tex";

    public static void writeDocument(File outDir, List<String> latexLines) throws IOException {
        outDir.mkdirs();
        StringList.write(latexLines, new File(outDir, DOCUMENT_FILE));
    }

    public static String quote(String str) {
        if (str == null) return null;

        str = str.replace("\\", "TmpBackslash");

        final String ESCAPE_CHARS = "$#%&_{}";
        for (int i=0; i<ESCAPE_CHARS.length(); i++) {
            final String ch = ESCAPE_CHARS.substring(i, i+1);
            str = str.replace(ch, "\\"+ch);
        }
        str = str.replace("TmpBackslash", "$\\backslash$");

        StringBuilder sb = new StringBuilder(str.length()*2);
        for (int i=0; i<str.length(); i++) {
            char ch = str.charAt(i);
 
            switch (ch) {

            case '^': sb.append("\\^{}"); break;
            case '~': sb.append("\\~{}"); break;
            case '<': sb.append("$<$"); break;
            case '>': sb.append("$>$"); break;
            case '|': sb.append("$|$"); break;
            case '\u00E4': sb.append("\\\"{a}"); break;
            case '\u00F6': sb.append("\\\"{o}"); break;
            case '\u00FC': sb.append("\\\"{u}"); break;
            case '\u00E5': sb.append("\\r{a}"); break;
            case '\u00C4': sb.append("\\\"{A}"); break;
            case '\u00D6': sb.append("\\\"{O}"); break;
            case '\u00DC': sb.append("\\\"{U}"); break;
            case '\u00C5': sb.append("\\r{A}"); break;

            default: sb.append(ch);
            }
        }

        return sb.toString();
    }
 
    public static List<String> formatFigure(String figureFilename,
                                            String label,
                                            String caption,
                                            String position,
                                            Double width,
                                            Double height) {
        if (figureFilename == null) {
            throw new NullPointerException("figureFilename is null");
        }

        List<String> result = new ArrayList<String>();

        String s = "\\begin{figure}";
        if (position != null) s += "["+position+"]";
        result.add(s);
        result.add("\\begin{center}");
        
        String params = null;
        if (width != null && height != null) {
            params = String.format(Locale.US, "width=%.2fcm,height=%.2fcm,keepaspectratio=true", width, height);
        }
        else if (width != null) {
            params = String.format(Locale.US, "width=%.2fcm", width);
        }
        else if (height != null) {
            params = String.format(Locale.US, "height=%.2fcm", height);
        }
        
        if (params == null) {
            result.add(String.format("\\includegraphics{%s}", figureFilename));
        } else {
            result.add(String.format("\\includegraphics[%s]{%s}", params, figureFilename));
        }

        if (caption != null) {
            result.add(String.format("\\caption{%s}", caption));
        }
        if (label != null) {
            result.add(String.format("\\label{%s}", label));
        }

        result.add("\\end{center}");
        result.add("\\end{figure}");
        return result;
    }

    public static List<String> formatFigure(String figureFilename,
                                            String label,
                                            String caption,
                                            String position,
                                            Double width) {
        return formatFigure(figureFilename, label, caption, position, width, null);
    }
 
    private static boolean tableIsEmpty(List<List<String>> table, boolean firstHeader) {
        return table.isEmpty() ||
               (firstHeader && table.size() <= 1);
    }

    public static List<String> formatTabular(List<List<String>> table,
                                             String             alignment,
                                             boolean            firstHeader) {
        return formatTabular(table, alignment, firstHeader, false);
    }

    public static List<String> formatTabular(List<List<String>> table,
                                             String             alignment,
                                             boolean            firstHeader,
                                             boolean            longtable) {
        if (table == null) {
            throw new NullPointerException("table is null");
        }
        if (alignment == null) {
            throw new NullPointerException("alignment is null");
        }
        
        List<String> result = new ArrayList<String>();
        if (tableIsEmpty(table, firstHeader)) return result;

        String tType = longtable ? "longtable" : "tabular";
        result.add(String.format("\\begin{%s}{%s}", tType, alignment));

        int columns = -1;
        int size    = table.size();
        for (int i=0; i<size; i++) {
            List<String> row = table.get(i);
            if (row == null) throw new NullPointerException("row is null: "+i);
            
            if (columns < 0) columns = row.size();
            else if (row.size() != columns) {
                String msg = String.format("Invalid number of columns (%d) on row %d: expected %d",
                    row.size(), i, columns);
                throw new IllegalArgumentException(msg);
            }

            String line = TextTools.join(row, " & "); 
            if (firstHeader && i==0) {
                line += "\\tabularnewline\\hline";
                result.add(line);
                if (longtable) {
                    result.add("\\endfirsthead{}");
                    result.add(line);
                    result.add(String.format("\\endhead\\hline\\multicolumn{%d}{r}{\\textit{Continued on next page\\ldots\\/}}",row.size()));
                    result.add("\\endfoot\\hline\\endlastfoot{}%");
                }
            } else {
            	if (i < size-1) {
            		line += "\\tabularnewline{}%";
            	}
                result.add(line);
            }
        }

        result.add(String.format("\\end{%s}", tType));
        if (longtable) result.add("\\addtocounter{table}{-1}");
        return result;
    }

    public static List<String> formatTable(List<List<String>> table,
                                           String             alignment,
                                           boolean            firstHeader,
                                           String             caption,
                                           String             position) {
        List<String> result = new ArrayList<String>();
        if (tableIsEmpty(table, firstHeader)) return result;
 
        if (position == null) result.add("\\begin{table}");
        else result.add(String.format("\\begin{table}[%s]", position));
 
        result.addAll(formatTabular(table, alignment, firstHeader, false));
 
        if (caption != null && !caption.isEmpty()) {
            result.add(String.format("\\caption{%s}", caption));
        }
 
        result.add("\\end{table}");
        return result;
    }
    
}
