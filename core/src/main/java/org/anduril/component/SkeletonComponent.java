package org.anduril.component;

import javax.management.Notification;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.management.*;

/**
 * Main entry point for components written in Java.
 * This class contains a run method that is executed
 * from the main method of the component.
 * 
 * <p>
 * Sample component:
 * <pre>
 * public class MyComponent extends SkeletonComponent {
 * 		protected ErrorCode runImpl(CommandFile cf) throws Exception {
 * 			// Do actual work here.
 * 		}
 * 
 * 		public static void main(String[] args) {
 * 			new MyComponent().run(args);
 * 		}
 * }
 * </pre>
 * </p>
 * 
 * <p>
 * This class monitors memory usage of Java heap and provides a
 * callback method {@link #lowMemoryWarning(MemoryPoolMXBean)} that
 * can be used by components to react to low memory situations.
 * Warning threshold is configurable using the constructor.
 * The default callback implementation prints an error message,
 * but a component overriding the method may clear cached objects
 * to release memory, for example. Memory monitoring is done using
 * java.lang.management.
 * </p>
 */
public abstract class SkeletonComponent implements NotificationListener {
    /** Default memory usage percentage threshold above which a low
      * memory warning is activated. */ 
    public static final double DEFAULT_MEMORY_WARNING_THRESHOLD = 0.9;
    
    private double memoryThreshold;
    
    /**
     * Initialize.
     * 
     * @param memoryThreshold Memory usage percentage threshold above which
      * a low memory warning is signaled. Must be between 0 and 1. For example,
      * 0.85 signals an error when 85% of heap memory is used. Use 1 to disable
      * monitoring. Signaling is done using a call to
      * {@link #lowMemoryWarning(MemoryPoolMXBean)}. 
     */
    public SkeletonComponent(double memoryThreshold) {
        if (memoryThreshold < 0 || memoryThreshold > 1) {
            throw new IllegalArgumentException("memoryThreshold must be between 0 and 1");
        }
        this.memoryThreshold = memoryThreshold;
    }
    
    /**
     * Initialize with default memory warning threshold.
     */
    public SkeletonComponent() {
        this(DEFAULT_MEMORY_WARNING_THRESHOLD);
    }
    
    /**
     * Event listener that is called when free memory is getting low.
     * The default implementation prints a warning message to
     * standard output and returns false.
     * 
     * @param pool The specific memory pool that is running out of free
     *  memory. Notice, however, that the heap may consist of several
     *  pools, of which the present pool is only one. To get global
     *  memory usage, use {@link MemoryMXBean}.
     * @return True if the component wishes to receive new warnings
     *  when memory gets low again, of false if the memory monitoring
     *  mechanism should be disabled from this on. Disabling monitoring
     *  ensures that a warning is not printed several times.
     */
    public boolean lowMemoryWarning(MemoryPoolMXBean pool) {
        final double MB = 1024 * 1024;
        long used = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        long max = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getMax();
        
        System.out.println(String.format(
                "Warning: Java heap memory is getting low: used %.1f of %.1f MB",
                used / MB, max / MB));
        
        for (MemoryPoolMXBean p: ManagementFactory.getMemoryPoolMXBeans()) {
            if (p.getType() != MemoryType.HEAP) continue;
            used = p.getUsage().getUsed();
            max = p.getUsage().getMax();
            if (max < 5*MB) continue; /* Don't show small pools to avoid clutter */
            
            System.out.println(String.format("Memory pool: %s, used %.1f of %.1f MB",
                    p.getName(),
                    used / MB,
                    max / MB));
        }
        return false;
    }

    @Override
    /**
     * Respond to a low memory situation by writing an error to stdout.
     * Also remove the memory monitor to ensure that the warning is
     * only given once.
     */
    public void handleNotification(Notification notification, Object handback) {
        String notifType = notification.getType();
        if (notifType.equals(MemoryNotificationInfo.MEMORY_THRESHOLD_EXCEEDED)) {
            MemoryPoolMXBean heap = (MemoryPoolMXBean)handback;
            boolean continueListening = lowMemoryWarning(heap);
            if (!continueListening) {
                /* Disable memory monitoring */
                heap.setUsageThreshold(0);
            }
        }
    }

    /**
     * Set up memory monitoring in order to respond to a
     * low memory situation.
     * @param usagePercentage Percentage between 0 and 1
     * 	at which usage a warning is given.
     */
    private void setMemoryMonitor(double usagePercentage) {
        // Adapted from http://www.javaspecialists.co.za/archive/Issue092.html
        // by Dr. Heinz M. Kabutz
        if (usagePercentage == 1) return;
        
        /* We want to monitor the heap area storing long-lived
         * objects. We try to find it by the following heurestics. */
        MemoryPoolMXBean heap = null;
        for (MemoryPoolMXBean pool: ManagementFactory.getMemoryPoolMXBeans()) {
            if (pool.getType() == MemoryType.HEAP &&
                    pool.isUsageThresholdSupported()) {
                heap = pool;
                break;
            }
        }
        if (heap == null) return;

        final long maxMemory = heap.getUsage().getMax();
        final long threshold = (long)(maxMemory*usagePercentage);
        heap.setUsageThreshold(threshold);

        MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
        NotificationEmitter emitter = (NotificationEmitter) mbean;
        emitter.addNotificationListener(this, null, heap);
    }

    /**
     * Perform the actions of the component.
     * @param cf Parsed command file that is used to access
     * 	inputs, outputs, parameters etc.
     * @return Exit status where 0 is success and non-zero is
     * 	error. Error codes are in {@link ErrorCode}.
     * @throws Exception If an unrecoverable error occurs.
     * 	The run method catches this exception and writes the
     * 	stack trace to error log.
     */
    protected abstract ErrorCode runImpl(CommandFile cf) throws Exception;

    /**
     * Entry point for components. Parse the command file and
     * execute runImpl with the command file as argument. Exit the
     * virtual machine with an appropriate exit code. Exit codes
     * are in {@link ErrorCode}.
     * 
     * <p>
     * This method catches all exceptions and runtime errors.
     * If the error file is defined (i.e. if command line was
     * parsed successfully), stack trace is written to the error
     * file. Otherwise, an exit code is returned. 
     * </p>
     * 
     * @param args Command line arguments.
     */
    public final void run(String[] args) {
        if (args.length == 0) System.exit(ErrorCode.NO_COMMAND_FILE.getCode());

        CommandFile cf;
        try {
            cf = CommandFile.fromFile(new File(args[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace(System.err);
            System.exit(ErrorCode.COMMAND_FILE_IO_ERROR.getCode());
            return;
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(ErrorCode.COMMAND_FILE_IO_ERROR.getCode());
            return;
        } catch (IllegalArgumentException e) {
            e.printStackTrace(System.err);
            System.exit(ErrorCode.INVALID_COMMAND_FILE.getCode());
            return;
        } catch (Throwable e) {
            e.printStackTrace(System.err);
            System.exit(ErrorCode.UNCAUGHT_EXCEPTION.getCode());
            return;
        }

        setMemoryMonitor(this.memoryThreshold);

        try {
            ErrorCode status = runImpl(cf);
            if (status == null) {
            	cf.writeError("Component exit code was null! Using "+
                              ErrorCode.ERROR+" instead.");
            	status = ErrorCode.ERROR;
            }
            cf.close();
            System.exit(status.getCode());
        } catch (Throwable e) {
            cf.writeError(e);
            System.exit(ErrorCode.UNCAUGHT_EXCEPTION.getCode());
        }
    }
}
