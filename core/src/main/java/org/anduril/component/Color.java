package org.anduril.component;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a RGB color. Internally, the color
 * is stored as a floating point value from 0 to 1, in
 * linear color space (gamma = 1). Conversion to and
 * from hex strings (#rrggbb) and 8-bit triples are
 * supported, using gamma correction. Facilities for
 * creating color slides are provided.
 */
public class Color {
    /** The default gamma exponent. */
    public static final double DEFAULT_GAMMA = 2.2;
    
    private final double red;
    private final double green;
    private final double blue;
    
    /**
     * Initialize from linear color values (gamma = 1)
     * in the range 0 to 1.
     * @param red Red component
     * @param green Green component
     * @param blue Blue component
     */
    public Color(double red, double green, double blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        validate();
    }
    
    /**
     * Initialize from 8-bit values, using gamma correction.
     * @param red8 Red component, from 0 to 255
     * @param green8 Green component, from 0 to 255
     * @param blue8 Blue component, from 0 to 255
     * @param gamma Gamma exponent used for gamma correction.
     *  Typical values are from 1.8 to 2.5.
     */
    public Color(int red8, int green8, int blue8, double gamma) {
        this.red = Math.pow(red8/255.0, gamma);
        this.green = Math.pow(green8/255.0, gamma);
        this.blue = Math.pow(blue8/255.0, gamma);
        validate();
    }
    
    /**
     * Initialize from a hex string (#rrggbb or #rgb), using
     * gamma correction.
     * @param hexFormat Hex color string.
     * @param gamma Gamma exponent used for gamma correction.
     *  Typical values are from 1.8 to 2.5.
     */
    public Color(String hexFormat, double gamma) {
        if (hexFormat.startsWith("#")) hexFormat = hexFormat.substring(1);
        if (hexFormat.length() == 3) {
            /* ABC is converted to AABBCC */
            final String c1 = hexFormat.substring(0, 1);
            final String c2 = hexFormat.substring(1, 2);
            final String c3 = hexFormat.substring(2, 3);
            hexFormat = c1 + c1 + c2 + c2 + c3 + c3;
        }
        
        final int RADIX = 16;
        final int r =  Integer.parseInt(hexFormat.substring(0, 2), RADIX);
        final int g = Integer.parseInt(hexFormat.substring(2, 4), RADIX);
        final int b = Integer.parseInt(hexFormat.substring(4, 6), RADIX);
        this.red = Math.pow(r/255.0, gamma);
        this.green = Math.pow(g/255.0, gamma);
        this.blue = Math.pow(b/255.0, gamma);
        validate();
    }
    
    /**
     * Initialize from a hex string (#rrggbb or #rgb), using default
     * gamma.
     * @param hexFormat Hex color string.
     */
    public Color(String hexFormat) {
        this(hexFormat, DEFAULT_GAMMA);
    }
    
    private void validate() {
        if (this.red < 0 || this.red > 1) {
            throw new IllegalArgumentException("Illegal red value: "+this.red);
        }
        if (this.green < 0 || this.green > 1) {
            throw new IllegalArgumentException("Illegal green value: "+this.green);
        }
        if (this.blue < 0 || this.blue > 1) {
            throw new IllegalArgumentException("Illegal blue value: "+this.blue);
        }
    }
    
    /**
     * Return the red component as a linear value
     * from 0 to 1.
     */
    public double getRed() { return this.red; }

    /**
     * Return the green component as a linear value
     * from 0 to 1.
     */
    public double getGreen() { return this.green; }
    
    /**
     * Return the blue component as a linear value
     * from 0 to 1.
     */
    public double getBlue() { return this.blue; }
    
    /**
     * Convert the color to three 8-bit values,
     * using gamma correction.
     * @param gamma Gamma exponent used for gamma correction.
     *  Typical values are from 1.8 to 2.5.
     */
    public int[] to8bit(double gamma) {
        final double exp = 1.0/gamma;
        final int r = (int)Math.round(Math.pow(this.red, exp) * 255);
        final int g = (int)Math.round(Math.pow(this.green, exp) * 255);
        final int b = (int)Math.round(Math.pow(this.blue, exp) * 255);
        return new int[] { r, g, b };
    }
    
    /**
     * Convert the color to three 8-bit values, using
     * default gamma.
     */
    public int[] to8bit() {
        return to8bit(DEFAULT_GAMMA);
    }
    
    /**
     * Convert the color to a hex string (#rrggbb), using
     * gamma correction.
     * @param gamma Gamma exponent used for gamma correction.
     *  Typical values are from 1.8 to 2.5.
     */
    public String toHex(double gamma) {
        int[] colors = to8bit(gamma);
        return String.format("#%02x%02x%02x", colors[0], colors[1], colors[2]);
    }
    
    /**
     * Convert the color to a hex string (#rrggbb), using
     * default gamma.
     */
    public String toHex() {
        return toHex(DEFAULT_GAMMA);
    }
    
    /**
     * Mix two colors using linear interpolation.
     * @param other The other color.
     * @param ratio From 0 to 1 inclusive, indicates the proportions
     *  of this color and other color. For 0, the current
     *  color is returned, and for 1, the other color is
     *  returned. For, e.g., 0.5, the result is an average
     *  of the two colors.
     */
    public Color mix(Color other, double ratio) {
        if (ratio < 0 || ratio > 1) {
            throw new IllegalArgumentException("ratio must be between 0 and 1");
        }
        final double complement = 1.0 - ratio;
        final double red = ratio*this.getRed() + complement*other.getRed();
        final double green = ratio*this.getGreen() + complement*other.getGreen();
        final double blue = ratio*this.getBlue() + complement*other.getBlue();
        return new Color(red, green, blue);

    }
    
    /**
     * Create a color slide by interpolating between
     * three colors. Interpolation goes from start
     * to middle color and from middle to end color.
     * @param n Number of colors in the slide.
     * @param start Start color.
     * @param middle Middle color.
     * @param end End color.
     */
    public static List<Color> interpolate(int n, Color start, Color middle, Color end) {
        List<Color> colors = new ArrayList<Color>();
        
        final int M = n / 2;
        final double STEP = 1.0 / M;
        final boolean isEven = (n % 2) == 0;
        final int middlePos = isEven ? M : M+1;
        
        double pos = 0.0; /* Runs from 0 to 1 to 0 */
        
        for (int i=0; i<middlePos; i++) {
            colors.add(start.mix(middle, 1-pos));
            pos += STEP;
        }
        
        pos -= STEP;
        if (!isEven) pos -= STEP;
        
        for (int i=0; i<M; i++) {
            colors.add(middle.mix(end, pos));
            pos -= STEP;
        }
        
        return colors;
    }
    
    /**
     * Choose a color from a list based on a numeric value.
     * When the value is smaller than the low limit, the first
     * color is returned; for values larger than the high limit,
     * the last color is returned. For values between the limits,
     * the color index in computed based on the position of
     * the value on the range from low limit to high limit.
     *  
     * @param colors List of colors.
     * @param value Used for computing the color index. 
     * @param low Low threshold, under which the color is always
     *  the first color.
     * @param high High threshold, under which the color is always
     *  the last color.
     */
    public static Color chooseColor(List<Color> colors, double value, double low, double high) {
        final int N = colors.size();
        if (low > high) {
            value = -value;
            low = -low;
            high = -high;
        }
        if (value <= low) return colors.get(0);
        else if (value >= high) return colors.get(N-1);
        else {
            final double pos = (value-low) / (high-low); /* 0 to 1 */
            return colors.get((int)Math.round(pos*(N-1))); /* 0 to N-1 */
        }
    }
}