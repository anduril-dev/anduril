package org.anduril.component;

import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.core.network.Port;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Represents an Anduril array index.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.core.Version Version} 1.2.1
 */
public class IndexFile implements Iterable<String> {

	private Map<String,File> files = new HashMap<String,File>();
	private List<String>     keys  = new LinkedList<String>();

	/**
	 * Initialize an empty index.
	 */
	public IndexFile() {}

	public Iterator<String> iterator() { return keys.iterator(); }

    /**
     * Returns the key associated to the given index (the first one is 0).
     * Notice that the integer index is different from array keys: keys are
     * always strings, even if they are syntactically valid integers.
     */
	public String getKey(int index) {
	    return this.keys.get(index);
	}
	
	/**
	 * Returns the file associated to the given index (the first one is 0).
	 * Notice that the integer index is different from array keys: keys are
	 * always strings, even if they are syntactically valid integers.
	 */
	public File getFile(int index) {
		return files.get(keys.get(index));
	}

	/**
	 * Returns the file associated to the given array key. Return null
	 * if the key is not found. The returned file is always an absolute
	 * path.
	 * @param key Array key.
	 */
	public File getFile(String key) { return files.get(key); }

	/**
	 * Returns the number of elements in the array.
	 */
	public int size() { return keys.size(); }

	/**
	 * Adds a new file in the end of the array or removes the key if the file is null.
	 *
	 * @param key  The array key for the file
	 * @param file The file itself
	 * @return     The previous assignment for the key
	 */
	public File add(String key, File file) {
		if (file == null) {
			keys.remove(key);
			return files.remove(key);
		} else {
			File f = files.put(key, file);
			if (f != null) {
				// Avoid duplicates and make sure that the key becomes the last one
				keys.remove(key);
			}
			keys.add(key);
			return f;
		}
	}

	/** Removes the given key from the array. */
	public File remove(String key) {
		return add(key, null);
	}

	/**
	 * Writes the index to given file.
	 * 
	 * @param file Array index file.
	 */
	public void write(File file) throws IOException {
		CSVWriter out = new CSVWriter(new String[] {Port.ARRAY_COLUMN_KEY,
			                                        Port.ARRAY_COLUMN_FILE},
			                          file);
		for (String key : keys) {
			out.write(key, false);
			out.write(files.get(key).getPath(), false);
		}
		out.close();
	}

	/**
	 * Writes the index to given output array port.
	 * @param cf Current command file
	 * @param outPortName Name of the output array port.
	 */
	public void write(CommandFile cf, String outPortName) throws IOException {
	    write(cf.getOutputArrayIndex(outPortName));
	}
	
	/**
	 * Reads the index from given file.
	 * 
	 * @param file Array index file.
	 */
	public static IndexFile read(File file) throws IOException {
	    if (file == null) return new IndexFile();
	    
	    IndexFile index;
	    File      dir = file.getParentFile();
	    CSVParser in  = new CSVParser(file);
	    try {
	        int  keyCol = in.getColumnIndex(Port.ARRAY_COLUMN_KEY);
	        int fileCol = in.getColumnIndex(Port.ARRAY_COLUMN_FILE);
	        index = new IndexFile();

	        for (String[] row : in) {
	            File elementFile = new File(row[fileCol]);
	            if (!elementFile.isAbsolute()) {
	                elementFile = new File(dir, elementFile.getPath());
	            }
	            index.add(row[keyCol], elementFile);
	        }
	    } finally {
	        in.close();
	    }
	    return index;
	}

}
