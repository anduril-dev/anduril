package org.anduril.component;

import org.anduril.core.network.DataType;
import org.anduril.core.network.DataTypeFunctionality;

import java.io.File;
import java.io.IOException;

public class DocumentFolderFunctionality extends DataTypeFunctionality {

    public DocumentFolderFunctionality(DataType type) {
        super(type);
    }

    public boolean compare(File file1, File file2, StringBuffer difference)
            throws IOException {
        boolean isOK = true;
        if (file1.isFile()) {
            difference.append(file1.getAbsolutePath())
                      .append(" is not a folder and thus it makes an invalid reference output.\n");
        } else
        if (file2.isFile()) {
            difference.append(file2.getAbsolutePath())
                      .append(" is not a folder.\n");
        } else {
            for (File f : file1.listFiles()) {
            	String name = f.getName();
            	if (name.charAt(0) != '.')
                   isOK = isOK && super.compare(f, new File(file2, name), difference);
            }
        }
        return isOK;
    }

    @Override
    protected boolean compareFiles(File file1, File file2, StringBuffer difference)
            throws IOException {
    	boolean isEqual;
    	String name = file1.getName(); 
        if (name.endsWith(".tex") ||
        	name.endsWith(".html")) {
            isEqual = TextFileTypeFunctionality.compareTextFiles(file1, file2, difference);
        } else if (name.endsWith(".png")) {
        	isEqual = ImageListTypeFunctionality.compareImageFiles(file1, file2, difference);
        } else {
        	isEqual = super.compareFiles(file1, file2, difference);
        }
        if (!isEqual) difference.append("\n  Content mismatch in ")
                                .append(file1.getName())
                                .append('.');
        return isEqual;
    }    

}
