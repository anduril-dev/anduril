package org.anduril.component;

import org.anduril.asser.io.CSVParser;
import org.anduril.core.network.DataType;
import org.anduril.core.network.DataTypeFunctionality;
import org.anduril.core.network.PresentationType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CSVTypeFunctionality extends DataTypeFunctionality {
    public static final double EPSILON = 1e-5;
    public static final String NEWLINE = "\n";

    public CSVTypeFunctionality(DataType type) {
        super(type);
    }
    
    @Override
    public boolean compare(File file1, File file2, StringBuffer difference) throws IOException {
    	if (getType().getPresentationType() == PresentationType.MULTIFILE) {
    		File[][] fileMap = Tools.mapMultifilePaths(file1, file2);
    		for (File[] files: fileMap) {
    			final boolean ok;
    			if (fileMap.length == 1 || files[0].getName().endsWith(".csv")) {
    				ok = compareCSV(files[0], files[1], difference);
    			} else {
    				ok = compareFiles(files[0], files[1], difference);
    			}
    			if (!ok) return false;
    		}
    		return true;
    	} else {
    		return compareCSV(file1, file2, difference);
    	}
    }
    
    private boolean compareCSV(File file1, File file2, StringBuffer difference)
            throws IOException {
        if (file1.length() == 0 && file2.length() == 0) return true;
        
        CSVParser reader1 = new CSVParser(file1);
        CSVParser reader2 = new CSVParser(file2);
        try {
        	final String[] header1 = reader1.getColumnNames();
        	final String[] header2 = reader2.getColumnNames();
            final int size1 = header1.length;
            final int size2 = header2.length;
            if (size1 != size2) {
                String msg = String.format("The files have different number of columns: %d vs. %d",
                        size1, size2);
                difference.append(msg);
                return false;
            }
            
            if (!Arrays.equals(header1, header2)) {
                String msg = "The column names are different";
                difference.append(msg);
                return false;
            }
            
            int rowNum = 0;
            while (reader1.hasNext() || reader2.hasNext()) {
                if (!reader1.hasNext() || !reader2.hasNext()) {
                    String msg = "The files have different number of rows";
                    difference.append(msg);
                    return false;
                }
                String[] row1 = reader1.next();
                String[] row2 = reader2.next();
                rowNum++;
                
                if (row1.length != row2.length) {
                    String msg = String.format(
                            "Rows %d have different number of columns: %d vs %d",
                            rowNum, row1.length, row2.length);
                        difference.append(msg);
                    return false;
                }
                
                for (int colNum=0; colNum<row1.length; colNum++) {
                    String value1 = row1[colNum];
                    String value2 = row2[colNum];

                    /* Enabling matching "NA" in regexp */
                    if (value1 == null) value1 = reader1.getNASymbol();
                    if (value2 == null) value2 = reader2.getNASymbol();

                    try {
                    	double numeric1 = Double.parseDouble(value1);
                    	double numeric2 = Double.parseDouble(value2);
                    	if (Math.abs(numeric1-numeric2) > EPSILON) {
                    		String msg = String.format(
                    				"Numerical values differ at row %d, column %d: %s vs. %s",
                    				rowNum, colNum+1, numeric1, numeric2);
                    		difference.append(msg);
                    		return false;
                    	}
                    } catch (NumberFormatException e) {
                    	final boolean equal = DataTypeFunctionality.compareRegexp(value1, value2);
                    	if (!equal) {
                    		String msg = String.format(
                    				"Values differ at row %d, column %d: %s vs. %s",
                    				rowNum, colNum+1, value1, value2);
                    		difference.append(msg);
                    		return false;
                    	}
                    }
                }
            }
        } finally {
            reader1.close();
            reader2.close();
        }
        return true;
    }

    public static int rowCount(File input) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(input));
        String line = reader.readLine();
        int lines = 0;
        while (line != null) {
            lines++;
            line = reader.readLine();
        }
        reader.close();
        return lines;
    }

    public static String joinStrings(List<String> tokens,
                                     int          maxLines,
                                     String       linePrefix) { // NOPMD 
        if (linePrefix == null) linePrefix = "";
        StringBuilder sb = new StringBuilder();
        int lineNumber = 0;
        int curLength = 0;
        
        for (String str: tokens) {
            if (curLength + str.length() < 77) {
                if (curLength > 0) {
                    sb.append(' ');
                    curLength++;
                }
                sb.append(str);
                curLength += str.length();
            } else {
                lineNumber++;
                curLength = 0;
                if (lineNumber >= maxLines) {
                    sb.append(" ...");
                    break;
                } else {
                    sb.append(NEWLINE);
                    sb.append(linePrefix);
                    curLength += linePrefix.length();
                }
            }
        }
        return sb.toString();
    }
}
