package org.anduril.component;

import org.anduril.core.network.DataType;
import org.anduril.core.network.PresentationType;
import org.anduril.core.utils.BaseFilenameFilter;
import org.anduril.core.utils.IOTools;

import java.io.*;

/**
 * Miscellaneous tools for writing components.
 */
public class Tools {
    
    /**
     * Read a file into string.
     * @param input Input file.
     */
    public static String readFile(File input) throws FileNotFoundException, IOException {
        char[] buffer = new char[1024*64];
        FileReader reader = new FileReader(input);
        StringBuilder sb = new StringBuilder(1024*8);
        
        try {
            while (true) {
                int count = reader.read(buffer);
                if (count == -1) break;
                else sb.append(buffer, 0, count);
            }
        } finally {
            reader.close();
        }
        
        return sb.toString();
    }
    
    /**
     * Write a string into a file.
     */
    public static void writeString(File output, String s) throws IOException {
        FileWriter writer = new FileWriter(output);
        try {
            writer.write(s);
        } finally {
            writer.close();
        }
    }

    /**
     * Copy contents of a stream to a file.
     * @param in Input stream.
     * @param out Output file.
     */
    public static void copyStreamToFile(InputStream in, File out)
            throws FileNotFoundException, IOException {
    	copyStreamToFile(in, out, false);
    }

    /**
     * Copy contents of a stream to a file.
     * @param in Input stream.
     * @param out Output file.
     * @param append If true, then the content of the input stream will be written
     *               to the end of the output file rather than the beginning
     */
    public static void copyStreamToFile(InputStream in, File out, boolean append)
            throws FileNotFoundException, IOException {
        final int BUFFER_SIZE = 16*1024;
        byte[] buffer = new byte[BUFFER_SIZE];

        FileOutputStream outStream = new FileOutputStream(out, append);
        try {
            while(true) {
                int bytes = in.read(buffer);
                if (bytes == -1) break;
                outStream.write(buffer, 0, bytes);
            }
        } finally {
            outStream.close();
        }
    }

    /**
     * Copy contents of a file to another file. Both files must
     * be actual files, not directories.
     * @param in Input file.
     * @param out Output file.
     */
    public static void copyFile(File in, File out)
            throws FileNotFoundException, IOException {
        copyFile(in, out, false);
    }

    /**
     * Copy contents of a file to another file. Both files must
     * be actual files, not directories.
     * @param in Input file.
     * @param out Output file.
     * @param append If true, then the content of the input file will be written
     *               to the end of the output file rather than the beginning
     */
    public static void copyFile(File in, File out, boolean append)
            throws FileNotFoundException, IOException {
        FileInputStream inStream = new FileInputStream(in);
        try {
            copyStreamToFile(inStream, out, append);
        } finally {
            inStream.close();
        }
    }

    /**
     * Recursively copy a directory to another directory.
     * All files and subdirectories are copied.
     * @param in Input directory.
     * @param out Output directory. If this doesn't exist,
     * 	it is created.
     * @param overwrite If true, allow overwriting existing
     * 	destination files. If false, throw IOException when
     * 	the destination exists.
     */
    public static void copyFileToDirectory(File in, File out, boolean overwrite)
        throws FileNotFoundException, IOException {
        if (in == null) throw new NullPointerException("in is null");
        if (out == null) throw new NullPointerException("out is null");
        if (in.isDirectory()) throw new IllegalArgumentException("in is a directory");
        
        if (!out.exists()) out.mkdirs();
        
        File dest = new File(out, in.getName());
        if (in.isFile()) {
            if (!overwrite && dest.exists()) {
                String msg = String.format("Can't copy %s: destination %s exists",
                        in.getPath(), dest.getPath());
                throw new IOException(msg);
            }
            copyFile(in, dest);
        }
        else {
            throw new IOException("Can't copy file: "+in);
        }
    }
    
    /**
     * Recursively copy a directory to another directory.
     * All files and subdirectories are copied.
     * @param in Input directory.
     * @param out Output directory. If this doesn't exist,
     * 	it is created.
     * @param overwrite If true, allow overwriting existing
     * 	destination files. If false, throw IOException when
     * 	the destination exists.
     * @param excludePattern Files and directories matching
     * 	this regexp are not copied. Matching is done for the
     * 	base name of the filename, i.e. the last component.
     */
    public static void copyDirectory(File in, File out, boolean overwrite, String excludePattern)
        throws FileNotFoundException, IOException {
        if (in == null) throw new NullPointerException("in is null");
        if (out == null) throw new NullPointerException("out is null");
        if (!in.isDirectory()) throw new IllegalArgumentException("in is not a directory");
        
        if (excludePattern != null && in.getName().matches(excludePattern)) return;
        
        if (!out.exists()) out.mkdirs();
        for (File f: in.listFiles()) {
            if (excludePattern != null && f.getName().matches(excludePattern)) continue;
            
            File dest = new File(out, f.getName());
            if (f.isFile()) {
                if (!overwrite && dest.exists()) {
                    String msg = String.format("Can't copy %s: destination %s exists",
                            f.getPath(), dest.getPath());
                    throw new IOException(msg);
                }
                copyFile(f, dest);
            }
            else if (f.isDirectory()) copyDirectory(f, dest, overwrite, excludePattern);
            else {
                throw new IOException("Can't copy file: "+f);
            }
        }
    }
    
    /**
     * Recursively copy a directory to another directory.
     * All files and subdirectories are copied.
     * @param in Input directory.
     * @param out Output directory. If this doesn't exist,
     * 	it is created.
     * @param overwrite If true, allow overwriting existing
     * 	destination files. If false, throw IOException when
     * 	the destination exists.
     */
    public static void copyDirectory(File in, File out, boolean overwrite)
        throws FileNotFoundException, IOException {
        copyDirectory(in, out, overwrite, null);
    }	
    
    /**
     * Recursively copy a directory to another directory, overwriting
     * existing files.
     * @param in Input directory.
     * @param out Output directory. If this doesn't exist,
     * 	it is created.
     */
    public static void copyDirectory(File in, File out) throws FileNotFoundException, IOException {
        copyDirectory(in, out, true);
    }

    /**
     * Recursive deletion of files and directories
     * @param in File or directory to be deleted.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void removeFile(File in)
            throws FileNotFoundException, IOException {
        if (in == null) throw new NullPointerException("in is null");
        if (in.isDirectory()) {
            for (File f: in.listFiles()) {
                if (f.isDirectory()) {
                	removeFile(f);
                	f.delete();
                }
                else {
                	f.delete();
                }
            }
            in.delete();
        } else {
            in.delete();
        }
    }
    
    /**
     * Create a symbolic link.
     * @param target Target file/directory.
     * @param newlink Name of the symbolic link.
     */
    public static void createSymbolicLink(String target, String newlink) 
        throws IOException,InterruptedException {

        Process process = Runtime.getRuntime().exec( 
                new String[] { "ln", "-s", target, newlink }
        );
        process.waitFor();
        process.destroy();

    }
    
    /**
     * Return a list of all files that correspond to a multifile.
     * @param primaryFile Path of the primary file.
     */
    public static File[] getMultifilePaths(File primaryFile) {
    	String basename = IOTools.removeFileExtension(primaryFile.getName());
    	FilenameFilter filter = new BaseFilenameFilter(basename);
    	return primaryFile.getParentFile().listFiles(filter); 
    }
    
    /**
     * Map file paths from a source multifile to a target multifile.
     * For each source file, there is a corresponding target file with
     * the same extension.
     * 
     * @param primaryFrom Path of the primary source file.
     * @param primaryTo Path of the primary target file.
     * @return A File[N][2] array, where N is the number of source
     * 	files. The element result[i][0] is the i'th source file and
     * 	result[i][1] is the corresponding target file.  
     */
    public static File[][] mapMultifilePaths(File primaryFrom, File primaryTo) {
    	File[] fromFiles = getMultifilePaths(primaryFrom);
    	File[][] result = new File[fromFiles.length][2];
    	
    	for (int i=0; i<fromFiles.length; i++) {
    		result[i][0] = fromFiles[i];
    		if (i == 0 && fromFiles.length == 1) {
    			result[i][1] = primaryTo;
    		} else {
    			String ext = Tools.getFileExtension(fromFiles[i]);
    			result[i][1] = new File(primaryTo.getParentFile(),
    					IOTools.removeFileExtension(primaryTo.getName()) + ext);
    		}
    	}
    	
    	return result;
    }
    
    /**
     * Return the file extension of given file, including the
     * leading dot. If the file has no extension, the empty string
     * is returned.
     */
    public static String getFileExtension(File file) {
    	String filename = file.getName();
    	int pos = filename.indexOf('.');
    	if (pos == 0) pos = filename.indexOf('.', 1);
    	if (pos < 0) return "";
    	return filename.substring(pos);
    }
    
    /**
     * Return true if the file is the primary file for the
     * given data type. If the data type is not a multifile,
     * return true always. Otherwise, return true if the
     * extension of the file matches the extension of the
     * data type.
     */
    public static boolean isPrimaryFile(File file, DataType type) {
    	if (type.getPresentationType() != PresentationType.MULTIFILE) {
    		return true;
    	}
    	
    	String ext = getFileExtension(file);
    	if (ext.startsWith(".")) {
    		ext = ext.substring(1);
    	}
    	String typeExt = type.getExtension(false);
    	
    	if (typeExt == null) {
    		return ext.equals("");
    	} else {
    		return ext.equals(typeExt);
    	}
    }
}
