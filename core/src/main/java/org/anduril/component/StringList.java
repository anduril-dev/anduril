package org.anduril.component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Read and write support for the built-in StringList
 * data type. StringList contains strings that don't
 * have newlines. It is a text file with one entry per line.
 */
public class StringList {
    /**
     * Read a StringList file.
     * @param input Input file.
     * @return List of strings. They contain no newlines and
     * 	also no leading or trailing whitespace.
     */
    public static List<String> read(File input) throws FileNotFoundException, IOException {
        List<String> lines = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(input));
        String line;
        while ((line = reader.readLine()) != null) {
            line = line.trim();
            if (!line.isEmpty()) lines.add(line);
        }
        reader.close();
        return lines;
    }
    
    /**
     * Write a StringList file.
     * @param strings Input strings. They must not contain newlines, except optionally
     * 	at the end of the string.
     * @param output Output file.
     */
    public static void write(List<String> strings, File output) throws IOException {
        FileWriter writer = new FileWriter(output);
        for (String str: strings) { 
            writer.write(str);
            if (!str.endsWith("\n")) writer.append('\n');
        }
        writer.close();
    }
}
