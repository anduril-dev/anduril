package org.anduril.component;

import org.anduril.core.engine.CommandFileWriter;
import org.anduril.core.network.Component;
import org.anduril.core.network.Port;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Command file parser and interface. After reading the command
 * file, the class can be used to write error and log messages
 * as well as access inputs, outputs and parameters. 
 */
public class CommandFile {

    /** Prefix for metadata record. **/
    static public final String METADATA = "metadata";
    
    /** Name of the pipeline component instance. **/
    static public final String METADATA_INSTANCE_NAME = "instanceName";

    /** Name of the pipeline host engine. **/
    static public final String METADATA_ENGINE = "engine";
    
    /** Filename of the pipeline. **/
    static public final String METADATA_SOURCE_FILE = "file";
    
    /** Absolute filename of the pipeline. **/
    static public final String METADATA_SOURCE_PATH = "path";
    
    /** Line and column location within source pipeline. **/
    static public final String METADATA_SOURCE_LOCATION = "sourceLocation";
    
    /** Line and column location within source pipeline. **/
    static public final String METADATA_SOURCE_LINE = "line";
    
    /** Line and column location within source pipeline. **/
    static public final String METADATA_SOURCE_COLUMN = "column";

    /** Name of the component underlying a component instance. **/
    static public final String METADATA_COMPONENT_NAME = "componentName";

    /** Absolute path to the component folder. **/
    static public final String METADATA_COMPONENT_PATH = "componentPath";

    /** Name of the component bundle. **/
    static public final String METADATA_BUNDLE_NAME = "bundleName";

    /** Absolute path to the bundle folder. **/
    static public final String METADATA_BUNDLE_PATH = "bundlePath";

    /** $ANDURIL_HOME. **/
    static public final String METADATA_ANDURIL_HOME = "andurilHome";

    private Map<String,File> inputs;
    private Map<String,File> outputs;
    private Map<String,String> params;
    private Map<String,String> metadata;
    private Writer errorsWriter;
    private Writer logWriter;
    
    public CommandFile() {
        this.inputs = new HashMap<String,File>();
        this.outputs = new HashMap<String,File>();
        this.params = new HashMap<String,String>();
        this.metadata = new HashMap<String,String>();
    }
    
    /**
     * If error or log streams are open, close them.
     */
    public void close() throws IOException {
        if (errorsWriter != null) errorsWriter.close();
        if (logWriter != null) logWriter.close();
    }
    
    /**
     * Return the filename of given input port. For array data types,
     * this returns the directory that contains the array. To get
     * the array index file, use {@link #getInputArrayIndex(String)}.
     * @param name Name of input port.
     * @return The filename, or null if the port doesn't exist
     * 	or is not connected.
     */
    public File getInput(String name) {
        return inputs.get(name);
    }
    
    /**
     * Set the filename of input port.
     * @param name Name of input port.
     * @param value Filename of the port.
     */
    public void setInput(String name, File value) {
        inputs.put(name, value);
    }
    
    /**
     * Read contents of given input file as a string. 
     * @param name Name of input port.
     * @throws IOException If input file can't be read.
     * @throws FileNotFoundException If input file doesn't exist.
     */
    public String readInput(String name) throws IOException, FileNotFoundException {
        return Tools.readFile(getInput(name));
    }
    
    /**
     * Return true if the input port exists, it is connected
     * and the input file exists.
     * @param name Name of input port.
     */
    public boolean inputDefined(String name) {
        File file = inputs.get(name); 
        return file != null && !file.toString().isEmpty() && file.exists();
    }
    
    /**
     * Returns the filename of given array input port. Returns null
     * if the port is not connected or is not an array.
     * @param arrayPortName Name of input array port. This is the
     *  regular, user-visible port name and not the pseudo-port that
     *  refers to the index.
     */
    public File getInputArrayIndex(String arrayPortName) {
        String indexPort = Port.ARRAY_INDEX_PORT_PREFIX + arrayPortName;
        return this.inputs.get(indexPort);
    }
    
    /**
     * Read the index of an input array.
     * @param arrayPortName Name of input array port. This is the
     *  regular, user-visible port name and not the pseudo-port that
     *  refers to the index.
     * @throws IOException If I/O error occurs while reading index.
     */
    public IndexFile readInputArrayIndex(String arrayPortName) throws IOException {
        if (!this.inputs.containsKey(arrayPortName)) {
            throw new IllegalArgumentException("Input port is not defined or contents are missing: "+arrayPortName);
        }
        return IndexFile.read(getInputArrayIndex(arrayPortName));
    }

    /**
     * Return the filename of given output port. For array data types,
     * this returns the directory that contains the array. To get
     * the array index file, use {@link #getOutputArrayIndex(String)}.
     * @param name Name of output port.
     * @return The filename, or null if the port doesn't exist
     * 	or is not connected.
     */
    public File getOutput(String name) {
    	if (name.equals(Component.TEMP_DIRECTORY_PORT_NAME)) {
    		File tempDir = outputs.get(name);
    		if (!tempDir.exists()) tempDir.mkdir();
    		return tempDir;
    	}
        return outputs.get(name);
    }

    /**
     * Set the filename of output port.
     * @param name Name of output port.
     * @param value Filename of the port.
     */
    public void setOutput(String name, File value) {
        outputs.put(name, value);
    }
    
    /**
     * Returns the filename of given array output port. Returns null
     * if the port is not an array. Tip: It is convenient to use
     * {@link IndexFile#write(CommandFile, String)} to write the index.
     * 
     * @param arrayPortName Name of output array port. This is the
     *  regular, user-visible port name and not the pseudo-port that
     *  refers to the index.
     */
    public File getOutputArrayIndex(String arrayPortName) {
        String indexPort = Port.ARRAY_INDEX_PORT_PREFIX + arrayPortName;
        return this.outputs.get(indexPort);
    }
    
    /**
     * Return the value of given parameter.
     * @param name Name of parameter.
     * @return The value, or null if the parameter doesn't exists
     * 	or has no value.
     */
    public String getParameter(String name) {
        return params.get(name);
    }
    
    /**
     * Return the value of given parameter as an integer.
     * @param name Name of parameter.
     * @throws IllegalArgumentException If the parameter
     *  has no value.
     * @throws NumberFormatException If the value is not
     *  a legal integer.
     */
    public boolean getBooleanParameter(String name) {
        final String value = params.get(name);
        if (value == null) {
            throw new IllegalArgumentException("Parameter does not have value: "+name);
        }
        if (value.equals("true")) return true;
        if (value.equals("false")) return false;
        else {
            String msg = String.format("Parameter %s is not a valid Boolean: %s",
                    name, value);
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Return the value of given parameter as a double.
     * @param name Name of parameter.
     * @throws IllegalArgumentException If the parameter
     *  has no value.
     * @throws NumberFormatException If the value is not
     *  a legal double.
     */
    public double getDoubleParameter(String name) {
        final String value = params.get(name);
        if (value == null) {
            throw new IllegalArgumentException("Parameter does not have value: "+name);
        }
        return Double.parseDouble(value);
    }

    /**
     * Return the value of given parameter as an integer.
     * @param name Name of parameter.
     * @throws IllegalArgumentException If the parameter
     *  has no value.
     * @throws NumberFormatException If the value is not
     *  a legal integer.
     */
    public int getIntParameter(String name) {
        final String value = params.get(name);
        if (value == null) {
            throw new IllegalArgumentException("Parameter does not have value: "+name);
        }
        return Integer.parseInt(value);
    }

    /**
     * Set the value of given parameter.
     * @param name Name of parameter.
     * @param value Value of parameter.
     */
    public void setParameter(String name, String value) {
        params.put(name, value);
    }
    
    /**
     * Return a metadata value. This is the contents
     * of the metadata.NAME line, where NAME is the
     * metadata name given.
     */
    public String getMetadata(String name) {
        return metadata.get(name);
    }
    
    /**
     * Set a metadata value.
     * @param name Name of metadata item.
     * @param value Value of metadata item.
     */
    public void setMetadata(String name, String value) {
        metadata.put(name, value);
    }
    
    /**
     * Return the name of the error file.
     */
    public File getErrorsFile() {
        return getOutput(CommandFileWriter.OUTPUT_ERRORS);
    }

    /**
     * Return the name of the log file.
     */
    public File getLogFile() {
        return getOutput(CommandFileWriter.OUTPUT_LOG);
    }

    /**
     * Returns a folder that may be used for temporary intermediates
     * of the component instance. This folder is automatically created
     * by this method and it is removed by the hosting engine after the
     * execution of the instance.
     */
    public File getTempDir() {
    	File tempDir = getOutput(CommandFileWriter.TEMP_DIRECTORY);
    	tempDir.mkdir();
    	return tempDir;
    }
    
    /**
     * Return a writer for the error stream. There same writer
     * is returned if the method is called several times.
     */
    public Writer getErrorsWriter() throws IOException {
        if (errorsWriter == null) {
            errorsWriter = new FileWriter(getErrorsFile());
        }
        return errorsWriter;
    }

    /**
     * Return a writer for the log stream. There same writer
     * is returned if the method is called several times.
     */
    public Writer getLogWriter() throws IOException {
        if (logWriter == null) {
            logWriter = new FileWriter(getLogFile());
        }
        return logWriter;
    }
    
    /**
     * Write a message to error stream.
     * @param msg Error message. It may contain newlines.
     */
    public void writeError(String msg) {
        try {
            Writer writer = getErrorsWriter();
            writer.write(msg);
            if (!msg.endsWith("\n")) writer.write("\n");
            writer.write("\n");
            writer.flush();
        } catch(IOException e) {
            System.exit(ErrorCode.INVALID_ERRORSTREAM.getCode());
        }
    }

    /**
     * Write stack trace of given exception to error stream.
     */
    public void writeError(Throwable exception) {
        try {
            PrintWriter w = new PrintWriter(getErrorsWriter());
            exception.printStackTrace(w);
            getErrorsWriter().write("\n");
            w.flush();
        } catch(IOException e) {
            writeError(e.toString());				
        }
    }

    /**
     * Write a message to log stream.
     * @param msg Log message. It may contain newlines.
     */
    public void writeLog(String msg) throws IOException {
        Writer writer = getLogWriter();
        writer.write(msg);
        if (!msg.endsWith("\n")) writer.write("\n");
        writer.write("\n");
        writer.flush();
    }
    
    /**
     * Low level parser that read a command file and return key, value
     * pairs.
     * @param input Filename of command file.
     * @throws FileNotFoundException If command file is not found.
     * @throws IOException If command file can't be read.
     * @throws IllegalArgumentException If command file contains errors.
     */
    public static Map<String,String> readCommandFile(File input) throws FileNotFoundException, IOException {
        Map<String,String> map = new HashMap<String,String>();

        // Need to use Properties because multiple line values are stored with Properties.store
    	Properties prop = new Properties();
    	prop.load(new FileInputStream(input));
    	Enumeration<?> e = prop.propertyNames();
    	while (e.hasMoreElements()) {
    		String key = (String) e.nextElement();
    		map.put(key, prop.getProperty(key));
    	}
        return map;
    }
    
    /**
     * Create a CommandFile instance by parsing a command file.
     * @param input Filename of command file.
     * @throws FileNotFoundException If command file is not found.
     * @throws IOException If command file can't be read.
     * @throws IllegalArgumentException If command file contains errors.
     */
    public static CommandFile fromFile(File input) throws FileNotFoundException, IOException {
        CommandFile cf = new CommandFile();
        Map<String,String> map = readCommandFile(input);
        
        for (Map.Entry<String, String> entry: map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String[] tokens = key.split("[.]");
            if (tokens.length < 2) {
                throw new IllegalArgumentException("Invalid key: "+key);
            }
            String type = tokens[0];
            String name = tokens[1];
            
            if (type.equals("input")) {
                if (value.length() > 1)
                   cf.setInput(name, new File(value));
            }
            else if (type.equals("output")) {
                cf.setOutput(name, new File(value));
            }
            else if (type.equals("parameter")) {
                cf.setParameter(name, value);
            }
            else if (type.equals("metadata")) {
                cf.setMetadata(name, value);
            } else {
                throw new IllegalArgumentException("Invalid key type: "+type);
            }
        }
        
        return cf;
    }
}
