package org.anduril.core.commandline;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import org.anduril.core.network.Network;
import org.anduril.core.network.StaticError;
import org.anduril.core.writers.JSONSerializer;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class ParseWorkflowCommand extends BaseCommand {

    public static final String OPT_OUT_FILE = "out-file";
    public static final String OPT_RUN = "run";

    public ParseWorkflowCommand() {
        super("parse-workflow", "WORKFLOW.jar -o OUT-FILE [--run] [-d EXECUTION-DIRECTORY]");
    }

    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getExecuteDirOption(false));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_OUT_FILE)
                .hasArg()
                .withDescription("Output file name.")
                .isRequired(true)
                .create('o'));

        options.addOption(OptionBuilder
            .withLongOpt(OPT_RUN)
            .withDescription("Allow executing (changed) dynamic parts of the workflow. Warning: this can modify the execution directory.")
            .isRequired(false)
            .create());
    }

    @Override
    protected void executeImpl() throws IOException, StaticError {
        File networkFile = getArgumentFile(0);
        if (networkFile == null) {
            printHelp();
            throw new StaticError("Expected workflow file");
        }

        loadAllBundlesFromJAR();

        final boolean runDynamic = getCommandLine().hasOption(OPT_RUN);

        final File execRoot = getExecutionRoot();
        final Network network = getEvaluator().evaluate(networkFile, runDynamic);
        if (evaluator.hasEngineErrors()) return;

        Gson gson = new GsonBuilder().serializeNulls().create();
        JSONSerializer serializer = new JSONSerializer(this.repository, gson, false);

        final File outFile = new File(getCommandLine().getOptionValue(OPT_OUT_FILE));
        final Writer writer = new FileWriter(outFile);
        final JsonWriter jsonWriter = new JsonWriter(writer);
        jsonWriter.setSerializeNulls(true);
        jsonWriter.setIndent("\t");
        gson.toJson(serializer.serializeNetwork(network, execRoot), jsonWriter);
        writer.close();
    }

    @Override
    protected String getHelpContents() {
        return "Write information on a workflow (without executing it) in JSON format.";
    }

}