package org.anduril.core.commandline;

import org.anduril.component.Tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Main class for the command line interface. CLI has a repository
 * of commands and delegates execution to the command that matches
 * the first command line argument. Each command has its own
 * command line syntax and is responsible for parsing arguments
 * and implementing the command actions. The CLI can be extended
 * by writing new command classes and adding the them to
 * {@link #buildCommands()}.
 * 
 * @see BaseCommand
 */
public abstract class CLI {

    public static final String ANDURIL_EXEC = "anduril";
    
    /**
     * Return the repository of commands.
     */
    public static Map<String,BaseCommand> buildCommands() {
        Map<String,BaseCommand> commands = new TreeMap<String,BaseCommand>();
        
        BaseCommand command; 

        command = new DefaultCommand();
        commands.put(command.getName(), command);
        
        command = new BuildDocCommand();
        commands.put(command.getName(), command);

        command = new CleanCommand();
        commands.put(command.getName(), command);

        command = new CompileBundleCommand();
        commands.put(command.getName(), command);

        command = new CompileWorkflowCommand();
        commands.put(command.getName(), command);

        command = new GenerateSBTCommand();
        commands.put(command.getName(), command);

        command = new ParseBundleCommand();
        commands.put(command.getName(), command);

        command = new ParseWorkflowCommand();
        commands.put(command.getName(), command);

        command = new RunTestComponentCommand();
        commands.put(command.getName(), command);

        command = new RunTestWorkflowCommand();
        commands.put(command.getName(), command);

        command = new RunWorkflowCommand();
        commands.put(command.getName(), command);

        return commands;
    }
    
    /**
     * Expand "-a" arguments. Each "-a FILENAME" is expanded into a
     * number of arguments, which are read from FILENAME.
     * Arguments quoted with ' or " inside the file are supported.
     * It is not currently possible to use escaped quote characters:
     * "xyz\"" is not the same as 'xyz"'.
     * @param args Command line arguments.
     */
    public static String[] expandArgs(String[] args) throws FileNotFoundException, IOException {
        ArrayList<String> argList = new ArrayList<String>();
        for (int i=0; i<args.length; i++) {
            if (i<args.length-1 && args[i].equals("-a") && !args[i+1].startsWith("-")) {
                File input = new File(args[i+1]);
                String contents = Tools.readFile(input);
                String prevEscape = null; /* One of null, ', " */
                for (String s: contents.split("\\s")) {
                    if (s.isEmpty()) continue;
                    if (prevEscape != null && s.endsWith(prevEscape)) {
                        String combined = argList.get(argList.size() - 1) + " " + s;
                        argList.set(argList.size() - 1, combined);
                        prevEscape = null;
                    }
                    else {
                        argList.add(s);
                        if      (s.charAt(0)=='\'') prevEscape = "'";
                        else if (s.charAt(0)=='\"') prevEscape = "\"";
                    }
                }
                i++;
            }
            else if (!args[i].isEmpty()) {
                argList.add(args[i]);
            }
        }
        
        return argList.toArray(new String[argList.size()]);
    }
    
    /**
     * Main method for the command line interface.
     * @param args Command line arguments.
     */
    public static void main(String[] args) throws IOException {

        if (args.length > 0) args = expandArgs(args);
        BaseCommand command;
        
        if (args.length == 0 || args[0].startsWith("-")) {
            command = new DefaultCommand();
        }
        else {
            Map<String,BaseCommand> commands = CLI.buildCommands();
            String name = args[0];
            command = commands.get(name);
            if (command == null) {
                System.out.println("Unknown command: "+name);
                command = new DefaultCommand();
                command.execute(new String[] {"--help"});
                System.exit(1);
            }	
        }
        
        boolean ok = command.execute(args);
        if (!ok) System.exit(1);
    }
}