package org.anduril.core.commandline;

import org.anduril.component.Tools;
import org.anduril.core.engine.TestCaseChecker;
import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.readers.PropertiesReader;
import org.anduril.core.readers.networkParser.NetworkHandler;
import org.anduril.core.utils.IOTools;
import org.anduril.runtime.Port;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class RunTestComponentCommand extends BaseCommand {
    public static final String OPT_COMPONENT = "component";
    public static final String OPT_TEST_CASE = "test-case";

    public RunTestComponentCommand() {
        super("run-test-component", "-b BUNDLE -c COMPONENT-NAME -t TEST-CASE-NAME");
    }

    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(true));
        options.addOption(getExecuteDirOption(false));
        options.addOption(getJavaHeapOption(false));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_COMPONENT)
                .hasArg()
                .withDescription("Execute only test cases for this component. Multiple -c can be given. "
                        +"If omitted, all components of selected bundles are included.")
                .isRequired(false)
                .create('c'));

        options.addOption(OptionBuilder
            .withLongOpt(OPT_TEST_CASE)
            .hasArg()
            .withDescription("Execute only these test cases. Multiple -t can be given. "
                    +"If omitted, all test cases of selected components are executed.")
            .isRequired(false)
            .create('t'));
    }

    @Override
    protected void executeImpl() throws IOException, StaticError {
        List<Bundle> explicitBundles = loadExplicitBundlesFromXML();

        final TestCaseChecker checker = new TestCaseChecker(getRepository(), getExecutionRoot(), getLogger());

        final List<String> explicitCompNames;
        if (getCommandLine().hasOption(OPT_COMPONENT)) {
            explicitCompNames = Arrays.asList(getCommandLine().getOptionValues(OPT_COMPONENT));
        } else {
            explicitCompNames = Collections.emptyList();
        }
        int nExplicitCompNames = explicitCompNames.size();
        List<String> compNames = new ArrayList<String>();
        
        if (explicitBundles.isEmpty()) {
            throw new StaticError("No bundles specified.");
        }

        for (Bundle bundle: explicitBundles) {
            ArrayList<Component> componentsAndFunctions = new ArrayList<Component>();
            componentsAndFunctions.addAll(bundle.getComponents(getRepository()));
            componentsAndFunctions.addAll(bundle.getFunctions(getRepository()));
            for (Component comp: componentsAndFunctions) {
                if (comp.getTestCases() != null && !comp.getTestCases().isEmpty()) {
                    boolean include = explicitCompNames.isEmpty() || explicitCompNames.contains(comp.getName());
                    if (include) {
                        compNames.add(comp.getNamespaceName());
                        nExplicitCompNames -= 1;
                    }
                }
                else {
                    checker.addTestlessComponent(comp);
                    getLogger().debug("No test cases found for " + comp.getName());
                }
            }
        }

        if (nExplicitCompNames > 0) {
            String s = (explicitCompNames.size() == 1 ? "" : "s");
            throw new StaticError("Unrecognized component"+ s + " or no testcases present in " + explicitCompNames);            
        }

        Set<String> enabledTestCases = null;
        if (getCommandLine().hasOption(OPT_TEST_CASE)) {
            enabledTestCases = new HashSet<String>();
            enabledTestCases.addAll(Arrays.asList(getCommandLine().getOptionValues(OPT_TEST_CASE)));
        }

        if (enabledTestCases != null && 
        	!enabledTestCases.isEmpty() && 
        	(compNames != null && compNames.size() > 1)) {
    		String msg = String.format("Exactly one component (-c) must be specified for test cases %s.", enabledTestCases);
            throw new StaticError(msg);
    	}

        Collections.sort(compNames);

        final Set<String> enabledTestCasesRef = new HashSet<String>();
        if (enabledTestCases != null && !enabledTestCases.isEmpty()) {
            enabledTestCasesRef.addAll(enabledTestCases);
        }

        final Set<String> failedLoadingComponents = new HashSet<>();
        for (String name: compNames) {
            try {
                addComponentTestCases(name, checker, enabledTestCases, enabledTestCasesRef);
            } catch (StaticError e) {
                e.report(getLogger());
                failedLoadingComponents.add(name);
            }
        }

        if (enabledTestCases != null && !enabledTestCases.isEmpty()) {
        	throw new StaticError("Unknown test cases: "+enabledTestCases);
        }

        try {
            Integer execExitCode = checker.executeTestCases();
            if (execExitCode.intValue() == TestCase.FAILED) {
                throw new StaticError("Test case failure");
            }
        } catch (IOException e) {
            throw new StaticError("Error running test cases: "+e.getMessage());
        } catch (ParserConfigurationException e) {
            throw new StaticError("Error running test cases: "+e.getMessage());
        } catch (SAXException e) {
            throw  new StaticError("Error running test cases: "+e.getMessage());
        } catch (InterruptedException e) {
            throw new StaticError("Error running test cases: "+e.getMessage());
		} catch (ExecutionException e) {
            throw new StaticError("Error running test cases: "+e.getMessage());
		}
        
        checker.printResultsToLog();

        if (!failedLoadingComponents.isEmpty()) {
            throw new StaticError("Failed to load components: "+failedLoadingComponents);
        }
    }

    private void addComponentTestCases(String name, TestCaseChecker checker, Set<String> enabledTestCases, Set<String> enabledTestCasesRef) throws StaticError {
        Component comp = getRepository().getComponent(name);
        if (comp == null) {

            comp = getRepository().getFunction(name);
            if (comp == null) {
                String msg = String.format("Component or function %s not found.", name);
                throw new StaticError(msg);
            }

            if (comp.getTestCaseDirectory() == null ||
                    comp.getTestCaseDirectory().listFiles() == null ||
                    comp.getTestCases() == null ||
                    comp.getTestCases().size() == 0) {
                checker.addTestlessComponent(comp);
                getLogger().info("No test cases found for " + comp.getName());
                return;
            }

            TreeMap<String,File> sortedFiles = new TreeMap<String,File>();
            for (File f: comp.getTestCaseDirectory().listFiles()) {
                sortedFiles.put(f.getAbsolutePath(), f);
            }

            for (File f : sortedFiles.values()) {
                if (f.getName().startsWith("."))
                    continue;

                if (enabledTestCasesRef != null    &&
                        !enabledTestCasesRef.isEmpty() &&
                        !enabledTestCasesRef.contains(f.getName()))
                    continue;

                Network network = new Network(getRepository());
                network.setDataDir(comp.getMainDirectory());
                NetworkHandler.setNetwork(network);
                java.util.Map<String,Object> functionArgs = new java.util.HashMap<String,Object>();
                populateFunctionInputs(comp, f, functionArgs);
                populateFunctionParameters(comp, f, functionArgs);
                String compClass = comp.getNamespaceName();
                try {
                    org.anduril.runtime.Component dynComp = org.anduril.core.readers.networkParser.Tools.invokeComponentByNameJava(compClass, functionArgs);
                    populateFunctionOutputs(comp, dynComp);
                }
                catch (Exception e) {
                    throw new StaticError(ExceptionUtils.getRootCauseMessage(e));
                }

                try {
                    ComponentTestCase funcTestCase =
                            new ComponentTestCase(f, repository, comp, network);

                    File propertiesFile = funcTestCase.getParameterFile();

                    if (propertiesFile != null) {
                        funcTestCase.setMetaParameters(PropertiesReader.readMetadata(network, propertiesFile));
                    }

                    if (enabledTestCases == null || enabledTestCases.contains(funcTestCase.getCaseName())) {
                        checker.addTestCase(funcTestCase);
                        if (enabledTestCases != null) enabledTestCases.remove(funcTestCase.getCaseName());
                    }
                } catch (IOException e) {
                    String msg = String.format("IO error: %s", e.getMessage());
                    throw new StaticError(msg);
                }
            }
        }
        else {
            for (TestCase testCase: comp.getTestCases()) {
                if (enabledTestCases == null || enabledTestCases.contains(testCase.getCaseName())) {
                    checker.addTestCase(testCase);
                    if (enabledTestCases != null) enabledTestCases.remove(testCase.getCaseName());
                }
            }

            if ((comp.getTestCases() == null) || (comp.getTestCases().size() == 0)) {
                checker.addTestlessComponent(comp);
            }
        }
    }

    private void populateFunctionInputs(Component comp, File testCaseFile, Map<String,Object> functionArgs) throws StaticError {
        File inputDir = new File(testCaseFile, ComponentTestCase.INPUT_DIRECTORY);
        if (!inputDir.exists()) {
            String msg = String.format("Input directory not found: %s", inputDir.getAbsoluteFile());
            throw new StaticError(msg);
        }

        for (File input: inputDir.listFiles()) {
            if (input.getName().startsWith(".")) continue;

            String inputName = IOTools.removeFileExtension(input.getName());
            InPort toPort = comp.getInPort(inputName);
            if (toPort == null) {
                String msg = String.format("Component %s, case %s: unknown input %s",
                        comp.getName(), testCaseFile.getName(), inputName);
                throw new StaticError(msg);
            }

            if (Tools.isPrimaryFile(input, toPort.getType())) {
                Port dynPort = org.anduril.core.readers.networkParser.Tools.invokeINPUT(input.getAbsolutePath());
                functionArgs.put(toPort.getName(), dynPort);
            }
        }
    }

	private void populateFunctionParameters(Component comp, File testCaseFile, Map<String,Object> functionArgs) throws StaticError {
        File propertiesFile = getFunctionParameterFile(testCaseFile);
        if (propertiesFile == null) return;

        Properties prop = new Properties();
        Reader reader;
        final SourceLocation location = new SourceLocation(propertiesFile);
        
        try {
            reader = new FileReader(propertiesFile);
        } catch (FileNotFoundException e) {
            throw new StaticError("Properties file not found", location);
        }
        
        try {
            prop.load(reader);
        } catch (IOException e) {
            throw new StaticError("Can't load properties", location);
        }

        for (Enumeration<?> enumer = prop.propertyNames(); enumer.hasMoreElements();) 
        {
            String key = (String)enumer.nextElement();
            String value = prop.getProperty(key);
            String[] tokens = key.split("[.]");
            
            if (tokens[0].equals(ComponentTestCase.META_PROPERTY_NAME)) continue;
            if (tokens.length == 2) {
            	Parameter param = comp.getParameter(tokens[1]);
            	if (param == null) {
            		throw new StaticError("Invalid parameter name: " + tokens[1], location);
            	}
                Object objValue;
                switch (param.getType()) {
                    case BOOLEAN:
                        objValue = Boolean.valueOf(value);
                        break;
                    case FLOAT:
                        objValue = Double.valueOf(value);
                        break;
                    case INT:
                        objValue = Integer.valueOf(value);
                        break;
                    case STRING:
                        objValue = value;
                        break;
                    default:
                        throw new RuntimeException("Unknown parameter type: "+param.getType());
                }
                functionArgs.put(param.getName(), objValue);
            } else {
                throw new StaticError("Invalid property key: " + key, location);
            }
        }
	}

    private void populateFunctionOutputs(Component comp, org.anduril.runtime.Component dynComp) {
        for (OutPort port: comp.getOutPorts()) {
            org.anduril.runtime.Port dynPort = org.anduril.core.readers.networkParser.Tools.getPortByName(dynComp, port.getName());
            org.anduril.runtime.Port dynOutput = org.anduril.core.readers.networkParser.Tools.invokeOUTPUT(dynPort);
            dynOutput.componentInstance().setOutPortAnnotation(Annotation.FILENAME, dynOutput.networkPort(), port.getName());
        }
    }

    /**
     * Get location of the parameter file for a function.
     */
	private File getFunctionParameterFile(File testCaseFile) {
        File propertiesFile = new File(testCaseFile, ComponentTestCase.FUNCTION_PROPERTIES_FILE);
        return propertiesFile.exists() ? propertiesFile : null;
    }

    protected String getHelpContents() {
        return "Run test cases for components. "
        + "Names of components are given as parameters, "
        + "or alternatively, all components that have "
        + "test cases are tested. The set of enabled test "
        + "cases can be fine tuned with --test-cases."
        ;
    }
}
