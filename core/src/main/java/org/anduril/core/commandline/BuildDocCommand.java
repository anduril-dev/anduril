package org.anduril.core.commandline;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.StaticError;
import org.anduril.core.writers.ComponentDocWriter;
import org.apache.commons.cli.Options;

import java.io.File;
import java.util.List;

public class BuildDocCommand extends BaseCommand {

    public BuildDocCommand() {
        super("build-doc", "destination-dir");
    }

    @Override
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(false));
    }

    @Override
    protected void executeImpl() throws StaticError {
    	List<Bundle> bundles = loadExplicitBundlesFromXML();

        File destinationDir = getArgumentFile(0);
        if (destinationDir == null) {
            printHelp();
            throw new StaticError("Expected destination directory");
        }
        
        try {
            new ComponentDocWriter(repository, bundles, destinationDir).write();
        } catch(Exception e) {
            throw new StaticError("Cannot write docs: ", e);
        }
    }
    
    @Override
    protected String getHelpContents() {
        return "(DEPRECATED) Build component interface and data type HTML docs.";
    }

}