package org.anduril.core.commandline;

import org.anduril.core.engine.ComponentExecutor;
import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.NetworkEvaluator;
import org.anduril.core.network.Bundle;
import org.anduril.core.network.Repository;
import org.anduril.core.network.StaticError;
import org.anduril.core.network.launcher.JavaLauncher;
import org.anduril.core.readers.networkParser.EvaluatorHandler;
import org.anduril.core.utils.IllegalAnnotationScopeException;
import org.anduril.core.utils.Logger;
import org.apache.commons.cli.*;

import java.io.*;
import java.util.*;

/**
 * The interface for command line interface (CLI) commands. Concrete
 * commands extend this class.
 * BaseCommand also contains helper methods for common actions that many
 * commands need to perform, such as loading components.
 * Apache CLI is used to parse command line arguments.
 * 
 * <p>
 * Actual commands must specify command line options
 * (in {@link #addOptions}) and implement the
 * method that performs the actions of the command
 * ({@link #executeImpl}).
 * </p>
 */
@SuppressWarnings("static-access")
public abstract class BaseCommand {
    public final static String OPT_BUNDLE = "bundle";
    public static final String OPT_CLASS_PATH = "cp";
    public final static String OPT_EXECUTION_DIR = "execution-dir";
    public final static String OPT_HELP = "help";
    public final static String OPT_JAVA_HEAP = "java-heap";
    public final static String OPT_THREADS = "threads";
    public final static char OPT_VERBOSE = 'v';

    public final static String HOME_ENVIRONMENT = "ANDURIL_HOME";
    public final static String BUNDLES_ENVIRONMENT = "ANDURIL_BUNDLES";
    public final static String DEFAULT_EXECUTE_DIR = "execute";

    private final String name;
    private final String usage;
    private final Options options;
    private CommandLine commandLine;
    private final Logger logger;
    
    protected Repository repository;
    protected NetworkEvaluator evaluator;

    public BaseCommand(String name, String usage) {
        this.name = name;
        this.usage = usage;
        this.options = new Options();
        this.logger = Logger.getLogger(this);

        /* Add common options */
        this.options.addOption(getHelpOption(false));
        this.options.addOption(getVerboseOption(false));

        /* Add command-specific options */
        addOptions(options);
    }
    
    /**
     * Return the name of the command. Name is used by CLI
     * to decide which command must be executed.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Return the usage message of the command.
     */
    public String getUsage() {
        return usage;
    }
    
    /**
     * Return the options instance that lists available
     * command line options.
     */
    public Options getOptions() {
        return options;
    }
    
    /**
     * Return the repository associated to the command.
     */
    public Repository getRepository() {
        return repository;
    }
    
    /**
     * Return the command line object that contains values
     * for options.
     * @throws NullPointerException If the object doesn't
     * 	yet exist. This happens when command line arguments
     * 	haven't been parsed.
     */
    public CommandLine getCommandLine() {
        if (commandLine == null) {
            throw new NullPointerException("No CommandLine instance: execute hasn't been called or had errors");
        }
        return commandLine;
    }
    
    public NetworkEvaluator getEvaluator() {
        return this.evaluator;
    }
    
    public Logger getLogger() {
        return this.logger;
    }
    
    /**
     * Parse command line arguments, execute the command and report
     * error messages.
     * @param args Command line arguments.
     * @return True if execution finished without errors.
     */
    public final boolean execute(String[] args) {
        final String help1 = "-" + OPT_HELP;
        final String help2 = "--" + OPT_HELP;
        for (String arg: args) {
            /* Manually search --help arguments because the parser
             * can not be constructed for some commands if mandatory
             * arguments are missing. */
            if (arg.equals(help1) || arg.equals(help2)) {
                printHelp();
                return true;
            }
        }
        
        try {
            this.commandLine = new PosixParser().parse(options, args);
        } catch(ParseException e) {
            System.out.println(e);
            System.out.println();
            printHelp();
            return false;
        }
        
        if (getCommandLine().hasOption(OPT_HELP)) {
            printHelp();
            return true;
        }

        String homeEnv = System.getenv(HOME_ENVIRONMENT);
        if (homeEnv == null) {
            getLogger().error("Engine home directory is not set.");
            getLogger().error("You must either give the --home argument or set "
                    + "the environment variable "+HOME_ENVIRONMENT);
            return false;
        }

        File homeDir = new File(homeEnv).getAbsoluteFile();
        if (!homeDir.exists()) {
            getLogger().error("Home directory does not exist: "+homeDir);
            return false;
        }
        
        try {
            this.repository = new Repository(homeDir, getLogger());
        } catch(IOException e) {
            getLogger().error("Invalid home directory: "+e);
            return false;
        }

        this.evaluator = new NetworkEvaluator(getExecutionRoot(), getRepository(), getLogger());
        EvaluatorHandler.setNetworkEvaluator(evaluator);

        if (getCommandLine().hasOption(OPT_JAVA_HEAP)) {
            int sizeMB = Integer.parseInt(getCommandLine().getOptionValue(OPT_JAVA_HEAP));
            JavaLauncher.setHeapSize(sizeMB);
        }
        if (getCommandLine().hasOption(OPT_VERBOSE)) {
            Logger.setVerbose(true);
        }
        if (getCommandLine().hasOption(OPT_THREADS)) {
            int threads = Integer.parseInt(getCommandLine().getOptionValue(OPT_THREADS));
            ComponentExecutor.getInstance().setMaxThreads(threads);
        }

        try {
            executeImpl();
            return true;
        } catch (IOException e) {
            getLogger().warning("I/O error: " + e);
        } catch (IllegalAnnotationScopeException aex) {
            String msg = String.format("In %s:(%s,%s), illegal annotation scope annotation %s.\n",
                    aex.getFile().getName(),
                    aex.getLocation().getLine(),
                    aex.getLocation().getColumn(),
                    aex.getAnnotationName());
            getLogger().warning(msg);
        } catch (StaticError e) {
            getLogger().warning(e.toString());
        } catch (DynamicError e) {
            getLogger().warning(e.toString());
        }

        return false;
    }

    /**
     * Do the actual work of the command.
     */
    protected abstract void executeImpl() throws IOException, StaticError, DynamicError;
    
    /**
     * Add command-specific options.
     * @param options The options instance where new options
     * 	are added.
     */
    protected abstract void addOptions(Options options);
    
    /**
     * Return the main contents of the command line help. This
     * tells what the command does when executed.
     */
    protected abstract String getHelpContents();
    
    /**
     * Print command usage message to stdout.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        String usage = getName();
        if (getUsage() != null && getUsage().length() > 0) {
            usage += " "+getUsage();
        }
        formatter.printHelp(usage, getHelpContents(), getOptions(), null);
    }
    
    /**
     * Print short command usage without options message to stdout.
     */
    public void printUsage() {
        HelpFormatter formatter = new HelpFormatter();
        String usage = getName();
        if (getUsage() != null && getUsage().length() > 0) {
            usage += " "+getUsage();
        }
        
        System.out.println(usage);
        System.out.flush();
        PrintWriter writer = new PrintWriter(System.out);
        formatter.printWrapped(writer, 79, getHelpContents());
        writer.flush();
    }
    
    // Option builders /////////////////////////////////////////////////

    protected Option getBundleOption(boolean required) {
        String desc =
            "Target bundle root directory (there can be several -b arguments).";
        return OptionBuilder
            .hasArgs()
            .withLongOpt(OPT_BUNDLE)
            .withDescription(desc)
            .isRequired(required)
            .create('b');
    }

    protected Option getClasspathOption(boolean required) {
        return OptionBuilder
                .withLongOpt(OPT_CLASS_PATH)
                .hasArg()
                .withDescription("Class path files.")
                .create();
    }

    protected Option getHelpOption(boolean required) {
        return OptionBuilder
            .withLongOpt(OPT_HELP)
            .withDescription("Print help")
            .isRequired(required)
            .create();
    }

    /**
     * Return representation for the option for setting
     * network execution directory.
     * @param required True if this option is required.
     */
    protected Option getExecuteDirOption(boolean required) {
        return OptionBuilder
            .hasArg()
            .withLongOpt(OPT_EXECUTION_DIR)
            .withDescription(String.format("Network execution directory (default: %s)", DEFAULT_EXECUTE_DIR))
            .isRequired(required)
            .create('d');
    }

    protected Option getJavaHeapOption(boolean required) {
        return OptionBuilder
            .hasArg()
            .withLongOpt(OPT_JAVA_HEAP)
            .withDescription("Heap size for Java components in MB (default: 200)")
            .isRequired(required)
            .create();
    }

    protected Option getThreadOption(boolean required) {
        String desc = String.format("Number of concurrent component threads (default: %s)",
                ComponentExecutor.DEFAULT_THREADS);
        return OptionBuilder
            .withLongOpt(OPT_THREADS)
            .hasArg()
            .withDescription(desc)
            .isRequired(required)
            .create();
    }

    protected Option getVerboseOption(boolean required) {
        return OptionBuilder
            .withDescription("Increase verbosity")
            .isRequired(required)
            .create(OPT_VERBOSE);
    }

    // Argument tools //////////////////////////////////////////////////
    
    public List<File> getExplicitBundles() throws StaticError {
        Set<File> files = new LinkedHashSet<File>();

        if (getCommandLine().hasOption(OPT_BUNDLE)) {
            for (String bundleName: getCommandLine().getOptionValues(OPT_BUNDLE)) {
                File bundleDir = getRepository().findBundle(bundleName);
                if (bundleDir == null) {
                    bundleDir = new File(bundleName);
                }
                if (bundleDir == null) {
                    throw new StaticError("Can not find bundle: "+bundleName);
                }
                try {
                    files.add(bundleDir.getCanonicalFile());
                } catch (IOException e) {
                    throw new StaticError("I/O error when accessing "+bundleDir);
                }
            }
        }

        return new ArrayList<File>(files);
    }

    public File getExecutionRoot() {
        return new File(getCommandLine().getOptionValue(OPT_EXECUTION_DIR, DEFAULT_EXECUTE_DIR)).getAbsoluteFile();
    }
    
    @SuppressWarnings("unchecked")
	public List<String> getCommandArgs() {
        List<String> li = (List<String>)getCommandLine().getArgList();
        return li.subList(1, li.size());
    }
    
    protected File getArgumentFile(int pos) {
        List<String> args = getCommandArgs();
        if (args.size() < pos+1) {
            return null;
        }
        return new File(args.get(pos));
    }

    // Actions tools ///////////////////////////////////////////////////

    public List<Bundle> loadExplicitBundlesFromXML() throws StaticError {
        ArrayList<Bundle> explicitBundles = new ArrayList<Bundle>();
        for (File bundleDir: getExplicitBundles()) {
            Bundle bundle = this.repository.loadBundleFromXML(bundleDir);
            explicitBundles.add(bundle);
        }
        return explicitBundles;
    }

    public void loadAllBundlesFromJAR() throws StaticError {
        HashSet<File> bundles = new HashSet<File>(this.repository.findAllBundles());
        for (File bundleDir: bundles) {
            Bundle tempBundle = new Bundle(bundleDir, bundleDir.getName());
            if (tempBundle.isCompiled()) {
                this.repository.loadBundleFromJAR(bundleDir);
            } else {
                String msg = String.format("Bundle is not compiled: %s", bundleDir);
                getLogger().warning(msg, Logger.tagWarningBundleNotCompiled());
            }
        }
    }
}
