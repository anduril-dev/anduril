package org.anduril.core.commandline;

import org.anduril.core.network.StaticError;
import org.anduril.core.writers.WorkflowSBTGenerator;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;

public class GenerateSBTCommand extends BaseCommand {

    public static final String OPT_OUT_FILE = "out-file";

    public static final String DEFAULT_OUT_FILE = "build.sbt";

    public GenerateSBTCommand() {
        super("generate-sbt", "PROJECT-NAME");
    }

    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(OptionBuilder
                .withLongOpt(OPT_OUT_FILE)
                .hasArg()
                .withDescription("Output file name (default: build.sbt).")
                .isRequired(false)
                .create('o'));
    }

    @Override
    protected void executeImpl() throws IOException, StaticError {
        final String projectName = getCommandArgs().get(0);

        final String outFile;
        if (getCommandLine().hasOption(OPT_OUT_FILE)) {
            outFile = getCommandLine().getOptionValue(OPT_OUT_FILE);
        } else {
            outFile = DEFAULT_OUT_FILE;
        }

        if (projectName == null || projectName.isEmpty()) {
            printHelp();
            throw new StaticError("Expected project name");
        }

        loadAllBundlesFromJAR();

        WorkflowSBTGenerator generator = new WorkflowSBTGenerator(projectName, repository.getBundles());
        generator.generateSBT(new File(outFile));
    }

    @Override
    protected String getHelpContents() {
        return "Write a template SBT build file that can be used to bootstrap workflow projects.";
    }

}
