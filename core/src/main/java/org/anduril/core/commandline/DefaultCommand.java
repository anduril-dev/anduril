package org.anduril.core.commandline;

import org.anduril.core.Version;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings("static-access")
public class DefaultCommand extends BaseCommand {
    public static final String CMD_DEFAULT = "default";
    
    public static final String OPT_VERSION = "version";

    public DefaultCommand() {
        super(CMD_DEFAULT, null);
    }

    /**
     * Print help for all commands.
     */
    public void printHelpForAll() {
        System.out.println(String.format(
                "General syntax: %s <command> <arguments>", CLI.ANDURIL_EXEC));
        System.out.println(String.format(
                "For options of each command, use \"%s <command> --help\"",
                CLI.ANDURIL_EXEC));
        System.out.println("Command summary: ");
        
        for (BaseCommand cmd: CLI.buildCommands().values()) {
            if (cmd.getName().equals(this.getName())) continue;
            System.out.println();
            cmd.printUsage();
        }
    }
    
    @Override
    public void printHelp() {
        printHelpForAll();
    }

    protected void addOptions(Options options) {
        options.addOption(OptionBuilder
                .withLongOpt(OPT_VERSION)
                .withDescription("Print Anduril version")
                .create());
    }

    @Override
    protected void executeImpl() {
        if (getCommandLine().hasOption(OPT_VERSION)) {
            String ver = String.format("Anduril version %s",
                    Version.ANDURIL_VERSION);
            System.out.println(ver);
        } else {
            printHelpForAll();
        }
    }

    @Override
    protected String getHelpContents() {
        return null;
    }

}
