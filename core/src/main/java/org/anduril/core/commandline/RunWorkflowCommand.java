package org.anduril.core.commandline;

import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.Network;
import org.anduril.core.network.StaticError;
import org.anduril.core.network.launcher.Launcher;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;

public class RunWorkflowCommand extends BaseCommand {
    public static final String OPT_DRY_RUN = "dry";
    public static final String OPT_FORCE = "force";
    public static final String OPT_FORCE_ALL = "force-all";
    public static final String OPT_DATA_DIR = "data-dir";
    public static final String OPT_MINIMIZE_SPACE = "min-space";
    public static final String OPT_RETAIN_NETWORK = "retain-network";
    public final static String OPT_WRAPPER = "wrapper";

    public RunWorkflowCommand() {
        super("run-workflow", "<WORKFLOW.jar>");
    }

    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getExecuteDirOption(false));
        options.addOption(getJavaHeapOption(false));
        options.addOption(getThreadOption(false));
        options.addOption(getClasspathOption(false));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_DRY_RUN)
                .withDescription("Generate a network for the pipeline and create a state based on the network, " +
                		"but do not execute the pipeline.")
                .create());
        
        options.addOption(OptionBuilder
                .withLongOpt(OPT_FORCE_ALL)
                .withDescription("Force execution of all components, even if their configuration is unchanged")
                .create());
        
        options.addOption(OptionBuilder
                .withLongOpt(OPT_FORCE)
                .hasArg()
                .withDescription("Force execution of the following components (in comma-separated list), " +
                		"even if their configuration is unchanged")
                .create());
        
        options.addOption(OptionBuilder
                .withLongOpt(OPT_MINIMIZE_SPACE)
                .withDescription("Minimize disk storage by removing unused intermediate output.")
                .create());

        options.addOption(OptionBuilder
                .withLongOpt(OPT_RETAIN_NETWORK)
                .withDescription("Do not write state file at the end, only when components finish, in order to avoid overwriting the whole network state at once.")
                .create());

        options.addOption(OptionBuilder
                .hasArg()
                .withLongOpt(OPT_DATA_DIR)
                .withDescription("Location of data files for input components. "
                        +"If not given, the directory containing workflow configuration is used. "
                        +"If <workflow-file> is \"-\", this directory is also searched for included workflow files.")
                .create());
        
        options.addOption(OptionBuilder
                .hasArg()
                .withLongOpt(OPT_WRAPPER)
                .withDescription("A command that customizes component launching logic. This command is placed as a prefix of the launch command.")
                .create());
    }
    
    @Override
    protected String getHelpContents() {
        return "Execute a workflow. Workflow configuration is read from <workflow-file> "
        + "or, if <workflow-file> is \"-\", from standard input. "        
        + "The state of previous workflow execution is read from disk "
        + "and only changed and failed components are executed. "
        + "This can be overriden with --force and --force-all. "
        ;
    }
    
    public String[] getForce() {
        if (getCommandLine().hasOption(OPT_FORCE_ALL)) return new String[] {"*"};
        else if (!getCommandLine().hasOption(OPT_FORCE)) return null;
        else return getCommandLine().getOptionValue(OPT_FORCE).split(",");
    }
    
    public File getDataDir() {
        String value = getCommandLine().getOptionValue(OPT_DATA_DIR);
        if (value == null) return null;
        else return new File(value);
    }

    public boolean getOptimizeSpace() {
        return getCommandLine().hasOption(OPT_MINIMIZE_SPACE);
    }
    public boolean getCommitFinalState() {
        return !getCommandLine().hasOption(OPT_RETAIN_NETWORK);
    }


    @Override
    protected void executeImpl() throws IOException, StaticError, DynamicError {
        File networkFile = getArgumentFile(0);
        if (networkFile == null) {
            printHelp();
            throw new StaticError("Expected workflow file");
        }

        loadAllBundlesFromJAR();

        if (getCommandLine().hasOption(OPT_FORCE_ALL)) {
            if (getCommandLine().hasOption(OPT_FORCE)) {
                throw new StaticError("--force and --force-all must not be both included");
            }
        }
        if (getCommandLine().hasOption(OPT_WRAPPER)) {
            final String wrapper = getCommandLine().getOptionValue(OPT_WRAPPER);
            Launcher.setWrapperPrefix(wrapper.split(" "));
        }

        boolean isDryRun = getCommandLine().hasOption(OPT_DRY_RUN);

        evaluator.setMinimizeSpace(getOptimizeSpace());
        boolean readPrevious = !getCommandLine().hasOption(OPT_FORCE_ALL);
        evaluator.setReadStateFile(readPrevious);
        evaluator.setForced(getForce());
        evaluator.setDataDir(getDataDir());

        Network network = getEvaluator().evaluate(networkFile, !isDryRun);

        if (getCommandLine().hasOption(OPT_VERBOSE)) {
            getLogger().debug(network.format());
        }
        
        if (isDryRun) {
            Engine engine = evaluator.makeEngine(network);
            System.out.println(engine.getExecutionDirectory().getRoot());
            engine.getExecutionDirectory().getRoot().mkdirs();
            engine.getStateFile().writeAll(network, engine);
            engine.getStateFile().commit();
        } else {
            evaluator.setCommitFinalState(getCommitFinalState());
            evaluator.execute(network);
        }
    }
}
