package org.anduril.core.commandline;

import org.anduril.core.engine.TestCaseChecker;
import org.anduril.core.network.Bundle;
import org.anduril.core.network.NetworkTestCase;
import org.anduril.core.network.StaticError;
import org.anduril.core.network.TestCase;
import org.anduril.core.utils.IOTools;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RunTestWorkflowCommand extends BaseCommand {

    public static final String OPT_WORKFLOW = "workflow";

    public RunTestWorkflowCommand() {
        super("run-test-workflow", "-b BUNDLE-NAME -w WORKFLOW-NAME [-w MORE]");
    }

    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(false));
        options.addOption(getExecuteDirOption(false));
        options.addOption(getJavaHeapOption(false));
        options.addOption(getThreadOption(false));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_WORKFLOW)
                .hasArg()
                .withDescription("Execute only these test workflows. Multiple -w can be given. "
                        +"If omitted, all test workflows of selected bundles are included.")
                .isRequired(false)
                .create('w'));

    }

    @Override
    protected void executeImpl() throws StaticError {
        loadAllBundlesFromJAR();

        final List<String> networkNames;
        if (getCommandLine().hasOption(OPT_WORKFLOW)) {
            networkNames = Arrays.asList(getCommandLine().getOptionValues(OPT_WORKFLOW));
        } else {
            networkNames = Collections.emptyList();
        }

        TestCaseChecker checker = new TestCaseChecker(getRepository(), getExecutionRoot(), getLogger());

        boolean isNetworkFound = false;
        for (File bundleDir: getExplicitBundles()) {
            Bundle bundle = getRepository().getBundle(bundleDir.getName());
            if (bundle == null) {
                throw new StaticError("Bundle not found: "+bundleDir);
            }

            if (bundle.getTestNetworkDirectory().exists()) {
                for (File dir: bundle.getTestNetworkDirectory().listFiles()) {
                    if (!dir.isDirectory()) continue;
                    if (dir.getName().startsWith(".")) continue;
                    if (!networkNames.isEmpty() && !networkNames.contains(dir.getName())) continue;
    
                    TestCase testCase;
                    try {
                        testCase = new NetworkTestCase(dir, getRepository(), bundle);
                        isNetworkFound = true;
                    } catch(IOException e) {
                        String msg = String.format("Error reading network test case %s: %s",
                                dir.getAbsolutePath(), e);
                        throw new StaticError(msg);
                    }
                    checker.addTestCase(testCase);
                }
            }
        }
        
        if (!isNetworkFound) {
            throw new StaticError("Could not find these networks: " + networkNames);
        }
        else {
            final int numFailed;
    		try {
                IOTools.rmdir(this.getExecutionRoot(), true);
    		    numFailed = checker.executeTestCases();
    		    checker.printResultsToLog();
    		} catch (Exception e) {
    		    throw new StaticError("Exception in test case checker: "+e, e);
    		}
            if (numFailed > 0) {
                throw new StaticError(String.format("Failed %d test cases", numFailed));
            }
        }
    }

    protected String getHelpContents() {
        return "Run workflow tests for one or more bundles. "
        + "Each test is defined by a workflow configuration and an expected output directory. "
        + "-w allows selecting tests by workflow directory names.";
    }

}
