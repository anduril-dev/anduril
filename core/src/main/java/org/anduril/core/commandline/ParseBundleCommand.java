package org.anduril.core.commandline;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import org.anduril.core.network.*;
import org.anduril.core.writers.JSONSerializer;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.*;
import java.util.*;

public class ParseBundleCommand extends BaseCommand {

    public static final String OPT_COMPONENT = "component";
    public static final String OPT_ENABLE_DATA_TYPES = "data-types";
    public static final String OPT_OUT_FILE = "out-file";

    public ParseBundleCommand() {
        super("parse-bundle", "-b BUNDLE -o OUT-FILE [-c COMPONENT] [-b MORE] [--data-types]");
    }

    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(true));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_COMPONENT)
                .hasArg()
                .withDescription("Include only given component(s). Multiple -c can be given. "
                        +"If omitted, all components in selected bundles are included.")
                .isRequired(false)
                .create('c'));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_ENABLE_DATA_TYPES)
                .withDescription("If given, also write data types into JSON output.")
                .isRequired(false)
                .create());

        options.addOption(OptionBuilder
                .withLongOpt(OPT_OUT_FILE)
                .hasArg()
                .withDescription("Output file name.")
                .isRequired(true)
                .create('o'));
    }

    @Override
    protected void executeImpl() throws IOException, StaticError {
        final Set<String> componentNames;
        if (getCommandLine().hasOption(OPT_COMPONENT)) {
            componentNames = new TreeSet<String>(Arrays.asList(getCommandLine().getOptionValues(OPT_COMPONENT)));
        } else {
            componentNames = null;
        }

        ArrayList<Bundle> explicitBundles = new ArrayList<Bundle>();
        for (File bundleDir: getExplicitBundles()) {
            Bundle bundle = this.repository.loadBundleFromXML(bundleDir, componentNames);
            explicitBundles.add(bundle);
        }

        final boolean enableDataTypes = getCommandLine().hasOption(OPT_ENABLE_DATA_TYPES);

        Gson gson = new GsonBuilder().serializeNulls().create();
        JSONSerializer serializer = new JSONSerializer(this.repository, gson, enableDataTypes);

        final File outFile = new File(getCommandLine().getOptionValue(OPT_OUT_FILE));
        final Writer writer = new FileWriter(outFile);
        final JsonWriter jsonWriter = new JsonWriter(writer);
        jsonWriter.setSerializeNulls(true);
        jsonWriter.setIndent("\t");
        gson.toJson(serializer.serializeBundles(explicitBundles), jsonWriter);
        writer.close();
    }

    @Override
    protected String getHelpContents() {
        return "Write bundle contents (components and data types) in JSON format.";
    }

}
