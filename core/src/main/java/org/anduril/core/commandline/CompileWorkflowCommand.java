package org.anduril.core.commandline;

import org.anduril.core.network.*;
import org.anduril.core.utils.IOTools;
import org.anduril.core.writers.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CompileWorkflowCommand extends BaseCommand {

    public static final String OPT_SOURCE = "source";
    public static final String OPT_JAR = "jar";

	public CompileWorkflowCommand() {
		super("compile-workflow", "-s SOURCE.scala --jar OUTPUT.jar [-s MORE.scala]");
	}

    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(false));
        options.addOption(getClasspathOption(false));

        options.addOption(OptionBuilder
	            .hasArg()
	            .withLongOpt(OPT_SOURCE)
	            .withDescription("Scala source file.")
	            .isRequired(true)
	            .create('s'));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_JAR)
                .hasArg()
                .withDescription("Target JAR for Scala source files.")
                .isRequired(true)
                .create('j'));
    }

    private void compileSource(String source, String targetJAR) throws StaticError {
        loadAllBundlesFromJAR();

        String classPath = getRepository().getEngineJAR().getAbsolutePath();
        for (Bundle bundle: this.getRepository().getBundles()) {
            classPath += ":" + bundle.getBundleJAR().getAbsolutePath();
        }

        if (getCommandLine().hasOption(OPT_CLASS_PATH)) {
            for (String cp : getCommandLine().getOptionValue(OPT_CLASS_PATH).split("[, ]")) {
                classPath += ":" + cp;
            }
        }

        String compileScript = "scalac -nobootcp -cp " + classPath + " -d " + targetJAR + " " + source;
		
        getLogger().info("Compile command: "+compileScript);
        
        try {
            int status = IOTools.launch(compileScript, null, null, null, getLogger());
            if (status != 0) {
                String msg = String.format("Error in workflow script (exit status: %d).\nCompile command was: %s",
                        status, compileScript);
                throw new StaticError(msg);
            }
        } catch (IOException e) {
            String msg = String.format("Error in workflow script: %s\nCompile command was: %s",
                    e.getMessage(), compileScript);
            throw new StaticError(msg);
        }
    }

	@Override
	protected void executeImpl() throws IOException, StaticError {
        String source = "";
        for (String s: getCommandLine().getOptionValues(OPT_SOURCE)) {
            source += " " + s;
        }

        final String targetJar = getCommandLine().getOptionValue(OPT_JAR);
        if (targetJar == null || targetJar.isEmpty()) {
            throw new StaticError("Need destination jar filename.");
        }

        if (source.isEmpty()) {
            throw new StaticError("Nothing to compile.");
        }

        compileSource(source, targetJar);
    }

	@Override
	protected String getHelpContents() {
		return "Compile a workflow from Scala source file(s). Compile code is written to a JAR file.";
	}

}
