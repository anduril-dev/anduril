package org.anduril.core.commandline;

import org.anduril.core.network.*;
import org.anduril.core.utils.IOTools;
import org.anduril.core.writers.*;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CompileBundleCommand extends BaseCommand {

    public static final String SOURCE_PREFIX = "generated-src" + File.separator;

	public CompileBundleCommand() {
		super("compile-bundle", "-b BUNDLE");
	}

    @Override
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(true));
        options.addOption(getClasspathOption(false));
    }

    private void compile(Bundle bundle) throws IOException, StaticError {
        String bundleName = bundle.getName();

        File destinationDir = bundle.getDirectory();

        System.out.println(String.format("generating Scala source for bundle %s at %s...", bundleName, destinationDir));

        // Clear out possible earlier compilations.
        IOTools.rmdir(new File(destinationDir, SOURCE_PREFIX), true);
        IOTools.rmdir(new File(destinationDir, bundleName + ".jar"), true);

        // Generate datatypes and package object.
        String sourceDestination = SOURCE_PREFIX + bundle.getSourcePackageName();
        File datatypesSource = new File(destinationDir, sourceDestination + File.separator + DatatypeGenerator.TYPES_PACKAGE);
        datatypesSource.mkdirs();
        DatatypeGenerator dtgen = new DatatypeGenerator(getRepository(), bundle);
        File dataTypesDestination = new File(datatypesSource, "types.scala");
        dtgen.generate(dataTypesDestination);

        PackageObjectGenerator pkgObjectGenerator = new PackageObjectGenerator(bundle, this.repository);
        File pkgObjectDestination = new File(new File(destinationDir, sourceDestination), "package.scala");
        pkgObjectGenerator.generate(pkgObjectDestination);

        File bundleSource = new File(destinationDir, sourceDestination);
        bundleSource.mkdirs();

        List<Component> bundleComponents = bundle.getComponents(repository);
        System.out.println("components (" + bundleComponents.size() + ")");

        for (Component comp: bundleComponents) {
            File targetDir = new File(destinationDir, sourceDestination);
            final String FILE_SUFFIX = ".scala";
            File target = new File(targetDir, comp.getName() + FILE_SUFFIX);
            new ComponentGenerator(comp, bundle, getRepository()).generateComponentScalaFile(target);

        }

        // Functions
        System.out.println(String.format("functions (%d)", repository.getFunctions().size()));
        for (Component func: repository.getFunctions()) {
            if (func.getBundle().getName().equals(bundleName)) {
                File targetDir = new File(destinationDir, sourceDestination);
                final String FILE_SUFFIX = ".scala";
                File target = new File(targetDir, func.getName() + FILE_SUFFIX);
                new LibFunctionGenerator(func, bundle, getRepository()).generateComponentScalaFile(target);
            }
        }

        // Execute command -- sbt package

        System.out.println("\npackaging " + bundleName + ".jar... ");

        BundleSBTGenerator sbtGenerator = new BundleSBTGenerator(bundle, getRepository());
        File sbtDestination = new File(destinationDir, "build.sbt");
        sbtGenerator.generateSBT(sbtDestination);
        int status = IOTools.launch("sbt package", destinationDir, null, null, getLogger());
        if (status != 0) {
            String msg = String.format("Bundle compilation failed (status: %d", status);
            throw new StaticError(msg);
        }
    }

	@Override
	protected void executeImpl() throws IOException, StaticError {
        List<Bundle> explicitBundles = loadExplicitBundlesFromXML();

        for (Bundle bundle: getRepository().getBundles()) {
            boolean isExplicit = explicitBundles.contains(bundle);
            if (isExplicit || !bundle.isCompiled()) {
                compile(bundle);
            }
        }
    }

	@Override
	protected String getHelpContents() {
		return "Compile a bundle. Scala source code is generated for each "
                + "component following the corresponding component.xml file. "
                + "A JAR file with the name of the bundle is generated and "
                + "stored in the bundle's main folder. Compilation takes place "
                + "at each invocation.";
	}

}
