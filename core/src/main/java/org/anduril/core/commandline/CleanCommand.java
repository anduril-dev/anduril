package org.anduril.core.commandline;

import org.anduril.component.Tools;
import org.anduril.core.engine.ExecutionDirectory;
import org.anduril.core.engine.NetworkEvaluator;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Network;
import org.anduril.core.network.StaticError;
import org.anduril.core.network.componentInstance.OutputComponentInstance;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashSet;
import java.util.Set;

public class CleanCommand extends BaseCommand {
    public static final String OPT_DRY = "dry";
    
    public CleanCommand() {
        super("clean", "<workflow-file | \"-\"> -d EXECDIR");
    }
    
    @Override
    @SuppressWarnings("static-access")
    protected void addOptions(Options options) {
        options.addOption(getBundleOption(false));
        options.addOption(getExecuteDirOption(true));

        options.addOption(OptionBuilder
                .withLongOpt(OPT_DRY)
                .withDescription("Dry run: show unused directories but do not delete anything")
                .create());
    }
    
    @Override
    protected void executeImpl() throws IOException, StaticError {
        File networkFile = getArgumentFile(0);
        if (networkFile == null) {
            printHelp();
            throw new StaticError("Expected workflow file");
        }

        loadAllBundlesFromJAR();

        final File execRoot = getExecutionRoot();
        final boolean dry = getCommandLine().hasOption(OPT_DRY);

        Network network = getEvaluator().evaluate(networkFile);
        if (evaluator.hasEngineErrors()) return;

        executeDirClean(execRoot, dry, network, evaluator);
    }
    
    static public void executeDirClean(File             execRoot,
    		                           boolean          dry,
    		                           Network          network,
    		                           NetworkEvaluator evaluator) throws IOException {
        Set<String> validFiles = new HashSet<String>();
        
        if (dry) System.out.println("Dry run");
        
        ExecutionDirectory execDir = evaluator.getExecutionDirectory(execRoot);
        File stateFile = execDir.getStateFile();
        if (!stateFile.exists()) {
            System.out.println("Directory does not seem to be execution directory: "+execRoot.getAbsolutePath());
            System.out.println("State file not present: "+stateFile.getAbsolutePath());
            return;
        }

        File dryRunStateFile = execDir.getTemporaryStateFile();
        if (dryRunStateFile.exists()) {
        	
        	if (!stateFile.exists()) stateFile.createNewFile(); 

            FileChannel in = null;
            FileChannel out = null;

            try {
	            in = new FileInputStream( dryRunStateFile ).getChannel();
	            out = new FileOutputStream( stateFile ).getChannel();
	            out.transferFrom( in, 0, in.size() );
            }
            catch (IOException e) {
            	throw new IOException("IO error while opening " +
            			dryRunStateFile.getAbsolutePath() + " or " + 
            			stateFile + ": " + e.getMessage());
            }
            finally {
	            if (out != null) out.close();
	            if (in != null) in.close();
            }
        }
        
        validFiles.add(stateFile.getName());
        
        for (ComponentInstance ci: network) {
            File compDir = execDir.getComponentInstanceRoot(ci);
            validFiles.add(compDir.getName());
            if (ci instanceof OutputComponentInstance) {
            	validFiles.add(OutputComponentInstance.OUTPUT_DIRECTORY);
            }
        }
        
        for (File file: execRoot.listFiles()) {
            final String name = file.getName();
            if (!validFiles.contains(name)) {
                System.out.println("Obsolete: "+file.getAbsolutePath());
                if (!dry) {
                    Tools.removeFile(file);
                }
            } else {
                System.out.println("Current: "+file.getAbsolutePath());
            }
        }
    }

    @Override
    protected String getHelpContents() {
        return "(DEPRECATED) Remove obsolete files and directories from an execution directory. "
        + "These may have been produced by component instances that have "
        + "since been removed or renamed. Only directories corresponding "
        + "to current component instances are preserved. Note that the -d argument "
        + "is mandatory.";
    }
}
