package org.anduril.core.writers;

import org.anduril.component.Tools;
import org.anduril.core.Version;
import org.anduril.core.network.*;
import org.anduril.core.utils.HTMLTools;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Collections;
import java.util.Comparator;

/**
 * Write document and data type HTML manual pages
 * using Apache Velocity templates. The templates
 * are located in <code>core/src/main/resources<code>.
 * For both kinds of manual pages, a summary page
 * and several individual pages are written.
 */
public class ComponentDocWriter {

    public static final String JS_DIR = "js";

    public static final String COMPONENT_FILE = "index.html";
    public static final String COMPONENT_SUMMARY_FILE = "component-summary.html";
    public static final String COMPONENT_JS_FILE = "component-db.js";
    public static final String TYPE_FILE = "index.html";
    public static final String TYPE_SUMMARY_FILE = "type-summary.html";
    public static final String TYPE_JS_FILE = "type-db.js";
    public static final String CATEGORY_FILE = "index.html";
    public static final String CATEGORY_SUMMARY_FILE = "category-summary.html";
    public static final String CATEGORY_JS_FILE = "category-db.js";
    public static final String AUTHOR_FILE = "index.html";
    public static final String AUTHOR_SUMMARY_FILE = "author-summary.html";

    public static final String TEMPLATE_COMPONENT = "component.html";
    public static final String TEMPLATE_COMPONENT_SUMMARY = "/"+COMPONENT_SUMMARY_FILE;
    public static final String TEMPLATE_COMPONENT_JS = "/"+JS_DIR+"/"+COMPONENT_JS_FILE;
    public static final String TEMPLATE_TYPE = "/type.html";
    public static final String TEMPLATE_TYPE_SUMMARY = "/"+TYPE_SUMMARY_FILE;
    public static final String TEMPLATE_TYPE_JS = "/"+JS_DIR+"/"+TYPE_JS_FILE;
    public static final String TEMPLATE_CATEGORY = "/category.html";
    public static final String TEMPLATE_CATEGORY_SUMMARY = "/"+CATEGORY_SUMMARY_FILE;
    public static final String TEMPLATE_CATEGORY_JS = "/"+JS_DIR+"/"+CATEGORY_JS_FILE;
    public static final String TEMPLATE_AUTHOR = "/author.html";
    public static final String TEMPLATE_AUTHOR_SUMMARY = "/"+AUTHOR_SUMMARY_FILE;

    public static final String COMPONENT_DIRECTORY = "components";
    public static final String TYPE_DIRECTORY = "types";
    public static final String CATEGORY_DIRECTORY = "categories";
    public static final String AUTHOR_DIRECTORY = "authors";

    public static final String CSS_FILE = "anduril.css";
    public static final String INDEX_FILE = "index.html";
    public static final String NAVIGATION_FILE = "navigation.html";
    public static final String IMAGES_DIR = "images";
    public static final String JS_SEARCH_FILE = "search.js";
    public static final String JS_VARIABLES_FILE = "variables.js";
    public static final String JS_FRAMES_FILE = "main-frame.js";
    public static final String JS_CONTENT_FILE = "content-frame.js";

    public static final String TEST_CASE_DIRECTORY = "testcases";
    public static final String TEST_CASE_INPUT_DIRECTORY = "input";
    public static final String TEST_CASE_OUTPUT_DIRECTORY = "output";
    
    public static final String ANDURIL_URL = "http://www.anduril.org/";
    
    public static final String JS_JQUERY_FILE = "jquery.js";
    public static final String JS_JQUERY_SNIPPET_FILE = "jquery.snippet.min.js";
    public static final String JS_SNIPPET_CSS_FILE = "jquery.snippet.min.css";
    
    private Repository repository;
    private List<Bundle> selectedBundles;
    private File directory;

    /**
     * Initialize.
     * @param repository The repository that contains components
     * 	and data types.
     * @param directory The target directory where the HTML tree
     * 	is to be written.
     */
    public ComponentDocWriter(Repository repository, List<Bundle> selectedBundles, File directory) throws Exception {
        this.repository = repository;
        this.selectedBundles = selectedBundles;
        this.directory  = directory;
        
        Velocity.setProperty("runtime.references.strict", "true");
        Velocity.setProperty("resource.loader", "class");
        Velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader"); 
        Velocity.init();
    }

    /**
     * Write component and data type manual pages.
     * @throws IOException if an I/O error occurred.
     */
    public void write() throws Exception {
        Template tComponent = Velocity.getTemplate(TEMPLATE_COMPONENT);
        Template tComponentSummary = Velocity.getTemplate(TEMPLATE_COMPONENT_SUMMARY);
        Template tComponentJs = Velocity.getTemplate(TEMPLATE_COMPONENT_JS);
        Template tType = Velocity.getTemplate(TEMPLATE_TYPE);
        Template tTypeSummary = Velocity.getTemplate(TEMPLATE_TYPE_SUMMARY);
        Template tTypeJs = Velocity.getTemplate(TEMPLATE_TYPE_JS);
        Template tCategory = Velocity.getTemplate(TEMPLATE_CATEGORY);
        Template tCategorySummary = Velocity.getTemplate(TEMPLATE_CATEGORY_SUMMARY);
        Template tCategoryJs = Velocity.getTemplate(TEMPLATE_CATEGORY_JS);
        Template tAuthor = Velocity.getTemplate(TEMPLATE_AUTHOR);
        Template tAuthorSummary = Velocity.getTemplate(TEMPLATE_AUTHOR_SUMMARY);
        
        copyURLResources(getBuiltinResources());
        writeTypeSummary(TYPE_SUMMARY_FILE, tTypeSummary);
        writeTypeSummary(TEMPLATE_TYPE_JS, tTypeJs);
        writeComponentSummary(COMPONENT_SUMMARY_FILE, tComponentSummary);
        writeComponentSummary(TEMPLATE_COMPONENT_JS, tComponentJs);
        writeCategorySummary(CATEGORY_SUMMARY_FILE, tCategorySummary);
        writeCategorySummary(TEMPLATE_CATEGORY_JS, tCategoryJs);
        writeAuthorSummary(AUTHOR_SUMMARY_FILE, tAuthorSummary);

        for (DataType type: repository.getDataTypes()) {
            writeType(type, tType);
        }
        
        for (Category category: repository.getCategoriesList()) {
            writeCategory(category, tCategory);
        }

        for (Author author: repository.getAuthorsList()) {
            writeAuthor(author, tAuthor);
        }
        
        for (Component comp: repository.getComponents(selectedBundles)) {
            writeComponent(comp, "Component", tComponent);
        }
        
        for (Component function: repository.getFunctions(this.selectedBundles)) {
            writeComponent(function, "Function", tComponent);
        }
    }
    
    private void renderVelocity(Template template, VelocityContext model, File target) throws Exception {
        FileWriter writer = new FileWriter(target);
        try {
            template.merge(model, writer);
        } finally {
            writer.close();
        }
    }
    
    private Set<String> getBuiltinResources() {
        Set<String> names = new HashSet<String>();
        names.add("/"+CSS_FILE);
        names.add("/"+IMAGES_DIR+"/arrow-right.png");
        names.add("/"+IMAGES_DIR+"/arrow-up.png");
        names.add("/"+IMAGES_DIR+"/category.png");
        names.add("/"+IMAGES_DIR+"/author.png");
        names.add("/"+IMAGES_DIR+"/component.png");
        names.add("/"+IMAGES_DIR+"/component-input.png");
        names.add("/"+IMAGES_DIR+"/component-output.png");
        names.add("/"+IMAGES_DIR+"/component-param.png");
        names.add("/"+IMAGES_DIR+"/datatype.png");
        names.add("/"+IMAGES_DIR+"/logo.png");
        names.add("/"+IMAGES_DIR+"/testcase.png");
        names.add("/"+INDEX_FILE);
        names.add("/"+NAVIGATION_FILE);
        names.add("/"+JS_DIR+"/"+JS_SEARCH_FILE);
        names.add("/"+JS_DIR+"/"+JS_VARIABLES_FILE);
        names.add("/"+JS_DIR+"/"+JS_FRAMES_FILE);
        names.add("/"+JS_DIR+"/"+JS_CONTENT_FILE);
        names.add("/"+JS_DIR+"/"+JS_JQUERY_FILE);
        names.add("/"+JS_DIR+"/"+JS_JQUERY_SNIPPET_FILE);
        names.add("/"+JS_SNIPPET_CSS_FILE);
        
        return names;
    }
    
    private VelocityContext getCommonModel(String title, int depth) {
        VelocityContext model = new VelocityContext();
        model.put("css", HTMLTools.descendDirectories(depth)+CSS_FILE);
        model.put("images", HTMLTools.descendDirectories(depth)+IMAGES_DIR);
        model.put("depth", depth);
        model.put("formatter", new VelocityFormatter(repository));
        model.put("timestamp", DateFormat.getDateTimeInstance().format(new Date()));
        model.put("title", title);
        model.put("version", Version.ANDURIL_VERSION.toString());
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String TIMESTAMP = format.format(new Date());
        
        String footer = String.format("<hr/>Generated %s by <a href=\"%s\">Anduril</a> %s",
                TIMESTAMP, ANDURIL_URL, Version.ANDURIL_VERSION.toString());
        model.put("footer", footer);
        
        return model;
    }

    private URL getResource(String name) throws IOException {
        URL url = ComponentDocWriter.class.getResource(name);
        if (url == null) {
            throw new IOException("Resource not found: "+name);
        }
        return url;
    }
    
    public static File getTypeFile(File baseDir, String typeName) {
        File common;
        if (baseDir == null)
            common = new File(TYPE_DIRECTORY);
        else
            common = new File(baseDir, TYPE_DIRECTORY);

        File typeDir = new File(common, typeName);
        return new File(typeDir, TYPE_FILE);
    }
    
    public static File getTypeFile(File baseDir, DataType type) {
        return getTypeFile(baseDir, type.getName());
    }
    
    public static File getCategoryFile(File baseDir, String categoryName) {
        File common;
        if (baseDir == null)
            common = new File(CATEGORY_DIRECTORY);
        else
            common = new File(baseDir, CATEGORY_DIRECTORY);

        File categoryDir = new File(common, categoryName);
        return new File(categoryDir, CATEGORY_FILE);
    }
    
    public static File getCategoryFile(File baseDir, Category category) {
        return getCategoryFile(baseDir, category.getName());
    }
    
    public static File getAuthorFile(File baseDir, String authorName) {
        File common;
        if (baseDir == null)
            common = new File(AUTHOR_DIRECTORY);
        else
            common = new File(baseDir, AUTHOR_DIRECTORY);

        File authorDir = new File(common, authorName);
        return new File(authorDir, AUTHOR_FILE);
    }
    
    public static File getAuthorFile(File baseDir, Author author) {
        return getAuthorFile(baseDir, author.getHtmlName());
    }
    
    public static File getComponentFile(File baseDir, String componentName) {
        File common;
        if (baseDir == null) common = new File(COMPONENT_DIRECTORY);
        else common = new File(baseDir, COMPONENT_DIRECTORY);
        
        File compDir = new File(common, componentName);
        return new File(compDir, COMPONENT_FILE);
    }
    
    public static File getComponentFile(File baseDir, Component component) {
        return getComponentFile(baseDir, component.getName());
    }
    
    private void copyResources(File targetDirectory, Set<File> resources) throws IOException {
        targetDirectory.mkdirs();
        for (File docResource: resources) {
            if (docResource.isDirectory()) {
                Tools.copyDirectory(docResource, new File(targetDirectory,
                        docResource.getName()),true,".svn");
            }
            else { 
                Tools.copyFile(docResource, new File(targetDirectory,
                        docResource.getName()));
            }
        }
    }
    
    private void copyURLResources(Set<String> resourceNames) throws IOException {
        for (String name: resourceNames) {
            URL url = getResource(name);
            InputStream in = url.openStream();
            File target = new File(directory, name);
            if (!target.getParentFile().exists()) {
                target.getParentFile().mkdirs();
            }
            Tools.copyStreamToFile(in, target);
            in.close();
        }
    }
    
    private void copyTestFiles(File targetDirectory, Component component) throws IOException {
        final File testDir = new File(targetDirectory, TEST_CASE_DIRECTORY);
        for (ComponentTestCase testCase: component.getTestCases()) {
            final File caseDir = new File(testDir, testCase.getCaseName());

            File propertiesFile = testCase.getParameterFile();
            if (propertiesFile != null) {
                if (!caseDir.exists() && !caseDir.mkdirs())
                    throw new IOException("Cannot create directory: "+caseDir);
                Tools.copyFile(propertiesFile, new File(caseDir, testCase.getEnvNameProps()));
            }

            for (InPort port: component.getInPorts()) {
                File sourceFile = testCase.getInputFile(port);
                if (sourceFile == null || !sourceFile.exists()) continue;

                File targetDir = new File(caseDir, TEST_CASE_INPUT_DIRECTORY);
                targetDir.mkdirs();
                File targetFile = new File(targetDir, sourceFile.getName());

                if (sourceFile.isDirectory()) Tools.copyDirectory(sourceFile, targetFile,true,".svn");
                else Tools.copyFile(sourceFile, targetFile);
            }

            for (OutPort port: component.getOutPorts()) {
                File sourceFile = testCase.getOutputFile(port);
                if (sourceFile == null || !sourceFile.exists()) continue;

                File targetDir = new File(caseDir, TEST_CASE_OUTPUT_DIRECTORY);
                targetDir.mkdirs();
                File targetFile = new File(targetDir, sourceFile.getName());

                if (sourceFile.isDirectory()) Tools.copyDirectory(sourceFile, targetFile,true,".svn");
                else Tools.copyFile(sourceFile, targetFile);
            }
        }
    }

    private void writeTypeSummary(String filename, Template template) throws Exception {
        VelocityContext model = getCommonModel("Data types", 0);
        model.put("types", repository.getDataTypes(this.selectedBundles));
        renderVelocity(template, model, new File(directory, filename));
    }

    private void writeType(DataType type, Template template) throws Exception {
        VelocityContext model = getCommonModel("Data type: "+type.getName(), 2);
        File typeFile = getTypeFile(directory, type);

        typeFile.getParentFile().mkdirs();
        model.put("type", type);
        renderVelocity(template, model, typeFile);
        copyResources(typeFile.getParentFile(), type.getDocResources());
    }

    private void writeCategorySummary(String filename, Template template) throws Exception {
        VelocityContext model = getCommonModel("Categories", 0);
        model.put("categories", repository.getCategoriesList());
        renderVelocity(template, model, new File(directory, filename));
    }
    
    private void writeCategory(Category category, Template template) throws Exception {
        VelocityContext model = getCommonModel("Category: "+category.getName(), 2);
        File categoryFile = getCategoryFile(directory, category.getName());
        categoryFile.getParentFile().mkdirs();
        model.put("category", category);
        renderVelocity(template, model, categoryFile);
        copyResources(categoryFile.getParentFile(), category.getDocResources());
    }
    
    private void writeAuthorSummary(String filename, Template template) throws Exception {
        VelocityContext model = getCommonModel("Authors", 0);
        model.put("authors", repository.getAuthorsList());
        renderVelocity(template, model, new File(directory, filename));
    }

    private void writeAuthor(Author author, Template template) throws Exception {
        VelocityContext model = getCommonModel("Author: "+ author.getHtmlName(), 2);
        File authorFile = getAuthorFile(directory, author.getName());
        authorFile.getParentFile().mkdirs();
        model.put("author", author);
        renderVelocity(template, model, authorFile);
    }

    private void writeComponentSummary(String filename, Template template) throws Exception {
        VelocityContext model = getCommonModel("Components", 0);
        model.put("components", getRepositorySorted(repository.getComponents(this.selectedBundles)));
        model.put("functions", getRepositorySorted(repository.getFunctions(this.selectedBundles)));
        renderVelocity(template, model, new File(directory, filename));
    }
    
    private void writeComponent(Component component, String componentType, Template template) throws Exception {
        VelocityContext model = getCommonModel(component.getName(), 2);
        File compFile = getComponentFile(directory, component);
        model.put("component", component);
        model.put("componentType", componentType);
        compFile.getParentFile().mkdirs();
        renderVelocity(template, model, compFile);
        copyResources(compFile.getParentFile(), component.getDocResources());
        copyTestFiles(compFile.getParentFile(), component);
    }
    
    private List<Component> getRepositorySorted(List<Component> components) {
        Collections.sort(components, new Comparator<Component>() {
            @Override
            public int compare(Component o1, Component o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });
        return components;
    }

}
