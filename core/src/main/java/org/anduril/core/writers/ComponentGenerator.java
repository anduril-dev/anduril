package org.anduril.core.writers;

import org.anduril.core.network.*;
import org.anduril.core.network.launcher.Launcher;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;

public class ComponentGenerator extends AbstractGenerator {
    private final static String[] IMPORTS = new String[] {
            "java.io.File",
            "org.anduril.core.network.ArrayStatus",
            "org.anduril.core.network.{Component => NComponent}",
            "org.anduril.core.network.DataType",
            "org.anduril.core.network.InPort",
            "org.anduril.core.network.OutPort",
            "org.anduril.core.network.Parameter",
            "org.anduril.core.network.ParameterType",
            "org.anduril.core.network.Repository",
            "org.anduril.core.network.Requirement",
            "org.anduril.core.network.RequirementResource",
            "org.anduril.core.network.TypeParameter",
            "org.anduril.core.network.launcher.Launcher",
            "org.anduril.runtime.Component",
            "org.anduril.runtime.IgnoreName",
            "org.anduril.runtime.Port",
            "scala.collection.Map",
    };

    private final static String[] SCALA_KEYWORDS_ARRAY = new String[] {
            "abstract", "case", "catch", "class", "def", "do", "else", "extends",
            "false", "final", "finally", "for", "forSome", "if", "implicit", "import",
            "lazy", "match", "new", "null", "object", "override", "package", "private", "protected",
            "return", "sealed", "super", "this", "throw", "trait", "true", "try", "type",
            "val", "var", "while", "with", "yield"};
    private final static HashSet<String> SCALA_KEYWORDS = new HashSet<String>();
    static { Collections.addAll(SCALA_KEYWORDS, SCALA_KEYWORDS_ARRAY); }

    private Component component;
    private Bundle bundle;
    private Repository repository;

    public ComponentGenerator(Component component, Bundle bundle, Repository repository) {
        this.component = component;
        this.bundle = bundle;
        this.repository = repository;
    }

    public void generateComponentScalaFile(File destination) {
        generateHeader();
        generateClassHeader();
        generateClassContent();
        generateClassFooter();
        generateObjectDefinition();
        writeToFile(destination);
    }

    public Component getComponent() {
        return this.component;
    }

    // Helper methods //////////////////////////////////////////////////

    private String parameterTypeToScalaType(ParameterType type) {
        if (type == ParameterType.BOOLEAN) return "Boolean";
        else if (type == ParameterType.FLOAT) return "Double";
        else if (type == ParameterType.INT) return "Int";
        else if (type == ParameterType.STRING) return "String";
        else throw new IllegalArgumentException("Invalid parameter type: "+type);
    }

    /** Return an expression that produces a port type object from type name.
     *  This is either done using generics or a "repository" object. */
    private String getPortTypeBuilder(DataType type, boolean isGeneric) {
        if (isGeneric) {
            return String.format("DataType.buildGenericType(\"%s\")", type.getName());
        } else {
            return String.format("repository.getDataType(\"%s\")", type.getName());
        }
    }

    protected String getSafeParameterName(String paramName, boolean checkOutPort) {
        boolean needEscape =
                SCALA_KEYWORDS.contains(paramName) ||
                        (checkOutPort && this.component.getOutPort(paramName) != null);
        if (needEscape) {
            return "_" + paramName;
        } else {
            return paramName;
        }
    }

    protected String getSafeParameterName(String paramName) {
        return getSafeParameterName(paramName, true);
    }

    protected String formatPortType(Port port) {
        if (port.getType().isGeneric()) {
            return "Port";
        } else {
            return port.getType().getName();
        }

    }

    // Top-level generators ////////////////////////////////////////////

    /** Generate package declaration and imports. */
    protected void generateHeader() {
        writeLineFormat("package %s", this.bundle.getCompilePackageName());
        writeNewline();

        for (String importSpec: IMPORTS) {
            writeLineFormat("import %s", importSpec);
        }

        for (Bundle dependBundle: this.bundle.getDependsBundles(this.repository)) {
            writeLineFormat("import %s._",
                    dependBundle.getCompilePackageName());
        }

        writeNewline();
    }

    /** Generate class constructor and extends&with declarations, up to
     * and including the starting "{". */
    protected void generateClassHeader() {
        writeFormat("class %s", this.component.getName());

        // Constructor
        write(" @IgnoreName()");
        write(" (");
        writeNewline();
        increaseIndent();
        generateConstructorArgs();
        decreaseIndent();
        write(")");

        // Extends Component
        writeNewline();
        String namespace = this.component.getBundle().getCompilePackageName();
        writeFormat("extends Component(\"%s\", \"%s\", new File(\"%s\"))",
                namespace + "." + this.component.getName(),
                this.component.getVersion(),
                this.component.getMainDirectory().getAbsolutePath());

        // With ProductN
        if (!this.component.getOutPorts().isEmpty()) {
            writeNewline();
            writeFormat("with Product%d", this.component.getOutPorts().size());
            write("[");
            boolean first = true;
            for (OutPort port: this.component.getOutPorts()) {
                if (!first) write(", ");
                write(formatPortType(port));
                first = false;
            }
            write("]");
        }

        writeNewline();
        write("{");
        writeNewline();
        increaseIndent();
    }

    protected void generateClassContent() {
        generateClassMapConstructor();
        writeNewline();
        generateClassConstructor();
        writeNewline();
        generateFillComponentMethod();
        writeNewline();
        generateLoadDataTypesMethod();
    }

    protected void generateClassFooter() {
        decreaseIndent();
        writeNewline();
        writeLine("}");
    }

    protected void generateObjectDefinition() {
        if (this.component.getDoc() != null) {
            writeLineFormat("/** %s */", escapeDocString(this.component.getDoc()));
        }
        writeLineFormat("object %s {", this.component.getName());
        increaseIndent();

        // apply(arg1, arg2, ...) method
        writeLine("@IgnoreName");
        writeIndent();
        write("def apply");
        write("(");
        writeNewline();
        increaseIndent();
        generateConstructorArgs();
        decreaseIndent();
        write(") = { ");
        writeNewline();

        writeLineFormat("new %s(", this.component.getName());
        increaseIndent();
        generateCallArgs(true, false);
        write(")");
        writeNewline();
        decreaseIndent();
        writeLine("}");

        // apply(map) method
        writeNewline();
        writeLine("@IgnoreName");
        writeIndent();
        write("def apply");
        write("(args: Map[String,Any]) = {");
        writeNewline();
        increaseIndent();
        writeLineFormat("new %s(args)", this.component.getName());
        decreaseIndent();
        writeLine("}");

        decreaseIndent();
        writeLine("}");
    }

    // Helper generators ///////////////////////////////////////////////

    protected String formatParameterDefaultValue(Parameter param) {
        if (param.getDefaultValue() != null) {
            if (param.getType() == ParameterType.STRING) {
                return String.format("\"\"\"%s\"\"\"", param.getDefaultValue());
            } else if (param.getType() == ParameterType.FLOAT) {
                return String.format("%sD", param.getDefaultValue());
            } else {
                return param.getDefaultValue() ;
            }
        } else {
            return null;
        }
    }

    /** Generate class constructor arguments: "port1: T1, ..., param1: t1, ...". */
    protected void generateConstructorArgs() {
        boolean first = true;

        for (InPort port: this.component.getInPorts()) {
            if (!first) {
                write(", ");
                writeNewline();
            }
            writeIndent();
            writeFormat("%s: %s", getSafeParameterName(port.getName()), formatPortType(port));

            if (port.isOptional()) write(" = null");
            first = false;
        }

        for (Parameter param: this.component.getParametersInsertOrder()) {
            if (!first) {
                write(", ");
                writeNewline();
            }
            writeIndent();
            writeFormat("%s: %s", getSafeParameterName(param.getName()), parameterTypeToScalaType(param.getType()));
            if (param.getDefaultValue() != null) {
                writeFormat(" = %s", formatParameterDefaultValue(param));
            }
            first = false;
        }

        if (!first) {
            write(", ");
            writeNewline();
        }
        writeIndent();
        writeFormat("_name: String = null");
    }

    protected void generateFillComponentMethod() {
        writeLine("protected override def _fillComponentInterface(component: NComponent, repository: Repository): Unit = {");
        increaseIndent();

        writeLineFormat("component.setBundle(repository.getBundle(%s))",
                getStringExpression(component.getBundle().getName()));
        writeLineFormat("component.setInstanceClass(\"%s\", repository)",
                this.component.getInstanceClass().getCanonicalName());

        // Type parameters
        for (TypeParameter typeParam: this.component.getTypeParameters()) {
            writeIndent();
            writeFormat(
                    "component.addTypeParameter(new TypeParameter(\"%s\", %d, ",
                    typeParam.getName(),
                    typeParam.getPosition());
            if (typeParam.getExtendsType() == null) {
                write("null");
            } else {
                writeFormat("repository.getDataType(\"%s\")", typeParam.getExtendsType().getName());
            }
            write("))");
            writeNewline();
        }
        writeNewline();

        // In-ports
        for (InPort port: this.component.getInPorts()) {
            writeIndent();
            writeFormat(
                    "component.addPort(new InPort(component, \"%s\", %d, \"\"\"%s\"\"\", ArrayStatus.%s, %s, %s))",
                    port.getName(),
                    port.getPosition(),
                    escapeJavaString(port.getDoc()),
                    port.isArray(),
                    getPortTypeBuilder(port.getType(), port.isGeneric()),
                    port.isOptional());
            writeNewline();
        }
        writeNewline();

        // Out-ports
        for (OutPort port: this.component.getOutPorts()) {
            writeIndent();
            writeFormat(
                    "component.addPort(new OutPort(component, \"%s\", %d, \"\"\"%s\"\"\", ArrayStatus.%s, %s))",
                    port.getName(),
                    port.getPosition(),
                    escapeJavaString(port.getDoc()),
                    port.isArray(),
                    getPortTypeBuilder(port.getType(), port.isGeneric()));
            writeNewline();
        }
        writeNewline();

        // Parameters
        for (Parameter param: this.component.getParametersInsertOrder()) {
            writeIndent();
            writeFormat(
                    "component.addParameter(new Parameter(\"%s\", ParameterType.fromString(\"%s\"), \"\"\"%s\"\"\", ",
                    param.getName(),
                    param.getType(),
                    escapeJavaString(param.getDoc()));
            if (param.getDefaultValue() == null) {
                write("null");
            } else {
                writeFormat("\"\"\"%s\"\"\"", param.getDefaultValue());
            }
            write("))");
            writeNewline();
        }
        writeNewline();

        // Launchers
        Launcher launcher = component.getLauncher();
        if (launcher != null) {
            writeIndent();
            write("val launcherArgs = new java.util.HashMap[String, String]()");
            writeNewline();

            for (String key: launcher.getArgs().keySet()) {
                final String value = launcher.getArgument(key);
                writeIndent();
                writeFormat("launcherArgs.put(\"%s\", \"\"\"%s\"\"\")", key, value);
                writeNewline();
            }

            writeIndent();
            writeFormat("component.addLauncher(_makeLauncher(\"%s\", launcherArgs, component))",
                    launcher.getType());
            writeNewline();
        }

        // Requirements
        if (!component.getRequires().isEmpty()) {
            writeNewline();
            writeLine("var req: Requirement = null");
            for (Requirement req: component.getRequires()) {
                writeLineFormat("req = new Requirement(%s, %s, %s, %s)",
                       getStringExpression(req.getName()),
                       getStringExpression(req.getURL()),
                       getStringExpression(req.getVersionNumber()),
                       req.getOptional());
                for (RequirementResource resource: req.getResources()) {
                    writeLineFormat("req.addResource(new RequirementResource(req, %s, %s))",
                            getStringExpression(resource.getType()),
                            getStringExpression(resource.getContents()));
                }
                writeLine("component.addRequires(req)");
            }
        }

        decreaseIndent();
        writeLine("}");
    }

    protected void generateLoadDataTypesMethod() {
        writeLine("protected override def _loadBundleDataTypes(): Unit = {");
        increaseIndent();
        writeLineFormat("%s.types.DataTypeLoader.load()", this.bundle.getCompilePackageName());
        decreaseIndent();
        writeLine("}");
    }

    /** Write constructor statements for connecting in-ports and setting parameters,
     *  and field definitions. */
    protected void generateClassConstructor() {
        writeLine("override val _componentInstance = _makeComponentInstance(_name)");
        writeLine("private val _ci = _componentInstance");

        for (InPort port: this.component.getInPorts()) {
            writeIndent();
            if (port.isOptional()) {
                writeFormat("if (%s != null) ", getSafeParameterName(port.getName()));
            }
            writeFormat("%s.componentInstance.connectTo(_ci, %s.networkPort, _ci.getComponent().getInPort(\"%s\"), %s.isForced)",
                    getSafeParameterName(port.getName()),
                    getSafeParameterName(port.getName()),
                    port.getName(),
                    getSafeParameterName(port.getName()));
            writeNewline();
        }

        for (Parameter param: this.component.getParametersInsertOrder()) {
            writeLineFormat("_ci.setParameterValue(\"%s\", %s)", param.getName(), getSafeParameterName(param.getName()));
        }

        writeLine("_addComponentInstance(_ci)");
        writeNewline();

        for (OutPort port: this.component.getOutPorts()) {
            if (port.getPosition() > 1) {
                writeNewline();
            }

            if (port.getDoc() != null) {
                writeLineFormat("/** %s */", escapeDocString(port.getDoc()));
            }

            writeLineFormat("val %s = new %s(\"%s\", this, _ci)",
                    port.getName(),
                    formatPortType(port),
                    port.getName());
            writeLineFormat("_addOutPort(\"%s\", %s)", port.getName(), port.getName());
            if (port.getPosition() < 22) {
                writeLineFormat("val _%d = %s", port.getPosition(), port.getName());
            }
        }
    }

    /** Generate a secondary constructor that takes arguments as a map. */
    protected void generateClassMapConstructor() {
        writeLine("@IgnoreName");
        writeLine("def this(args: Map[String,Any]) = {");
        increaseIndent();
        writeLine("this(");
        increaseIndent();

        boolean first = true;
        for (InPort port: this.component.getInPorts()) {
            if (!first) {
                write(",");
                writeNewline();
            }
            writeIndent();
            writeFormat("args.getOrElse(\"%s\", null).asInstanceOf[%s]",
                    port.getName(),
                    formatPortType(port));
            first = false;
        }

        for (Parameter param: this.component.getParametersInsertOrder()) {
            if (!first) {
                write(",");
                writeNewline();
            }
            writeIndent();
            if (param.getDefaultValue() == null) {
                writeFormat("args(\"%s\").asInstanceOf[%s]",
                        getSafeParameterName(param.getName()),
                        parameterTypeToScalaType(param.getType()));

            } else {
                writeFormat("args.getOrElse(\"%s\", %s).asInstanceOf[%s]",
                        getSafeParameterName(param.getName()),
                        formatParameterDefaultValue(param),
                        parameterTypeToScalaType(param.getType()));
            }
            first = false;
        }

        if (!first) {
            write(", ");
            writeNewline();
        }
        writeIndent();
        writeFormat("args.getOrElse(\"_name\", null).asInstanceOf[String]");

        write(")");
        writeNewline();
        decreaseIndent();
        decreaseIndent();
        writeLine("}");
    }

    protected void generateCallArgs(boolean includeNameAnnotation, boolean includeArgNames) {
        boolean first = true;

        for (InPort port: this.component.getInPorts()) {
            if (!first) {
                write(",");
                writeNewline();
            }
            writeIndent();
            if (includeArgNames) {
                writeFormat("%s = %s",
                        getSafeParameterName(port.getName(), false),
                        getSafeParameterName(port.getName(), true));
            } else {
                writeFormat("%s",
                        getSafeParameterName(port.getName(), true));

            }
            first = false;
        }

        for (Parameter param: this.component.getParametersInsertOrder()) {
            if (!first) {
                write(",");
                writeNewline();
            }
            writeIndent();
            if (includeArgNames) {
                writeFormat("%s = %s",
                        getSafeParameterName(param.getName(), false),
                        getSafeParameterName(param.getName(), true));
            } else {
                writeFormat("%s",
                        getSafeParameterName(param.getName(), true));

            }
            first = false;
        }

        if (includeNameAnnotation) {
            if (!first) {
                write(", ");
                writeNewline();
            }
            writeIndent();
            writeFormat("_name");
        }
    }
}
