package org.anduril.core.writers;

import org.anduril.core.network.*;
import org.anduril.core.utils.IOTools;

import java.io.File;
import java.io.IOException;

public class LibFunctionGenerator extends ComponentGenerator {
    public LibFunctionGenerator(Component component, Bundle bundle, Repository repository) {
        super(component, bundle, repository);
    }

    @Override
    protected void generateClassContent() {
        generateClassMapConstructor();
        writeNewline();

        File functionSource = getComponent().getBundle().getNetworkScalaSource(getComponent());
        writeLineFormat("private object Implementation {");
        increaseIndent();
        writeLineFormat("/* %s */", functionSource.getAbsolutePath());
        writeLineFormat("val _functionFolder = \"%s%s\"", functionSource.getParentFile().getAbsolutePath(), File.separator);

        try {
            String[] lines = IOTools.readLines(functionSource.getAbsolutePath());
            for (String line: lines) {
                writeLine(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("Error reading "+functionSource.getAbsolutePath(), e);
        }
        decreaseIndent();
        writeLine("}");
        writeNewline();

        writeLine("override val _componentInstance = null");
        writeLineFormat("private val _result = Implementation.%s(", getComponent().getName());
        increaseIndent();
        //generateCallArgs(false, true);
        generateCallArgs(false, false);
        decreaseIndent();
        write(")");
        writeNewline();
        writeNewline();

        for (OutPort port: getComponent().getOutPorts()) {
            if (port.getPosition() > 1) {
                writeNewline();
            }

            if (port.getDoc() != null) {
                writeLineFormat("/** %s */", escapeDocString(port.getDoc()));
            }

            writeLineFormat("val %s = _extractFunctionResult(_result, \"%s\", %d)",
                    port.getName(),
                    port.getName(),
                    port.getPosition());
            writeLineFormat("_addOutPort(\"%s\", %s)", port.getName(), port.getName());
            if (port.getPosition() < 22) {
                writeLineFormat("val _%d = %s", port.getPosition(), port.getName());
            }
        }

        writeNewline();
        generateFillComponentMethod();
        writeNewline();
        generateLoadDataTypesMethod();
    }
}
