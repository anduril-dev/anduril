package org.anduril.core.writers;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AbstractGenerator {
    private static final String INDENT = "    ";

    /** Builder that holds the nascent Scala source code.
     * Writer methods append to this builder. */
    private StringBuilder builder = new StringBuilder();

    private int indentLevel = 0;

    protected void increaseIndent() {
        this.indentLevel++;
    }

    protected void decreaseIndent() {
        if (this.indentLevel <= 0) {
            throw new IllegalStateException("Indent level too low (< 0)");
        }
        this.indentLevel--;
    }

    protected void write(String code) {
        this.builder.append(code);
    }

    protected void writeIndent() {
        for (int i=0; i<this.indentLevel; i++) this.builder.append(INDENT);
    }

    protected void writeNewline() {
        this.builder.append('\n');
    }

    protected void writeFormat(String format, Object... args) {
        write(String.format(format, args));
    }

    protected void writeLine(String line) {
        writeIndent();
        write(line);
        writeNewline();
    }

    protected void writeLineFormat(String format, Object... args) {
        writeLine(String.format(format, args));
    }

    protected String escapeJavaString(String stringLiteral) {
        return StringEscapeUtils.escapeJava(stringLiteral);
    }

    protected String escapeDocString(String doc) {
        return doc.replace("*/", "*&#47;");
    }

    protected String getStringExpression(String str) {
        if (str == null) return "null";
        else return String.format("\"\"\"%s\"\"\"", str);
    }

    public String toString() {
        return this.builder.toString();
    }

    public void writeToFile(File destination) {
        BufferedWriter writer = null;
        try{
            writer = new BufferedWriter( new FileWriter(destination));
            writer.write(this.builder.toString());
        }
        catch ( IOException e){
            e.printStackTrace();
        }
        finally{
            try{
                if ( writer != null)
                    writer.close( );
            }catch ( IOException e){
                e.printStackTrace();
            }
        }
    }
}