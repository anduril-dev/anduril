package org.anduril.core.writers;

import org.anduril.core.commandline.BaseCommand;
import org.anduril.core.commandline.CompileBundleCommand;
import org.anduril.core.network.Bundle;
import org.anduril.core.network.Repository;

import java.io.File;
import java.util.List;

public class WorkflowSBTGenerator extends AbstractGenerator {

    final String projectName;
    final List<Bundle> bundles;

    public WorkflowSBTGenerator(String projectName, List<Bundle> bundles) {
        this.projectName = projectName;
        this.bundles = bundles;
    }

    public void generateSBT(File destination) {
        writeLineFormat("name := \"%s\"", this.projectName);
        writeNewline();

        writeLineFormat("scalaVersion := \"%s\"", BundleSBTGenerator.SCALA_VERSION);
        writeNewline();

        writeLineFormat("scalaSource in Compile := baseDirectory.value");
        writeNewline();

        writeLine("scalacOptions ++= Seq( \"-unchecked\", \"-deprecation\" )");
        writeNewline();

        writeLineFormat("unmanagedClasspath in Compile += file(System.getenv(\"%s\")) / \"%s\"",
                BaseCommand.HOME_ENVIRONMENT, Repository.ENGINE_JAR);
        writeNewline();

        for (Bundle bundle: this.bundles) {
            writeLineFormat("unmanagedClasspath in Compile += file(\"%s\")",
                    bundle.getBundleJAR());
            writeNewline();

            for (String imports: bundle.getImportsJARs()) {
                writeLineFormat("unmanagedClasspath in Compile += file(\"%s\")", imports);
                writeNewline();
            }
        }

        writeLine("crossTarget := baseDirectory.value");
        writeNewline();

        writeLine("artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>");
        increaseIndent();
        writeLine("artifact.name + \".\" + artifact.extension");
        decreaseIndent();
        writeLine("}");

        writeToFile(destination);
    }

}
