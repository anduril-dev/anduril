package org.anduril.core.writers;

import org.anduril.core.network.*;
import org.anduril.core.utils.HTMLTools;
import org.anduril.core.utils.TextTools;

import org.apache.commons.lang.StringEscapeUtils;
import java.io.File;
import java.util.*;

public class VelocityFormatter {

    static public final String HTML_MISSING_RESOURCE = "<i>(missing)</i>";

    private final Repository repository;
    
    public VelocityFormatter(Repository repository) {
        this.repository = repository;
    }
    
    public Repository getRepository() {
        return repository;
    }
    
    public String join(String[] strings, String sep) {
        return TextTools.join(strings, sep);
    }
    
    public String join(List<String> strings, String sep) {
        return TextTools.join(strings, sep);
    }

    public String joinSerial(List<String> strings, String sep) {
        LinkedList<String> l = new LinkedList<String>();
        ArrayList<String>  s = new ArrayList<String>();
        String             body;
        int                colSize;

        for (String t : strings) {
            t = t.trim();
            int i;
            for (i=t.length()-1; (i >= 0) && Character.isDigit(t.charAt(i)); i--);
            if (i >= 0 && i < t.length()-1) {
                body = t.substring(0,i+1);
                if (s.isEmpty() || s.get(0).startsWith(body)) {
                    s.add(t);
                    continue;
                }
            }
            colSize = s.size();
            if (colSize > 0) {
               l.add(s.get(0));
               if (colSize == 3) {
                   l.add(s.get(1));
               } else if (colSize > 3) {
                   l.add("...");
               }
               if (colSize > 2) l.add(s.get(colSize-1));
               s.clear();
            }
            l.add(t);
        }
        colSize = s.size();
        if (colSize > 0) {
           l.add(s.get(0));
           if (colSize == 3) {
              l.add(s.get(1));
           } else if (colSize > 3) {
              l.add("...");
           }
           if (colSize > 2) l.add(s.get(colSize-1));
        }
        return TextTools.join(l, sep);
    }

    public String formatCredits(List<Author> authors, String sep, String pathPrefix) {
        return formatAuthors(authors, sep, pathPrefix);
    }

    public String formatAuthors(List<Author> authors, String sep, String pathPrefix) {
        if (authors == null) return "";
        StringBuilder sb = new StringBuilder(2048);
        for (int i=0; i<authors.size(); i++) {
            Author author = authors.get(i);
            if (i > 0) sb.append(sep);
            if (author.getEmail() == null)
                sb.append(author.getHtmlName());
            else
                sb.append("<a href=\"").append(pathPrefix).append(makeAuthorLink(author.getHtmlName())).append("\">")
                  .append(author.getHtmlName())
                  .append("</a>");
            if (author.getEmail() != null) {
                sb.append(String.format(" (<a href=\"mailto:%s\">%s</a>)",
                        author.getEmail(), author.getEmail()));
            }
        }
        
        return sb.toString();
    }

    public String formatIssues(String issues, String component) {
        if ( issues == "" ) { return ""; }
        return String.format("<a href=\"%s\" target=_BLANK>View/Report issues</a>", issues)
            .replace("@COMPONENT@",component);
    }
    
    public String makeDataTypeLink(int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
            + ComponentDocWriter.TYPE_SUMMARY_FILE;
    }
    
    public String makeDataTypeLink(String typeName, int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
            + ComponentDocWriter.getTypeFile(null, typeName).getPath();
    }
    
    public String formatPortType(Port port, int depth) {
        String typeDisplay;
        
        ArrayStatus isArray = port.isArray();
        final String typeName = port.getType().getName();
        if (isArray.isTrue()) {
            typeDisplay = String.format("Array&lt;%s&gt;", typeName);
        } else if (isArray.isGeneric()) {
            typeDisplay = String.format("%s <i>or</i> Array&lt;%s&gt;", typeName, typeName);
        } else {
            typeDisplay = typeName;
        }
        
        if (port.getType().isGeneric()) {
            typeDisplay = String.format("%s <i>(generic)</i>", typeDisplay);
        } else {
            typeDisplay = String.format("<a href=\"%s\">%s</a>",
                    makeDataTypeLink(typeName, depth), typeDisplay);
        }
        
        if (port.isStreamble()) {
            typeDisplay += " <i>(stream)</i>";
        }
        
        return typeDisplay;
    }
    
    public String makeCategoryLink(int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
            + ComponentDocWriter.CATEGORY_SUMMARY_FILE;
    }
    
    public String makeCategoryLink(String categoryName, int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
            + ComponentDocWriter.getCategoryFile(null, categoryName).getPath();
    }
    
    public List<Category> makeCategoryTree() {
        return repository.getCategoryTree();
    }

    public String makeAuthorLink(String authorName) {
        return ComponentDocWriter.getAuthorFile(null, authorName).getPath();
    }
    
    public String makeComponentLink(int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
            + ComponentDocWriter.COMPONENT_SUMMARY_FILE;
    }
    
    public String makeComponentLink(String componentName, int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
        + ComponentDocWriter.getComponentFile(null, componentName).getPath();
    }
        
    public String formatParagraph(String s) {
        for (String newline: new String[] {"\r\n", "\n", "\r"}) {
            s = s.replaceAll(newline+"\\s*"+newline, newline+"</p><p>"+newline);
        }
        return s;
    }
    
    public List<Component> getDataTypeMembers(DataType type, boolean includeInput, boolean includeOutput) {
        List<Component> list = repository.getComponents();
        list.addAll(repository.getFunctions());
        Collections.sort(list, new Comparator<Component>() {
            @Override
            public int compare(Component c0, Component c1) {
                return c0.getName().compareTo(c1.getName());
            }
        });
        return repository.getDataTypeMembers(type, includeInput, includeOutput, list);
    }

    public Map<String,List<Component>> getCategories() {
        return repository.getCategories();
    }

    public List<Component> getCategoryComponents(String category) {
        List<Component> categories = repository.getComponentsByCategory().get(category);
        if (categories == null) {
            categories = new ArrayList<Component>();
        } else {
            Collections.sort(categories, new Comparator<Component>() {
                @Override
                public int compare(Component o1, Component o2) {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
        }
        return categories;
    }
    
    public List<Component> getAllCategoryComponents(String category) {
        return repository.getAllCategoryComponents(category);
    }
    
    public List<Category> getCategoryChildren(String aCatName) {
        return repository.getCategoryChildren(aCatName);
    }

    public List<Component> getAuthorComponents(String author) {
        List<Component> authors = repository.getComponentsByAuthor().get(author);
        if (authors == null) {
            authors = new ArrayList<Component>();
        } else {
            Collections.sort(authors, new Comparator<Component>() {
                @Override
                public int compare(Component o1, Component o2) {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
        }
        return authors;
    }
    
    public List<Author> getAuthors() {
        return repository.getAuthors();
    }
    
    /**
     * Constructs an HTML link for the test case specific parameter file.
     */
  /*  public String testCaseParametersLink(ComponentTestCase testCase) {
        File propertiesFile = testCase.getParameterFile();
        if (propertiesFile == null) return HTML_MISSING_RESOURCE;

        String href = String.format("%s/%s/%s",
                                    ComponentDocWriter.TEST_CASE_DIRECTORY,
                                    testCase.getCaseName(),
                                    testCase.getEnvNameProps());
        return String.format("<a href=\"%s\">properties</a>", href);
    }
*/

        
    public String testCaseLink(ComponentTestCase testCase, InPort port) {
        File testFile = testCase.getInputFile(port);
        if (testFile == null || !testFile.exists()) return HTML_MISSING_RESOURCE;

        String href = String.format("%s/%s/%s/%s",
                ComponentDocWriter.TEST_CASE_DIRECTORY,
                testCase.getCaseName(),
                ComponentDocWriter.TEST_CASE_INPUT_DIRECTORY,
                testFile.getName());
        return String.format("<a href=\"%s\">%s</a>", href, port.getName());
    }

    public String testCaseLink(ComponentTestCase testCase, OutPort port) {
        if (!testCase.expectSuccess()) return "<i>(expecting failure)</i>";
        File testFile = testCase.getOutputFile(port);
        if (testFile == null || !testFile.exists()) return HTML_MISSING_RESOURCE;
        
        String href = String.format("%s/%s/%s/%s",
                ComponentDocWriter.TEST_CASE_DIRECTORY,
                testCase.getCaseName(),
                ComponentDocWriter.TEST_CASE_OUTPUT_DIRECTORY,
                testFile.getName());
        /*
        if (port.getType().getExtension() != null) {
            href += "." + port.getType().getExtension();
        }
        */
        return String.format("<a href=\"%s\">%s</a>", href, port.getName());
    }
    
    /**
     * Returns HTML formatted test case parameters
     */
    public String testCaseParameters(ComponentTestCase testCase) {
        File propertiesFile = testCase.getParameterFile();
        StringBuilder buf = new StringBuilder();

        try {
            Scanner scanner = new Scanner(propertiesFile);
 
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                line = line.replace("component.", "");
                line = line.replace("=", "</strong>=");
                buf.append(String.format("<strong>%s,<br/>", line));
            }
            
            scanner.close();
        } catch (Exception e) {
        }
        String str = buf.toString();
        
        if(str.length() > 6)
            return str.substring(0, str.length()-6);
        else
            return str;
    }
    /**
     * Returns HTML formatted test network
     */
    public String testNetwork(Component comp) {
        File networkFile = comp.getTestNetwork();
        if (networkFile == null) return "";
        StringBuffer buf = new StringBuffer();

        //buf.append("<code>");
        try {
            Scanner scanner = new Scanner(networkFile);
 
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                line = line.replaceAll("(?i)</pre>", "<|pre>");
                if(line.length() == 0) line+=" ";
                line = StringEscapeUtils.escapeHtml(line);
                buf.append(line+"\n");
            }
            
            scanner.close();
        } catch (Exception e) {
        }
        //buf.append("</code>");
        String str = buf.toString();
        
        return str;
    }
    
    public String formatRequirement(Requirement req) {
        StringBuffer sb = new StringBuffer();
        
        String name = req.getDisplayName();
        if (req.getURL() != null) {
            sb.append(String.format("<a href=\"%s\" target=\"_top\">%s</a>", req.getURL(), name));
        } else {
            sb.append(name);
        }
        
        if (req.getVersionNumber() != null) {
            String relation = req.getVersionRelation();
            relation = relation.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            sb.append(String.format(" %s %s",
                    relation, req.getVersionNumber()));
        }
        
        List<RequirementResource> resources = req.getResources();
        if (resources.size() == 1) {
            if (!resources.get(0).getType().equals(Requirement.REQ_MANUAL)) {
                sb.append(String.format(" (%s)", resources.get(0).getType()));
            }
        } else {
            sb.append(" (");
            for (int i=0; i<resources.size(); i++) {
                RequirementResource resource = resources.get(i);
                if (i > 0) sb.append(" | ");
                sb.append(String.format("%s:%s", resource.getType(), resource.getContents()));
            }
            sb.append(')');
        }
        
        return sb.toString();
    }
}
