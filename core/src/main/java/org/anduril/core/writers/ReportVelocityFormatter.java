package org.anduril.core.writers;

import org.anduril.core.network.*;
import org.anduril.core.readers.TestReportItem;
import org.anduril.core.readers.TestReportReader;
import org.anduril.core.utils.HTMLTools;
import org.anduril.core.utils.TextTools;

import java.io.File;
import java.util.List;

public class ReportVelocityFormatter {

    static public final String HTML_MISSING_RESOURCE = "<i>(missing)</i>";

    private final TestReportReader repository;
    
    public ReportVelocityFormatter(TestReportReader repository) {
        this.repository = repository;
    }
    
    public TestReportReader getRepository() {
        return repository;
    }
    
    public String join(String[] strings, String sep) {
        return TextTools.join(strings, sep);
    }
    
    public String join(List<String> strings, String sep) {
        return join(strings.toArray(new String[strings.size()]), sep);
    }
    
    public String formatCredits(List<Author> authors, String sep) {
    	return formatAuthors(authors, sep);
    }
    
    public String formatAuthors(List<Author> authors, String sep) {
        if (authors == null) return "";
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<authors.size(); i++) {
            Author author = authors.get(i);
            if (i > 0) sb.append(sep);
            sb.append(author.getHtmlName());
            if (author.getEmail() != null) {
                sb.append(String.format(" (<a href=\"mailto:%s\">%s</a>)",
                        author.getEmail(), author.getEmail()));
            }
        }
        
        return sb.toString();
    }
    
    public String makeComponentLink(int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
            + ComponentDocWriter.COMPONENT_SUMMARY_FILE;
    }
    
    public String makeComponentLink(String componentName, int fromDepth) {
        return HTMLTools.descendDirectories(fromDepth)
        + ComponentDocWriter.getComponentFile(null, componentName).getPath();
    }
        
    public String formatParagraph(String s) {
        for (String newline: new String[] {"\r\n", "\n", "\r"}) {
            s = s.replaceAll(newline+"\\s*"+newline, newline+"</p><p>"+newline);
        }
        return s;
    }
    
    public List<Component> getTestlessComponents() {
    	return repository.getTestlessComponents();
    }
    
    public List<TestReportItem> getComponentTests() {
    	return repository.getReportList();
    }
    
    public List<TestReportItem> getFailedReportList() {
    	return repository.getFailedReportList();
    }
    
    public String getStartDate() {
    	return repository.getStartDate();
    }
    
    public String getStartTime() {
    	return repository.getStartTime();
    }

    public String getEndDate() {
    	return repository.getEndDate();
    }
    
    public String getEndTime() {
    	return repository.getEndTime();
    }
    
    public String getElaspedSecsSum() {
    	return repository.getElaspedSecsSum();
    }
    
    public String getElaspedSecsTestCasesMean() {
    	return repository.getElaspedSecsTestCasesMean();
    }
    
    public String getFractionPassedComps() {
        return repository.getFractionPassedComps();
    }
    
    public String getFractionPassedTests() {
        return repository.getFractionPassedTests();
    }

    public String getFractionInactiveTests() {
        return repository.getFractionInactiveTests();
    }
    
    public String getMeanInACI() {
        return repository.getMeanInACI();
    }

    public String getMeanOutACI() {
        return repository.getMeanOutACI();
    }

    public String getMeanPCover() {
        return repository.getMeanPCover();
    }
    
    /**
     * Constructs an HTML link for the test case specific parameter file.
     */
    public String testCaseParametersLink(ComponentTestCase testCase) {
        File propertiesFile = testCase.getParameterFile();
        if (propertiesFile == null) return HTML_MISSING_RESOURCE;

        String href = String.format("%s/%s/%s",
                                    ComponentDocWriter.TEST_CASE_DIRECTORY,
                                    testCase.getCaseName(),
                                    testCase.getEnvNameProps());
        return String.format("<a href=\"%s\">properties</a>", href);
	}
	

    public String testCaseLink(ComponentTestCase testCase, InPort port) {
        File testFile = testCase.getInputFile(port);
        if (testFile == null || !testFile.exists()) return HTML_MISSING_RESOURCE;

        String href = String.format("%s/%s/%s/%s",
                ComponentDocWriter.TEST_CASE_DIRECTORY,
                testCase.getCaseName(),
                ComponentDocWriter.TEST_CASE_INPUT_DIRECTORY,
                port.getName());
        return String.format("<a href=\"%s\">%s</a>", href, port.getName());
    }

    public String testCaseLink(ComponentTestCase testCase, OutPort port) {
        if (!testCase.expectSuccess()) return "<i>(expecting failure)</i>";
        File testFile = testCase.getOutputFile(port);
        if (testFile == null || !testFile.exists()) return HTML_MISSING_RESOURCE;
        
        String href = String.format("%s/%s/%s/%s",
                ComponentDocWriter.TEST_CASE_DIRECTORY,
                testCase.getCaseName(),
                ComponentDocWriter.TEST_CASE_OUTPUT_DIRECTORY,
                port.getName());
        if (port.getType().getExtension(port.isArray().isTrue()) != null) {
            href += "." + port.getType().getExtension(port.isArray().isTrue());
        }
        return String.format("<a href=\"%s\">%s</a>", href, port.getName());
    }

}
