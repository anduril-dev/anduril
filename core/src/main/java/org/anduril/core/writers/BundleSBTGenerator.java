package org.anduril.core.writers;

import org.anduril.core.commandline.BaseCommand;
import org.anduril.core.commandline.CompileBundleCommand;
import org.anduril.core.network.Bundle;
import org.anduril.core.network.Repository;

import java.io.File;

public class BundleSBTGenerator extends AbstractGenerator {

    public static final String SCALA_VERSION = "2.11.7";

    final private Bundle bundle;
    final private Repository repository;

    public BundleSBTGenerator(Bundle bundle, Repository repository) {
        this.bundle = bundle;
        this.repository = repository;
    }

    public void generateSBT(File destination) {
        writeLineFormat("name := \"%s\"", this.bundle.getName());
        writeNewline();

        writeLineFormat("scalaVersion := \"%s\"", SCALA_VERSION);
        writeNewline();

        writeLineFormat("scalaSource in Compile := baseDirectory.value / \"%s\"", CompileBundleCommand.SOURCE_PREFIX);
        writeNewline();

        writeLine("sourcesInBase := false");
        writeNewline();

        writeLineFormat("javaSource in Compile := baseDirectory.value / \"%s\"", CompileBundleCommand.SOURCE_PREFIX);
        writeNewline();

        writeLineFormat("scalacOptions in (Compile, doc) ++= Opts.doc.title(\"%s bundle\")",
                this.bundle.getName());
        writeNewline();

        writeLine("scalacOptions ++= Seq( \"-unchecked\", \"-deprecation\" )");
        writeNewline();

        File jarDirectory = this.bundle.getLibDirectory("java");
        if (jarDirectory.exists()) {
            writeLineFormat("unmanagedBase := file(\"%s\")", jarDirectory.getAbsolutePath());
            writeNewline();
        }

        writeLineFormat("unmanagedClasspath in Compile += file(System.getenv(\"%s\")) / \"%s\"",
                BaseCommand.HOME_ENVIRONMENT, Repository.ENGINE_JAR);
        writeNewline();

        for (Bundle dependBundle: this.bundle.getDependsBundles(this.repository)) {
            writeLineFormat("unmanagedClasspath in Compile += file(\"%s\")",
                    dependBundle.getBundleJAR());
            writeNewline();
        }

        for (String imports: this.bundle.getImportsJARs()) {
            writeLineFormat("unmanagedClasspath in Compile += file(\"%s\")", imports);
            writeNewline();
        }

        writeLine("crossTarget := baseDirectory.value");
        writeNewline();

        writeLine("artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>");
        increaseIndent();
        writeLine("artifact.name + \".\" + artifact.extension");
        decreaseIndent();
        writeLine("}");

        writeToFile(destination);
    }
}
