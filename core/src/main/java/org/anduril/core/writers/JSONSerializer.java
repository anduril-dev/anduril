package org.anduril.core.writers;

import com.google.gson.*;
import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.network.launcher.Launcher;

import java.io.File;
import java.util.*;

public class JSONSerializer {

    private final Repository repository;
    private final Gson gson;
    private final boolean enableDataTypes;

    public JSONSerializer(Repository repository, Gson gson, boolean enableDataTypes) {
        this.repository = repository;
        this.gson = gson;
        this.enableDataTypes = enableDataTypes;
    }

    public JsonArray serializeAuthors(Collection<Author> authors) {
        final JsonArray result = new JsonArray();

        for (Author author: authors) {
            final JsonObject authorMap = new JsonObject();
            authorMap.addProperty("name", author.getName());
            authorMap.addProperty("email", author.getEmail());
            result.add(authorMap);
        }

        return result;
    }

    public JsonObject serializeBundle(Bundle bundle) {
        final JsonObject result = new JsonObject();

        result.addProperty("compilePackage", bundle.getCompilePackageName());
        result.add("path", serializeFile(bundle.getDirectory()));
        result.add("depends", serializeSimpleObject(bundle.getDependsBundleNames()));
        result.add("imports", serializeSimpleObject(bundle.getImportsJARs()));

        final List<Bundle> selectedBundle = Collections.singletonList(bundle);
        final List<Component> allComponents = new ArrayList<>();
        allComponents.addAll(this.repository.getComponents(selectedBundle));
        allComponents.addAll(this.repository.getFunctions(selectedBundle));
        result.add("components", serializeComponents(allComponents));

        if (this.enableDataTypes) {
            result.add("datatypes", serializeDataTypes(this.repository.getDataTypes(selectedBundle)));
        }

        return result;
    }

    public JsonObject serializeBundles(Collection<Bundle> bundles) {
        final JsonObject result = new JsonObject();

        for (Bundle bundle: bundles) {
            result.add(bundle.getName(), serializeBundle(bundle));
        }

        return result;
    }

    public JsonObject serializeComponents(Collection<Component> components) {
        final JsonObject result = new JsonObject();
        for (Component component: components) {
            result.add(component.getName(), serializeComponent(component));
        }
        return result;
    }

    public JsonObject serializeComponent(Component component) {
        final JsonObject result = new JsonObject();

        final String instanceClass;
        if (component.getInstanceClass() == null) {
            instanceClass = null;
        } else {
            instanceClass = component.getInstanceClass().getCanonicalName();
        }

        result.addProperty("name", component.getName());
        result.addProperty("version", component.getVersion());
        result.addProperty("doc", component.getDoc());
        result.addProperty("shortDoc", component.getShortDoc());
        result.addProperty("deprecated", component.getDeprecated());
        result.addProperty("instanceClass", instanceClass);
        result.add("author", serializeAuthors(component.getAuthors()));
        result.add("category", serializeSimpleObject(component.getCategories()));
        result.add("launcher", serializeLauncher(component.getLauncher()));
        result.add("requires", serializeRequirements(component.getRequires()));
        result.add("typeParameters", serializeTypeParameters(component.getTypeParameters()));
        result.add("sourceFiles", serializeFileList(component.getSourceFiles()));
        result.add("inputs", serializeInPorts(component.getInPorts()));
        result.add("outputs", serializeOutPorts(component.getOutPorts()));
        result.add("parameters", serializeParameters(component.getParameters()));
        result.add("testCases", serializeTestCases(component.getTestCases(), component));

        return result;
    }

    public JsonObject serializeDataTypes(Collection<DataType> dataTypes) {
        final JsonObject result = new JsonObject();

        for (DataType dataType: dataTypes) {
            final JsonObject dataTypeMap = new JsonObject();

            final String functionalityClass;
            if (dataType.getFunctionalityClass() == null) {
                functionalityClass = null;
            } else {
                functionalityClass = dataType.getFunctionalityClass().getCanonicalName();
            }

            final String parentType;
            final String parentTypeBundle;
            if (dataType.getParentType() == null) {
                parentType = null;
                parentTypeBundle = null;
            } else {
                parentType = dataType.getParentType().getName();
                if (dataType.getParentType().getBundle() == null) {
                    parentTypeBundle = null;
                } else {
                    parentTypeBundle = dataType.getParentType().getBundle().getName();
                }
            }

            dataTypeMap.addProperty("name", dataType.getName());
            dataTypeMap.addProperty("parentType", parentType);
            dataTypeMap.addProperty("parentTypeBundle", parentTypeBundle);
            dataTypeMap.addProperty("doc", dataType.getDoc());
            dataTypeMap.addProperty("shortDoc", dataType.getShortDoc());
            dataTypeMap.addProperty("presentationDesc", dataType.getPresentationDesc());
            dataTypeMap.addProperty("presentationType", dataType.getPresentationType().getType());
            dataTypeMap.addProperty("extension", dataType.getExtension(false));
            dataTypeMap.addProperty("functionalityClass", functionalityClass);
            dataTypeMap.add("exampleFiles", serializeFileList(dataType.getExampleFiles()));

            result.add(dataType.getName(), dataTypeMap);
        }

        return result;
    }

    public JsonElement serializeFile(File obj) {
        if (obj == null) {
            return JsonNull.INSTANCE;
        } else {
            return this.gson.toJsonTree(obj.getPath());
        }
    }

    public JsonElement serializeFileList(List<File> obj) {
        if (obj == null) {
            return JsonNull.INSTANCE;
        } else {
            final JsonArray result = new JsonArray();
            for (File file: obj) {
                result.add(serializeFile(file));
            }
            return result;
        }
    }

    public JsonArray serializeInPorts(Collection<InPort> inPorts) {
        final JsonArray result = new JsonArray();

        if (inPorts != null) {
            for (InPort port: inPorts) {
                if (!port.isSpecial()) {
                    JsonObject portMap = serializePort(port);
                    portMap.addProperty("optional", port.isOptional());
                    result.add(portMap);
                }
            }
        }

        return result;
    }

    public JsonObject serializePortConnection(PortConnection connection) {
        final JsonObject result = new JsonObject();
        // s = String.format("Input connection: %s.%s => %s.%s, type: %s => %s, force: %s",

        result.addProperty("fromComponent", connection.getFrom().getName());
        result.addProperty("fromPort", connection.getFromPort().getName());

        result.addProperty("toComponent", connection.getTo().getName());
        result.addProperty("toPort", connection.getToPort().getName());

        result.addProperty("fromType", connection.getFromPort().getType().getName());
        result.addProperty("toType", connection.getToPort().getType().getName());

        result.addProperty("forced", connection.isForced());

        return result;
    }

    public JsonObject serializeNetwork(Network network, File executionRoot) {
        final JsonObject result = new JsonObject();

        for (ComponentInstance ci: network) {
            final JsonObject ciMap = new JsonObject();

            final String componentBundle;
            if (ci.getComponent().getBundle() == null) {
                componentBundle = null;
            } else {
                componentBundle = ci.getComponent().getBundle().getName();
            }

            ciMap.addProperty("name", ci.getName());
            ciMap.addProperty("simpleName", ci.getSimpleName());
            ciMap.addProperty("component", ci.getComponent().getName());
            ciMap.addProperty("componentBundle", componentBundle);
            ciMap.addProperty("doc", ci.getDoc());
            ciMap.addProperty("configurationDigest", ci.getConfigurationDigest(network));

            final JsonObject inputFileMap = new JsonObject();
            for (InPort port: ci.getComponent().getInPorts()) {
                inputFileMap.add(port.getName(), serializeFile(ci.getInputFile(port, executionRoot)));
            }
            ciMap.add("inputFiles", inputFileMap);

            final JsonObject outputFileMap = new JsonObject();
            for (OutPort port: ci.getComponent().getOutPorts()) {
                outputFileMap.add(port.getName(), serializeFile(ci.getOutputFile(port, executionRoot)));
            }
            ciMap.add("outputFiles", outputFileMap);

            final JsonArray inputConnectionArray = new JsonArray();
            for (PortConnection conn: ci.getInPortConnections()) {
                inputConnectionArray.add(serializePortConnection(conn));
            }
            ciMap.add("inputConnections", inputConnectionArray);

            final JsonArray outputConnectionArray = new JsonArray();
            for (PortConnection conn: ci.getOutPortConnections()) {
                outputConnectionArray.add(serializePortConnection(conn));
            }
            ciMap.add("outputConnections", outputConnectionArray);

            final JsonObject parameterMap = new JsonObject();
            for (Parameter param: ci.getComponent().getParameters()) {
                parameterMap.add(param.getName(), this.gson.toJsonTree(ci.getParameterValue(param)));
            }
            ciMap.add("parameters", parameterMap);

            final JsonObject annotationMap = new JsonObject();
            for (Map.Entry<Annotation, Object> annotationValue: ci.getInstanceAnnotations().entrySet()) {
                final Annotation annotation = annotationValue.getKey();
                annotationMap.addProperty(annotation.name, annotationValue.getValue().toString());
            }
            ciMap.add("annotations", annotationMap);

            result.add(ci.getName(), ciMap);
        }

        return result;
    }

    public JsonElement serializeLauncher(Launcher launcher) {
        if (launcher == null) {
            return JsonNull.INSTANCE;
        } else {
            final JsonObject result = new JsonObject();
            result.addProperty("type", launcher.getType());
            result.add("arguments", serializeSimpleObject(launcher.getArgs()));
            return result;
        }
    }

    public JsonArray serializeOutPorts(Collection<OutPort> outPorts) {
        final JsonArray result = new JsonArray();

        if (outPorts != null) {
            for (OutPort port: outPorts) {
                if (!port.isSpecial()) {
                    JsonObject portMap = serializePort(port);
                    result.add(portMap);
                }
            }
        }

        return result;
    }

    public JsonArray serializeParameters(Collection<Parameter> parameters) {
        final JsonArray result = new JsonArray();

        if (parameters != null) {
            for (Parameter parameter: parameters) {
                JsonObject parameterMap = new JsonObject();
                parameterMap.addProperty("name", parameter.getName());
                parameterMap.addProperty("type", parameter.getType().TYPE);
                parameterMap.addProperty("default", parameter.getDefaultValue());
                parameterMap.addProperty("doc", parameter.getDoc());
                result.add(parameterMap);
            }
        }

        return result;
    }

    public JsonObject serializePort(Port port) {
        final String typeBundle;
        if (port.getType() == null || port.getType().getBundle() == null) {
            typeBundle = null;
        } else {
            typeBundle = port.getType().getBundle().getName();
        }

        final JsonObject result = new JsonObject();
        result.addProperty("name", port.getName());
        result.addProperty("type", port.getType().getName());
        result.addProperty("typeBundle", typeBundle);
        result.addProperty("position", port.getPosition());
        result.addProperty("array", port.isArray().name().toLowerCase());
        result.addProperty("generic", port.isGeneric());
        result.addProperty("doc", port.getDoc());
        return result;
    }

    public JsonArray serializeRequirements(Collection<Requirement> requirements) {
        final JsonArray result = new JsonArray();

        if (requirements != null) {
            for (Requirement requirement: requirements) {
                final JsonObject reqMap = new JsonObject();
                reqMap.addProperty("name", requirement.getName());
                reqMap.addProperty("versionNumber", requirement.getVersionNumber());
                reqMap.addProperty("versionRelation", requirement.getVersionRelation());
                reqMap.addProperty("url", requirement.getURL());
                reqMap.addProperty("optional", requirement.getOptional());

                final JsonArray resourcesArray = new JsonArray();
                if (requirement.getResources() != null) {
                    for (RequirementResource resource: requirement.getResources()) {
                        final JsonObject resourceMap = new JsonObject();
                        resourceMap.addProperty("type", resource.getType());
                        resourceMap.addProperty("contents", resource.getContents());
                        resourcesArray.add(resourceMap);
                    }
                }
                reqMap.add("resource", resourcesArray);

                result.add(reqMap);
            }
        }

        return result;
    }

    public JsonElement serializeSimpleObject(Map<String, String> obj) {
        return this.gson.toJsonTree(obj);
    }

    public JsonElement serializeSimpleObject(String[] obj) {
        return this.gson.toJsonTree(obj);
    }

    public JsonElement serializeSimpleObject(Iterable<String> obj) {
        return this.gson.toJsonTree(obj);
    }

    public JsonArray serializeTestCases(Collection<ComponentTestCase> testCases, Component component) {
        final JsonArray result = new JsonArray();

        for (ComponentTestCase testCase: testCases) {
            final JsonObject testCaseMap = new JsonObject();

            testCaseMap.addProperty("name", testCase.getCaseName());
            testCaseMap.addProperty("expectSuccess", testCase.expectSuccess());
            testCaseMap.add("propertiesFile", serializeFile(testCase.getParameterFile()));

            final JsonObject inPortMap = new JsonObject();
            for (InPort port: component.getInPorts()) {
                inPortMap.add(port.getName(), serializeFile(testCase.getInputFile(port)));
            }
            testCaseMap.add("inFiles", inPortMap);

            final JsonObject outPortMap = new JsonObject();
            for (OutPort port: component.getOutPorts()) {
                outPortMap.add(port.getName(), serializeFile(testCase.getOutputFile(port)));
            }
            testCaseMap.add("outFiles", outPortMap);

            result.add(testCaseMap);
        }

        return result;
    }

    public JsonObject serializeTypeParameters(Collection<TypeParameter> typeParameters) {
        final JsonObject result = new JsonObject();

        for (TypeParameter typeParameter: typeParameters) {
            final JsonObject typeParamMap = new JsonObject();
            typeParamMap.addProperty("name", typeParameter.getName());
            typeParamMap.addProperty("position", typeParameter.getPosition());

            final String extendsName;
            final String extendsBundle;
            if (typeParameter.getExtendsType() == null) {
                extendsName = null;
                extendsBundle = null;
            } else {
                extendsName = typeParameter.getExtendsType().getName();
                if (typeParameter.getExtendsType().getBundle() == null) {
                    extendsBundle = null;
                } else {
                    extendsBundle = typeParameter.getExtendsType().getBundle().getName();
                }
            }
            typeParamMap.addProperty("extends", extendsName);
            typeParamMap.addProperty("extendsBundle", extendsBundle);

            result.add(typeParameter.getName(), typeParamMap);
        }

        return result;
    }
}

