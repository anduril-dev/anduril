package org.anduril.core.writers;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.DataType;
import org.anduril.core.network.Repository;
import org.anduril.core.utils.IOTools;

import java.io.File;
import java.io.IOException;

public class PackageObjectGenerator extends AbstractGenerator {

    public static final String PACKAGE_OBJECT_FRAGMENT = "package.scala";

    private Bundle bundle;
    private Repository repository;

    public PackageObjectGenerator(Bundle bundle, Repository repository) {
        this.bundle = bundle;
        this.repository = repository;
    }

    public void generate(File destination) throws IOException {
        // TODO: handle lastDotPos < 0; not dot in package name
        int lastDotPos = this.bundle.getCompilePackageName().lastIndexOf('.');
        String packageHead = this.bundle.getCompilePackageName().substring(0, lastDotPos);
        String packageTail = this.bundle.getCompilePackageName().substring(lastDotPos+1);

        writeLineFormat("package %s", packageHead);
        writeNewline();
        writeLineFormat("import org.anduril.runtime.Port");
        writeNewline();

        writeLineFormat("package object %s {", packageTail);
        increaseIndent();

        insertPackageObjectFragment();

        for (DataType type: this.repository.getOrderedDataTypes()) {
            if (type.getBundle().getName().equals(bundle.getName())) {
                if (type.getDoc() != null && !type.getDoc().isEmpty()) {
                    writeLineFormat("/** %s */", type.getDoc());
                }
                writeLineFormat("type %s = Port", type.getName());
            }
        }

        decreaseIndent();
        writeLine("}");

        writeToFile(destination);
    }

    private void insertPackageObjectFragment() throws IOException {
        final File fragment = new File(bundle.getDirectory(), PACKAGE_OBJECT_FRAGMENT);
        if (fragment.exists()) {
            for (String line: IOTools.readLines(fragment.getAbsolutePath())) {
                writeLine(line);
            }
        }
    }
}
