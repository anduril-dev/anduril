package org.anduril.core.writers;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.DataType;
import org.anduril.core.network.Repository;

import java.io.File;

public class DatatypeGenerator extends AbstractGenerator {
    public static final String TYPES_PACKAGE = "types";

    private final static String[] IMPORTS = new String[] {
            "org.anduril.core.Version",
            "org.anduril.core.network.PresentationType",
            "org.anduril.core.readers.networkParser.Tools",
            "org.anduril.runtime.Port"
    };

    private Repository repository;
    private Bundle bundle;

    public DatatypeGenerator(Repository repository, Bundle bundle) {
        this.repository = repository;
        this.bundle = bundle;
    }

    public void generate(File destination) {
        generateDataTypeLoader();
        writeToFile(destination);
    }

    private void generateDataTypeLoader() {
        writeLineFormat("package %s.%s", this.bundle.getCompilePackageName(), DatatypeGenerator.TYPES_PACKAGE);
        writeNewline();

        for (String importSpec: IMPORTS) {
            writeLineFormat("import %s", importSpec);
        }

        for (Bundle dependBundle: this.bundle.getDependsBundles(this.repository)) {
            writeLineFormat("import %s.%s._",
                    dependBundle.getCompilePackageName(),
                    DatatypeGenerator.TYPES_PACKAGE);
        }

        writeNewline();

        writeLine("object DataTypeLoader {");
        increaseIndent();
        writeLine("private var isLoaded = false");
        writeLine("def load(): Unit = {");
        increaseIndent();
        writeLine("if (!isLoaded) {");
        increaseIndent();
        writeLine("isLoaded = true");

        for (Bundle depends: this.bundle.getDependsBundles(this.repository)) {
            writeLineFormat("%s.types.DataTypeLoader.load()", depends.getCompilePackageName());
        }

        for (DataType type: repository.getOrderedDataTypes()) {
            if (type.getBundle().getName().equals(bundle.getName())) {
                generateDataType(type);
            }
        }

        decreaseIndent();
        writeLine("}");
        decreaseIndent();
        writeLine("}");
        decreaseIndent();
        writeLine("}");
        writeNewline();
    }

    private void generateDataType(DataType type) {
        writeIndent();
        write("Tools.addDataTypeToRepository(");

        writeFormat("%s, ", getStringExpression(type.getName()));

        if (type.getParentType() == null) {
            write("null, ");
        } else {
            writeFormat("%s, ", getStringExpression(type.getParentType().getName()));
        }

        writeFormat("%s, ", getStringExpression(type.getPresentationType().name()));

        writeFormat("%s", getStringExpression(type.getExtension(false)));

        write(")\n");
    }
}
