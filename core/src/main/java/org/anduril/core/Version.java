package org.anduril.core;

/**
 * Representation of a version number with format
 * A.B.C, where B or C may be missing. Also, a
 * branch identifier can be part of the version
 * number, e.g. A.B.C-mybranch.
 */
public class Version implements Comparable<Version> {
    
    /**
     * Version number of the pipeline engine.
     */
    public static final Version ANDURIL_VERSION = new Version(2, 0, 0);
    
    private int major;
    private Integer minor;
    private Integer revision;
    private String branch;
    
    /**
     * Initialize.
     * @param major Major version number.
     * @param minor Minor version number. May be null.
     * @param revision Revision version number. May be null.
     * @param branch Branch identifier. May be null.
     */
    public Version(int major, Integer minor, Integer revision, String branch) {
        this.major = major;
        this.minor = minor;
        this.revision = revision;
        this.branch = branch;
    }

    public Version(Version version) {
        major = version.major;
        minor = version.minor;
        revision = version.revision;
        branch = version.branch;
    }

    public Version(int major, Integer minor) {
        this(major, minor, null, null);
    }

    public Version(int major, Integer minor, Integer revision) {
        this(major, minor, revision, null);
    }
    
    private static int compareInt(int a, int b) {
        if (a < b) return -1;
        else if (a == b) return 0;
        else return 1;
    }
    
    public int compareTo(Version other) {
        if (!sameBranch(other)) return -1;
        
        if (this.getMajor() != other.getMajor()) {
            return compareInt(this.getMajor(), other.getMajor());
        }
        if (this.getMinorInt() != other.getMinorInt()) {
            return compareInt(this.getMinorInt(), other.getMinorInt());
        }
        if (this.getRevisionInt() != other.getRevisionInt()) {
            return compareInt(this.getRevisionInt(), other.getRevisionInt());
        }
        return 0;
    }
    
    public boolean sameBranch(Version other) {
        String t = this.getBranch();
        String o = other.getBranch();
        if (t == null && o == null) return true;
        else return (t != null && o != null && t.equals(o));
    }

    public int getMajor() {
        return major;
    }
    
    public void setMajor(int major) {
        this.major = major;
    }
    
    public Integer getMinor() {
        return minor;
    }

    public int getMinorInt() {
        if (minor == null) return 0;
        else return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }
    
    public Integer getRevision() {
        return revision;
    }
    
    public int getRevisionInt() {
        if (revision == null) return 0;
        else return revision;
    }
    
    public void setRevision(Integer revision) {
        this.revision = revision;
    }
    
    public String getBranch() {
        return branch;
    }
    
    public void setBranch(String branch) {
        this.branch = branch;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMajor());
        if (getMinor()    != null) sb.append('.').append(getMinor());
        if (getRevision() != null) sb.append('.').append(getRevision());
        if (getBranch()   != null) sb.append('-').append(getBranch());
        return sb.toString();
    }

    public static Version parse(String version) {
        String branch = null;
        
        String[] tokens = version.split("-", 2);
        if (tokens.length == 2) branch = tokens[1];
        
        tokens = tokens[0].split("[.]");
        if (tokens.length == 1) {
            return new Version(Integer.parseInt(tokens[0]), null, null, branch);
        }
        else if (tokens.length == 2) {
            return new Version(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]),
                    null, branch);
        }
        else if (tokens.length == 3) {
            return new Version(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]),
                    Integer.parseInt(tokens[2]), branch);
        }
        else {
            throw new IllegalArgumentException("Can't parse "+version+": too many numeric parts");
        }
    }
    
    public static void main(String[] args) {
        System.out.println(ANDURIL_VERSION);
    }
}
