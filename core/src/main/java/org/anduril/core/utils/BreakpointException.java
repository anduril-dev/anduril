package org.anduril.core.utils;

import org.anduril.core.engine.DynamicError;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.SourceLocation;
import org.anduril.core.network.StaticError;

public class BreakpointException extends DynamicError {
	private static final long serialVersionUID = -8079108031784486984L;

	public BreakpointException() {
		super(null, null, null);
	}
	
	public BreakpointException(String message, ComponentInstance ci) {
		super(message, ci);
	}
}
