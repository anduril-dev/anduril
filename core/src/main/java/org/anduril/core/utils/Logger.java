package org.anduril.core.utils;

import org.anduril.core.commandline.BaseCommand;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.SourceLocation;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class Logger {

    private static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static boolean verbose = false;

    private enum Level {
        DEBUG,
        INFO,
        LOG,
        WARN,
        ERROR
    }

    private final String label;

    private Logger(String label) {
        this.label = label;
    }

    private void writeMessage(Level level, String message, String... tags) {
        final StringBuilder sb = new StringBuilder();

        sb.append('[');
        sb.append(level.name());
        sb.append(' ');
        sb.append(this.label);
        sb.append("] ");

        sb.append(message);

        for (String tag: tags) {
            if (tag == null) {
                continue;
            }
            sb.append(" (");
            sb.append(tag);
            sb.append(')');
        }

        final String formattedMessage = sb.toString();
        System.out.println(formattedMessage);
    }

    /**
     * Log a message to the log at level CONFIG.
     */
    public void debug(String msg, String... tags) {
        if (Logger.verbose) {
            writeMessage(Level.DEBUG, msg, tags);
        }
    }

    /**
     * Log a message to the log at level INFO.
     */
    public void info(String msg, String... tags) {
        writeMessage(Level.INFO, msg, tags);
    }

    /**
     * Log a message to the log at level LOG.
     * This is the same verbosity level as INFO
     * but denotes logging from an external
     * process.
     */
    public void log(String msg, String... tags) {
        writeMessage(Level.LOG, msg, tags);
    }

    /**
     * Log a message to the log at level WARNING.
     */
    public void warning(String msg, String... tags) {
        writeMessage(Level.WARN, msg, tags);
    }
    
    /**
     * Log a message to the log at level ERROR.
     */
    public void error(String msg, String... tags) {
        writeMessage(Level.ERROR, msg, tags);
    }

    /**
     * Log a message to the log at level ERROR.
     */
    public void error(Throwable exception, String... tags) {
        error(ExceptionUtils.getRootCauseMessage(exception), tags);
        for (String trace: ExceptionUtils.getRootCauseStackTrace(exception)) {
            error(trace, tags);
        }
    }

    // Verbosity

    public static boolean isVerbose() {
        return Logger.verbose;
    }

    public static void setVerbose(boolean verbose) {
        Logger.verbose = verbose;
    }

    // Instance builders

    public static Logger getLogger(ComponentInstance ci) {
        return new Logger(ci.getName());
    }

    public static Logger getLogger(BaseCommand command) {
        return new Logger(String.format("<%s>", command.getName()));
    }

    public static Logger getLogger(String label) {
        return new Logger(label);
    }

    // Tags

    public static String tagComponentConfigurationChanged() {
        return "COMPONENT-CONFIGURATION-CHANGED";
    }

    public static String tagComponentFinished(boolean success) {
        return success ? "COMPONENT-FINISHED-OK" : "COMPONENT-FINISHED-ERROR";
    }

    public static String tagComponentStarted() {
        return "COMPONENT-STARTED";
    }

    public static String tagCallback() {
        return "CALLBACK";
    }

    public static String tagReadyQueue(int size) {
        return String.format("READY-QUEUE %d", size);
    }

    public static String tagSource(SourceLocation location) {
        if (location == null) {
            return null;
        } else {
            return "SOURCE " + location.toString();
        }
    }

    public static String tagTimestamp() {
        return TIMESTAMP_FORMAT.format(new Date());
    }

    public static String tagWarningWorkflowStructure() {
        return "WARNING-WORKFLOW-STRUCTURE";
    }

    public static String tagWarningBundleNotCompiled() {
        return "WARNING-BUNDLE-NOT-COMPILED";
    }

}
