package org.anduril.core.utils;

import java.util.List;

/**
 * Tools for formatting strings.
 */
public class TextTools {

    public static String join(String[] strings, String sep) {
        StringBuilder sb = new StringBuilder(512);
        for (int i=0; i<strings.length; i++) {
            if (i > 0) sb.append(sep);

            String s = strings[i];
            if (s != null) sb.append(s);
        }
        return sb.toString();
    }

    public static String join(List<String> strings, String sep) {
        return join(strings.toArray(new String[strings.size()]), sep);
    }

    /**
     * Utility function to create a short documentation string that
     * usually fits into one or two lines. This is the first sentence of the
     * documentation string. The end of the sentence is found by the
     * regular expression "[.]\\s+[A-Z]", that is, a dot followed by
     * at least whitespace followed by a capital character A-Z.
     * @param doc The full documentation string.
     * @return Short documentation string, or null if doc is null.
     */
    public static String getShortDoc(String doc) {
        if (doc == null) return null;
        final String PATTERN = "[.]\\s+[A-Z]";
        String[] tokens = doc.split(PATTERN, 2);
        if (tokens.length == 0) return null;
        String result = tokens[0].trim();
        if (!result.endsWith(".")) result += ".";
        return result;
    }
    
}
