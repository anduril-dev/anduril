package org.anduril.core.utils;

import java.io.File;
import java.io.FilenameFilter;

public class BaseFilenameFilter implements FilenameFilter {
    private final String basename;
    public BaseFilenameFilter(String basename) {
        this.basename = basename;
    }
    
    @Override
    public boolean accept(File dir, String name) {
        String base = IOTools.removeFileExtension(name);
        return this.basename.equals(base);
    }
}