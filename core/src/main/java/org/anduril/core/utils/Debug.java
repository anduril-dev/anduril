package org.anduril.core.utils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Debug {

    public static final boolean DEBUG = true;
    public static final String LOGO = " DEBUG :: ";
    
    public static final Format TIME_FORMATTER = new SimpleDateFormat("[yyyy-MM-dd HH.mm.ss:SSS]",
    		                                                    Locale.ENGLISH);

    public static String timestamp() {
        Date date = new Date(System.currentTimeMillis() + (int)(System.nanoTime() / 1000000));
        return TIME_FORMATTER.format(date);
    }

    public static void out(String aMessage) {
        if (Debug.DEBUG) System.out.println(timestamp() + LOGO + aMessage);
    }

    public static void out(String aMessage, Object... values) {
        Debug.out(String.format(aMessage, values));
    }

    public static void out(boolean isDisplayable, String aMessage, Object... values) {
        if (isDisplayable) Debug.out(aMessage, values);
    }
}
