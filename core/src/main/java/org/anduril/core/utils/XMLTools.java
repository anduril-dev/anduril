package org.anduril.core.utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Tools for handling XML DOM trees.
 */
public class XMLTools {

    public static String factoryClass = null;

    public static void setFactoryClass(String factoryClass) {
        XMLTools.factoryClass = factoryClass;
    }
    
    public static String getFactoryClass() {
        return XMLTools.factoryClass;
    }
    
    public static DocumentBuilder getDocumentBuilder(String externalSchemeLocation,
            ErrorHandler errorHandler) throws ParserConfigurationException {
        final String FACTORY_PROPERTY = "javax.xml.parsers.DocumentBuilderFactory";
        /* Remember old value and restore it afterwards */
        final String oldProperty = System.getProperty(FACTORY_PROPERTY);
        
        try {
            if (XMLTools.factoryClass != null) {
                System.setProperty(FACTORY_PROPERTY, XMLTools.factoryClass);
            }

            DocumentBuilderFactory dbf = (factoryClass == null) ?
            		DocumentBuilderFactory.newInstance() :
            		DocumentBuilderFactory.newInstance(factoryClass, XMLTools.class.getClassLoader());
            dbf.setNamespaceAware(true);

            if (externalSchemeLocation != null) {
                dbf.setValidating(true);
                dbf.setFeature("http://apache.org/xml/features/validation/schema", true);
                dbf.setAttribute("http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation",
                        externalSchemeLocation);
            }

            DocumentBuilder builder = dbf.newDocumentBuilder();
            if (errorHandler != null) builder.setErrorHandler(errorHandler);
            return builder;
        } finally {
            if (XMLTools.factoryClass != null && oldProperty != null && !oldProperty.isEmpty()) {
                System.setProperty(FACTORY_PROPERTY, oldProperty);
            }
        }
    }
    
    /**
     * Return recursive child elements of given element that have given name.
     * This includes the whole subtree of the element.
     * @param element Root element of subtree.
     * @param name Name of the child elements of interest.
     */
    public static List<Element> getElementsByTagName(Element element, String name) {
        NodeList nl = element.getElementsByTagName(name);
        List<Element> els = new ArrayList<Element>();
        for (int i=0; i<nl.getLength(); i++) {
            els.add((Element)nl.item(i));
        }
        return els;
    }
    
    /**
     * Return immediate child elements of given element that have given name.
     * This only includes children of given element and not grandchildren.
     * 
     * @param element Parent element.
     * @param name Name of the child elements of interest.
     */
    public static List<Element> getChildrenByName(Element element, String name) {
        List<Element> nodes = new ArrayList<Element>();
        
        NodeList nl = element.getChildNodes();
        for (int i=0; i<nl.getLength(); i++) {
            Node node = nl.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals(name)) {
                nodes.add((Element)node);
            }
        }
        
        return nodes;
    }

    /**
     * Return all immediate child elements of given elements.
     * This only includes children of given element and not grandchildren.
     * @param element Parent element.
     */
    public static List<Element> getChildren(Element element) {
        List<Element> nodes = new ArrayList<Element>();
        
        NodeList nl = element.getChildNodes();
        for (int i=0; i<nl.getLength(); i++) {
            Node node = nl.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                nodes.add((Element)node);
            }
        }
        
        return nodes;
    }

    /**
     * Return the first child of the element that has given name
     * @param element Parent element.
     * @param name Name of child element.
     * @return The first child element with the name, or null if there
     * 	is no such child.
     */
    public static Element getChildByName(Element element, String name) {
        List<Element> children = getChildrenByName(element, name);
        if (children.isEmpty()) return null;
        else return children.get(0);
    }
    
    /**
     * Return the text contents of immediate child elements that have given name.
     * @param element Parent element.
     * @param name Name of the child elements of interest.
     * @see #getChildrenByName(Element, String)
     */
    public static List<String> getChildrenContents(Element element, String name) {
        List<String> contents = new ArrayList<String>();
        for (Element el: getChildrenByName(element, name)) {
            contents.add(el.getTextContent());
        }
        return contents;
    }

    /**
     * Return text contents of the first immediate child element that has given
     * name.
     * @param element Parent element.
     * @param name Name of the child elements of interest.
     * @return Text contents, or null if there is no such child.
     */
    public static String getSingleChildContents(Element element, String name) {
        List<Element> els = getChildrenByName(element, name);
        if (els.size() == 0) return null;
        else return els.get(0).getTextContent();
    }
    
    /**
     * Return the XML node as a string like it appears in
     * XML source file.
     * @param node XML node.
     * @return XML string, or null if an error occurred.
     */
    public static String getNodeAsString(Node node) {
        Writer writer = new StringWriter();
        DOMSource domSource = new DOMSource(node);
        TransformerFactory tf = TransformerFactory.newInstance();
        StreamResult result = new StreamResult(writer);

        Transformer serializer;
        try {
            serializer = tf.newTransformer();
        } catch(TransformerConfigurationException e) {
            return null;
        }
        
        serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

        try {
            serializer.transform(domSource, result);
        } catch (TransformerException e) {
            return null;
        }
        return (writer.toString());
    }

    public static String getElementContentsAsString(Element element) {
        StringBuilder sb = new StringBuilder();
        NodeList nl = element.getChildNodes();
        for (int i=0; i<nl.getLength(); i++) {
            Node node = nl.item(i);
            sb.append(getNodeAsString(node));
        }
        return sb.toString().trim();
    }
}
