package org.anduril.core.utils;

import org.anduril.core.network.SourceLocation;

import java.io.File;

/**
 * Unhandled exception for traversing most of the core without having to update 
 * many affected classes; catched only in commandline package.
 */
@SuppressWarnings("serial")
public class IllegalAnnotationScopeException extends RuntimeException {

    private String annotationName;
    private SourceLocation location;
    private File file;
    
    public IllegalAnnotationScopeException(String annotationName, SourceLocation location) {
        this.annotationName = annotationName;
        this.location = location;
        this.file = location.getFile();
    }
    
    public String getAnnotationName() {
        return this.annotationName;
    }
    
    public SourceLocation getLocation() {
        return this.location;
    }
    
    public File getFile() {
        return this.file;
    }
    
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
    
}
