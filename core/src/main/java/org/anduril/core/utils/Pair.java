package org.anduril.core.utils;

public class Pair<T1, T2> {
    private T1 obj1;
    private T2 obj2;
    
    public Pair(T1 obj1, T2 obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }
    
    public String toString() {
        return String.format(get1().toString()+","+get2().toString());
    }
    
    @SuppressWarnings("unchecked")
	public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;
        Pair<T1,T2> other = (Pair<T1,T2>)o;
        return (get1()==null ?
                other.get1()==null : get1().equals(other.get1()))
                &&
                (get2()==null ?
                other.get2()==null : get2().equals(other.get2()));
    }
    
    public int hashCode() {
        return (get1()==null ? 0 : get1().hashCode()) ^
            (get2()==null ? 0 : get2().hashCode());
    }
    
    public T1 get1() {
        return obj1;
    }

    public void set1(T1 obj1) {
        this.obj1 = obj1;
    }

    public T2 get2() {
        return obj2;
    }
    
    public void set2(T2 obj2) {
        this.obj2 = obj2;
    }
}
