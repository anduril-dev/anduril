package org.anduril.core.utils;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.*;
import java.net.URL;
import java.util.Map;

/**
 * Tools for rendering HTML.
 */
public class HTMLTools {
    private static boolean velocityInitialized = false;
    
    /**
     * Render a velocity template.
     * @param templateURL URL for the template.
     * @param model Context model for the template. The model consists
     * 	of name, object pairs that can be accessed in the template.
     * @return Rendered string.
     * @throws IOException If the template can't be read.
     */
    public static String renderVelocity(URL templateURL, Map<String,Object> model) throws IOException {
        if (!HTMLTools.velocityInitialized) {
            try {
                Velocity.init();
                HTMLTools.velocityInitialized = true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        
        VelocityContext context = new VelocityContext(model);
        StringWriter writer = new StringWriter();
        
        InputStreamReader reader = new InputStreamReader(templateURL.openStream());
        
        try {
            Velocity.evaluate(context, writer, templateURL.toString(), reader);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            reader.close();
        }
        
        writer.flush();
        return writer.toString();
    }
    
    public static void renderVelocityToFile(URL templateURL, Map<String,Object> model, File output)
            throws IOException {
        String s = HTMLTools.renderVelocity(templateURL, model);
        FileWriter writer = new FileWriter(output);
        try {
            writer.write(s);
        } finally {
            writer.close();
        }
    }
    
    public static String descendDirectories(int depth) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<depth; i++) sb.append("../");
        return sb.toString();
    }

}