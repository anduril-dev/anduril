package org.anduril.core.utils;

import org.anduril.component.LatexTools;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public enum Language {
    scala() {
        public String quote(String text) {
            String s = text.trim();
            if (s.isEmpty()) return "emptyString";
            s = StringEscapeUtils.escapeHtml(s);
            s = s.replaceAll("[^\\dA-Za-z]+", "_");
            if (Character.isDigit(s.charAt(0))) {
                s = 'n'+s;
            }
            if (s.charAt(s.length()-1) == '_') {
                s = s.substring(0,s.length()-1);
            }
            return s;
        }
    },
    html() {
        public String quote(String text) { return StringEscapeUtils.escapeHtml(text); }
    },
    latex() {
        public String quote(String text) { return LatexTools.quote(text); }
    },
    url() {
        public String quote(String text) {
            try { return URLEncoder.encode(text, "UTF-8"); }
            catch (UnsupportedEncodingException err) {
                throw new UnsupportedOperationException("No UTF-8 support.", err);
            }
        }
    };

    /** Adds the language specific escape sequences */
    abstract public String quote(String text); 

    public static String quote(String text, String targetLanguage) {
        Language lang = null;
        try {
            lang = Language.valueOf(targetLanguage.toLowerCase());
        } catch (IllegalArgumentException err) {
            throw new IllegalArgumentException("Invalid language: "+targetLanguage);
        }
        return lang.quote(text);
    }

}
