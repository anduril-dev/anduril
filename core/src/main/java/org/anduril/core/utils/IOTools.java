package org.anduril.core.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tools for I/O.
 */
public final class IOTools {

    /** No instantiation */
    private IOTools() {}

    static class StreamReaderThread implements Runnable {
        private final BufferedReader reader;
        private final Logger logger;
        
        public StreamReaderThread(BufferedReader reader, Logger logger) {
            this.reader = reader;
            this.logger = logger;
        }
        
        public void run() {
            while (true) {
                String line;
                try {
                    line = reader.readLine();
                } catch(IOException e) {
                    logger.error(e.toString());
                    break;
                }
                if (line == null) break;
                if (logger != null) {
                    logger.log(line);
                }
            }
        }
    }

    /**
     * Launch an external process and wait until it returns.
     * @param command Command array.
     * @param workDirectory If non-null, the working directory
     *  is switched to this directory before launching.
     * @param environment Environment variables as a map. May be
     *  null, in which case the child process inherits the
     *  environment from the parent process.
     * @param stdinContent If non-null, this string is written to
     *  the standard input of the child process. If null, nothing
     *  is written.
     * @param logger The logger that is used to print stdout and
     *  stderr. If null, the default logger is used.
     * @return Process exit status. Status of 0 means success and
     *  non-zero means error.
     * @throws IOException If execution failed and no exit status
     *  could be produced.
     * @see java.lang.Runtime#exec(java.lang.String[],java.lang.String[],java.io.File)
     */
    public static int launch(String[] command, File workDirectory, Map<String,String> environment,
            String stdinContent, Logger logger) throws IOException {
        String[] envp = null;
        if (environment != null) {
            envp = new String[environment.size()];
            int i = 0;
            for (Map.Entry<String, String> entry: environment.entrySet()) {
                envp[i] = String.format("%s=%s", entry.getKey(), entry.getValue());
                i++;
            }
        }
        
        /*
         * To support both the String and String[] arguments in one
         * function, the launch(String, ...) variants pass an array
         * of {command, null} to indicate that the String form is
         * wanted. Here we recognize this form. Note that otherwise,
         * null is not allowed in the String[] array.
         */
        Process process;
        if (command.length == 2 && command[1] == null) {
            process = Runtime.getRuntime().exec(command[0], envp, workDirectory);
        } else {
            process = Runtime.getRuntime().exec(command, envp, workDirectory);
        }

        BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader stderrReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        
        Thread stdoutThread = null;
        Thread stderrThread = null;
        
        try {
            stdoutThread = new Thread(new StreamReaderThread(stdoutReader, logger));
            stderrThread = new Thread(new StreamReaderThread(stderrReader, logger));
            stdoutThread.start();
            stderrThread.start();

            if (stdinContent != null && !stdinContent.isEmpty()) {
                OutputStreamWriter stdinWriter = new OutputStreamWriter(process.getOutputStream());
                try {
                    stdinWriter.write(stdinContent);
                } finally {
                    stdinWriter.close();
                }
            }
            process.getOutputStream().close();

            int status;
            try {
                status = process.waitFor();
                stdoutThread.join(5000);
                stderrThread.join(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return -1;
            }
            return status;
        } finally {
            stdoutReader.close();
            stderrReader.close();

            if (stdoutThread != null) stdoutThread.interrupt();
            if (stderrThread != null) stderrThread.interrupt();
        }
    }

    public static int launch(String[] command, File workDirectory, Map<String,String> environment,
            String stdinContent) throws IOException {
        return launch(command,
                workDirectory, environment,
                stdinContent, null);
    }
    
    public static int launch(String[] command, File workDirectory, Map<String,String> environment) throws IOException {
        return launch(command, workDirectory, environment, null, null);
    }

    public static int launch(String command, File workDirectory, Map<String,String> environment,
            String stdinContent, Logger logger) throws IOException {
        command = command.replace("\\", "/");
        
        String regex = "\"([^\"]*)\"|(\\S+)";
        Matcher m = Pattern.compile(regex).matcher(command);
        List<String> matches = new ArrayList<String>();
        while(m.find()){
            matches.add(m.group());
        }
        String [] arguments = matches.toArray(new String[matches.size()]);

        return launch(arguments,
                workDirectory, environment,
                stdinContent, logger);
    }
    
    public static int launch(String command, File workDirectory, Map<String,String> environment) throws IOException {
        return launch(command,
                workDirectory, environment,
                null, null);
    }
    
    public static int launch(String command, File workDirectory) throws IOException {
        return launch(command, workDirectory, null, null, null);
    }

    public static int launch(String command) throws IOException {
        return launch(command, null, null, null, null);
    }

    /**
     * Remove the contents of a directory.
     * @param dir Name of the directory.
     * @param recursive If true, all subdirectories are removed.
     * 	If false, only files present in the directory are
     *  removed.
     */
    public static boolean rmdir(File dir, boolean recursive) {
        if (dir == null) throw new NullPointerException("dir is null");
        boolean success = true;
        
        File[] files = dir.listFiles();
        if (files == null) return false;
        
        for (File f: files) {
            boolean thisSuccess;
            // Try to delete ( succeeds in: empty folder, normal file or symlink )
            thisSuccess = f.delete();
            if (!thisSuccess) {
                // Target was a folder with files, proceed with recursive.
                thisSuccess = rmdir(f, recursive);
                if (!thisSuccess) success = false;
                thisSuccess = f.delete();
                if (!thisSuccess) success = false;
            }
        }
        return success;
    }

    /**
     * Return true if the name is a valid file name.
     */
    public static boolean isValidName(String name) {
        final String re = "[a-zA-Z_][a-zA-Z0-9_\\.]*";
        return name.matches(re);
    }

    /**
     * Compare the contents of two files and return true
     * if they are byte-wise equal.
     * @param file1 First file.
     * @param file2 Second file.
     */
    public static boolean compareFileContents(File file1, File file2)
            throws IOException {
        final int BUFFER_SIZE = 1024*16;
        byte[] buffer1 = new byte[BUFFER_SIZE];
        byte[] buffer2 = new byte[BUFFER_SIZE];
        
        FileInputStream in1 = new FileInputStream(file1);
        FileInputStream in2 = new FileInputStream(file2);
        boolean same = true;
        
        while(true) {
            int bytes1 = in1.read(buffer1);
            int bytes2 = in2.read(buffer2);
            
            if (bytes1 != bytes2 || !Arrays.equals(buffer1, buffer2)) {
                same = false;
                break;
            }
            if (bytes1 < 0) break;
        }
        
        in1.close();
        in2.close();
        return same;
    }
    
    // Directory comparator //
    
    /**
     * Result object for compareDirectories.
     */
    public static class DirectoryDiff {
        private List<File> missing1 = new ArrayList<File>();
        private List<File> missing2 = new ArrayList<File>();
        private List<Pair<File,File>> different = new ArrayList<Pair<File,File>>();

        public String toString() {
            StringBuilder sb = new StringBuilder(1024);
            if (!missing1.isEmpty()) {
                sb.append("Missing from directory 1: \n");
                for (File file: missing1) {
                    sb.append(file.getPath()).append('\n');
                }
            }

            if (!missing2.isEmpty()) {
                sb.append("Missing from directory 2: \n");
                for (File file: missing2) {
                    sb.append(file.getPath()).append('\n');
                }
            }

            if (!different.isEmpty()) {
                sb.append("Different contents: \n");
                for (Pair<File,File> pair: different) {
                    sb.append(pair.get1().getPath())
                      .append(" / ")
                      .append(pair.get2().getPath()).append('\n');
                }
            }
            return sb.toString();
        }
        
        /**
         * Return files that are present in directory 2 but missing
         * in directory 1.
         */
        public List<File> getMissing1() {
            return missing1;
        }
        /**
         * Return files that are present in directory 1 but missing
         * in directory 2.
         */
        public List<File> getMissing2() {
            return missing2;
        }
        
        /**
         * Return files that are present in both directories but whose
         * contents differ. This also includes cases where the other
         * file is a directory and the other is a normal file. The
         * first file is located in directory 1 and the second in
         * directory 2.
         */
        public List<Pair<File,File>> getDifferent() {
            return different;
        }
        
        /**
         * Return true if the directories are same.
         */
        public boolean same() {
            return missing1.isEmpty() && missing2.isEmpty()
                && different.isEmpty();
        }

        public void addDifferent(File file1, File file2) {
            getDifferent().add(new Pair<File,File>(file1, file2));
        }
        
        protected void combine(DirectoryDiff other) {
            this.getMissing1().addAll(other.getMissing1());
            this.getMissing2().addAll(other.getMissing2());
            this.getDifferent().addAll(other.getDifferent());
        }
    }
    
    /**
     * Recursively compare contents of directories and report differences.
     * Files are compared byte-by-byte. The result object lists files that
     * are present in dir1 but missing from dir2, files that are present
     * in dir2 but missing in dir1, and files that are present in both but
     * have different contents. 
     * @param dir1 First directory.
     * @param dir2 Second directory.
     * @param filenameFilter If non-null, files and directories that match
     * 	this regular expression are not compared. Matching is done for
     * 	the base name of the file, i.e. only the last component is matched.
     * 	If a directory matches the filter, the recursive search is cut off
     * 	and files in that directory are not compared. 
     */
    public static DirectoryDiff compareDirectories(File dir1, File dir2, String filenameFilter) {
        DirectoryDiff result = new DirectoryDiff();
        
        if (!dir1.exists() || !dir1.isDirectory()) {
            result.getMissing1().add(dir1);
        }
        else {
            for (File file1: dir1.listFiles()) {
                if (filenameFilter != null && file1.getName().matches(filenameFilter)) continue;
                
                File file2 = new File(dir2, file1.getName());
                if (!file2.exists()) result.getMissing2().add(file2);
                else if (file1.isDirectory() && file2.isDirectory()) {
                    DirectoryDiff sub = compareDirectories(file1, file2, filenameFilter);
                    result.combine(sub);
                }
                else if (file1.isFile() && file2.isFile()) {
                    boolean same;
                    try {
                        same = compareFileContents(file1, file2);
                    } catch(IOException e) {
                        same = false;
                    }
                    if (!same) result.addDifferent(file1, file2);
                }
                else {
                    result.addDifferent(file1, file2);
                }
            }
        }
        
        if (!dir2.exists() || !dir2.isDirectory()) {
            result.getMissing2().add(dir2);
        }
        else {
            for (File file2: dir2.listFiles()) {
                if (filenameFilter != null && file2.getName().matches(filenameFilter)) continue;
                File file1 = new File(dir1, file2.getName());
                if (!file1.exists()) result.getMissing1().add(file1);
            }			
        }
        
        return result;
    }
    
    public static DirectoryDiff compareDirectories(File dir1, File dir2) {
        return compareDirectories(dir1, dir2, null);
    }
    
    public static boolean compareAny(File file1, File file2, String filenameFilter) throws IOException {
        if (file1.isFile() && file2.isFile()) {
            return compareFileContents(file1, file2);
        }
        else if (file1.isDirectory() && file2.isDirectory()) {
            DirectoryDiff diff = compareDirectories(file1, file2, filenameFilter);
            return diff.same();
        }
        else {
            return false;
        }
    }
    
    public static String[] readLines(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        ArrayList<String> s = new ArrayList<String>();
        String line;
        while ((line=reader.readLine()) != null) {
            s.add(line);
        }
        reader.close();
        return s.toArray(new String[s.size()]);
    }
    
    public static String removeFileExtension(String filename) {
        final int pos = filename.indexOf('.');
        if (pos <= 0) return filename;
        return filename.substring(0, pos);
    }
}
