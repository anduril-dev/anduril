package org.anduril.core.utils;

import org.anduril.core.network.ComponentInstance;

/**
 * Base class for errors that occur during loading or executing the
 * network. Errors optionally have a source, such as a component instance,
 * that is displayed along with the error message. This also allows to
 * find all errors related to a particular source.
 * 
 * Errors keep track of whether they have been reported (printed), which
 * makes sure that errors are only reported once but don't have to be
 * reported at the time of the occurrence.
 */
public abstract class BaseError extends Exception {
    private static final long serialVersionUID = 1L;
    
    private String message;
    private ComponentInstance ci;
    private Throwable exception;
    private boolean reported;
    
    /**
     * Initialize.
     * @param message Error message.
     * @param ci The component instance related to this error. May be null.
     * @param exception A relevant caught exception. May be null.  
     */
    public BaseError(String message, ComponentInstance ci, Throwable exception) {
        super(message);
        this.message = message;
        this.ci = ci;
        this.exception = exception;
        this.reported = false;

        if (exception instanceof java.lang.reflect.InvocationTargetException
                && exception.getCause() != null) {
            /* Avoid printing reflection related call stacks, obtained in
             * workflow evaluation. */
            this.exception = exception.getCause();
        }
    }
    
    public String toString() {
        return format();
    }
    
    public String getMessage() {
        return message;
    }
    
    public ComponentInstance getComponentInstance() {
        return ci;
    }
    
    public Throwable getException() {
        return exception;
    }

    public void report(Logger defaultLogger) {
        if (this.reported) return;

        final Logger logger;
        if (this.ci != null) {
            logger = ci.getLogger();
        } else {
            logger = defaultLogger;
        }
        logger.error(format());
        this.reported = true;
    }
    
    /**
     * Format the error as a string. The string has as a header
     * the source of the error, such as a component name, and
     * after that, the error message. 
     */
    public abstract String format();
}
