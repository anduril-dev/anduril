package org.anduril.core.network;

import org.anduril.core.engine.NetworkEvaluator;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.network.componentInstance.OutputComponentInstance;
import org.anduril.core.utils.TextTools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Network test case.
 */
public class NetworkTestCase extends TestCase {
    public static final String NETWORK_CONFIGURATION_FILE = "network.jar";
    
    private Bundle bundle;
    
    public NetworkTestCase(File caseDirectory, Repository repository, Bundle bundle)
            throws IOException {
        super(caseDirectory, repository, true);
        this.bundle = bundle;
    }
    
    public Bundle getBundle() {
        return bundle;
    }
    
    @Override
    public Component getComponent() {
        return null;
    }

    @Override
    public TestCase.Tuple<Network, ComponentInstance, NetworkEvaluator> createNetwork(File executionRoot) throws StaticError {
        File networkFile = new File(getCaseDirectory(), NETWORK_CONFIGURATION_FILE);
        
        if (!networkFile.exists()) {
            String msg = String.format(
                    "%s: network configuration file %s not found",
                    formatCaseName(), networkFile.getAbsolutePath());
            throw new StaticError(msg);
        }

        NetworkEvaluator evaluator = new NetworkEvaluator(executionRoot, getRepository(), getLogger());
        Network network = evaluator.evaluate(networkFile);
        network.setDataDir(networkFile.getParentFile());
        return new TestCase.Tuple<Network, ComponentInstance, NetworkEvaluator>(network, null, evaluator);
    }

    @Override
    public String formatCaseName() {
        return String.format("Bundle %s, network %s", bundle.getName(), getCaseName());
    }

    @Override
    public File getActualDirectory(File executionDirectory) {
        return new File(executionDirectory, OutputComponentInstance.OUTPUT_DIRECTORY);
    }

    @Override
    public File getNetworkExecutionRoot(File testExecutionRoot) {
        return new File(
            new File(testExecutionRoot, bundle.getName()),
            getCaseName());
    }
    
    @Override
    public boolean executeTwice() {
        return true;
    }
    
    @Override
    public List<TestCase.ResultFile> getExpectedFiles(Network network) throws IOException {
        List<TestCase.ResultFile> files = new ArrayList<ResultFile>();

        for (File file: getExpectedOutputDirectory().listFiles()) {
            if (file.getName().startsWith(".")) continue;
            
            String[] filenameTokens = file.getName().split("[.]", 2);
            String[] ciNameTokens = filenameTokens[0].split(ComponentInstance.FULL_NAME_SEPARATOR);
            String[] trimmedTokens = Arrays.copyOf(ciNameTokens, ciNameTokens.length-1);
            String fullName = TextTools.join(trimmedTokens, ComponentInstance.FULL_NAME_SEPARATOR);
            ComponentInstance ci = network.getComponentInstance(fullName);
                if (ci == null) {
                Tuple<ComponentInstance,DataType,OutPort> tup = searchRenamedComponentInstance(fullName);
                if (tup == null || tup.x == null) {
                    String msg = "File in expected-output does not match any component: "
                        +file.getAbsolutePath();
                    throw new IOException(msg);
                }
                else {
                    files.add(new ResultFile(file,tup.y,tup.z));
                }
            }
            else {
                String portName = ciNameTokens[ciNameTokens.length-1];
                OutPort port = ci.getComponent().getOutPort(portName);
                if (port == null) {
                    String msg = String.format(
                            "File in expected-output does not match any port " +
                                    "of component %s: %s", ci.getName(), file.getAbsolutePath());
                    throw new IOException(msg);
                }
                files.add(new ResultFile(file, port.getType(), port));
            }
        }
        
        return files;
    }

    private Tuple<ComponentInstance,DataType,OutPort> searchRenamedComponentInstance(String ciName) {
        for (ComponentInstance ci : getCaseNetwork()) {
            List<OutPort> ops = ci.getComponent().getOutPorts();
            for (OutPort op : ops) {
                String filenameAnn = (String)ci.getOutPortAnnotation(Annotation.FILENAME, op);
                if (filenameAnn != null && filenameAnn.equals(ciName)) {
                    return new Tuple<ComponentInstance,DataType,OutPort>(ci,op.getType(),op);
                }
            }
        }
        return null;
    }

    @Override
    public Network getFunctionNetwork() {
        return null;
    }

}
