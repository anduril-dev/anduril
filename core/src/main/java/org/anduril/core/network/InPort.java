package org.anduril.core.network;

/**
 * Input port of a component.
 */
public class InPort extends Port {

    private boolean optional;
    
    /**
     * Initialize.
     * @param component The component that owns this port.
     * @param name Name of the port.
     * @param position Position index of the port. This is a number
     * 	from 1 to N, where N is the number of (non-special) input
     * 	ports. For ports that don't use position, this is
     * 	Port.NO_POSITION.
     * @param desc Documentation string for the port.
     * @param type Data type of the port.
     * @param optional If true, then connecting this port in the
     * 	network is optional.
     * @param special If true, the port is a special port used
     * 	by the engine. Examples include error port and log port.
     */	
    public InPort(Component component, String name, int position, String desc, ArrayStatus isArray, DataType type, boolean optional, boolean special) {
        super(component, name, position, desc, isArray, type, special);
        this.optional = optional;
    }
    
    /**
     * Initialize a non-special input port.
     */
    public InPort(Component component, String name, int position, String desc, ArrayStatus isArray, DataType type, boolean optional) {
        this(component, name, position, desc, isArray, type, optional, false);
    }

    public String toString() {
        return "InPort: (name "+ getName() + ", type " + getType() + 
            ", component " + getComponent().getName() + ")";
    }
    
    /**
     * Return whether the port is optional. If true, then connecting this
     * port in the network is optional.
     */
    public boolean isOptional() {
        return optional;
    }

    /**
     * Set the flag that indicates whether the port is optional.
     */
    public void setOptional(boolean isOptional) {
        this.optional = isOptional;
    }

    /**
     * If the port is generic, convert it to a non-generic port by
     * replacing the data type with an actual type. If the port is
     * not generic, return the port as is.
     */
    public InPort getNonGenericPort(DataType actualType, ArrayStatus isArray) {
        if (!isGeneric()) return this;
        if (actualType == null) throw new NullPointerException("actualType is null");
        if (actualType.isGeneric()) {
            throw new IllegalArgumentException("actualType is generic: "+actualType.getName());
        }
        return new InPort(getComponent(), getName(), getPosition(), getDoc(),
                isArray, actualType,
                isOptional(), isSpecial());
    }

}