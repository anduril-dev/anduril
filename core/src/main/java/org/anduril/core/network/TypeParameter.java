package org.anduril.core.network;

/**
 * Type parameters for a generic component.
 */
public class TypeParameter implements Comparable<TypeParameter> {
    private String name;
    private int position;
    private DataType extendsType;
    
    /**
     * Initialize.
     * @param name Name of the type parameter.
     * @param position Position of the type parameter (1 to N).
     * @param extendsType Specifies that the actual type for
     * 	this parameter must extend some type. This may be
     * 	null.
     */
    public TypeParameter(String name, int position, DataType extendsType) {
        this.name = name;
        this.position = position;
        this.extendsType = extendsType;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof TypeParameter)) {
            return false;
        }
        TypeParameter paramOther = (TypeParameter)other;
        return paramOther.getName().equals(getName()) && paramOther.getPosition() == getPosition();
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public int compareTo(TypeParameter other) {
        return getName().compareTo(other.getName());
    }
    
    public String toString() {
        return String.format("TypeParameter: "+name);
    }
    
    /**
     * Return the name of the type parameter.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Return the position of the type parameter.
     */
    public int getPosition() {
        return position;
    }
    
    /**
     * Specifies that the actual type for this parameter must
     * extend some type. This may be null.
     */
    public DataType getExtendsType() {
        return extendsType;
    }
}
