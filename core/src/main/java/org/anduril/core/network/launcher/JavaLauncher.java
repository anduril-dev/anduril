package org.anduril.core.network.launcher;

import org.anduril.core.network.*;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Launcher for Java components. The launcher has one mandatory
 * argument, <code>class</code>, that contains the main executable
 * class name of the component. The optional argument
 * <code>extraClasspath</code> contains a string that is appended
 * to Java classpath. 
 */
public class JavaLauncher extends Launcher {

    private static int heapSizeMB = 200;
    private static String componentClasspath;

    private String className;
    private String extraClasspath;
    private boolean scalaLauncher = false;

    public JavaLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("class")) {
            throw new IllegalArgumentException("The Java launcher requires the argument 'class'");
        }
        
        this.className = getArgument("class");
        this.extraClasspath = getArgument("extraClasspath");
        
        if (this.extraClasspath != null && this.extraClasspath.isEmpty()) {
            this.extraClasspath = null;
        }
        setSourceFiles(getArgument("source"), null);
    }

    public static void setComponentClasspath(String path) {
    	if (path == null) {
        	componentClasspath = path;
    	} else {
        	componentClasspath = path.trim();
        	if (componentClasspath.isEmpty()) {
        		componentClasspath = null;
        	}
    	}
    }

    public static void setHeapSize(int sizeMB) {
        if (sizeMB < 10) {
            String msg = String.format("Heap size too small: %d MB", sizeMB);
            throw new IllegalArgumentException(msg);
        }
        JavaLauncher.heapSizeMB = sizeMB;
    }

    public static int getHeapSize() {
        return JavaLauncher.heapSizeMB;
    }
    
    /**
     * Return the name of the main executable class of the component.
     */
    public String getClassName() {
        return className;
    }
    
    /**
     * Return a string that is appended to Java classpath. May be null.
     */
    public String getExtraClasspath() {
        return extraClasspath;
    }

    public boolean getIsScalaLauncher() {
        return this.scalaLauncher;
    }
   
    public void setIsScalaLauncher(boolean isScalaLauncher) {
        this.scalaLauncher = isScalaLauncher;
    }

    /**
     * Create a classpath for component execution. The classpath
     * contains (1) existing CLASSPATH environment variable,
     * (2) <code>extraClasspath</code>, if defined,
     * (3) JAR and class files found in bundle directories
     * (<code>$BUNDLE/lib/java</code>) and
     * (4) the engine main JAR file.
     * @param repository Component repository
     */
    public String makeClasspath(Repository repository) {
        final String sep = System.getProperty("path.separator");
        
        StringBuilder classpath = new StringBuilder(2048);
        
        /* Existing CLASSPATH environment variable */
        String existing = System.getenv("CLASSPATH");
        if (existing != null) classpath.append(existing).append(sep);
        
        /* Extra classpath given in component XML file */
        if (extraClasspath != null) classpath.append(extraClasspath).append(sep);

        /* Build set of required JAR files. If * is encountered,
         * all JARs are included and jars is set to null. */
        Set<String> jars = new HashSet<String>();
        for (RequirementResource req: getComponent().getRequires(Requirement.REQ_JAVA_JAR)) {
            final String jar = req.getContents();
            if (jar.equals("*")) {
                jars = null;
                break;
            }
            jars.add(jar);
        }
        
        /* Classpaths in bundles. For each bundle, lib/java and
         * all required JAR files under lib/java are added to classpath. */
        for (Bundle bundle: repository.getBundles()) {
            File javaLib = new File(bundle.getLibDirectory(), "java");
            if (!javaLib.exists()) continue;

            classpath.append(javaLib.getAbsolutePath()).append(sep);

            for (File libFile: javaLib.listFiles()) {
                if (jars == null || jars.contains(libFile.getName())) {
                    if (libFile.getName().toLowerCase().endsWith(".jar")) {
                        classpath.append(libFile.getAbsolutePath()).append(sep);
                    }
                }
            }
        }

        /* Launcher specific additions */
        if (componentClasspath != null) classpath.append(componentClasspath).append(sep);
        
        /* Engine JAR and . */
        classpath.append(repository.getEngineJAR().getAbsolutePath())
                 .append(sep)
                 .append('.');

        return classpath.toString();
    }

    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
            Repository repository) throws IOException {

        String[] cmd = new String[] {
                this.scalaLauncher ? "scala" : "java",
                (this.scalaLauncher ? "-J" : "") + String.format("-Xmx%dm", getHeapSize()),
                "-cp", makeClasspath(repository),
                getClassName(),
                commandFile.getPath()
        };

        return cmd;
    }

}
