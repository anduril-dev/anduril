package org.anduril.core.network.launcher;

import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;
import org.apache.commons.lang.ArrayUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PerlLauncher extends Launcher {
    private File sourceFile;
    private static String executable = "perl";
    
    public PerlLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The Perl launcher requires the argument 'file'");
        }
        this.sourceFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.sourceFile);
    }

    /**
     * Return the command that is used to execute Perl.
     */
    public static String getExecutable() {
        return executable;
    }

    /**
     * Set the command that is used to execute Perl.
     */
    public static void setExecutable(String exec) {
        executable = exec;
    }

    /**
     * Return the Perl script source file.
     */
    public File getSourceFile() {
        return sourceFile;
    }

    @Override
    public Map<String, String> getExtraEnvironment(Repository repository) {
        final String PERLLIB = "PERL5LIB";
        final String sep = System.getProperty("path.separator");
        
        String path = System.getenv(PERLLIB);
        if (path == null) path = "";
        else path += sep;
        
        File perlLib = new File(new File(repository.getEngineJAR().getParentFile(), "lang"),"perl");
        path += perlLib;
        
        Map<String, String> env = new HashMap<String, String>();
        env.put(PERLLIB, path);
        return env;
    }

    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {

        String[] cmd = new String[] {
            executable,
            getSourceFile().getAbsolutePath(),
            commandFile.getPath()
        };

        return cmd;
    }
}
