package org.anduril.core.network.componentInstance;

import org.anduril.core.engine.ActiveState;
import org.anduril.core.engine.ComponentExecutor;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.*;

import java.io.File;

/**
 * Instance of the pause component. When executed, the component
 * tells execution engine to stop executing the network. The
 * pause component must have at least one incoming control
 * connection. The pause component has no ports.
 */
public class PauseComponentInstance extends AbstractComponentInstance {
    public PauseComponentInstance(String[] name, SourceLocation location, Component component) {
        super(name, location, component);
    }

    protected File getDefaultOutputFile(String portName, File executionRoot) {
        return null;
    }

    public boolean isExecutable() {
        return false;
    }
    
    public void initialize(Network network) throws StaticError {
        if (getInConnections(true, true).isEmpty()) {
            throw new StaticError("Pause component has no incoming connections", this);
        }
    }
    
    public void launch(Engine engine) {
        synchronized(ComponentExecutor.getInstance()) {
            for (ComponentInstance skip: getReachableComponents(engine)) {
                getLogger().info("Skip "+skip.getName());
                engine.setState(skip, ActiveState.DISABLED);
                engine.getExecutionDirectory().clearComponentDirectory(skip);
            }
        }
    }

}
