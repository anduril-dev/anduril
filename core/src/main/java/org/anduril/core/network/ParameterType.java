package org.anduril.core.network;


/**
 * Represent the set of possible parameter types: boolean,
 * int, float and string.
 */
public enum ParameterType {
    BOOLEAN ("boolean"),
    INT ("int"),
    FLOAT ("float"),
    STRING ("string");
    
    public final String TYPE;
    
    private ParameterType(String type) {
        this.TYPE = type;
    }

    @Override
    public String toString() {
        return TYPE;
    }
    
    public boolean validate(String value) {
        if (value == null) return false;
        
        switch (this) {
            case BOOLEAN: {
                return (value.equals("true") || value.equals("false"));
            }
            case INT: {
                try {
                    Integer.valueOf(value);
                    return true;
                } catch(NumberFormatException e) {
                    return false;
                }
            }
            case FLOAT: {
                try {
                    Double.valueOf(value);
                    return true;
                } catch(NumberFormatException e) {
                    return false;
                }
            }
            case STRING: return true;
            default: return false; 
        }        
    }
    
    /**
     * Get ParameterType from string.
     * @param type String representation of the type.
     * @return The ParameterType, or null if the string
     * 	is unknown.
     */
    public static ParameterType fromString(String type) {
        for (ParameterType pt: ParameterType.values()) {
            if (pt.TYPE.equals(type)) return pt;
        }
        return null;
    }
}
