package org.anduril.core.network;

/**
 * A network or repository element that can be traced to
 * a location is a source file. 
 */
public interface Locatable {
    /**
     * Return the location in a source file that contains
     * the definition of this element. This
     * typically identifies the position in a network
     * configuration file or an XML file, depending on element.
     * The returned value is never null, but any of the attributes
     * of the location may be null.
     */
    public SourceLocation getSourceLocation();
}
