package org.anduril.core.network.componentInstance;

import org.anduril.component.ErrorCode;
import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.AbstractComponentInstance;
import org.anduril.core.network.Component;
import org.anduril.core.network.SourceLocation;
import org.anduril.core.network.launcher.Launcher;

import java.io.File;
import java.io.IOException;

/**
 * Instance of a regular component that has an associated executable
 * file and that is not a branch.
 */
public class RegularComponentInstance extends AbstractComponentInstance {
    
    public RegularComponentInstance(String[] name, SourceLocation location, Component component) {
        super(name, location, component);
    }
    
    @Override
    public boolean isExecutable() {
        return true;
    }
    
    @Override
    protected File getDefaultOutputFile(String portName, File executionRoot) {
        return getRegularOutputFile(portName, executionRoot);
    }

    @Override
    public void launch(Engine engine) {
        Launcher launcher = getComponent().getLauncher();


        if (launcher == null) {
            engine.addError(new DynamicError("Couldn't find a launcher", this));
            return;
        }

        int status;
        try {
            // In case of previous abnormal termination of execution
            // where such directory could not be removed.
            engine.getExecutionDirectory().clearTempDirectory(this);
            status = launcher.launch(this,
                    engine.getExecutionDirectory().getCommandFile(this),
                    engine.getNetwork().getRepository(), engine.getExecutionDirectory());

            if (status == 0) {
                File root = engine.getExecutionDirectory().getRoot();
                boolean errorsExist = getOutputFile(
                        getComponent().getOutPort(Component.ERRORS_PORT_NAME), root).exists();
                if (!errorsExist) {
                    engine.getExecutionDirectory().removeTempDirectory(this);
                }
            }
        }
        catch (IOException e) {
            engine.getExecutionDirectory().removeTempDirectory(this);
            engine.addError(new DynamicError("I/O error when executing", this, e));
            return;
        }

        if (status != 0) {
            ErrorCode ec = ErrorCode.getErrorCode(status);
            String msg = "Component returned error status: "+status;
            if (ec != null) msg += " ("+ec.getDesc()+")";
            engine.addError(new DynamicError(msg, this));
        }
    }

}
