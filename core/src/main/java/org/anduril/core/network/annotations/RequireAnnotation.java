package org.anduril.core.network.annotations;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.ParameterType;
import org.anduril.core.network.Port;

public class RequireAnnotation extends Annotation {
	public RequireAnnotation() {
		super("require", Boolean.TRUE, ParameterType.BOOLEAN, false, true, false);
	}

    @Override
    public String validate(Object value, ComponentInstance ci, Port port) {
        return null;
    }

    @Override
    public void applyFinalization(Object value, ComponentInstance ci, Port port) {

    }
}