package org.anduril.core.network.annotations;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.ParameterType;
import org.anduril.core.network.Port;

public abstract class Annotation {
	public static final Annotation ENABLED = new EnabledAnnotation();
	public static final Annotation EXECUTE = new ExecuteAnnotation();
	public static final Annotation FILENAME = new FilenameAnnotation();
	public static final Annotation KEEP = new KeepAnnotation();
	public static final Annotation PRIORITY = new PriorityAnnotation();
	public static final Annotation REQUIRE = new RequireAnnotation();

	public final String name; 
	public final Object defaultValue;
	public final ParameterType exprType;
	public final boolean instanceScope;
	public final boolean inPortScope;
	public final boolean outPortScope;

    public Annotation(String name, Object defaultValue, ParameterType exprType,
    		boolean instanceScope, boolean inPortScope, boolean outPortScope) {
    	this.name = name;
    	this.defaultValue = defaultValue;
    	this.exprType = exprType;
    	this.instanceScope = instanceScope;
    	this.inPortScope = inPortScope;
    	this.outPortScope = outPortScope;
    }

    /**
     * Validate annotation value. This method can assume that the
     * value type and scope are correct.
     * @param value Annotation value.
     * @param ci Component instance holding the value.
     * @param port The port in question, or null for instance-level
     *  annotations.
     * @return Null if the value passes validation, or an
     *  error message otherwise.
     */
    public abstract String validate(Object value, ComponentInstance ci, Port port);

    /**
     * Method that is called for each annotation before the
     * component instance is added to the network.
     * @param value Annotation value.
     * @param ci Component instance holding the value.
     * @param port The port in question, or null for instance-level
     *  annotations.
     */
    public void applyAddToNetwork(Object value, ComponentInstance ci, Port port) { }

    public void applyFinalization(Object value, ComponentInstance ci, Port port) { }

    /*
    public boolean isValidType(Object value) {
    	if (value == null) return true;
    	
    	switch(this.exprType) {
    	case BOOLEAN:
    		return (value instanceof Boolean);
    	case FLOAT:
    		return (value instanceof Double) || (value instanceof Integer);
    	case INT:
    		return (value instanceof Double) || (value instanceof Integer);
    	case STRING:
    		return (value instanceof Boolean);
    	}
    	return true;
    }
    */
    
    public String toString() {
        return this.name;
    }

    public static boolean convertBoolean(Object value) {
        if (value instanceof Boolean) {
            return (Boolean) value;
        } else if (value instanceof String) {
            String stringVal = (String) value;
            if (stringVal.equals("true")) return true;
            else if (stringVal.equals("false")) return false;
            else throw new IllegalArgumentException("Can not convert value to boolean: "+value);
        } else {
            throw new IllegalArgumentException("Can not convert value to boolean: "+value);
        }
    }
    
    public static double convertDouble(Object value) {
    	if (value instanceof Integer) {
    		return (Integer)value;
    	} else if (value instanceof Double) {
    		return (Double)value;
    	} else {
    		throw new IllegalArgumentException("Can not convert value to double: "+value);
    	}
    }
    
    public static int convertInt(Object value) {
    	if (value instanceof Integer) {
    		return (Integer)value;
    	} else if (value instanceof Double) {
    		double dbl = (Double)value;
    		return (int)Math.round(dbl);
    	} else {
    		throw new IllegalArgumentException("Can not convert value to integer: "+value);
    	}
    }


}
