package org.anduril.core.network.componentInstance;

import org.anduril.component.Tools;
import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.AbstractComponentInstance;
import org.anduril.core.network.Component;
import org.anduril.core.network.OutPort;
import org.anduril.core.network.SourceLocation;

import java.io.File;
import java.io.IOException;

/**
 * Test component implementation for the techtest bundle to support
 * engine unit tests. Used for components C1 and C2 in techtest.
 */
public class TestComponentInstance extends AbstractComponentInstance {
    
    public TestComponentInstance(String[] name, SourceLocation location, Component component) {
        super(name, location, component);
    }

    @Override
    protected File getDefaultOutputFile(String portName, File executionRoot) {
        return getRegularOutputFile(portName, executionRoot);
    }

    @Override
    public boolean isExecutable() {
        return false;
    }

    @Override
    public void launch(Engine engine) {
        Integer nRows = Integer.parseInt(getParameterValue("nRows").toString());
        StringBuilder sb = new StringBuilder(256);
        for (int i=1; i<10; i++) {
            sb.setLength(0);
            final OutPort port = getComponent().getOutPort("out"+i);
            if (port == null) continue;
            File outFile = getOutputFile(port, engine.getExecutionDirectory().getRoot());
            if (outFile != null) {
                try {
                    outFile.getParentFile().mkdirs();
                    if (nRows != null && nRows > 0) {
                        sb.append("header\n");
                        for (int j = 0; j < nRows; j++) 
                            sb.append("row" + (j+1) + "\n");
                        Tools.writeString(outFile, sb.toString());
                    }
                    else {
                        Tools.writeString(outFile, getName());
                    }
                } catch(IOException e) {
                    engine.addError(new DynamicError("I/O error when writing "+port+": "+e, this));
                }
            }
        }
        
        double delay = Double.parseDouble(getParameterValue("delay").toString());
        if (delay > 0) {
            try {
                Thread.sleep((long)delay*1000);
            } catch(InterruptedException e) {
                engine.addError(new DynamicError("Sleep was interruped", this));
            }
        }
        
        final String failValue = getParameterValue("fail").toString();
        if (failValue != null && failValue.equals("true")) {
            engine.addError(new DynamicError("Test component was set to fail", this));
        }

        final int ensureStage = Integer.parseInt(getParameterValue("ensureStage").toString());
        if (ensureStage >= 0) {
            if (engine.getStage() != ensureStage) {
                String msg = String.format("Invalid engine stage: expected %d, got %d",
                        ensureStage, engine.getStage());
                engine.addError(new DynamicError(msg, this));
            }
        }
    }

}
