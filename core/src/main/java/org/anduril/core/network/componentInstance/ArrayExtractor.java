package org.anduril.core.network.componentInstance;

import org.anduril.component.IndexFile;
import org.anduril.component.Tools;
import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.utils.IOTools;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ArrayExtractor extends AbstractComponentInstance {
    public static final String DUMMY_OUTPUT = "_dummy";
    
    private Map<String,File> outPortMap;
    
    public ArrayExtractor(String[] name, SourceLocation location,  Component component) {
        super(name, location, component);
        this.outPortMap = null;
    }

    private File getDummyFile(File executionRoot) {
        String name = getName()+"/"+DUMMY_OUTPUT;
        return new File(executionRoot, name);
    }
    
    private File getLinkFile(String portName, File executionRoot) {
        File path = getRegularOutputFile(portName, executionRoot);
        String basename = "_link_"+IOTools.removeFileExtension(path.getName());
        return new File(path.getParentFile(), basename);
    }
    
    @Override
    protected File getDefaultOutputFile(String portName, File executionRoot) {
        if (this.outPortMap != null) {
            File element = this.outPortMap.get(portName);
            if (element != null) return element;
        }
        
        File link = getLinkFile(portName, executionRoot);
        try {
            String path = Tools.readFile(link);
            return new File(path.trim());
        } catch (IOException e) {
            return getDummyFile(executionRoot);
        }
    }

    /**
     * None of the output ports of ArrayExtractor are actually
     * produced by it - instead they are links to files elsewhere.
     */
    @Override
    public boolean isFileReal(OutPort port, File executionRoot) {
    	return false;
    }

    @Override
    public boolean isExecutable() {
        return false;
    }

    @Override
    public void initialize(Network network) throws StaticError {
        super.initialize(network);
        
        if (!Annotation.convertBoolean(getInstanceAnnotation(Annotation.KEEP))) { 
        	setInstanceAnnotation(Annotation.KEEP, Boolean.TRUE);
        	getLogger().warning(String.format(
        		"ArrayExtractor must not have _keep=false; set to _keep=true"));
        }
    }
    
    @Override
    public void launch(Engine engine) {
        final File execRoot = engine.getExecutionDirectory().getRoot();
        
        try {
            File dummy = getDummyFile(execRoot);
            dummy.getParentFile().mkdirs();
            Tools.writeString(dummy, "");
        } catch (IOException e) {
            engine.addError(new DynamicError("Error writing dummy output file: "+e, this));
            return;
        }
        
        this.outPortMap = new HashMap<String, File>();
        
        final Map<String,Integer> keyMap = new HashMap<String, Integer>();
        for (int i=1; i<=Port.ARRAY_EXTRACTOR_PORTS; i++) {
            String paramName = "key"+i;
            String key = getParameterValue(paramName).toString();
            if (!key.isEmpty()) {
                keyMap.put(key, i);
            }
        }
        
        HashSet<String> seenKeys = new HashSet<String>();
        IndexFile inputIndex;
        try {
            inputIndex = IndexFile.read(
                    getInputArrayIndex(getComponent().getInPort(1),
                    engine.getExecutionDirectory().getRoot()).getAbsoluteFile());
        } catch(IOException e) {
            engine.addError(new DynamicError("Error reading array index: "+e, this));
            return;
        }

        for (String key: inputIndex) {
            Integer position = keyMap.get(key);
            if (position != null) {
                String outPort = "file" + position.intValue();
                File target = inputIndex.getFile(key);
                this.outPortMap.put(outPort, target);
                seenKeys.add(key);
                File link = getLinkFile(outPort, execRoot);
                try {
                    Tools.writeString(link, target.getAbsolutePath());
                } catch (IOException e) {
                    engine.addError(new DynamicError("Error writing link file: "+e, this));
                }
            }
        }
        
        keyMap.keySet().removeAll(seenKeys);
        if (!keyMap.isEmpty()) {
        	engine.addError(new DynamicError("Key(s) not found in array: "+keyMap.keySet(), this));
        }
    }
 
}
