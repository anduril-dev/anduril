package org.anduril.core.network;

import java.io.File;

/**
 * Represents a location is a source file. Used to
 * track down the definition of a component instance
 * in a network configuration file, for example.
 * The location is composed of a file name, line number and
 * column number. Any of these may be unknown (null).
 */
public class SourceLocation {

    public static final int MISSING_VALUE = -1;
    
    private File file;
    private String className;
    private String methodName;
    private int line;
    private int column;

    /**
     * Initialize.
     * @param file Name of the originating file. May be null.
     * @param className Class name. May be null.
     * @param methodName Method name. May be null.
     * @param line Line number, starting from 1. Missing value is -1.
     * @param column Column number, starting from 1. Missing value is -1.
     */
    public SourceLocation(File file, String className, String methodName, int line, int column) {
        this.file = file;
        this.className = className;
        this.methodName = methodName;
        this.line = line;
        this.column = column;
    }

    public SourceLocation(File file) {
        this(file, null, null, MISSING_VALUE, MISSING_VALUE);
    }

    /**
     * Initialize empty location where all attributes are missing.
     */
    public SourceLocation() {
        this(null, null, null, MISSING_VALUE, MISSING_VALUE);
    }
    
    /**
     * Returns string with current member values.
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        if (this.file != null) {
            buffer.append(this.file.getName());
        }
        if (this.line != MISSING_VALUE) {
            buffer.append(':');
            buffer.append(this.line);
        }
        if (this.column != MISSING_VALUE) {
            buffer.append(':');
            buffer.append(this.column);
        }

        return buffer.toString();
    }
    
    /**
     * Return the name of the originating file.
     */
    public File getFile() {
        return file;
    }

    public String getClassName() {
        return this.className;
    }

    public String getMethodName() {
        return this.methodName;
    }

    /**
     * Return the line in the originating file, starting from 1.
     * May be null.
     */
    public Integer getLine() {
        return this.line == MISSING_VALUE ? null : this.line;
    }

    /**
     * Return the column in the originating file, starting from 1.
     * May be null.
     */
    public Integer getColumn() {
        return this.column == MISSING_VALUE ? null : column;
    }
    
    /**
     * Return true if all location attributes are null. Return
     * false if any attribute is non-null.
     */
    public boolean allNull() {
        return file == null &&
                className == null &&
                methodName == null &&
                line == MISSING_VALUE &&
                column == MISSING_VALUE;
    }

    @Override
    public int hashCode() {
        return ((3571 * line) ^ column);
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof SourceLocation))
            throw new ClassCastException("SourceLocation objects expected.");
        
        SourceLocation loc1 = (SourceLocation) o;
        
        boolean sameFiles = false;
        if (this.getFile() == null && loc1.getFile() == null) {
            sameFiles = true;
        }
        else if (this.getFile() != null && loc1.getFile() != null &&
                     this.getFile().getAbsoluteFile().equals(loc1.getFile().getAbsoluteFile())) {
            sameFiles = true;
        }
        
        if (this.getLine() - loc1.getLine() == 0 &&
                this.getColumn() - loc1.getColumn() == 0 &&
                sameFiles) {
            return true;
        }

        return false;
    }

}
