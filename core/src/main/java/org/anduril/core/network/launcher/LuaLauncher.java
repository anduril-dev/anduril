package org.anduril.core.network.launcher;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LuaLauncher extends Launcher {
    private File sourceFile;
    private static String executable = "lua";

    public LuaLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The Lua launcher requires the argument 'file'");
        }
        this.sourceFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.sourceFile);
    }
    
    /**
     * Return the Lua script source file.
     */
    public File getSourceFile() {
        return sourceFile;
    }

    @Override
    public Map<String, String> getExtraEnvironment(Repository repository) {
        final String LUA_PATH = "LUA_PATH";
        final String LUA_CPATH = "LUA_CPATH";
        final String sep = ";";
        
        String luaPath = System.getenv(LUA_PATH);
        if (luaPath == null) luaPath = "";
        else luaPath += sep;
        
        String luaCPath = System.getenv(LUA_CPATH);
        if (luaCPath == null) luaCPath = "";
        else luaCPath += sep;
        
        File skeleton = new File(repository.getEngineJAR().getParentFile(), "lua");
        luaPath += skeleton.getAbsolutePath() + "/?.lua" + sep;
        
        for (Bundle bundle: repository.getBundles()) {
            File luaLib = new File(bundle.getLibDirectory(), "lua");
            if (!luaLib.exists()) continue;
            luaPath += luaLib.getAbsolutePath() + "/?.lua" + sep;
            luaCPath += luaLib.getAbsolutePath() + "/?.so" + sep;
            luaCPath += luaLib.getAbsolutePath() + "/?.dll" + sep;
        }
        
        /* Add default Lua search paths */
        luaPath += sep;
        luaCPath += sep;
        
        Map<String, String> env = new HashMap<String, String>();
        env.put(LUA_PATH, luaPath);
        env.put(LUA_CPATH, luaCPath);
        return env;
    }
    
    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {
        String[] cmd = new String[] {
            executable,
            getSourceFile().getAbsolutePath(),
            commandFile.getPath()
        };

        return cmd;
    }

}
