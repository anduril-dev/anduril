package org.anduril.core.network;

import org.anduril.component.ArrayTypeFunctionality;
import org.anduril.core.Version;
import org.anduril.core.utils.IOTools;
import org.anduril.core.utils.TextTools;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Data type that is associated with a port. A data type contains
 * documentation about physical file layout that is important in
 * enabling component authors to use the type.
 */
public class DataType implements Comparable<DataType> {
    /** 
     * Name of the StringList built-in data type.
     */
    public final static String STRING_LIST_NAME = "StringList";

    /** Name of the TopType built-in data type. */
    public final static String TOP_TYPE_NAME = "TopType";

    private String name;
    private boolean generic;
    private DataType parentType;
    private List<DataType> children;
    private String doc;
    private PresentationType ptype;
    private String presentationDesc;
    private String extension;
    private Version version;
    
    private Set<File> docResources;
    private List<File> exampleFiles;
    private Bundle bundle;
    
    private Class<? extends DataTypeFunctionality> functionalityClass;
    
    /**
     * Initialize a normal, non-generic data type.
     * @param name Name of the data type. This must be unique among data types.
     * @param parentType Parent data type, or null if the type is a top-level
     * 	type.
     * @param desc Documentation string. This gives a high level description of
     * 	the type.
     * @param ptype Presentation type on disk: file or directory.
     * @param presentationDesc Free-format description of the physical
     * 	representation of the data type. This should document the file
     * 	layout in enough detail that component authors are able to use
     * 	the data type.
     * @param extension For file types, a file extension. This may be null.
     */
    public DataType(String name, DataType parentType, String desc, PresentationType ptype, String presentationDesc,
            String extension, Version version, boolean generic) {
        if (!IOTools.isValidName(name)) {
            throw new IllegalArgumentException("Name is not valid: "+name);
        }
        if (!generic && parentType == null && !name.equals(TOP_TYPE_NAME)) {
            throw new IllegalArgumentException("Only the top type is allowed to have null parent type: "+name);
        }
        if (name.equals(TOP_TYPE_NAME) && parentType != null) {
            throw new IllegalArgumentException("The top type must have null parent type");
        }
        
        this.name = name;
        this.generic = generic;

        this.parentType = parentType;
        this.children = new ArrayList<DataType>();
        this.doc = desc;
        this.ptype = ptype;
        this.presentationDesc = presentationDesc;
        this.extension = extension;
        this.version = version;
        this.docResources = new HashSet<File>();
        this.exampleFiles = new ArrayList<File>();
        this.functionalityClass = parentType == null ? DataTypeFunctionality.class
                : parentType.getFunctionalityClass();
        
        if (parentType != null) {
            parentType.addChild(this);
        }
    }

    /**
     * Return a generic type.
     * @param name Name of the data type.
     */
    public static DataType buildGenericType(String name) {
        DataType type = new DataType(name, null, null, null, null, null, null, true);
        return type;
        
    }
    
    public String toString() {
        return "DataType: "+name;
    }
    
    /**
     * Return data type name.
     */
    public String getName() {
        return name;
    }

    public Bundle getBundle() {
        return this.bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    /**
     * Return the parent type if this type, or null if this
     * type is a root type.
     */
    public DataType getParentType() {
        return parentType;
    }

    public void addChild(DataType child) {
        if (child.getParentType() != this) {
            String s = String.format("Parent type of %s is not this type but %s", child.getName(), child.getParentType());
            throw new IllegalArgumentException(s);
        }

        if (children.contains(child) == false)
            children.add(child);
    }

    public List<DataType> getChildren() {
        return children;
    }

    /**
     * Return all ancestor types of this type. This
     * includes the parent, the parent of parent, etc.
     * The list is empty if this type is a root type.
     */
    public List<DataType> getAncestors() {
        List<DataType> ancestors = new ArrayList<DataType>();
        
        DataType type = this;
        while (type != null) {
            ancestors.add(type);
            type = type.getParentType();
        }
        
        return ancestors;
    }
    
    /**
     * Return all descendant types (sub types) of this type.
     * Descendants include this type it self, all children,
     * all grand children, etc.
     */
    public List<DataType> getDescendants() {
        List<DataType> descendants = new ArrayList<DataType>();
        descendants.add(this);
        for (DataType child: children) {
            descendants.addAll(child.getDescendants());
        }
        return descendants;
    }

    /**
     * Return true if this type is a subtype of given
     * type. A is a subtype of B if there is a path
     * (of any length) from B to A in the tree of types.
     */
    public boolean isSubTypeOf(DataType another) {
        if (this.equals(another)) return true;
        DataType type = this;
        while (type != null) {
            if (type.equals(another)) return true;
            type = type.getParentType();
        }
        return false;
    }

    /**
     * Return the documentation string. This gives a high level description of
     * the type.
     */
    public String getDoc() {
        return doc;
    }
    
    /**
     * Set documentation string.
     */
    public void setDoc(String doc) {
        this.doc = doc;
    }
    
    /**
     * Return a short documentation string that usually fits into one
     * or two lines. This may be null. This is the first sentence of the
     * documentation string. The end of the sentence is found by the
     * regular expression "[.]\\s+[A-Z]", that is, a dot followed by
     * at least whitespace followed by a capital character A-Z.
     */
    public String getShortDoc() {
        return TextTools.getShortDoc(doc);
    }
    
    /**
     * Return the presentation type of the data type.
     */
    public PresentationType getPresentationType() {
        return ptype;
    }
    
    /**
     * Set the presentation type.
     */
    public String getPresentationDesc() {
        return presentationDesc;
    }
    
    /**
     * Return the file extension of the data type. This is only used
     * for file types.
     * @param isArray If true, the query concerns a port that has
     *  array type. Array types do not have extension.
     */
    public String getExtension(boolean isArray) {
        return isArray ? null : this.extension;
    }
    
    /**
     * Return true if the type has an associated non-empty
     * file extension.
     * @param isArray If true, the query concerns a port that has
     *  array type. Array types do not have extension.
     */
    public boolean hasExtension(boolean isArray) {
        return !isArray && extension != null && !extension.isEmpty();
    }
    
    /**
     * Return the version number of the type.
     */
    public Version getVersion() {
        return version;
    }
    
    /**
     * Return true if this data type represent a
     * generic type.
     */
    public boolean isGeneric() {
        return generic;
    }

    /**
     * Return the example files for this data types that
     * are used for documentation.
     */
    public List<File> getExampleFiles() {
        return exampleFiles;
    }

    /**
     * Add an example files for this data types that
     * is used for documentation.
     */
    public void addExampleFile(File file) {
        addDocResource(file);
        exampleFiles.add(file);
    }
    
    /**
     * Return documentation resource files. These are copied into
     * the HTML documentation directory of the component.
     * The files may be regular files or directories.
     * Directories are copied recursively.
     */
    public Set<File> getDocResources() {
        return docResources;
    }
    
    /**
     * Register a documentation resource file for the component.
     * The file may be a regular file or a directory. The file
     * should not be the name of the doc-files directory that
     * holds resource files, but rather a file (or directory)
     * that is located in the doc-files directory.
     */
    public void addDocResource(File docResource) {
        if (docResource == null) {
            throw new NullPointerException("docResource is null");
        }
        this.docResources.add(docResource);
    }
    
    public Class<? extends DataTypeFunctionality> getFunctionalityClass() {
        return this.functionalityClass;
    }
    
    public void setFunctionalityClass(Class<? extends DataTypeFunctionality> functionalityClass) {
        this.functionalityClass = functionalityClass;
    }
    
    public DataTypeFunctionality makeFunctionalityInstance(boolean isArray) throws RuntimeException {
        if (isArray) {
            return new ArrayTypeFunctionality(this);
        }
        
        Constructor<? extends DataTypeFunctionality> cons ;
        try {
            cons = functionalityClass.getConstructor(DataType.class);
        } catch(NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        try {
            return cons.newInstance(this);
        } catch(IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch(InstantiationException e) {
            throw new RuntimeException(e);
        } catch(InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
    
    public boolean equals(Object obj) {
        if (!(obj instanceof DataType)) return false;
        else return getName().equals(((DataType)obj).getName());
    }
    
    public int compareTo(DataType other) {
        return getName().compareTo(other.getName());
    }
    
}
