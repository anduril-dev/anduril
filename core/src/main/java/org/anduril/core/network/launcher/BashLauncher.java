package org.anduril.core.network.launcher;

import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Execute a component by invoking Bash with a script
 * file. The launcher has one mandatory argument,
 * <code>file</code>, that gives the name of the Bash
 * shell script relative to component main directory.
 */
public class BashLauncher extends Launcher {
    private File scriptFile;
    
    public BashLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The Bash launcher requires the argument 'file'");
        }
        this.scriptFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.scriptFile);
    }
    
    /**
     * Return the path of the script file.
     */
    public File getScriptFile() {
        return scriptFile;
    }

    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {

        String[] cmd = new String[] {
            "bash",
            getScriptFile().getAbsolutePath(),
            commandFile.getPath()
        };

        return cmd;
    }
}
