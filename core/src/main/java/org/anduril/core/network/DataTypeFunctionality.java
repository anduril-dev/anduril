package org.anduril.core.network;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import org.anduril.component.Tools;
import org.anduril.core.utils.IOTools;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Functionality related to a data type.
 */
public class DataTypeFunctionality {

    public static final String REGEXP_START = "{{";
    public static final String REGEXP_END = "}}";
    
    private DataType type;
    
    public DataTypeFunctionality(DataType type) {
        if (type == null) throw new NullPointerException("type is null");
        this.type = type;
    }
    
    /**
     * Return the associated data type.
     */
    public final DataType getType() {
        return type;
    }
    
    /**
     * Compare two files for equality using a data type specific
     * comparison method. The implementation of this method can
     * assume that the files have compatible types. The default
     * implementation compares the contents of files (or directories)
     * byte-wise.
     *  
     * @param file1 A file (or directory) that represents this type.
     * @param file2 A file (or directory) that represents this type
     * 	or some sub-type of this type.
     * @param difference An initially empty buffer where a human-readable
     * 	description of the differences between the files should be written.
     * 	If the files match, the buffer must be left empty.
     * @return True if the contents of the files are considered equal;
     *  false otherwise.
     * @throws IOException If one of the files can not be read.
     */
    public boolean compare(File file1, File file2, StringBuffer difference)
            throws IOException {
    	if (getType().getPresentationType() == PresentationType.MULTIFILE) {
    		return compareMultifile(file1, file2, difference);
    	} else if (file1.isFile() && file2.isFile()) {
            return compareFiles(file1, file2, difference);
        } else if (file1.isDirectory() && file2.isDirectory()) {
            return compareDirectories(file1, file2, difference);
        } else {
            return false;
        }
    }
    
    protected boolean compareFiles(File file1, File file2, StringBuffer difference)
            throws IOException {
        boolean equal = IOTools.compareFileContents(file1, file2);
        if (equal) return equal;
        
        final long MAX_DIFF_LENGTH = 10000; 
        if (file1.isFile() && file2.isFile() &&
                file1.length() < MAX_DIFF_LENGTH && file2.length() < MAX_DIFF_LENGTH) {
            String[] lines1 = IOTools.readLines(file1.getAbsolutePath());
            String[] lines2 = IOTools.readLines(file2.getAbsolutePath());
            Patch patch = DiffUtils.diff(Arrays.asList(lines1), Arrays.asList(lines2));
            for (Delta delta: patch.getDeltas()) {
            	difference.append(delta);
            	difference.append('\n');
            }
        }
        
        return equal;
    }
    
    protected boolean compareDirectories(File dir1, File dir2, StringBuffer difference)
        throws IOException {
        IOTools.DirectoryDiff diff = IOTools.compareDirectories(dir1, dir2, "[.].*");
        if (diff.same()) return true;
        difference.append(diff.toString());
        return false;
    }
    
    protected boolean compareMultifile(File file1, File file2, StringBuffer difference) throws IOException {
    	for (File[] files: Tools.mapMultifilePaths(file1, file2)) {
    		File elem1 = files[0];
    		File elem2 = files[1];
    		boolean ok;
            if (file1.isFile() && file2.isFile()) {
            	ok = compareFiles(elem1, elem2, difference);
            } else if (file1.isDirectory() && file2.isDirectory()) {
            	ok = compareDirectories(elem1, elem2, difference);
            } else {
                return false;
            }
    		if (!ok) return false;
    	}
    	return true;
    }
    
    /**
     * Compare two strings for equality, using regular expression evaluation
     * if the first string contains a regexp pattern. Regular expressions are
     * detected by the prefix REGEXP_START and the suffix REGEXP_END.
     * If value1 does not contain a pattern, the strings are compared
     * character-by-character.
     */
    public static boolean compareRegexp(String value1, String value2) {
        int reStart = value1.indexOf(DataTypeFunctionality.REGEXP_START);
        if (reStart < 0) return value1.equals(value2);

        final int    sLENGTH = DataTypeFunctionality.REGEXP_START.length();
        final int    eLENGTH = DataTypeFunctionality.REGEXP_END.length();
        StringBuffer pattern = new StringBuffer(value1.length()*2);
        int          pos     = 0;
        while (reStart >= 0) {
        	if (pos < reStart)
        	   pattern.append(Pattern.quote(value1.substring(pos, reStart)));
        	pos = value1.indexOf(DataTypeFunctionality.REGEXP_END, reStart+sLENGTH);

        	// THIS IS A REGEXP_END SPECIFIC HACK TO ENABLE REGEXPS ENDING WITH '}'
        	if ((pos+eLENGTH < value1.length()) && (value1.charAt(pos+eLENGTH)=='}'))
        		pos++;

            if (pos < 0) {
/*                Log.warning("Unterminated regular expression ("+value1+").");*/
                pos = reStart;
                break;
            }
        	pattern.append(value1.substring(reStart+sLENGTH, pos));
        	reStart = value1.indexOf(DataTypeFunctionality.REGEXP_START, pos+sLENGTH);
        	pos += eLENGTH;
        }
        if (pos < value1.length())
        	pattern.append(Pattern.quote(value1.substring(pos)));
        return Pattern.matches(pattern.toString(), value2);
    }

}