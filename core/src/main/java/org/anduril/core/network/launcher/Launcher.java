package org.anduril.core.network.launcher;

import org.anduril.core.engine.ExecutionDirectory;
import org.anduril.core.network.Bundle;
import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;
import org.anduril.core.utils.IOTools;
import org.apache.commons.lang.ArrayUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Launcher executes the executable file associated to a
 * component.
 */
public abstract class Launcher {

    private static String[] wrapperPrefix = null;

    private String type;
    private Map<String,String> args;
    private Component component;
    private List<File> sourceFiles;

    public static String[] getWrapperPrefix() {
        return Launcher.wrapperPrefix;
    }

    public static void setWrapperPrefix(String[] wrapperPrefix) {
        Launcher.wrapperPrefix = wrapperPrefix;
    }

    /**
     * Initialize.
     * @param type Launcher type.
     * @param args Launcher parameters that are used to configure
     * 	the launcher. The set of parameters depends on the concrete
     * 	launcher subclass.
     * @param component The component associated with the launcher.
     */
    public Launcher(String type, Map<String,String> args, Component component) {
        this.type = type;
        this.args = args;
        this.component = component;
    }
    
    /**
     * Return launcher type.
     */
    public String getType() {
        return type;
    }
    
    /**
     * Return true if the argument with given name is
     * bound to a value.
     * @param name Name of the argument.
     */
    public boolean hasArgument(String name) {
        return args.containsKey(name);
    }

    public Map<String,String> getArgs() {
        return args;
    }

    /**
     * Return the value of the argument, or null if the
     * value is not set.
     * @param name Name of the argument.
     */
    public String getArgument(String name) {
        return args.get(name);
    }

    /**
     * Return the component associated with the launcher.
     */
    public Component getComponent() {
        return component;
    }
    
    /**
     * Return a command-line string array that is used to launch a component
     * instance. 
     *
     * @param ci Component instance.
     * @param commandFile The path of the command file that contains
     *  parameters for the executable. This path must not be mapped using
     *  path mapper.
     * @param repository The component repository that contains the
     *  definition of the interface of ci.
     * */
    public abstract String[] getCommand(ComponentInstance ci, File commandFile,
                                        Repository repository) throws IOException;
    
    /**
     * Return extra environment variables for the child process as
     * a map. These are added to default environment variables so that
     * returned values replace possibly existing ones. If null is returned,
     * no extra variables are defined. The default implementation returns
     * null.
     * 
     * @param repository Current component repository. Can be queried
     *  for engine main location.
     */
    public Map<String, String> getExtraEnvironment(Repository repository) {
        return null;
    }
    
    /**
     * Return the list of plain-text source files associated to this
     * launcher, if any. Source files are included in component documentation.
     */
    public final List<File> getSourceFiles() {
        return this.sourceFiles;
    }
    
    public void setSourceFiles(String sourceAttr, File additional) {
        this.sourceFiles = new ArrayList<File>();
        if (sourceAttr != null) {
            for (String fn: sourceAttr.split(",")) {
                if (fn.isEmpty()) continue;
                this.sourceFiles.add(new File(component.getMainDirectory(), fn));
            }
        }
        
        if (additional != null && !this.sourceFiles.contains(additional)) {
            this.sourceFiles.add(additional);
        }
    }

    private void insertWrapperEnvironment(Map<String, String> env, ComponentInstance ci, ExecutionDirectory executionDirectory) {
        env.put("ANDURIL_COMPONENT_BUNDLE", ci.getComponent().getBundle().getName());
        env.put("ANDURIL_COMPONENT_NAME", ci.getComponent().getSimpleName());
        env.put("ANDURIL_COMPONENT_DIRECTORY", ci.getComponent().getMainDirectory().getAbsolutePath());
        env.put("ANDURIL_COMPONENT_EXECUTION_DIRECTORY", executionDirectory.getComponentInstanceRoot(ci).getAbsolutePath());
        env.put("ANDURIL_EXECUTION_DIRECTORY", executionDirectory.getRoot().getAbsolutePath());

        for (Map.Entry<String, String> entry: ci.getCustomAnnotation().entrySet()) {
            String normalizedKey = entry.getKey().toUpperCase();
            normalizedKey = normalizedKey.replaceAll("[^A-Z0-9_]", "_");
            env.put("ANDURIL_WRAPPER_" + normalizedKey, entry.getValue()); // TODO: deprecated
            env.put("ANDURIL_CUSTOM_" + normalizedKey, entry.getValue());
        }
    }
    
    /**
     * Launch a component instance executable using this launcher.
     * This method always uses the local host.
     * @param ci Component instance.
     * @param commandFile The command file that contains parameters
     * 	for the executable.
     * @param repository The component repository that contains the
     * 	definition of the interface of ci. Used by concrete launchers to
     * 	extract possible additional information such as bundle directories.
     * @return Return status. Zero means success an d non-zero means
     * 	failure. Return statuses are listed in ErrorCode.
     * @throws IOException If an I/O error occurred during launching,
     * 	for example, if the executable was not found.
     */
    public int launch(ComponentInstance ci, File commandFile, Repository repository, ExecutionDirectory executionDirectory)
            throws IOException {
        String[] cmd = getCommand(ci, commandFile, repository);
        File workDir = ci.getComponent().getMainDirectory();

        String homeDir = repository.getHomeDirectory().getAbsolutePath();

        if(workDir == null) {
            String bundleName = ci.getComponent().getBundle().getName();
            String componentPath = Bundle.COMPONENTS_DIR;
            workDir = new File(homeDir + File.separator
                    + bundleName + File.separator
                    + componentPath + File.separator
                    + ci.getComponent().getName());
        }

        Map<String, String> extraEnv = getExtraEnvironment(repository);
        Map<String, String> env;
        if (extraEnv != null) {
            env = new HashMap<String, String>(System.getenv());
            env.putAll(extraEnv);
        } else {
            env = null;
        }

        if (Launcher.wrapperPrefix != null) {
            cmd = (String[]) ArrayUtils.addAll(Launcher.wrapperPrefix, cmd);
            if (env == null) {
                env = new HashMap<String, String>(System.getenv());
            }
            insertWrapperEnvironment(env, ci, executionDirectory);
        }

        String[] envp = new String[0];
        if (env != null) {
            envp = new String[env.size()];
            int i = 0;
            for (Map.Entry<String, String> entry: env.entrySet()) {
                // Double quote - should quotes in the value be handled specially?
                // FIXME: Needs test case.
                envp[i] = String.format("%s=\"%s\"", entry.getKey(), entry.getValue());
                i++;
            }
        }
        
        File commandLineFile = new File(commandFile.getParentFile(), "_launch");

        BufferedWriter out = new BufferedWriter(new FileWriter(commandLineFile));
        out.write("#!/usr/bin/env anduril-run-instance"); out.newLine();

        out.write("cd " + workDir); out.newLine();
        for ( String s: envp ) {
            out.write(s + " ");
        }
        for ( String s: cmd ) {
            if (s!=null) {
                out.write("\"" + s + "\" "); // Double quote - should quotes inside cmd be escaped? FIXME: Needs test case.
            }
        }
        
        out.newLine();
        out.flush();
        out.close();
        commandLineFile.setExecutable(true);

        return IOTools.launch(cmd, workDir, env, null, ci.getLogger());
    }
    
}
