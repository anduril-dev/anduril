package org.anduril.core.network;

/**
 * How a data type is stored on disk: as file or as directory.
 */
public enum PresentationType {
    FILE ("file"),
    DIRECTORY ("directory"),
    MULTIFILE ("multifile");
    
    private String type;
    
    private PresentationType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }
}
