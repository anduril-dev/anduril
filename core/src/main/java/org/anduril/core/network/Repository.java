package org.anduril.core.network;

import org.anduril.core.commandline.BaseCommand;
import org.anduril.core.readers.BundleReader;
import org.anduril.core.readers.CategoryReader;
import org.anduril.core.readers.ComponentReader;
import org.anduril.core.readers.DataTypeReader;
import org.anduril.core.utils.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Collator;
import java.util.*;

/**
 * Repository stores static component interfaces and
 * data types.
 */
public class Repository {
    public static final String ENGINE_JAR = "anduril.jar";
    public static final String HOME_BUNDLES_DIR = "bundles";
    
    private File homeDirectory;
    private Logger logger;
    
    private Map<String,DataType> dataTypes;
    private ArrayList<DataType> orderedDataTypes;
    private Map<String,Category> categories;
    private Map<String,Author> authors;
    private Map<String,Component> components;
    private Map<String,Component> functions;
    private LinkedHashMap<String,Bundle> bundles;

    /**
     * List of paths for searching bundles. A single shared instance
     * is created in the first call to getBundleSearchPath.
     */
    private ArrayList<File> bundleSearchPath = null;

    /**
     * Initialize.
     * @param homeDirectory Home directory of the engine
     *  that contains the main JAR file.
     * @param logger Logger used for reporting.
     */
    public Repository(File homeDirectory, Logger logger) throws IOException {
        if (homeDirectory == null) {
            throw new NullPointerException("homeDirectory is null");
        }
        if (!homeDirectory.exists()) {
            String msg = String.format("Home directory %s does not exist", homeDirectory);
            throw new FileNotFoundException(msg);
        }
        if (!homeDirectory.isDirectory()) {
            String msg = String.format("Home directory %s is not a directory", homeDirectory);
            throw new IOException(msg);
        }
        
        if (logger == null) logger = Logger.getLogger("<repository>");
        
        this.homeDirectory = homeDirectory;
        this.logger = logger;

        final Collator comparator = getStringComparator();
        this.dataTypes = new TreeMap<String,DataType>(comparator);
        this.orderedDataTypes = new ArrayList<DataType>();
        this.categories = new TreeMap<String,Category>(comparator);
        this.authors = new TreeMap<String,Author>(comparator);
        this.components = new TreeMap<String,Component>(comparator);
        this.functions = new TreeMap<String,Component>(comparator);
        this.bundles = new LinkedHashMap<String, Bundle>();
    }
    
    /**
     * Initialize with home directory from ANDURIL_HOME and a
     * default environment.
     */
    public Repository() throws IOException {
        this(
                new File(System.getenv(BaseCommand.HOME_ENVIRONMENT)).getAbsoluteFile(),
                null);
    }
    
    /**
     * Create a string comparator that does not ignore character
     * case.
     */
    private Collator getStringComparator() {
        Collator collator = Collator.getInstance(Locale.US);
        collator.setStrength(Collator.TERTIARY);
        return collator;
    }

    /**
     * Return the home directory of the engine
     * that contains the main JAR file.
     */
    public File getHomeDirectory() {
        return homeDirectory;
    }
    
    /**
     * Return the main JAR file of the engine.
     */
    public File getEngineJAR() {
        return new File(getHomeDirectory(), Repository.ENGINE_JAR);
    }

    /**
     * Return a data type with given name.
     * @param name Name of data type.
     * @return Data type, or null if it doesn't exist.
     */
    public DataType getDataType(String name) {
        return dataTypes.get(name);
    }
    
    /**
     * Return a category with given name. Return
     * null if the category is not found.
     */
    public Category getCategory(String name) {
        return categories.get(name);
    }
    
    /**
     * Return an author with given name. Return
     * null if the author is not found.
     */
    public Author getAuthor(String name) {
        return authors.get(name);
    }
    
    /**
     * Return the set of all data types.
     */
    public List<DataType> getDataTypes() {
        return new ArrayList<DataType>(dataTypes.values());
    }

    /**
     * Return data types defined in given bundles.
     */
    public List<DataType> getDataTypes(Collection<Bundle> selectedBundles) {
        ArrayList<DataType> types = new ArrayList<DataType>();
        for (DataType type: this.dataTypes.values()) {
            if (selectedBundles.contains(type.getBundle())) {
                types.add(type);
            }
        }
        return types;
    }

    /**
     * Return the set of all authors.
     */
    public List<Author> getAuthorsList() {
    	for (Component comp : getComponents()) {
    	    if (comp == null) continue;
    		for (Author author : comp.getAuthors()) {
    			if (author != null && author.getEmail() != null)
    				authors.put(author.getName(), author);
    		}    		
    	}
        return new ArrayList<Author>(authors.values());
    }
    
    /**
     * Return the set of all categories.
     */
    public List<Category> getCategoriesList() {
        return new ArrayList<Category>(categories.values());
    }
    
    public List<Category> getTopCategories() {
    	List<Category> myCats = new ArrayList<Category>(getCategoriesList());
    	List<Category> myTopCats = new ArrayList<Category>();
    	
    	for (Category myCat : myCats) {
    		if (myCat.getDepth() == 1) {
    			myTopCats.add(myCat);
    		}
    	}
    	
    	return myTopCats;
    }
        
    private void flattenCategories(List<Category> aTopCats, List<Component> aComponents) {
    	if (aTopCats == null || aTopCats.isEmpty()) return;

    	for (Category myCat : aTopCats) {
    		List<Component> myComps = getComponentsByCategory().get(myCat.getName());
    		if (myComps != null && !myComps.isEmpty()) {
    			aComponents.addAll(this.getComponentsByCategory().get(myCat.getName()));
    		}
    		flattenCategories(myCat.getChildren(), aComponents);
    	}
    }
    
    public List<Component> getAllCategoryComponents(String aCategory) {
    	List<Component> myComponents = new ArrayList<Component>();
    	List<Category> myCategories = new ArrayList<Category>();
    	myCategories.add(getCategory(aCategory));
    	
    	flattenCategories(myCategories, myComponents);
    	
    	return myComponents;
    }
    
    private void buildCategoryTree(List<Category> aTopCats, List<Category> aCats) {
    	if (aTopCats == null || aTopCats.isEmpty()) {
    		return;
    	}

    	for (Category myCat : aTopCats) {
    		aCats.add(myCat);
    		buildCategoryTree(myCat.getChildren(), aCats);
    	}
    }
    
    public List<Category> getCategoryTree() {
    	List<Category> myCats = new ArrayList<Category>();
    	List<Category> myTopCats = getTopCategories();
    	   	
    	buildCategoryTree(myTopCats, myCats);
    	
    	return myCats;
    }
    
    /**
     * Return a map of all component categories and the list of
     * components that belong to each category.
     */
    public Map<String,List<Component>> getCategories() {
        Map<String,List<Component>> map = new TreeMap<String,List<Component>>(getStringComparator());
        for (Component comp: getComponents()) {
            for (String category: comp.getCategories()) {
                List<Component> catList = map.get(category);
                if (catList == null) {
                    catList = new ArrayList<Component>();
                    map.put(category, catList);
                }
                catList.add(comp);
            }
        }
        return map;
    }
    
    /**
     * Return a list of all authors.
     */    
    public List<Author> getAuthors() {
    	return new ArrayList<Author>(authors.values());
    }
    
    /**
     * Add a data type.
     * @throws IllegalArgumentException if a data type with the
     * 	same name exists.
     */
    public synchronized void addDataType(DataType type) {
        //if (dataTypes.containsKey(type.getName()))
        //    return;

        if (dataTypes.containsKey(type.getName())) {
            return;
//            String msg = String.format("Data type %s already exists", type.getName());
//            throw new IllegalArgumentException(msg);
        }
        
        dataTypes.put(type.getName(), type);
        orderedDataTypes.add(type);
    }

    public List<DataType> getOrderedDataTypes() {
        return this.orderedDataTypes;
    }

    public synchronized void addCategory(Category category) {
        if (categories.containsKey(category.getName())) {
            String msg = String.format("Category %s already exists", category.getName());
            throw new IllegalArgumentException(msg);
        }
        
        categories.put(category.getName(), category);
    }
    
    public synchronized void addAuthor(Author author) {
        if (authors.containsKey(author.getHtmlName())) {
            String msg = String.format("Author %s already exists", author.getHtmlName());
            throw new IllegalArgumentException(msg);
        }
        
        authors.put(author.getHtmlName(), author);
    }

    /**
     * Return the list of components that use a given data type
     * as input port or output port.
     * @param type The data type.
     * @param includeInput If true, include components that take
     * 	the data type as input.
     * @param includeOutput If true, include components that take
     * 	the data type as input.
     * @param candidates A set of components or functions
     * @throws IllegalArgumentException if both includeInput and
     * 	includeOutput are false.
     * @see #getComponents()
     * @see #getFunctions()
     */
    public List<Component> getDataTypeMembers(DataType              type,
    		                                  boolean               includeInput,
    		                                  boolean               includeOutput,
    		                                  Collection<Component> candidates) {
        if (!includeInput && !includeOutput) {
            throw new IllegalArgumentException("At least one of includeInput, includeOutput must be true");
        }
        List<Component> comps = new ArrayList<Component>();
        for (Component comp: candidates) {
            boolean matches = false;
            
            if (includeInput) {
                for (InPort port: comp.getInPorts(true)) {
                    if (port.getType().equals(type)) {
                        matches = true;
                        break;
                    }
                }
            }

            if (includeOutput) {
                for (OutPort port: comp.getOutPorts(true)) {
                    if (port.getType().equals(type)) {
                        matches = true;
                        break;
                    }
                }
            }
            
            if (matches) comps.add(comp); 
        }
        
        return comps;
    }

    /**
     * Return a component with given name.
     * @param name Name of component.
     * @return Component, or null if it doesn't exist.
     */
    public Component getComponent(String name) {
        return components.get(name);
    }
    
    /**
     * Return the set of all components.
     */
    public List<Component> getComponents() {
        return new ArrayList<Component>(components.values());
    }

    /**
     * Return components defined in given bundles.
     */
    public List<Component> getComponents(Collection<Bundle> selectedBundles) {
        ArrayList<Component> components = new ArrayList<Component>();
        for (Component type: this.components.values()) {
            if (selectedBundles.contains(type.getBundle())) {
                components.add(type);
            }
        }
        return components;
    }

    /**
     * Add a component.
     * @throws IllegalArgumentException if a component with the
     * 	same name exists.
     */
    public synchronized void addComponent(Component component) {
        if (components.containsKey(component.getName())) {
            String msg = String.format("Component %s already exists", component.getName());
            throw new IllegalArgumentException(msg);
        }
        String namespace = component.getBundle().getCompilePackageName();
        components.put(component.getNamespaceName(), component);
    }
    
    /**
     * Return a map of all component categories and the list of
     * components and functions that belong to each category.
     */
    public Map<String,List<Component>> getComponentsByCategory() {
        Map<String,List<Component>> map = new TreeMap<String,List<Component>>(getStringComparator());
        
        for (Component comp: getComponents()) {
            for (String category: comp.getCategories()) {
                List<Component> catList = map.get(category);
                if (catList == null) {
                    catList = new ArrayList<Component>();
                    map.put(category, catList);
                }
                catList.add(comp);
            }
        }

        for (Component func: getFunctions()) {
            for (String category: func.getCategories()) {
                List<Component> catList = map.get(category);
                if (catList == null) {
                    catList = new ArrayList<Component>();
                    map.put(category, catList);
                }
                catList.add(func);
            }
        }

        return map;
    }
    
    public Map<String,List<Component>> getComponentsByAuthor() {
        Map<String,List<Component>> map = new TreeMap<String,List<Component>>(getStringComparator());
        
        for (Component comp: getComponents()) {
            for (Author author: comp.getAuthors()) {
                List<Component> authorList = map.get(author.getHtmlName());
                if (authorList == null) {
                    authorList = new ArrayList<Component>();
                    map.put(author.getHtmlName(), authorList);
                }
                authorList.add(comp);
            }
        }

        for (Component func: getFunctions()) {
            for (Author author: func.getAuthors()) {
                List<Component> authorList = map.get(author.getHtmlName());
                if (authorList == null) {
                    authorList = new ArrayList<Component>();
                    map.put(author.getHtmlName(), authorList);
                }
                authorList.add(func);
            }
        }

        return map;
    }

    public List<Category> getCategoryChildren(String aCatName) {
    	if (aCatName == null) return new ArrayList<Category>();
    	
    	Category myCat = getCategory(aCatName);
    	if (myCat == null) return new ArrayList<Category>();
    	
    	List<Category> myCatChildren = myCat.getChildren();
    	if (myCatChildren == null) return new ArrayList<Category>();
    	
    	return myCatChildren;
    }

    /**
     * Return a function (composite component) with given name.
     * @param name Name of interface.
     * @return Function, or null if it doesn't exist.
     */
    public Component getFunction(String name) {
        return functions.get(name);
    }
    
    /**
     * Return the set of all functions (composite components).
     */
    public List<Component> getFunctions() {
        return new ArrayList<Component>(functions.values());
    }

    /**
     * Return functions defined in given bundles.
     */
    public List<Component> getFunctions(Collection<Bundle> selectedBundles) {
        ArrayList<Component> components = new ArrayList<Component>();
        for (Component type: this.functions.values()) {
            if (selectedBundles.contains(type.getBundle())) {
                components.add(type);
            }
        }
        return components;
    }

    /**
     * Add a function (composite component).
     * @throws IllegalArgumentException if a function or a component with the
     * 	same name exists.
     */
    public synchronized void addFunction(Component function) {
        if (functions.containsKey(function.getName())) {
            String msg = String.format("Function %s already exists", function.getName());
            throw new IllegalArgumentException(msg);
        }
        if (components.containsKey(function.getName())) {
            String msg = String.format("A component with the same name as function %s exists",
                    function.getName());
            throw new IllegalArgumentException(msg);
        }
        String namespace = function.getBundle().getCompilePackageName();
        functions.put(function.getNamespaceName(), function);
    }
    
    /**
     * Return a bundle with given name.
     * @param name Name of bundle.
     * @return Bundle, or null if it doesn't exist.
     */
    public Bundle getBundle(String name) {
        return bundles.get(name);
    }
    
    /**
     * Return the set of all bundles.
     */
    public List<Bundle> getBundles() {
        return new ArrayList<Bundle>(bundles.values());
    }
    
    /**
     * Add a bundle.
     * @throws IllegalArgumentException if a bundle with the
     * 	same name exists.
     */
    public synchronized void addBundle(Bundle bundle) {
        if (bundles.containsKey(bundle.getName())) {
            return;
//            String msg = String.format("Bundle %s already exists", bundle.getName());
//            throw new IllegalArgumentException(msg);
        }
        bundles.put(bundle.getName(), bundle);
    }
    
    /**
     * Return true if a bundle with the given name
     * has been loaded.
     */
    public boolean hasBundle(String bundleName) {
        return bundles.containsKey(bundleName);
    }

    public Bundle loadBundleFromXML(File bundleDir, Set<String> includeComponentNames) throws StaticError {
        if (hasBundle(bundleDir.getName())) {
            return getBundle(bundleDir.getName());
        }

        Bundle bundle = loadBundleXML(bundleDir);

        for (String unloadedBundleName: getUnloadedDependsBundles(bundle)) {
            File unloadedBundleDir = findBundle(unloadedBundleName);
            if (unloadedBundleDir == null) {
                String msg = String.format("Can not load bundle %s: dependency bundle %s not found",
                        bundle.getName(), unloadedBundleName);
                throw new StaticError(msg);
            }
            loadBundleFromXML(unloadedBundleDir, includeComponentNames);
        }

        File dataTypesXML = new File(bundleDir, Bundle.DATATYPES_XML_FILE);
        loadDataTypes(dataTypesXML, bundle);

        File categoriesXML = new File(bundleDir, Bundle.CATEGORIES_XML_FILE);
        loadCategories(categoriesXML);

        loadBundleComponents(bundle, includeComponentNames);

        loadBundleFunctions(bundle, includeComponentNames);

        addBundle(bundle);
        return bundle;
    }

    public Bundle loadBundleFromXML(File bundleDir) throws StaticError {
        return loadBundleFromXML(bundleDir, null);
    }

    private Bundle loadBundleXML(File bundleDir) throws StaticError {
        File xml = new File(bundleDir, Bundle.BUNDLE_XML_FILE);
        try {
            BundleReader reader = new BundleReader(xml, this);
            Bundle bundle = reader.parse();
            return bundle;
        } catch(IOException e) {
            throw new StaticError("Can't load bundle XML: "+e,
                    new SourceLocation(xml));
        } catch(ParserConfigurationException e) {
            throw new StaticError("Can't load bundle XML: "+e,
                    new SourceLocation(xml));
        } catch(SAXException e) {
            throw new StaticError("Can't load bundle XML: "+e,
                    new SourceLocation(xml));
        }
    }

    private void loadDataTypes(File dataTypesXML, Bundle bundle) throws StaticError {
        if (!dataTypesXML.exists()) return;

        try {
            DataTypeReader reader = new DataTypeReader(dataTypesXML, this, bundle);
            reader.parse();
        } catch(IOException e) {
            throw new StaticError("Can't load data type XML: "+e,
                    new SourceLocation(dataTypesXML));
        } catch(ParserConfigurationException e) {
            throw new StaticError("Can't load data type XML: "+e,
                    new SourceLocation(dataTypesXML));
        } catch(SAXParseException e) {
            throw new StaticError("Can't load data type XML: "+e.getMessage(),
                    new SourceLocation(dataTypesXML, null, null, e.getLineNumber(), e.getColumnNumber()));
        } catch(SAXException e) {
            throw new StaticError("Can't load data type XML: "+e,
                    new SourceLocation(dataTypesXML));
        } catch(IllegalArgumentException e) {
            throw new StaticError("Can't load data type XML: "+e,
                    new SourceLocation(dataTypesXML));
        }
    }

    private void loadCategories(File categoryFileXML) throws StaticError {
        if (!categoryFileXML.exists()) return;

        try {
            CategoryReader reader = new CategoryReader(categoryFileXML, this);
            reader.parse();
        } catch(IOException e) {
            throw new StaticError("Can't load category XML: "+e,
                    new SourceLocation(categoryFileXML));
        } catch(ParserConfigurationException e) {
            throw new StaticError("Can't load category XML: "+e,
                    new SourceLocation(categoryFileXML));
        } catch(SAXParseException e) {
            throw new StaticError("Can't load category XML: "+e.getMessage(),
                    new SourceLocation(categoryFileXML, null, null,
                            e.getLineNumber(), e.getColumnNumber()));
        } catch(SAXException e) {
            throw  new StaticError("Can't load category XML: "+e,
                    new SourceLocation(categoryFileXML));
        }
    }

    private void loadBundleComponents(Bundle bundle, Set<String> includeComponentNames) throws StaticError {
        for (File xmlFile: bundle.getComponentDescriptors(includeComponentNames)) {
            Component comp = loadComponentXML(xmlFile, bundle);
            addComponent(comp);
        }
    }

    private void loadBundleFunctions(Bundle bundle, Set<String> includeFunctionNames) throws StaticError {
        for (File functionXML: bundle.getNetworkDescriptors(includeFunctionNames)) {
            Component comp = loadComponentXML(functionXML, bundle);
            addFunction(comp);
        }
    }

    private Component loadComponentXML(File descriptorXML, Bundle bundle) throws StaticError {
        try {
            Component comp = new ComponentReader(descriptorXML, this).parse();
            comp.setBundle(bundle);
            return comp;
        } catch(IOException e) {
            throw new StaticError("Can't load component XML file: "+e,
                    new SourceLocation(descriptorXML));
        } catch(ParserConfigurationException e) {
            throw  new StaticError("Can't load component XML file: "+e,
                    new SourceLocation(descriptorXML));
        } catch(SAXParseException e) {
            throw new StaticError("Can't load component XML file: " + e.getMessage(),
                    new SourceLocation(descriptorXML, null, null,
                            e.getLineNumber(), e.getColumnNumber()));
        } catch(SAXException e) {
            throw new StaticError("Can't load component XML file: "+e,
                    new SourceLocation(descriptorXML));
        }
    }


    public List<File> getBundleSearchPath() {
        if (this.bundleSearchPath != null) {
            return this.bundleSearchPath;
        }

        this.bundleSearchPath = new ArrayList<File>();

        String bundlesEnv = System.getenv(BaseCommand.BUNDLES_ENVIRONMENT);

        if (bundlesEnv != null) {
            for (String searchDirName: bundlesEnv.split(System.getProperty("path.separator"))) {
                if (!searchDirName.isEmpty()) {
                    File searchDir = new File(searchDirName);
                    if (searchDir.exists()) {
                        this.bundleSearchPath.add(searchDir.getAbsoluteFile());
                    }
                }
            }
        }

        final File homeBundles = new File(getHomeDirectory(), HOME_BUNDLES_DIR);
        this.bundleSearchPath.add(homeBundles);

        return this.bundleSearchPath;
    }

    public File findBundle(String bundleName) {
        for (File searchDir: getBundleSearchPath()) {
            File bundleDir = new File(searchDir, bundleName);
            if (bundleDir.exists()) return bundleDir;
        }
        return null;
    }

    /**
     * Find bundles that are located under given root
     * directory.
     */
    public List<File> findAllBundles() {
        List<File> bundleDirs = new ArrayList<File>();
        for (File searchDir: getBundleSearchPath()) {
            for (File bundleDir: searchDir.listFiles()) {
                if (bundleDir.isDirectory()) {
                    File xml = new File(bundleDir, Bundle.BUNDLE_XML_FILE);
                    if (xml.exists()) {
                        bundleDirs.add(bundleDir);
                    }
                }
            }
        }

        return bundleDirs;
    }

    private List<String> getUnloadedDependsBundles(Bundle target) {
        ArrayList<String> unloadedBundleNames = new ArrayList<String>();
        for (String dependsBundle: target.getDependsBundleNames()) {
            if (!hasBundle(dependsBundle) && !dependsBundle.equals(target.getName())) {
                unloadedBundleNames.add(dependsBundle);
            }
        }
        return unloadedBundleNames;
    }

    // reference - http://stackoverflow.com/questions/11016092/how-to-load-classes-at-runtime-from-a-folder-or-jar
    public Bundle loadBundleFromJAR(File bundleDir) throws StaticError {
        if (hasBundle(bundleDir.getName())) {
            return getBundle(bundleDir.getName());
        }

        Bundle bundle = loadBundleXML(bundleDir);

        for (String unloadedBundleName: getUnloadedDependsBundles(bundle)) {
            File unloadedBundleDir = findBundle(unloadedBundleName);
            if (unloadedBundleDir == null) {
                String msg = String.format("Can not load bundle %s: dependency bundle %s not found",
                        bundle.getName(), unloadedBundleName);
                throw new StaticError(msg);
            }
            loadBundleFromJAR(unloadedBundleDir);
        }

        addBundle(bundle);
        return bundle;
    }

}
