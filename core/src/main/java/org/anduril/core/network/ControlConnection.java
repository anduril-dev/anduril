package org.anduril.core.network;

/**
 * Control connection specifies that a component needs to be
 * executed before another, but port data is not shared.
 */
public class ControlConnection extends Connection {
    
    public ControlConnection(ComponentInstance from, ComponentInstance to) {
        super(from, to);
    }

    @Override
    public boolean isControlConnection() {
        return true;
    }

}
