package org.anduril.core.network.annotations;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.ParameterType;
import org.anduril.core.network.Port;

public class ExecuteAnnotation extends Annotation {

    @Override
    public String validate(Object value, ComponentInstance ci, Port port) {
        if (value instanceof String) {
            String v = ((String)value).trim();
            for (Execute ev : Execute.values()) {
                if (ev.name().equals(v)) return null;
            }
            return String.format("Illegal value %s for " +
                    "execute annotation in ci %s", v, ci.getName());
        }
        else return String.format("Cannot cast %s to boolean " +
                "in enabled annotation in ci %s", value, ci.getName());
    }

    @Override
    public void applyFinalization(Object value, ComponentInstance ci, Port port) {

    }

    public static enum Execute {
        always,
        changed,
        once
    }
            
    public ExecuteAnnotation () {
    	super("execute", Execute.changed.toString(), ParameterType.STRING, true, false, false);
    }
    
}