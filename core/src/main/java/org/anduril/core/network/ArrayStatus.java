package org.anduril.core.network;

/**
 * Indicates whether a port is an array. 
 */
public enum ArrayStatus {
    /** The port is known to be an array. */
    TRUE,
    
    /** The port is known to not be an array. */
    FALSE,
    
    /** The port is array-generic and may be an array or non-array. */
    GENERIC;

    public boolean isTrue() {
        return this == TRUE;
    }
    
    public boolean isFalse() {
        return this == FALSE;
    }
    
    public boolean isGeneric() {
        return this == GENERIC;
    }
    
    /**
     * Return true if the port is may be an array (FALSE or GENERIC).
     */
    public boolean isPotentiallyArray() {
        return this != FALSE;
    }
    
    /**
     * Return true if two statuses do not contradict each other.
     * Contradiction occurs when the other is TRUE and the other
     * FALSE. Other combinations are compatible.
     * @param other Other status
     */
    public boolean isCompatible(ArrayStatus other) {
        if (this.isGeneric() || other.isGeneric()) return true;
        return this == other;
    }
    
    public static ArrayStatus fromBoolean(boolean isArray) {
        return isArray ? TRUE : FALSE;
    }
}