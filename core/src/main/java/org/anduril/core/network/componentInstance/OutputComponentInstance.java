package org.anduril.core.network.componentInstance;

import org.anduril.component.Tools;
import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.engine.ExecutionDirectory;
import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.utils.IOTools;

import java.io.File;
import java.io.IOException;

/**
 * Instance of the generic output component (OUTPUT). The output
 * component copies result files to a special output directory.
 * The output component has no associated executable file. The
 * component has one input port and one output port. The output
 * port shouldn't be connected to anything.
 */
public class OutputComponentInstance extends AbstractComponentInstance {
    
    /**
     * Name of both the input port and output port.
     */
    public static final String IN_PORT_NAME = "in";
    public static final String OUT_PORT_NAME = "out";
    
    /**
     * Name of the special output directory. This is created under
     * network execution root directory.
     */
    public static final String OUTPUT_DIRECTORY = "output";
    
    public OutputComponentInstance(String[] name, SourceLocation location, Component component) {
        super(name, location, component);
    }
    
    public File getOutputDirectory(File executionRoot) {
        return new File(executionRoot, OUTPUT_DIRECTORY);
    }

    protected File getDefaultOutputFile(String portName, File executionRoot) {
        if (portName.equals(Component.ERRORS_PORT_NAME) ||
                portName.equals(Component.LOG_PORT_NAME)) return null;

        PortConnection conn = getInConnection(getComponent().getInPort(IN_PORT_NAME));
        ComponentInstance from;
        Port fromPort;
        from = conn.getFrom();
        fromPort = conn.getFromPort();

        OutPort op = this.getComponent().getOutPort(OUT_PORT_NAME);
        String filenameAnn = (String)getOutPortAnnotation(Annotation.FILENAME, op);
        if (filenameAnn != null) {
        	return new File(getOutputDirectory(executionRoot), filenameAnn);
        }

        String fromCompName = from.getName();
        String fromPortName = fromPort.getName();
        String baseName = fromCompName+"-"+fromPortName;

        OutPort outPort = getInConnection(getComponent().getInPort(IN_PORT_NAME)).getNongenericFromPort();
        DataType type = outPort.getType();
        final boolean isArray = getPortIsArray(outPort).isPotentiallyArray();
        if (type.hasExtension(isArray)) {
            baseName += "."+type.getExtension(isArray);
        }
        return new File(getOutputDirectory(executionRoot), baseName);
    }
    
    public boolean isExecutable() {
        return false;
    }

    public void launch(Engine engine) {
        ExecutionDirectory execDir = engine.getExecutionDirectory();
        File primaryIn = getInputFile(OutputComponentInstance.IN_PORT_NAME, execDir.getRoot());
        
        if (!primaryIn.exists()) {
            String msg = "Source file not found: " + primaryIn.getAbsolutePath();
            engine.addError(new DynamicError(msg, this));
            return;
        }
        
        final File[] inFiles;
        OutPort outPort = getInConnection(getComponent().getInPort(IN_PORT_NAME)).getNongenericFromPort();
        final boolean isMultifile = outPort.getType().getPresentationType() == PresentationType.MULTIFILE;
        if (isMultifile) {
        	inFiles = Tools.getMultifilePaths(primaryIn);
        } else {
        	inFiles = new File[] { primaryIn };
        }
        
        boolean doLink = "true".equals(getParameterValue("link").toString()) ; 
        final String OS = System.getProperty("os.name").toLowerCase();
        // Consider implementing symbolic linking on Windows 7 and above - which means implementing the operating system test likewise
        boolean isUnix = OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 ;
               
        for (File inFile: inFiles) {
        	File out = getOutputFile(OutputComponentInstance.OUT_PORT_NAME, execDir.getRoot());
        	if (isMultifile) {
        		out = new File(out.getParentFile(), IOTools.removeFileExtension(out.getName())+Tools.getFileExtension(inFile));
        	}
        	try {
        	    if (doLink && isUnix) {
        	        String linkSource = inFile.toString().replace(execDir.getRoot().toString(), "..");
        	        IOTools.launch(new String [] { "/bin/ln", "-s", linkSource, out.toString()  }, null, null, null, null);
        	    } else {
                    if (primaryIn.isFile()) Tools.copyFile(inFile, out);
                    else Tools.copyDirectory(inFile, out);
        	    }
        	} catch(IOException e) {
        		engine.addError(new DynamicError("Could not copy output file", this, e));
        	}
        }
    }
}
