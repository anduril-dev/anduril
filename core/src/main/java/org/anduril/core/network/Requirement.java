package org.anduril.core.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents an environmental requirement of a component,
 * such as a specific R package or a JAR file. Requirements
 * can also be free-format text.
 */
public class Requirement {
    public static final String REQ_ANT = "ant";
    public static final String REQ_BASH = "bash";
    public static final String REQ_PYTHON = "python";
    public static final String REQ_PYTHON3 = "python3";
    public static final String REQ_DEBIAN = "DEB";
    public static final String REQ_JAVA_JAR = "jar";
    public static final String REQ_MAKEFILE = "make";
    public static final String REQ_MANUAL = "manual";
    public static final String REQ_R_BIOCONDUCTOR = "R-bioconductor";
    public static final String REQ_R_PACKAGE = "R-package";
//    public static final String REQ_R_SOURCE = "R-source";

    public static final String[] TYPES = new String[] {
        REQ_ANT, REQ_BASH, REQ_PYTHON, REQ_PYTHON3, REQ_DEBIAN, REQ_JAVA_JAR,
        REQ_MAKEFILE, REQ_MANUAL, REQ_R_BIOCONDUCTOR, REQ_R_PACKAGE,
    };
    
    public static final String[] VERSION_RELATIONS = new String[] {
        "<", "<<", "<=", "=", ">", ">=", ">>"
    };

    static {
        /* Arrays are sorted for fast binary searching */
        Arrays.sort(TYPES);
        Arrays.sort(VERSION_RELATIONS);
    }
    
    private final List<RequirementResource> resources;
    private Component owner;
    private final String name;
    private final String url;
    private final String versionRelation;
    private final String versionNumber;
    private final boolean optional;
    
    /**
     * Initialize.
     */
    public Requirement(String name, String url, String version, boolean optional) {
        this.resources = new ArrayList<RequirementResource>();
        this.name = name;
        this.url = url;
        this.optional = optional;
        
        if (version == null) {
            this.versionRelation = null;
            this.versionNumber = null;
        } else {
            String[] tokens = version.trim().split("[ ]+", 2);
            if (tokens.length != 2) {
                throw new IllegalArgumentException("Invalid requirement version: "+version);
            }
            this.versionRelation = tokens[0];
            if (Arrays.binarySearch(VERSION_RELATIONS, this.versionRelation) < 0) {
                throw new IllegalArgumentException("Invalid requirement version relation: "+this.versionRelation);
            }
            this.versionNumber = tokens[1];
        }
    }
    
    public void addResource(RequirementResource resource) {
        this.resources.add(resource);
    }

    public List<RequirementResource> getResources() {
        return this.resources;
    }
    
    public RequirementResource getResource(String... types) {
        for (String type: types) {
            for (RequirementResource resource: this.resources) {
                if (resource.getType().equals(type)) {
                    return resource;
                }
            }
        }
        return null;
    }
    
    public Component getOwner() {
        return this.owner;
    }
    
    public void setOwner(Component owner) {
        this.owner = owner;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getDisplayName() {
        if (this.name != null) return this.name;
        for (RequirementResource resource: this.resources) {
            return resource.getContents();
        }
        return "(NO NAME)";
    }
    
    public String getURL() {
        return this.url;
    }
    
    public String getVersionRelation() {
        return this.versionRelation;
    }
    
    public String getVersionNumber() {
        return this.versionNumber;
    }
    
    public boolean getOptional() {
        return this.optional;
    }
}
