package org.anduril.core.network;

/**
 * Ports are the mechanism how component instances can communicate with
 * each other. There are two kinds of ports: input and output ports. This
 * class is the base class for both of them. Ports can be referred to
 * using either a name or a position integer.
 * 
 * <p>
 * Ports have an associated data type which tells what kind of data
 * flows through the port.
 */
public abstract class Port implements Comparable<Port> {

    public static final int NO_POSITION = Integer.MAX_VALUE;
    public static final int DEFAULT_STREAM_PRIORITY = -1;
    
    public static final String ARRAY_INDEX_FILE = "_index";
    public static final String ARRAY_INDEX_PORT_PREFIX = "_index_";
    
    public static final String ARRAY_CONSTRUCTOR_NAME = "ArrayConstructor";
    public static final int ARRAY_CONSTRUCTOR_PORTS = 99;
    
    public static final String ARRAY_EXTRACTOR_NAME = "ArrayExtractor";
    public static final int ARRAY_EXTRACTOR_PORTS = 9;

    public static final String ARRAY_COMBINER_NAME = "ArrayCombiner";
    public static final int ARRAY_COMBINER_PORTS = 20;
    
    public static final String ARRAY_COLUMN_KEY = "Key";
    public static final String ARRAY_COLUMN_FILE = "File";
    
    private Component component;
    private String name;
    private int position;
    private String doc;
    private DataType type;
    private ArrayStatus isArray;
    private boolean streamable;
    private boolean special;
    
    /**
     * Initialize.
     * @param component The component that owns this port.
     * @param name Name of the port.
     * @param position Position index of the port. This is a number
     * 	from 1 to N, where N is the number of (non-special) input
     * 	ports. For ports that don't use position, this is
     * 	Port.NO_POSITION.
     * @param doc Documentation string for the port.
     * @param type Data type of the port.
     * @param special If true, the port is a special port used
     * 	by the engine. Examples include error port and log port.
     */
    public Port(Component component, String name, int position, String doc, ArrayStatus isArray, DataType type, boolean special) {
        if (component == null) {
            throw new NullPointerException("component is null");
        }
        
        this.component = component;
        this.name = name;
        this.position = position;
        this.doc = doc;
        this.isArray = isArray;
        this.streamable = false;
        this.special = special;
        setType(type);
    }

    @Override
    public int compareTo(Port other) {
        return getName().compareTo(other.getName());
    }
    
    /**
     * Return the component that owns this port.
     */
    public Component getComponent() {
        return component;
    }
    
    /**
     * Return the name of the port.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Set the name of the port.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Return the position of the port.
     */
    public int getPosition() {
        return position;
    }
    
    /**
     * Set the position of the port.
     * @param position Position integer, greater or equal to 1.
     */
    public void setPosition(int position) {
        this.position = position;
    }
    
    /**
     * Return the documentation string for the port.
     */
    public String getDoc() {
        return doc;
    }
    
    /**
     * Set the documentation string for the port.
     */
    public void setDoc(String doc) {
        this.doc = doc;
    }
    
    /**
     * Return the data type of the port.
     */
    public DataType getType() {
        return type;
    }
    
    /**
     * Set the data type of the port. The type must not be null.
     */
    public void setType(DataType type) {
        if (type == null) throw new NullPointerException("type is null");
        this.type = type;
    }
    
    public ArrayStatus isArray() {
        return this.isArray;
    }

    public boolean isStreamble() {
        return this.streamable;
    }

    public void setStreamable(boolean streamable) {
        this.streamable = streamable;
    }
    
    /**
     * Tell whether the port is a special port used by the engine.
     */
    public boolean isSpecial() {
        return special;
    }
    
    /**
     * Return true if the data type is a generic. A generic port needs to
     * be converted to an actual port before it can be used in a network.
     */
    public boolean isGeneric() {
        return type.isGeneric();
    }

}
