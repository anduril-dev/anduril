package org.anduril.core.network;

import org.anduril.core.engine.NetworkEvaluator;
import org.anduril.core.readers.networkParser.EvaluatorHandler;
import org.anduril.core.readers.networkParser.NetworkHandler;
import org.anduril.core.utils.IOTools;
import org.anduril.core.utils.IllegalAnnotationScopeException;
import org.anduril.core.utils.Logger;
import org.anduril.core.utils.TextTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;


/**
 * Base class for component and network black box test
 * cases. All test cases are stored as a directory structure;
 * the details differ for component and network test cases. 
 */
public abstract class TestCase {
        
    public static final String DEFAULT_COMPONENT_TEST_NAME = "component";
    public static final String DEFAULT_FUNCTION_TEST_NAME = "test";
    public static final String FAILURE_FILE = "failure";
    public static final String EXPECTED_OUTPUT_DIR = "expected-output";
    
    public static final Long DEFAULT_TIMEOUT_sec = 3600L;  // 1 hour.
    
    public static final Integer PASSED = 0;
    public static final Integer FAILED = 1;
    public static final Integer TIMEOUT = 2;
    public static final Integer INACTIVE = -1;
    public static final Integer UNDETERMINED_RESULT = -100;

    public static class Tuple<X, Y, Z> {
        public final X x; 
        public final Y y;
        public final Z z; 
        public Tuple(X x, Y y, Z z) {
            this.x = x; 
            this.y = y; 
            this.z = z;
        }  
    } 
    
    public static class ResultFile {
        private final File file;
        private final DataType type;
        private final OutPort port;
        
        public ResultFile(File file, DataType type, OutPort port) {
            if (file == null) throw new NullPointerException("file is null");
            if (type == null) throw new NullPointerException("type is null");
            this.file = file;
            this.type = type;
            this.port = port;
        }

        public File getFile() {
            return file;
        }
        
        public DataType getType() {
            return type;
        }
        
        public OutPort getPort() {
            return port;
        }
    }
    
    class Task implements Callable<Integer> {
        private NetworkEvaluator evaluator;
        
        public Task(NetworkEvaluator evaluator) {
                this.evaluator = evaluator;
        }

        @Override
        public Integer call() {
                try {
                    evaluator.execute(caseNetwork);
                } catch (Exception e) {
                    evaluator.getDefaultLogger().error(e);
                    return TestCase.FAILED;
                }
            return evaluator.hasEngineErrors() ? TestCase.FAILED : TestCase.PASSED;
        }
    }

    private File executionRoot;
    private Repository repository;
    private File caseDirectory;
    private boolean exactDirectoryComparison;
    private Network caseNetwork;
    private ComponentInstance ci;
    
    private Map<String,String> metaParameters;
    boolean isFailure;
    private long timeOut;
    
    private List<InPort> missingInPorts = new ArrayList<InPort>();
    private List<InPort> usedInPorts = new ArrayList<InPort>();
    private List<OutPort> missingOutPorts = new ArrayList<OutPort>();
    private List<OutPort> usedOutPorts = new ArrayList<OutPort>();
    private List<String> paramsUsage;

    private boolean expectSuccess;
    private double elapsedSeconds;
    private int nParameters;
    private StringBuffer errors;
    private Integer testCaseResult;
    private HashMap<ComponentInstance,File> logFiles;
    private StringBuffer logContent = new StringBuffer();
    private String networkName;
    
    /**
     * Initialize.
     * @param caseDirectory The directory that stores the test case.
     * @param repository The underlying component repository.
     * @throws IOException If the test directory does not exist
     *  or is not a directory.
     */
    public TestCase(File caseDirectory, Repository repository, boolean exactDirectoryComparison) 
        throws IOException 
        {
        if (caseDirectory == null) {
            throw new NullPointerException("caseDirectory is null");
        }
        if (!caseDirectory.exists()) {
            String msg = String.format("Test case root directory %s does not exist", caseDirectory);
            throw new FileNotFoundException(msg);
        }
        if (!caseDirectory.isDirectory()) {
            String msg = String.format("Test case root directory %s is not a directory", caseDirectory);
            throw new IOException(msg);
        }

        this.caseDirectory = caseDirectory;
        this.repository = repository;
        this.exactDirectoryComparison = exactDirectoryComparison;
        this.metaParameters = new HashMap<String,String>();
        this.timeOut = DEFAULT_TIMEOUT_sec;
        this.errors = new StringBuffer();
        this.testCaseResult = TestCase.UNDETERMINED_RESULT;
        this.logFiles = new HashMap<ComponentInstance,File>();

        this.isFailure = false || new File(caseDirectory, FAILURE_FILE).exists();
        boolean outputDir = getExpectedOutputDirectory().exists();

        if (this.isFailure && outputDir) {
            String msg = String.format("Both %s and %s present; exactly one must exist",
                    FAILURE_FILE, EXPECTED_OUTPUT_DIR);
            throw new IOException(msg);
        }
        else if (!(this.isFailure || outputDir)) {
            String msg = String.format("One of '%s' or '%s' must exist; neither was found",
                    FAILURE_FILE, EXPECTED_OUTPUT_DIR);
            throw new IOException(msg);
        }
        else {
            this.expectSuccess = !this.isFailure;
        }
    }
    
    public String toString() {
        return this.caseDirectory.getAbsolutePath();
    }
    
    public boolean isFailure() {
        return this.isFailure;
    }

    public Repository getRepository() {
        return this.repository;
    }

    /**
     * Return a name for this test case.
     */
    public String getCaseName() {
        return caseDirectory.getName();
    }
    
    /**
     * Return the directory that stores the test case.
     */
    public File getCaseDirectory() {
        return caseDirectory;
    }
    
    public boolean getExactDirectoryComparison() {
        return exactDirectoryComparison;
    }
    
    public Map<String,String> getMetaParameters() {
        return metaParameters;
    }
    
    
    /////////////////////////////////////////////
    // Meta parameters.
    
    public void setMetaParameters(Map<String,String> metaParameters) {
        if (metaParameters != null) {
            this.metaParameters = metaParameters;
        }
        
        String timeOut_str = getMetaParameters().get("timeout");
        if (timeOut_str != null) {
                try {
                this.timeOut = Long.parseLong(timeOut_str);
                }
                catch (NumberFormatException nfe) {
                        throw new IllegalArgumentException("Could not convert " +
                                        "given timeout " + timeOut_str);
                }
        }
    }
    
    public long getTimeOut() {
        return this.timeOut;
    }
    
    /////////////////////////////////////////////
    
    /**
     * Return the directory that stores the expected
     * output files of this test case.
     */
    public File getExpectedOutputDirectory() {
        return new File(caseDirectory, EXPECTED_OUTPUT_DIR);
    }
    
    /**
     * Return true if this test case expects successful
     * execution.
     */
    public boolean expectSuccess() {
        return expectSuccess;
    }

        public double getSeconds() {
                return elapsedSeconds;
        }

        public int getnParameters() {
                return nParameters;
        }
        
        public StringBuffer getErrors() {
                return this.errors;
        }
        
        public File getExecutionRoot() {
                return this.executionRoot;
        }
        
        public Integer getTestCaseResult() {
                return testCaseResult;
        }
        
        public Network getCaseNetwork() {
                return this.caseNetwork;
        }

        public ComponentInstance getComponentInstance() {
                return this.ci;
        }

        public void setExecutionRoot(File executionRoot) {
                this.executionRoot = executionRoot;
        }

        public void setnParameters(int nParameters) {
                this.nParameters = nParameters;
        }
        
        public void setExpectSuccess(boolean expectSuccess) {
                this.expectSuccess = expectSuccess;
        }
        
        public TestCase execute() throws StaticError {
                testCaseResult = TestCase.UNDETERMINED_RESULT; 
                try {
                        long start = System.nanoTime();         
                    this.testCaseResult = executeAux();                 
                        this.elapsedSeconds = (System.nanoTime() - start) / 1e9;
                } catch (IllegalAnnotationScopeException aex) {
                    String infoMsg = "Expecting failed execution";
                    String msg = String.format("In %s:(%s,%s), %s\n%s\nIllegal annotation scope for annotation %s.\n",
                    aex.getFile().getName(),
                    aex.getLocation().getLine(),
                    aex.getLocation().getColumn(),
                            this.caseDirectory.getAbsoluteFile(),
                    infoMsg,
                    aex.getAnnotationName());
                    testCaseResult = TestCase.FAILED;
                    throw new StaticError(msg);
                } catch (Exception e) {
                    testCaseResult = TestCase.FAILED;
                    String msg = String.format("%s: unhandled exception", formatCaseName());
                    throw new StaticError(msg, e);
                }
                
                for (File logFile : this.getErrorLogs()) {
                        if (!logFile.exists()) continue;
                        FileReader reader;
                        try {
                                reader = new FileReader(logFile);
                                int n = (int) logFile.length();
                            char[] chars = new char[n];
                                reader.read(chars);

                                logContent.setLength(0);
                            logContent.append(chars);
                    } catch (FileNotFoundException e) {
                                e.printStackTrace();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }

        return this;
    }

    public Logger getLogger() {
        return Logger.getLogger(getCaseName());
    }

    private Integer executeAux() {
        getLogger().info("\nTest case "+getCaseDirectory().getAbsolutePath());

        final boolean expectingFailure = !expectSuccess();

        File executionRoot = getNetworkExecutionRoot(getExecutionRoot());

        if (executionRoot.exists()) {
            String infoMsg = "Clearing test root directory "+executionRoot.getAbsolutePath();
            getLogger().info(infoMsg);
            boolean ok = IOTools.rmdir(executionRoot, true);
            if (!ok) {
                String msg = String.format("%s: execution directory %s could not be cleared",
                        formatCaseName(),
                        executionRoot.getAbsolutePath());
                getLogger().error(msg);
                return TestCase.FAILED;
            }
        }

        Tuple<Network, ComponentInstance, NetworkEvaluator> tuple;
        try {
            tuple = createNetwork(executionRoot);
        } catch (StaticError e) {
            e.report(getLogger());
            if (expectSuccess()) return TestCase.FAILED;
            else return TestCase.PASSED;
        }
        if (tuple == null) {
            return TestCase.FAILED;
        }
        caseNetwork = (Network) tuple.x;
        if (caseNetwork == null) {
            return TestCase.FAILED;
        }
        else
            networkName = caseNetwork.generateInstanceName("test_function_" + caseDirectory.getName());

        NetworkEvaluator evaluator = (NetworkEvaluator) tuple.z;
        if (evaluator == null) {
            evaluator = new NetworkEvaluator(executionRoot, repository);
            NetworkHandler.setNetwork(caseNetwork);
            EvaluatorHandler.setNetworkEvaluator(evaluator);
        }

        ci = (ComponentInstance) tuple.y;

        if (expectingFailure) {
            String infoMsg = "Expecting failed execution";
            getLogger().info(infoMsg);
        }

        if (Logger.isVerbose()) {
            getLogger().debug(caseNetwork.format());
        }

        Integer myTCResult = TestCase.UNDETERMINED_RESULT;
        String testCaseName = getCaseName();
        long timeOut = getTimeOut();

        if (timeOut < 0) {
            getLogger().warning("Negative timeout " + timeOut +
                    " sec for test case " + testCaseName + "; setting it to default " +
                    TestCase.DEFAULT_TIMEOUT_sec);
            timeOut = TestCase.DEFAULT_TIMEOUT_sec;
        }

        if (timeOut == 0) {
            getLogger().info("Timeout 0 sec: test case " + testCaseName + " set to inactive.");
            return TestCase.INACTIVE;
        }

        try {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Integer> future = executor.submit(new Task(evaluator));
            future.get(timeOut, TimeUnit.SECONDS);
            executor.shutdownNow();

        } catch (TimeoutException e) {
            getLogger().warning("Test case " + testCaseName + " timed out.");
            return TestCase.TIMEOUT;
        }
        catch(Exception e) {
            String msg = String.format("%s: engine threw an unhandled exception",
                    formatCaseName());
            getLogger().error(msg);
            getLogger().error(e);
            return TestCase.FAILED;
        }

        if (expectingFailure) {
            if (evaluator.hasEngineErrors()) {
                return TestCase.PASSED;
            }
            else {
                String msg = String.format("%s: expected failed execution but got success",
                        formatCaseName());
                getLogger().error(msg);
                return TestCase.FAILED;
            }
        }

        if (evaluator.hasEngineErrors()) {
            String msg = String.format("%s: failed execution",
                    formatCaseName());
            getLogger().error(msg);
            return TestCase.FAILED;
        }

        Set<String> extraneousFiles = new HashSet<String>();
        if (getExactDirectoryComparison()) {
            for (File f: getActualDirectory(executionRoot).listFiles()) {
                extraneousFiles.add(f.getAbsolutePath());
            }
        }

        List<TestCase.ResultFile> expectedResults;
        try {
            expectedResults = getExpectedFiles(caseNetwork);
        } catch(IOException e) {
            String msg = String.format("%s: I/O error when reading expected output files: %s",
                    formatCaseName(), e);
            getLogger().error(msg);
            return TestCase.FAILED;
        }

        for (TestCase.ResultFile expectedResult: expectedResults) {
            File actual = new File(getActualDirectory(executionRoot),
                    expectedResult.getFile().getName());

            if (!actual.exists()) {
                String msg = String.format("%s: output file %s is missing",
                        formatCaseName(), actual.getAbsoluteFile());
                getLogger().error(msg);
                return TestCase.FAILED;
            }

            extraneousFiles.remove(actual.getAbsolutePath());

            boolean resultsMatch;
            StringBuffer difference = new StringBuffer();
            Port port = expectedResult.getPort();
            final boolean isArray = (port != null) && expectedResult.getPort().isArray().isTrue();
            DataTypeFunctionality func = expectedResult.getType().makeFunctionalityInstance(isArray);
            try {
                resultsMatch = func.compare(expectedResult.getFile(), actual, difference);
            } catch (IOException e) {
                String msg = String.format("%s: I/O error when comparing files: %s",
                        formatCaseName(), e);

                getLogger().error(msg);
                return TestCase.FAILED;
            } catch (IllegalStateException e) {
                String msg = String.format("%s: %s", formatCaseName(), e.getMessage());

                getLogger().error(msg);
                return TestCase.FAILED;
            }

            if (!resultsMatch) {
                String msg = String.format("Testcase %s: mismatch between\n  actual file %s\n  expected file %s ",
                        formatCaseName(), actual.getAbsolutePath(), expectedResult.getFile().getAbsolutePath());
                if (difference.length() > 0) msg += ": " + difference.toString();

                getLogger().error(msg);
                return TestCase.FAILED;
            }
        }

        if (!extraneousFiles.isEmpty()) {
            String msg = "Extraneous files in actual output directory: "
                    +TextTools.join(new ArrayList<String>(extraneousFiles), ", ");
            getLogger().error(msg);
            return TestCase.FAILED;
        }

        if (!executeTwice()) {
            return TestCase.PASSED;
        }

        getLogger().info("Second run, with state information from first run");
        try {
            tuple = createNetwork(executionRoot);
        } catch (StaticError e) {
            e.report(getLogger());
            if (expectSuccess()) return TestCase.FAILED;
            else return TestCase.PASSED;
        }
        if (tuple == null) {
            return TestCase.FAILED;
        }

        caseNetwork = (Network) tuple.x;

        evaluator = new NetworkEvaluator(executionRoot, repository);

        try {
            if (timeOut == 0) {
                getLogger().info("Timeout 0 sec: test case " + testCaseName + " set to inactive.");
                return TestCase.INACTIVE;
            }

            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Integer> future = executor.submit(new Task(evaluator));
            myTCResult = future.get(timeOut, TimeUnit.SECONDS);
            executor.shutdownNow();

            if (myTCResult == TestCase.FAILED || myTCResult == TestCase.TIMEOUT)
                return myTCResult;

        } catch (TimeoutException e) {
            myTCResult = TestCase.TIMEOUT;
            getLogger().warning("Test case " + testCaseName + " timed out.");
        }
        catch(Exception e) {
            String msg = String.format("%s: engine threw an unhandled exception on second run",
                    formatCaseName());
            getLogger().error(msg);
            getLogger().error(e);
            return TestCase.FAILED;
        }

        if (evaluator.hasEngineErrors() && !expectingFailure) {
            String msg = String.format("%s: failed execution on second run",
                    formatCaseName());
            getLogger().error(msg);
            return TestCase.FAILED;
        }

        return TestCase.PASSED;
    }

    public List<InPort> getMissingInPorts() {
                return missingInPorts;
        }

        public List<InPort> getUsedInPorts() {
                return usedInPorts;
        }

        public List<OutPort> getMissingOutPorts() {
                return missingOutPorts;
        }

        public List<OutPort> getUsedOutPorts() {
                return usedOutPorts;
        }

        public List<String> getParamsUsage() {
                return paramsUsage;
        }
        
        public StringBuffer getLogContent() {
                return this.logContent;
        }
        
        public List<File> getErrorLogs() {

                if (this.logFiles.size() <= 1)
                        return new ArrayList<File>(this.logFiles.values());

                List<File> errorLogs = new ArrayList<File>();
                for (Map.Entry<ComponentInstance,File> e : this.logFiles.entrySet()) {
                        ComponentInstance aCi = e.getKey();
                        OutPort oPort = aCi.getComponent().getOutPort(Component.ERRORS_PORT_NAME);
                        if (oPort == null) 
                                continue;

                        File file = aCi.getOutputFile(oPort, this.executionRoot);
                        if (file != null && file.exists())
                                errorLogs.add(this.logFiles.get(aCi));
                }

                return errorLogs;
        }

        public void setParamsUsage(List<String> paramsUsage) {
                this.paramsUsage = paramsUsage;
        }

        public abstract Component getComponent();

    public abstract Network getFunctionNetwork();
    
    public abstract String formatCaseName();
    
    public abstract File getNetworkExecutionRoot(File testExecutionRoot);
    
    /**
     * Create an executable network that represents the test case.
     */
    public abstract TestCase.Tuple<Network,ComponentInstance, NetworkEvaluator> createNetwork(File executionRoot) throws StaticError;
    
    /**
     * Return the directory that contains actual results based on
     * the root execution directory.
     * @param executionDirectory Root execution directory.
     */
    public abstract File getActualDirectory(File executionDirectory);
    
    public abstract List<ResultFile> getExpectedFiles(Network network)
        throws IOException; 
    
    /**
     * Return whether the test case should be executed twice. On
     * the second run, state information from the first run is
     * used.
     */
    public abstract boolean executeTwice();

}
