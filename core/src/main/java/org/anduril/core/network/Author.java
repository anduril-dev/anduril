package org.anduril.core.network;

import org.anduril.core.utils.Language;

/**
 * Represent an author of a component. The author
 * has a name and optionally an email address.
 */
public class Author {
    private String name;
    private String email;
    
    public Author(String name, String email, Repository repository) {
        if (name == null) {
            throw new NullPointerException("name is null");
        }
    
        if (email != null) {
            Language lang = Language.valueOf("html");
            if (email.equals(lang.quote(email)) == false) {
                throw new IllegalArgumentException(String.format(
                      "Illegal characters in email %s for author %s", email, name));
            }
        }
        
        this.name = name;
        this.email = email;
    }
    
    /**
     * Return the email address. May be null.
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * Return the name escaped as HTML.
     */
    public String getHtmlName() {
        return Language.valueOf("html").quote(name);
    }
    
    /**
     * Return the name as declared in component.xml.
     */
    public String getName() {
        return this.name;
    }
}
