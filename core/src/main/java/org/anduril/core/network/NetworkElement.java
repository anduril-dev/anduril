package org.anduril.core.network;

/**
 * Base class for network elements that have an associated
 * source code location.
 */
public class NetworkElement implements Locatable {
    private final SourceLocation sourceLocation;
    
    public NetworkElement(SourceLocation location) {
        this.sourceLocation = location;
    }
    
    /**
     * Return the location in a source file that contains
     * the definition of this network element. This
     * typically identifies the position in a network
     * configuration file or an XML file, depending on element.
     * The returned value is never null, but any of the attributes
     * of the location may be null.
     */
    public SourceLocation getSourceLocation() {
        return this.sourceLocation;
    }
}
