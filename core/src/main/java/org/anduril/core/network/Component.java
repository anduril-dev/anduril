package org.anduril.core.network;

import org.anduril.core.network.componentInstance.RegularComponentInstance;
import org.anduril.core.network.launcher.Launcher;
import org.anduril.core.utils.IOTools;
import org.anduril.core.utils.TextTools;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Component is the basic unit of the network.
 * This class represents a static view of the component that specifies
 * the component interface. There is only one Component object
 * per component even if a network contains several instances of a
 * component.
 * 
 * <p>
 * Usually a component has an associated executable file that is launched
 * when the component is executed, but there are also special components
 * used by the workflow engine that don't have associated executables.
 * Components contain a number of input and output port and launchers
 * that allow the component to be executed. The component also has
 * parameters, which are scalar values that are passed to the executable.
 * 
 * <p>
 * This class doesn't contain inter-component relationships or bind values
 * to parameters; these are done by ComponentInstance and its subclasses.
 * 
 * <p>
 * A specific component type is a generic component. This kind
 * of component has one or more type parameter that must be specified before
 * the component can be executed. The type may be different for
 * different instances of the component, so the type is set in
 * ComponentInstance.
 * 
 * <p>
 * Instantiation of Component has two phases. First the constructor
 * is executed and then addSpecialPorts is called.
 */
public class Component extends NetworkElement {
    /**
     * Name of the special output port for error messages.
     * This port is available for all components.
     */
    public static final String ERRORS_PORT_NAME = "_errors";
    
    /**
     * Name of the special output port for log messages.
     * This port is available for all components.
     */
    public static final String LOG_PORT_NAME = "_log";
    
    /**
     * Name of the special output port for temporal directory.
     * This port is available for all components.
     */
    public static final String TEMP_DIRECTORY_PORT_NAME = "_tempdir";

    public static final String TEST_CASE_DIRECTORY = "testcases";

    private String name;
    private String namespaceName;
    private String doc;
    private String deprecated;
    private boolean isDeprecated = false;
    private String version;
    private List<Requirement> requirements;
    private String[] categories;	
    private List<Author> authors;
    private List<Author> credits;
    private File mainDirectory;
    private Bundle bundle;
    private Class<? extends ComponentInstance> instanceClass;

    private SortedMap<String,InPort> inPorts;
    private SortedMap<String,OutPort> outPorts;
    private SortedMap<String,Launcher> launchers;
    private SortedMap<String,Parameter> parameters;
    private List<Parameter> parametersInsertOrder;
    private SortedMap<String,TypeParameter> typeParameters;
    private Set<File> docResources;
    private List<ComponentTestCase> testCases;
    private String TEST_NETWORK_FILE;

    class PortComparator implements Comparator<Port> {
        public int compare(Port port1, Port port2) {
            return port1.getPosition() - port2.getPosition();
        }
    }

    class TestCaseComparator implements Comparator<TestCase> {
        public int compare(TestCase case1, TestCase case2) {
            return case1.getCaseName().compareTo(case2.getCaseName());
        }
    }
    
    /**
     * Create a component.
     * 
     * @param name Component name. This must be unique among all components.
     * @param doc Component documentation string. May be null.
     * @param version Component version. May be null.
     * @param categories List of categories that the component belongs to.
     * 	Categories are simply strings that describe the category.
     * @param authors List of authors who have contributed to the component.
     * @param credits List of authors who are not directly associated with the component.
     * @param requirements Describes the external requirements
     * 	such as an installation of R.
     */
    public Component(String name,
            String doc,
            String version,
            String[] categories,
            List<Author> authors,
            List<Author> credits,
            List<Requirement> requirements,
            SourceLocation location) {
        super(location);
        setName(name);
        if (requirements == null) {
            requirements = new ArrayList<Requirement>();
        }
        if (authors == null) {
            authors = new ArrayList<Author>();
        }
        if (credits == null) {
            credits = new ArrayList<Author>();
        }

        for (Requirement req: requirements) req.setOwner(this);
        
        this.doc = doc;
        this.version = version;
        this.categories = categories;
        this.authors = authors;
        this.credits = credits;
        this.requirements = requirements;

        this.inPorts = new TreeMap<String,InPort>();
        this.outPorts = new TreeMap<String,OutPort>();
        this.launchers = new TreeMap<String,Launcher>();
        this.parameters = new TreeMap<String,Parameter>();
        this.parametersInsertOrder = new ArrayList<Parameter>();
        this.typeParameters = new TreeMap<String,TypeParameter>();
        this.docResources = new HashSet<File>();
        this.testCases = new ArrayList<ComponentTestCase>();
        this.TEST_NETWORK_FILE = "../../test-networks/"+name+"/network.scala";
        
        this.instanceClass = RegularComponentInstance.class;
    }
    
    /**
     * Create a non-branch component with null values for
     * other attributes than the name.
     */
    public Component(String name) {
        this(name, null, null, null, null, null, null, null);
        
    }
    
    public String toString() {
        return "Component: "+getName();
    }
    
    /**
     * Return a multi-line string that describes the component
     * in detail.
     * @param prefix Each line must start with this string.
     * @param postfix Each line must end with this string.
     */
    public String format(String prefix, String postfix) {
        StringBuilder sb = new StringBuilder(1024);
        String s = String.format("Component %s (is generic: %s)",
                getName(), isGeneric());
        sb.append(prefix).append(s).append(postfix);
        String newPrefix = prefix + ComponentInstance.FORMAT_INDENT;
        
        s = String.format("Instance class: %s", instanceClass);
        sb.append(newPrefix).append(s).append(postfix);
        
        for (TypeParameter typeParam: getTypeParameters()) {
            s = String.format("Type parameter %d: %s",
                    typeParam.getPosition(),
                    typeParam.getName());
            if (typeParam.getExtendsType() != null) {
                s += String.format(", extends %s",
                        typeParam.getExtendsType().getName());
            }
            sb.append(newPrefix).append(s).append(postfix);
        }
        
        for (InPort port: getInPorts(true)) {
            s = String.format("Input port %d: %s, type: %s, generic: %s, optional: %s",
                    port.getPosition(),
                    port.getName(),
                    port.getType().getName(),
                    port.isGeneric(),
                    port.isOptional());
            sb.append(newPrefix).append(s).append(postfix);
        }

        for (OutPort port: getOutPorts(true)) {
            s = String.format("Output port %d: %s, type: %s, generic: %s",
                    port.getPosition(),
                    port.getName(),
                    port.getType().getName(),
                    port.isGeneric());
            sb.append(newPrefix).append(s).append(postfix);
        }

        for (Parameter param: getParameters()) {
            sb.append(newPrefix)
              .append("Parameter ")
              .append(param.getName())
              .append(": type: ")
              .append(param.getType())
              .append(", default: ")
              .append(param.getDefaultValue())
              .append(postfix);
        }

        for (Launcher launcher: launchers.values()) {
            //s = String.format("Launcher: %s", launcher.getType());
            s = String.format("Launcher: %s", launcher.toString());
            sb.append(newPrefix).append(s).append(postfix);
        }
        return sb.toString();
    }

    /**
     * Format with empty prefix and "\n" as postfix.
     */
    public final String format() {
        return format("", "\n");
    }
    
    /**
     * Add ports for error messages, log messages and branch choice (if
     * the component is a branch). 
     * @param repository Used to retrieve built-in datatypes 
     * 		DataType.STRING_LIST_NAME and DataType.SIMPLE_STRING_LIST_NAME.
     * 		It is an error if these types are not found.
     */
    public void addSpecialPorts(Repository repository) {
        DataType stringList = repository.getDataType(DataType.STRING_LIST_NAME);
        if (stringList == null) {
            String msg = String.format("Built-in data type %s not found", DataType.STRING_LIST_NAME);
            throw new RuntimeException(msg);
        }
        
        int pos = getOutPorts(true).size() + 1;
        addPort(new OutPort(this, ERRORS_PORT_NAME, pos, "Error messages", ArrayStatus.FALSE, stringList, true));
        addPort(new OutPort(this, LOG_PORT_NAME, pos+1, "Log messages", ArrayStatus.FALSE, stringList, true));
        addPort(new OutPort(this, TEMP_DIRECTORY_PORT_NAME, pos+2, "Temporary directory", ArrayStatus.FALSE, stringList, true));
    }

    /**
     * Returns the hosting bundle or null if the component belongs to none.
     */
    public Bundle getBundle() {
    	return bundle;
    }

    /**
     * Assigns a host bundle for the component.
     */
    public void setBundle(Bundle bundle) {
    	this.bundle = bundle;
        this.namespaceName = this.bundle.getCompilePackageName() + "." + this.name;
    }

    /**
     * Return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the last component of the name.
     */
    public String getSimpleName() {
        final int pos = this.name.lastIndexOf('.');
        if (pos < 0) {
            return this.name;
        } else {
            return this.name.substring(pos + 1);
        }
    }

    public String getNamespaceName() {
        return this.namespaceName;
    }

    /**
     * Set the name.
     */
    public void setName(String name) {
        if (!IOTools.isValidName(name)) {
            throw new IllegalArgumentException("Name is not valid: "+name);
        }
        this.name = name;
    }
    
    /**
     * Return the documentation string. This may be null.
     */
    public String getDoc() {
        return doc;
    }
    
    /**
     * Return a short documentation string that usually fits into one
     * or two lines. This may be null. This is the first sentence of the
     * documentation string. The end of the sentence is found by the
     * regular expression "[.]\\s+[A-Z]", that is, a dot followed by
     * at least whitespace followed by a capital character A-Z.
     */
    public String getShortDoc() {
        return TextTools.getShortDoc(doc);
    }
    
    /**
     * Set documentation string.
     */
    public void setDoc(String doc) {
        this.doc = doc;
    }

    /**
     * Whether the component is deprecated.
     */
    public boolean isDeprecated() {
    	return isDeprecated;
    }
    
    /**
     * Returns the migration documentation if the component has
     * been set for deprecation. The string may be empty or null
     * if no information has been added in component.xml.
     */
    public String getDeprecated() {
    	return deprecated;
    }

    /** 
     * Set migration documentation for deprecated component.
     */
    public void setDeprecated(String doc) {
    	deprecated = doc;
    	isDeprecated = true;
    }
    
    /**
     * Return the version. This may be null.
     */
    public String getVersion() {
        return version;
    }
    
    /**
     * Set version.
     */
    public void setVersion(String version) {
        this.version = version;
    }
    
    /**
     * Return categories.
     */
    public String[] getCategories() {
        return categories;
    }
    
    /**
     * Set categories.
     */
    public void setCategories(String[] categories) {
        this.categories = categories;
    }
    
    /**
     * Return component authors.
     */
    public List<Author> getAuthors() {
        return authors;
    }
    
    /**
     * Set authors.
     */
    public void setAuthors(List<Author> authors) {
        if (authors == null) {
            authors = new ArrayList<Author>();
        }
        this.authors = authors;
    }
    
    /**
     * Return component credits.
     */
    public List<Author> getCredits() {
        return credits;
    }
    
    /**
     * Set credits.
     */
    public void setCredits(List<Author> credits) {
        if (credits == null) {
            credits = new ArrayList<Author>();
        }
        this.credits = credits;
    }
    
    /**
     * Return all external requirements.
     */
    public List<Requirement> getRequires() {
        return requirements;
    }
    
    /**
     * Return external requirements of given type.
     */
    public List<RequirementResource> getRequires(String type) {
        List<RequirementResource> reqs = new ArrayList<RequirementResource>();
        for (Requirement req: this.requirements) {
            RequirementResource resource = req.getResource(type);
            if (resource != null) reqs.add(resource);
        }
        return reqs;
    }
    
    /**
     * Add an external requirement.
     */
    public void addRequires(Requirement requirement) {
        this.requirements.add(requirement);
    }
    
    /**
     * Return true if the component has a type parameter.
     */
    public boolean isGeneric() {
        return !typeParameters.isEmpty();
    }

    /**
     * Return the directory where the component XML
     * file is stored.
     */
    public File getMainDirectory() {
        return mainDirectory;
    }

    /**
     * Set the directory where the component XML
     * file is stored.
     */
    public void setMainDirectory(File directory) {
        this.mainDirectory = directory;
    }
    
    /**
     * Return the directory where test cases are stored.
     * This is the a directory that contains individual
     * test cases as subdirectories. The directory may
     * not exist; this has to be checked by the user of
     * this method.
     */
    public File getTestCaseDirectory() {
        return new File(mainDirectory, TEST_CASE_DIRECTORY);
    }
    
    /**
     * Set the class that is used to make instances of the component.
     * The class must be a subclass of ComponentInstance and have
     * a constructor String name, Component component.
     */
    public void setInstanceClass(Class<? extends ComponentInstance> instanceClass) {
        this.instanceClass = instanceClass;
    }

    public void setInstanceClass(String className, Repository repository) {
        className = className.trim();
        ClassLoader cl = Component.class.getClassLoader();
        Class<? extends ComponentInstance> instanceClass;
        try {
            instanceClass = (Class<? extends ComponentInstance>) cl.loadClass(className);
            this.setInstanceClass(instanceClass);
        } catch(ClassNotFoundException e) {
            throw new RuntimeException("Invalid instance class name: "+className);
        } catch(NoClassDefFoundError e) {
            throw new RuntimeException(String.format("Instance class ("+className+") not available: "+
                    e.getMessage()+" not found."));
        }
    }

    public Class<? extends ComponentInstance> getInstanceClass() {
        return this.instanceClass;
    }
    
    /**
     * Create a ComponentInstance associated with this component.
     * A Component may have any number of ComponentInstances and all
     * of them share the same ComponentInstance.
     * 
     * @param name ComponentInstance name. This must be unique among
     * 	ComponentInstances.
     * @return An instance of the class that has been set with
     * 	setInstanceClass.
     * @throws RuntimeException If the class is invalid, e.g.
     * 	has no suitable constructor.
     */
    public ComponentInstance makeComponentInstance(String[] name, SourceLocation location) throws RuntimeException {
        // Maybe errors should be handled better.
        Constructor<? extends ComponentInstance> cons ;
        try {
            cons = instanceClass.getConstructor(String[].class, SourceLocation.class, Component.class);
        } catch(NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        try {
            return cons.newInstance(name, location, this);
        } catch(IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch(InstantiationException e) {
            throw new RuntimeException(e);
        } catch(InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
    
    protected SortedMap<String,InPort> getInPortMap() {
        return inPorts;
    }

    /**
     * Return input ports.
     * @param includeSpecial If true, also return special input ports.
     * 	If false, return only those ports added by the user with addPort.
     */
    public List<InPort> getInPorts(boolean includeSpecial) {
        if (includeSpecial) return new ArrayList<InPort>(getInPortMap().values());
        
        List<InPort> ports = new ArrayList<InPort>();
        for (InPort port: getInPortMap().values()) {
            if (!port.isSpecial()) ports.add(port);
        }
        Collections.sort(ports, new PortComparator());
        return ports;
    }
    
    /**
     * Return non-special input ports.
     */
    public List<InPort> getInPorts() {
        return getInPorts(false);
    }
    
    /**
     * Return the input port of given name, or null if the port is not found.
     * @param name Port name
     */
    public InPort getInPort(String name) {
        return getInPortMap().get(name);
    }
    
    /**
     * Return the input port for given position, or null if the port is
     * not found.
     * @param position Port position.
     */
    public InPort getInPort(int position) {
        for (InPort port: getInPortMap().values()) {
            if (port.getPosition() == position) return port;
        }
        return null;
    }
    
    protected SortedMap<String,OutPort> getOutPortMap() {
        return outPorts;
    }

    /**
     * Return output ports.
     * @param includeSpecial If true, also return special output ports,
     * 	such as error and log ports. If false, return only those ports added
     * 	by the user with addPort.
     */
    public List<OutPort> getOutPorts(boolean includeSpecial) {
        if (includeSpecial) return new ArrayList<OutPort>(getOutPortMap().values());
        
        List<OutPort> ports = new ArrayList<OutPort>();
        for (OutPort port: getOutPortMap().values()) {
            if (!port.isSpecial()) ports.add(port);
        }
        Collections.sort(ports, new PortComparator());
        return ports;
    }

    /**
     * Return non-special output ports.
     */
    public List<OutPort> getOutPorts() {
        return getOutPorts(false);
    }
    
    /**
     * Return the output port of given name, or null if the port is not found.
     * @param name Port name
     */
    public OutPort getOutPort(String name) {
        return getOutPortMap().get(name);
    }
    
    /**
     * Return the output port for given position, or null if the port is
     * not found.
     * @param position Port position.
     */
    public OutPort getOutPort(int position) {
        for (OutPort port: getOutPortMap().values()) {
            if (port.getPosition() == position) return port;
        }
        return null;
    }
    
    /**
     * Return port names as a list.
     */
    public static List<String> portsToNames(Collection<? extends Port> ports) {
        List<String> names = new ArrayList<String>();
        for (Port port: ports) names.add(port.getName());
        return names;
    }
    
    /**
     * Return names of all input ports in no particular order.
     * If there are no input ports, return an empty list.
     */
    public List<String> getInPortNames(boolean includeSpecial) {
        return portsToNames(getInPorts(includeSpecial));
    }
    
    /**
     * Return names of all output ports in no particular order.
     * If there are no output ports, return an empty array.
     */
    public List<String> getOutPortNames(boolean includeSpecial) {
        return portsToNames(getOutPorts(includeSpecial));
    }
    
    /**
     * Return the special error messages output port.
     */
    public OutPort getErrorsPort() {
        return getOutPort(ERRORS_PORT_NAME);
    }
    
    /**
     * Return the special log messages output port.
     */
    public OutPort getLogPort() {
        return getOutPort(LOG_PORT_NAME);
    }
    
    /**
     * Add an input port to the component. If there is already an
     * input port with the same name, it is replaced.
     * @throws IllegalArgumentException if port.getComponent()
     * 	returns a different component than this one.
     */
    public void addPort(InPort port) {
        if (port.getComponent() != this) {
            throw new IllegalArgumentException("Component of InPort is not current component");
        }
        
        InPort existing = getInPort(port.getPosition());
        if (existing != null) {
            String msg = String.format(
                    "Can't add port %s: port with position %d (%s) is already present",
                    port.getName(),
                    port.getPosition(),
                    existing.getName());
                    
            throw new IllegalArgumentException(msg);
        }
        
        inPorts.put(port.getName(), port);
    }

    /**
     * Add an output port to the component. If there is already an
     * output port with the same name, it is replaced.
     * @throws IllegalArgumentException if port.getComponent()
     * 	returns a different component than this one.
     */
    public void addPort(OutPort port) {
        if (port.getComponent() != this) {
            throw new IllegalArgumentException("Component of OutPort is not current component");
        }
        
        OutPort existing = getOutPort(port.getPosition());
        if (existing != null) {
            String msg = String.format(
                    "Can't add port %s: port with position %d (%s) is already present",
                    port.getName(),
                    port.getPosition(),
                    existing.getName());
                    
            throw new IllegalArgumentException(msg);
        }
        
        outPorts.put(port.getName(), port);
    }
    
    /**
     * Return the primary launcher.
     */
    public Launcher getLauncher() {
        if (launchers.isEmpty()) return null;
        String key = launchers.firstKey();
        return launchers.get(key);
    }
    
    /**
     * Return a launcher of given type. If no launcher for the
     * type is found, return null.
     */
    public Launcher getLauncher(String type) {
        return launchers.get(type);
    }

    /**
     * Add a launcher. If there is already a launcher with the same
     * type, it is replaced.
     */
    public void addLauncher(Launcher launcher) {
        launchers.put(launcher.getType(), launcher);
    }

    protected SortedMap<String,Parameter> getParameterMap() {
        return parameters;
    }

    /**
     * Return names of parameters.
     */
    public String[] getParameterNames() {
        return (String[]) getParameterMap().keySet().toArray();
    }

    /**
     * Return parameters in alphabetical order.
     */
    public List<Parameter> getParameters() {
        return new ArrayList<Parameter>(getParameterMap().values());
    }

    /**
     * Return parameters in insertion order.
     */
    public List<Parameter> getParametersInsertOrder() {
        return this.parametersInsertOrder;
    }

    /**
     * Return a parameter by name.
     */
    public Parameter getParameter(String name) {
        if (name == null) return null;
        return this.parameters.get(name);
    }
    
    /**
     * Add a parameter. If there already is a parameter
     * with the same name, it is replaced.
     */
    public void addParameter(Parameter param) {
        getParameterMap().put(param.getName(), param);
        this.parametersInsertOrder.add(param);
    }

    /**
     * For generic components, add a type parameter.
     */
    public void addTypeParameter(TypeParameter typeParameter) {
        typeParameters.put(typeParameter.getName(), typeParameter);
    }
    
    /**
     * For generic component, return all type parameters.
     */
    public List<TypeParameter> getTypeParameters() {
        return new ArrayList<TypeParameter>(typeParameters.values());
    }
    
    /**
     * For generic components, return a type parameter with
     * given name.
     * @param name Name of type parameter.
     * @return The type parameter, or null if it is not found.
     */
    public TypeParameter getTypeParameter(String name) {
        return typeParameters.get(name);
    }
    
    /**
     * For generic components, return a type parameter with
     * given position.
     * @param position Position of type parameter (greater or
     * 	equal to 1).
     * @return The type parameter, or null if it is not found.
     */
    public TypeParameter getTypeParameter(int position) {
        for (TypeParameter typeParam: typeParameters.values()) {
            if (typeParam.getPosition() == position) return typeParam;
        }
        return null;
    }
    
    /**
     * Return documentation resource files. These are copied into
     * the HTML documentation directory of the component.
     * The files may be regular files or directories.
     * Directories are copied recursively. Resource files returned
     * by this method are those added manually using addDocResource;
     * or source files that are included automatically. See getSourceFiles
     * to access only the source files. 
     */
    public Set<File> getDocResources() {
        Set<File> resources = new TreeSet<File>(docResources);
        resources.addAll(getSourceFiles());
        return resources;
    }
    
    /**
     * Register a documentation resource file for the component.
     * The file may be a regular file or a directory. The file
     * should not be the name of the doc-files directory that
     * holds resource files, but rather a file (or directory)
     * that is located in the doc-files directory.
     */
    public void addDocResource(File docResource) {
        if (docResource == null) {
            throw new NullPointerException("docResource is null");
        }
        this.docResources.add(docResource);
    }
    
    /**
     * Return the list of plain-text source files associated to this
     * launcher, if any. Source files are included in component documentation.
     */
    public List<File> getSourceFiles() {
        List<File> files = new ArrayList<File>();
        
        File file = new File(getMainDirectory(), Bundle.COMPONENT_XML_FILE);
        if (file.exists()) files.add(file);
        file = new File(getMainDirectory(), Bundle.FUNCTION_FILE);
        if (file.exists()) files.add(file);
        
        if (getLauncher() != null) {
            List<File> launcherFiles = getLauncher().getSourceFiles();
            if (launcherFiles != null) files.addAll(launcherFiles);
        }
        return files;
    }
    
    /**
     * Return the component black box test cases.
     */
    public List<ComponentTestCase> getTestCases() {
        Collections.sort(testCases, new TestCaseComparator());
        return testCases;
    }

    /**
     * Add a component black box test case.
     */
    public void addTestCase(ComponentTestCase testCase) {
        this.testCases.add(testCase);
    }
    
    public File getTestNetwork() {
        File file = new File(getMainDirectory(), this.TEST_NETWORK_FILE);
        if (file.exists()) return file;
        return null;
    }
}
