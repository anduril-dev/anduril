package org.anduril.core.network.annotations;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.ParameterType;
import org.anduril.core.network.Port;

public class PriorityAnnotation extends Annotation {
    public PriorityAnnotation() {
    	super("priority", new Integer(0), ParameterType.INT, true, false, false);
    }

    @Override
    public String validate(Object value, ComponentInstance ci, Port port) {
        return null;
    }

    @Override
    public void applyFinalization(Object value, ComponentInstance ci, Port port) {

    }
}