package org.anduril.core.network;

/**
 * A connection between two component instances. If two
 * component instances A, B are connected, it means that
 * A must be executed before B can be executed. A connection
 * is a directed edge in a graph. There are two kinds of
 * connections: control and port connections. This class is
 * the base class for them.
 */
public abstract class Connection {
    private ComponentInstance from;
    private ComponentInstance to;
    
    /**
     * Initialize.
     * @param from Start point of the connection.
     * @param to End point of the connection.
     */
    public Connection(ComponentInstance from, ComponentInstance to) {
        if (from == null) throw new IllegalArgumentException("from is null");
        if (to == null) throw new IllegalArgumentException("to is null");
        if (from == to) {
            String msg = String.format("From (%s) == to (%s)",
                    from.getName(), to.getName());
            throw new IllegalArgumentException(msg);
        }
        
        this.from = from;
        this.to = to;
    }
    
    @Override
    public String toString() {
        return String.format("%s => %s", from.getName(), to.getName());
    }
    
    /**
     * Return true if the connection is a control connection,
     * false if port connection.
     */
    public abstract boolean isControlConnection();
    
    /**
     * Return the start point of the connection.
     */
    public ComponentInstance getFrom() {
        return from;
    }
    
    /**
     * Return the end point of the connection.
     */
    public ComponentInstance getTo() {
        return to;
    }
    
}
