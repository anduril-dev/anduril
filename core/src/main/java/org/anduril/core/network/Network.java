package org.anduril.core.network;

import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.network.componentInstance.OutputComponentInstance;
import org.anduril.core.utils.Logger;

import java.io.File;
import java.util.*;

/**
 * Network is a collection of component instances and their connections
 * (network topology). The network is formed by port and control connections
 * and is flat; hierarchy links of component instances are not part of
 * the network.
 * Network also maintains a collection of static errors that occurred during
 * network loading.
 */
public class Network implements Iterable<ComponentInstance> {
    public static final String DOC_RESOURCES_DIR = "doc-files";
    
    private Repository repository;
    private Map<String,ComponentInstance> componentInstances;
    private boolean isFinalized = false;
    private Map<String,Integer> nameCounters = new HashMap<String, Integer>();
    private File dataDir;

    /**
     * Initialize.
     * @param repository Reference to the component repository.
     *  Must not be null.
     */
    public Network(Repository repository) {
        if (repository == null) {
            throw new NullPointerException("repository is null");
        }

        this.repository = repository;
        this.componentInstances = new HashMap<String,ComponentInstance>();
    }

    // TODO: warn on the CI_DISPLAYED_LIMIT
    public String format() {
        final int CI_DISPLAYED_LIMIT = 50;
        
        StringBuilder sb = new StringBuilder();
        int i = 0;
        ComponentInstance ci;

        Iterator<ComponentInstance> it = this.getMembersDFS(true);
        while (it.hasNext()) {
            i = i + 1;
            if (i > CI_DISPLAYED_LIMIT) {
                sb.append("  (and ");
                sb.append(this.componentInstances.size() - CI_DISPLAYED_LIMIT);
                sb.append(" component instances more)");
                break;
            }
            
            ci = it.next();
            sb.append(ci.format("", "\n"));
        }

        return sb.toString();
    }
    
    /**
     * Return the component repository.
     */
    public Repository getRepository() {
        return repository;
    }
    
    /**
     * Return true if the network has been finalized by
     * NetworkFinalizer and is ready for execution.
     */
    public boolean getFinalized() {
        return isFinalized;
    }
    
    /**
     * Set network finalization status.
     */
    public void setFinalized(boolean isFinalized) {
        this.isFinalized = isFinalized;
    }
    
    /**
     * Generate a unique name for a component instance.
     * The name is unique within this network, but
     * not necessarily in other networks.
     * The name has the format baseNameN, where N is
     * an integer counter. 
     * @param baseName Base name for the identifier.
     *  This is often the name of the component whose
     *  instance we are creating.
     */
    public String generateInstanceName(String baseName) {
        Integer counter = nameCounters.get(baseName);
        if (counter == null) counter = 1;
        String name = baseName + counter;
        nameCounters.put(baseName, counter + 1);
        return name;
    }
    
    /**
     * Return the set of all component instances. The
     * order is arbitrary.
     */
    public List<ComponentInstance> getMembers() {
        return new ArrayList<ComponentInstance>(componentInstances.values());
    }
    
    /**
     * Return network nodes in depth first search order.
     * If forward is true, iteration starts at source nodes
     * (having no input connections); dependency connections are
     * traversed forwards. If forward is false, iteration
     * starts at sink nodes (having no output connections) and
     * honors reverse topological order i.e. you can trust
     * that dependencies are listed only after all of their
     * successors have been listed.
     * 
     * Notice that if forward is false, you must iterate backwards
     * with getPrevious.
     */
    public ListIterator<ComponentInstance> getMembersDFS(boolean forward) {
        final int size = this.componentInstances.size();
        List<ComponentInstance> nodes = new ArrayList<ComponentInstance>(size);
        HashSet<ComponentInstance> visited = new HashSet<ComponentInstance>(size);
        ArrayDeque<ComponentInstance> stack = new ArrayDeque<ComponentInstance>(size);

        for (ComponentInstance root: this) {
            final boolean isRoot = root.getInComponents().isEmpty();
            if (isRoot) {
                stack.add(root);
                while (!stack.isEmpty()) {
                    final ComponentInstance node = stack.pop();
                    if (visited.contains(node)) continue;

                    if (!forward && !visited.containsAll(node.getInComponents())) {
                        // Do not finish this node yet (or continue to its children)
                    	// as otherwise it will appear in the reverse list before some
                    	// of its dependencies. However it's traversed again later so
                        // it will be added - once it's finished.
                    	continue;
                    }
                   	for (ComponentInstance child: node.getOutComponents())
                   		stack.push(child);

                    nodes.add(node);
                    visited.add(node);
                }
            }
        }

        if (nodes.size() != size) {
            throw new RuntimeException(String.format(
                    "Internal error: incorrect number (%d) of nodes in DFS list (network size %d)",
                    nodes.size(), size));
        }
        if (forward)
        	return nodes.listIterator();
        else return nodes.listIterator(nodes.size());
    }
    
    /**
     * Return a component instance with given name that is part of
     * this network.
     * @param name Name of component instance.
     * @return Component instance, or null if it doesn't exist.
     */
    public ComponentInstance getComponentInstance(String name) {
        return componentInstances.get(name);
    }

    public List<ComponentInstance> getOutputComponentInstances() {
        List<ComponentInstance> outputCis = new ArrayList<ComponentInstance>();
        for (ComponentInstance ci : componentInstances.values()) {
            if (ci instanceof OutputComponentInstance)
                outputCis.add(ci);
        }
        return outputCis;
    }

    /**
     * Add a component instance to the network.
     * @throws IllegalArgumentException if a component instance with the
     * 	same name exists.
     */
    public void addComponentInstance(ComponentInstance ci) {
        for (Map.Entry<Annotation, Object> entry: ci.getInstanceAnnotations().entrySet()) {
            entry.getKey().applyAddToNetwork(entry.getValue(), ci, null);
        }

        final String name = ci.getName();
        if (componentInstances.containsKey(name)) {
            String msg = String.format("Component instance %s already exists.", name);
            throw new IllegalArgumentException(msg);
        }
        if (ci.getComponent().isDeprecated()) {
        	String msg = String.format("\nWarning: Component %s deprecated for component instance %s." +
        			"\nWarning: %s\n", 
        			name, ci.getComponent().getName(), ci.getComponent().getDeprecated());
            ci.getLogger().warning(msg, Logger.tagWarningWorkflowStructure());
        }
        
        componentInstances.put(name, ci);
        ci.setNetwork(this);
    }
    
    /**
     * Remove a component instance from the network.
     * @param ci The CI to be removed.
     * @throws IllegalArgumentException if ci is not found in
     * 	the network.
     */
    public void removeComponentInstance(ComponentInstance ci) {
        if (!componentInstances.containsKey(ci.getName())) {
            String msg = String.format("Component instance %s is not found in the network",
                    ci.getName());
            throw new IllegalArgumentException(msg);
        }
        
        for (PortConnection connection: ci.getOutPortConnections()) {
            ci.removeConnection(connection);
        }
        componentInstances.remove(ci.getName());
    }
    
    /**
     * Return true if given component instance belongs to the
     * network.
     */
    public boolean containsComponentInstance(ComponentInstance ci) {
        return componentInstances.containsValue(ci);
    }
    
    /**
     * Rename a component instance.
     * @param ci The component instance to be renamed.
     * @param oldFullName The old full name, under which the component is in the network.
     * @param newFullName The new full name.
     */
    public void renameComponentInstance(ComponentInstance ci, String oldFullName, String newFullName) {
        if (!componentInstances.containsKey(oldFullName)) {
            String msg = String.format("Can't rename component instance %s: not part of the network",
                    ci.getName());
            throw new IllegalArgumentException(msg);
        }
        
        componentInstances.remove(oldFullName);
        componentInstances.put(newFullName, ci);
    }

    /**
     * Iterate over component instances.
     */
    @SuppressWarnings("unchecked")
	public Iterator<ComponentInstance> iterator() {
        if (componentInstances == null) {
            return Collections.EMPTY_LIST.iterator();
        } else {
            return componentInstances.values().iterator();
        }
    }

    /**
     * Return the data directory that is the default location
     * of INPUT component paths. The data directory is used
     * when the path of an INPUT component is relative. 
     */
    public File getDataDir() {
        if (dataDir != null) return dataDir;
        else {
            String pwd = System.getProperty("user.dir");
            if (pwd != null) return new File(pwd);
            else return null;
        }
    }
    
    /**
     * Set the data directory. May be null.
     */
    public void setDataDir(File dataDir) {
        this.dataDir = dataDir;
    }

}
