package org.anduril.core.network;

import org.anduril.component.Tools;
import org.anduril.core.engine.NetworkEvaluator;
import org.anduril.core.readers.NetworkFinalizer;
import org.anduril.core.readers.PropertiesReader;
import org.anduril.core.utils.IOTools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Black box test case for a component.
 */
public class ComponentTestCase extends TestCase {

    public static final String FUNCTION_NAME  = "output";
    public static final String META_PROPERTY_NAME = "metadata";
    public static final String PROPERTIES_SUFFIX = ".properties";
    public static final String FUNCTION_PROPERTIES_FILE = "component" + PROPERTIES_SUFFIX;
    public static final String INPUT_DIRECTORY = "input";

    private Component component;
    private ComponentInstance ci = null;
    private Network functionNetwork = null;

    private String envName;
    private String envNameProps;
    

    public ComponentTestCase(File caseDirectory, Repository repository, Component component) 
	throws IOException {
        super(caseDirectory, repository, false);
        
        this.component = component;
        fetchPorts(component);
        envName = TestCase.DEFAULT_COMPONENT_TEST_NAME;
        envNameProps = "/" + FUNCTION_PROPERTIES_FILE;        
    }

    public ComponentTestCase(File caseDirectory, Repository repository, Component component, Network functionNetwork) 
	throws IOException {
        super(caseDirectory, repository, false);
        
        this.component = component;
        this.functionNetwork = functionNetwork;
        fetchPorts(component);
        envName = TestCase.DEFAULT_FUNCTION_TEST_NAME;
        envNameProps = "/" + FUNCTION_PROPERTIES_FILE;
    }

    private void fetchPorts(Component comp) {
    	for (InPort myInPort : comp.getInPorts()) {
            File testFile = getInputFile(myInPort);
            if (testFile == null || !testFile.exists()) getMissingInPorts().add(myInPort);
            else getUsedInPorts().add(myInPort);
        }

    	for (OutPort myOutPort : comp.getOutPorts()) {
            File testFile = getOutputFile(myOutPort);
            if (testFile == null || !testFile.exists()) getMissingOutPorts().add(myOutPort);
            else getUsedOutPorts().add(myOutPort);
        }
    }

	@Override
    public Component getComponent() {
        return component;
    }
    
    public Network getFunctionNetwork() {
    	return functionNetwork;
    }
    
    @Override
    public String formatCaseName() {
        return String.format("Component %s, case %s", component.getName(), getCaseName());
    }

    @Override
    public File getNetworkExecutionRoot(File testExecutionRoot) {
        return new File(
            new File(testExecutionRoot, getComponent().getName()),
            getCaseDirectory().getName());
    }
    
    private File getPortFile(Port port, File baseDir) {
        if (port == null) throw new NullPointerException("port is null");
        if (baseDir == null) throw new NullPointerException("baseDir is null");
        if (!baseDir.isDirectory()) return null;

        if (port.getType().getPresentationType() == PresentationType.MULTIFILE && port.getType().hasExtension(false)) {
            File primaryFile = new File(baseDir, String.format("%s.%s", port.getName(), port.getType().getExtension(false)));
            if (primaryFile.exists()) {
                return primaryFile.getAbsoluteFile();
            }
        }

        final String portName = port.getName();
        File inputFile = null;
        for (File file: baseDir.listFiles()) {
            String name = IOTools.removeFileExtension(file.getName());
            if (name.equals(portName)) {
                if (inputFile != null && port.getType().getPresentationType() != PresentationType.MULTIFILE) {
                    throw new IllegalArgumentException(String.format(
                            "Component %s: Two files found that match non-multifile port %s: %s and %s",
                            port.getComponent().getName(), port.getName(), inputFile.getName(), file.getName()));
                }
                inputFile = file.getAbsoluteFile();
            }
        }
        return inputFile;
    }
    
    public File getInputFile(InPort port) {
        return getPortFile(port, new File(getCaseDirectory(), INPUT_DIRECTORY));
    }

    public File getOutputFile(OutPort port) {
        return getPortFile(port, new File(getCaseDirectory(), TestCase.EXPECTED_OUTPUT_DIR));
    }

    @Override
    public TestCase.Tuple<Network, ComponentInstance, NetworkEvaluator> createNetwork(File executionRoot) throws StaticError {
    	
    	if (functionNetwork != null) 
    		return new TestCase.Tuple<Network, ComponentInstance, NetworkEvaluator>(functionNetwork, null, null);

        Network network = new Network(getRepository());
        Component inputComp = getRepository().getComponent("anduril.builtin.INPUT");

        if (inputComp == null)
            return null;

        ci = component.makeComponentInstance(new String[] { envName }, null);
        network.addComponentInstance(ci);
        
        File inputDir = new File(getCaseDirectory(), INPUT_DIRECTORY);
        if (!inputDir.exists()) {
            String msg = String.format("%s: input directory not found", formatCaseName());
            throw new StaticError(msg, ci);
        }
        
        HashSet<String> seenBasenames = new HashSet<String>();
        for (File input: inputDir.listFiles()) {
            String inputName = IOTools.removeFileExtension(input.getName());
            /* Skip .svn directories and such. */
            if (inputName.startsWith(".")) continue;
            
            InPort toPort = ci.getComponent().getInPort(inputName);
            if (toPort == null) {
                String msg = String.format("Component %s, case %s: unknown input %s",
                                           component.getName(), getCaseName(), inputName);
                throw new StaticError(msg, ci);
            }

            if (!Tools.isPrimaryFile(input, toPort.getType())) {
                continue;
            }

            if (toPort.getType().getPresentationType() == PresentationType.MULTIFILE) {
            	if (seenBasenames.contains(inputName)) continue;
            	seenBasenames.add(inputName);
            }
            
            ComponentInstance inCI = inputComp.makeComponentInstance(new String[] { inputName }, null);
            inCI.setParameterValue(inCI.getComponent().getParameter("path"),
                    input.getAbsolutePath());
            network.addComponentInstance(inCI);
            
            inCI.connectTo(ci, inCI.getComponent().getOutPort("out"), toPort, false);
        }
        
        File propertiesFile = getParameterFile();
        if (propertiesFile != null) {
            try {
                super.setMetaParameters(PropertiesReader.read(network, propertiesFile, this.envName));
            } catch (IOException e) {
                throw new StaticError(e.getMessage());
            }
        }

        new NetworkFinalizer(network, null).runFinalizer();
        return new Tuple<Network, ComponentInstance, NetworkEvaluator>(network, ci, null);
    }

    /**
     * Get location of the parameter file for a component.
     */
    public File getParameterFile() {
        File propertiesFile = new File(getCaseDirectory(), this.envNameProps);
        return propertiesFile.exists() ? propertiesFile : null;
    }

	/**
	 * Return true if test case has got parameters, false otherwise
	 */
    public boolean hasParameters() {
		if(this.getParameterFile() != null)
			return true;
		else
			return false;
	}

    @Override
    public File getActualDirectory(File executionDirectory) {
    	if (functionNetwork != null) return new File(executionDirectory, FUNCTION_NAME);
        return new File(executionDirectory, envName);
    }

    @Override
    public boolean executeTwice() {
        return false;
    }

    @Override
    public List<TestCase.ResultFile> getExpectedFiles(Network network) {
        List<TestCase.ResultFile> files = new ArrayList<ResultFile>();
        for (OutPort port: component.getOutPorts()) {
            String baseName = port.getName();

            if (functionNetwork == null) {
	            ComponentInstance ci = network.getComponentInstance(envName);
	            port = ci.getNongenericOutPort(port);
            }
            
            if (port.getType().hasExtension(port.isArray().isTrue())) {
                baseName += "." + port.getType().getExtension(port.isArray().isTrue());
            }
            
            File file = new File(getExpectedOutputDirectory(), baseName);
            if (file.exists()) files.add(new ResultFile(file, port.getType(), port));
        }
        return files;
    }
	
	public String getEnvName() {
		return this.envName;
	}
	
	public String getEnvNameProps() {
		return this.envNameProps;
	}

}

