package org.anduril.core.network;


/**
 * Output port of a component.
 */
public class OutPort extends Port {
    /**
     * Initialize.
     * @param component The component that owns this port.
     * @param name Name of the port.
     * @param position Position index of the port. This is a number
     * 	from 1 to N, where N is the number of (non-special) input
     * 	ports. For ports that don't use position, this is
     * 	Port.NO_POSITION.
     * @param desc Documentation string for the port.
     * @param type Data type of the port.
     * @param special If true, the port is a special port used
     * 	by the engine. Examples include error port and log port.
     */	
    public OutPort(Component component, String name, int position, String desc, ArrayStatus isArray, DataType type, boolean special) {
        super(component, name, position, desc, isArray, type, special);
    }
    
    /**
     * Initialize a non-special output port whose output file
     * may not be missing.
     */
    public OutPort(Component component, String name, int position, String desc, ArrayStatus isArray, DataType type) {
        this(component, name, position, desc, isArray, type, false);
    }

    public String toString() {
        return "OutPort: (" + getName() + "," + this.getType() + ")";
    }
    
    /**
     * If the port is generic, convert it to a non-generic port by
     * replacing the data type with an actual type. If the port is
     * not generic, return the port as is.
     */
    public OutPort getNonGenericPort(DataType actualType, ArrayStatus isArray) {
        if (!isGeneric()) return this;
        // if (actualType == null) throw new NullPointerException("actualType is null");
        if (actualType == null) actualType = this.getType();
//        if (actualType.isGeneric()) {
//            throw new IllegalArgumentException("actualType is generic: "+actualType.getName());
//        }
        return new OutPort(getComponent(), getName(), getPosition(), getDoc(),
                isArray, actualType,
                isSpecial());
    }
    
}
