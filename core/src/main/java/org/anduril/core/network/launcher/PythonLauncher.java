package org.anduril.core.network.launcher;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PythonLauncher extends Launcher {
    private File sourceFile;
    private static String executable = "python";
    
    public PythonLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The Python launcher requires the argument 'file'");
        }
        this.sourceFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.sourceFile);
    }
    
    /**
     * Return the command that is used to execute Python.
     */
    public static String getExecutable() {
        return executable;
    }

    /**
     * Set the command that is used to execute Python.
     */
    public static void setExecutable(String exec) {
        executable = exec;
    }

    /**
     * Return the Python script source file.
     */
    public File getSourceFile() {
        return sourceFile;
    }

    @Override
    public Map<String, String> getExtraEnvironment(Repository repository) {
        final String PYTHONPATH = "PYTHONPATH";
        final String sep = System.getProperty("path.separator");
        
        String path = System.getenv(PYTHONPATH);
        if (path == null) path = "";
        else path += sep;
        
        File pythonLib = new File(repository.getHomeDirectory() , "lang/python");
        path += pythonLib.getAbsolutePath();

        Bundle bundle = this.getComponent().getBundle();
        if (bundle.getLibDirectory() != null)
        {
            File bundleLib= new File(bundle.getLibDirectory(), "python");
            if (bundleLib.exists())
            {
                path += sep;
                path += bundleLib.getAbsolutePath();
            }
        }
        Map<String, String> env = new HashMap<String, String>(); 
        env.put(PYTHONPATH, path);
        return env;
    }

    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {

        String[] cmd = new String[] {
            executable,
            getSourceFile().getAbsolutePath(),
            commandFile.getPath()
        };

        return cmd;
    }
}
