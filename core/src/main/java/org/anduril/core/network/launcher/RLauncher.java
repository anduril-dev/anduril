package org.anduril.core.network.launcher;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Launcher for R components. The launcher has one mandatory
 * argument, <code>file</code>, that gives the filename of the
 * R script. The filename is usually a relative to the
 * main directory of the component.
 */
public class RLauncher extends Launcher {
    private File sourceFile;
    private static String executable = "Rscript";
    
    public RLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The R launcher requires the argument 'file'");
        }
        this.sourceFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.sourceFile);
    }
    
    /**
     * Return the command that is used to execute R.
     */
    public static String getExecutable() {
        return executable;
    }

    /**
     * Set the command that is used to execute R.
     */
    public static void setExecutable(String exec) {
        executable = exec;
    }
    
    /**
     * Return the R script source file.
     */
    public File getSourceFile() {
        return sourceFile;
    }

    @Override
    public Map<String, String> getExtraEnvironment(Repository repository) {
        final String R_LIBS = "R_LIBS";
        final String sep = System.getProperty("path.separator");
        
        String path = System.getenv(R_LIBS);
        if (path == null) path = "";
        else path += sep;
        
        File rLib = new File(repository.getHomeDirectory() , "lang/r/R_LIBS");
        path += rLib.getAbsolutePath();

        Bundle bundle = this.getComponent().getBundle();
        if (bundle.getLibDirectory() != null)
        {
            File bundleLib= new File(bundle.getLibDirectory(), "r/R_LIBS");
            if (bundleLib.exists())
            {
                path += sep;
                path += bundleLib.getAbsolutePath();
            }
        }
        Map<String, String> env = new HashMap<String, String>(); 
        env.put(R_LIBS, path);
        return env;
    }

    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {
        String[] cmd = new String[] {
            executable,
            getSourceFile().getAbsolutePath(),
            commandFile.getPath(),
        };

        return cmd;
    }

}
