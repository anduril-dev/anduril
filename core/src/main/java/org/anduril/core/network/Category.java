package org.anduril.core.network;

import org.anduril.core.utils.TextTools;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Data category that is associated with a port. A data category contains
 * documentation about physical file layout that is important in
 * enabling component authors to use the category.
 */
public class Category implements Comparable<Category> {
    /** 
     * Name of the StringList built-in data category.
     */
    public final static String STRING_LIST_NAME = "StringList";

    private String name;
    private Category parentCategory;
    private List<Category> children;
    private String doc;
    private Set<File> docResources;
    private List<Component> theComponents;
    
    /**
     * Initialize a data category.
     * @param name Name of the data category. This must be unique among data categories.
     * @param parentCategory Parent data category, or null if the category is a top-level
     * 	category.
     * @param desc Documentation string. This gives a high level description of
     * 	the category.
     */
    public Category(String name, Category parentCategory, String desc) {
    	// name = name.replace(" ", "");
    	/*
        if (!IOTools.isValidName(name)) {
            throw new IllegalArgumentException("Name is not valid: "+name);
        }
        */
        
        this.name = name;
        this.parentCategory = parentCategory;
        this.children = new ArrayList<Category>();
        this.doc = desc;
        this.docResources = new HashSet<File>();
        theComponents = new ArrayList<Component>();
        
        if (parentCategory != null) {
            parentCategory.addChild(this);
        }
    }
    
    /**
     * Return a generic category.
     * @param name Name of the data category.
     */
    public static Category buildGenericCategory(String name) {
        Category category = new Category(name, null, null);
        return category;
        
    }
    
    public String toString() {
        return "Category: "+name;
    }
    
    /**
     * Return data category name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Return the parent category if this category, or null if this
     * category is a root category.
     */
    public Category getParentCategory() {
        return parentCategory;
    }
    
    public void addChild(Category child) {
        if (child.getParentCategory() != this) {
            String s = String.format("Parent category of %s is not this category but %s", child.getName(), child.getParentCategory());
            throw new IllegalArgumentException(s);
        }
        if (children.contains(child)) {
            String s = String.format("Child category %s is already present", child.getName());
            throw new IllegalArgumentException(s);
        }
        children.add(child);
    }
    
    public List<Category> getChildren() {
        return children;
    }
    
    public List<Component> getComponents() {
    	return theComponents;
    }
    
    public void setComponents(List<Component> aComponents) {
    	if (aComponents != null) 
    		theComponents = new ArrayList<Component>(aComponents);
    }
    
    /**
     * Return true if this category is a subcategory of given
     * category. A is a subcategory of B if there is a path
     * (of any length) from B to A in the tree of categorys.  
     */
    public boolean isSubCategoryOf(Category another) {
        Category category = this;
        while (category != null) {
            if (category.equals(another)) return true;
            category = category.getParentCategory();
        }
        return false;
    }
    
    /**
     * Return all ancestor categorys of this category. This
     * includes the parent, the parent of parent, etc.
     * The list is empty if this category is a root category.
     */
    public List<Category> getAncestors() {
        List<Category> ancestors = new ArrayList<Category>();
        
        Category category = this;
        while (category != null) {
            ancestors.add(category);
            category = category.getParentCategory();
        }
        
        return ancestors;
    }
    
    /**
     * Return all descendant categorys (sub categorys) of this category.
     * Descendants include this category it self, all children,
     * all grand children, etc.
     */
    public List<Category> getDescendants() {
        List<Category> descendants = new ArrayList<Category>();
        descendants.add(this);
        for (Category child: children) {
            descendants.addAll(child.getDescendants());
        }
        return descendants;
    }
    
    /**
     * Return the depth of this category in the category
     * hierarchy. For a root category, depth is 1;
     * its children have depth 2, etc.
     */
    public int getDepth() {
        int depth = 0;
        Category category = this;
        while (category != null) {
            depth++;
            category = category.getParentCategory();
        }
        return depth;
    }
    
    public String getDepthSpace() {
    	int i = getDepth() - 1;
    	if (i == 0) return "";
    	
    	String mySpace = "class=\"indent"+i+"\"";
    	/*for (int j = 0; j < i; j++) {
    		mySpace += "&nbsp;&nbsp;&nbsp;&nbsp;";
                mySpace += "&nbsp;&nbsp;&nbsp;&nbsp;";
                mySpace += "&nbsp;&nbsp;&nbsp;&nbsp;";
    	}*/
    	return mySpace;
    }

    /**
     * Return the documentation string. This gives a high level description of
     * the category.
     */
    public String getDoc() {
        return doc;
    }
    
    /**
     * Set documentation string.
     */
    public void setDoc(String doc) {
        this.doc = doc;
    }
    
    /**
     * Return a short documentation string that usually fits into one
     * or two lines. This may be null. This is the first sentence of the
     * documentation string. The end of the sentence is found by the
     * regular expression "[.]\\s+[A-Z]", that is, a dot followed by
     * at least whitespace followed by a capital character A-Z.
     */
    public String getShortDoc() {
        return TextTools.getShortDoc(doc);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Category)) return false;
        else return getName().equals(((Category)obj).getName());
    }
    
    public int compareTo(Category other) {
        return getName().compareTo(other.getName());
    }
    

    /**
     * Return documentation resource files. These are copied into
     * the HTML documentation directory of the component.
     * The files may be regular files or directories.
     * Directories are copied recursively.
     */
    public Set<File> getDocResources() {
        return docResources;
    }
    
    /**
     * Register a documentation resource file for the component.
     * The file may be a regular file or a directory. The file
     * should not be the name of the doc-files directory that
     * holds resource files, but rather a file (or directory)
     * that is located in the doc-files directory.
     */
    public void addDocResource(File docResource) {
        if (docResource == null) {
            throw new NullPointerException("docResource is null");
        }
        this.docResources.add(docResource);
    }
    
    
}
