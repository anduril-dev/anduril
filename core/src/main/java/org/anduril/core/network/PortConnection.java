package org.anduril.core.network;

/**
 * Specific type of connection where data from the source
 * output port goes to destination input port. 
 */
public class PortConnection extends Connection {
    private OutPort fromPort;
    private InPort toPort;
    private boolean force;

    /**
     * Initialize.
     * @param from Start point of the connection.
     * @param to End point of the connection.
     * @param fromPort Output port of the start point component instance.
     * @param toPort Input port of the end point component instance.
     */
    public PortConnection(ComponentInstance from, ComponentInstance to,
            OutPort fromPort, InPort toPort, boolean force) {
        super(from, to);
        
        if (fromPort == null) throw new IllegalArgumentException("fromPort is null");
        if (toPort == null) throw new IllegalArgumentException("toPort is null");
        
        this.fromPort = fromPort;
        this.toPort = toPort;
        this.force = force;

        if (!from.getComponent().equals(fromPort.getComponent())) {
            String msg = String.format("%s: fromPort (%s.%s) doesn't match the 'from' component (%s)",
                    this,
                    fromPort.getComponent().getName(), fromPort.getName(),
                    from.getName());
            throw new IllegalArgumentException(msg);
        }
        if (!to.getComponent().equals(toPort.getComponent())) {
            String msg = String.format("%s: toPort (%s.%s) doesn't match the 'to' component (%s)",
                    this,
                    toPort.getComponent().getName(), toPort.getName(),
                    to.getName());
            throw new IllegalArgumentException(msg);
        }
        
        if (from.getComponent().getOutPort(fromPort.getName()) == null) {
            String msg = String.format("%s: Source component %s doesn't contain the fromPort %s",
                    this, from, fromPort.getName());
            throw new IllegalArgumentException(msg);
        }
        if (to.getComponent().getInPort(toPort.getName()) == null) {
            String msg = String.format("%s: Destination component %s doesn't contain the toPort %s",
                    this, to, toPort.getName());
            throw new IllegalArgumentException(msg);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        else if (!(obj instanceof Connection)) return false;
        else if (!super.equals(obj)) return false;
        
        PortConnection other = (PortConnection)obj;
        return this.fromPort.equals(other.fromPort)
            && this.toPort.equals(other.toPort);
    }
    
    @Override
    public String toString() {
        return String.format("%s.%s => %s.%s",
                getFrom().getName(),
                getFromPort().getName(),
                getTo().getName(),
                getToPort().getName());
    }
    
    @Override
    public boolean isControlConnection() {
        return false;
    }
    
    /**
     * Return the output port of the start point component instance.
     */
    public OutPort getFromPort() {
        return fromPort;
    }
    
    /**
     * Return the input port of the end point component instance.
     */
    public InPort getToPort() {
        return toPort;
    }
    
    /**
     * Return a non-generic version of the from port.
     */
    public OutPort getNongenericFromPort() {
        return getFrom().getNongenericOutPort(fromPort);
    }
    
    /**
     * Return a non-generic version of the to port.
     */
    public InPort getNongenericToPort() {
        return getTo().getNongenericInPort(toPort);
    }
    
    /**
     * Return true if the connection is forced. Forced
     * connections bypass type checks.
     */
    public boolean isForced() {
        return force;
    }

    /**
     * Return the from port as a ComponentInstancePort
     * object.
     */
    public ComponentInstancePort<OutPort> getFromCIP() {
        return new ComponentInstancePort<OutPort>(getFrom(), getFromPort());
    }
    
    /**
     * Return the to port as a ComponentInstancePort
     * object.
     */
    public ComponentInstancePort<InPort> getToCIP() {
        return new ComponentInstancePort<InPort>(getTo(), getToPort());
    }
    
}
