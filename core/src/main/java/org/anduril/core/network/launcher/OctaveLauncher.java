package org.anduril.core.network.launcher;

import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class OctaveLauncher extends Launcher {
    private File sourceFile;
    private static String executable = "octave";
    
    /**
     * Initialize.
     * @param type Launcher type.
     * @param args Launcher parameters that are used to configure
     *  the launcher. The set of parameters depends on the concrete
     *  launcher subclass.
     * @param component The component associated with the launcher.
     */
    public OctaveLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The Octave launcher requires the argument 'file'");
        }
        this.sourceFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.sourceFile);
    }

    /**
     * Return the Octave script source file.
     */
    public File getSourceFile() {
        return sourceFile;
    }
    
    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {
        /* Remove file extension from the source file */
        String octaveCommand = getSourceFile().getName();
        int pos = octaveCommand.lastIndexOf('.');
        if (pos > 0) octaveCommand = octaveCommand.substring(0, pos);
        /* This points to the Octave component skeleton, located
         * under the "octave" directory in the root directory of
         * Anduril.
         */
        File octaveLib = new File(repository.getHomeDirectory(), "octave");

        String[] cmd = new String[] {
            executable,
            "--silent",
            "--eval",
            String.format("addpath %s ; commandfile='%s' ; %s",
                    octaveLib.getAbsolutePath(),
                    commandFile.getPath(),
                    octaveCommand)
        };


        return cmd;
    }

}
