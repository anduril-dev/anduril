package org.anduril.core.network;

/**
 * Parameter is a simple scalar value that modifies the execution of the
 * component. It is normally passed to the executable associated with the
 * component. Actual parameter values are stored in component instances.
 */
public class Parameter implements Comparable<Parameter> {
    private String name;
    private ParameterType type;
    private String doc;
    private String defaultValue;
    
    /**
     * Initialize.
     * @param name Name of the parameter
     * @param type Type of the parameter.
     * @param doc  Documentation string of the parameter.
     * @param defaultValue Default value for the parameter. If the parameter
     * 	has a default value, it is not necessary the explicitly set parameter
     *  value. Otherwise, it is an error to execute a component with no value
     *  for a parameter.
     */
    public Parameter(String name, ParameterType type, String doc, String defaultValue) {
        this.name = name;
        this.type = type;
        this.doc = doc;
        this.defaultValue = defaultValue;
    }
    
    @Override
    public int compareTo(Parameter other) {
        return getName().compareTo(other.getName());
    }
    
    public String toString() {
    	return "Parameter " + name + ", type " + type + 
    		", doc " + doc + ", defaultValue " + defaultValue;
    }

    /**
     * Return parameter name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Set parameter name.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Return data type of the parameter.
     */
    public ParameterType getType() {
        return type;
    }
    
    /**
     * Set the data type.
     */
    public void setType(ParameterType type) {
        this.type = type;
    }
    
    /**
     * Return the documentation string.
     */
    public String getDoc() {
        return doc;
    }
    
    /**
     * Return the default value of the parameter.
     * If the default value is non-null, it is not necessary the explicitly
     * set parameter value for the component instance. Otherwise, it is an error to
     * execute a component with no value for a parameter.
     */
    public String getDefaultValue() { return defaultValue; }

}
