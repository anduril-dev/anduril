package org.anduril.core.network;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * A resource bundle combines associated components, data
 * types, libraries, network functions and test networks. This class
 * represents the directory structure of the bundle. 
 */
public class Bundle {
    public static final String BUNDLE_PACKAGE_PREFIX = "anduril.";
    public static final String BUNDLE_XML_FILE = "bundle.xml";
    public static final String COMPONENTS_DIR = "components";
    public static final String COMPONENT_DESCRIPTION_DIR = COMPONENTS_DIR+File.separator+"report-descriptions";
    public static final String COMPONENT_XML_FILE = "component.xml";
    public static final String DATATYPES_XML_FILE = "datatypes.xml";
    public static final String CATEGORIES_XML_FILE = "categories.xml";
    public static final String FUNCTION_FILE = "function.scala";
    public static final String FUNCTIONS_DIR = "functions";
    public static final String BUILTIN_BUNDLE = "builtin";
    
    private File directory;
    private String name;
    private String issues;
    private LinkedHashSet<String> dependsBundleNames;
    private LinkedHashSet<String> importsJARs;

    /**
     * Initialize.
     * @param directory Root directory of bundle.
     * @param name Name of the bundle.
     */
    public Bundle(File directory, String name) {
        if (directory == null) {
            throw new NullPointerException("directory is null");
        }

        this.directory = directory;
        this.name = name;
        this.issues = "";

        this.dependsBundleNames = new LinkedHashSet<String>();
        if (!this.name.equals(Bundle.BUILTIN_BUNDLE)) {
            addDependsBundle(BUILTIN_BUNDLE);
        }

        this.importsJARs = new LinkedHashSet<String>();
    }

    /**
     * Return the root directory of the bundle.
     */
    public File getDirectory() {
        return directory;
    }

    /**
     * Return the name of the bundle.
     */
    public String getName() {
        return name;
    }
    /**
     * Return the URL to issue tracker.
     */
    public String getIssues() {
        return issues;
    }


    /**
     * Return the name of the bundle.
     */
    public String getCompilePackageName() {
        return BUNDLE_PACKAGE_PREFIX + this.name;
    }

    public String getSourcePackageName() {
        return getCompilePackageName().replaceAll("\\.",File.separator);
    }

    public File getBundleJAR() {
        return new File(this.directory, String.format("%s.jar", this.name));
    }

    /**
     * Return the root directory for the components. The directory
     * may not exists; this must be checked by the
     * user of this method.
     */
    public File getComponentDirectory() {
        return new File(directory, COMPONENTS_DIR);
    }

    /**
     * Return the root directory for the components. The directory
     * may not exists; this must be checked by the
     * user of this method.
     */
    public File getComponentDescriptionDirectory() {
        return new File(directory, COMPONENT_DESCRIPTION_DIR);
    }

    /**
     * Return the library directory. The directory
     * may not exists; this must be checked by the
     * user of this method.
     */
    public File getLibDirectory() {
        return new File(directory, "lib");
    }

    /**
     * Return the library sub-directory for given language.
     * The directory may not exists; this must be checked by the
     * user of this method.
     */

    public File getLibDirectory(String language) {
        return new File(getLibDirectory(), language);
    }

    /**
     * Return the directory that contains test networks.
     * The directory may not exists; this must be checked
     * by the user of this method.
     */
    public File getTestNetworkDirectory() {
        return new File(directory, "test-networks");
    }

    public File getDataTypeXMLFile() {
        return new File(directory, DATATYPES_XML_FILE);
    }

    /**
     * Return the directory that contains example files
     * for data types.
     * The directory may not exists; this must be checked
     * by the user of this method.
     */
    public File getDataTypeFileDirectory() {
        return new File(directory, Network.DOC_RESOURCES_DIR);
    }
    
    /**
     * Return the root directory that contains network
     * functions. The actual functions are subdirectories
     * of this root directory. 
     * The directory may not exists; this must be checked
     * by the user of this method.
     */
    public File getNetworksDirectory() {
        return new File(directory, FUNCTIONS_DIR);
    }

    public List<File> getComponentDescriptors(Set<String> includeComponentNames) {
        return findComponents(getComponentDirectory(), includeComponentNames);
    }

    /**
     * Return the descriptor XML files for network functions
     * that are under the directory given by
     * {@link #getNetworksDirectory getNetworksDirectory}.
     */
    public List<File> getNetworkDescriptors(Set<String> includeComponentNames) {
        return findComponents(getNetworksDirectory(), includeComponentNames);
    }

    public File getNetworkScalaSource(Component function) {
        return new File(new File(getNetworksDirectory(), function.getName()), FUNCTION_FILE);
    }

    /**
     * Find component XML descriptor files by searching
     * the subdirectories of given root directory.
     */
    private List<File> findComponents(File root, Set<String> includeComponentNames) {
        final String XML_FILENAME = COMPONENT_XML_FILE;
        List<File> files = new ArrayList<File>();
        if (root == null || !root.exists()) return files;
        
        for (File dir: root.listFiles()) {
            if (!dir.isDirectory()) continue;

            final String componentName = dir.getName();
            if (includeComponentNames == null || includeComponentNames.contains(componentName)) {
                File xmlFile = new File(dir, XML_FILENAME);
                if (xmlFile.exists()) files.add(xmlFile);
            }
        }
        
        return files;
    }

    @Override
    public String toString() { return name; }

    public List<Component> getComponents(Repository repository) {
        ArrayList<Component> myComponents = new ArrayList<Component>();

        for (Component comp: repository.getComponents()) {
            if (comp.getBundle() == this) myComponents.add(comp);
        }

        return myComponents;
    }

    public List<Component> getFunctions(Repository repository) {
        ArrayList<Component> myFunctions = new ArrayList<Component>();

        for (Component func: repository.getFunctions()) {
            if (func.getBundle() == this) myFunctions.add(func);
        }

        return myFunctions;
    }

    public void addDependsBundle(String bundleName) {
        this.dependsBundleNames.add(bundleName);
    }

    public void addImportsJARs(String jar) {
        this.importsJARs.add(jar);
    }

    public void setIssues(String issues) {
        this.issues = issues;
    }

    public Iterable<String> getDependsBundleNames() {
        return this.dependsBundleNames;
    }

    public Iterable<String> getImportsJARs() {
        return this.importsJARs;
    }

    public List<Bundle> getDependsBundles(Repository repository) {
        ArrayList<Bundle> bundles = new ArrayList<Bundle>();
        for (String dependBundleName: getDependsBundleNames()) {
            Bundle dependBundle = repository.getBundle(dependBundleName);
            bundles.add(dependBundle);
        }
        return bundles;
    }

    public boolean isCompiled() {
        return getBundleJAR().exists();
    }

}
