package org.anduril.core.network;

import java.util.Arrays;

public class RequirementResource {
    private final Requirement owner;
    private final String type;
    private final String contents;
    
    /**
     * Initialize.
     * @param type Type of requirement.
     * @param contents Description of the requirement. The
     *  interpretation depends on the type.
     */
    public RequirementResource(Requirement owner, String type, String contents) {
        if (contents == null) {
            throw new IllegalArgumentException("contents is null");
        }
        if (Arrays.binarySearch(Requirement.TYPES, type) < 0) {
            throw new IllegalArgumentException("Invalid requirement type: "+type);
        }
        this.owner = owner;
        this.type = type;
        this.contents = contents;
    }
    
    public String toString() {
        return String.format("Resource: %s (%s)", this.contents, this.type);
    }
    
    public Requirement getOwner() {
        return this.owner;
    }

    public String getType() {
        return type;
    }

    public String getContents() {
        return contents;
    }
}
