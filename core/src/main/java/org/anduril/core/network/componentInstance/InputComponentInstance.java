package org.anduril.core.network.componentInstance;

import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;

import java.io.File;

/**
 * Instance of the generic input component (INPUT). The input
 * component allows files to be fed into the network. Input component
 * has no associated executable file and takes input file as a
 * parameter. The input component also has no physical output file:
 * the filename of the sole output port is the same as the parameter
 * value.
 */
public class InputComponentInstance extends AbstractComponentInstance {

    /**
     * Name of the parameter for filename. 
     */
    public static final String PARAM_PATH = "path";
    
    /**
     * Name of the parameter for recursive scan of input folder.
     */
    public static final String PARAM_RECURSIVE = "recursive";
    
    /**
     * Name of the sole output port.
     */
    public static final String PORT_NAME = "out";
    
    /**
     * Name of the input component.
     */
    public static final String COMPONENT_NAME = "INPUT";
    
    private long latestModified = 0L;
    private boolean recursive = true;
    
    public InputComponentInstance(String[] name, SourceLocation location, Component component) {
        super(name, location, component);
    }
    
    @Override
    public File getDefaultOutputFile(String portName, File executionRoot) {
        if (portName.equals(Component.ERRORS_PORT_NAME) ||
            portName.equals(Component.LOG_PORT_NAME)) return null;
        
        String value = (String)this.getParameterValue(PARAM_PATH);
        // TODO: error handling		
        return new File(value);
    }
    
    @Override
    public String getConfigurationDigest(Network network) {
        StringBuilder sb = new StringBuilder(512);
        sb.append(getComponent().getName())
          .append(" OUT ").append(PORT_NAME)
          .append(" P ").append(PARAM_PATH).append('=').append(getParameterValue(PARAM_PATH));

        OutPort port =  getComponent().getOutPort(PORT_NAME);
        File file = getOutputFile(port, new File("."));          
        if (recursive) getLatestModified(file, 0);  // Scan possible directories for latest timestamp.
        else {
            latestModified = file.lastModified();
            if (file.isDirectory()) {
                File[] currentDir = file.listFiles();
                for (File f : currentDir) {
                    if (f.lastModified() > latestModified) latestModified = f.lastModified();
                }
            }
        }
        sb.append(String.format(" TS %s=%d ", port.getName(), latestModified));
        return sb.toString();
    }
    
    // Scan a file or directory tree and set latestModified 
    // to the largest (latest) file or directory timestamp found.
	private void getLatestModified(File file, int count) {
		if (file.lastModified() > latestModified) {
			latestModified = file.lastModified();
		}

		if (file.isDirectory()) {
			File[] files = file.listFiles();
			if (files == null)
				return;
			for (File f : files) {
				if (f.isFile()) {
					if (f.lastModified() > latestModified) {
						latestModified = f.lastModified();
					}
					count++;
					if (count == 1001) {
                        getLogger().warning("The recursion in "+getName()+" ("+getParameterValue(PARAM_PATH)+") has already found more than 1000 files and it may take some time.");
					}
				} else if (f.isDirectory()) {
					getLatestModified(f, count);
				}
			}
		}
	}
    
    @Override
    public boolean isExecutable() {
        return false;
    }
    
    @Override
    public void initialize(Network network) throws StaticError {
        super.initialize(network);

        boolean keep = Annotation.convertBoolean(getInstanceAnnotation(Annotation.KEEP));
        if (!keep) {
        	this.setInstanceAnnotation(Annotation.KEEP, Boolean.TRUE);
        	getLogger().warning(String.format(
        			"Warning: %s: INPUT must not have _keep=false; set to _keep=true", getName()));
        }

        Object defVal = getParameterValue(PARAM_RECURSIVE);
        if (defVal == null) defVal = getComponent().getParameter(PARAM_RECURSIVE).getDefaultValue();
        this.recursive = Annotation.convertBoolean(defVal);

        // WARNING: This routine updates the value of the PARAM_PATH assignment
        //          by converting it to an absolute path.
        Object pathValue = getParameterValue(PARAM_PATH);
        if (!(pathValue instanceof String)) {
            throw new StaticError("Cannot assign "+pathValue.getClass()+" values to "+PARAM_PATH+'.', this);
        }
        String filepath = (String)pathValue;
        if (filepath != null) {
            String filepathStr = filepath.trim();
            if (!filepathStr.isEmpty()) {
                File inFile = new File(filepathStr);
                if (!inFile.isAbsolute()) {
                	File dataDir = network.getDataDir();
                    if (dataDir == null) {
                        File sourceFile = getSourceLocation().getFile();
                        if (sourceFile != null) dataDir = sourceFile.getParentFile();
                    }
                	if (dataDir != null) {
                		inFile = new File(dataDir, inFile.getPath()).getAbsoluteFile();
                		setParameterValue(PARAM_PATH, inFile.getAbsolutePath());
                	}
                }
            }
        }
    }

    @Override
    public void launch(Engine engine) {
        String path = (String) getParameterValue(PARAM_PATH);
        File inFile = new File(path);
        if (!inFile.exists()) {
            engine.addError(new DynamicError(
                    "Input file does not exist: "+inFile.getAbsolutePath(), this));
        }
    }
    
    public OutPort getPort() {
        return getComponent().getOutPort(PORT_NAME);
    }
    
    @Override
    public File getOutputArrayIndex(OutPort port, File executionRoot) {
        return super.getOutputArrayIndex(port, executionRoot);
    }
}
