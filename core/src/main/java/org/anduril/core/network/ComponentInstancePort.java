package org.anduril.core.network;

/**
 * Data structure that represent a specific port of a specific
 * component instance. The port always belongs to the component
 * associated to the component instance. Input and output ports
 * are supported through the type parameter T. Objects of this
 * class can be used in hash tables and tree maps. 
 */
public class ComponentInstancePort<T extends Port> { //implements Comparable<ComponentInstancePort<T>> {
    private final ComponentInstance ci;
    private final T port;

    /**
     * Initialize.
     * @param ci The component instance; must not be null
     * @param port The port; must not be null and must belong to
     *  the component associated to ci
     */
    public ComponentInstancePort(ComponentInstance ci, T port) {
        if (ci == null) throw new NullPointerException("ci is null");
        if (port == null) throw new NullPointerException("port is null");
        if (ci.getComponent() != port.getComponent()) {
            throw new IllegalArgumentException(
                    "Component associated to the component instance does not own the given port");
        }
        
        this.ci = ci;
        this.port = port;
    }

    /*
    @Override
    public int compareTo(ComponentInstancePort<T> other) {
        final String s1 = String.format("%s.%s", this.ci.getName(), this.port.getName());
        final String s2 = String.format("%s.%s", other.ci.getName(), other.port.getName());
        return s1.compareTo(s2);
    }
    */
    
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof ComponentInstancePort<?>)) return false;
        ComponentInstancePort<T> cip = (ComponentInstancePort<T>)other;
        return this.ci.equals(cip.ci) && this.port.equals(cip.port);
    }
    
    @Override
    public int hashCode() {
        return this.ci.hashCode() ^ this.port.hashCode();
    }
    
    @Override
    public String toString() {
        return String.format("%s.%s", this.ci.getName(), this.port.getName());
    }
    
    /**
     * Return the component instance.
     */
    public ComponentInstance getComponentInstance() {
        return this.ci;
    }
    
    /**
     * Return the port.
     */
    public T getPort() {
        return this.port;
    }
    
    /**
     * Return true if the port is an input port, false if
     * it is an output port.
     */
    public boolean isInput() {
        return this.port instanceof InPort;
    }
    
    /**
     * Return a concrete (non-generic) type for this port.
     * If the port is not generic, return the type directly.
     * Otherwise, if the generic type is set to a concrete
     * type, return that type. For non-assigned generic
     * type, return the type that the generic type must
     * extend. This may be null, so a fully generic port
     * with no extends restriction may produce a null result.
     */
    public DataType getNongenericType() {
        if (this.port.getType().isGeneric()) {
            String typeParamName = this.port.getType().getName();
            TypeParameter typeParam = this.ci.getComponent().getTypeParameter(typeParamName);
            if (this.ci.genericTypeSet(this.port)) {
                return this.ci.getGenericType(typeParam);
            } else {
                return typeParam.getExtendsType();
            }
        } else {
            return this.port.getType();
        }
    }
}
