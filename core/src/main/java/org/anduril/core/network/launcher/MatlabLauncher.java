package org.anduril.core.network.launcher;

import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Repository;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class MatlabLauncher extends Launcher {
    private File sourceFile;
    private static String executable = "matlab";
    
    /**
     * Initialize.
     * @param type Launcher type.
     * @param args Launcher parameters that are used to configure
     *  the launcher. The set of parameters depends on the concrete
     *  launcher subclass.
     * @param component The component associated with the launcher.
     */
    public MatlabLauncher(String type, Map<String,String> args, Component component) {
        super(type, args, component);
        if (!hasArgument("file")) {
            throw new IllegalArgumentException("The Matlab launcher requires the argument 'file'");
        }
        this.sourceFile = new File(component.getMainDirectory(), getArgument("file"));
        setSourceFiles(getArgument("source"), this.sourceFile);
    }

    /**
     * Return the Matlab script source file.
     */
    public File getSourceFile() {
        return sourceFile;
    }

    @Override
    public String[] getCommand(ComponentInstance ci, File commandFile,
                               Repository repository) throws IOException {
        /* Remove file extension from the source file */
        String matlabCommand = getSourceFile().getName();
        int pos = matlabCommand.lastIndexOf('.');
        if (pos > 0) matlabCommand = matlabCommand.substring(0, pos);
        /* This points to the Matlab component skeleton, located
         * under the "matlab" directory in the root directory of
         * Anduril.
         */
        File matlabLib = new File(repository.getHomeDirectory(), "lang/matlab");

        String[] cmd = new String[] {
            executable,
            "-nodesktop",
            "-nosplash",
            "-r",
            String.format("addpath %s ; commandfile='%s' ; %s",
                    matlabLib.getAbsolutePath(),
                    commandFile.getPath(),
                    matlabCommand)
        };

        return cmd;
    }

}
