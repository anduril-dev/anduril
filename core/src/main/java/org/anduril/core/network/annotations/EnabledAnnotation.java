package org.anduril.core.network.annotations;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.ParameterType;
import org.anduril.core.network.Port;

public class EnabledAnnotation extends Annotation {
    public EnabledAnnotation () {
    	super("enabled", Boolean.TRUE, ParameterType.BOOLEAN, true, false, false);
    }

    @Override
    public String validate(Object value, ComponentInstance ci, Port port) {
        if (value instanceof Boolean) return null;
        else return String.format("Cannot cast %s to boolean " +
                "in enabled annotation in ci %s", value, ci.getName());
    }

    @Override
    public void applyFinalization(Object value, ComponentInstance ci, Port port) {

    }
}
