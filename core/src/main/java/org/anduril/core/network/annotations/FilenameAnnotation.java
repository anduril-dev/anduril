package org.anduril.core.network.annotations;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.ParameterType;
import org.anduril.core.network.Port;

public class FilenameAnnotation extends Annotation {

    public FilenameAnnotation() {
    	super("filename", null, ParameterType.STRING, false, false, true);
    }

    @Override
    public String validate(Object value, ComponentInstance ci, Port port) {
        if (value instanceof String) {
            if ( ((String)value).trim().length() == 0 ) {
                return String.format("Empty filename in filename annotation " +
                        "in ci %s", ci.getName());
            }
            return null;
        }
        else return String.format("Cannot cast %s to string in filename " +
                "annotation in ci %s", value, ci.getName());
    }

    @Override
    public void applyFinalization(Object value, ComponentInstance ci, Port port) {

    }

}
