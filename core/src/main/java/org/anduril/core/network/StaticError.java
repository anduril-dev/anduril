package org.anduril.core.network;

import org.anduril.core.utils.BaseError;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Static error that occurred when loading components, data types or network
 * topology. This class of errors excludes dynamic errors that occur during
 * network execution.
 *
 * @see org.anduril.core.engine.DynamicError
 */
public class StaticError extends BaseError {
    private static final long serialVersionUID = 1L;
    
    private SourceLocation location;
    private Component component;
    
    /**
     * Initialize.
     * @param message Error message.
     * @param location The file and position that was the source of the error.
     * 	May be null. 
     * @param component The component related to this error. May be null.
     * 	If component instance is given, this should be null.
     * @param ci The component instance related to this error. May be null.
     * @param exception A relevant caught exception. May be null.  
     */
    public StaticError(String message, SourceLocation location, Component component,
            ComponentInstance ci, Throwable exception) {
        super(message, ci, exception);
        if (location == null) {
        	this.location = new SourceLocation();
        } else {
            this.location = location;
        }
        this.component = component;
    }

    public StaticError(String message, ComponentInstance ci) {
        this(message, ci.getSourceLocation(), null, ci, null);
    }
    
    public StaticError(String message, Component component) {
        this(message, component.getSourceLocation(), component, null, null);
    }
    
    public StaticError(String message, SourceLocation location) {
        this(message, location, null, null, null);
    }
    
    public StaticError(String message, SourceLocation location, Throwable exception) {
        this(message, location, null, null, exception);
    }

    public StaticError(String message, Throwable exception) {
        this(message, null, null, null, exception);
    }

    public StaticError(String message) {
        this(message, null, null, null, null);
    }
    
    /**
     * Return the location that was the source of the error.
     * This is never null, but any of the location attributes
     * may be null.
     */
    public SourceLocation getLocation() {
        return location;
    }
    
    /**
     * Return the component related to this error.
     */
    public Component getComponent() {
        return component;
    }
    
    /**
     * Format the error as a string. The string has as a header
     * the source of the error, such as a component name, and
     * after that, the error message. 
     */
    public String format() {
        StringBuilder sb = new StringBuilder();
        
        if (!location.allNull()) {
            sb.append(location.toString());
        }
        
        if (getComponentInstance() != null) {
            if (sb.length() > 0) sb.append(", ");
            sb.append("component ").append(getComponentInstance().getName());
        } else if (getComponent() != null) {
            if (sb.length() > 0) sb.append(", ");
            sb.append("component ").append(getComponent().getName());
        }
        
        if (sb.length() > 0) sb.append(": ");
        if (getMessage() != null) sb.append(getMessage());
        
        if (getException() != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            getException().printStackTrace(printWriter);
            printWriter.close();
            sb.append(' ').append(stringWriter.toString());
        }
        
        return sb.toString();
    }

}
