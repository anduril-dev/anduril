package org.anduril.core.engine;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.Network;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Read and write state files that store the execution
 * states of component instances. Implemented using
 * a text file.
 */
public class StateFile implements Iterable<StateFile.StateRecord> {
    public static class StateRecord {
        public final String instanceName;
        public final CurrentState currentState;
        public final ActiveState activeState;
        public final String digest;
        public StateRecord(String instanceName,
                CurrentState currentState, ActiveState activeState, String digest) {
            this.instanceName = instanceName;
            this.currentState = currentState;
            this.activeState = activeState;
            this.digest = digest;
        }
    }

    private boolean closed = false;
    private File stateFile;
    private Map<String,StateRecord> cache;
    
    protected void finalize() {
        if (!this.closed) {
            try {
                close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void open(File stateFile) throws IOException {
        if (this.cache != null) {
            throw new IOException("State file is already open");
        }
        
        this.cache = new HashMap<String, StateRecord>();
        this.stateFile = stateFile;
        if (!stateFile.exists()) return;
        
        BufferedReader reader = new BufferedReader(new FileReader(this.stateFile));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("\t");
                if (tokens.length != 4) {
/*                    Log.warning("Warning: Invalid line in state file: "+line);*/
                    continue;
                }
                
                final String name = tokens[0];
                CurrentState currentState = CurrentState.fromString(tokens[1]);
                ActiveState activeState = ActiveState.fromString(tokens[2]);
                final String digest = tokens[3];
                
                if (activeState == ActiveState.DISABLED) {
                    activeState = ActiveState.YES;
                }
                StateRecord record = new StateRecord(name, currentState, activeState, digest);
                this.cache.put(name, record);
            }            
            
        } finally {
            reader.close();
        }
    }
    
    public void close() throws IOException {
    	this.closed = true;
        this.stateFile = null;
        this.cache = null;
    }

    public void commit() throws IOException {
        File tempFile = new File(this.stateFile+".tmp");
        
        FileWriter writer = new FileWriter(tempFile);
        try {
            for (StateRecord record: this) {
                writer.write(record.instanceName);
                writer.append('\t').write(record.currentState.getID());
                writer.append('\t').write(record.activeState.getID());
                writer.append('\t').write(record.digest);
                writer.append('\n');
            }
        } finally {
            writer.close();
        }
        
        boolean renameOK = tempFile.renameTo(this.stateFile);
        if (!renameOK) {
            /* Renaming fails on Windows if target exists. As a workaround,
             * we delete the target first and rename then. This leaves a
             * short window of time when no state file exists, making the
             * operation non-atomic. */
            
            if (this.stateFile.exists()) {
                boolean deleteOK = this.stateFile.delete();
                if (!deleteOK) {
                    throw new IOException("Deleting final state file prior to renaming failed");
                }
                renameOK = tempFile.renameTo(this.stateFile);
            }
            
            if (!renameOK) {
                throw new IOException("Renaming temporary state file to final state file failed");
            }
        }
    }

    public Iterator<StateRecord> iterator() {
        return this.cache.values().iterator();
    }

    public void write(StateRecord record) {
        this.cache.put(record.instanceName, record);
    }

    public void write(ComponentInstance ci, Engine engine) throws IOException {
        write(new StateRecord(ci.getName(),
                engine.getCurrentState(ci), engine.getActiveState(ci),
                ci.getConfigurationDigest(engine.getNetwork())));
    }
    
    public void writeAll(Network network, Engine engine) throws IOException {
        for (ComponentInstance ci: network) {
            write(ci, engine);
        }
    }

    public void register(ComponentInstance ci, Engine engine) throws IOException {
        register(new StateRecord(ci.getName(),
                CurrentState.NO, engine.getActiveState(ci),
                ci.getConfigurationDigest(engine.getNetwork())));
    }
    
    public void clean(Network network) throws IOException {
        Iterator<StateRecord> iter = iterator();
        while (iter.hasNext()) {
            StateRecord record = iter.next();
            ComponentInstance ci = network.getComponentInstance(record.instanceName);
            if (ci == null) iter.remove();
        }
    }
    /**
     * Works like write but doesn't overwrite an existing record.
     * Only adds the record if a record by an identical instanceName doesn't exist.
     * The function is used to fill the state file with instances that are going
     * to be run before they are completed for the first time.
     * @param record
     * @throws IOException
     */
    public void register(StateRecord record) {
        if (!this.cache.containsKey(record.instanceName)) {
            this.cache.put(record.instanceName, record);
        }
    }
    
    public boolean remove(StateRecord record) {
        return this.cache.remove(record.instanceName) != null;
    }
}
