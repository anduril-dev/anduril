package org.anduril.core.engine;

/**
 * Dynamic state of a component instance that describes whether
 * whether the instance is up-to-date. 
 */
public enum CurrentState {
    YES ("YES"),
//    NOT_ON_DISK ("NOT_ON_DISK"),
    NO ("NO");
    
    private String id;
    
    private CurrentState(String id) {
        this.id = id;
    }
    
    public String getID() {
        return id;
    }
    
    /**
     * Return true if the state is YES or
     * NOT_ON_DISK.
     */
    public boolean isCurrent() {
        return this == YES; // || this == NOT_ON_DISK;
    }
    
    public static CurrentState fromString(String ID) {
        for (CurrentState state: CurrentState.values()) {
            if (state.getID().equals(ID)) return state;
        }
        return null;
    }

}
