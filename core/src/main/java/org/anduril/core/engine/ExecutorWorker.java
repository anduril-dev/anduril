package org.anduril.core.engine;

import org.anduril.core.network.ComponentInstance;

/**
 * Worker thread that executes a component. Instances
 * of this class are created by {@link ComponentExecutor}
 * that acts as a coordinator.
 */
public class ExecutorWorker extends Thread {
    private ComponentExecutor coordinator;
    private Engine engine;
    private ComponentInstance job;
    
    /**
     * Initialize.
     * @param coordinator The coordinator that created this worker.
     * @param job The job that represents the payload, i.e. pending
     * 	component execution.
     */
    public ExecutorWorker(ComponentExecutor coordinator, Engine engine, ComponentInstance job) {
        if (coordinator == null) throw new NullPointerException("coordinator is null");
        if (job == null) throw new NullPointerException("job is null");
        
        this.coordinator = coordinator;
        this.engine = engine;
        this.job = job;
    }
    
    public Engine getEngine() {
        return this.engine;
    }
    
    public ComponentInstance getJob() {
        return this.job;
    }

    @Override
    /** 
     * Execute the component and call {@link ComponentExecutor#workerFinished}
     * afterwards.
     */
    public void run() {
        String msg = String.format("Worker started: component %s, thread %s",
                job.getName(), this);
        this.job.getLogger().debug(msg);
        
        try {
            getJob().launch(engine);

            msg = String.format("Worker finished: component %s, thread %s",
                    job.getName(), this);
            this.job.getLogger().debug(msg);
        } catch (Exception e) {
            engine.addError(
                    new DynamicError("Uncaught exception during component execution",
                            job, e));
        } finally {
            try {
                coordinator.workerFinished(this);
            } catch (Exception e) {
                engine.addError(
                        new DynamicError("Uncaught exception during component execution",
                                job, e));
            }
        }
    }

}
