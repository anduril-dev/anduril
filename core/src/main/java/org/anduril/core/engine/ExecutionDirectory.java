package org.anduril.core.engine;

import org.anduril.component.StringList;
import org.anduril.component.Tools;
import org.anduril.core.network.Component;
import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.OutPort;
import org.anduril.core.network.componentInstance.OutputComponentInstance;
import org.anduril.core.utils.IOTools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Controls the contents of an execution directory that
 * stores component outputs and the state file.
 */
public class ExecutionDirectory {
    public static final String COMMAND_FILE = "_command";
    public static final String STATE_FILE = "_state";
    public static final String TEMP_STATE_FILE = "_state_temp";
    public static final String LOG_DIRECTORY = "_log";
    public static final String GLOBAL_LOG_FILE = "global-log.txt";

    private Engine engine;
    private File root;
    
    /**
     * Initialize.
     * @param engine The associated execution engine.
     * @param root Root of the execution directory.
     */
    public ExecutionDirectory(Engine engine, File root) {
        if (root != null && root.isFile()) {
            String msg = String.format("Execution directory is a file; it must be a directory: %s",
                    root.getAbsolutePath());
            throw new IllegalArgumentException(msg);
        }
        
        this.engine = engine;
        this.root = root.getAbsoluteFile();
    }
    
    /**
     * Return the associated execution engine.
     */
    public Engine getEngine() {
        return engine;
    }
    
    /**
     * Return the root of the execution directory.
     */
    public File getRoot() {
        return root;
    }
    
    /**
     * Return the sub-directory for a given component instance.
     * This directory contains the command file and outputs
     * of the CI.
     */
    public File getComponentInstanceRoot(ComponentInstance ci) {
        if (ci.getName().equals(OutputComponentInstance.OUTPUT_DIRECTORY))
            return new File(root, '_'+ci.getName());
        else
            return new File(root, ci.getName());
    }
    
    /**
     * Return the path of the command file for given component
     * instance.
     */
    public File getCommandFile(ComponentInstance ci) {
        return new File(getComponentInstanceRoot(ci), COMMAND_FILE);
    }

    public File getTempDirectory(ComponentInstance ci) {
        return new File(getComponentInstanceRoot(ci), Component.TEMP_DIRECTORY_PORT_NAME);
    }
    
    /**
     * Remove temporal directory for a given component instance.
     * This operation is performed once the <code>AbstractComponentInstance</code> 
     * launcher method has completed execution.
     */
    public void removeTempDirectory(ComponentInstance ci) {
		File tempDir = getTempDirectory(ci);
		if (tempDir.exists()) {
			boolean isSubDirsRemoved = IOTools.rmdir(tempDir, true);
			boolean isDirRemoved = tempDir.delete();
			if (!isSubDirsRemoved || !isDirRemoved) {
				ci.getLogger().warning("Could not remove temporary directory: " + tempDir);
			}
		}
    }

    public void clearTempDirectory(ComponentInstance ci) {
    	File tempDir = getTempDirectory(ci);
//    	if (!tempDir.exists()) {
//    		boolean isDirCreated = tempDir.mkdir();
//    		if (!isDirCreated) Log.warning("Could not create temporary directory for " + ci.getName());
//    		return;
//    	}
    	if (tempDir.exists()) {
	    	// Remove files and subdirectories, not the directory.
	    	IOTools.rmdir(tempDir, true);
    	}
    }
    
    /**
     * Create the sub-directory for a given component instance.
     * All parent directories are created as well if necessary.
     */
    public void createComponentInstanceRoot(ComponentInstance ci) {
        getComponentInstanceRoot(ci).mkdirs();
    }
    
    /**
     * Prepare the special output directory that contains
     * products of OUTPUT component instances. If the directory
     * does not exist, it is created. Otherwise, those files
     * that belong to OUTPUT nodes that have current-state=YES and
     * active-state=YES are retained. All other files from the
     * output directory are deleted.
     */
    public void initOutputDirectory() {
        File dir = new File(getRoot(), OutputComponentInstance.OUTPUT_DIRECTORY);
        if (!dir.exists()) {
            dir.mkdirs();
            return;
        }
        
        Set<File> deletableFiles = new HashSet<File>();
        for (File file: dir.listFiles()) {
            deletableFiles.add(file.getAbsoluteFile());
        }
        for (ComponentInstance ci: getEngine().getNetwork()) {
            if (!(ci instanceof OutputComponentInstance)) continue;
            
            if (getEngine().getCurrentState(ci).isCurrent() && getEngine().getActiveState(ci).isActive()) {
                for (OutPort port: ci.getComponent().getOutPorts()) {
                    File output = ci.getOutputFile(port, getRoot()).getAbsoluteFile();
                    if (output.exists()) deletableFiles.remove(output);
                }
            }
        }
        
        for (File deletable: deletableFiles) {
            try {
                Tools.removeFile(deletable);
            } catch(IOException e) {
                getEngine().addError(new DynamicError("Can not clear OUTPUT directory: "+e));
            }
        }
    }
    
    /**
     * Write a command file for given component instance.
     * @param ci The component instance
     * @throws IOException if an I/O error occurs
     */
    public void writeCommandFile(ComponentInstance ci) throws IOException {
        if (!ci.isExecutable()) return;
        createComponentInstanceRoot(ci);
        new CommandFileWriter(ci, getCommandFile(ci), this).write(); 
    }
    
    /**
     * Read a list of error messages for given component instance from
     * an error stream file.
     * Return messages as strings. Separator strings have been filtered
     * out; the list is ready to be printed out.
     */
    public List<String> readComponentInstanceErrors(ComponentInstance ci) {
        File file = ci.getOutputFile(
                ci.getComponent().getOutPort(Component.ERRORS_PORT_NAME),
                getRoot());
        List<String> list = new ArrayList<String>();
        if (file == null || !file.canRead()) return list;
        
        try {
            list = StringList.read(file);
        } catch(IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Read a list of log messages for given component instance from
     * a log stream file.
     * Return messages as strings. Separator strings have been filtered
     * out; the list is ready to be printed out.
     */
    public List<String> readComponentInstanceLog(ComponentInstance ci) {
        File file = ci.getOutputFile(
                ci.getComponent().getOutPort(Component.LOG_PORT_NAME),
                getRoot());
        List<String> list = new ArrayList<String>();
        if (file == null || !file.canRead()) return list;
        
        try {
            list = StringList.read(file);
        } catch(IOException e) {
            e.printStackTrace();
        }
        
        return list;
    }
    
    /**
     * Check if output files exist after execution. Return list
     * of OutPort instances that have the output file missing.
     * @param ci
     */
    public Set<OutPort> missingOutputFiles(ComponentInstance ci) {
        Set<OutPort> missing = new HashSet<OutPort>();
        for (OutPort port: ci.getComponent().getOutPorts()) {
            if (!outputFileExists(ci, port)) {
                missing.add(port);
            }
        }

        return missing;
    }

    /**
     * Return true, if output file exists. false otherwise.
     *
     * @param ci
     * @param port
     */
    private boolean outputFileExists(ComponentInstance ci, OutPort port) {
        File file = ci.getOutputFile(port, getRoot());
        if (file == null) {
            return false;
        } else {
            // Java 1.6 implementation
            if (!file.exists()) {
               return false;
            }
            // Java 1.7 implementation suggested by Michael Clarke.
                /*
                try {  FileSystems.getDefault().provider().checkAccess(file.toPath()); }
                catch (IOException e) {
                   getEngine().getLogger().info(ci.getName()+'.'+port.getName()+" is not accessible: "+e.getMessage());
                   missing.add(port);
                }
                */
        }

        return true;
    }


    /**
     * Check if output files exist prior to execution that should
     * trigger re-execution of the component. Return list
     * of OutPort instances that have the output file missing.
     * 
     * Only files that are actually produced by the component
     * instance (and not e.g. linked by it) should be reported. 
     * @param ci
     */
    public Set<OutPort> missingOwnOutputFiles(ComponentInstance ci) {
        Set<OutPort> missing = new HashSet<OutPort>();
        for (OutPort port: ci.getComponent().getOutPorts()) {
            boolean isReal = ci.isFileReal(port, getRoot());
            if (isReal) {
            	File file = ci.getOutputFile(port, getRoot());
            	if (file == null || !file.exists()) {
            		missing.add(port);
            	}
            }
        }
        
        return missing;
    }
    
    /**
     * Clear the sub-directory for a component instance, usually
     * in preparation for component execution.
     */
    public void clearComponentDirectory(ComponentInstance ci) {
        if (!componentRootExists(ci)) return;
        ci.getLogger().info("Clearing directory "+getComponentInstanceRoot(ci));
        File dir = getComponentInstanceRoot(ci);
        IOTools.rmdir(dir, true);
        if (dir.exists()) {
        	String[] content = dir.list();
        	if (content == null) {
        	   engine.addError(new DynamicError("Cannot clear "+dir+" since it is not an accessible directory."));
        	} else if (content.length > 0) {
               engine.addError(new DynamicError("Cannot clear directory "+dir+" since the following files cannot be removed:"+Arrays.toString(dir.list())));
        	}
        }
    }

    /**
     * Return true if the sub-directory for a component instance
     * exists.
     */
    public boolean componentRootExists(ComponentInstance ci) {
        return getComponentInstanceRoot(ci).exists();
    }
    
    /**
     * Return the path of the state file.
     */
    public File getStateFile() {
        return new File(getRoot(), STATE_FILE);
    }

    /**
     * Return the path of the temporary state file.
     * This file is used to implement two-stage state
     * file writing.
     */
    public File getTemporaryStateFile() {
        return new File(getRoot(), TEMP_STATE_FILE);
    }

    public File getLogFile(ComponentInstance ci) {
        final File logDir = new File(getRoot(), LOG_DIRECTORY);
        if (ci == null) return new File(logDir, GLOBAL_LOG_FILE);
        else return new File(logDir, ci.getName());
    }
    
    public void appendLog(ComponentInstance ci, String message) {
        File logFile = getLogFile(ci);
        if (!logFile.getParentFile().exists()) {
            logFile.getParentFile().mkdirs();
        }
        
        try {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(logFile, true));
                writer.write(message);
                if (!message.endsWith("\n") && !message.endsWith("\r")) {
                    writer.write('\n');
                }
            }  finally {
                if (writer != null) writer.close();
            }
        } catch(IOException e) {
            getEngine().addError(new DynamicError("I/O error when writing to log: "+e, ci));
        } 
    }
    
    public void deleteLogFile(ComponentInstance ci) {
        File logFile = getLogFile(ci);
        if (logFile.exists()) {
            logFile.delete();
        }
    }
}
