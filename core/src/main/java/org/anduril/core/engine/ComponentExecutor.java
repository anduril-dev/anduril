package org.anduril.core.engine;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.utils.Logger;

import java.util.*;

/**
 * A singleton monitor that coordinates the multi-threaded execution
 * of components. When the engine executes a component, it first
 * obtains a job from the monitor, which represent a (pending)
 * component execution. The component instance to be executed is
 * removed from the READY queue of the engine. Then, the engine
 * obtains a worker from the monitor. This is a thread that
 * actually executes the component. When the worker is done,
 * it notifies the monitor, which notifies the engine.
 * 
 * <p>The maximum number of concurrent threads is given as
 * a parameter.
 */
public class ComponentExecutor {
    public static final int DEFAULT_THREADS = 4;
    
    private static final ComponentExecutor instance = new ComponentExecutor(DEFAULT_THREADS);
    
    /** The number of currently active workers. */
    private int numWorkers;
    
    /** The maximum number of workers. */
    private int maxWorkers;
    
    /** For each engine, the number of jobs that have been
     * handed out but not finished. */
    final private Map<Engine, Integer> unfinishedJobs;
    
    /** Used for printing debug messages */
    private long baselineNanotime;
    
    /**
     * Initialize.
     * @param threads The maximum number of concurrent threads,
     * 	greater or equal to 1.
     */
    private ComponentExecutor(int threads) {
        this.numWorkers = 0;
        this.maxWorkers = threads;
        this.unfinishedJobs = new HashMap<Engine, Integer>();
        this.baselineNanotime = System.nanoTime();
        setMaxThreads(threads);
    }
    
    /**
     * Return the singleton instance.
     */
    public static ComponentExecutor getInstance() {
        return instance;
    }
    
    /**
     * Return the maximum number of concurrent threads.
     */
    public int getMaxThreads() {
        return this.maxWorkers;
    }
    
    /**
     * Set the maximum number of concurrent threads.
     * @param threads Number of threads. Must be at least 1.
     * @throws IllegalArgumentException if threads is < 1
     * @throws RuntimeException if some workers are active
     */
    public synchronized void setMaxThreads(int threads) {
        if (threads < 1) throw new IllegalArgumentException("threads is < 1");
        if (this.numWorkers > 0) {
            throw new RuntimeException("Can't set number of threads: some threads are active");
        }
        this.maxWorkers = threads;
    }

    private ComponentInstance fetchNextComponentInstance(Engine engine) {
        final Queue<ComponentInstance> readyQueue = engine.getReady();
        
        ComponentInstance ci = readyQueue.peek();
        if (ci == null) {
            return null;
        }

        return readyQueue.poll();
    }
    
    /**
     * Pop a component instance from the READY set of the engine
     * and return the corresponding job. If the READY set is
     * non-empty, return immediately. Otherwise, block until
     * all jobs for the engine are finished. Then, if the
     * READY set is non-empty, return a job; otherwise, return
     * null. When null is returned, there will be no more jobs.
     * 
     * <p>When a non-null job is obtained, it must always be
     * handled. Either {@link #workerFinished} or {@link #abortJob}
     * must be called for that job.
     */
    public synchronized ComponentInstance getJob(Engine engine) {
        ComponentInstance ci = fetchNextComponentInstance(engine);
        
        if (ci != null) {
            Integer unfinished = this.unfinishedJobs.get(engine);
            if (unfinished == null) unfinished = 0;
            unfinished++;
            this.unfinishedJobs.put(engine, unfinished);
            writeLog(String.format(
                    "gave job for %s; unfinished jobs: %d",
                    ci.getName(), unfinished), ci.getLogger());
            return ci;
        }
        
        while (true) {
            Integer unfinished = this.unfinishedJobs.get(engine);
            if (unfinished != null && unfinished > 0 && engine.getReady().isEmpty()) {
                try { wait(); }
                catch (InterruptedException e) {}
            }
            else break;
        }
        
        ci = fetchNextComponentInstance(engine);
        if (ci != null) {
            Integer unfinished = this.unfinishedJobs.get(engine);
            if (unfinished == null) unfinished = 0;
            unfinished++;
            this.unfinishedJobs.put(engine, unfinished);
            writeLog(String.format(
                    "gave job for %s; unfinished jobs: %d",
                    ci.getName(), unfinished), ci.getLogger());
            return ci;
        } else {
            return null;
        }
    }
    
    /**
     * Abort the processing of a job before a worker
     * has been assigned to it.
     * @param job The job to be aborted.
     */
    public synchronized void abortJob(Engine engine, ComponentInstance job) {
        Integer unfinished = this.unfinishedJobs.get(engine);
        if (unfinished == null || unfinished <= 0) {
            error(job, "unfinishedJobs[engine] is " + unfinished);
        }
        unfinished--;
        this.unfinishedJobs.put(engine, unfinished);
        writeLog(String.format(
                "aborted job for %s; unfinished jobs: %d",
                job.getName(), unfinished), job.getLogger());
        notifyAll();
    }
    
    /**
     * Obtain a worker thread for the given job. The thread
     * is not yet started; it must be done manually with
     * <code>worker.start()</code>. If there are no free workers,
     * block until there is a free worker.
     */
    public synchronized ExecutorWorker getWorker(Engine engine, ComponentInstance job) {
        if (this.numWorkers < 0 || this.numWorkers > this.maxWorkers) {
            error(job, "numWorkers is "+this.numWorkers);
        }

        while (this.numWorkers >= this.maxWorkers) {
            try { wait(); }
            catch (InterruptedException e) {}
        }
        
        this.numWorkers++;

        if (this.numWorkers > this.maxWorkers) {
            error(job, "numWorkers is "+this.numWorkers);
        }
        
        writeLog(String.format("gave worker for %s", job.getName()), job.getLogger());
        return new ExecutorWorker(this, engine, job);
    }
    
    /**
     * Called by a execution worker thread when the
     * worker is finished.
     * The engine is notified of finished component execution.
     */
    public synchronized void workerFinished(ExecutorWorker worker) {
        this.numWorkers--;

        try {
            ComponentInstance job = worker.getJob();

            Integer unfinished = this.unfinishedJobs.get(worker.getEngine());
            if (unfinished == null || unfinished <= 0) {
                error(job, "unfinishedJobs[engine] is " + unfinished);
            }
            unfinished--;
            this.unfinishedJobs.put(worker.getEngine(), unfinished);
            writeLog(String.format(
                    "worker finished for %s; unfinished jobs: %d",
                    job.getName(), unfinished), job.getLogger());
            worker.getEngine().notifyComponentFinished(job);
        } finally {
            notifyAll();
        }
    }
    
    /**
     * Return true if there are unfinished jobs for the
     * engine.
     */
    public synchronized boolean hasPendingJobs(Engine engine) {
        Integer unfinished = this.unfinishedJobs.get(engine);
        return unfinished != null && unfinished > 0; 
    }
    
    private void error(ComponentInstance job, String message) throws RuntimeException {
        String s = String.format("Job %s: %s",
                job.getName(), message);
        throw new RuntimeException(s);
    }
    
    private final void writeLog(String message, Logger logger) {
        if (!Logger.isVerbose()) {
            return;
        }

        final double time = (System.nanoTime() - this.baselineNanotime) / 1e9;
        String msg = String.format(Locale.US,
                "ComponentExecutor: %s; numWorkers: %d; time: %.3f s",
                message, this.numWorkers, time);
        logger.debug(msg);
    }
}
