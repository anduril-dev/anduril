package org.anduril.core.engine;

import org.anduril.component.IndexFile;
import org.anduril.component.Tools;
import org.anduril.core.engine.StateFile.StateRecord;
import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.network.annotations.ExecuteAnnotation;
import org.anduril.core.network.componentInstance.InputComponentInstance;
import org.anduril.core.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Workflow execution engine that executes a component network.
 * The engine mains a state for each component instance
 * that indicates whether the component has been executed and whether
 * it is active. States may be read from a state file if the network
 * is being re-executed.
 * The engine also maintains a ready-set that contains components
 * that are ready to be executed (i.e. whose state is State.READY).
 * Components are executed in parallel using a user-configurable
 * number of threads.
 */
public class Engine {
    private Network network;
    private final ExecutionDirectory execDir;
    private final StateFile stateFile;
    private int stage;
    private boolean finalStage;
    private Engine previousStageEngine;
    private boolean commitFinalState;
    private Logger parentLogger;

    private Map<ComponentInstance, CurrentState> currentStates;
    private Map<ComponentInstance, ActiveState> activeStates;
    private Map<ComponentInstance, Integer> preCounts;
    private Map<ComponentInstancePort<OutPort>, Integer> postCounts;
    private Queue<ComponentInstance> ready;
    private HashSet<ComponentInstance> force;
    private List<DynamicError> errors;
    private List<ComponentInstance> failed;
    private List<ComponentInstance> active;

    /**
     * Strings with format "hostID:componentInstance/portName" that indicate a
     * file is known to be present in a remote host.
     */
    private Set<String> remoteFiles;

    /**
     * Strings with format "hostID:directory" that indicate a directory is known
     * to be present in a remote host.
     */
    private Set<String> remoteDirectories;

    class DynamicExecutionState {
		public DynamicExecutionState() {
            this.trackedArrayInputFiles = new HashMap<File, ComponentInstancePort<OutPort>>();
            this.finished = new HashSet<ComponentInstance>();
            this.failed = new HashSet<ComponentInstance>();
        }
		/**
         * ComponentInstances that are fed into arrays and are marked for disk
         * optimization need bookkeeping. This map links them to their originating
         * ports.
         */
        private Map<File, ComponentInstancePort<OutPort>> trackedArrayInputFiles;

        private HashSet<ComponentInstance> finished;
        private HashSet<ComponentInstance> failed;
    }

    private DynamicExecutionState execState;
	private Map<ComponentInstance, Set<ComponentInstancePort<OutPort>>> staticIndirectDependencies;
	private Map<ComponentInstance, Set<ComponentInstancePort<OutPort>>> dynamicIndirectDependencies;

    class PriorityComparator implements Comparator<ComponentInstance> {
        @Override
        public int compare(ComponentInstance first, ComponentInstance second) {
            return -Integer.valueOf(first.getPriority()).compareTo(second.getPriority());
        }
    }

    /**
     * Construct an initial (stage-0) engine.
     * 
     * @param network
     *            The network that is to be executed.
     * @param executionRoot
     *            The execution directory that is used to store component
     *            outputs and the state file.
     * @param isFinalStage If true, this engine is the last engine to be
     *            executed for the network; if false, a later-stage
     *            engine will follow.
     * @param readPreviousState
     *            If true and the state file from previous execution is
     *            available, previous states are read from that file. This is
     *            generally recommended.
     * @param force
     *            Set of component instances whose execution is forced.
     */
    public Engine(Network network, File executionRoot, boolean isFinalStage,
                  boolean readPreviousState, Set<ComponentInstance> force, Logger parentLogger) {
        initCommon(network, isFinalStage, force, parentLogger);

        if (executionRoot == null) {
            throw new NullPointerException("executionRoot is null");
        }
        this.execDir = new ExecutionDirectory(this, executionRoot);
        this.stateFile = new StateFile();
        this.stage = 0;

        this.execState = new DynamicExecutionState();
        this.remoteFiles = new HashSet<String>();
        this.remoteDirectories = new HashSet<String>();
        this.staticIndirectDependencies = new HashMap<ComponentInstance, Set<ComponentInstancePort<OutPort>>>();
        this.dynamicIndirectDependencies = new HashMap<ComponentInstance, Set<ComponentInstancePort<OutPort>>>();

        initStates(readPreviousState, force);
        backwardInferenceOnceAnnotation();
    }

    /**
     * Construct a state-N engine that takes dynamic information from the
     * previous (N-1) engine.
     * @param previousStageEngine Stage N-1 engine.
     */
    public Engine(Engine previousStageEngine, Network network, boolean isFinalStage, Set<ComponentInstance> force, Logger parentLogger) {
        initCommon(network, isFinalStage, force, parentLogger);

        this.previousStageEngine = previousStageEngine;
        this.stage = previousStageEngine.getStage() + 1;

        this.execDir = new ExecutionDirectory(this, previousStageEngine.getExecutionDirectory().getRoot());
        this.stateFile = previousStageEngine.stateFile;
        this.execState = previousStageEngine.getDynamicExecutionState();
        this.remoteFiles = previousStageEngine.remoteFiles;
        this.remoteDirectories = previousStageEngine.remoteDirectories;
        this.staticIndirectDependencies = previousStageEngine.staticIndirectDependencies;
        this.dynamicIndirectDependencies = previousStageEngine.dynamicIndirectDependencies;

        initStates(true, force);
        backwardInferenceOnceAnnotation();
    }

    private void initCommon(Network network, boolean isFinalStage, Set<ComponentInstance> force, Logger parentLogger) {
        this.network = network;
        this.finalStage = isFinalStage;
        this.commitFinalState = true;
        this.parentLogger = parentLogger;

        this.currentStates = new HashMap<ComponentInstance, CurrentState>();
        this.activeStates = new HashMap<ComponentInstance, ActiveState>();
        this.preCounts = new HashMap<ComponentInstance, Integer>();
        this.postCounts = new HashMap<ComponentInstancePort<OutPort>, Integer>();
        this.ready = new PriorityQueue<ComponentInstance>(11, new PriorityComparator());
        this.errors = Collections.synchronizedList(new ArrayList<DynamicError>());

        this.failed = Collections.synchronizedList(new ArrayList<ComponentInstance>());
        this.active = Collections.synchronizedList(new ArrayList<ComponentInstance>());

        if (force == null) this.force = new HashSet<ComponentInstance>();
        else this.force = new HashSet<ComponentInstance>(force);
    }

    // Basic functionality /////////////////////////////////////////////

    public String toString() {
        StringBuilder sb = new StringBuilder(512);
        sb.append("ready queue: ");
        for (ComponentInstance ci : ready) {
            sb.append("\n    " + ci.getName());
        }
        sb.append('\n');
        return sb.toString();
    }

    public void setCommitFinalState(boolean value) {
        this.commitFinalState=value;
    }
 
    /**
     * Returns dynamic execution state which stores information
     * between dynamic evaluation passes.
     */
    public DynamicExecutionState getDynamicExecutionState() {
        return this.execState;
    }

    public HashSet<ComponentInstance> getForced() {
        return this.force;
    }

    public int getStage() {
        return this.stage;
    }

    public boolean isFinalStage() {
        return this.finalStage;
    }

    public Engine getPreviousStageEngine() {
        return this.previousStageEngine;
    }

    public StateFile getStateFile() {
        return this.stateFile;
    }

    /**
     * Initialize states and counts of component instances so that the engine is
     * ready for execution. Initialization takes into account execute
     * annotations, disabled nodes, state file and forced component instances.
     * 
     * @param readPreviousState
     *            If true, read previous states from a state file.
     * @param force Set of component instances whose execution is forced.
     */
    public void initStates(boolean readPreviousState, Set<ComponentInstance> force) {

        if (force == null) force = new HashSet<ComponentInstance>();

        for (ComponentInstance ci : network) {
            if (ci.isDisabled() || this.execState.failed.contains(ci)) {
                this.activeStates.put(ci, ActiveState.DISABLED);
            } else {
                this.activeStates.put(ci, ActiveState.YES);
            }
            this.currentStates.put(ci, CurrentState.NO);
        }

        try {
            this.stateFile.open(this.execDir.getStateFile());
        } catch (IOException e) {
            addError(new DynamicError("Cannot open state file: " + e));
        }

        Set<ComponentInstance> changed = new HashSet<ComponentInstance>();
        if (readPreviousState)
            readStateFile(changed);

        for (ComponentInstance forced : force) {
            changed.add(forced);
            changed.addAll(forced.getReachableComponents(this));
        }

        for (ComponentInstance ci : this.execState.finished) {
            changed.addAll(ci.getReachableComponents(this));
        }

        changed.removeAll(this.execState.finished);

        for (ComponentInstance ci : network) {
            if (getCurrentState(ci).isCurrent()) {
                ci.locateGenericOutputFiles(this.getExecutionDirectory().getRoot());

                if (getCurrentState(ci).isCurrent() && getActiveState(ci).isActive() && !markedForDiskOptimization(ci)
                        && !this.execDir.missingOwnOutputFiles(ci).isEmpty()) {
                    this.currentStates.put(ci, CurrentState.NO);
                    changed.addAll(ci.getReachableComponents(this));
                    ci.getLogger().info("Output files are not present");
                }
            }
        }

        for (ComponentInstance ci : changed) {
        	String execute = (String)ci.getInstanceAnnotation(Annotation.EXECUTE);
            boolean once = (execute != null && execute.equals(ExecuteAnnotation.Execute.once.toString()));

            boolean isForced = force.contains(ci);
            if (!(once && getCurrentState(ci).isCurrent()) || isForced) {
                this.currentStates.put(ci, CurrentState.NO);
            }
        }

        triggerIfMissingOutputsAreNeeded();

        computeCounts();
    }

    
    /**
     * Run during state initialization.
     * 
     * For each component in reverse topological (dependency) order, do:
     * 
	 * Check all outgoing connections to components that should be re-executed.
	 * If related output is missing, the component is re-triggered.
     * 
     * In this way upstream components, later in the iteration, producing
     * input for this component know that they should be rerun if their
     * output is missing.
     *
     */
	private void triggerIfMissingOutputsAreNeeded() {
		ListIterator<ComponentInstance> iter = network.getMembersDFS(false);
        while (iter.hasPrevious()) {
        	ComponentInstance ci = iter.previous();
            if (getCurrentState(ci).isCurrent()) {
                for (OutPort port : ci.getComponent().getOutPorts()) {
                    final File output = ci.getOutputFile(port, this.execDir.getRoot());
                    if (!output.exists()) {
                        for (PortConnection conn : ci.getOutConnections(port)) {
                            if (isPending(conn.getTo())) {
                                this.currentStates.put(ci, CurrentState.NO);
                            }
                        }
                    } else triggerOnMissingArrayReferences(ci, port); 
                }
            }
        }
	}

    /**
     * Run during state initialization.
     * 
     * Inspects component instance's out-port. If it's an array, checks
     * all connections to components that should be re-executed.
     * If the index file produced by this component is determined to
     * have dangling references to files, this component is re-triggered.
     * 
     * In this way upstream components producing input for this component
     * know that they should be rerun if their output is missing, or
     * similarly, if they produced an array for this component that
     * also has dangling references.
     * 
     * @param ci Component instance whose upstream should be triggered
     * @param port Out-port
     */
	private void triggerOnMissingArrayReferences(ComponentInstance ci,
			OutPort port) {
		if ( !ci.getPortIsArray(port).isTrue() )
			return;

		for (PortConnection conn : ci.getOutConnections(port)) {
		    if (isPending(conn.getTo())) {
				try {
					IndexFile inputIndex = IndexFile.read(ci.getOutputArrayIndex(conn.getFromPort(),
					        this.execDir.getRoot()).getAbsoluteFile());
		            for (String fileName : inputIndex) {
		                File file = inputIndex.getFile(fileName).getAbsoluteFile();
		                if (!file.exists()) {
		                	ci.getLogger().info("Found missing array index reference to: " +
                                    file + ". Triggering upstream execution on missing outputs.");
		                	this.currentStates.put(ci, CurrentState.NO);
		                	return;
		                }
		            }
				} catch (IOException e) {
					addError(new DynamicError("Error reading array index produced by: " + e, ci));
				}
		    }
		}
	}

    /**
     * Read the state file from the execution directory. This modifies the
     * initial states of component instances. Add component instances whose
     * configuration has changed (and their dependents) to changeSet.
     */
    private void readStateFile(Set<ComponentInstance> changeSet) {

        ArrayList<ComponentInstance> primaryChangeSet = new ArrayList<ComponentInstance>();
        for (StateRecord record : this.stateFile) {
            final ComponentInstance ci = this.network.getComponentInstance(record.instanceName);
            if (ci == null)
                continue;

            if (record.activeState == ActiveState.SUSPENDED) {
                this.activeStates.put(ci, ActiveState.SUSPENDED);
            }

            String execute = (String)ci.getInstanceAnnotation(Annotation.EXECUTE);
            boolean always = (execute != null && execute.equals(ExecuteAnnotation.Execute.always.toString()));
            boolean once = (execute != null && execute.equals(ExecuteAnnotation.Execute.once.toString()));

            boolean confChanged = !ci.getConfigurationDigest(network).equals(record.digest);

            if (confChanged && !once) {
                ci.getLogger().info("Configuration has changed", Logger.tagComponentConfigurationChanged());
            }

            if ((confChanged && !once) || always) {
                primaryChangeSet.add(ci);
            } else if (record.currentState.isCurrent()) {
                this.currentStates.put(ci, CurrentState.YES);
            }
        }

        for (ComponentInstance changed : primaryChangeSet) {
            changeSet.add(changed);
            if (changed.isDisabled() && this.currentStates.get(changed).equals(CurrentState.NO)) {
                changeSet.addAll(changed.getReachableComponents(null));
            } else
                changeSet.addAll(changed.getReachableComponents(this));
        }

//        Set<ComponentInstance> allOnceSet = new HashSet<ComponentInstance>();
//        for (ComponentInstance ci : changeSet) {
//            if (this.allOnce(ci)) allOnceSet.add(ci);
//        }
//
//        changeSet.removeAll(allOnceSet);
    }

    /**
       For each ci, backward inference to determine whether
       all inports are reached by "once" component instance outports.
       Those ci with inferred all "once" inports are temporarily set to once.
       AndurilScript coded value "once" remains.
    */
    private void backwardInferenceOnceAnnotation() {
        for (ComponentInstance ci : network) {
            boolean isAllOnce = allOnce(ci);
            if (isAllOnce) {
                ci.setInstanceAnnotation(Annotation.EXECUTE, ExecuteAnnotation.Execute.once.toString());
            }
        }
    }

    private boolean allOnce(ComponentInstance ci)
    {
    	String execute = (String)ci.getInstanceAnnotation(Annotation.EXECUTE);
        boolean once = (execute != null && execute.equals(ExecuteAnnotation.Execute.once.toString()));

        if (once && !force.contains(ci)) return true;
        if (ci.getInPortConnections().isEmpty() && !force.contains(ci)) return once;

        boolean isAllOnce = true;
        for (PortConnection ip : ci.getInPortConnections()) {
            isAllOnce = isAllOnce && allOnce(ip.getFrom());
        }
        return isAllOnce;
    }

    /**
     * Compute pre-counts and post-counts and initialize the READY queue.
     */
    private void computeCounts() {
        for (ComponentInstance ci : network) {
            this.preCounts.put(ci, 0);

            for (OutPort port : ci.getComponent().getOutPorts()) {
            	this.postCounts.put(new ComponentInstancePort<OutPort>(ci, port), 0);
            }
            
            if (markedForDiskOptimization(ci)) {
                for (PortConnection conn : ci.getOutPortConnections()) {
                    for (PortConnection outConn : conn.getTo().getOutPortConnections()) {
                        /*
                         * This component produces input for an array so the
                         * output might be referred to later on via any number
                         * of other arrays. Therefore the outport's postcount
                         * needs to be incremented whenever someone refers to it
                         * later on.
                         */
                        if (outConn.getTo().getPortIsArray(outConn.getToPort()).isPotentiallyArray()) {
                            ComponentInstancePort<OutPort> outPort = new ComponentInstancePort<OutPort>(ci,
                                    conn.getFromPort());
                            File fileToTrack = ci.getOutputFile(conn.getFromPort(), this.execDir.getRoot())
                                    .getAbsoluteFile();
                            this.execState.trackedArrayInputFiles.put(fileToTrack, outPort);
                            break;
                        }
                    }
                }
            }
        }

        HashSet<ComponentInstance> neededFromPrevious = new HashSet<>();
        ListIterator<ComponentInstance> iter = network.getMembersDFS(true);
        while ( iter.hasNext() ) {
        	ComponentInstance ci = iter.next();
            if (isPending(ci) || this.execState.failed.contains(ci)) {
                for (ComponentInstance succ : ci.getOutComponents()) {
                    if (this.preCounts.get(succ) != null) {
                        int old = this.preCounts.get(succ);
                        this.preCounts.put(succ, old + 1);
                    }
                }

                for (PortConnection conn : ci.getInPortConnections()) {
                    incPostCount(conn.getFromCIP());

                    File file = conn.getFrom().getOutputFile(conn.getFromPort(), this.execDir.getRoot())
                            .getAbsoluteFile();

                    /*
                     * Check if we can statically deduce that a file is indirectly needed by
                     * this component. It might be that all array components have already
                     * been executed - thus nobody would increment the post-count
                     * of the file before the component that needs the file is run.
                     * 
                     * Because the count is incremented here, it might need to be decremented
                     * twice when the target component completes. This is flagged in
                     * a list.
                     * 
                     * Incrementing the count here also prevents deleting the file in the
                     * following code segment where unneeded files from the dynamic
                     * runs are cleaned.
                     */
                    if (this.execState.trackedArrayInputFiles.containsKey(file)) {
                    	ComponentInstancePort<OutPort> cip = this.execState.trackedArrayInputFiles.get(file);
                    	
                    	// Do not increment again what we just did - it's a direct connection
                        if (cip.equals(conn.getFromCIP()))
                        	continue;

                        neededFromPrevious.add(cip.getComponentInstance());
                		incPostCount(cip);
                		if (!this.staticIndirectDependencies.containsKey(ci))
                			this.staticIndirectDependencies.put(ci, new HashSet<ComponentInstancePort<OutPort> >());
                		this.staticIndirectDependencies.get(ci).add(cip);
                    }
                }
            }
        }

        /*
         * Clean up files that aren't needed to complete this run that are
         * marked for disk optimization.
         */
        if (this.finalStage) {
            for (ComponentInstance ci: this.execState.finished) {
                if (!neededFromPrevious.contains(ci)) {
                    optimizeSpace(ci, false);
                }
            }
        }

        for (ComponentInstance ci : network) {
            if (isPending(ci) && getPreCount(ci) == 0) {
                this.ready.add(ci);
            }
        }
    }

    /**
     * Check pre-execution invariants.
     * 
     * @throws IllegalArgumentException
     *             If invariant are not satisfied.
     */
    private void checkPreExecutionInvariants() {
        // Create a local set for efficient membership queries
        Set<ComponentInstance> readySet = new HashSet<ComponentInstance>(this.ready);
        for (ComponentInstance ci : readySet) {
            int count = getPreCount(ci);
            if (count != 0) {
                throw new IllegalArgumentException("Component is in READY but pre-count is " + count);
            }
            if (getCurrentState(ci).isCurrent()) {
                throw new IllegalArgumentException("Component is in READY but current-state is " + getCurrentState(ci));
            }
            if (!getActiveState(ci).isActive()) {
                throw new IllegalArgumentException("Component is in READY but active-state is " + getActiveState(ci));
            }
        }

        for (ComponentInstance ci : network) {
            final int count = getPreCount(ci);
            final boolean current = getCurrentState(ci).isCurrent();
            final boolean active = getActiveState(ci).isActive();

            if (!readySet.contains(ci) && !current && active && count == 0) {
                if (!getActiveState(ci).isActive()) {
                    throw new IllegalArgumentException("Component is not in READY although it is ready for execution "
                            + ci.getName());
                }
            }
            if (count < 0) {
                throw new IllegalArgumentException("Pre-count is negative: " + ci.getName());
            }
            if (count > ci.getInComponents().size()) {
                String msg = String.format("Pre-count (%d) is larger than number of predecessors (%d): %s", count, ci
                        .getInComponents().size(), ci.getName());
                throw new IllegalArgumentException(msg);
            }

            String execute = (String)ci.getInstanceAnnotation(Annotation.EXECUTE);
            boolean executeAlways = 
                (execute != null && execute.equals(ExecuteAnnotation.Execute.always.toString()));
            if (active && current && executeAlways) {
                throw new IllegalArgumentException(
                        "Component is marked @execute=always but it is not marked for execution: " + ci.getName());
            }

            if (ci.isDisabled() && active) {
                throw new IllegalArgumentException("Component is marked disabled but active-state is YES: "
                        + ci.getName());
            }
        }
    }

    /**
     * Return the network under execution.
     */
    public Network getNetwork() {
        return network;
    }

    /**
     * Return the execution directory that is
     * used to store component outputs and the state file.
     */
    public ExecutionDirectory getExecutionDirectory() {
        return execDir;
    }

    // Errors //////////////////////////////////////////////////////////

    /**
     * Return the dynamic errors that occurred during network
     * execution.
     */
    public List<DynamicError> getErrors() {
        return errors;
    }

    /**
     * Add a dynamic error that occurred during network execution.
     * This method is safe to call from internal components.
     */
    public void addError(DynamicError error) {
        errors.add(error);
        if (error.getComponentInstance() != null) {
            setFailed(error.getComponentInstance());
        }
        error.report(this.parentLogger);
    }

    /**
     * Return true if the network execution had errors.
     */
    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    /**
     * Return true if the execution of given component instance
     * resulted in an error.
     */
    public boolean hasErrors(ComponentInstance ci) {
        return failed.contains(ci);
    }

    public List<ComponentInstance> getFailed() {
        return this.failed;
    }

    /**
     * Return true if this engine or any previous stage engine
     * had errors.
     */
    public boolean hasOverallErrors() {
        return !this.execState.failed.isEmpty();
    }

    // States //////////////////////////////////////////////////////////

    /**
     * Return the runtime state of given component instance that
     * indicates whether the CI is up-to-date.
     * @throws NullPointerException if ci is null
     */
    public CurrentState getCurrentState(ComponentInstance ci) {
        if (ci == null) {
            throw new NullPointerException("Component instance is null.");
        }
        CurrentState state = this.currentStates.get(ci);
        if (state == null) {
            throw new IllegalArgumentException("CurrentState: Component instance " + ci.getName() + " not found");
        }
        return state;
    }

    /**
     * Return the runtime state of given component instance that
     * indicates whether the CI is active.
     * @throws NullPointerException if ci is null
     */
    public ActiveState getActiveState(ComponentInstance ci) {
        if (ci == null) {
            throw new NullPointerException("Component instance is null.");
        }
        ActiveState state = this.activeStates.get(ci);
        if (state == null) {
            throw new IllegalArgumentException("ActiveState: Component instance " + ci.getName() + " not found");
        }
        return state;
    }

    /**
     * Set the active state of given component instance. This should
     * only be used for component instances that have not yet been
     * executed.
     * @param ci The component instance
     * @param state The new state
     */
    public void setState(ComponentInstance ci, ActiveState state) {
        if (ci == null) {
            throw new IllegalArgumentException("Component instance is null.");
        }
        if (state == null) {
            throw new IllegalArgumentException("State is null.");
        }

        synchronized (ComponentExecutor.getInstance()) {
            boolean isCurrent = getCurrentState(ci).isCurrent();
            boolean isActive = getActiveState(ci).isActive();
            boolean activated = !isCurrent && !isActive && state.isActive();
            boolean inactivated = !isCurrent && isActive && !state.isActive();
            if (activated || inactivated) {
                for (ComponentInstance succ : ci.getOutComponents()) {
                    int count = this.preCounts.get(succ);
                    count = count + (activated ? 1 : -1);
                    this.preCounts.put(succ, count);
                    if (count == 0 && isPending(succ)) {
                        ready.add(succ);
                    } else if (count > 0 && ready.contains(succ)) {
                        ready.remove(succ);
                    }
                }
                for (PortConnection conn : ci.getInPortConnections()) {
                    int count = this.postCounts.get(conn.getFromCIP());
                    count = count + (activated ? 1 : -1);
                    this.postCounts.put(conn.getFromCIP(), count);

                }
            }

            if (activated) {
                int count = this.preCounts.get(ci);
                if (count == 0 && isPending(ci) && !this.ready.contains(ci)) {
                    this.ready.add(ci);
                }
            }

            this.activeStates.put(ci, state);
            if (!state.isActive() && ready.contains(ci)) {
                ready.remove(ci);
            }
        }
    }

    /**
     * Return true if given component instance is awaiting
     * execution in this run. This occurs when current-state
     * is NO and active-state is YES.
     */
    public boolean isPending(ComponentInstance ci) {
        return !getCurrentState(ci).isCurrent() && getActiveState(ci).isActive();
    }

    private void setState(ComponentInstance ci, CurrentState state) {
        if (ci == null) {
            throw new IllegalArgumentException("Component instance is null");
        }
        if (state == null) {
            throw new IllegalArgumentException("State is null");
        }
        this.currentStates.put(ci, state);
    }

    private int getPreCount(ComponentInstance ci) {
        Integer count = this.preCounts.get(ci);
        return (count == null) ? -1 : count;
    }

    private int getPostCount(ComponentInstancePort<OutPort> cip) {
        Integer count = this.postCounts.get(cip);
        return (count == null) ? -1 : count;
    }

    private int decPreCount(ComponentInstance ci) {
        synchronized (ComponentExecutor.getInstance()) {
            ci.getLogger().debug(
                    String.format("%s: pre-count decreased to %d", ci.getName(), getPreCount(ci) - 1));
            int count = getPreCount(ci);
            if (count == 0) {
                throw new IllegalArgumentException("Pre-count of component instance " + ci.getName() + " is already 0");
            }
            return decPreCountImpl(ci, count);
        }
    }

    private int decPreCountImpl(ComponentInstance ci, int count) {
        this.preCounts.put(ci, count - 1);
        return count - 1;
    }

    private int decPostCount(ComponentInstancePort<OutPort> cip) {
        synchronized (ComponentExecutor.getInstance()) {
            cip.getComponentInstance().getLogger().debug(
                    String.format("%s: post-count decreased to %d", cip, getPostCount(cip) - 1));
            int count = getPostCount(cip);
            if (count == 0) {
                throw new IllegalArgumentException("Post-count of " + cip + " is already 0");
            }
            return decPostCountImpl(cip, count);
        }
    }

    private int decPostCountImpl(ComponentInstancePort<OutPort> cip, int count) {
        this.postCounts.put(cip, count - 1);
        return count - 1;
    }

    public void printState() {
        if (!Logger.isVerbose()) {
            return;
        }

        for (ComponentInstance ci : network) {
            String s = String.format("current=%s, active=%s, precount=%d", getCurrentState(ci),
                    getActiveState(ci), getPreCount(ci));
            ci.getLogger().debug(s);
        }
    }

    private void setFailed(ComponentInstance ci) {
        if (ci != null) {
            if (!this.failed.contains(ci)) {
                this.failed.add(ci);
            }
            this.execState.failed.add(ci);
        }
    }

    // Execution ///////////////////////////////////////////////////////

    /**
     * Return the list of component instances that
     * are ready for execution.
     */
    public Queue<ComponentInstance> getReady() {
        return ready;
    }

    /**
     * Return the list of finished component instances.
     * Most recently finished are first.
     */
    public Set<ComponentInstance> getFinished() {
        return this.execState.finished;
    }

    /**
     * Return the currently active component instances.
     */
    public List<ComponentInstance> getActive() {
        return this.active;
    }

    private String formatReady() {
        if (ready.isEmpty()) return "(empty)";
        StringBuilder sb = new StringBuilder(512);

        int i = 1;
        int n = ready.size();
        int threshold = 10;
        if (n - threshold < 5)
            threshold = n;
        
        Iterator<ComponentInstance> it = ready.iterator();
        while (it.hasNext() && i <= threshold) {
            if (i > 1) {
                sb.append(' ');
            }
            sb.append(it.next().getName());
            i += 1;
        }
         
        if (i+1 <= n) 
            sb.append("(and " + (n-i) + " more).");
        
        return sb.toString();
    }
    
    /**
     * Prepare a component instance for execution. Return true if execution can
     * proceed, false if there was an error and the job must be aborted.
     */
    private boolean preExecution(ComponentInstance ci) {
        if (getCurrentState(ci) != CurrentState.NO) {
            addError(new DynamicError("Component is in READY queue but current-state is " + getCurrentState(ci), ci));
        }
        if (getActiveState(ci) != ActiveState.YES) {
            addError(new DynamicError("Component is in READY queue but active-state is " + getActiveState(ci), ci));
        }

        execDir.clearComponentDirectory(ci);

        if (hasErrors(ci)) {
            setFailed(ci);
            return false;
        }

        try {
            execDir.writeCommandFile(ci);
        } catch (IOException e) {
            addError(new DynamicError("I/O error: ", ci, e));
        }

        if (hasErrors(ci)) {
            setFailed(ci);
            return false;
        }

        return true;
    }

    /**
     * After component instance has been executed, check that output files are
     * present and whether there are errors. Print log messages from the log
     * file of the component instance.
     * 
     * @return True if execution was successful, false if there were errors.
     */
    private boolean postExecution(ComponentInstance ci) {
        if (ready.contains(ci)) {
            throw new RuntimeException("READY queue contains a component that was executed: " + ci.getName());
        }

        for (String msg : execDir.readComponentInstanceErrors(ci)) {
            addError(new DynamicError(msg, ci));
        }

        if (!hasErrors(ci)) {
            for (OutPort port : execDir.missingOutputFiles(ci)) {
                String msg = String.format("Output file is missing for port %s and execDir %s", port.getName(), execDir
                        .getRoot().getAbsolutePath());
                addError(new DynamicError(msg, ci));
            }
        }

        for (String logMessage : execDir.readComponentInstanceLog(ci)) {
            ci.getLogger().log(logMessage);
        }

        return !hasErrors(ci);
    }

    /**
     * Return true if postcounts should not be incremented and decremented for
     * the file except for direct connections between components where it's done
     * always.
     * 
     * @param file
     *            The file that should be inspected
     * @return True if file should be ignored during tracking and checking for
     *         missing outputs.
     */
    private boolean shouldNotTrack(File file) {
        return !this.execState.trackedArrayInputFiles.containsKey(file);
    }

    /**
     * Return true if any port of given component instance is marked for disk
     * space optimization. Return false if this component instance does not use
     * the optimization feature.
     */
    private boolean markedForDiskOptimization(ComponentInstance ci) {
        /* INPUT components must not delete their output port */
        if (ci instanceof InputComponentInstance)
            return false;
        boolean keep = Annotation.convertBoolean(ci.getInstanceAnnotation(Annotation.KEEP));
        return !keep;
    }

    /**
     * Remove output files of given component instance if they are no longer
     * needed. This is only done for component instances that are marked for
     * disk space optimization. Each output port is checked for eligibility for
     * removal. The output file is removed if post-count of the port is zero or
     * force is true.
     * 
     * Output files aren't removed during dynamic runs because they might
     * be needed in the future.
     * 
     * @param ci
     *            The component instance
     * @param force
     *            If true, output files are removed even if post-count of the
     *            port is non-zero, i.e., the port could potentially be used.
     */
    private void optimizeSpace(ComponentInstance ci, boolean force) {
        if (!this.finalStage)
            return;
        if (!markedForDiskOptimization(ci))
            return;

        for (Connection conn : ci.getOutPortConnections()) {
            if (failed.contains(conn.getTo())) return;
        }

        for (OutPort port : ci.getComponent().getOutPorts()) {
            ComponentInstancePort<OutPort> cip = new ComponentInstancePort<OutPort>(ci, port);
            if (force || getPostCount(cip) == 0) {
                try {
                    File output = ci.getOutputFile(port, this.execDir.getRoot());
                    if (output.exists()) {
                    	ci.getLogger().info("Removing unnecessary output: " + output.getAbsolutePath());
                        Tools.removeFile(output);
                    }
                } catch (IOException e) {
                    addError(new DynamicError("Failed to remove output file: " + e, ci));
                    break;
                }
            }
        }
    }

    /**
     * Notify the engine that the given component instance
     * has finished execution. The engine updates the state
     * of the component instance and may add new items to
     * the READY queue.
     */
    public void notifyComponentFinished(ComponentInstance ci) {
        if (ci == null) {
            throw new NullPointerException("Component instance is null.");
        }

        boolean success = postExecution(ci);

        if (success) {
            /* Optimize disk space for out-ports that do not have connections */
            optimizeSpace(ci, false);

            setState(ci, CurrentState.YES);
            this.execState.finished.add(ci);
            for (ComponentInstance succ : ci.getOutComponents()) {
                int count = decPreCount(succ);
                if (count == 0 && getCurrentState(succ) == CurrentState.NO && getActiveState(succ) == ActiveState.YES) {
                    this.ready.add(succ);
                }
            }
        } else {
            setFailed(ci);
            /* Optimize disk space without checking post-count */
            optimizeSpace(ci, false);
        }

        relaxDependencies(ci);

        try {
            this.stateFile.write(ci, this);
            this.stateFile.commit();
        } catch (IOException e) {
            addError(new DynamicError("Cannot write state file", e));
        }

        final String status = success ? "success" : "failure";
        final String msg = String.format("Component finished with %s", status);
        ci.getLogger().info(msg, Logger.tagComponentFinished(success), Logger.tagTimestamp());

        String readyMsg = "Current ready queue: " + formatReady();
        ci.getLogger().info(readyMsg, Logger.tagReadyQueue(this.ready.size()));
    }

    /**
     * Ensure components providing input for this one are able to free their
     * resources.
     * 
     * @param ci
     *            Component instance that doesn't depend on others anymore.
     */
    private void relaxDependencies(ComponentInstance ci) {
        for (PortConnection conn : ci.getOutPortConnections()) {
            if (!isPending(conn.getTo()))
                continue;

            if (ci.getPortIsArray(conn.getFromPort()).isTrue()) {
                /*
                 * Increment post-count for the originating component for each
                 * outgoing array connection that includes a tracked file.
                 */
                try {
                    IndexFile inputIndex = IndexFile.read(ci.getOutputArrayIndex(conn.getFromPort(),
                            this.execDir.getRoot()).getAbsoluteFile());
                    for (String fileName : inputIndex) {
                        File file = inputIndex.getFile(fileName).getAbsoluteFile();
                        if (shouldNotTrack(file))
                            continue;
                        ComponentInstancePort<OutPort> outPort = this.execState.trackedArrayInputFiles.get(file);

                        incPostCount(outPort);
                        ComponentInstance toComp = conn.getTo();
                		if (!this.dynamicIndirectDependencies.containsKey(toComp))
                			this.dynamicIndirectDependencies.put(toComp, new HashSet<ComponentInstancePort<OutPort> >());
                		this.dynamicIndirectDependencies.get(toComp).add(outPort);
                    }
                } catch (IOException e) {
                    addError(new DynamicError("Error reading array index produced by: " + e, ci));
                    return;
                }
            } else {
                /*
                 * Check if this component is producing output by directly
                 * referencing a file in an array.
                 */
                File file = ci.getOutputFile(conn.getFromPort(), this.execDir.getRoot()).getAbsoluteFile();
                if (shouldNotTrack(file))
                    continue;
                ComponentInstancePort<OutPort> outPort = this.execState.trackedArrayInputFiles.get(file);
                if (!this.execState.trackedArrayInputFiles.get(file).getComponentInstance().equals(ci)) {
                    incPostCount(outPort);
                    ComponentInstance toComp = conn.getTo();
            		if (!this.dynamicIndirectDependencies.containsKey(toComp))
            			this.dynamicIndirectDependencies.put(toComp, new HashSet<ComponentInstancePort<OutPort> >());
            		this.dynamicIndirectDependencies.get(toComp).add(outPort);
                }
            }
        }

        /*
         * Post-counts of predecessors are decreased regardless of success
         * status
         */
        for (PortConnection conn : ci.getInPortConnections()) {
            /*
             * In case this component has an array input, decrement also
             * post-counts of indirect predecessors that yielded files for the
             * array.
             */
            if (ci.getPortIsArray(conn.getToPort()).isTrue()) {
                try {
                    IndexFile inputIndex = IndexFile.read(conn.getTo()
                            .getInputArrayIndex(conn.getToPort(), this.execDir.getRoot()).getAbsoluteFile());
                    for (String fileName : inputIndex) {
                        File file = inputIndex.getFile(fileName).getAbsoluteFile();
                        if (shouldNotTrack(file))
                            continue;
                        ComponentInstancePort<OutPort> outPort = this.execState.trackedArrayInputFiles.get(file);

                    	if ( this.dynamicIndirectDependencies.containsKey(ci)
                        		&& this.dynamicIndirectDependencies.get(ci).contains(outPort)) {
                    		this.dynamicIndirectDependencies.get(ci).remove(outPort);
                    		int count = decPostCount(outPort);
                    		
                            if (count == 0 && getCurrentState(outPort.getComponentInstance()).isCurrent()) {
                                optimizeSpace(outPort.getComponentInstance(), false);
                            }
                    	}
                    }
                } catch (IOException e) {
                    addError(new DynamicError("Error reading array index produced by: " + e, conn.getFrom()));
                    return;
                }
            } else {
                /*
                 * Check if this component is consuming a file that comes from
                 * an array.
                 */
                File file = ci.getInputFile(conn.getToPort(), this.execDir.getRoot()).getAbsoluteFile();
                ComponentInstancePort<OutPort> inPort = this.execState.trackedArrayInputFiles.get(file);
                if (inPort != null && !inPort.getComponentInstance().equals(conn.getFrom()) && !shouldNotTrack(file)) {
                	int count = getPostCount(inPort);
                	if ( this.staticIndirectDependencies.containsKey(ci)
                		&& this.staticIndirectDependencies.get(ci).contains(inPort)) {
                		this.staticIndirectDependencies.get(ci).remove(inPort);
            			count = decPostCount(inPort);
                	}
                	if ( this.dynamicIndirectDependencies.containsKey(ci)
                		&& this.dynamicIndirectDependencies.get(ci).contains(inPort)) {
                		this.dynamicIndirectDependencies.get(ci).remove(inPort);
                		count = decPostCount(inPort);
                    }

                    if (count == 0 && getCurrentState(inPort.getComponentInstance()).isCurrent()) {
                        optimizeSpace(inPort.getComponentInstance(), false);
                    }
                }
            }
            int count = decPostCount(conn.getFromCIP());
            if (count == 0 && getCurrentState(conn.getFrom()).isCurrent()) {
                optimizeSpace(conn.getFrom(), false);
            }

        }
    }

    private int incPostCount(ComponentInstancePort<OutPort> outPort) {
        int old = this.postCounts.get(outPort);
        this.postCounts.put(outPort, old + 1);
        return old;
    }

    private void runNetwork() throws Exception {
        ComponentExecutor coordinator = ComponentExecutor.getInstance();
        while (true) {
            /* May block if the queue is empty and some worker is working. */
            ComponentInstance job = coordinator.getJob(this);
            if (job == null)
                break;

            boolean ok;
            try {
                ok = preExecution(job);
            } catch (Exception e) {
                coordinator.abortJob(this, job);
                throw e;
            }

            if (!ok) {
                coordinator.abortJob(this, job);
                continue;
            }

            /* May block if all threads are in use. */
            ExecutorWorker worker = coordinator.getWorker(this, job);

            String msg = String.format("Executing %s (%s)", job.getName(), job.getComponent().getName());
            job.getLogger().info(msg, Logger.tagSource(job.getSourceLocation()), Logger.tagComponentStarted(), Logger.tagTimestamp());

            worker.start();
        }

        if (coordinator.hasPendingJobs(this)) {
            addError(new DynamicError("Post-execute check error: there are pending jobs"));
        }
    }

    /**
     * Check post-execution invariants. Add errors to network if invariants are
     * not satisfied. It is assumed that execution did not result in errors.
     */
    private void checkPostExecutionInvariants() {
        if (!ready.isEmpty()) {
            addError(new DynamicError("Post-execute check error: READY is not empty"));
        }

        for (ComponentInstance ci : network) {
            if (!getActiveState(ci).isActive())
                continue;

            CurrentState state = getCurrentState(ci);
            if (!state.isCurrent()) {
                addError(new DynamicError("Post-execute check error: component has state " + state.getID(), ci));
            }
            int count = getPreCount(ci);
            if (state.isCurrent() && count > 0) {
                addError(new DynamicError("Post-execute check error: component is finished but pre-count is " + count,
                        ci));
            }
            if (this.active.contains(ci)) {
                addError(new DynamicError("Post-execute check error: component is finished but it is marked active", ci));
            }
        }
    }

    private void executeImpl() {
        this.parentLogger.debug("Component instance states at the beginning of execution:");
        printState();

        checkPreExecutionInvariants();
        if (hasErrors())
            return;
        if (!network.getFinalized()) {
            addError(new DynamicError("The network has not been finalized; can't execute"));
            return;
        }
        execDir.initOutputDirectory();
        execDir.deleteLogFile(null); // Delete global log
        
        try {
            // We do not really care if an exception is thrown because registering
            // the component instances here is just a convenience to the user
            // and should be a no-op most of the time.
            for (ComponentInstance ci : network) {
                    this.stateFile.register(ci, this);
            }
            this.stateFile.commit();
        } catch (IOException e) {
        }

        if (getReady().isEmpty()) {
            if (this.finalStage) {
                this.parentLogger.info("Nothing to execute", Logger.tagReadyQueue(this.ready.size()));
            }
            return;
        }

        String readyMsg = "Current ready queue: " + formatReady();
        this.parentLogger.info(readyMsg, Logger.tagReadyQueue(this.ready.size()));

        try {
            runNetwork();
        } catch (Exception e) {
            addError(new DynamicError("Uncaught exception", e));
        }

        this.parentLogger.debug("Component instance states at the end of execution:");
        printState();

        if (!hasErrors())
            checkPostExecutionInvariants();

        if (hasErrors()) {
            StringBuilder sb = new StringBuilder("Done. The following components had errors: ");
            Formatter formatter = null;
            for (ComponentInstance ci : failed) {
                formatter = new Formatter();
                final String location;
                if (ci.getSourceLocation() == null) {
                    location = "";
                } else {
                    location = ci.getSourceLocation().toString();
                }
                sb.append(formatter.format("\n %-32s %-10s", ci.getName() + " (" + ci.getComponent().getName() + ")",
                        location + " ").toString());
            }
            this.parentLogger.error(sb.toString());
        } else {
            if (this.finalStage){
                this.parentLogger.info("Done. No errors occurred.");
            }
        }
    }

    /**
     * Execute the component network and write a state file to execution
     * directory. If errors occur during execution, they can be accessed
     * using {@link #getErrors getErrors}.
     */
    public void execute() {
        try {
            executeImpl();

            try {
                if (this.finalStage) {
                    this.stateFile.clean(this.network);
                }
                this.stateFile.writeAll(this.network, this);
                if (this.commitFinalState) { // The state is not committed if e.g. --retain-network is set, so that you can run your pipeline with parts of it commented out, and not lose your already executed, but commented out components.
                    this.stateFile.writeAll(this.network, this);
                    this.stateFile.commit();
                } else {
                }
            } catch (IOException e) {
                addError(new DynamicError("Cannot write state file", e));
            }
        } finally {
            try {
                this.stateFile.close();
            } catch (IOException e) {
                addError(new DynamicError("Cannot close state file", e));
            }
        }
    }
}
