package org.anduril.core.engine;

import org.anduril.component.CommandFile;
import org.anduril.core.Version;
import org.anduril.core.commandline.BaseCommand;
import org.anduril.core.network.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Write a command file for a component. The
 * command file contains paths of input and output files,
 * as well as values for all parameters. Also, some metadata
 * fields are included.
 */
public class CommandFileWriter {
    public static final String OUTPUT_ERRORS = "_errors";
    public static final String OUTPUT_LOG = "_log";
    public static final String TEMP_DIRECTORY = "_tempdir";
    
    private static final String NEWLINE = "\n";
    
    private ComponentInstance ci;
    private File output;
    private ExecutionDirectory execDir;
    private Writer writer;
    private Map<String,File> inputOverride;
    private Map<String,File> outputOverride;
    private Map<String,String> paramOverride;
    
    /**
     * Initialize.
     * @param ci The component instance for which the command file
     * 	is written. 
     * @param output The target path of the command file.
     * @param execDir The execution directory instance.
     */
    public CommandFileWriter(ComponentInstance ci, File output, ExecutionDirectory execDir) {
        this.ci = ci;
        this.output = output;
        this.execDir = execDir;
        this.inputOverride = new HashMap<String,File>(); 
        this.outputOverride = new HashMap<String,File>();
        this.paramOverride = new HashMap<String,String>();
    }
    
    private void writeMetadata() throws IOException {

    	String ciName = ci.getName();
        String s = String.format("metadata.%s=%s", CommandFile.METADATA_INSTANCE_NAME, ciName);
        writer.write(s+NEWLINE);

        SourceLocation sourceLocation = ci.getSourceLocation();
        String slFilename = "";
        String slLine = "";
        if (sourceLocation != null) {
            File slFile = sourceLocation.getFile();
            slFilename = (slFile == null ? "" : slFile.getAbsolutePath());
            slLine = sourceLocation.getLine().toString();
        }
        s = String.format("metadata.%s=%s:%s", CommandFile.METADATA_SOURCE_LOCATION, slFilename, slLine);
        writer.write(s+NEWLINE);

        s = String.format("metadata.%s=Anduril %s", CommandFile.METADATA_ENGINE, Version.ANDURIL_VERSION);
        writer.write(s+NEWLINE);

        Component comp = ci.getComponent();
        s = String.format("metadata.%s=%s", CommandFile.METADATA_COMPONENT_NAME,
                comp.getName());
        writer.write(s+NEWLINE);

        File compFile = comp.getMainDirectory();
        s = String.format("metadata.%s=%s", CommandFile.METADATA_COMPONENT_PATH,
                compFile == null ? "" : compFile.getAbsolutePath());
        writer.write(s+NEWLINE);

        String andurilHome = System.getenv(BaseCommand.HOME_ENVIRONMENT);
        s = String.format("metadata.%s=%s", CommandFile.METADATA_ANDURIL_HOME, andurilHome);
        writer.write(s+NEWLINE);

        Bundle bundle = comp.getBundle();
        s = String.format("metadata.%s=%s", CommandFile.METADATA_BUNDLE_NAME,
                bundle == null ? "" : bundle.getName());
        writer.write(s+NEWLINE);
        s = String.format("metadata.%s=%s", CommandFile.METADATA_BUNDLE_PATH,
                bundle == null ? "" : bundle.getDirectory().getAbsolutePath());
        writer.write(s+NEWLINE);

        for (Map.Entry<String, String> entry: ci.getCustomAnnotation().entrySet()) {
            s = String.format("metadata.custom_%s=%s", entry.getKey(), entry.getValue());
            writer.write(s+NEWLINE);

        }
    }
    
    private void writeInput(InPort port, File path) throws IOException {
        if (inputOverride.containsKey(port.getName())) {
            path = inputOverride.get(port.getName());
        }
        if (path == null) path = new File("");
        
        if (path.getName().startsWith("_dummy")) {
            throw new IOException(String.format("Internal error: tried to use a _dummy file as input: %s.%s",
            		this.ci.getName(), port.getName()));
        }
        
        String s = String.format("input.%s=%s", port.getName(), path);
        writer.write(s+NEWLINE);
    }
    
    private void writeInputArrayIndex(InPort port, File indexPath) throws IOException{
        // TODO: input override
        if (indexPath == null) return;
        String s = String.format("input.%s%s=%s",
                Port.ARRAY_INDEX_PORT_PREFIX, port.getName(), indexPath);
        writer.write(s+NEWLINE);
    }
    
    private void writeOutput(OutPort port, File path) throws IOException {
        if (outputOverride.containsKey(port.getName())) {
            path = outputOverride.get(port.getName());
        }
        if (path == null) path = new File("");
        
        String s = String.format("output.%s=%s", port.getName(), path);
        writer.write(s+NEWLINE);
    }
    
    private void writeOutputArrayIndex(OutPort port, File indexPath) throws IOException {
        // TODO: output override
        if (indexPath == null) return;
        String s = String.format("output.%s%s=%s",
                Port.ARRAY_INDEX_PORT_PREFIX, port.getName(), indexPath);
        writer.write(s+NEWLINE);
    }
    
    private void writeParameter(Parameter param, String value) throws IOException {
        if (paramOverride.containsKey(param.getName())) {
            value = paramOverride.get(param.getName());
        }
        if (value == null) value = "";
        
        // Properties, by documentation, always adds a # comment line with date etc.
        // which we don't want.
        // So let it write to a string and then skip the first line.
        Properties prop = new Properties();
        prop.setProperty(String.format("parameter.%s", param.getName()), value);
        Writer stringOut = new StringWriter();
        prop.store(stringOut, null);
        String string = stringOut.toString();
        String sep = System.getProperty("line.separator");
        writer.write(string.substring(string.indexOf(sep) + sep.length()));
    }
    
    /**
     * Override an input file path for the given port. Normally,
     * the component instance determines the paths of input and
     * output files. This method can be used to customize the
     * path.
     * @param port The input port.
     * @param input The path that represents the input port.
     * 	This is where the component reads the input.
     */
    public void setInputOverride(InPort port, File input) {
        inputOverride.put(port.getName(), input);
    }

    /**
     * Override an output file path for the given port. Normally,
     * the component instance determines the paths of input and
     * output files. This method can be used to customize the
     * path.
     * @param port The output port.
     * @param input The path that represents the output port.
     * 	This is where the component writes the output.
     */
    public void setOutputOverride(OutPort port, File input) {
        outputOverride.put(port.getName(), input);
    }
    
    /**
     * Override the value for the given parameter.
     * @param param The parameter.
     * @param value The new value for the parameter.
     */
    public void setParameterOverride(Parameter param, String value) {
        paramOverride.put(param.getName(), value);
    }
    
    /**
     * Write the command file.
     * @throws IOException if an I/O error occurred.
     */
    public void write() throws IOException {
        this.writer = new FileWriter(this.output);
        final Engine engine = execDir.getEngine();
        final File executionRoot = this.execDir.getRoot();

        try {
            writeMetadata();

            for (InPort port: ci.getComponent().getInPorts(true)) {
                File path = ci.getInputFile(port, executionRoot);

                PortConnection conn = ci.getInConnection(port);
                final boolean unconnected = conn == null || !engine.getActiveState(conn.getFrom()).isActive();
                if (unconnected) path = null;

                writeInput(port, path);
                
                if (ci.getPortIsArray(port).isPotentiallyArray()) {
                    File indexPath = ci.getInputArrayIndex(port, executionRoot);
                    if (unconnected) indexPath = null;
                    writeInputArrayIndex(port, indexPath);
                }
            }

            for (OutPort port: ci.getComponent().getOutPorts(true)) {
                File path = ci.getOutputFile(port, executionRoot);
                writeOutput(port, path);
                
                if (ci.getPortIsArray(port).isPotentiallyArray()) {
                    File indexPath = ci.getOutputArrayIndex(port, executionRoot);
                    writeOutputArrayIndex(port, indexPath);
                }
            }

            for (Parameter param: ci.getComponent().getParameters()) {
                String value = ci.getParameterValue(param).toString();
                writeParameter(param, value);
            }
        } finally {       
            this.writer.close();
        }
    }
    
}
