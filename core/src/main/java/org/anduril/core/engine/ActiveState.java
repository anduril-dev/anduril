package org.anduril.core.engine;

/**
 * Dynamic state of a component instance that describes whether
 * whether the instance is active, disabled or suspended. 
 */
public enum ActiveState {
    YES ("YES"),
    DISABLED ("DISABLED"),
    SUSPENDED ("SUSPENDED");
    
    private String id;
    
    private ActiveState(String id) {
        this.id = id;
    }
    
    public String getID() {
        return id;
    }
    
    /**
     * Return true if the state is YES.
     */
    public boolean isActive() {
        return this == YES;
    }
    
    public static ActiveState fromString(String ID) {
        for (ActiveState state: ActiveState.values()) {
            if (state.getID().equals(ID)) return state;
        }
        return null;
    }

}
