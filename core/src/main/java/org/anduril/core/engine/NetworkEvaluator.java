package org.anduril.core.engine;

import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;
import org.anduril.core.network.componentInstance.OutputComponentInstance;
import org.anduril.core.readers.NetworkFinalizer;
import org.anduril.core.readers.networkParser.EvaluatorHandler;
import org.anduril.core.readers.networkParser.NetworkHandler;
import org.anduril.core.utils.BreakpointException;
import org.anduril.core.utils.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.jar.*;

/**
 * Entry point for network parsing, evaluating, finalization and
 * execution. Networks with dynamic elements (e.g., for loops) are
 * supported. Using this class is preferred over calling {@link Engine}
 * or other related class manually.
 */
public class NetworkEvaluator {
    private File executionRoot;
    private Logger parentLogger;
    private Repository repository;
    private File dataDir;

    private Engine previousEngine;

    private String[] forcedNames;
    private List<DynamicError> engineErrors;

    private boolean callbackEnabled;

    private boolean readPreviousState;
    private boolean minimizeSpace;
    private boolean commitFinalState;
    private Set<ComponentInstance> uptodateDynamicNodes;

    /**
     * Initialize with an execution directory.
     * @param executionRoot Root of the execution directory. The state
     *  file is stored in this directory. May be null, in which case
     *  evaluating dynamic networks or executing networks is not
     *  possible.
     * @param parentLogger Logger for top-level messages.
     */
    public NetworkEvaluator(File executionRoot, Repository repository, Logger parentLogger) {
        if (parentLogger == null) parentLogger = Logger.getLogger("<workflow-executor>");

        this.executionRoot = executionRoot;
        this.parentLogger = parentLogger;
        this.repository = repository;

        this.previousEngine = null;
        this.readPreviousState = true;
        this.minimizeSpace = false;
        this.callbackEnabled = true;

        NetworkHandler.setRepository(getRepository());
        EvaluatorHandler.setNetworkEvaluator(this);
    }

    public NetworkEvaluator(File executionRoot, Repository repository) {
        this(executionRoot, repository, null);
    }

    /**
     * Return true if there are errors in the engine(s).
     */
    public boolean hasEngineErrors() {
        return (this.engineErrors != null && !this.engineErrors.isEmpty());
    }

    /**
     * Return dynamic error from engine execution(s).
     */
    public List<DynamicError> getEngineErrors() {
        if (this.engineErrors == null) return Collections.emptyList();
        else return this.engineErrors;
    }

    /**
     * Return the root of the execution directory. May be null.
     */
    public File getExecutionRoot() {
        return this.executionRoot;
    }

    public ExecutionDirectory getExecutionDirectory(File executionRoot) {
        return new ExecutionDirectory(null, executionRoot);
    }

    public Repository getRepository() {
        return this.repository;
    }

    public Logger getDefaultLogger() {
        return this.parentLogger;
    }

    public void setMinimizeSpace(boolean minimizeSpace) {
        this.minimizeSpace = minimizeSpace;
    }

    public void setCommitFinalState(boolean commitFinalState) {
        this.commitFinalState = commitFinalState;
    }

    public File getDataDir() {
        return this.dataDir;
    }

    public void setDataDir(File dataDir) {
        this.dataDir = dataDir;
    }

    // Evaluating Scala code //

    public Network evaluate(File workflowSource, boolean dynamic) throws StaticError {
        if (dynamic) {
            enableCallback();
            if (this.engineErrors != null) this.engineErrors.clear();
        } else {
            disableCallback();
        }

        Network network = new Network(getRepository());
        if (getDataDir() == null) {
            network.setDataDir(workflowSource.getParentFile());
        } else {
            network.setDataDir(getDataDir());
        }

        final String ERROR_MESSAGE = "Cannot load JAR workflow: ";
        try {
            loadWorkflowJAR(workflowSource, network);
        } catch (
                NoSuchMethodException |
                InvocationTargetException |
                IllegalAccessException |
                ClassNotFoundException |
                IOException |
                InstantiationException e) {
            throw new StaticError(ERROR_MESSAGE + e,
                    new SourceLocation(workflowSource), e);
        }

        if (this.minimizeSpace) {
            for (ComponentInstance ci: network) {
                if (!(ci instanceof OutputComponentInstance)) {
                    ci.setInstanceAnnotation(Annotation.KEEP, Boolean.FALSE);
                }
            }
        }

        finalizeNetwork(network, workflowSource);
        this.warnUnconnectedOutputs(network);

        return network;
    }

    public Network evaluate(File workflowSource) throws StaticError {
        return evaluate(workflowSource, true);
    }

    // reference - http://stackoverflow.com/questions/11016092/how-to-load-classes-at-runtime-from-a-folder-or-jar
    private void loadWorkflowJAR(File workflowJAR, Network network)
            throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException, ClassNotFoundException, InstantiationException, StaticError {

        if (workflowJAR == null) {
            throw new StaticError("No JAR workflow file given");
        }

        NetworkHandler.setNetwork(network);
        NetworkHandler.setRepository(getRepository());
        EvaluatorHandler.setNetworkEvaluator(this);

        JarFile jarfile;

        try {
            jarfile = new JarFile(workflowJAR);
        } catch (java.util.zip.ZipException ze) {
            throw new StaticError("Cannot open given file " + workflowJAR + " as JAR." + ze,
                    new SourceLocation(workflowJAR), ze);
        }

        // Get the manifest
        Manifest manifest = jarfile.getManifest();
        // Get the main attributes in the manifest
        Attributes attrs = (Attributes)manifest.getMainAttributes();
        String mainClass = attrs.getValue("Main-Class");

        if (mainClass == null) {
            /* No Main-Class: load all classes in JAR file to invoke
             * their static constructors. */
            for (String className: getAllJARClassNames(workflowJAR)) {
                try {
                    Class.forName(className);
                } catch (ExceptionInInitializerError iie) {
                    this.parentLogger.error(iie.getCause());
                    throw new StaticError("Could not load class " + className);
                }
            }
        } else {
            Class dynamic = Class.forName(mainClass);
            Class[] mainParameters = new Class[]{String[].class};
            Method main = dynamic.getMethod("main", mainParameters);

            // Declare the method as accessible.
            main.setAccessible(true);

            Object mainArgs = new String[]{};
            main.invoke(null, mainArgs);
        }
    }

    private ArrayList<String> getAllJARClassNames(File jarFile) throws IOException {
        ArrayList<String> classNames = new ArrayList<String>();

        JarInputStream inputStream = new JarInputStream(new FileInputStream(jarFile));
        JarEntry entry = inputStream.getNextJarEntry();
        while (entry != null) {
            String fileName = entry.getName();
            if (fileName.endsWith(".class") && !entry.isDirectory()) {
                String className = fileName.replaceAll("/", ".");
                className = className.substring(0, className.length()-".class".length());
                classNames.add(className);
            }
            entry = inputStream.getNextJarEntry();
        }
        inputStream.close();

        return classNames;
    }

    private void finalizeNetwork(Network network, File scalaSource) throws StaticError {
        NetworkFinalizer nf = new NetworkFinalizer(network, scalaSource);
        nf.runFinalizer();
    }

    /**
     * Implement W1: Warn if all outputs of a component instance are unconnected.
     */
    private void warnUnconnectedOutputs(Network network) {
        List<String> unconnected = new ArrayList<String>();
        for (ComponentInstance ci: network) {
            if (!ci.isExecutable() ||
                    ci.isDisabled() ||
                    ci.getComponent().getOutPorts().isEmpty() ||
                    !ci.getOutPortConnections().isEmpty())
                continue;
            unconnected.add(ci.getName());
            ci.getLogger().debug("Warning: all out-ports are unconnected", Logger.tagWarningWorkflowStructure());
        }
    }

    // Callback //

    private void enableCallback() {
        this.callbackEnabled = true;
        if (this.uptodateDynamicNodes != null) this.uptodateDynamicNodes.clear();
    }
    
    private void disableCallback() {
        this.callbackEnabled = false;
    }

    /**
     * Callback method for the AndurilScript parser. Return the
     * location of the file for given output port used in a
     * dynamic context. Before returning, NetworkEvaluator
     * ensures that the contents of the file are up-to-date.
     * This is done by executing the current partial network,
     * or omitting execution if the file is already up-to-date.
     * 
     * @param cip An output port of a component instance.
     * @param location Source code location of the dynamic
     *  element. Used for formatting errors.
     * @throws BreakpointException If callbacks are disabled.
     */
    public File getDynamicOutputFile(ComponentInstancePort<OutPort> cip, SourceLocation location)
    throws DynamicError {
        if (!this.callbackEnabled) {
            throw new BreakpointException("Callbacks not enabled", cip.getComponentInstance());
        }
        if (this.executionRoot == null) {
            throw new DynamicError("Can not evaluate dynamic network: execution root has not been set", cip.getComponentInstance());
        }
        
        final ComponentInstance ci = cip.getComponentInstance();
        final OutPort port = cip.getPort();

        ci.getLogger().info(String.format("Accessing dynamic contents of port '%s'", port.getName()), Logger.tagCallback(), Logger.tagSource(location));

        boolean keep = Annotation.convertBoolean(ci.getInstanceAnnotation(Annotation.KEEP));
        if (!keep) {
            throw new DynamicError("Component instance with a dynamic port must have @keep=true", cip.getComponentInstance());
        }
        
        boolean enabled = Annotation.convertBoolean(ci.getInstanceAnnotation(Annotation.ENABLED));
        if (!enabled) {
            throw new DynamicError("Component instance with a dynamic port must have @enabled=true", cip.getComponentInstance());
        }

        boolean uptodate = this.uptodateDynamicNodes != null && this.uptodateDynamicNodes.contains(ci);
        if (!uptodate) {
            try {
                execute(cip.getComponentInstance().getNetwork(), true);
            } catch (StaticError e) {
                throw new DynamicError("Error when executing workflow to obtain dynamic output", cip.getComponentInstance(), e);
            }
        }

        if (port == null) {
            throw new NullPointerException("port is null");
        }

        File outFile = ci.getOutputFile(port, this.executionRoot);  // All port types are actual types.

        if (outFile == null || !outFile.exists()) {
            throw new DynamicError("Can not locate output file of "+ci.getName(), cip.getComponentInstance());
        }

        if (this.uptodateDynamicNodes == null) {
            this.uptodateDynamicNodes = new HashSet<ComponentInstance>();
        }
        this.uptodateDynamicNodes.add(ci);

        return outFile;
    }

    // Execution //
    
    public void setForced(String[] forcedNames) {
        this.forcedNames = forcedNames;
    }

    private Set<ComponentInstance> getForcedNodes(Network network, boolean reportError) throws StaticError {
        if (this.forcedNames == null) return null;
        
        final Set<ComponentInstance> forcedNodes = new HashSet<ComponentInstance>();
        
        for (String name: this.forcedNames) {
            name = name.trim();
            if (name.isEmpty()) continue;
            if (name.equals("*")) {
                forcedNodes.addAll(network.getMembers());
                break;
            }
            
            ComponentInstance ci = network.getComponentInstance(name);
            if (ci == null) {
                if (reportError) {
                    throw new StaticError("Invalid name for forced component: "+name);
                }
                continue;
            }
            forcedNodes.add(ci);
        }

        return forcedNodes;
    }
    
    public void setReadStateFile(boolean readStateFile) {
        this.readPreviousState = readStateFile;
    }

    public Engine makeEngine(Network network) throws StaticError {
        return makeEngine(network, false);
    }

    private Engine makeEngine(Network network, boolean fromCallback) throws StaticError {
        enableCallback();

        NetworkHandler.setNetwork(network);

        if (!network.getFinalized() || fromCallback) {
            finalizeNetwork(network, null);
        }

        Set<ComponentInstance> forcedNodes = getForcedNodes(network, !fromCallback);

        final boolean isFinalStage = !fromCallback;

        if (this.previousEngine == null) {
            this.previousEngine = new Engine(network,
                    this.executionRoot, isFinalStage, this.readPreviousState,
                    forcedNodes, this.parentLogger);
        }
        else {
            this.previousEngine = new Engine(this.previousEngine, network, isFinalStage, forcedNodes, this.parentLogger);
        }

        return this.previousEngine;
    }

    private Engine execute(Network network, boolean fromCallback) throws StaticError, DynamicError {
        Engine engine = makeEngine(network, fromCallback);
        execute(engine);
        return engine;
    }

    /**
     * Execute a network. If the network has not been parsed or
     * evaluated, this is done first. Execution errors should be
     * queried using {@link #getEngineErrors()} and not from the
     * returned Engine instance, because there may be multiple
     * Engine instances for networks with dynamic elements.
     *
     * @return The Engine for the last execution round. For
     *  non-dynamic networks, this is the only Engine used. The
     *  instance can be used to query post-execution component
     *  instance states.
     */
    public Engine execute(Network network) throws StaticError, DynamicError {
        return execute(network, false);
    }

    public void execute(Engine engine) throws StaticError, DynamicError {
        final int numPreviousErrors = getEngineErrors().size();

        engine.setCommitFinalState(this.commitFinalState);
        engine.execute();

        if (!this.readPreviousState) {
            this.readPreviousState = true;
            this.forcedNames = new String[] {"*"};
        }

        if (engine.hasErrors()) {
            if (this.engineErrors == null) {
                this.engineErrors = new ArrayList<DynamicError>();
            }
            this.engineErrors.addAll(engine.getErrors());

            final int numCurrentErrors = engine.getErrors().size();

            if (numCurrentErrors > numPreviousErrors) {
                final String msg = String.format(
                        "%d error%s occurred during execution",
                        numCurrentErrors, numCurrentErrors > 1 ? "s" : "");
                this.parentLogger.error(msg);
            }
        }

        if (engine.isFinalStage() && engine.hasOverallErrors()) {
            throw new DynamicError("Failed execution");
        }
    }
}
