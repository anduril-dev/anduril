package org.anduril.core.engine;

import org.anduril.core.network.ComponentInstance;
import org.anduril.core.network.SourceLocation;
import org.anduril.core.utils.BaseError;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * A dynamic error that occurred during network execution.
 */
public class DynamicError extends BaseError {
    private static final long serialVersionUID = 1L;

    /**
     * Initialize.
     * @param message Error message.
     * @param ci The component instance related to this error. May be null.
     * @param exception A relevant caught exception. May be null.  
     */
    public DynamicError(String message, ComponentInstance ci, Throwable exception) {
        super(message, ci, exception);
    }
    
    public DynamicError(String message, ComponentInstance ci) {
        this(message, ci, null);
    }
    
    public DynamicError(String message, Throwable exception) {
        this(message, null, exception);
    }
    
    public DynamicError(String message) {
        this(message, null, null);
    }
    
    public String format() {
        StringBuilder sb = new StringBuilder();
        
        if (getComponentInstance() != null) {
            SourceLocation location = getComponentInstance().getSourceLocation();
            if (location != null && !location.allNull()) {
                sb.append(location.toString());
            }
            if (sb.length() > 0) sb.append(", ");
            sb.append("Component " + getComponentInstance().getName());
        }
        
        if (sb.length() > 0) sb.append(": ");
        sb.append(getMessage());
        
        if (getException() != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            getException().printStackTrace(printWriter);
            printWriter.close();
            sb.append(" "+stringWriter.toString());
        }
        
        return sb.toString();
    }
    
}
