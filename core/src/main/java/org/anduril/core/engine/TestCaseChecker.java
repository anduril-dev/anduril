package org.anduril.core.engine;

import org.anduril.core.network.*;
import org.anduril.core.readers.TestReportReader;
import org.anduril.core.utils.Logger;
import org.anduril.core.writers.ReportWriter;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

/**
 * Execute component or network black box tests.
 */
public class TestCaseChecker {
	
    private final Repository repository;
    private final File executionRoot;
    private final Logger defaultLogger;
    private List<TestCase> testCases;
    private List<Component> testlessComponents;
    private List<TestCase> passed;
    private List<TestCase> inactive;
    private List<TestCase> failed;

    public TestCaseChecker(Repository repository, File executionRoot, Logger defaultLogger) {
        this.repository = repository;
        this.executionRoot = executionRoot;
        this.defaultLogger = defaultLogger;
        this.testCases = new ArrayList<TestCase>();
        this.testlessComponents = new ArrayList<Component>();
        
        this.passed = new ArrayList<TestCase>();
        this.inactive = new ArrayList<TestCase>();
        this.failed = new ArrayList<TestCase>();
    }

    public File getExecutionRoot() {
        return executionRoot;
    }

    public List<TestCase> getTestCases() {
    	TreeMap<String,TestCase> sortedTCs = new TreeMap<String,TestCase>();
    	for (TestCase testcase : testCases) {
    		sortedTCs.put(testcase.getCaseDirectory().getAbsolutePath(), testcase);
    	}
    	return new ArrayList<TestCase>(sortedTCs.values());
    }

    public void addTestCase(TestCase testCase) {
    	testCase.setExecutionRoot(getExecutionRoot());
        testCases.add(testCase);
    }
    
    public void addTestlessComponent(Component comp) {
    	testlessComponents.add(comp);
    }

    public List<TestCase> getPassed() {
        return passed;
    }

    public List<TestCase> getInactive() {
        return inactive;
    }
    
    public List<TestCase> getFailed() {
        return failed;
    }
    
    public int executeTestCases()
	throws IOException, ParserConfigurationException, SAXException, 
			InterruptedException, ExecutionException, StaticError
	{
    	if ((getTestCases() == null) || (getTestCases().size() == 0)) {
    	    throw new StaticError("No test cases to execute");
    	}

    	long testStarts = System.currentTimeMillis();
        List<TestCase> processedTestCases = new ArrayList<TestCase>();

        for (TestCase future : getTestCases()) {
            processedTestCases.add(future.execute());
        }

        long testEnds = System.currentTimeMillis();

        Map<String, Map<String,TestCase>> testCasesTree = new TreeMap<String, Map<String,TestCase>>();
        boolean areComponentTestCases = true;

        for (TestCase testCase: processedTestCases) {
            if (testCase.getTestCaseResult() == TestCase.PASSED)
                passed.add(testCase);
            else if (testCase.getTestCaseResult() == TestCase.INACTIVE)
                inactive.add(testCase);
            else
                failed.add(testCase);

            testCase.setnParameters(0);
            testCase.setExpectSuccess(true);

            if (testCase.getCaseNetwork() == null) {
            	continue;
            }

            if (testCase instanceof NetworkTestCase) { // Network test case.
                areComponentTestCases = false;
                NetworkTestCase networkTestCase = (NetworkTestCase) testCase;
                Map<String, TestCase> compTree = testCasesTree.get(networkTestCase.getCaseName());
                if (compTree == null) {
                    compTree = new TreeMap<String, TestCase>();
                    compTree.put(networkTestCase.getCaseDirectory().getName(), networkTestCase);
                    testCasesTree.put(networkTestCase.getCaseName(), compTree);
                }
                else {
                    TestCase tCase = compTree.get(testCase.getCaseDirectory().getName());
                    if (tCase == null) {
                        compTree.put(testCase.getCaseDirectory().getName(), testCase);
                    }
                }
            }
            //else (testCase instanceof ComponentTestCase)
            else {
                ComponentTestCase compTC = (ComponentTestCase)testCase;
                int nParameters = compTC.getComponent().getParameters().size();
                List<String> paramsUsage = this.getParameterUsage(testCase);

                testCase.setnParameters(nParameters);
                compTC.setParamsUsage(paramsUsage);
                testCase.expectSuccess();

                Map<String, TestCase> compTree = testCasesTree.get(testCase.getComponent().getName());
                if (compTree == null) {
                    compTree = new TreeMap<String, TestCase>();
                    compTree.put(testCase.getCaseDirectory().getName(), testCase);
                    testCasesTree.put(testCase.getComponent().getName(), compTree);
                }
                else {
                    TestCase tCase = compTree.get(testCase.getCaseDirectory().getName());
                    if (tCase == null) {
                        compTree.put(testCase.getCaseDirectory().getName(), testCase);
                    }
                }
            }
        }

        TestCaseXMLWriter xmlWriter = new TestCaseXMLWriter(testStarts, testEnds);
        File xmlFile = xmlWriter.writeXML(testCasesTree);

        TestReportReader reader = new TestReportReader(testlessComponents, xmlFile);
        ReportWriter writer = new ReportWriter(reader, executionRoot, areComponentTestCases);
        writer.write();
        xmlFile.delete();

        return failed.size();
    }

    private List<String> getParameterUsage(TestCase testCase) 
    {
    	List<String> myParamsUsage = new ArrayList<String>();

        ComponentInstance myCi = testCase.getComponentInstance();
        if (myCi == null)
        	return myParamsUsage;

        for (Parameter myParam : myCi.getComponent().getParameters()) {
            Object val = myCi.getParameterValue(myParam);
            if (val == null) { continue; } // Value assignments have failed already and the test case is broken!
        	String myValue = val.toString();
            if (myValue == null) continue;

        	String myParamName = myParam.getName();
        	String myDefaultValue = myParam.getDefaultValue();

        	String myLabel = ""; 
        	if (myDefaultValue != null) {
            	if (myValue.trim().equals(myDefaultValue.trim())) {
            		myLabel = "defval_" + myParamName;
            	}
            	else if (myParam.getType().equals(ParameterType.INT) ||
            			 myParam.getType().equals(ParameterType.FLOAT)){
            		try {
            			double myVal = Double.parseDouble(myValue);
            			double myDVal = Double.parseDouble(myDefaultValue);
            			if (myVal == myDVal) myLabel = "defval_" + myParamName;
            			else myLabel = "nondefval_" + myParamName;
            			
            		}
            		catch (NumberFormatException e) {
            			String msg = String.format("In component %s, could not convert value " 
            					+ "for parameter %s of type INT or FLOAT into DOUBLE.",
                                myCi.getComponent().getName(), myParam.getName());
                        throw new RuntimeException(msg, e);
            		}
            	}
            	else {
            		myLabel = "nondefval_" + myParamName;
            	}
        	}
        	myParamsUsage.add(myLabel);
        }

        return myParamsUsage;
    }
    
    
    public void printResultsToLog() {
        if (!passed.isEmpty()) {
            this.defaultLogger.info("Passed test cases:");
            for (TestCase testCase: passed) {
                this.defaultLogger.info(testCase.getCaseDirectory().getAbsolutePath());
            }
        }

        if (!failed.isEmpty()) {
            this.defaultLogger.error("Failed test cases:");
            for (TestCase testCase: failed) {
                this.defaultLogger.error(testCase.getCaseDirectory().getAbsolutePath());
            }
        }
        
        if (!inactive.isEmpty()) {
            this.defaultLogger.info("Inactive test cases:");
            for (TestCase testCase: inactive) {
                this.defaultLogger.info(testCase.getCaseDirectory().getAbsolutePath());
            }
        }
    }
    
}
