package org.anduril.core.engine;

import org.anduril.core.network.InPort;
import org.anduril.core.network.OutPort;
import org.anduril.core.network.TestCase;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Write test case result XML file. This class is a Java
 * Logging handler that listens to the global log; log
 * messages during execution of test cases are written
 * into <code>log</code> elements in the XML file. This
 * class is usually invoked by TestCaseChecker.
 */
public class TestCaseXMLWriter {
	public static final char CHAR_LOWER_LEGAL_BOUND = 0x0020;
    public static final String NEWLINE = "\n";
    public static final String EMPTY_STR = "";
    
    private final File xmlFile;
    private final Writer xmlWriter;
    long testStarts; 
    long testEnds;

    public TestCaseXMLWriter(long testStarts, long testEnds) throws IOException {
    	xmlFile = File.createTempFile("report-tmp", ".xml");
        this.xmlWriter = new BufferedWriter(new FileWriter(xmlFile));
        this.testStarts = testStarts;
        this.testEnds = testEnds;
    }
    
    public File writeXML(Map<String, Map<String,TestCase>> testCasesTree) {
    	try {
			writeHeader();
	    	for (Map.Entry<String, Map<String,TestCase>> comp : testCasesTree.entrySet()) {
	    		this.beginComponent(comp.getKey());
	    		for (Map.Entry<String,TestCase> testCase : comp.getValue().entrySet()) {
	    				writeTestCase(testCase.getKey(), testCase.getValue());
	    		}
	    		this.endComponent();
	    	}
	    	this.writeFooter();
	    	this.flush();
	    	this.close();
    	} catch (IOException e) {
			e.printStackTrace();
		}

    	return this.xmlFile;
    }

    private void close() {
        try {
            this.xmlWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void flush() {
        try {
            this.xmlWriter.flush();
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }

    private void writeXMLFiltered(String original) 
    {
        String msg = original;
        msg = msg.replace("&", "&amp;");
        msg = msg.replace("<", "&lt;");
        msg = msg.replace(">", "&gt;");
        msg = msg.replace("\"", "&quot;");
        msg = msg.replace("'", "&apos;");

        char[] charMsg = msg.toCharArray();
        for (int i=0; i < charMsg.length; i++) {
        	if (charMsg[i] < CHAR_LOWER_LEGAL_BOUND &&
        	   (charMsg[i] != 0x0009)               &&
        	   (charMsg[i] != 0x000A)               &&
               (charMsg[i] != 0x000D)) {
    			charMsg[i] = CHAR_LOWER_LEGAL_BOUND;    				
        	}
        }

        String filteredMsg = new String(charMsg);
        try {
            this.xmlWriter.write(filteredMsg+NEWLINE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void write(String str) throws IOException {
        this.xmlWriter.write(str);
        if (!str.endsWith("\n") && !str.endsWith("\r")) {
            this.xmlWriter.write(NEWLINE);
        }
    }

    /**
     * Write XML header and the start tag.
     */
    private void writeHeader() throws IOException {
        write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss",   Locale.US);

        Date starts = new Date(testStarts);
        write(String.format("<report startdate=\"%s\" starttime=\"%s\" ",
                dateFormat.format(starts), timeFormat.format(starts)));

        Date ends = new Date(testEnds);
        write(String.format("enddate=\"%s\" endtime=\"%s\">",
                dateFormat.format(ends), timeFormat.format(ends)));
    }

    /**
     * Write XML footer.
     */
    private void writeFooter() throws IOException {
        write("</report>");
    }
    
    /**
     * Open a <code>component</code> element. This elements
     * usually contains <code>testcase</code> elements as
     * children.
     * @param componentName Name of the component.
     */
    private void beginComponent(String componentName) throws IOException {
        write(String.format("<component name=\"%s\">", componentName));
    }
    
    /**
     * Close a <code>component</code> element.
     */
    private void endComponent() throws IOException {
        write("</component>");
    	flush();
    }

    /**
     * Close a <code>testcase</code> element. This includes
     * closing the previously opened <code>log</code> element
     * and writing <code>outcome</code>, <code>usedInPorts</code>
     * and <code>missingInPorts</code> elements.
     * 
     * @param outcome <code>TestCase</code> result.
     * @param seconds Elapsed seconds for the test case.
     * @param usedInPorts List of input ports that were utilized in
     *  this test case.
     * @param unusedInPorts List if input ports that were not used
     *  in this test case.
	*  @param usedOutPorts List of output ports that were utilized in
     *  this test case.
     * @param unusedOutPorts List if output ports that were not used
     *  in this test case.
     * @param paramsUsage List of parameter names with usage 
     *  prefix (defval_ or nondefval_).
     */
    private void writeTestCase(String caseName, TestCase testCase) {
    	try {
			write(String.format("<testcase name=\"%s\">", caseName));
			write("<log>");
			this.writeXMLFiltered(testCase.getLogContent().toString());
			write("</log>");

			Integer outcome = testCase.getTestCaseResult();
			double seconds = testCase.getSeconds(); 
			List<InPort> usedInPorts = testCase.getUsedInPorts();
			List<InPort> unusedInPorts = testCase.getMissingInPorts(); 
			List<OutPort> usedOutPorts = testCase.getUsedOutPorts();
			List<OutPort> unusedOutPorts = testCase.getMissingOutPorts();
			int nParameters = testCase.getnParameters();
			List<String> paramsUsage = testCase.getParamsUsage();
			boolean isExpectSuccess = !testCase.isFailure();
	        
			write(String.format(Locale.US, "<outcome success=\"%s\" elapsedSeconds=\"%.1f\" expectSuccess=\"%s\"/>", 
	        		outcome, seconds, isExpectSuccess));
	        
	        StringBuilder sb = new StringBuilder(10000);
	        final String MANDATORY_PREFIX = "m_";
	        
	        if (usedInPorts == null) {
	            write(String.format("<usedInPorts>%s</usedInPorts>", EMPTY_STR));
	        } else {
	            for (InPort port: usedInPorts) {
	                if (sb.length() > 0) sb.append(',');
	                if (!port.isOptional()) sb.append(MANDATORY_PREFIX);
	                sb.append(port.getName());
	            }
	            write(String.format("<usedInPorts>%s</usedInPorts>", sb.toString()));
	        }
	        
	        if (unusedInPorts == null) {
	            write(String.format("<missingInPorts>%s</missingInPorts>", EMPTY_STR));
	        } else {
	            sb.setLength(0);
	            for (InPort port: unusedInPorts) {
	                if (sb.length() > 0) sb.append(',');
	                if (!port.isOptional()) sb.append(MANDATORY_PREFIX);
	                sb.append(port.getName());
	            }
	            write(String.format("<missingInPorts>%s</missingInPorts>", sb.toString()));
	        }
	        
	        if (usedOutPorts == null) {
	            write(String.format("<usedOutPorts>%s</usedOutPorts>", EMPTY_STR));
	        } else {
		        sb.setLength(0);
		        for (OutPort port : usedOutPorts) {
		        	if (sb.length() > 0) sb.append(',');
	                sb.append(MANDATORY_PREFIX).append(port.getName());
		        }
		        write(String.format("<usedOutPorts>%s</usedOutPorts>", sb.toString()));
	        }
	        
	        if (unusedOutPorts == null) {
	            write(String.format("<missingOutPorts>%s</missingOutPorts>", EMPTY_STR));
	        } else {
		        sb.setLength(0);
		        for (OutPort port : unusedOutPorts) {
		        	if (sb.length() > 0) sb.append(',');
	                sb.append(MANDATORY_PREFIX).append(port.getName());
		        }
		        write(String.format("<missingOutPorts>%s</missingOutPorts>", sb.toString()));
	        } 
	        
	        write(String.format("<nParameters>%s</nParameters>", nParameters));
	        
	        if (paramsUsage == null) {
	            write(String.format("<parameterUsage>%s</parameterUsage>", EMPTY_STR));
	        } else {
		        sb.setLength(0);
		        for (String myLabel : paramsUsage) {
		        	if (sb.length() > 0) sb.append(',');
	                sb.append(myLabel);
		        }
		        write(String.format("<parameterUsage>%s</parameterUsage>", sb.toString()));
	        } 

	        write("</testcase>");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

}
