package org.anduril.core.readers;

import org.anduril.core.network.TestCase;
import org.anduril.core.utils.XMLTools;
import org.w3c.dom.Element;

import java.text.DecimalFormat;
import java.util.*;

public class TestReportItem {
	private final static String PASSED = "passed";
	private final static String INACTIVE = "inactive";
	private final static String FAILED = "<span style=\"background-color:red\">failed</span>";

	private String theComponentName = "";
	private List<String> theAllInPorts = new ArrayList<String>();
	private List<String> theAllOptionalInPorts = new ArrayList<String>();
	private List<String> theAllOutPorts = new ArrayList<String>();
	private Set<String> theParameterCoverage = new HashSet<String>();
	private int theNInPorts = 0;
	private int theNOutPorts = 0;
	private int theNMandatoryInPorts = 0;
	private int theNOptionalInPorts = 0;
	private int thePopulation = 0;
	private int theOptionalPopulation = 0;
	private int theNTests = 0;
	private double theElapsedSecs = 0;
	private StringBuffer theElapsedSecsDiv = new StringBuffer();
	private StringBuffer theFinalElapsedSecsDiv = new StringBuffer();
	private int theNParameters = 0;
	private List<TestCaseItem> thePassed = new ArrayList<TestCaseItem>();
	private List<TestCaseItem> theInactive = new ArrayList<TestCaseItem>();
	private List<TestCaseItem> theFailed = new ArrayList<TestCaseItem>();
	
	public TestReportItem(Element aItem) {
		theComponentName = aItem.getAttribute("name");

		
		TestCaseItem myTCItem = null;
		for (Element myCase : XMLTools.getChildrenByName(aItem, "testcase")) {
			myTCItem = new TestCaseItem(myCase);
				for (String myStr : myTCItem.theUsedInPorts) {
					if (myStr.indexOf("m_") == -1) theOptionalPopulation += 1;
				}
				thePopulation += myTCItem.theUsedInPorts.size();
				theElapsedSecs += myTCItem.theSeconds; 
				theElapsedSecsDiv.append(String.format(Locale.US, "%.1f; ", myTCItem.theSeconds));
				
				theNParameters = myTCItem.theNParameters;
				theParameterCoverage.addAll(myTCItem.theParameterUsage);
				
				if (myTCItem.isPassed) thePassed.add(myTCItem);
				else if (myTCItem.isInactive) theInactive.add(myTCItem);
				else theFailed.add(myTCItem);
		}
		
		if (myTCItem != null) {
			if (myTCItem.theMissingInPorts.size() > 0) theAllInPorts.addAll(myTCItem.theMissingInPorts);
			if (myTCItem.theUsedInPorts.size() > 0) theAllInPorts.addAll(myTCItem.theUsedInPorts);
			theNInPorts = theAllInPorts.size();
			// TODO: theNOptionalInPorts = ..aItem. ;
		}
		
		if (myTCItem != null) {
			if (myTCItem.theMissingOutPorts.size() > 0) theAllOutPorts.addAll(myTCItem.theMissingOutPorts);
			if (myTCItem.theUsedOutPorts.size() > 0) theAllOutPorts.addAll(myTCItem.theUsedOutPorts);
			theNOutPorts = theAllOutPorts.size();
			// TODO: theNOptionalInPorts = ..aItem. ;
		}
		
		
		for (String myP : theAllInPorts) {
			if (myP.indexOf("m_") != -1) { 
				theNMandatoryInPorts += 1;
				theAllOptionalInPorts.add(myP);
			}
		}
		
		theNOptionalInPorts = theNInPorts - theNMandatoryInPorts;
		theNTests = thePassed.size() + theInactive.size() + theFailed.size();
	}
	
	public String getComponentNameTagged() {
		if (isFailed()) 
			return 
				"<a href=\"#" + theComponentName + "\">" + 
				"<span style=\"background-color:red\">" + 
				theComponentName + "</span>";
		return theComponentName;
	}
		
	public String getComponentName() {
		return theComponentName;
	}
	
	public List<TestCaseItem> getPassed() {
		return thePassed;
	}
	
	public List<TestCaseItem> getInactive() {
        return theInactive;
    }
	
	public List<TestCaseItem> getFailed() {
		return theFailed;
	}
	
	public boolean isPassed() {
		if ((getFailed().size() == 0) && (getInactive().size() == 0)) return true;
		return false;
	}
	
   public boolean isFailed() {
        if (getFailed().size() > 0) return true;
        return false;
    }
	   
	public boolean isInactive() {
        if (getInactive().size() > 0) return true;
        return false;
    }
	
	public String getResult() {
	    if (getFailed().size() > 0) return FAILED;
	    if (getInactive().size() == theNTests) return INACTIVE;
	    if (getInactive().size() > 0) return PASSED;
		if (getFailed().size() == 0) return PASSED;
		return FAILED;
	}
	
	public String getTestProportion() {
	    int myInact = theInactive.size();
	    String inactive = "";
	    if (myInact > 0) inactive = 
	        " <span style=\"background-color:yellow\">(" + theInactive.size() + ")</span> ";

		return thePassed.size() + " / " + theNTests + inactive;
	}
	
	public double getElapsedSecs() {
		return theElapsedSecs;
	}
	
	public String getElapsedSecsStr() {
		int n = theElapsedSecsDiv.length();
		theElapsedSecsDiv.replace(n-2, n, "");
		theFinalElapsedSecsDiv.setLength(0);
		theFinalElapsedSecsDiv.append("<div title=\"");
		theFinalElapsedSecsDiv.append(getComponentName()).append(":  ");
		theFinalElapsedSecsDiv.append(theElapsedSecsDiv);
		theFinalElapsedSecsDiv.append("\"> ");
		theFinalElapsedSecsDiv.append(String.format(Locale.US, "%.1f", theElapsedSecs));
		theFinalElapsedSecsDiv.append("</div>");
		
		return theFinalElapsedSecsDiv.toString();
	}
	
	/**
	 * Evenly distributed index for test case input to the component in ports.
	 * Value 1 means perfectly even distribution, 0  means all input from all 
	 * test cases concentrated in one in port.
	 */
	public String getEDI() {
		if ((theNOptionalInPorts == 0) || (theOptionalPopulation == 0)) return "0";
		double myEDI = 0D;
		for (String myInPort : theAllOptionalInPorts) {
			int myN = 0;
			for (TestCaseItem myTCI : thePassed) {
				for (String myUsed : myTCI.theUsedInPorts) {
					if (myUsed.equals(myInPort)) myN += 1;
				}
			}
			
			for (TestCaseItem myTCI : theFailed) {
				for (String myUsed : myTCI.theUsedInPorts) {
					if (myUsed.equals(myInPort)) myN += 1;
				}
			}
			
			myEDI += Math.abs(((double)myN / (double)theOptionalPopulation) - (1D / (double) theNOptionalInPorts));
		}
		
		myEDI /= 2D;
		myEDI = 1D - myEDI;
		DecimalFormat df = new DecimalFormat("#.##");
		
		return df.format(myEDI);
	}
	
	public double getInACIVal() {
       if (theNInPorts == 0) return 0;
        int myUncovered = 0;
        boolean isCovered = false;
        for (String myInPort : theAllInPorts) {         
            isCovered = false;
            for (TestCaseItem myTCI : thePassed) {
                for (String myUsed : myTCI.theUsedInPorts) {
                    if (myUsed.equals(myInPort))  isCovered = true;
                }
            }
            
            for (TestCaseItem myTCI : theFailed) {
                for (String myUsed : myTCI.theUsedInPorts) {
                    if (myUsed.equals(myInPort)) isCovered = true;
                }
            }
            
            if (isCovered == false) myUncovered += 1;
            isCovered = false;
        }
        
        return (1D - (double)myUncovered / (double)theNInPorts);
	}
	
	public String getInACI() {
		if (theNInPorts == 0) return "0";
		int myUncovered = 0;
		boolean isCovered = false;
		for (String myInPort : theAllInPorts) {			
			isCovered = false;
			for (TestCaseItem myTCI : thePassed) {
				for (String myUsed : myTCI.theUsedInPorts) {
					if (myUsed.equals(myInPort))  isCovered = true;
				}
			}
			
			for (TestCaseItem myTCI : theFailed) {
				for (String myUsed : myTCI.theUsedInPorts) {
					if (myUsed.equals(myInPort)) isCovered = true;
				}
			}
			
			if (isCovered == false) myUncovered += 1;
			isCovered = false;
		}
		
		double myACI = (1D - (double)myUncovered / (double)theNInPorts);

		double norm_covered = myACI;
		int green = 0;
		int red = 255;
		if ((norm_covered >= 0.4) && (norm_covered < 0.75)) green = 255;
		else if (norm_covered >= 0.75) { green = 255; red = 0; }

		DecimalFormat df = new DecimalFormat("#.##");
		
		if (myACI < 1D) return "<span style=\"background-color:rgb(" 
			+ red + "," + green + ",0)\">" + df.format(myACI) + "</span>";

		return df.format(myACI);
	}
	
	public double getOutACIVal() {
        if (theNOutPorts == 0) return 0;
        int myUncovered = 0;
        boolean isCovered = false;
        for (String myOutPort : theAllOutPorts) {           
            isCovered = false;
            for (TestCaseItem myTCI : thePassed) {
                for (String myUsed : myTCI.theUsedOutPorts) {
                    if (myUsed.equals(myOutPort))  isCovered = true;
                }
            }
            
            for (TestCaseItem myTCI : theFailed) {
                for (String myUsed : myTCI.theUsedOutPorts) {
                    if (myUsed.equals(myOutPort)) isCovered = true;
                }
            }
            
            if (isCovered == false) myUncovered += 1;
            isCovered = false;
        }
        
        return (1D - (double)myUncovered / (double)theNOutPorts); 
}
	
	public String getOutACI() {
		if (theNOutPorts == 0) return "0";
		int myUncovered = 0;
		boolean isCovered = false;
		for (String myOutPort : theAllOutPorts) {			
			isCovered = false;
			for (TestCaseItem myTCI : thePassed) {
				for (String myUsed : myTCI.theUsedOutPorts) {
					if (myUsed.equals(myOutPort))  isCovered = true;
				}
			}
			
			for (TestCaseItem myTCI : theFailed) {
				for (String myUsed : myTCI.theUsedOutPorts) {
					if (myUsed.equals(myOutPort)) isCovered = true;
				}
			}
			
			if (isCovered == false) myUncovered += 1;
			isCovered = false;
		}
		
		double myACI = (1D - (double)myUncovered / (double)theNOutPorts); 
		
		double norm_covered = myACI;
		int green = 0;
		int red = 255;
		if ((norm_covered >= 0.4) && (norm_covered < 0.75)) green = 255;
		else if (norm_covered >= 0.75) { green = 255; red = 0; }

		DecimalFormat df = new DecimalFormat("#.##");
		
		if (myACI < 1D) return "<span style=\"background-color:rgb(" 
			+ red + "," + green + ",0)\">" + df.format(myACI) + "</span>";

		return df.format(myACI);
	}
	
   public double getPCoverVal() {
        int myCovered = theParameterCoverage.size();
        if (myCovered == 0) {
            return 0;
        }
        
        int myAllCases = 2 * theNParameters;
        if (myCovered == myAllCases) {
            return 1;
        }
                
        return ((double) myCovered / myAllCases);
    }
   
	public String getParameterCoverage() {
		int myCovered = theParameterCoverage.size();
		if (myCovered == 0) {
			return "0";
		}
		
		int myAllCases = 2 * theNParameters;
		if (myCovered == myAllCases) {
			return "1";
		}
				
		double norm_covered = 2 * ((double) myCovered / myAllCases) - 1;
		int green = 0;
		int red = 255;
		if ((norm_covered >= 0.4) && (norm_covered < 0.75)) green = 255;
		else if (norm_covered >= 0.75) { green = 255; red = 0; }
		
		String myPCoverage = myCovered + "/" + myAllCases;
		return "<span style=\"background-color:rgb(" + red + "," + green + ",0)\">" 
			+ myPCoverage + "</span>";
	}
	
	public String getAvIP() {
		if (theNInPorts == 0) return "0";
		double myAvIP = (double) thePopulation / (double) theNInPorts; // TODO: pretty format.
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(myAvIP);
	}
	
	public class TestCaseItem {
		public String theCaseName = "";
		public boolean isPassed = false;
		public boolean isInactive = false;
		public String theExpectedSuccess = "";
		public double theSeconds = 0;
		public List<String> theMissingInPorts = new ArrayList<String>();
		public List<String> theUsedInPorts = new ArrayList<String>();
		public List<String> theMissingOutPorts = new ArrayList<String>();
		public List<String> theUsedOutPorts = new ArrayList<String>();
		public Integer theNParameters = null;
		public List<String> theParameterUsage = new ArrayList<String>();
		public List<String> theMessage = new ArrayList<String>();
		
		public TestCaseItem(Element aCase) {
			theCaseName = aCase.getAttribute("name");
			
			Element outcome = XMLTools.getChildByName(aCase, "outcome");
			String myResult_str = outcome.getAttribute("success");
			Integer myResult = Integer.parseInt(myResult_str);
			if (myResult == TestCase.PASSED) isPassed = true;
			else if (myResult == TestCase.INACTIVE) isInactive = true;
			else isPassed = false;
			
			theExpectedSuccess = outcome.getAttribute("expectSuccess");
			
			
			String myTime = outcome.getAttribute("elapsedSeconds");
			try {
				theSeconds = Double.parseDouble(myTime);
			} catch (Exception e) {
				theSeconds = 0;
			}
			
			Element myMissing = XMLTools.getChildByName(aCase, "missingInPorts");
			String[] myMissingArr = XMLTools.getElementContentsAsString(myMissing).split(",");
			for (String myStr : myMissingArr) {
				if (myStr.trim().length() > 0) theMissingInPorts.add(myStr.trim());
			}
			
			Element myUsed = XMLTools.getChildByName(aCase, "usedInPorts");
			String[] myUsedArr = XMLTools.getElementContentsAsString(myUsed).split(",");
			for (String myStr : myUsedArr) {
				if (myStr.trim().length() > 0) theUsedInPorts.add(myStr.trim());
			}			
			
			Element myOMissing = XMLTools.getChildByName(aCase, "missingOutPorts");
			String[] myOMissingArr = XMLTools.getElementContentsAsString(myOMissing).split(",");
			for (String myStr : myOMissingArr) {
				if (myStr.trim().length() > 0) theMissingOutPorts.add(myStr.trim());
			}
			
			Element myOUsed = XMLTools.getChildByName(aCase, "usedOutPorts");
			String[] myOUsedArr = XMLTools.getElementContentsAsString(myOUsed).split(",");
			for (String myStr : myOUsedArr) {
				if (myStr.trim().length() > 0) theUsedOutPorts.add(myStr.trim());
			}
			
			Element myNParameters = XMLTools.getChildByName(aCase, "nParameters");
            String myNParametersStr = XMLTools.getElementContentsAsString(myNParameters);
            try {
                theNParameters = Integer.parseInt(myNParametersStr);
            } catch (NumberFormatException nfe) {
                theNParameters = 0;
            }
            
			
			Element myParamsUsage = XMLTools.getChildByName(aCase, "parameterUsage");
			String[] myParamsUsageArr = XMLTools.getElementContentsAsString(myParamsUsage).split(",");
			for (String myStr : myParamsUsageArr) {
				if (myStr.trim().length() > 0) theParameterUsage.add(myStr.trim());
			}
			
			Element myLog = XMLTools.getChildByName(aCase, "log");
			addMessageLine(XMLTools.getElementContentsAsString(myLog));
		}
		
		public String getCaseName() {
			return theCaseName;
		}
		
		public String getLog() {
			if (theMessage.isEmpty()) return "";
			StringBuilder mySB = new StringBuilder();
			for (String myMsg : theMessage) {
				mySB.append(myMsg + "<br/>");
			}
			return mySB.toString();
		}
		
		public void addMessageLine(String aLine) {			
			if (aLine == null) return;
			String[] myLines = aLine.split("\n");
			for (String myLine : myLines) {
//				if (myLine.contains("[ERROR]") || myLine.contains("[STDERR")) { 
					theMessage.add(myLine);				
//				}
			}
		}
	}
}
