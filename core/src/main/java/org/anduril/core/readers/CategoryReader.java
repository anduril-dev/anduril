package org.anduril.core.readers;

import org.anduril.core.network.Category;
import org.anduril.core.network.Network;
import org.anduril.core.network.Repository;
import org.anduril.core.network.StaticError;
import org.anduril.core.utils.XMLTools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Parse a category XML files and add instances of Category to
 * the network. There may be several categories declared in one
 * XML file.
 */
public class CategoryReader implements ErrorHandler {
    private File file;
    private Repository repository;
    private Document doc;
    private Element root;
    
    /**
     * Create a parser from file.
     * @param file Source XML file.
     * @param repository The repository where categories and parse
     * 	errors are added. 
     */
    public CategoryReader(File file, Repository repository) throws ParserConfigurationException,
    SAXException, IOException {
        this.file = file;
        this.repository = repository;
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        this.doc = builder.parse(file);
        this.root = doc.getDocumentElement();
    }

    /**
     * Create a parser from URL.
     * @param url Source URL for the XML file.
     * @param repository The repository where categories and parse
     * 	errors are added. 
     */
    public CategoryReader(URL url, Repository repository) throws ParserConfigurationException,
    SAXException, IOException {
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        this.file = null;
        this.repository = repository;
        this.doc = builder.parse(url.toExternalForm());
        this.root = doc.getDocumentElement();
    }
    
    protected String getSchemaLocation() {
        String xsd = "/categories.xsd";
        File full = new File(repository.getHomeDirectory(), "/core/src/main/resources" +xsd);
        return full.getAbsolutePath();
    }
    
    /**
     * Parse data type XML files and add instance of DataType to the network.
     * @return The categories that were read.
     */
    public Map<String,Category> parse() throws StaticError {
        File docResource = null;
        if (file != null) {
            docResource = new File(file.getParentFile(), Network.DOC_RESOURCES_DIR);
            if (!docResource.exists()) docResource = null;
        }
        
        Map<String,Category> categories = new HashMap<String,Category>();
        
        for (Element el: XMLTools.getChildrenByName(root, "category")) {
            String name = XMLTools.getSingleChildContents(el, "name");
            
            String parentName = XMLTools.getSingleChildContents(el, "parent-category");
            Category parentCategory = null;
            if (parentName != null) {
                parentCategory = repository.getCategory(parentName);
                if (parentCategory == null) {
                    throw new StaticError("Unknown parent category of "+name+": "
                    		+ parentName);
                }
            }
            
            String doc = null;
            Element docElem = XMLTools.getChildByName(el, "doc");
            if (docElem != null) doc = XMLTools.getElementContentsAsString(docElem);
            
            Category category = new Category(name, parentCategory, doc);
            categories.put(name, category);
            repository.addCategory(category);
        }
        
        return categories;
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        throw e;
    }
    
}
