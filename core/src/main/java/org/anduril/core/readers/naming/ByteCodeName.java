package org.anduril.core.readers.naming;

public final class ByteCodeName {
    final private String className;
    final private String method;
    final private int startLine;
    final private int endLine;
    final private String name;
    final private boolean ignored;
    final private boolean accurate;

    public ByteCodeName(String className, String method, int startLine, int endLine, String name,
        boolean ignored, boolean accurate) {
        if (startLine > endLine) {
            throw new IllegalArgumentException(String.format(
                    "Start line is greater than end line: %s.%s %d .. %d = %s",
                    className, method, startLine, endLine, name));
        }

        this.className = className;
        this.method = method;
        this.startLine = startLine;
        this.endLine = endLine;
        this.name = name;
        this.ignored = ignored;
        this.accurate = accurate;
    }

    @Override
    public String toString() {
        return String.format("%s.%s(%d-%d): %s (ignored: %s, accurate: %s)",
                this.className, this.method, this.startLine, this.endLine,
                this.name, this.ignored, this.accurate);
    }

    public String getClassName() {
        return this.className;
    }

    public String getMethod() {
        return this.method;
    }

    public int getStartLine() {
        return this.startLine;
    }

    public int getEndLine() {
        return this.endLine;
    }

    public String getName() {
        return this.name;
    }

    public boolean isIgnored() {
        return this.ignored;
    }

    public boolean isAccurate() {
        return this.accurate;
    }
}
