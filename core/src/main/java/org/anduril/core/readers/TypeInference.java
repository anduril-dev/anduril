package org.anduril.core.readers;

import org.anduril.core.network.*;
import org.anduril.core.utils.Pair;

import java.util.*;

public class TypeInference {
    private class ComponentInstanceTypeParameter extends Pair<ComponentInstance, TypeParameter> {
        public ComponentInstanceTypeParameter(ComponentInstance ci, TypeParameter typeParam) {
            super(ci, typeParam);
        }
    }

    private Network network;
    private List<ComponentInstance> genericComponents;
    private Map<ComponentInstanceTypeParameter,Set<DataType>> alternatives;
    private Map<DataType,Set<DataType>> subTypeCache;
    private Map<DataType,Set<DataType>> superTypeCache;
    private Set<DataType> allTypesCache;

    public TypeInference(Network network) {
        this.network = network;
        this.genericComponents = new ArrayList<ComponentInstance>();
        this.alternatives = new HashMap<ComponentInstanceTypeParameter, Set<DataType>>();
        this.subTypeCache = new HashMap<DataType, Set<DataType>>();
        this.superTypeCache = new HashMap<DataType, Set<DataType>>();
        this.allTypesCache = Collections.unmodifiableSet(new HashSet<DataType>(network.getRepository().getDataTypes()));
    }

    public void inferGenericTypes() throws StaticError {
        initAlternatives();
        determineAlternatives();
        setTypeParamsUsingHighestTypes();
    }

    /**
     * Fill genericComponents and alternatives with initial values.
     */
    private void initAlternatives() {
        for (ComponentInstance ci: this.network) {
            if (!ci.getComponent().isGeneric()) {
                continue;
            }
            this.genericComponents.add(ci);
            for (TypeParameter typeParam: ci.getComponent().getTypeParameters()) {
                final ComponentInstanceTypeParameter key = new ComponentInstanceTypeParameter(ci, typeParam);
                this.alternatives.put(key, new HashSet<DataType>(getSubTypes(typeParam)));
            }
        }
    }

    private void determineAlternatives() {
        boolean progress = true;
        while (progress) {
            progress = false;

            for (ComponentInstance genericCI: this.genericComponents) {
                for (PortConnection inConn: genericCI.getInPortConnections()) {
                    if (!inConn.getToPort().isGeneric()) continue;
                    TypeParameter typeParam = getTypeParam(inConn.getToPort());
                    // Contravariance
                    Set<DataType> superTypes = getSuperTypes(inConn.getFrom(), inConn.getFromPort(), inConn.isForced());
                    boolean changed = updateAlternativeWithIntersection(genericCI, typeParam, superTypes);
                    progress = progress || changed;
                }

                for (PortConnection outConn: genericCI.getOutPortConnections()) {
                    if (!outConn.getFromPort().isGeneric()) continue;
                    TypeParameter typeParam = getTypeParam(outConn.getFromPort());
                    // Covariance
                    Set<DataType> subTypes = getSubTypes(outConn.getTo(), outConn.getToPort(), outConn.isForced());
                    boolean changed = updateAlternativeWithIntersection(genericCI, typeParam, subTypes);
                    progress = progress || changed;
                }
            }
        }
    }

    private void setTypeParamsUsingHighestTypes() throws StaticError {
        for (Map.Entry<ComponentInstanceTypeParameter,Set<DataType>> altEntry: this.alternatives.entrySet()) {
            final ComponentInstance ci = altEntry.getKey().get1();
            final TypeParameter typeParam = altEntry.getKey().get2();
            final Set<DataType> altTypes = altEntry.getValue();
            DataType upperBound = collapseToHighestType(altTypes);
            if (upperBound == null) {
                throw new StaticError(
                        "Can not determine generic type of type parameter "+typeParam.getName(),
                        ci);
            } else {
                ci.setGenericType(typeParam, upperBound);
            }
        }
    }

    private DataType collapseToHighestType(Set<DataType> candidateTypes) {
        Set<DataType> upperBound = new HashSet<DataType>();

        // Start with a union of all sub-types
        for (DataType type: candidateTypes) {
            upperBound.addAll(getSubTypes(type));
        }

        // Intersect with super-types
        for (DataType type: candidateTypes) {
            upperBound.retainAll(getSuperTypes(type));
        }

        if (upperBound.isEmpty()) {
            return null;
        } else if (upperBound.size() == 1) {
            return upperBound.iterator().next();
        } else {
            throw new IllegalArgumentException("Ambiguous upper bound of types: "+candidateTypes);
        }
    }

    private TypeParameter getTypeParam(Port port) {
        if (!port.isGeneric()) {
            throw new IllegalArgumentException("Internal error: port is not generic: "+port);
        }
        return port.getComponent().getTypeParameter(port.getType().getName());
    }

    private boolean updateAlternativeWithIntersection(ComponentInstance ci, TypeParameter typeParam,
            Set<DataType> intersectTypes) {
        final ComponentInstanceTypeParameter key = new ComponentInstanceTypeParameter(ci, typeParam);
        Set<DataType> myAlternatives = this.alternatives.get(key);
        return myAlternatives.retainAll(intersectTypes);
    }

    private Set<DataType> getSubTypes(DataType type) {
        Set<DataType> types = this.subTypeCache.get(type);
        if (types == null) {
            types = Collections.unmodifiableSet(new HashSet<DataType>(type.getDescendants()));
            this.subTypeCache.put(type, types);
        }
        return types;
    }

    private Set<DataType> getSubTypes(TypeParameter typeParam) {
        if (typeParam.getExtendsType() == null) {
            return this.allTypesCache;
        } else {
            return getSubTypes(typeParam.getExtendsType());
        }
    }

    private Set<DataType> getSubTypes(ComponentInstance ci, Port port, boolean forcedConnection) {
        if (forcedConnection) {
            return this.allTypesCache;
        } else if (port.isGeneric()) {
            ComponentInstanceTypeParameter key = new ComponentInstanceTypeParameter(ci, getTypeParam(port));
            Set<DataType> subTypes = new HashSet<DataType>();
            for (DataType altType: this.alternatives.get(key)) {
                subTypes.addAll(getSubTypes(altType));
            }
            return subTypes;
        } else {
            return getSubTypes(port.getType());
        }
    }

    private Set<DataType> getSuperTypes(DataType type) {
        Set<DataType> types = this.superTypeCache.get(type);
        if (types == null) {
            types = Collections.unmodifiableSet(new HashSet<DataType>(type.getAncestors()));
            this.superTypeCache.put(type, types);
        }
        return types;
    }

    private Set<DataType> getSuperTypes(TypeParameter typeParam) {
        if (typeParam.getExtendsType() == null) {
            return this.allTypesCache;
        } else {
            return getSuperTypes(typeParam.getExtendsType());
        }
    }

    private Set<DataType> getSuperTypes(ComponentInstance ci, Port port, boolean forcedConnection) {
        if (forcedConnection) {
            return this.allTypesCache;
        } else if (port.isGeneric()) {
            ComponentInstanceTypeParameter key = new ComponentInstanceTypeParameter(ci, getTypeParam(port));
            Set<DataType> superTypes = new HashSet<DataType>();
            for (DataType altType: this.alternatives.get(key)) {
                superTypes.addAll(getSuperTypes(altType));
            }
            return superTypes;
        } else {
            return getSuperTypes(port.getType());
        }
    }
}
