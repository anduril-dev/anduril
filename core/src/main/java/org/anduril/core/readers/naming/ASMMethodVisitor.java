package org.anduril.core.readers.naming;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.ArrayList;
import java.util.HashMap;

public class ASMMethodVisitor extends MethodVisitor {
    private final String className;
    private final String methodName;
    private final ByteCodeNameCache nameCache;
    private final HashMap<Label, Integer> labelIndices;
    private final ArrayList<Integer> lineNumbers;
    private int curLine;
    private boolean methodIsIgnored;

    public ASMMethodVisitor(String className, String methodName, boolean classIsIgnored, ByteCodeNameCache nameCache) {
        super(Opcodes.ASM4);
        this.className = className;
        this.methodName = methodName;
        this.nameCache = nameCache;
        this.labelIndices = new HashMap<Label, Integer>();
        this.lineNumbers = new ArrayList<Integer>();
        this.curLine = -1;
        this.methodIsIgnored = classIsIgnored;
    }

    private boolean isRegularObjectReference(String typeDescriptor) {
        return typeDescriptor.startsWith("L") && !typeDescriptor.contains("$");
    }

    private int getLine(Label label) {
        int labelIndex = this.labelIndices.get(label);
        return this.lineNumbers.get(labelIndex);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        if (StackNameFinder.DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug(String.format(
                    "method %s.%s annotation: %s", this.className, this.methodName, desc));
        }

        if (StackNameFinder.IGNORE_NAME_CLASS_DESCRIPTOR.equals(desc)) {
            this.methodIsIgnored = true;
            this.nameCache.addIgnoredMethod(this.className, this.methodName);
        }
        return null;
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String fieldName, String desc) {
        if (isRegularObjectReference(desc) && this.curLine >= 0 && !fieldName.contains("$")) {
            boolean isScalaInternal = (desc.equals("Lscala/runtime/BoxedUnit;"));
            boolean isIgnored = this.methodIsIgnored || isScalaInternal;
            ByteCodeName name = new ByteCodeName(this.className, this.methodName,
                    this.curLine, this.curLine,
                    fieldName, isIgnored, true);
            this.nameCache.addName(name);

            if (StackNameFinder.DEBUG) {
                StackNameFinder.DEBUG_LOGGER.debug(String.format(
                        "field opcode=%d, name=%s desc=%s line=%d",
                        opcode, fieldName, desc, this.curLine));
            }
        }
    }

    @Override
    public void visitLabel(Label label) {
        if (StackNameFinder.DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug(String.format(
                    "label %s (I=%d) line %d",
                    label.toString(), this.labelIndices.size(), this.curLine));
        }

        this.labelIndices.put(label, this.labelIndices.size());
        this.lineNumbers.add(this.curLine);
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        if (StackNameFinder.DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug(String.format(
                    "line number %d %s",
                    line, start.toString()));
        }

        this.curLine = line;
    }

    @Override
    public void visitLocalVariable(String varName, String desc, String signature, Label start, Label end, int index) {
        final int startLine = getLine(start);
        final int exclusiveEndLine = getLine(end);
        final int inclusiveEndLine = Math.max(startLine, exclusiveEndLine - 1);

        if (StackNameFinder.DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug(String.format(
                    "variable %s desc=%s signature=%s start=%d (label=%s) end=%d (label=%s)",
                    varName, desc, signature,
                    startLine, start.toString(), exclusiveEndLine, end.toString()));
        }

        if (isRegularObjectReference(desc) && startLine >= 0 && !varName.contains("$")) {
            ByteCodeName name = new ByteCodeName(this.className, this.methodName, startLine, inclusiveEndLine,
                    varName, this.methodIsIgnored, true);
            this.nameCache.addName(name);
        }
    }
}
