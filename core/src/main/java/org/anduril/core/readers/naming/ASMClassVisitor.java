package org.anduril.core.readers.naming;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class ASMClassVisitor extends ClassVisitor {
    private final String className;
    private final ByteCodeNameCache nameCache;
    private boolean classIsIgnored;

    public ASMClassVisitor(String className, ByteCodeNameCache nameCache) {
        super(Opcodes.ASM4);
        this.className = className;
        this.nameCache = nameCache;
        this.classIsIgnored = false;
        this.nameCache.registerClass(className);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        if (StackNameFinder.DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug(String.format(
                "class %s annotation: %s", this.className, desc));
        }

        if (StackNameFinder.IGNORE_NAME_CLASS_DESCRIPTOR.equals(desc)) {
            this.classIsIgnored = true;
            this.nameCache.addIgnoredClass(this.className);
        }
        return null;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        if (StackNameFinder.DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug(String.format("parse method %s.%s", this.className, name));
        }
        return new ASMMethodVisitor(this.className, name, this.classIsIgnored, this.nameCache);
    }
}
