package org.anduril.core.readers;

import org.anduril.core.network.*;
import org.anduril.core.network.annotations.Annotation;

import java.io.File;
import java.util.*;

/**
 * Perform finalization and error checking to a network.
 */
public class NetworkFinalizer {

    final private Network network;
    final private File networkFile;

    /**
     * Initialize.
     * @param network The network that is being finalized.
     * @param networkFile The source file of the network.
     */
    public NetworkFinalizer(Network network, File networkFile) {
        this.network = network;
        this.networkFile = networkFile;
    }
    
    /**
     * Return the network that is being finalized.
     */
    public Network getNetwork() {
        return network;
    }
    
    /**
     * Return the source file of the network.
     */
    public File getNetworkFile() {
        return networkFile;
    }
    
    /**
     * Perform all finalization.
     */
    public void runFinalizer() throws StaticError {
        checkWellFormed(network);

        recursiveDisable();
        validateApplyAnnotations();
        initComponentInstances();
        inferGenericTypes();

        assignCheckParameters();
        checkParameterTypeValidity();
        checkMandatoryConnections();
        checkConnectionTypes();

        network.setFinalized(true);
    }

    /**
     * Implement M1: Recursively disable component instances that depend on
 * 		CI's that are already disabled.
     */
    private void recursiveDisable() {
        for (ComponentInstance ci: network) {
            if (!ci.isDisabled()) {
                /* Check if any virtual substituted source node is disabled */
                for (PortConnection inConn: ci.getInPortConnections()) {
                	if (!inConn.getToPort().isOptional() && Annotation.convertBoolean(ci.getInPortAnnotation(Annotation.REQUIRE, inConn.getToPort()))) {
                		if (inConn.getFrom().isDisabled()) ci.markForDisabling();
                	}
                }
            }
        }

        for (ComponentInstance ci: network) {
            if (ci.isDisabled()) ci.disable();
        }
    }
    
    /**
     * Implement M2: Run the initialize method for component instances.
     */
    private void initComponentInstances() throws StaticError {
        for (ComponentInstance ci: network) {
            if (!ci.isDisabled()) ci.initialize(network);
        }
    }
    
    /**
     * Implement M4 and E4.
     * Assign default parameter values for those parameters that don't yet
     * have a value but do have a default.
     * Check that all parameters have a value (which may be the default value).
     */
    private void assignCheckParameters() throws StaticError {
        for (ComponentInstance ci: network) {
            for (Parameter param: ci.getComponent().getParameters()) {
                Object value = ci.getParameterValue(param);
                if (value == null) {
                    String defaultValue = param.getDefaultValue();
                    if (defaultValue == null) {
                        throw new StaticError("Parameter has no value: "+param.getName(), ci);
                    }
                    else {
                        ci.setParameterValue(param, defaultValue);
                    }
                }
            }
        }
    }
    
    /** Validate and apply annotations. */
    private void validateApplyAnnotations() {
    	for (ComponentInstance ci: network) {
    		for (Map.Entry<Annotation, Object> entry: ci.getInstanceAnnotations().entrySet()) {
    			final Object value = entry.getValue();
    			entry.getKey().validate(value, ci, null);
    			entry.getKey().applyFinalization(value, ci, null);
    		}

    		for (Map.Entry<String, Map<Annotation,Object>> portEntry: ci.getInPortAnnotations().entrySet()) {
    			final InPort port = ci.getComponent().getInPort(portEntry.getKey());
    			for (Map.Entry<Annotation, Object> entry: portEntry.getValue().entrySet()) {
    				final Object value = entry.getValue();
    				entry.getKey().validate(value, ci, port);
    				entry.getKey().applyFinalization(value, ci, null);
    			}
    		}
    		
    		for (Map.Entry<String, Map<Annotation,Object>> portEntry: ci.getOutPortAnnotations().entrySet()) {
    			final OutPort port = ci.getComponent().getOutPort(portEntry.getKey());
    			for (Map.Entry<Annotation, Object> entry: portEntry.getValue().entrySet()) {
    				final Object value = entry.getValue();
    				entry.getKey().validate(value, ci, port);
    				entry.getKey().applyFinalization(value, ci, null);
    			}
    		}
    	}
    }

    /**
     * Implement E5: Check that all parameter values have the correct type.
     */
    private void checkParameterTypeValidity() throws StaticError {
        for (ComponentInstance ci: network) {
            for (Parameter param: ci.getComponent().getParameters()) {
                ParameterType ptype = param.getType();
                
                if (ptype == null) {
                    String msg = String.format("Invalid type for parameter %s: %s",
                            param.getName(), ptype);
                    throw new StaticError(msg, ci);
                }

                Object value = ci.getParameterValue(param);
                if (value == null) continue;
                String valueStr = value.toString();
                boolean ok = ptype.validate(valueStr);
                if (!ok) {
                    /* Coerce float to int by rounding */
                    if (ptype == ParameterType.INT) {
                        if (ParameterType.FLOAT.validate(valueStr)) {
                            final double doubleValue = Double.parseDouble(valueStr);
                            final int intValue = (int)Math.round(doubleValue);
                            ci.setParameterValue(param, String.valueOf(intValue));
                            ok = true;
                            if (Math.abs(doubleValue-intValue) > 0.001) {
                                ci.getLogger().info(String.format(Locale.US,
                                        "Parameter %s: Rounding float %.3f to integer %d",
                                        param.getName(),
                                        doubleValue, intValue));
                            }
                        }
                    }
                }
                
                if (!ok) {
                    String msg = String.format("Parameter %s is not a valid %s: %s",
                            param.getName(), ptype, value);
                    throw new StaticError(msg, ci);
                }
            }
        }
    }
    
    /**
     * Implement E2: Check that all mandatory input ports are connected.
     */
    private void checkMandatoryConnections() throws StaticError {
        for (ComponentInstance ci: network) {
            if (ci.isDisabled()) continue;
            for (InPort port: ci.getComponent().getInPorts()) {
                Object val = ci.getInPortAnnotation(Annotation.REQUIRE, port);
                if (val != null) {
                    Boolean bval = (Boolean)val;
                    if (port.isOptional() && !bval) continue;
                }                
                if (port.isOptional()) continue;
                if (ci.getInConnection(port) == null) {
                    String msg = String.format("Mandatory in-port %s is not connected", port.getName());
                    throw new StaticError(msg, ci);
                }
            }
        }
    }

    /**
     * Implement M3: Infer actual types for generic components.
     */
    private void inferGenericTypes() throws StaticError {
        TypeInference tInfer = new TypeInference(network);
        tInfer.inferGenericTypes();
    }
    
    /**
     * Implement E7: Check that the network object structure is well formed.
     */
    private void checkWellFormed(Network network) throws StaticError {
        for (ComponentInstance ci: network) {
            if (network.getComponentInstance(ci.getName()) != ci) {
                String msg = String.format(
                        "Internal error: Component instance %s is not found in the network when querying by name",
                        ci.getName());
                throw new StaticError(msg, ci);
            }

            for (Connection conn: ci.getInConnections(true, true)) {
                if (!conn.getFrom().hasOutConnection(ci, true, true)) {
                    String msg = String.format(
                            "Internal error: Invalid connection %s: the connection is not present in source component",
                            conn);
                    throw new StaticError(msg, ci);
                }
            }

            for (Connection conn: ci.getOutConnections(true, true)) {
                if (!conn.getTo().hasInConnection(ci, true, true)) {
                    String msg = String.format(
                            "Internal error: Invalid out connection %s for ci %s: the connection is not present in destination component",
                            conn, ci.format());
                    throw new StaticError(msg, ci);
                }
            }
        }
    }

    /**
     * Check that connections have correct types.
     */
    private void checkConnectionTypes() throws StaticError {
        for (ComponentInstance ci: this.network) {
            for (PortConnection conn: ci.getOutPortConnections()) {
                OutPort fromPort;
                try {
                    fromPort = conn.getNongenericFromPort();
                } catch (IllegalArgumentException e) {
                    throw new StaticError(e.getMessage(), conn.getFrom());
                }

                InPort toPort;
                try {
                    toPort = conn.getNongenericToPort();
                } catch (IllegalArgumentException e) {
                    throw new StaticError(e.getMessage(), conn.getTo());
                } catch (IllegalStateException e) {
                    throw new StaticError(e.getMessage(), conn.getTo());
                }

                DataType from = fromPort.getType();
                DataType to = toPort.getType();

                if (from == null && !fromPort.isSpecial()) {
                    throw new StaticError(
                            "Type is null for port "+fromPort.getName(),
                            conn.getFrom());
                }

                if (to == null && !toPort.isSpecial()) {
                    throw new StaticError(
                            "Type is null for port "+toPort.getName(),
                            conn.getTo());
                }

                if (from.isGeneric()) {
                    String msg = String.format("[ERROR]  In connection %s: generic type %s for %s is not set",
                            conn, from.getName(), conn.getFrom().getName());
                    throw new StaticError(msg, conn.getFrom());
                }

                if (to.isGeneric()) {
                    String msg = String.format("[ERROR]  In connection %s: generic type %s for %s is not set",
                            conn, to.getName(), conn.getTo().getName());
                    throw new StaticError(msg, conn.getTo());
                }

                if (!conn.isForced()) {
                    if (!conn.getFrom().getPortIsArray(fromPort).isCompatible(conn.getTo().getPortIsArray(toPort))) {
                        String msg = String.format("[ERROR]  In connection %s: connection between array and non-array",
                                conn);
                        throw new StaticError(msg, conn.getTo());
                    }

                    if (!from.isSubTypeOf(to)) {
                        String msg = String.format(
                                "[ERROR]  Port type mismatch: %s.%s has type %s but %s.%s has type %s",
                                conn.getFrom().getName(), conn.getFromPort().getName(), from.getName(),
                                conn.getTo().getName(), conn.getToPort().getName(), to.getName());
                        throw new StaticError(msg, conn.getTo());
                    }
                }
            }
        }
    }
}
