package org.anduril.core.readers;

import org.anduril.core.network.Bundle;
import org.anduril.core.network.Repository;
import org.anduril.core.utils.XMLTools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class BundleReader {
    private File file;
    private Repository repository;
    private Document doc;
    private Element root;
    
    /**
     * Create a parser from file.
     * @param file Source XML file.
     * @param repository The repository where parse errors are added. 
     */
    public BundleReader(File file, Repository repository) throws ParserConfigurationException,
    SAXException, IOException {
        DocumentBuilder builder = XMLTools.getDocumentBuilder(null, null);
        this.file = file;
        File bundleDir = file.getParentFile().getAbsoluteFile();
        if (!bundleDir.exists()) {
            throw new IOException("Bundle directory does not exist: "+bundleDir.getAbsolutePath());
        }
        
        this.repository = repository;
        this.doc = builder.parse(file);
        this.root = doc.getDocumentElement();
    }

    /**
     * Create a parser from URL.
     * @param url Source URL for the XML file.
     * @param repository The repository where parse errors are added. 
     */
    public BundleReader(URL url, Repository repository) throws ParserConfigurationException,
    SAXException, IOException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        this.file = null;
        this.repository = repository;
        this.doc = builder.parse(url.toExternalForm());
        this.root = doc.getDocumentElement();
    }

    public Bundle parse() {
        File bundleDir = file.getParentFile().getAbsoluteFile();
        String name = XMLTools.getSingleChildContents(root, "name");
        Bundle bundle = new Bundle(bundleDir, name);

        for (String bundleName: XMLTools.getChildrenContents(root, "depends")) {
            bundle.addDependsBundle(bundleName);
        }

        for (String jar: XMLTools.getChildrenContents(root, "imports")) {
            bundle.addImportsJARs(jar);
        }

        for (String issues: XMLTools.getChildrenContents(root, "issues")) {
            bundle.setIssues(issues);
        }

        return bundle;
    }
}
