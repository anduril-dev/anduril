package org.anduril.core.readers;

import org.anduril.core.network.*;
import org.anduril.core.network.launcher.*;
import org.anduril.core.utils.Logger;
import org.anduril.core.utils.XMLTools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Parse a component XML file and add an instance of Component
 * to the repository.
 */
public class ComponentReader implements ErrorHandler {
	public static final String RELEVANCE_AUTHOR = "author";
	public static final String RELEVANCE_CREDIT = "credit";

    private File file;
    private Repository repository;
    private Element root;
    private Component comp;
    private Logger logger;
    
    /**
     * Create reader from a file.
     * @param file Source XML file.
     * @param repository Repository to which the components (or parse errors)
     * 	are added. 
     */
    public ComponentReader(File file, Repository repository) throws ParserConfigurationException,
            SAXException, IOException {
        this.file = file;
        this.repository = repository;
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        Document doc = builder.parse(file);
        this.root = doc.getDocumentElement();
        this.logger = Logger.getLogger("<component-xml-reader>");
    }

    protected String getSchemaLocation() {
        String xsd = "/component.xsd";
        File full = new File(repository.getHomeDirectory(), "/core/src/main/resources" +xsd);
        return full.getAbsolutePath();
    }
    
    /**
     * Return the path of the source XML file, or null if the
     * reader was created from a stream.
     */
    public File getFile() {
        return file;
    }
    
    /**
     * Return the repository to which the components (or parse errors)
     * are added. 
     */
    public Repository getRepository() {
        return repository;
    }

    /**
     * Return the root element of the DOM tree.
     */
    public Element getDocumentRoot() {
        return root;
    }
    
    /**
     * Return the component that was created when parsing. Before
     * <code>parse</code> has been called, this is null.
     */
    protected Component getComponent() {
        return comp;
    }
    
    protected void setDirectory() {
        if (file == null) return;
        comp.setMainDirectory(file.getParentFile());
    }
    
    protected void setComponent(Component comp) {
        this.comp = comp;
    }
    
    protected static String getDoc(Element element) {
        Element docElem = XMLTools.getChildByName(element, "doc");
        return (docElem == null) ? null : XMLTools.getElementContentsAsString(docElem);
    }
    
    protected static String getDeprecated(Element element) {
        Element docElem = XMLTools.getChildByName(element, "deprecated");
        return (docElem == null) ? null : XMLTools.getElementContentsAsString(docElem);
    }
    
    /**
     * Parse XML file and return the component object. Parse errors
     * are added to the network.
     */
    @SuppressWarnings("unchecked")
	public Component parse() throws StaticError {
        String name = XMLTools.getSingleChildContents(root, "name");
        String doc = getDoc(root);
        String deprecated = getDeprecated(root);
        String version = XMLTools.getSingleChildContents(root, "version");
        
        List<Requirement> reqs;
        try {
            reqs = parseRequirements(root);
        } catch (IllegalArgumentException e) {
            throw new StaticError(e.getMessage(), new SourceLocation(file));
        }
        
        List<Author> authors = parseAuthoring(root, RELEVANCE_AUTHOR);
        List<Author> credits = parseAuthoring(root, RELEVANCE_CREDIT);
        String[] categories = parseCategories(root, name);
        comp = new Component(name, doc, version, categories, authors, credits, reqs, new SourceLocation(file));
        if (deprecated != null) comp.setDeprecated(deprecated); // If element exists in component.xml, add contents.
        setDirectory();

        String className = XMLTools.getSingleChildContents(root, "instance-class");
        if (className != null) {
            className = className.trim();
            ClassLoader cl = ComponentReader.class.getClassLoader();
            Class<? extends ComponentInstance> instanceClass;
            try {
                instanceClass = (Class<? extends ComponentInstance>) cl.loadClass(className);
                comp.setInstanceClass(instanceClass);
            } catch(ClassNotFoundException e) {
                throw new StaticError("Invalid instance class name: "+className,
                        comp);
            } catch(NoClassDefFoundError e) {
                throw new StaticError("Instance class ("+className+") not available: "+
                                                    e.getMessage()+" not found.",
                        comp);
            }
        }

        parseLaunchers(root);
        parseTypeParameters(root);
        parsePorts(root);
        parseParams(root);
        addDocResources();
        addTestCases();
        comp.addSpecialPorts(repository);

        return comp;
    }
    
    private String getAttribute(Element element, String name) {
        String value = element.getAttribute(name);
        return value.isEmpty() ? null : value;
    }
    
    protected List<Requirement> parseRequirements(Element componentRoot) throws StaticError {
        List<Requirement> reqs = new ArrayList<Requirement>();
        
        for (Element el: XMLTools.getChildrenByName(componentRoot, "requires")) {
            String name = getAttribute(el, "name");
            String url = getAttribute(el, "URL");
            String version = getAttribute(el, "version");
            boolean optional = el.getAttribute("optional").equals("true");
            final Requirement req = new Requirement(name, url, version, optional);
            
            String inlineType = getAttribute(el, "type");
            if (inlineType == null) {
                for (Element resourceEl: XMLTools.getChildrenByName(el, "resource")) {
                    String type = getAttribute(resourceEl, "type");
                    String contents = XMLTools.getElementContentsAsString(resourceEl);
                    req.addResource(new RequirementResource(req, type, contents));
                }

                if (req.getResources().isEmpty()) {
                    String inlineContents = XMLTools.getElementContentsAsString(el).trim();
                    if (!inlineContents.isEmpty()) {
                        req.addResource(new RequirementResource(req, Requirement.REQ_MANUAL, inlineContents));
                    }
                }
            } else {
                /* Inline format */
                String contents = XMLTools.getElementContentsAsString(el);
                req.addResource(new RequirementResource(req, inlineType, contents));
            }

            if (req.getResources().isEmpty()) {
                throw new StaticError("Invalid requirement: no resources defined", new SourceLocation(file));
            }
            reqs.add(req);
        }
        
        return reqs;
    }

    // relevance may be instantiated to "author" or to "credit".
    protected List<Author> parseAuthoring(Element componentRoot, String relevance) {
        List<Author> authors = new ArrayList<Author>();
        
        for (Element el: XMLTools.getChildrenByName(componentRoot, relevance)) {
            String email = el.getAttribute("email");
            if (email.isEmpty()) email = null;
            String name = XMLTools.getElementContentsAsString(el);
            authors.add(new Author(name, email, repository));
        }
        
        return authors;
    }
    
    protected String[] parseCategories(Element componentRoot, String componentName) {
        List<String> categoriesTmp = XMLTools.getChildrenContents(root, "category");
        List<String> categories = new ArrayList<String>();
        
        if (categoriesTmp != null) {
            for (String category: categoriesTmp) {
                if (category == null) continue;
                Category cat = repository.getCategory(category); 
                if (cat == null) {
// TODO: revise categories with new packaging system.
//                    repository.getEnv().warning(String.format(
//                            "Warning: Component %s: category %s is not declared in categories.xml",
//                            componentName, category));
                } else {
                	if (!cat.getName().equals(category)) {
                	    this.logger.warning(String.format(
                                "Warning: Component %s: category %s has a wrong character case.",
                                componentName, category));
                	    category = cat.getName();
                	}
                    categories.add(category);
                }
            }
        }

        return categories.toArray(new String[categories.size()]);
    }
    
    protected void parseLaunchers(Element componentRoot) throws StaticError {
        for (Element el: XMLTools.getChildrenByName(componentRoot, "launcher")) {
            String type = el.getAttribute("type").toLowerCase();
            Map<String,String> args = new TreeMap<String,String>();
            
            for (Element argElement: XMLTools.getChildrenByName(el, "argument")) {
                String name = argElement.getAttribute("name");
                String value = argElement.getAttribute("value");
                String oldValue = args.get(name);
                if (oldValue != null) value = oldValue+","+value;
                args.put(name, value);
            }
            
            Launcher launcher = null;
            /* XXX: make this modular */
            if (type.equals("bash")) {
                launcher = new BashLauncher(type, args, comp);
            } else if (type.equals("java")) {
                launcher = new JavaLauncher(type, args, comp);
            } else if (type.equals("r")) {
                launcher = new RLauncher(type, args, comp);
            } else if (type.equals("python")) {
                launcher = new PythonLauncher(type, args, comp);
            } else if (type.equals("python3")) {
                launcher = new Python3Launcher(type, args, comp);
            } else if (type.equals("perl")) {
                launcher = new PerlLauncher(type, args, comp);
            } else if (type.equals("matlab")) {
                launcher = new MatlabLauncher(type, args, comp);
            } else if (type.equals("octave")) {
                launcher = new OctaveLauncher(type, args, comp);
            } else if (type.equals("lua")) {
                launcher = new LuaLauncher(type, args, comp);
            } else if (type.equals("scala")) {
                JavaLauncher javaLauncher = new JavaLauncher(type, args, comp);
                javaLauncher.setIsScalaLauncher(true);
                launcher = javaLauncher;
            } else {
                throw new StaticError("Unknown launcher type: "+type,
                        comp);
            }
            
            if (launcher != null) comp.addLauncher(launcher);
        }
    }
    
    protected void parseTypeParameters(Element componentRoot) throws StaticError {
        int pos = 1;
        for (Element el: XMLTools.getElementsByTagName(componentRoot, "type-parameter")) {
            String name = el.getAttribute("name");
            
            String extendsValue = el.getAttribute("extends");
            DataType extendsType = null;
            if (extendsValue != null && !extendsValue.isEmpty()) {
                extendsType = repository.getDataType(extendsValue);
                if (extendsType == null) {
                    String msg = String.format("Type parameter %s: unknown extends type %s",
                            name, extendsValue);
                    throw new StaticError(msg, comp);
                }
            }
            
            comp.addTypeParameter(new TypeParameter(name, pos, extendsType));
            pos++;
        }
    }
    
    private ArrayStatus parseArrayDefinition(String arrayAttr) throws StaticError {
        if (arrayAttr == null || arrayAttr.isEmpty()) {
            return ArrayStatus.FALSE;
        }
        
        if (arrayAttr.equals("true")) return ArrayStatus.TRUE;
        else if (arrayAttr.equals("false")) return ArrayStatus.FALSE;
        else if (arrayAttr.equals("generic")) return ArrayStatus.GENERIC;
        else {
            throw new StaticError("Invalid 'array' attribute value: "+arrayAttr, comp);
        }
    }
    
    protected void parsePorts(Element componentRoot) throws StaticError {
        String name, typeString, doc;
        DataType type;
        boolean optional;

        int pos = 1;
        boolean seenOptional = false;
        for (Element el: XMLTools.getElementsByTagName(componentRoot, "input")) {
            name = el.getAttribute("name");
            doc = getDoc(el);
            
            typeString = el.getAttribute("type");
            if (comp.getTypeParameter(typeString) != null) {
                type = DataType.buildGenericType(typeString);
            } else {
                type = repository.getDataType(typeString);
            }
            if (type == null) {
                throw new StaticError("Unknown input data type: "+typeString, comp);
            }
            if (!typeString.equals(type.getName())) {
                this.logger.warning(
            	    String.format("Warning: %s has an invalid character case in input data type: %s",
            	                  comp.getName(), typeString));
            }
            
            ArrayStatus isArray = parseArrayDefinition(el.getAttribute("array"));
            if (isArray == null) continue;

            optional = el.getAttribute("optional").equals("true");
            if (optional) seenOptional = true;
            else if (seenOptional) {
                String msg = String.format("Input port %s: all mandatory ports must come before optional ports",
                        name);
                throw new StaticError(msg, comp);
            }
            
            InPort port = new InPort(comp, name, pos, doc, isArray, type, optional);
            
            String streamableStr = el.getAttribute("streamable");
            if (!streamableStr.isEmpty()) {
                port.setStreamable("true".equals(streamableStr));
            }
            
            comp.addPort(port);
            pos++;
        }

        pos = 1;
        for (Element el: XMLTools.getElementsByTagName(componentRoot, "output")) {
            name = el.getAttribute("name");
            
            typeString = el.getAttribute("type");
            if (comp.getTypeParameter(typeString) != null) {
                type = DataType.buildGenericType(typeString);
            } else {
                type = repository.getDataType(typeString);
            }
            if (type == null) {
                throw new StaticError("Unknown output data type: "+typeString, comp);
            }
            if (!typeString.equals(type.getName())) {
                this.logger.warning(
            	    String.format("Warning: %s has an invalid character case in output data type: %s",
            	                  comp.getName(), typeString));
            }
            
            ArrayStatus isArray = parseArrayDefinition(el.getAttribute("array"));
            if (isArray == null) continue;
            
            doc = getDoc(el);
            OutPort port = new OutPort(comp, name, pos, doc, isArray, type);
            
            String streamableStr = el.getAttribute("streamable");
            if (!streamableStr.isEmpty()) {
                port.setStreamable("true".equals(streamableStr));
            }
            
            comp.addPort(port);
            pos++;
        }
    }
    
    protected void parseParams(Element componentRoot) throws StaticError {
        for (Element el: XMLTools.getElementsByTagName(componentRoot, "parameter")) {
            String name = el.getAttribute("name");
            String typeStr = el.getAttribute("type");
            String defaultValue = null;
            if (el.hasAttribute("default")) defaultValue = el.getAttribute("default");
            String doc = getDoc(el);
            ParameterType type = ParameterType.fromString(typeStr);
            if (type == null) {
            	throw new StaticError("Unknown parameter type: "+typeStr, comp);
            }
            comp.addParameter(new Parameter(name, type, doc, defaultValue));
        }		
    }

    protected void addDocResources() {
        if (file != null) {
            File docResourceDir = new File(file.getParentFile(), Network.DOC_RESOURCES_DIR);
            if (docResourceDir.exists()) {
                this.logger.debug("Registering documentation resource dir "+docResourceDir.getPath());
                for (File docResource: docResourceDir.listFiles()) {
                    comp.addDocResource(docResource);	
                }
            }
        }
    }
    
    protected void addTestCases() throws StaticError {
        File caseDir = comp.getTestCaseDirectory();
        if (!caseDir.exists()) return;
        
        for (File f: comp.getTestCaseDirectory().listFiles()) {
            if (f.isDirectory() && !f.getName().startsWith(".")) {
                ComponentTestCase testCase;
                try {
                    testCase = new ComponentTestCase(f, repository, comp);
                } catch(IOException e) {
                    String msg = String.format("Error adding test case directory %s: %s", f, e);
                    throw new StaticError(msg, comp);
                }
                comp.addTestCase(testCase);
            }
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        throw e;
    }
}
