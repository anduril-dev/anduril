package org.anduril.core.readers;

import org.anduril.core.network.Component;
import org.anduril.core.network.Repository;
import org.anduril.core.utils.XMLTools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.*;

/**
 * This class provides services for reading from a temporal xml formatted 
 * test cases report file, parses it into a standard form, and provides similar 
 * services as the class repository for writing test cases reports through an 
 * Apache Velocity type of formatter.
 */

public class TestReportReader implements ErrorHandler {

    private String[] internalCompsNames = new String[]{
            "INPUT", "OUTPUT", "Pause", "SystemDiagnostics"};

    private Document doc;
    private Element root;
    private List<TestReportItem> theTRItems = new ArrayList<TestReportItem>();
    private List<Component> theTestlessComps = new ArrayList<Component>();
    private String theStartDate = "";
    private String theStartTime = "";
    private String theEndDate = "";
    private String theEndTime = "";
    private double theElaspedSecsSum = 0D;
    
    private long thePassedComps = 0L;
    private long theTotalComps = 0L;
    private long thePassedTests = 0L;
    private long theInactiveTests = 0L;
    private long theTotalTests = 0L;
    
    private double theMeanInACI = 0D;
    private double theMeanOutACI = 0D;
    private double theMeanPCover = 0D;
    
    private DecimalFormat df = new DecimalFormat("#.##");
    
        
    /**
     * Create a parser from file.
     * @param file Source XML file.
     * 	errors are added. 
     */
    public TestReportReader(List<Component> aTestlessComps, File file) throws ParserConfigurationException,
    SAXException, IOException {
        if (aTestlessComps != null) theTestlessComps = filterTestlessComponents(aTestlessComps);
    	
/*    	Log.config("Input xml file: " + file.getAbsolutePath());*/
    	
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        this.doc = builder.parse(file);
        this.root = doc.getDocumentElement();
        
        theStartDate = root.getAttribute("startdate");        
        theStartTime = root.getAttribute("starttime");
        theEndDate = root.getAttribute("enddate");
        theEndTime = root.getAttribute("endtime");
                
        for (Element el: XMLTools.getChildrenByName(root, "component")) {
            TestReportItem myTRItem = new TestReportItem(el);
            theTRItems.add(myTRItem);
            
            if (myTRItem.isPassed()) thePassedComps += 1;
            theTotalComps += 1;
            thePassedTests += myTRItem.getPassed().size();
            theInactiveTests += myTRItem.getInactive().size();
            theTotalTests += myTRItem.getPassed().size() 
                    + myTRItem.getFailed().size() 
                    + myTRItem.getInactive().size();
            
            theMeanInACI += myTRItem.getInACIVal();
            theMeanOutACI += myTRItem.getOutACIVal();
            theMeanPCover += myTRItem.getPCoverVal();
            theElaspedSecsSum += myTRItem.getElapsedSecs();
        }
        
        if (theTotalComps > 0) {
	        theMeanInACI = theMeanInACI /(double)theTotalComps;
	        theMeanOutACI = theMeanOutACI/ (double)theTotalComps;
	        theMeanPCover = theMeanPCover / (double)theTotalComps;
        }
    }

    private List<Component> filterTestlessComponents(List<Component> aTestlessComps) {
        List<Component> myComps = new ArrayList<Component>();
        if (aTestlessComps == null) return myComps;

        Set<String> internalComps = new HashSet<String>(Arrays.asList(internalCompsNames));        
        for (Component myComp : aTestlessComps) {
            if (!internalComps.contains(myComp.getName())) myComps.add(myComp);
        }

        return myComps;
    }

    protected String getSchemaLocation() {
        String full = "/component-report.xsd";
        URL url = Repository.class.getResource(full);
        if (url == null) {
/*            Log.warning("Warning: schema of report-tmp.xml not found");*/
            return null;
        }
        return url.toString();
    }
    
    public List<Component> getTestlessComponents() {
    	return theTestlessComps;
    }
    
    /**
     * Parse component test cases XML files.
     */
    public Map<String,TestReportItem> parse() {
    	Map<String,TestReportItem> myTRItems = new HashMap<String,TestReportItem>();
        
        for (Element el: XMLTools.getChildrenByName(root, "component")) {
        	String myName = XMLTools.getSingleChildContents(el, "name");
            TestReportItem myTRItem = new TestReportItem(el);
            myTRItems.put(myName, myTRItem);
        }
        
        return myTRItems;
    }
    
    public String getFractionPassedComps() {
        String msg = String.format(("%s / %s"), thePassedComps, theTotalComps);
        return msg;
    }
    
    public String getFractionPassedTests() {
        String msg = String.format(("%s / %s"), thePassedTests, theTotalTests);
        return msg;
    }

    public String getFractionInactiveTests() {
        String msg = String.format(("%s / %s"), theInactiveTests, theTotalTests);
        return msg;
    }

    public String getMeanInACI() {
        return df.format(theMeanInACI);
    }

    public String getMeanOutACI() {
        return df.format(theMeanOutACI);
    }

    public String getMeanPCover() {
        return df.format(theMeanPCover);
    }

    public List<TestReportItem> getReportList() {
        return theTRItems;
    }
    
    public String getStartDate() {
    	return theStartDate;
    }
    
    public String getStartTime() {
    	return theStartTime;
    }
    
    public String getEndDate() {
    	return theEndDate;
    }
    
    public String getEndTime() {
    	return theEndTime;
    }
    
    public String getElaspedSecsSum() {
		return String.format(Locale.US, "%.1f", theElaspedSecsSum);
    }
    
    public String getElaspedSecsTestCasesMean() {
		return String.format(Locale.US, "%.1f", theElaspedSecsSum / (theTotalTests - theInactiveTests));
    }
    
    public List<TestReportItem> getPassedReportList() {
        List<TestReportItem> myTRItems = new ArrayList<TestReportItem>();
        
        for (Element el: XMLTools.getChildrenByName(root, "component")) {
            TestReportItem myTRItem = new TestReportItem(el);
            if (myTRItem.isPassed()) myTRItems.add(myTRItem);
        }
        
        return myTRItems;
    }
    
    public List<TestReportItem> getFailedReportList() {
        List<TestReportItem> myTRItems = new ArrayList<TestReportItem>();
        
        for (Element el: XMLTools.getChildrenByName(root, "component")) {
            TestReportItem myTRItem = new TestReportItem(el);
            if (!myTRItem.isPassed() && !myTRItem.isInactive()) { 
                myTRItems.add(myTRItem); 
            }
        }
        
        return myTRItems;
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        throw e;
    }

}
