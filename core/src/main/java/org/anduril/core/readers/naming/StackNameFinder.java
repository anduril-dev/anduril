package org.anduril.core.readers.naming;

import org.anduril.core.network.SourceLocation;
import org.anduril.core.readers.networkParser.NetworkHandler;
import org.anduril.core.utils.Logger;
import org.anduril.runtime.IgnoreName;
import org.objectweb.asm.ClassReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class StackNameFinder {
    public static final boolean DEBUG = false;

    public static final Logger DEBUG_LOGGER = Logger.getLogger("<jar-parser>");

    private static final StackNameFinder instance = new StackNameFinder();

    public static final StackNameFinder getInstance() {
        return instance;
    }

    public static final String IGNORE_NAME_CLASS_DESCRIPTOR =
            "L" + IgnoreName.class.getName().replace('.', '/') + ";";

    private final ByteCodeNameCache nameCache = new ByteCodeNameCache();

    public String[] getStackName() throws IOException {
        if (DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug("getStackName");
        }

        ArrayList<String> nameTokens = new ArrayList<String>();
        boolean isFirstNonIgnoredName = true;
        boolean firstNameIsAccurate = false;

        for (StackTraceElement frame: Thread.currentThread().getStackTrace()) {
            final String className = frame.getClassName();
            if (isSystemClass(className)) continue;

            if (DEBUG) {
                StackNameFinder.DEBUG_LOGGER.debug(String.format("frame: class=%s method=%s line=%d",
                        frame.getClassName(), frame.getMethodName(), frame.getLineNumber()));
            }

            final ByteCodeName name = getName(className, frame.getMethodName(), frame.getLineNumber());

            if (DEBUG) {
                StackNameFinder.DEBUG_LOGGER.debug("found "+name);
            }

            if (!name.isIgnored()) {
                if (isFirstNonIgnoredName) {
                    firstNameIsAccurate = name.isAccurate();
                    isFirstNonIgnoredName = false;
                    nameTokens.add(name.getName());
                } else {
                    if (name.getName() != null) {
                        nameTokens.add(name.getName());
                    }
                }
            }
        }

        // Construct array in reverse order
        String[] tokenArray = new String[nameTokens.size()];
        final int n = nameTokens.size();
        for (int i=0; i<n; i++) {
            tokenArray[n-i-1] = nameTokens.get(i);
        }
        if (!firstNameIsAccurate && n > 0) {
            tokenArray[n-1] = null;
        }

        return tokenArray;
    }

    public SourceLocation getSourceLocation() throws IOException {
        for (StackTraceElement frame: Thread.currentThread().getStackTrace()) {
            final String className = frame.getClassName();
            if (isSystemClass(className)) continue;

            final ByteCodeName name = getName(className, frame.getMethodName(), frame.getLineNumber());
            if (!name.isIgnored()) {
                // First non-ignored name gives the source location
                File sourceFile;
                if (frame.getFileName() != null) {
                    sourceFile = new File(frame.getFileName());
                    if (!sourceFile.isAbsolute()) {
                        File parent = NetworkHandler.getNetwork().getDataDir();
                        if (parent != null) {
                            sourceFile = new File(parent, sourceFile.getName());
                        }
                    }
                } else {
                    sourceFile = null;
                }
                return new SourceLocation(sourceFile, name.getClassName(), name.getMethod(), name.getStartLine(), -1);
            }
        }

        return null;
    }

    private boolean isSystemClass(String className) {
        return
                className.startsWith("java.") ||
                className.startsWith("jdk.") ||
                className.startsWith("scala.") ||
                className.startsWith("sun.") ||
                className.startsWith("org.anduril.core.") ||
                className.startsWith("org.anduril.runtime.") ||
                className.startsWith("com.intellij.rt.execution.") ||
                className.startsWith("org.scalatest.");
    }

    private ByteCodeName getName(String className, String method, int line) throws IOException {
        if (!this.nameCache.containsClass(className)) {
            parseClass(className);
        }
        return this.nameCache.getName(className, method, line);
    }

    private void parseClass(String className) throws IOException {
        if (DEBUG) {
            StackNameFinder.DEBUG_LOGGER.debug("parse class "+className);
        }

        ClassReader reader;
        try {
            reader = new ClassReader(className);
        } catch (IOException e) {
            try {
                ClassLoader classLoader = Class.forName(className).getClassLoader();
                String normalizedName = className.replace('.', '/') + ".class";
                reader = new ClassReader(classLoader.getResourceAsStream(normalizedName));
            } catch (ClassNotFoundException e2) {
                throw new IOException("Class not found: "+className, e);
            }
        }

        reader.accept(new ASMClassVisitor(className, this.nameCache), 0);
    }
}
