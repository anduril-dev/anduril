package org.anduril.core.readers.naming;

import java.util.HashMap;
import java.util.HashSet;

public class ByteCodeNameCache {
    /** className -> (method -> (line -> variable)) */
    private final HashMap<String, HashMap<String, HashMap<Integer, ByteCodeName>>> classes;

    /** Set of class names that are annotated with IgnoreName. */
    private final HashSet<String> ignoredClasses;

    private final HashMap<String, HashSet<String>> ignoredMethods;

    public ByteCodeNameCache() {
        this.classes = new HashMap<String, HashMap<String, HashMap<Integer, ByteCodeName>>>();
        this.ignoredClasses = new HashSet<String>();
        this.ignoredMethods = new HashMap<String, HashSet<String>>();
    }

    public boolean containsClass(String className) {
        return this.classes.containsKey(className);
    }

    public void registerClass(String className) {
        getClassMap(className); // Result ignored
    }

    private HashMap<String, HashMap<Integer, ByteCodeName>> getClassMap(String className) {
        HashMap<String, HashMap<Integer, ByteCodeName>> classMap = this.classes.get(className);
        if (classMap == null) {
            classMap = new HashMap<String, HashMap<Integer, ByteCodeName>>();
            this.classes.put(className, classMap);
        }
        return classMap;
    }

    private String quoteMethodName(String method) {
        return method.replace('<', '_').replace('>', '_').replace('$', '_');
    }

    private HashMap<Integer, ByteCodeName> getMethodMap(String className, String methodName) {
        HashMap<String, HashMap<Integer, ByteCodeName>> classMap = getClassMap(className);
        HashMap<Integer, ByteCodeName> methodMap = classMap.get(methodName);
        if (methodMap == null) {
            methodMap = new HashMap<Integer, ByteCodeName>();
            classMap.put(methodName, methodMap);
        }
        return methodMap;
    }

    public void addIgnoredClass(String className) {
        this.ignoredClasses.add(className);
    }

    public void addIgnoredMethod(String className, String methodName) {
        HashSet<String> methodSet = this.ignoredMethods.get(className);
        if (methodSet == null) {
            methodSet = new HashSet<String>();
            this.ignoredMethods.put(className, methodSet);
        }
        methodSet.add(methodName);
    }

    public boolean isClassOrMethodIgnored(String className, String methodName) {
        if (this.ignoredClasses.contains(className)) {
            return true;
        }
        HashSet<String> methodSet = this.ignoredMethods.get(className);
        if (methodSet != null) {
            return methodSet.contains(methodName);
        }
        return false;
    }

    public void addName(ByteCodeName name) {
        HashMap<Integer, ByteCodeName> methodMap = getMethodMap(name.getClassName(), name.getMethod());
        methodMap.put(name.getStartLine(), name);
    }

    public ByteCodeName getName(String className, String method, int line) {
        HashMap<Integer, ByteCodeName> methodMap = getMethodMap(className, method);
        ByteCodeName name = methodMap.get(line);
        if (name == null) {
            boolean ignored = isClassOrMethodIgnored(className, method) || line < 0;
            return new ByteCodeName(className, method, line, line,
                    quoteMethodName(method), ignored, false);
        } else {
            return name;
        }
    }
}
