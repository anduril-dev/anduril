package org.anduril.core.readers;

import org.anduril.core.Version;
import org.anduril.core.network.*;
import org.anduril.core.utils.XMLTools;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Parse a data type XML files and add instances of DataType to
 * the network. There may be several type declarations in one
 * XML file.
 */
public class DataTypeReader implements ErrorHandler {
    private File file;
    private Repository repository;
    private Document doc;
    private Element root;
    private Bundle bundle;

    /**
     * Create a parser from file.
     * @param file Source XML file.
     * @param repository The repository where data types and parse
     * 	errors are added.
     */
    public DataTypeReader(File file, Repository repository, Bundle bundle) throws ParserConfigurationException,
    SAXException, IOException {
        this.file = file;
        this.repository = repository;
        this.bundle = bundle;
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        this.doc = builder.parse(file);
        this.root = doc.getDocumentElement();
    }

    /**
     * Create a parser from URL.
     * @param url Source URL for the XML file.
     * @param repository The repository where data types and parse
     * 	errors are added.
     */
    public DataTypeReader(URL url, Repository repository, Bundle bundle) throws ParserConfigurationException,
    SAXException, IOException {
        DocumentBuilder builder = XMLTools.getDocumentBuilder(getSchemaLocation(), this);
        this.file = null;
        this.repository = repository;
        this.bundle = bundle;
        this.doc = builder.parse(url.toExternalForm());
        this.root = doc.getDocumentElement();
    }

    protected String getSchemaLocation() {
        String xsd = "/datatypes.xsd";
        File full = new File(repository.getHomeDirectory(), "/core/src/main/resources" +xsd);
        return full.getAbsolutePath();
    }

    /**
     * Parse data type XML files and add instance of DataType to the network.
     * @return The data types that were read.
     */
    @SuppressWarnings("unchecked")
	public Map<String,DataType> parse() throws StaticError {
        File docResource = null;
        if (file != null) {
            docResource = new File(file.getParentFile(), Network.DOC_RESOURCES_DIR);
            if (!docResource.exists()) docResource = null;
        }
        
        Map<String,DataType> types = new HashMap<String,DataType>();
        
        for (Element el: XMLTools.getChildrenByName(root, "type")) {
            String name = XMLTools.getSingleChildContents(el, "name");
            
            String parentName = XMLTools.getSingleChildContents(el, "parent-type");
            DataType parentType;
            if (parentName == null) {
                if (name.equals(DataType.TOP_TYPE_NAME)) {
                    parentType = null;
                } else {
                    parentType = repository.getDataType(DataType.TOP_TYPE_NAME);
                }
            } else {
                parentType = repository.getDataType(parentName);
                if (parentType == null) {
                    throw new StaticError("Parent data type (" +
                            parentName + ") not found for " + name+ '.');
                }
            }
            
            String desc = null;
            Element docElem = XMLTools.getChildByName(el, "desc");
            if (docElem != null) desc = XMLTools.getElementContentsAsString(docElem);

            PresentationType ptype = convPresentationType(
                    XMLTools.getSingleChildContents(el, "presentation-type"));

            String extension = XMLTools.getSingleChildContents(el, "extension");
            
            String presentationDesc = null;
            Element presentationDocElem = XMLTools.getChildByName(el, "presentation-desc");
            if (presentationDocElem != null) {
                presentationDesc = XMLTools.getElementContentsAsString(presentationDocElem);
            }
            
            Version version = null;
            String versionStr = XMLTools.getSingleChildContents(el, "version");
            if (versionStr != null) version = Version.parse(versionStr);
            
            DataType type = new DataType(name, parentType, desc, ptype, presentationDesc, extension, version, false);
            type.setBundle(this.bundle);

            for (Element exampleElement: XMLTools.getChildrenByName(el, "example-file")) {
                if (docResource == null) {
                    throw new StaticError("Data type has an example file but no doc-files resource dir exists: "+name);
                }
                String filename = exampleElement.getAttribute("path");
                File exampleFile = new File(new File(docResource, name), filename);
                if (!exampleFile.exists()) {
                    String msg = String.format("Example file for type %s not found: %s", name, filename);
                    throw new StaticError(msg);
                }
                type.addExampleFile(exampleFile);
            }
            
            String className = XMLTools.getSingleChildContents(el, "functionality-class");
            if (className != null) {
                ClassLoader cl = DataTypeReader.class.getClassLoader();
                Class<? extends DataTypeFunctionality> functionalityClass;
                try {
                    functionalityClass = (Class<? extends DataTypeFunctionality>) cl.loadClass(className);
                    type.setFunctionalityClass(functionalityClass);
                } catch(ClassNotFoundException e) {
                    throw new StaticError("Invalid functionality class name: "+className,
                            new SourceLocation(file));
                }
            }
            
            types.put(name, type);
            repository.addDataType(type);
        }
        
        return types;
    }

    private PresentationType convPresentationType(String ptype) throws StaticError {
        if (ptype.equals("file")) return PresentationType.FILE;
        else if (ptype.equals("directory")) return PresentationType.DIRECTORY;
        else if (ptype.equals("multifile")) return PresentationType.MULTIFILE;
        else {
            throw new StaticError("Invalid presentation type: "+ptype,
                    new SourceLocation(file));
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        throw e;
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        throw e;
    }
    
}
