package org.anduril.core.readers;

import org.anduril.core.network.*;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Read a properties file. Properties files are used to set
 * component parameters.
 */
public class PropertiesReader {
    
    /**
     * Read metadata in a properties file.
     * @param network The network that is used to store results.
     * @param input Properties filename.
     * @return HashMap of meta-parameter name and value cast to String.
     */
    public static Map<String,String> readMetadata(Network network, File input) throws IOException {
        return read(network, input, true, null);
    }
    
    /**
     * Read a properties file and update the component instances.
     * @param network The network that is used to store results.
     * @param input Properties filename.
     * @return HashMap of meta-parameter name and value cast to String.
     */
    public static Map<String,String> read(Network network, File input, String ciName) throws IOException {
        return read(network, input, false, ciName);
    }

    private static Map<String,String> read(Network network, File input, boolean isMetadata, String ciName) throws IOException {
        Properties prop = new Properties();
        Map<String,String> metaParams = null;
        Reader reader;
        final SourceLocation location = new SourceLocation(input);

        reader = new FileReader(input);
        prop.load(reader);

        metaParams = new HashMap<String,String>();
        for (Enumeration<?> enumer = prop.propertyNames(); enumer.hasMoreElements();) {
            String key = (String)enumer.nextElement();
            String value = prop.getProperty(key);
            String[] tokens = key.split("[.]");
            
            if (tokens.length < 2) {
                throw new IOException("Invalid property key: "+key);
            }
            
            if (ciName == null)
            	ciName = tokens[0];

            if (tokens[0].equals(ComponentTestCase.META_PROPERTY_NAME)) {
            	if (tokens.length == 2)
            	    metaParams.put(tokens[1], value);
            	else 
            		throw new IOException("Invalid property key: " + key);
            }
            else if (!isMetadata) {
	            ComponentInstance ci = network.getComponentInstance(ciName);
	            if (ci == null) {
                    throw new IOException("Unknown component instance when reading properties: "+key);
	            }
	
	            if (tokens.length == 2) {
	                try {
	                    if (ci.getComponent().getParameter(tokens[1]) == null) {
                            throw new IOException("Properties: Unknown parameter " + tokens[1]);
	                    }
	                    ci.setParameterValue(ci.getComponent().getParameter(tokens[1]), value);
	                } catch (IllegalArgumentException e) {
                        throw new IOException("Cannot set property: "+e.toString());
	                }
	            } else {
                    throw new IOException("Invalid property key: "+key);
	            }
            }
        }

        return metaParams;
    }
}
