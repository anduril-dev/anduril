package org.anduril.asser;

import java.io.PrintStream;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Command line argument parameter.
 */
public class MainArgument {

	/** Command line word prefix indicating an argument name **/
	static public final String ARGUMENT_PREFIX = "-";

	private final String   desc;
	private final String[] argNames;
	private final String[] argDefs;

	private boolean  isSet = false;
	private String[] values;

    /**
     * Add a new command line argument prototype with no parameters.
     *
     * @param name         Name of the command line argument
     * @param description  A short explanation for the argument and its purpose
     * @param protos       Set of possible command line arguments
     * @return A reference to the new argument prototype.
     **/
    static public MainArgument add(String                    name,
                                   String                    description,
                                   Map<String, MainArgument> protos) {
      return add(name, (String[])null, description, (String[])null, protos);
    }

    /**
     * Add a new command line argument prototype with at most one mandatory
     * parameter.
     *
     * @param name         Name of the command line argument
     * @param arg          Name of the argument of the parameter
     *                     (null if the argument takes no parameters)
     * @param description  A short explanation for the argument and its purpose
     * @param protos       Set of possible command line arguments
     * @return A reference to the new argument prototype.
     **/
    static public MainArgument add(String                    name,
                                   String                    arg,
                                   String                    description,
                                   Map<String, MainArgument> protos) {
      String[] argV = (arg == null) ? null : new String[] {arg};
      return add(name, argV, description, (String[])null, protos);
    }

    /**
     * Add a new command line argument prototype with at most one parameter.
     *
     * @param name         Name of the command line argument
     * @param arg          Name of the argument of the parameter
     *                     (null if the argument takes no parameters)
     * @param description  A short explanation for the argument and its purpose
     * @param defaultValue The default value for the argument
     *                     (null if the parameter is mandatory)
     * @param protos       Set of possible command line arguments
     * @return A reference to the new argument prototype.
     **/
    static public MainArgument add(String                    name,
                                   String                    arg,
                                   String                    description,
				                   String                    defaultValue,
                                   Map<String, MainArgument> protos) {
      String[] argV = (arg          == null) ? null : new String[] {arg};
      String[] defV = (defaultValue == null) ? null : new String[] {defaultValue};
      return add(name, argV, description, defV, protos);
    }

    /**
     * Add a new command line argument prototype to the given set of expected
     * arguments. All arguments of this parameter are mandatory.
     *
     * @param name          Name of the command line argument
     * @param args          Names of the arguments of the parameter
     *                      (null if the argument takes no parameters)
     * @param description   A short explanation for the argument and its purpose
     * @param protos        Set of possible command line arguments
     * @return A reference to the new argument prototype.
     **/
    static public MainArgument add(String                    name,
                                   String[]                  args,
                                   String                    description,
                                   Map<String, MainArgument> protos) {
      return add(name, args, description, (String[])null, protos);
    }

    /**
     * Add a new command line argument prototype to the given set of expected
     * arguments.
     *
     * @param name          Name of the command line argument
     * @param args          Names of the arguments of the parameter
     *                      (null if the argument takes no parameters)
     * @param description   A short explanation for the argument and its purpose
     * @param defaultValues A list of default values for the arguments
     *                      (null if the parameters are mandatory)
     * @param protos        Set of possible command line arguments
     * @return A reference to the new argument prototype.
     **/
    static public MainArgument add(String                    name,
                                   String[]                  args,
                                   String                    description,
                                   String[]                  defaultValues,
                                   Map<String, MainArgument> protos) {
      MainArgument ma  = new MainArgument(args, description, defaultValues);
      MainArgument old = protos.put(name, ma);
      if (old != null)
         throw new IllegalStateException(name+" was already defined.");
      return ma;
    }

    /**
     * This constructor is not supposed to be called outside of this class.
     * The friend visibly is for the test cases only.
     *
     * @param args          Parameter names for the argument
     * @param description   Semantic description for the argument
     * @param defaultValues Default values for the parameters
     */
	MainArgument(String[] args,
	             String   description,
	             String[] defaultValues) {
		argNames = (args == null) ? AsserUtil.EMPTY_ARRAY_STRING : args;
        if ((defaultValues != null) && (defaultValues.length != argNames.length))
           throw new IllegalArgumentException("Number of default values does not match the parameter vector length.");
        values   = (argNames.length>0) ? new String[argNames.length] : null;
        argDefs  = defaultValues;
        desc     = description;
	}

	/**
	 * Returns a short semantic description for the argument and its purpose. 
	 */
	public String getDescription() {
		return desc;
	}

	/**
	 * Returns true if this argument has been defined by the user.
	 */
	public boolean isUsed() {
		return isSet;
	}

	/**
	 * Returns the first value of the argument list. This method is
	 * intended for the arguments with exactly one parameter.
	 */
	public String getValue() {
		String[] values = getValues();
		return (values == null) ? null : getValues()[0]; 
	}

	/**
	 * Returns all parameter values of the argument.
	 */
	public String[] getValues() {
	    return (isSet ? values : argDefs);
	}
	
	/**
	 * Returns an integer representation for the first argument.
	 */
	public int getInt() {
		return Integer.parseInt(getValue());
	}

	/**
	 * Returns a long integer representation for the first argument.
	 */
	public long getLong() {
		return Long.parseLong(getValue());
	}

	/**
	 * Returns a double representation for the first argument.
	 */
	public double getDouble() {
		return Double.parseDouble(getValue());
	}

	@Override
	public String toString() {
		return (values==null ? "-unspecified-" : Arrays.toString(values));
	}

	/**
	 * Produces a textual listing of the arguments available for the user.
	 * This method can be used to print usage helps to the end users.
	 *
	 * @param protos A set of supported arguments
	 * @param out    Target stream for the listing (for example {@link java.lang.System#err})
	 */
	static public void printArguments(Map<String, MainArgument> protos,
			                          PrintStream               out) {
		String[]     keys = protos.keySet().toArray(new String[protos.size()]);
		final String PAD  = "              ";
		
		Arrays.sort(keys, Collator.getInstance());
		out.println("Options:");
		if (keys.length == 0) {
			out.println(" No options available for this program!");
		} else
		for (String key : keys) {
			MainArgument arg = protos.get(key);
			StringBuffer buf = new StringBuffer();
			buf.append(' ')
			   .append(ARGUMENT_PREFIX)
			   .append(key);
			for (String aName : arg.argNames) {
			    buf.append(' ').append(aName);
			}
			if (buf.length() < PAD.length()) {
				out.print(buf.toString());
	 		    out.print(PAD.substring(buf.length()));
			} else {
				out.println(buf.toString());
				out.print(PAD);
			}
			out.println(arg.getDescription());
			buf.setLength(0);
			if (arg.argDefs != null) {
			   out.print(PAD);
			   if (arg.argDefs.length == 1) {
				   out.print("default: ");
				   out.println(arg.argDefs[0]);
			   } else {
			       out.print("defaults: ");
			       out.println(Arrays.toString(arg.argDefs));
			   }
			}
		}
	}

    /**
     * Populates the given argument pool with the command line
     * arguments given by the user. Anonymous arguments are arguments
     * that are not prefixed with a hyphen (-) or their parameters.
     *
     * @param argv    Original vector of command line arguments
     * @param protos  A set of supported arguments
     * @param leftMin Minimum number of anonymous command line arguments
     * @param leftMax Maximum number of command line arguments of a negative
     *                number for an unlimited argument list
     * @return A vector of anonymous command line arguments
     */
    static public String[] parseInput(String[]                  argv,
                                      Map<String, MainArgument> protos,
                                      int                       leftMin,
                                      int                       leftMax) {
        return parseInput(argv, protos, leftMin, leftMax, false);
    }

    /**
     * Populates the given argument pool with the command line
     * arguments given by the user. Anonymous arguments are arguments
     * that are not prefixed with a hyphen (-) or their parameters.
     *
     * @param argv         Original vector of command line arguments
     * @param protos       A set of supported arguments
     * @param leftMin      Minimum number of anonymous command line arguments
     * @param leftMax      Maximum number of command line arguments of a negative
     *                     number for an unlimited argument list
     * @param leaveUnknown Leave unexpected parameters to the list of anonymous arguments
     * @return A vector of anonymous command line arguments
     */
	static public String[] parseInput(String[]                  argv,
			                          Map<String, MainArgument> protos,
                                      int                       leftMin,
                                      int                       leftMax,
                                      boolean                   leaveUnknown) {
		List<String> leftOvers = new ArrayList<String>(argv.length);

		for (int i=0; i<argv.length; i++) {
			if (argv[i].startsWith(ARGUMENT_PREFIX)) {
				String       name = argv[i].substring(ARGUMENT_PREFIX.length());
				MainArgument arg  = protos.get(name);
				
				if (arg == null) {
                    if (!leaveUnknown)
					  throw new IllegalArgumentException("Unknown parameter: "+name);
                    leftOvers.add(argv[i]);
                    continue;
                }
				if (arg.isUsed())
					throw new IllegalArgumentException("Duplicate parameter: "+name);
				arg.isSet = true;
				if (arg.argNames.length+i+1 > argv.length)
					throw new IllegalArgumentException("Arguments missing for "+name+" parameter.");
				for (int j=0; j<arg.argNames.length; j++) {
					arg.values[j] = argv[++i];
				}
			} else {
				leftOvers.add(argv[i]);
			}
		}
		int lSize = leftOvers.size();
		if (lSize < leftMin)
		   throw new IllegalArgumentException("Only "+lSize+"/"+leftMin+" of mandatory parameters were given.");
		if ((leftMax >= 0) && (lSize > leftMax))
		   throw new IllegalArgumentException("Too many parameters: "+lSize+"/"+leftMax);
		return leftOvers.toArray(new String[lSize]);
	}

}

