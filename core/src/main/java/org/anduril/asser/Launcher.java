package org.anduril.asser;

import org.anduril.asser.annotation.IDConverter;
import org.anduril.asser.annotation.RegionGO;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.LineJoin;
import org.anduril.asser.sequence.MotifMatch;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a wrapper for all Asser.jar applications and this is also the
 * default executable of the distribution.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.80
 */
public final class Launcher {

    /**
     * No instantiation for this class.
     */
    private Launcher() {}

    /**
     * Lauch the selected target program.
     *
     * @param argv Command line arguments including definition of the target program.
     */
    public static void main(String[] argv) {
        // Read command line arguments...
        Map<String, MainArgument> args       = new HashMap<String, MainArgument>();
        MainArgument              argProgram = MainArgument.add("ASoft", "name", "Target program selection", args);
        String[]                  argLeft;
        try { argLeft = MainArgument.parseInput(argv, args, 0, -1, true); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            printSynopsis(args);
            return;
        }
        if (!argProgram.isUsed()) {
            printSynopsis(args);
            return;
        }
        
        String pName = argProgram.getValue().trim();
        try {
            if ("CSVValidator".equals(pName)) {
                CSVParser.main(argLeft);
            } else
            if ("GOSearch".equals(pName)) {
                RegionGO.main(argLeft);
            } else
            if ("Korvasieni".equals(pName)) {
                IDConverter.main(argLeft);
            } else
            if ("LineJoin".equals(pName)) {
               LineJoin.main(argLeft);
            } else
            if ("MotifMatch".equals(pName)) {
                MotifMatch.main(argLeft);
            } else
            if ("Version".equals(pName)) {
               Version.main(argLeft);
            } else {
                System.err.println("Unknown program: "+pName);
                System.err.println();
                printSynopsis(args);
            }
        } catch (Exception e) {
            System.err.println("Program failure in: "+pName);
            e.printStackTrace();
        }
    }

    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("Asser.jar v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("--------------------------------");
        System.err.println("asser.jar [options]");
        MainArgument.printArguments(args, System.err);
        System.err.println();
        System.err.println("Currently supported programs are:");
        System.err.println("CSVValidator   - Reads the given files to make sure they are fine");
        System.err.println("GOSearch       - Gene ontology tool for the item selections");
        System.err.println("Korvasieni     - Ensembl based ID converter");
        System.err.println("LineJoin       - Combines the lines of multiple files");
        System.err.println("MotifMatch     - Compares a motif against a set of DNA sequencies");
        System.err.println("Version        - Prints version number");
    }

}
