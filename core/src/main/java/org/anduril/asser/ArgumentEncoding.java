package org.anduril.asser;

/**
 * Main argument value encoding values.
 */
public enum ArgumentEncoding {

	EMPTY          ("\\e", AsserUtil.EMPTY_STRING, "empty string"),
	SPACE          ("\\s", " ",  "space"),
	CARRIAGE_RETURN("\\r", "\r", "carriage return"),
	NEW_LINE       ("\\n", "\n", "new line"),
	TAB            ("\\t", "\t", "tab"),
	QUOTATION_MARK ("\\q", "\"", "quotation mark"),
	SEMICOLON      ("\\c", ";",  "semicolon");

	public final String symbol;
	public final String value;
	public final String description;
	
	ArgumentEncoding(String symbol, String value, String desc) {
	   this.symbol      = symbol;
	   this.value       = value;
	   this.description = desc;
	}

	static public String decode(String encoded) {
		String value = encoded;

		value = value.replaceAll("\\\\\\\\", "\0");
        for (ArgumentEncoding elem: ArgumentEncoding.values()) {
        	value = value.replaceAll("\\"+elem.symbol, elem.value);
        }
        value = value.replaceAll("\0", "\\\\");
		return value;
	}

}