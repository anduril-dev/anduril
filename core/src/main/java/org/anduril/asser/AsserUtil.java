package org.anduril.asser;

import org.anduril.asser.io.CSVParser;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

/**
 * Miscellaneous utility functions.
 * 
 * @author Marko Laakso
 */
public final class AsserUtil {

    static public final String[] EMPTY_ARRAY_STRING = new String[0];
    static public final String   EMPTY_STRING       = "";

    /** A comma for the comma separated lists. */
    static public final String DEFAULT_DELIMITER  = ",";

	/** No instantiation **/
	private AsserUtil() {}

    /**
     * Creates a map from the list of comma separated key=value pairs.
     */
    static public Map<String,String> parseMap(String listDef) {
        return parseMap(listDef, DEFAULT_DELIMITER);
    }

    /**
     * Creates a map from the list of key=value pairs.
     *
     * @param separator Separator character between the key=value pairs 
     */
    static public Map<String,String> parseMap(String listDef, String separator) {
        Map<String,String> map   = new HashMap<String,String>();
        StringTokenizer    pairs = new StringTokenizer(listDef, separator);
        
        while (pairs.hasMoreTokens()) {
            String pair  = pairs.nextToken();
            int    delim = pair.indexOf('=');
            if (delim < 1)
                throw new IllegalArgumentException("Invalid key=value pair: "+pair);
            map.put(pair.substring(0,delim).trim(), pair.substring(delim+1).trim());
        }
        return map;
    }

    /**
     * Generates a table of key=value pairs where the value is optional.
     *
     * @param  list A comma separated list of key=value pairs.
     *              Value element may be left out together with = sign. 
     */
    static public String[][] prepareMapping(String list) {
        String[] defs = AsserUtil.split(list);
        if ((defs.length==1) && defs[0].isEmpty())
            return new String[0][];

        String[][] map = new String[defs.length][2];
        for (int i=0; i<defs.length; i++) {
            int pos = defs[i].indexOf('=');
            if (pos < 0) {
                map[i][0] = defs[i];
            } else {
                map[i][0] = defs[i].substring(0,pos);
                map[i][1] = defs[i].substring(pos+1);
            }
        }
        return map;
    }

    /**
     * Generates a comma separated list of the given elements.
     */
    static public String collapse(Collection<?> items) {
    	return collapse(items, DEFAULT_DELIMITER);
    }

    /**
     * Generates a delimiter separated list of the given elements.
     */
    static public String collapse(Collection<?> items, String delim) {
    	if (items.isEmpty()) return EMPTY_STRING;

    	StringBuffer buf        = new StringBuffer(1024);
    	boolean      hasContent = false;
    	for (Object o : items) {
    		if (hasContent) {
    			buf.append(delim);
    		} else {
    			hasContent = true;
    		}
    		buf.append(o);
    	}
    	return buf.toString();
    }

    /**
     * Concatenates the given strings together using the given separator
     * between the entries.
     */
    static public String collapse(String[] values,
                                  String   separator) {
        if (values.length == 0) return EMPTY_STRING;
        if (values.length == 1) return values[0];

        StringBuffer buffer = new StringBuffer(512);
        boolean      first  = true;
        for (String value : values) {
            if (first) {
                first = false;
            } else {
                buffer.append(separator);
            }
            buffer.append(value);
        }
        return buffer.toString();
    }

    /**
     * Split comma separated values to an array. 
     */
    static public String[] split(String values) {
        return split(values, DEFAULT_DELIMITER);
    }

    /**
     * Split delimiter separated values to an array. 
     */
    static public String[] split(String values, String delim) {
        if (values == null) return EMPTY_ARRAY_STRING;
        String[] array = values.split(delim);
        return trimAll(array);
    }

    /**
     * Split delimiter separated values to an array preserving each empty
     * string between and after them.
     */
    static public String[] strictSplit(String values, String delim) {
    	if (values == null) return EMPTY_ARRAY_STRING;
    	List<String> list = new ArrayList<String>(20);

    	int    pos1 = 0;
    	int    pos2;
    	while ((pos2=values.indexOf(delim, pos1))>=0) {
    		list.add(values.substring(pos1, pos2));
    		pos1 = pos2+delim.length();
    	}
    	list.add(values.substring(pos1));
    	return trimAll(list.toArray(new String[list.size()]));
    }

    /**
     * Performs {@link java.lang.String#trim()} for all items of the given list.
     *
     * @return This is just a convenience reference to the input array 
     */
    static public String[] trimAll(String... values) {
        for (int i=0; i<values.length; i++) {
            if (values[i] != null)
                values[i] = values[i].trim();
        }
        return values;
    }

	/**
	 * Replace all matches with the given substitute.
	 *
	 * @param buffer Text document to be modified 
	 * @param from   Original substring
	 * @param to     New replacement for the original string
	 */
	static public void replaceAll(StringBuffer buffer, String from, String to) {
		int i = 0;

        if (from == null) throw new NullPointerException("Original substring is null.");
        if (to   == null) throw new NullPointerException("Replacement string is null.");
		while ((i=buffer.indexOf(from, i)) >= 0) {
			buffer.replace(i, i+from.length(), to);
			i += to.length();
		}
	}

	/**
	 * Find all indices of the given values from the list of values.
	 *
	 * @param keys          Values of interest
	 * @param list          List of values
	 * @param caseSensitive True for case sensitive matching
     * @param mandatory     Throw an exception if the key is not found
	 * @return              The first index of each key within the list or -1 if not found
	 * @since               {@link org.anduril.asser.Version Version} 1.53 
	 */
	static public int[] indicesOf(String[] keys,
			                      String[] list,
			                      boolean  caseSensitive,
			                      boolean  mandatory) {
		int[] indices = new int[keys.length];
		for (int i=0; i<keys.length; i++) {
			indices[i] = indexOf(keys[i], list, caseSensitive, mandatory);
		}
		return indices;
	}

	/**
	 * Find the index of the given value from the list of values.
	 *
	 * @param key           Value of interest
	 * @param list          List of values
	 * @param caseSensitive True for case sensitive matching
     * @param mandatory     Throw an exception if the key is not found
	 * @return              The first index of the key within the list or -1 if not found 
	 */
	static public int indexOf(String   key,
			                  String[] list,
			                  boolean  caseSensitive,
			                  boolean  mandatory) {
		for (int i=0; i<list.length; i++) {
			if (caseSensitive) {
				if (key.equals(list[i]))           return i;
			} else {
			    if (key.equalsIgnoreCase(list[i])) return i;
			}
		}
		if (mandatory) {
			if (list.length < 100)
		       throw new IllegalArgumentException("Value ("+key+") not available in "+Arrays.toString(list)+'.');
			else
			   throw new IllegalArgumentException("Value ("+key+") not found.");
		}
		return -1;
	}

	/**
	 * Round the given value so that it has at most the given number of
	 * decimals and produces the corresponding String presentation for it.
	 *
	 * @param value    Original value
	 * @param decimals Number of digits after the decimal point
	 */
    static public String round(double value, int decimals) {
    	return round(value, decimals, CSVParser.DEFAULT_MISSING_VALUE_SYMBOL);
    }

	/**
	 * Round the given value so that it has at most the given number of
	 * decimals and produces the corresponding String presentation for it.
	 *
	 * @param value    Original value
	 * @param decimals Number of digits after the decimal point
	 */
    static public String round(double value, int decimals, String missingValue) {
    	if (Double.isNaN(value) || Double.isInfinite(value)) {
    		if (missingValue == null)
    			throw new IllegalArgumentException("Cannot format: "+value);
    		return missingValue;
    	}
    	String               pattern = "#0.00000000000".substring(0,3+decimals);
    	DecimalFormat        format  = new DecimalFormat(pattern);
    	DecimalFormatSymbols s       = format.getDecimalFormatSymbols();
    	s.setDecimalSeparator('.');
    	format.setDecimalFormatSymbols(s);
    	return format.format(value);
    }

    /**
     * This method splits all values of the given array and puts them into a single array.
     *
     * @param  values    A list of item lists
     * @param  separator Item separator string
     * @return List of all values where each item has it own index
     */
    static public String[] expandJointValues(String[] values, String separator) {
        List<String> array = null;
        for (int i=0; i<values.length; i++) {
            if ((values[i] != null) && (values[i].indexOf(separator) >= 0)) {
                array = new LinkedList<String>();
                break;
            }
        }

        if (array == null) {
           return values;
        } else {
           for (int i=0; i<values.length; i++) {
               if (values[i] == null) {
                   array.add(null);
               } else {
                   String[] items = values[i].split(separator);
                   for (int j=0; j<items.length; j++) {
                       array.add(items[j]);
                   }
               }
           }
           return array.toArray(new String[array.size()]);
        }
    }

    /**
     * Generates a textual representation of an object matrix.
     */
    static public String toString(List<Object[]> matrix) {
    	StringBuffer buffer = new StringBuffer();
    	boolean      firstR = true;
    	for (Object[] row : matrix) {
    		if (firstR) {
    			firstR = false;
    		} else {
    			buffer.append('\n');
    		}
    		buffer.append(Arrays.toString(row));
    	}
    	return buffer.toString();
    }

    /**
     * This is an efficient implementation of (String.trim().length() < 1).
     */
    static public boolean onlyWhitespace(String text) {
        int length = text.length();
        for (int i=0; i<length; i++) {
            if (!Character.isWhitespace(text.charAt(i)))
                return false;
        }
        return true;
    }

    /**
     * Returns the only item of the given collection.
     */
    static public Object selectOnly(Collection<?> singleton) {
        if (singleton.isEmpty())
            throw new IllegalStateException("No items in the given collection.");
        if (singleton.size() > 1)
            throw new IllegalStateException("More than one items: "+singleton);
        if (singleton instanceof List<?>)
            return ((List<?>)singleton).get(0);
        return singleton.iterator().next();
    }

    /**
     * Calculate the minimum of the given list of values.
     */
    static public double min(double... values) {
        double min = (values.length > 0) ? Double.POSITIVE_INFINITY : Double.NaN;

        for (double v : values) {
            if (v < min) min = v;
        }
        return min; 
    }

    /**
     * Calculate the maximum of the given list of values.
     */
    static public double max(double... values) {
        double max = (values.length > 0) ? Double.NEGATIVE_INFINITY : Double.NaN;

        for (double v : values) {
            if (v > max) max = v;
        }
        return max;
    }

}
