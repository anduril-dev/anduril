package org.anduril.asser.annotation;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.db.JDBCConnection;
import org.anduril.asser.db.TypeConverter;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This component can be used to fetch the closest genes for the given loci.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.72
 */
public class NextGeneComponent extends SkeletonComponent {

    static public final String PARAMETER_ID_COLUMN     = "idColumn";
    static public final String PARAMETER_CHR_COLUMN    = "chrColumn";
    static public final String PARAMETER_BASE_COLUMN   = "baseColumn";
    static public final String PARAMETER_ECHO_COLS     = "echoColumns";
    static public final String PARAMETER_WINDOW_BEFORE = "bpBefore";
    static public final String PARAMETER_WINDOW_AFTER  = "bpAfter";
    static public final String PARAMETER_NEAREST_ONLY  = "nearestOnly";
    static public final String PARAMETER_BOTH_STRANDS  = "bothStrands";
    static public final String PARAMETER_NEXT_DISTANCE = "nextDist";
    static public final String PARAMETER_LOCATION_DEF  = "locationDef";
    static public final String PARAMETER_SIGN_DISTANCE = "signDist";
    static public final String PARAMETER_SEARCH_MODE   = "elementType";

    static public final String INPUT_CONNECTION = "connection";
    static public final String INPUT_KEYS       = "sourceKeys";

    static public final String OUTPUT_ANNOTATION = "bioAnnotation";

    protected ErrorCode runImpl(CommandFile cf) throws IOException,
                                                       SQLException {
        File           conCfg   = cf.getInput(INPUT_CONNECTION);
        CSVParser      in       = new CSVParser(cf.getInput(INPUT_KEYS));
        String[]       colsIn   = in.getColumnNames();
        String[]       colsEcho = AsserUtil.split(cf.getParameter(PARAMETER_ECHO_COLS));
        int[]          echoInd  = colsEcho[0].isEmpty() ? null : AsserUtil.indicesOf(colsEcho, colsIn, true, true);
        int            idCol    = AsserUtil.indexOf(cf.getParameter(PARAMETER_ID_COLUMN),   colsIn, true, true);
        int            chrCol   = AsserUtil.indexOf(cf.getParameter(PARAMETER_CHR_COLUMN),  colsIn, true, false);
        int            baseCol  = AsserUtil.indexOf(cf.getParameter(PARAMETER_BASE_COLUMN), colsIn, true, true);
        long           before   = Long.parseLong(cf.getParameter(PARAMETER_WINDOW_BEFORE));
        long           after    = Long.parseLong(cf.getParameter(PARAMETER_WINDOW_AFTER));
        long           nextLim  = Long.parseLong(cf.getParameter(PARAMETER_NEXT_DISTANCE));
        String         modeS    = cf.getParameter(PARAMETER_SEARCH_MODE);
        boolean        nearest  = cf.getBooleanParameter(PARAMETER_NEAREST_ONLY);
        boolean        bothDNA  = cf.getBooleanParameter(PARAMETER_BOTH_STRANDS);
        boolean        signDist = cf.getBooleanParameter(PARAMETER_SIGN_DISTANCE);
        boolean[]      hits     = new boolean[4];
        String         lMethS   = cf.getParameter(PARAMETER_LOCATION_DEF);
        JDBCConnection db;
        int            locMeth;
        String         tableSQL;
        String         idColOut;
        String         whereClause;

        if (     "start".equalsIgnoreCase(lMethS))
            locMeth   = 0;
        else if ("average".equalsIgnoreCase(lMethS))
            locMeth = 1;
        else if ("end".equalsIgnoreCase(lMethS))
            locMeth = 2;
        else
            throw new IllegalArgumentException("Invalid location definition: "+lMethS);

        if ("gene".equals(modeS)) {
        	idColOut    = ".GeneId";
        	tableSQL    = "gene G ";
        	whereClause = "AND (G.status = \"KNOWN\") ";
        } else
        if ("exon".equals(modeS)) {
        	idColOut    = ".ExonId";
            tableSQL    = "exon G ";
            whereClause = "";
        } else
        if ("transcript".equals(modeS)) {
          	idColOut    = ".TranscriptId";
            tableSQL    = "transcript G ";
            whereClause = "AND (G.status = \"KNOWN\") ";
        } else throw new IllegalArgumentException("Invalid elementType definition: "+modeS);

        if (conCfg == null) {
            db = new JDBCConnection(IDConverter.CFG_FILE);
        } else {
            db = new JDBCConnection(conCfg);
        }

        List<String> colnames = new ArrayList<String>();
        colnames.add(colsIn[idCol]);
        colnames.add(idColOut);
        colnames.add("strand");
        colnames.add("chromosome");
        colnames.add(modeS+"_start");
        colnames.add(modeS+"_end");
        colnames.add("distance");
        if (echoInd != null) for (String col : colsEcho) {
        	colnames.add(col);
        }
        CSVWriter out = new CSVWriter(colnames.toArray(new String[colnames.size()]),
                                      cf.getOutput(OUTPUT_ANNOTATION),
                                      false);
        while (in.hasNext()) {
            String[]       line = in.next();
            long           loci;
            String         chr;
            List<Object[]> genes;
            
            if (chrCol < 0) {
                InterestingRegion reg = new InterestingRegion(line[baseCol]);
                chr = reg.getChromosome();
                switch (locMeth) {
                  case 0: loci = reg.getStart();                  break;
                  case 1: loci = (reg.getStart()+reg.getEnd())/2; break;
                  case 2: loci = reg.getEnd();                    break;
                  default: throw new IllegalStateException("Unsupported location definition: "+locMeth);
                }
            } else {
                chr  = line[chrCol];
                loci = Long.parseLong(line[baseCol]);
            }

            genes = db.fetchObjects(
                       "SELECT G.stable_id, "+
                       "       G.seq_region_strand, "+
                       "       G.seq_region_start, "+
                       "       G.seq_region_end, "+
                       "       GREATEST(0, CAST(G.seq_region_start AS SIGNED) - ?, ? - CAST(G.seq_region_end AS SIGNED)) AS distance "+
                       "FROM   seq_region SR, "+tableSQL+
                       "WHERE  (G.seq_region_id     = SR.seq_region_id) AND "+
                       "       (G.is_current        = 1)                AND "+
                       "       (SR.name             = ?)                AND "+
                       "       (SR.coord_system_id  = ("+
                       "         SELECT coord_system_id "+
                       "         FROM   coord_system "+
                       "         WHERE  (name   = 'chromosome') AND "+
                       "                (attrib = 'default_version'))) AND "+
                       "       (((G.seq_region_strand = 1) AND "+
                       "         (G.seq_region_start <= ?) AND "+
                       "         (G.seq_region_end   >= ?)) OR "+
                       "        ((G.seq_region_strand = -1) AND "+
                       "         (G.seq_region_start <=  ?) AND "+
                       "         (G.seq_region_end   >=  ?)))"+
                       whereClause+
                       "ORDER  BY distance, 1",
                       loci, loci,
                       chr,
                       loci+after,  loci-before,
                       loci+before, loci-after);
            long maxDist    = -1;
            long prevDist   = -1;
            int  prevStrand = -1;
            hits[0] = false;
            hits[1] = (nextLim < 0);
            hits[2] = hits[3] = !bothDNA;
            for (Object[] gene : genes) {
                int  strand = TypeConverter.toInt (gene[1]);
                long start  = TypeConverter.toLong(gene[2]);
                long end    = TypeConverter.toLong(gene[3]);
                long cDist  = TypeConverter.toLong(gene[4]);

                if (nearest) {
                	if (prevDist < cDist) {
                       if (hits[0] && hits[1] && hits[2] && hits[3])
                          break; // All hits are found
                       if ((maxDist >= 0) && (maxDist < cDist))
                          break; // These hits are too distant
                	}
                    int hIndex = ((nextLim<0)||(cDist==0)||(loci < start)) ? 0 : 1; 
                    if (bothDNA && (strand == -1)) hIndex += 2;
                    if (hIndex != prevStrand || cDist > prevDist) {
                       if (cDist == 0) {
                           if (hits[hIndex] && hits[hIndex+1])
                               continue; // These sides have been found already
                           hits[hIndex+1] = true;
                       } else if (hits[hIndex]) {
                           continue; // This side has been found already
                       }
                    }
                    hits[hIndex] = true;
                    if ((nextLim >=0) && (maxDist < 0)) {
                        maxDist = cDist+nextLim;
                    }
                    prevDist   = cDist;
                    prevStrand = hIndex;
                }

                if (signDist) {
                    if (strand > 0)
                       cDist = start-loci;
                    else
                       cDist = end-loci;
                }

                // Print the gene...
                out.write(line[idCol]);
                out.write(gene[0].toString(), false);
                out.write(strand);
                out.write(chr, false);
                out.write(start);
                out.write(end);
                out.write(cDist);
                if (echoInd != null) for (int ec : echoInd) {
                	out.write(line[ec]);
                }
            }
        }

        out.close();
        db.close();
        return ErrorCode.OK;
    }

    /**
     * @param argv Pipeline arguments
     */
    public static void main(String[] argv) {
        new NextGeneComponent().run(argv);
    }

}
