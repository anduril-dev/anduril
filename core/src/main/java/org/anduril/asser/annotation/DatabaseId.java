package org.anduril.asser.annotation;

/**
 * Represents a labeled database identifier. The internal value of each instance is immutable.
 * 
 * @author Marko Laakso
 */
public class DatabaseId {
	
	private String label;
	private final String id;
	
	/**
	 * Create new identifier.
	 * 
	 * @param accessionNumber Internal value of the identifier
	 * @param name            Label value
	 */
	public DatabaseId(Object accessionNumber,
			          Object name) {
		id    = accessionNumber.toString();
		label = (name == null) ? "[undefined]" : name.toString();
	}

	/**
	 * Returns the name of the identifier.
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets a new name for the identifier.
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Returns the internal value of the identifier.
	 */
	public String getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DatabaseId) {
			DatabaseId in = (DatabaseId)obj;
			return id.equals(in.id);
		}
		return false;		
	}

	@Override
	public int hashCode() {
		return id.hashCode(); 
	}

	@Override
	public String toString() {
		return id;
	}

}