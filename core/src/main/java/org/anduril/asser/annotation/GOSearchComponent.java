package org.anduril.asser.annotation;

import org.anduril.asser.AsserUtil;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a pipeline wrapper for {@link org.anduril.asser.annotation.RegionGO}.
 * 
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class GOSearchComponent extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        List<String> args = new ArrayList<String>(64);
        args.add("-db");     args.add(cf.getInput("go").getAbsolutePath());
        args.add("-f");      args.add(cf.getInput("bioAnnotation").getAbsolutePath());
        args.add("-out");    args.add(cf.getOutput("hits").getAbsolutePath());
        args.add("-s");      args.add(cf.getParameter("numMatches"));
        args.add("-goCol");  args.add(cf.getParameter("goColumn"));
        args.add("-keyCol"); args.add(cf.getParameter("geneColumn"));
        for (String term : AsserUtil.split(cf.getParameter("terms"))) {
            args.add(term);
        }
        if (cf.getBooleanParameter("parentFilter")) {
        	args.add("-p");
        }
        RegionGO.main(args.toArray(new String[args.size()]));
        return ErrorCode.OK;
    }

    /**
     * @param argv Pipeline arguments
     */
    public static void main(String[] argv) {
        new GOSearchComponent().run(argv);
    }

}
