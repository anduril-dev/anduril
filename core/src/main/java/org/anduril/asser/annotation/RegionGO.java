package org.anduril.asser.annotation;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.MainArgument;
import org.anduril.asser.Version;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * This program reads AnnotateRegions outputs and lists regions that
 * contain references to given Gene Ontology.
 *
 * @author Marko Laakso
 */
public final class RegionGO {
    
	/** No instantiation for this stand-alone program. **/
	private RegionGO() {}
	
    static private final Comparator<String[]> GO_HIT_COMPARATOR = new Comparator<String[]>() {
        public int compare(String[] id1, String[] id2) {
            int c = id1[0].compareTo(id2[0]);
            if (c==0)
                c = id1[1].compareTo(id2[1]);
            return c;
        }
    };

    static public InterestingGene[] readGenesKorvasieni(String filename) throws IOException {
        BufferedReader        in   = new BufferedReader(new FileReader(filename));
        List<InterestingGene> gBuf = new ArrayList<InterestingGene>(500);
        String                line = in.readLine();
        InterestingGene       gene = null;
        StringTokenizer       tok;
        String                name;
        String                term;

        while ((line=in.readLine()) != null) {
            line = line.trim();
            if (line.isEmpty())
               continue;
            tok  = new StringTokenizer(line);
            name = tok.nextToken();
            while (tok.hasMoreTokens()) {
                term = tok.nextToken();
                if (term.startsWith("GO:")) {
                    if (name != null) {
                        gene = new InterestingGene(name);
                        name = null;
                        gBuf.add(gene);
                    }
                    gene.addGOTerm(term, gene.getId());
                }
            }
        }
        in.close();

        return (InterestingGene[])gBuf.toArray(new InterestingGene[gBuf.size()]);
    }

    static public InterestingGene[] readGenesAnduril(String filename,
                                                     String idColumn,
                                                     String goColumn) throws IOException {
        CSVParser             in    = new CSVParser(filename);
        int                   idCol = 0;
        int                   goCol = AsserUtil.indexOf(goColumn, in.getColumnNames(), false, true);
        List<InterestingGene> gBuf  = new ArrayList<InterestingGene>(500);
        String[]              line;
        InterestingGene       gene;

        if (!idColumn.isEmpty())
            idCol = AsserUtil.indexOf(idColumn, in.getColumnNames(), false, true);
        while (in.hasNext()) {
            line = in.next();
            gene = new InterestingGene(line[idCol]);
            for (String term : AsserUtil.split(line[goCol])) {
                gene.addGOTerm(term, line[idCol]);
            }
            gBuf.add(gene);
        }
        in.close();

        return (InterestingGene[])gBuf.toArray(new InterestingGene[gBuf.size()]);
    }

    static public void main(String[] argv) throws IOException {
    	final String FILE = "file";
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argHideParent = MainArgument.add("h",              "Do not show parents for the term descriptions", args);
        MainArgument argHideType   = MainArgument.add("H",              "Do not include ontology categories to the descriptions", args);
        MainArgument argParentF    = MainArgument.add("p",              "Skip parents if their child is present", args);
        MainArgument argFileAnnot  = MainArgument.add("f",      FILE,   "Gene annotation file with GO identifiers", args);
        MainArgument argFileGO     = MainArgument.add("db",     FILE,   "Name of the GO database file (*.obo)", "gene_ontology_ext.obo", args);
        MainArgument argAnduril    = MainArgument.add("out",    FILE,   "Anduril output file", args);
        MainArgument argTerms      = MainArgument.add("terms",  FILE,   "GO terms of this optional file are combined to the argument IDs", args);
        MainArgument argInCol      = MainArgument.add("keyCol", "name", "Gene ID column for Anduril input (empty = 1. column)", AsserUtil.EMPTY_STRING, args);
        MainArgument argGOCol      = MainArgument.add("goCol",  "name", "Gene Ontology column for Anduril input", "GO", args);
        MainArgument argScore      = MainArgument.add("s",      "min",  "Minimum number of GO hits", "1", args);
        String[] argLeft;
        try { argLeft = MainArgument.parseInput(argv, args, 0, -1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
	        System.err.println();
	        printSynopsis(args);
            return;
        }
        if (argTerms.isUsed()) {
        	argLeft = readTerms(argTerms.getValue(), argLeft);
        }
        argLeft = AsserUtil.expandJointValues(argLeft, AsserUtil.DEFAULT_DELIMITER);
        if (argLeft.length < 1) {
            System.err.println("You need at least one GO term for the search.\n");
	        printSynopsis(args);
            return;        	
        }

        double                    scoreLimit = argScore.getDouble();
        boolean                   isAnduril  = false;
        boolean                   parentF    = argParentF.isUsed();
        CSVWriter                 csvOut     = null;
        Map<String, String>       hits;
        GOTarget[]                items;
        Map<String, OntologyTerm> ontology;
        OntologyTerm              term;
        OntologyTerm[]            refs;

        // Load ontology
        ontology = loadGO(argFileGO.getValue(), false);
        hits     = new HashMap<String, String>(ontology.size()/2);

        // Print GO description
        if (!argFileAnnot.isUsed()) {
        	if (argAnduril.isUsed()) {
        		if (argHideType.isUsed())
        		   csvOut = new CSVWriter(new String[] {"GO","description"},
        				                  argAnduril.getValue());
        		else
                   csvOut = new CSVWriter(new String[] {"GO","description","ontology"},
                		                  argAnduril.getValue());
        		for (int i=0; i<argLeft.length; i++) {
    	        	OntologyTerm ot = ontology.get(argLeft[i]);
                    if (ot == null) {
                        System.err.println("***** Unknown GO term: "+argLeft[i]);
                        continue;
                    }
    	        	csvOut.write(ot.getID(), false);
    	        	csvOut.write(ot.getName());
    	        	if (!argHideType.isUsed()) {
    	        		csvOut.write(ot.getCategory());
    	        	}
        		}
        		csvOut.close();
        	} else
        	for (int i=0; i<argLeft.length; i++) {
	        	OntologyTerm ot = ontology.get(argLeft[i]);
                if (ot == null) {
                    System.err.println("***** Unknown GO term: "+argLeft[i]);
                    continue;
                }
                if (argHideType.isUsed()) {
	        	   System.out.println(ot.getID()+'\t'+ot.getName());
                } else {
                   System.out.println(ot.getID()+'\t'+ot.getName()+" ("+ot.getCategory()+')');
                }
	        	if (!argHideParent.isUsed()) {
	        	   System.out.println("\t\tParent nodes are:");
	        	   printParents(0, ot.getParents(), ontology, argHideType.isUsed());
	        	   System.out.println();
	        	}
        	}
        	return;
        }

        refs = new OntologyTerm[argLeft.length];
        for (int i=0; i<refs.length; i++) {
            refs[i] = ontology.get(argLeft[i]);
            if (refs[i]==null)
               throw new IllegalArgumentException(argLeft[i]+" is not available in "+argFileGO.getValue());
        }

        // Load items for annotations
        if (argAnduril.isUsed()) {
            isAnduril = true;
            items     = readGenesAnduril(argFileAnnot.getValue(),
                                         argInCol.getValue(),
                                         argGOCol.getValue());
        } else {
            items = readGenesKorvasieni(argFileAnnot.getValue());
        }

        // Count GO annotations
        for (int r=0; r<items.length; r++) {
        	if (items[r].isEmpty()) continue;
            String[][] terms = items[r].getGOTerms();
            items[r].removeGOTerms();

            for (int t=0; t<terms.length; t++) {
                term = ontology.get(terms[t][0]);
                if (term==null) {
                    System.err.println("***** warning: unknown GO term ("+items[r]+"): "+terms[t][0]);
                    continue;
                }
                if (hasParent(term, refs, ontology, hits)) {
                    items[r].addGOTerm(terms[t][0], terms[t][1]);
                }
                if (parentF) filterParents(items[r], ontology);
            }
            items[r].setScore(items[r].size());
        }

        // Print regions based on their scores
        if (isAnduril) {
            csvOut = new CSVWriter(new String[] {"entry", "query", "GO", "desc", "ontology"},
                                   argAnduril.getValue(),
                                   false); // No quotations for column names
        } else {
            System.out.print("Interesting regions for [");
            for (int i=0; i<refs.length; i++) {
                if (i>0) {
                   System.out.print(", ");
                }
                System.out.append(refs[i].getID()).append(' ').append(refs[i].getName())
                          .append(" (").append(refs[i].getCategory()).append(')');
            }
            System.out.println("]:");
        }
        Arrays.sort(items);
        for (int r=items.length-1; r>=0; r--) {
            if (items[r].getScore() < scoreLimit) continue;

            String[][] terms = items[r].getGOTerms();
            Arrays.sort(terms, GO_HIT_COMPARATOR);

            if (!isAnduril) {
                System.out.println(" - "+items[r]+" ("+items[r].getScore()+")");
            }
            for (int t=0; t<terms.length; t++) {
                term = ontology.get(terms[t][0]);
                if (isAnduril) {
                   csvOut.write(items[r].toString());
                   csvOut.write(hits.get(terms[t][0]),false);
                   csvOut.write(term.getID(), false);
                   csvOut.write(term.getName());
                   csvOut.write(goCategory(term.getCategory()), false);
                } else {
                   System.out.println("   "+terms[t][0]+" "+term.getName());
                }
            }
        }
        if (csvOut != null) csvOut.close();
    }

    /** Return the abbreviation of the given Gene Ontology. */
    static public String goCategory(String category) {
 	   if ("biological_process".equals(category))
 		  return "BP";
 	   else
 	   if ("cellular_component".equals(category))
  		  return "CC";
 	   else
 	   if ("molecular_function".equals(category))
 		   return "MF";
 	   return category;
    }

    static void filterParents(GOTarget                 target,
    		                  Map<String,OntologyTerm> ontology) {
    	if (target.isEmpty()) return;

        String[][]     terms = target.getGOTerms();
        OntologyTerm[] refs  = new OntologyTerm[terms.length];
        for (int i=0; i<refs.length; i++) {
            refs[i] = ontology.get(terms[i][0]);
        }
        termSearch:
        for (int t=terms.length-1; t>=0; t--) {
        	for (int i=0; i<refs.length; i++) {
        		if (t==i) continue;
                if (hasParent(refs[i], refs[t], ontology)) {
                   target.removeGOTerm(t);
                   continue termSearch;
                }
        	}
        }
    }

    static private String[] readTerms(String filename, String[] args) throws IOException {
    	List<String> terms = new ArrayList<String>(4000);
    	for (String t : args) {
    		terms.add(t);
        }
    	Scanner scan = new Scanner(new File(filename));
    	scan.useDelimiter(Pattern.compile("[\\s\"\'#,]"));
    	while (scan.hasNext()) {
    		String t = scan.next();
    		if (!t.isEmpty()) terms.add(t);
    	}
    	scan.close();
    	return terms.toArray(new String[terms.size()]);
    }

    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("GOSearch v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("-------------------------------");
        System.err.println("GOSearch [options] [GO_id...]");
        MainArgument.printArguments(args, System.err);
    }

    static private void printParents(int                       level,
    		                         String[]                  parents,
    		                         Map<String, OntologyTerm> ontology,
    		                         boolean                   hideType) {
    	final String PAD = "                             ";
    	if (parents == null)
    	   return;
    	for (int i=0; i<parents.length; i++) {
    		OntologyTerm ot = ontology.get(parents[i]);
    		if (hideType) {
    			System.out.println("\t\t"+PAD.substring(0,level)+ot.getID()+" = "+ot.getName());
    		} else {
    		    System.out.println("\t\t"+PAD.substring(0,level)+ot.getID()+" = "+ot.getName()+
    		    		           " ("+ot.getCategory()+')');
    		}
    		printParents(level+1, ot.getParents(), ontology, hideType);
    	}
    }

  static public Map<String, OntologyTerm> loadGO(String  filename,
                                                 boolean regulations) throws IOException {
    return loadGO(new FileReader(filename), regulations);
  }

  static public Map<String, OntologyTerm> loadGO(URL     fileaddress,
                                                 boolean regulations) throws IOException {
    return loadGO(new InputStreamReader(fileaddress.openStream()), regulations);
  }

  static public Map<String, OntologyTerm> loadGO(Reader  content,
		                                         boolean regulations) throws IOException {
    Map<String, OntologyTerm> ontology = new HashMap<String, OntologyTerm>(23000);
    String                    line;

    // Read file
    BufferedReader in = new BufferedReader(content);
    while ((line=in.readLine()) != null) {
       if ("[Term]".equals(line)) {
          OntologyTerm term = new OntologyTerm();
          while ((line=in.readLine()) != null) {
             int i = line.indexOf(':');
             if (i < 1) break;
             String key = line.substring(0, i);
             if ("id".equals(key))
                term.setID(line.substring(i+2));
             else if ("namespace".equals(key))
                term.setCategory(line.substring(i+2).intern());
             else if ("name".equals(key))
                term.setName(line.substring(i+2));
             else if ("is_a".equals(key))
                term.addParent(line.substring(i+2, line.indexOf(' ', i+2)));
             else if ("alt_id".equals(key))
                ontology.put(line.substring(i+2), term);
             else if ("relationship".equals(key)) {
            	String role = line.substring(i+2, (i = line.indexOf(' ', i+2)));
                if ("part_of".equals(role)) {
                    term.addParent(line.substring(i+1, line.indexOf(' ', i+2)));
                } else if (regulations && role.endsWith("regulates")) {
                	String direction = null;
                	if (role.endsWith("positively_regulates")) direction = OntologyTerm.ROLE_POSITIVELY;
                	else
                    if (role.endsWith("negatively_regulates")) direction = OntologyTerm.ROLE_NEGATIVELY;
                	if (direction != null) term.addTarget(line.substring(i+1, line.indexOf(' ', i+2)), direction);
                }
             }
          }
          ontology.put(term.getID(), term);
       }
    }
    in.close();
    return ontology;
  }

  static private boolean hasParent(OntologyTerm              node,
                                   OntologyTerm[]            refs,
                                   Map<String, OntologyTerm> ontology,
                                   Map<String, String>       hits) {
     if (hits.containsKey(node.getID()))
        return true;
     StringBuffer m = null;
     for (OntologyTerm t : refs) {
         if (hasParent(node, t, ontology)) {
            if (m == null) {
               m = new StringBuffer(t.getID());
            } else {
               m.append(',').append(t.getID());
            }
         }
     }
     if (m == null)
        return false;
     hits.put(node.getID(), m.toString());
     return true;
  }

  static private boolean hasParent(OntologyTerm              node,
		                           OntologyTerm              ref,
		                           Map<String, OntologyTerm> ontology) {
      if (node.equals(ref)) return true;

      String[] par = node.getParents();
      if (par == null) return false;

      for (int i=0; i<par.length; i++) {
          if (hasParent(ontology.get(par[i]), ref, ontology))
              return true;
      }
      return false;
  }

}
