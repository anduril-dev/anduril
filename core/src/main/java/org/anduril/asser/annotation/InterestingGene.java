package org.anduril.asser.annotation;

/**
 * Gene Ontology annotated gene of interest.
 */
public class InterestingGene extends GOTarget {

	private final String id;

	public InterestingGene(String identifier) {
		id = identifier;
	}

    public String getId() {
		return id;
	}

    @Override
    public String toString() {
        return id;
    }

}
