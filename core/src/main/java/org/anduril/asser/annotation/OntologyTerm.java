package org.anduril.asser.annotation;

/**
 * Represents a term within an ontology DAG.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 */
public class OntologyTerm {

	static public final String ROLE_POSITIVELY = "+";
	static public final String ROLE_NEGATIVELY = "-";

	private String id;
	
	private String category = "[none]";
	
	private String[] parents = null;
	
	private String[][] targets = null;
	
	private String name;
	
	public void addParent(String parent) {
	  if (parents == null) {
	     parents = new String[] { parent };
	  } else {
	     String[] nPar = new String[parents.length+1];
	     nPar[parents.length] = parent;
	     System.arraycopy(parents, 0,
	                      nPar,    0,
	                      parents.length);
	     parents = nPar;
	  }
	}

	public void addTarget(String target, String role) {
	  if (targets == null) {
	     targets = new String[][] {{ target, role }};
	  } else {
	     String[][] nTar = new String[targets.length+1][];
	     nTar[targets.length] = new String[] { target, role };
	     System.arraycopy(targets, 0,
	                      nTar,    0,
	                      targets.length);
	     targets = nTar;
	  }
	}

	public boolean hasTargets() {
		return ((targets != null) && (targets.length > 0));
	}

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String[] getParents() {
        return parents;
    }

    public String[][] getTargets() {
        return targets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
    	return id+" "+name;
    }

}
