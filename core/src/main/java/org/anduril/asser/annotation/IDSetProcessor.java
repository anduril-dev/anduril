package org.anduril.asser.annotation;

import java.util.List;
import java.util.Set;

/**
 * Defines a common interface for the output procesessors of
 * {@link org.anduril.asser.annotation.IDConverter}.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.21
 */
public interface IDSetProcessor {

    /**
     * 
     * @param sourceKey The original query key
     * @param targetDbs List of target databases for the query
     * @param toId      Flags indicating type of interest for each
     *                  target database (true for accession numbers and
     *                  false for display names)
     * @param ids       List of identifiers for all target databases
     */
    public void processIDs(String                sourceKey,
                           String[]              targetDbs,
                           boolean[]             toId,
                           List<Set<DatabaseId>> ids);

}
