package org.anduril.asser.annotation;

import org.anduril.asser.AsserUtil;

import java.io.PrintStream;
import java.util.List;
import java.util.Set;

/**
 * This output mode produces multiple lines for one source key
 * matching several target keys.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.21
 */
public class IDTableWriter implements IDSetProcessor {

    private final boolean hideI;
    private final boolean hideS;
    private final String  sep;
    private final String  na;

    private PrintStream out = System.out;

    IDTableWriter(boolean hideIndicator,
                  boolean hideSource,
                  String  separator,
                  String  missingValueSymbol) {
        hideI = hideIndicator;                  
        hideS = hideSource;
        sep   = separator;
        na    = missingValueSymbol;
    }

    PrintStream setStream(PrintStream stream) {
    	PrintStream old = out;
    	out = stream;
    	return old;
    }

    public void processIDs(String                sourceKey,
                           String[]              targetDbs,
                           boolean[]             toId,
                           List<Set<DatabaseId>> ids) {
        if (ids == null)
            return;
        printLines((hideS ? null : sourceKey), 0, false, ids, toId); 
    }

    private void printLines(String                prefix, // NOPMD
                            int                   level,
                            boolean               hasHits,
                            List<Set<DatabaseId>> ids,
                            boolean[]             toId) {
        prefix = (prefix == null) ? AsserUtil.EMPTY_STRING : prefix+sep;

        Set<DatabaseId> set = ids.get(level);
        if (set.isEmpty()) {
            if (level < toId.length-1) {
                printLines(prefix+na, level+1, hasHits, ids, toId);
            } else {
                if (hasHits || (!hideI && !hideS)) {
                   out.print(prefix);
                   out.println(na);
                }
            }
        } else
        for (DatabaseId id: set) {
            String value = toId[level] ? id.getId() : id.getLabel();
            if (level < toId.length-1) {
                printLines(prefix+value, level+1, true, ids, toId);
            } else {
                out.print(prefix);
                out.println(value);
            }
        }
    }

}
