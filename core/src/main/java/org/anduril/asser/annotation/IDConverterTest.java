package org.anduril.asser.annotation;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Unit tests for {@link org.anduril.asser.annotation.IDConverter}.
 * 
 * @author Marko Laakso
 */
public class IDConverterTest {

	@Test
	public void testClean() {
		final String A = "AA";

		assertSame(A,        IDConverter.clean(A));
		assertEquals(A,      IDConverter.clean("\"AA\""));
		assertEquals(A,      IDConverter.clean("\'AA\'"));
		assertEquals(A,      IDConverter.clean(",AA,"));
		assertEquals(A,      IDConverter.clean("\",AA,\""));
		assertEquals("A\"A", IDConverter.clean("A\"A"));
		assertEquals("A,A",  IDConverter.clean("A,A"));
		assertEquals("",     IDConverter.clean("\",\'"));
	}

	@Test
	public void testMergeRegions() {
		List<Object[]> values = new ArrayList<Object[]>(5);
		values.add(new Object[] {1, "A", 10, 20});
		values.add(new Object[] {2, "B", 21, 40});
		values.add(new Object[] {3, "B", 41, 50});
		values.add(new Object[] {4, "B", 52, 60});
		values.add(new Object[] {5, "B", 55, 57});

		IDConverter.mergeRegions(values);

		assertEquals(3, values.size());
		Object[] i;
		i = values.get(0);
		assertEquals(1,  i[0]);
		assertEquals(20, i[3]);
		i = values.get(1);
		assertEquals(2,  i[0]);
		assertEquals(50, i[3]);
		i = values.get(2);
		assertEquals(4,  i[0]);
		assertEquals(60, i[3]);
	}

}
