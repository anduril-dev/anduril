package org.anduril.asser.annotation;

import java.util.ArrayList;
import java.util.List;

/**
 * A biological concept that may be annotated with Gene Ontology terms and a score.
 */
public class GOTarget implements Comparable<GOTarget> {

    private double         score;
    private List<String[]> goTerms;

    public boolean isEmpty() {
    	return ((goTerms==null) || goTerms.isEmpty());
    }

    public int size() {
    	return (goTerms==null) ? 0 : goTerms.size();
    }

    public void addGOTerm(String id, String gene) {
        if (goTerms == null)
            goTerms = new ArrayList<String[]>();
        goTerms.add(new String[] {id, gene});
    }

    public String[][] getGOTerms() {
        if (goTerms == null)
            return new String[0][];
        else
            return (String[][])goTerms.toArray(new String[goTerms.size()][]);
    }

    public void removeGOTerm(int index) {
    	goTerms.remove(index);
    }

    public void removeGOTerms() {
        goTerms = null;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
 
    /**
     * Compares the scores of the instance and the given item.
     */
    public int compareTo(GOTarget r) {
        if (r.score > score) return -1;
        if (r.score < score) return  1;
        return 0;
    }

}
