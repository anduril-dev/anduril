package org.anduril.asser.annotation;

/**
 * Evidence codes for the assignments of the Gene Ontology annotations.
 *
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link org.anduril.asser.Version Version} 3.17
 */
public enum GOEvidence {

	EXP("Inferred from Experiment"),
	IDA("Inferred from Direct Assay"),
	IPI("Inferred from Physical Interaction"),
	IMP("Inferred from Mutant Phenotype"),
	IGI("Inferred from Genetic Interaction"),
	IEP("Inferred from Expression Pattern"),
	ISS("Inferred from Sequence or Structural Similarity"),
	ISO("Inferred from Sequence Orthology"),
	ISA("Inferred from Sequence Alignment"),
	ISM("Inferred from Sequence Model"),
	IGC("Inferred from Genomic Context"),
	RCA("inferred from Reviewed Computational Analysis"),
	TAS("Traceable Author Statement"),
	NAS("Non-traceable Author Statement"),
	IC ("Inferred by Curator"),
	ND ("No biological Data available"),
	IEA("Inferred from Electronic Annotation");

	private final String title;

	GOEvidence(String label) {
		title=label;
	}

	/** Returns the human readable name for the evidence code. */
	public String getTitle() {
		return title;
	}

}