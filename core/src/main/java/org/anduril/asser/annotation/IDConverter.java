package org.anduril.asser.annotation;

import org.anduril.asser.ArgumentEncoding;
import org.anduril.asser.AsserUtil;
import org.anduril.asser.MainArgument;
import org.anduril.asser.Version;
import org.anduril.asser.db.JDBCConnection;
import org.anduril.asser.db.TypeConverter;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

/**
 * This the executable class of Korvasieni.
 * Korvasieni is a database identifier converter that is based on
 * Ensembl database.
 *  
 * @author Marko Laakso
 */
public class IDConverter {

    /** Name of the default configuration file. This file should be located into the classpath. **/
    static public final String CFG_FILE = "annotationdb.properties"; 

    /** Supported Ensembl object types **/
    static public enum EnsemblType {
        /** Database type for genes and DNA locations */
        Gene,
        /** Database type for messenger RNAs */
        Transcript,
        /** Database type for proteins */
        Translation,
        /** This database type covers all other types */
        Any
    }

    /** Skipping level for the missing values **/
    static public enum SkipLevel {
        /** Report all source identifiers. */
        never,
        /** Skip entries if the source identifier is unknown. */
        source,
        /** Skip entries if there are no target identifiers of any type. */
        target,
        /** Skip entries if any of the target identifiers is missing. */
        any
    }

    /** Output modes **/
    static public enum OutputMode { Line, Table }

    static public final String BIOTYPE                = ".Biotype";
    static public final String DNA_REGION             = ".DNARegion";
    static public final String DNA_BAND               = ".DNABand";
    static public final String DNA_STRAND             = ".DNAStrand";
    static public final String ENSEMBL_GENE_DESC      = ".GeneDesc";
    static public final String ENSEMBL_GENE_NAME      = ".GeneName";
    static public final String ENSEMBL_GENE_ID        = ".GeneId";
    static public final String ENSEMBL_TRANSCRIPT_ID  = ".TranscriptId";
    static public final String ENSEMBL_TRANSLATION_ID = ".TranslationId";

    /** An empty array **/
    static private final Object[] NOTHING = new Object[0];

    /** Ensembl connection **/
    private JDBCConnection con;

    /** Skipping policy for the missing values **/
    private SkipLevel skipLevel = SkipLevel.never;

    /** This stream is used to print additional status information */
    private PrintStream verboseOut = null;

    /**
     * Enables the removal of the quotation marks an some other
     * formating from the input keys.
     */
    private boolean keyCleaning = true;

    /** Skip secondary identifiers such as LGR codes */
    private boolean primaryOnly = false;

    private String goEvidenceFilter = AsserUtil.EMPTY_STRING;

    /**
     * Creates an instance using the default configurations
     * ({@link #CFG_FILE}).
     */
    public IDConverter() throws IOException, SQLException {
        this(CFG_FILE);
    }

    /**
     * Creates an instance using the given configuration file.
     */
    public IDConverter(String configFile) throws IOException, SQLException {
        con = new JDBCConnection(configFile);
    }

    /**
     * Creates an instance using the given configuration file.
     */
    public IDConverter(File configFile) throws IOException, SQLException {
        con = new JDBCConnection(configFile);
    }

    /**
     * Creates an instance using the given configuration stream.
     */
    public IDConverter(InputStream cfg, String resourceName) throws IOException, SQLException {
        con = new JDBCConnection(cfg, resourceName);
    }

    public void setGOEvidenceFilter(String terms) {
    	if (AsserUtil.onlyWhitespace(terms)) {
    		goEvidenceFilter = AsserUtil.EMPTY_STRING;
    		return;
    	}

    	String[]     termA  = AsserUtil.split(terms);
    	GOEvidence[] filter = new GOEvidence[termA.length];

    	for (int i=0; i<termA.length; i++) {
    		try { filter[i] = GOEvidence.valueOf(termA[i]); }
    		catch (IllegalArgumentException err) {
    			throw new IllegalArgumentException("Invalid GO envidence code: "+termA[i], err);
    		}
    	}
    	setGOEvidenceFilter(filter);
    }

    public void setGOEvidenceFilter(GOEvidence[] terms) {
    	if ((terms == null) || (terms.length < 1)) {
    		goEvidenceFilter = AsserUtil.EMPTY_STRING;
    	} else {
    		StringBuffer buffer = new StringBuffer(512);
    		buffer.append("AND (OX.object_xref_id = GOF.object_xref_id) AND (GOF.linkage_type NOT IN ('");
    		for (int i=0; i<terms.length; i++) {
   				if (i>0) buffer.append("','");
   				buffer.append(terms[i].name());
    		}
    		buffer.append("')) ");
    		goEvidenceFilter = buffer.toString();
    	}
    }

    /**
     * Set filter for the unknown values.
     */
    public void setSkipLevel(SkipLevel level) {
        skipLevel = level;
    }

    /**
     * Returns the current filter for the unknown values.
     */
    public SkipLevel getSkipLevel() {
        return skipLevel;
    }

    /** JDBC URL for Ensembl connection **/
    public String getURL() {
      return con.getURL();
    }

    /**
     * Determines whether to remove surrounding formating of
     * the input keys.
     */
    public void setKeyCleaning(boolean value) {
    	keyCleaning = value;
    }

    /**
     * Enables the skipping of the secondary identifiers such as LGR identifiers.
     */
    public void setPrimaryOnly(boolean value) {
    	primaryOnly = value;
    }

    private void processFile(String         filename,
                             String         sourceType,
                             String         sourceDb,
                             boolean        isId,
                             String[]       targetDb,
                             boolean[]      toId,
                             IDSetProcessor processor) throws IOException,
                                                              SQLException {
        Reader in = new FileReader(filename);
        try {
           long sTime = (verboseOut==null) ? 0 : System.currentTimeMillis();
           processReader(in, sourceType, sourceDb, isId, targetDb, toId, processor);
           if (verboseOut != null) {
               verboseOut.println("***** The file processing took "+
                                  ((System.currentTimeMillis()-sTime)/1000.0)+
                                  " seconds.");
           }
        }
        finally { in.close(); }
    }

    private void processReader(Reader         reader,
                               String         sourceType,
                               String         sourceDb,
                               boolean        isId,
                               String[]       targetDb,
                               boolean[]      toId,
                               IDSetProcessor processor) throws IOException,
                                                                SQLException {
        BufferedReader in = new BufferedReader(reader);
        String         line;

        while ((line=in.readLine()) != null) {
            line = line.trim();
            if (line.length() < 1)
                continue;
            processKey(line, sourceType, sourceDb, isId, targetDb, toId, processor);
        }
        in.close();
    }

    private void processKey(String         key,
                            String         sourceType,
                            String         sourceDb,
                            boolean        isId,
                            String[]       targetDbs,
                            boolean[]      toId,
                            IDSetProcessor processor) throws SQLException {
        EnsemblType           st   = EnsemblType.valueOf(sourceType);
        List<Set<DatabaseId>> keys = convert(key, isId, st, sourceDb, targetDbs, toId);

        if ((keys == null) && (skipLevel != SkipLevel.never))
            return; // We can skip these results.
        processor.processIDs(key, targetDbs, toId, keys);
    }

    /**
     * This method converts the given key to a list of corresponding values in the
     * given source databases. Each item of the list corresponds to a type declaration
     * in the source lists. 
     * 
     * @param keyIn      Source key for the query
     * @param isId       Flag indicating whether the source key is
     *                   {@link org.anduril.asser.annotation.DatabaseId}
     *                   identifier of label
     * @param sourceType Type of the source key
     * @param sourceDb   Database name for the source key
     * @param targetDb   A list of database names for the target keys
     * @param toId       A list of flags indicating whether the source keys should refer to
     *                   {@link org.anduril.asser.annotation.DatabaseId}
     *                   identifiers of labels
     * @return A list of key sets or null if the key is not found from the database.
     */
    public List<Set<DatabaseId>> convert(String      keyIn,
                                         boolean     isId,
                                         EnsemblType sourceType,
                                         String      sourceDb,
                                         String[]    targetDb,
                                         boolean[]   toId) throws SQLException {
        List<Set<DatabaseId>> keys;
        Set<DatabaseId>       keySet;
        String                key          = keyIn;
        Object[]              genes        = null;
        Object[]              transcripts  = null;
        Object[]              translations = null;
        boolean               any          = (sourceType == EnsemblType.Any);
        Set<Object>           gS           = (any ? new TreeSet<Object>() : null);
        Set<Object>           tS           = (any ? new TreeSet<Object>() : null);
        Set<Object>           pS           = (any ? new TreeSet<Object>() : null);

        if (keyCleaning) {
        	key = clean(key);
        }

        // Convert public key to database identifiers
        if (sourceType == EnsemblType.Gene || any) {
            genes = idsForKey(key, isId, EnsemblType.Gene, sourceDb, !any);
            if (genes.length > 0) {
               transcripts  = genesToTranscripts(genes);
               translations = transcriptsToTranslations(transcripts);
               if (any) { addResults(genes, gS); addResults(transcripts, tS); addResults(translations, pS); }
            } else if (!any) return null;
        }
        if (sourceType == EnsemblType.Transcript || any) {
            transcripts = idsForKey(key, isId, EnsemblType.Transcript, sourceDb, !any);
            if (transcripts.length > 0) {
               translations = transcriptsToTranslations(transcripts);
               genes        = transcriptsToGenes(transcripts);
               if (any) { addResults(genes, gS); addResults(transcripts, tS); addResults(translations, pS); }
            } else if (!any) return null;
        }
        if (sourceType == EnsemblType.Translation || any) {
            translations = idsForKey(key, isId, EnsemblType.Translation, sourceDb, !any);
            if (translations.length > 0) {
               transcripts = translationsToTranscripts(translations);
               genes       = transcriptsToGenes(transcripts);
               if (any) { addResults(genes, gS); addResults(transcripts, tS); addResults(translations, pS); }
            } else if (!any) return null;
        }
        if (any) {
            genes        = gS.isEmpty() ? NOTHING : gS.toArray();
            transcripts  = tS.isEmpty() ? NOTHING : tS.toArray();
            translations = pS.isEmpty() ? NOTHING : pS.toArray();
            if (genes == NOTHING && transcripts == NOTHING && translations == NOTHING)
               return null;
        }
        if (verboseOut != null) {
            verboseOut.printf("***** %s matched: %d genes, %d transcripts, %d translations.%n",
                              key, genes.length, transcripts.length, translations.length);
        }

        boolean hasHits = false;
        keys = new ArrayList<Set<DatabaseId>>(targetDb.length);
        for (int t=0; t<targetDb.length; t++) {
            // This comparator sorts hits based on the selected criteria (id/label)
            keySet = new TreeSet<DatabaseId>(new IDComparator(toId[t]));
            // Convert database identifiers to public keys
            idToKeys(EnsemblType.Gene,        targetDb[t],        genes, keySet);
            idToKeys(EnsemblType.Transcript,  targetDb[t],  transcripts, keySet);
            idToKeys(EnsemblType.Translation, targetDb[t], translations, keySet);
            if (!toId[t] && DNA_REGION.equals(targetDb[t])) {
                mergeRegions(keySet);
            }
            if (keySet.isEmpty()) {
                if (skipLevel == SkipLevel.any) return null;
            } else {
                hasHits = true;
            }
            keys.add(keySet);
        }
        if (!hasHits && (skipLevel == SkipLevel.target))
            return null;
        return keys;
    }

    /**
     * Remove the surrounding formating of the string.
     */
    static public String clean(String keyIn) {
    	String key = keyIn;
    	int    l   = keyIn.length();
    	int    s;
    	int    e;

    	start: for (s=0; s<keyIn.length(); s++) {
    		switch (keyIn.charAt(s)) {
               case     ',':
               case     '\"':
               case     '\'':
                        break;
			   default: break start;
			}
    	}
    	end: for (e=l-1; e>s; e--) {
    		switch (keyIn.charAt(e)) {
               case     ',':
               case     '\"':
               case     '\'':
                        break;
			   default: break end;
			}
    	}
    	if ((s>0) || (e<l-1)) {
    		key = key.substring(s, e+1);
    	}
    	return key;
    }

    static private void addResults(Object[] additions, Set<Object> set) {
        for (Object o : additions)
            set.add(o);
    }

    static private String sqlAssignment(Object[] items) {
        if (items.length == 1) return "= ?)";

        StringBuffer query = new StringBuffer("IN (");
        for (int i=0; i<items.length; i++) {
            if (i>0) query.append(',');
            query.append('?');
        }
        query.append("))");
        return query.toString();
    }

    private Object[] idToId(String sql, Object[] sourceIds) throws SQLException {
    	if (sourceIds.length < 1) return NOTHING;
        String query = sql+sqlAssignment(sourceIds);
        //if (verboseOut != null) verboseOut.println("***** Converting internal identifiers: "+query);
        List<Object[]> values = con.fetchObjects(query, sourceIds);
        return TypeConverter.columnOf(values, 0);
    }

    private Object[] translationsToTranscripts(Object[] translations) throws SQLException {
        return idToId("SELECT DISTINCT transcript_id FROM translation WHERE (translation_id ",
                      translations);
    }

    private Object[] transcriptsToTranslations(Object[] transcripts) throws SQLException {
        return idToId("SELECT DISTINCT translation_id FROM translation WHERE (transcript_id ",
                      transcripts);
    }

    private Object[] transcriptsToGenes(Object[] transcripts) throws SQLException {
        return idToId("SELECT DISTINCT gene_id FROM transcript WHERE (transcript_id ",
                      transcripts);
    }

    private Object[] genesToTranscripts(Object[] genes) throws SQLException {
        return idToId("SELECT DISTINCT transcript_id FROM transcript WHERE (gene_id ",
                      genes);
    }

    private Object[] idsForKey(String      key,
                               boolean     isId,
                               EnsemblType sourceType,
                               String      sourceDb,
                               boolean     checkType) throws SQLException {
        List<Object[]> values;
        if (BIOTYPE.equals(sourceDb)) {
            if (EnsemblType.Gene.equals(sourceType)) {
                values = con.fetchObjects(
                          "SELECT DISTINCT gene_id FROM gene WHERE (biotype = ?)",
                          key);
            } else if (EnsemblType.Transcript.equals(sourceType)) {
                values = con.fetchObjects(
                          "SELECT DISTINCT transcript_id FROM transcript WHERE (biotype = ?)",
                          key);
            } else {
                if (checkType)
                    throw new IllegalArgumentException("Biotypes cannot be queried for "+sourceType+'.');
                return NOTHING;
            }
        } else
        if (DNA_BAND.equals(sourceDb)) {
            if (checkType) typeValidation(EnsemblType.Gene, sourceType, sourceDb);
            else if (!EnsemblType.Gene.equals(sourceType)) return NOTHING;
            if (isId) {
                values = con.fetchObjects(
                          "SELECT DISTINCT gene_id "+
                          "FROM   gene G, karyotype K " +
                          "WHERE  (K.seq_region_id     = G.seq_region_id)  AND "+
                          "       (K.karyotype_id      = ?)                AND "+
                          "       (K.seq_region_start <= G.seq_region_end) AND "+
                          "       (K.seq_region_end   >= G.seq_region_start)",
                          key);
            } else {
                String[] keySplit = key.split("p|q");
                values = con.fetchObjects(
                   "SELECT DISTINCT G.gene_id "+
                   "FROM   gene G, seq_region SR, karyotype K "+
                   "WHERE  (G.seq_region_id     = SR.seq_region_id)   AND "+
                   "       (K.seq_region_id     = SR.seq_region_id)   AND "+
                   "       (SR.name             = ?)                  AND "+
                   "       (K.band              = ?)                  AND "+
                   "       (G.seq_region_end   >= K.seq_region_start) AND "+
                   "       (G.seq_region_start <= K.seq_region_end)   AND "+
                   "       (SR.coord_system_id  = ("+
                   "         SELECT coord_system_id "+
                   "         FROM   coord_system "+
                   "         WHERE  (name   = 'chromosome') AND "+
                   "                (attrib = 'default_version')))",
                   keySplit[0],
                   key.substring(keySplit[0].length()));
            }
        } else
        if (DNA_REGION.equals(sourceDb)) {
           if (checkType) typeValidation(EnsemblType.Gene, sourceType, sourceDb);
           else if (!EnsemblType.Gene.equals(sourceType)) return NOTHING;
           if (isId) {
               values = con.fetchObjects(
                         "SELECT DISTINCT gene_id FROM gene WHERE (seq_region_id = ?)",
                         key);
           } else {
               InterestingRegion location = new InterestingRegion(key);
               values = con.fetchObjects(
                  "SELECT DISTINCT G.gene_id "+
                  "FROM   gene G, seq_region SR "+
                  "WHERE  (G.seq_region_id     = SR.seq_region_id) AND "+
                  "       (SR.name             = ?)                AND "+
                  "       (G.seq_region_end   >= ?)                AND "+
                  "       (G.seq_region_start <= ?)                AND "+
                  "       (SR.coord_system_id  = ("+
                  "         SELECT coord_system_id "+
                  "         FROM   coord_system "+
                  "         WHERE  (name   = 'chromosome') AND "+
                  "                (attrib = 'default_version')))",
                  location.getChromosome(),
                  location.getStart(),
                  location.getEnd());
           }
        } else
        if (DNA_STRAND.equals(sourceDb)) {
           if (checkType) typeValidation(EnsemblType.Gene, sourceType, sourceDb);
           else if (!EnsemblType.Gene.equals(sourceType)) return NOTHING;
           if (isId) {
               values = con.fetchObjects(
                         "SELECT DISTINCT gene_id FROM gene WHERE (seq_region_id = ?)",
                         key);
           } else {
               values = con.fetchObjects(
                         "SELECT DISTINCT gene_id FROM gene WHERE (seq_region_strand = ?)",
                         key);
           }
        } else
        if (ENSEMBL_GENE_DESC.equals(sourceDb)) {
           if (checkType) typeValidation(EnsemblType.Gene, sourceType, sourceDb);
           else if (!EnsemblType.Gene.equals(sourceType)) return NOTHING;
           if (isId) return new Object[] { key };
           values = con.fetchObjects(
              "SELECT DISTINCT gene_id FROM gene WHERE (description LIKE ?)",
              "%"+key.replaceAll("%","[%]")+"%");
        } else
        if (ENSEMBL_GENE_NAME.equals(sourceDb)) {
            if (checkType) typeValidation(EnsemblType.Gene, sourceType, sourceDb);
            else if (!EnsemblType.Gene.equals(sourceType)) return NOTHING;
            values = con.fetchObjects(
              "SELECT DISTINCT G.gene_id "+
              "FROM   gene G, xref X "+
              "WHERE  (X."+(isId ? "dbprimary_acc": "display_label")+" = ?) AND "+
              "       (X.xref_id = G.display_xref_id)"
            , key);
        } else
        if (ENSEMBL_GENE_ID.equals(sourceDb)) {
            if (checkType) typeValidation(EnsemblType.Gene, sourceType, sourceDb);
            else if (!EnsemblType.Gene.equals(sourceType)) return NOTHING;
            values = con.fetchObjects(isId ?
              "SELECT DISTINCT gene_id FROM gene WHERE (  gene_id = ?)" :
              "SELECT DISTINCT gene_id FROM gene WHERE (stable_id = ?)", key);
        } else
        if (ENSEMBL_TRANSCRIPT_ID.equals(sourceDb)) {
            if (checkType) typeValidation(EnsemblType.Transcript, sourceType, sourceDb);
            else if (!EnsemblType.Transcript.equals(sourceType)) return NOTHING;
            values = con.fetchObjects(isId ?
              "SELECT DISTINCT transcript_id FROM transcript WHERE (transcript_id = ?)" :
              "SELECT DISTINCT transcript_id FROM transcript WHERE (    stable_id = ?)", key);
        } else
        if (ENSEMBL_TRANSLATION_ID.equals(sourceDb)) {
            if (checkType) typeValidation(EnsemblType.Translation, sourceType, sourceDb);
            else if (!EnsemblType.Translation.equals(sourceType)) return NOTHING;
            values = con.fetchObjects(isId ?
              "SELECT DISTINCT translation_id FROM translation WHERE (translation_id = ?)" :
              "SELECT DISTINCT translation_id FROM translation WHERE (     stable_id = ?)", key);
        } else {
            values = con.fetchObjects(
              "SELECT DISTINCT OX.ensembl_id "+
              "FROM   object_xref OX, xref X, external_db ED "+
              "WHERE  (OX.ensembl_object_type = ?)                AND "+
              "       (OX.xref_id             = X.xref_id)        AND "+
              "       (ED.external_db_id      = X.external_db_id) AND "+
              "       (ED.db_name             = ?)                AND "+
              "       (X."+(isId ? "dbprimary_acc": "display_label")+" = ?)"
            , sourceType.toString(), sourceDb, key);
        }
        return TypeConverter.columnOf(values, 0);
    }

    /**
     * Convert given identifier to the corresponding keys of the given type.
     *
     * @param  targetType   Type of the keys should match the ID type.
     * @param  targetDb     Database name for the key
     * @param  ids          Query identifiers
     * @param  keySet       Return values are added into this target set.
     * @throws SQLException In case Ensembl connection fails.
     */
    public void idToKeys(EnsemblType     targetType,
                         String          targetDb,
                         Object[]        ids,
                         Set<DatabaseId> keySet) throws SQLException {
        if (ids.length < 1) return;

        List<Object[]> values;
        if (BIOTYPE.equals(targetDb)) {
            if (EnsemblType.Gene == targetType)
               values = con.fetchObjects("SELECT DISTINCT gene_id, biotype FROM gene WHERE (gene_id "+sqlAssignment(ids), ids);
            else if (EnsemblType.Transcript == targetType)
               values = con.fetchObjects("SELECT DISTINCT transcript_id, biotype FROM transcript WHERE (transcript_id "+sqlAssignment(ids), ids);
            else
               return;
        } else
        if (DNA_BAND.equals(targetDb)) {
            if (EnsemblType.Gene != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT K.karyotype_id, Concat(SR.name, K.band) "+
              "FROM   gene G, karyotype K, seq_region SR "+
              "WHERE  (G.gene_id       "+sqlAssignment(ids)+"  AND "+
              "       (G.seq_region_id = K.seq_region_id)      AND "+
              "       (G.seq_region_id = SR.seq_region_id)     AND "+
              "       (SR.coord_system_id IN ( "+
              "        SELECT coord_system_id "+
              "        FROM   coord_system "+
              "        WHERE  (name = 'chromosome')))          AND "+
              "       (K.seq_region_start <= G.seq_region_end) AND "+
              "       (K.seq_region_end   >= G.seq_region_start) "+
              (primaryOnly ? "AND (SR.name NOT LIKE '%\\_%') " : "")+
              "ORDER  BY 1"
            , ids);
        } else
        if (DNA_REGION.equals(targetDb)) {
            if (EnsemblType.Gene != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT SR.seq_region_id, "+
              "                SR.name, G.seq_region_start, G.seq_region_end "+
              "FROM   gene G, seq_region SR "+
              "WHERE  (G.gene_id    "+sqlAssignment(ids)+" AND "+
              "       (G.seq_region_id = SR.seq_region_id) AND "+
              "       (SR.coord_system_id IN ( "+
              "        SELECT coord_system_id "+
              "        FROM   coord_system "+
              "        WHERE  (name = 'chromosome'))) "+
              (primaryOnly ? "AND (SR.name NOT LIKE '%\\_%') " : "")+
              "ORDER  BY 2,3,4"
            , ids);
            mergeRegions(values);
            for (Object[] row : values) {
              row[1] = row[1]+":"+row[2]+"-"+row[3];
            }
        } else
        if (DNA_STRAND.equals(targetDb)) {
            if (EnsemblType.Gene != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT seq_region_id, seq_region_strand FROM gene WHERE (gene_id "+sqlAssignment(ids)
            , ids);
        } else
        if (ENSEMBL_GENE_NAME.equals(targetDb)) {
            if (EnsemblType.Gene != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT X.dbprimary_acc, X.display_label "+
              "FROM   gene G, xref X "+
              "WHERE  (X.xref_id = G.display_xref_id) AND "+
              "       (G.gene_id "+sqlAssignment(ids)
            , ids);
        } else
        if (ENSEMBL_GENE_DESC.equals(targetDb)) {
            if (EnsemblType.Gene != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT gene_id, description FROM gene WHERE (gene_id "+sqlAssignment(ids), ids);
        } else
        if (ENSEMBL_GENE_ID.equals(targetDb)) {
            if (EnsemblType.Gene != targetType) return;
            String sql = "SELECT DISTINCT gene_id, stable_id FROM gene WHERE (gene_id "+sqlAssignment(ids);
            if (primaryOnly) {
            	sql += " AND (source LIKE 'ensembl%')";
            }
            values = con.fetchObjects(sql, ids);
        } else
        if (ENSEMBL_TRANSCRIPT_ID.equals(targetDb)) {
            if (EnsemblType.Transcript != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT transcript_id, stable_id FROM transcript WHERE (transcript_id "+sqlAssignment(ids), ids);
        } else
        if (ENSEMBL_TRANSLATION_ID.equals(targetDb)) {
            if (EnsemblType.Translation != targetType) return;
            values = con.fetchObjects(
              "SELECT DISTINCT translation_id, stable_id FROM translation WHERE (translation_id "+sqlAssignment(ids), ids);
        } else {
        	boolean  useGOf = (!goEvidenceFilter.isEmpty() && "GO".equals(targetDb)); 
            Object[] allIds = new Object[ids.length+2];
            System.arraycopy(ids, 0, allIds, 2, ids.length);
            allIds[0] = targetType.toString();
            allIds[1] = targetDb;
            values = con.fetchObjects(
              "SELECT DISTINCT X.dbprimary_acc, X.display_label "+
              "FROM   object_xref OX, xref X, external_db ED "+
              (useGOf ? ", ontology_xref GOF " : AsserUtil.EMPTY_STRING)+
              "WHERE  (OX.xref_id             = X.xref_id)        AND "+
              "       (ED.external_db_id      = X.external_db_id) AND "+
              "       (OX.ensembl_object_type = ?)                AND "+
              "       (ED.db_name             = ?)                AND "+
              "       (OX.ensembl_id "+sqlAssignment(ids)+
              (useGOf ? goEvidenceFilter : AsserUtil.EMPTY_STRING)+
              " ORDER BY X.display_label"
            , allIds);
        }
        for (Object[] row : values) {
            keySet.add(new DatabaseId(row[0], row[1]));
        }
    }

    /**
     * Produces a list of database names and their descriptions.
     */
    public List<Object[]> getDatabases(boolean withContent) throws SQLException {
        List<Object[]> values = con.fetchObjects(
          "SELECT DISTINCT db_name, max(db_display_name) "+
          "FROM   external_db "+
          (withContent ?
                  "WHERE external_db_id IN (SELECT DISTINCT external_db_id FROM xref) " :
                  AsserUtil.EMPTY_STRING)+
          "GROUP  BY db_name "+
          "ORDER  BY db_name");
        return values;
    }

    /** Close database connection **/
    public void close() {
      con.close();
    }

    /**
     * Removes the overlapping regions and merges the continuous regions together.
     **/
    static void mergeRegions(Set<DatabaseId> keySet) {
        if (keySet.size() < 2) return;
        DatabaseId prev = null;
        for (Iterator<DatabaseId> it = keySet.iterator(); it.hasNext(); ) {
            DatabaseId curr = it.next();
            if (prev != null) {
                String pS  = prev.getLabel();
                String cS  = curr.getLabel();
                int    pCL = pS.indexOf(':');
                int    cCL = cS.indexOf(':');
                String cC  = cS.substring(0, cCL);
                if (cC.equals(pS.substring(0, pCL))) {
                    int pLL = pS.indexOf('-', pCL);
                    int cLL = cS.indexOf('-', cCL);
                    long pE = Long.parseLong(pS.substring(pLL+1));
                    long cB = Long.parseLong(cS.substring(cCL+1, cLL));
                    if (cB-1 <= pE) {
                        long cE = Long.parseLong(cS.substring(cLL+1));
                        if (cE-1 >= pE) {
                            prev.setLabel(pS.substring(0,pLL+1)+cE);
                        }
                        it.remove();
                    }
                }
            }
            prev = curr;
        }
    }

    /**
     * Combine overlapping regions. This method assumes that
     * the regions are already in correct order.
     */
    static void mergeRegions(List<Object[]> values) {
        for (int i=1; i<values.size(); i++) {
           Object[] prev = values.get(i-1);
           Object[] curr = values.get(i);
           if (prev[1].equals(curr[1])) {
              long startC = TypeConverter.toLong(curr[2]);
              long endP   = TypeConverter.toLong(prev[3]);
              long endC   = TypeConverter.toLong(curr[3]);
              if (startC-1 <= endP) {
                 if (endC-1 >= endP) {
                    prev[3] = curr[3];
                 }
                 values.remove(i);
              }
           }
        }      
    }

    static private void typeValidation(EnsemblType expected,
                                       EnsemblType given,
                                       String      db) {
        if (!expected.equals(given))
           throw new IllegalArgumentException(db+" type is "+expected+" (not "+given+").");
    }

    /**
     * Tests if the given database name is valid.
     */
    public void checkAvailable(String db) throws SQLException {
      if (BIOTYPE.equals(db)              ||
          DNA_BAND.equals(db)              ||
          DNA_REGION.equals(db)            ||
          DNA_STRAND.equals(db)            ||
          ENSEMBL_GENE_DESC.equals(db)     ||
          ENSEMBL_GENE_NAME.equals(db)     ||
          ENSEMBL_GENE_ID.equals(db)       ||
          ENSEMBL_TRANSCRIPT_ID.equals(db) ||
          ENSEMBL_TRANSLATION_ID.equals(db))
         return;
      Object obj = con.fetchValue("SELECT count(*) FROM external_db WHERE (db_name = ?)",
                                  true, db);
      int count = TypeConverter.toInt(obj);
      if (count < 1)
         throw new IllegalArgumentException("Invalid database reference: "+db);
    }

    /**
     * This is the startup method of Korvasieni.
     */
    static public void main(String[] argv) throws IOException, SQLException {
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argPrimary       = MainArgument.add("p","Select primary identifiers only", args);
        MainArgument argRaw           = MainArgument.add("r","Use raw input with the formating characters", args);
        MainArgument argVerbose       = MainArgument.add("v","Verbose output mode", args);
        MainArgument argShowURL       = MainArgument.add("d","Print current database address",args);
        MainArgument argExplainOutput = MainArgument.add("e","Explain output format",args);
        MainArgument argListAllDb     = MainArgument.add("l","List supported databases (sourceDb/targetDb)",args);
        MainArgument argListUsedDb    = MainArgument.add("L","List supported databases with some content (takes awhile)",args);
        MainArgument argHideIndicator = MainArgument.add("h","Hide key status indicator (+/-)",args);
        MainArgument argHideSource    = MainArgument.add("H","Turn source key echo off",args);
        MainArgument argListGOFs      = MainArgument.add("goL","List Gene Ontology evidence codes",args);
        MainArgument argGOFilter      = MainArgument.add("goF",   "codes", "Rejected GO annotation evidences",args);
        MainArgument argConvertKey    = MainArgument.add("c",     "key",   "Convert the given key",args);
        MainArgument argConvertFile   = MainArgument.add("f",     "file",  "Convert all keys of the given file",args);
        MainArgument argOutputFile    = MainArgument.add("out",   "file",  "Write output to this file",args);
        MainArgument argColumns       = MainArgument.add("cols",  "list",  "Column count pattern for targetDbs (cl1_cl2_cl3 ...)",args);
        MainArgument argSeparator     = MainArgument.add("s",     "sep",   "Delimiter between the target keys", "\\s",args);
        MainArgument argNA            = MainArgument.add("na",    "symbol","Missing value symbol for fixed column counts", "?",args);
        MainArgument argLayout        = MainArgument.add("layout","mode",  "Output layout "+Arrays.toString(OutputMode.values()), OutputMode.Line.name(),args);
        MainArgument argType          = MainArgument.add("t",     "type",  "Source type "+Arrays.toString(EnsemblType.values()), EnsemblType.Any.name(),args);
        MainArgument argSkipLevel     = MainArgument.add("skip",  "level", "Drop unknown values "+Arrays.toString(SkipLevel.values()), SkipLevel.never.name(),args);
        String[] argLeft;
        try { argLeft = MainArgument.parseInput(argv, args, 0, -1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            printSynopsis(args);
            return;
        }

        IDConverter app;
        try { app = new IDConverter(); }
        catch (MissingResourceException e) {
            System.err.println("Configuration error in "+CFG_FILE+": "+e.getKey()+" is not defined.");
            return;
        }
        if (argVerbose.isUsed()) { app.verboseOut = System.err; }

        if (argListAllDb.isUsed() || argListUsedDb.isUsed()) {
            System.out.println("External databases supported:");
            for (Object[] db : app.getDatabases(argListUsedDb.isUsed())) {
                System.out.println(db[0]+" ("+db[1]+')');
            }
            System.out.println("---");
            System.out.println(BIOTYPE+" (Biotypes of genes and transcripts)");
            System.out.println(DNA_BAND+" (Chromosome bands)");
            System.out.println(DNA_REGION+" (Genomic ranges [chromosome:start-end])");
            System.out.println(DNA_STRAND+" (Chromosome strands [1=forward,-1=backward])");
            System.out.println(ENSEMBL_GENE_DESC+" (Ensembl descriptions for genes)");
            System.out.println(ENSEMBL_GENE_NAME+" (Ensembl names for genes)");
            System.out.println(ENSEMBL_GENE_ID+" (Ensembl IDs for genes)");
            System.out.println(ENSEMBL_TRANSCRIPT_ID+" (Ensembl IDs for transcripts)");
            System.out.println(ENSEMBL_TRANSLATION_ID+" (Ensembl IDs for translations)");
        } else
        if (argListGOFs.isUsed()) {
        	System.out.println("The Gene Ontology evidence codes supported:");
        	for (GOEvidence code : GOEvidence.values()) {
        		System.out.println(code.name()+" ("+code.getTitle()+')');
        	}
        	System.out.println("A comma separated list of these codes can be given to -goF parameter.");
        } else
        if (argShowURL.isUsed()) {
            System.out.println(app.getURL());
        } else
        if (argExplainOutput.isUsed()) {
            System.out.println("Output of Korvasieni v."+Version.getVersion()+"\n");
            System.out.println(
                "Output consists of lines corresponding the query keys. "+
                "Each entry starts with the original "+
                "query key followed by an indicator column, "+
                "which tells whether the key was recognized by the "+
                "database (+) or not (-). The first two columns can be omitted using "+
                "the relevant options. Target keys are listed after "+
                "the indicator column in case they are found. The keys are separated "+
                "by a delimiter string that can be defined by a command line argument."
            );
            System.out.println("\nCharacter substituents for the target key delimiter:");
            for (ArgumentEncoding rep : ArgumentEncoding.values()) {
                System.out.println(rep.symbol+" = "+rep.description);
            }
            System.out.println(
                "\nThe number of columns reserved for each target database can be "+
                "defined using a column count pattern. The pattern is an underscore "+
                "separated list of column counts. All keys can be printed using 'A' "+
                "instead of an integer. The unlimited value is used as a default if "+
                "the number of counters is less than the number of target databases. "+
                "A special missing key symbol is used whenever the target database "+
                "provides too few keys to fill all the reserved columns. " +
                "An example pattern for three target databases could be '1_A_3'."
            );
        } else {
            if (argLeft.length < 2) {
               printSynopsis(args);
               return;
            }
            app.skipLevel = SkipLevel.valueOf(argSkipLevel.getValue());
            String   sourceDb = argLeft[0];
            String[] targetDb = Arrays.copyOfRange(argLeft, 1, argLeft.length);
            boolean  isId     = (sourceDb.charAt(0) == '_');
            boolean  toId[]   = new boolean[targetDb.length];
            if (isId) { sourceDb = sourceDb.substring(1); }
            app.checkAvailable(sourceDb);
            for (int t=0; t<targetDb.length; t++) {
                toId[t] = (targetDb[t].charAt(0) == '_');
                if (toId[t]) { targetDb[t] = targetDb[t].substring(1); }
                app.checkAvailable(targetDb[t]);
            }
            String sep = argSeparator.getValue();
            sep = ArgumentEncoding.decode(sep);
            Integer[] cols = new Integer[targetDb.length];
            if (argColumns.isUsed()) {
               StringTokenizer tok = new StringTokenizer(argColumns.getValue(), "_");
               for (int f=0; f<cols.length; f++) {
                   if (tok.hasMoreTokens()) {
                      String token = tok.nextToken();
                      if ("A".equalsIgnoreCase(token))
                         continue;
                      try { cols[f] = Integer.parseInt(token); }
                      catch (NumberFormatException e) {
                         System.err.println("Invalid number of columns for "+targetDb[f]+": "+token);
                         return;
                      }
                   } else {
                      break;
                   }
               }
               if (tok.hasMoreTokens()) {
                  System.err.println("Too many fields declared in column pattern: "+argColumns.getValue());
                  return;
               }
            }
            PrintStream out = null;
            if (argOutputFile.isUsed()) {
               try { out = new PrintStream(new FileOutputStream(argOutputFile.getValue())); }
               catch (IOException e) {
                  System.err.println("Cannot open "+argOutputFile.getValue()+" for output: "+e.toString());
                  return;
               }
            }
            IDSetProcessor processor;
            switch (OutputMode.valueOf(argLayout.getValue())) {
                case Line:
                  processor = new IDLineWriter(argHideIndicator.isUsed(),
                                               argHideSource.isUsed(),
                                               sep,
                                               argNA.getValue(),
                                               cols);
                  if (out != null) ((IDLineWriter)processor).setStream(out);
                  break;
                case Table:
                  processor = new IDTableWriter(argHideIndicator.isUsed(),
                                                argHideSource.isUsed(),
                                                sep,
                                                argNA.getValue());
                  if (out != null) ((IDTableWriter)processor).setStream(out);
                  break;
                default:
                  throw new IllegalStateException("Partial implementation of an output mode");
            }
            if (argGOFilter.isUsed()) app.setGOEvidenceFilter(argGOFilter.getValue());
            if (argRaw.isUsed())      app.setKeyCleaning(false);
            if (argPrimary.isUsed())  app.setPrimaryOnly(true);
            if (argConvertFile.isUsed()) {
               app.processFile(argConvertFile.getValue(),
                               argType.getValue(),
                               sourceDb, isId, targetDb, toId,
                               processor);
            } else
            if (argConvertKey.isUsed()) {
               app.processKey(argConvertKey.getValue(),
                              argType.getValue(),
                              sourceDb, isId, targetDb, toId,
                              processor);
            } else {
               app.processReader(new InputStreamReader(System.in),
                                 argType.getValue(),
                                 sourceDb, isId, targetDb, toId,
                                 processor);
            }
            if (out != null) { out.close(); }
        }
        app.close();
    }

    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("Korvasieni v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("---------------------------------");
        System.err.println("korvasieni [options] [sourceDb targetDb...]");
        MainArgument.printArguments(args, System.err);
        System.err.println("You may prefix database names with '_' for accession numbers.");
        System.err.println("Keys are read from the standard input stream by default.");
    }

    /**
     * Sorting routine for target keys. This class can be set to use either identifiers or
     * labels of the keys.
     */
    private class IDComparator implements Comparator<DatabaseId> {

        private final boolean useId;

        IDComparator(boolean toId) {
            useId = toId;
        }

        public int compare(DatabaseId id1, DatabaseId id2) {
            if (useId) {
                return id1.getId().compareTo(id2.getId());
            }
            return id1.getLabel().compareTo(id2.getLabel());
        }

    }

}
