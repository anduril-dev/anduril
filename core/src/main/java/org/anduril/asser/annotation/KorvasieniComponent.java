package org.anduril.asser.annotation;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.annotation.IDConverter.EnsemblType;
import org.anduril.asser.annotation.IDConverter.SkipLevel;
import org.anduril.asser.io.CSVCleaner;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * This is a pipeline wrapper for {@link org.anduril.asser.annotation.IDConverter}.
 * 
 * @author Marko Laakso
 */
public class KorvasieniComponent extends SkeletonComponent {

    static public final String PARAMETER_INPUT_DB     = "inputDB";
    static public final String PARAMETER_INPUT_TYPE   = "inputType";
    static public final String PARAMETER_TARGET_DB    = "targetDB";
    static public final String PARAMETER_KEY_COLUMN   = "keyColumn";
    static public final String PARAMETER_LIST_KEY     = "isListKey";
    static public final String PARAMETER_UNIQUE       = "unique";
    static public final String PARAMETER_ECHO_COLS    = "echoColumns";
    static public final String PARAMETER_RENAME       = "rename";
    static public final String PARAMETER_MAX_HITS     = "maxHits";
    static public final String PARAMETER_SKIP_LEVEL   = "skipLevel";
    static public final String PARAMETER_INDICATOR    = "indicator";
    static public final String PARAMETER_GO_FILTER    = "goFilter";
    static public final String PARAMETER_PRIMARY_ONLY = "primary";

	static public final String INPUT_CONNECTION = "connection";
	static public final String INPUT_KEYS       = "sourceKeys";

	static public final String OUTPUT_ANNOTATION = "bioAnnotation";

	protected ErrorCode runImpl(CommandFile cf) throws IOException,
	                                                   SQLException {
		String      inputDB  = cf.getParameter(PARAMETER_INPUT_DB).trim();
        String      echoStr  = cf.getParameter(PARAMETER_ECHO_COLS).trim();
		String[]    targetDB = AsserUtil.split(cf.getParameter(PARAMETER_TARGET_DB));
		String      colName  = cf.getParameter(PARAMETER_KEY_COLUMN).trim();
        String      rename   = cf.getParameter(PARAMETER_RENAME);
        int         maxHits  = cf.getIntParameter(PARAMETER_MAX_HITS);
        boolean     useIndi  = cf.getBooleanParameter(PARAMETER_INDICATOR);
		boolean     uniq     = cf.getBooleanParameter(PARAMETER_UNIQUE);
		boolean     isList   = cf.getBooleanParameter(PARAMETER_LIST_KEY);
		File        conCfg   = cf.getInput(INPUT_CONNECTION);
		EnsemblType inType   = EnsemblType.valueOf(cf.getParameter(PARAMETER_INPUT_TYPE));
        CSVParser   keys     = new CSVParser(cf.getInput(INPUT_KEYS));
        String[]    inCols   = keys.getColumnNames();
		Set<String> used     = null;

		IDConverter korvasieni;
		if (conCfg == null)
		    korvasieni = new IDConverter();
		else
			korvasieni = new IDConverter(conCfg);
		cf.writeLog("Target database is "+korvasieni.getURL());

		korvasieni.setPrimaryOnly(cf.getBooleanParameter(PARAMETER_PRIMARY_ONLY));

        SkipLevel skipLevel = SkipLevel.valueOf(cf.getParameter(PARAMETER_SKIP_LEVEL)); 
        korvasieni.setSkipLevel(skipLevel);

        korvasieni.setGOEvidenceFilter(cf.getParameter(PARAMETER_GO_FILTER));

		int     col;
        int     colStart       = -1;
        int     colEnd         = -1;
        boolean multicolRegion = (IDConverter.DNA_REGION.equals(inputDB) && (colName.indexOf(':')>0));
        if (multicolRegion) {
            int splitI1 = colName.indexOf(':');
            int splitI2 = colName.indexOf('-', splitI1);
            col = AsserUtil.indexOf(colName.substring(0,splitI1), inCols, false, true);
            if (splitI2 >= 0) {
               colStart = AsserUtil.indexOf(colName.substring(splitI1+1, splitI2), inCols, false, true);
               colEnd   = AsserUtil.indexOf(colName.substring(splitI2+1),          inCols, false, true);
            } else {
               colStart = AsserUtil.indexOf(colName.substring(splitI1+1), inCols, false, true);
            }
        } else {
            col = colName.isEmpty() ? 0 : AsserUtil.indexOf(colName, inCols, false, true);
        }

		if (inputDB.length() < 1) {
			inputDB = inCols[col];
		}
		cf.writeLog("Converting: "+inputDB+" ("+inType+") values to "+Arrays.toString(targetDB));

        boolean isId   = (inputDB.charAt(0) == '_');
        boolean toId[] = new boolean[targetDB.length];
        if (isId) { inputDB = inputDB.substring(1); }
        korvasieni.checkAvailable(inputDB);
        for (int t=0; t<targetDB.length; t++) {
        	targetDB[t] = targetDB[t].trim();
            toId[t]     = (targetDB[t].charAt(0) == '_');
            if (toId[t]) { targetDB[t] = targetDB[t].substring(1); }
            korvasieni.checkAvailable(targetDB[t]);
        }

        if (uniq) {
        	used = new TreeSet<String>();
        }
        String[] echos;
        if (echoStr.isEmpty()) {
        	echos = AsserUtil.EMPTY_ARRAY_STRING;
        } else {
            if ("*".equals(echoStr)) {
               echos = new String[inCols.length-(multicolRegion ? (colEnd<0 ? 2:3) : 1)];
               int lag = 0;
               for (int i=0; i<inCols.length; i++) {
                   if ((i==col) || (multicolRegion && ((i==colStart)||(i==colEnd)))) {
                       lag++;
                       continue;
                   }
                   echos[i-lag] = inCols[i];
               }
            } else {
               echos = AsserUtil.split(echoStr);
            }
        }
        int[] echoI = new int[echos.length];
        for (int i=0; i<echos.length; i++) {
            echos[i] = echos[i].trim();
            echoI[i] = AsserUtil.indexOf(echos[i], inCols, true, true);
        }

        int colShift = (useIndi ? 2 : 1);
        String[] cols = new String[targetDB.length+echos.length+colShift];
        cols[0] = multicolRegion ? colName : inCols[col];
        if (useIndi) cols[1] = "indicator";
        System.arraycopy(targetDB, 0, cols, colShift, targetDB.length);
        System.arraycopy(echos,    0, cols, targetDB.length+colShift, echos.length);
        for (int i=2; i<cols.length; i++) {
            if (cols[0].equals(cols[i])) {
               cols[0] = "sourceKey";
               break;
            }
        }
        // Automatic renaming of the columns with conflicting names
        for (int t=targetDB.length-1; t>=0; t--) {
    		int count = 0;
    		for (int i=0; i<t+colShift; i++) {
    			if (cols[i].equals(cols[t+colShift]))
    			   count++;
    		}
    		if (count > 0) cols[t+colShift] = cols[t+colShift]+(count+1);
        }
        cols = CSVCleaner.renameColumns(cols, rename);

        CSVWriter out      = new CSVWriter(cols, cf.getOutput(OUTPUT_ANNOTATION));
        String[]  keyArray = isList ? null : new String[1];
		while (keys.hasNext()) {
            String[]              row  = keys.next();
			List<Set<DatabaseId>> values;

			if (row[col] == null)    // No key
				continue;
            row[col] = row[col].trim();
			if (row[col].isEmpty())  // Empty string as a key
				continue;
            if (isList)
                keyArray = AsserUtil.split(row[col]);
            else
                keyArray[0] = row[col];
            if (multicolRegion)
                readRegionBoundaries(keyArray, row, colStart, colEnd);
            for (String key : keyArray) {
    			if (uniq && !used.add(key)) // No not generate duplicates if the unique flag is set
    				continue;
    			values = korvasieni.convert(key, isId, inType, inputDB, targetDB, toId);
                if ((values == null) && (skipLevel != SkipLevel.never))
                    continue; // We can skip these results.
    			out.write(key, false);
    			if (values == null) {
    				if (useIndi) out.write(0);
    				out.skip(targetDB.length);
    			} else {
    				if (useIndi) out.write(1);
    				StringBuffer colCont = new StringBuffer();
    				for (int db=0; db<targetDB.length; db++) {
    				    Set<DatabaseId> results = values.get(db);
                        int             idC     = 0;
    				    for (DatabaseId id : results) {
                            if (++idC > maxHits) break;
    				    	if (idC > 1)         colCont.append(',');
    				    	colCont.append(toId[db] ? id.getId() : id.getLabel());
    				    }
    				    if (colCont.length() < 1) {
    				    	out.skip();
    				    } else {
    					    out.write(colCont.toString());
    					    colCont.setLength(0);
    				    }
    				}
    			}
                for (int i : echoI) {
                    out.write(row[i]);
                }
            }
		}
		out.close();

		return ErrorCode.OK;
	}

    static private void readRegionBoundaries(String[] keys,
                                             String[] row,
                                             int      start,
                                             int      end) {
        if (keys.length > 0) {
            String[] starts = AsserUtil.split(row[start]);
            if (end < 0) {
               for (int i=0; i<keys.length; i++) {
                   keys[i] += ':'+starts[i];
               }            	
            } else {
               String[] ends = AsserUtil.split(row[end]);
               for (int i=0; i<keys.length; i++) {
                   keys[i] += ':'+starts[i]+'-'+ends[i];
               }
            }
        } else {
            if (end < 0)
               keys[0] += ':'+row[start];
            else
               keys[0] += ':'+row[start]+'-'+row[end];
        }
    }

	/**
	 * @param argv Pipeline arguments
	 */
	public static void main(String[] argv) {
		new KorvasieniComponent().run(argv);
	}

}
