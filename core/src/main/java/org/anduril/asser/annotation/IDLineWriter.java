package org.anduril.asser.annotation;

import java.io.PrintStream;
import java.util.List;
import java.util.Set;

/**
 * This output mode produces one line of output for each source key.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.21
 */
class IDLineWriter implements IDSetProcessor {

    private final boolean   hideI;
    private final boolean   hideS;
    private final String    sep;
    private final String    na;
    private final Integer[] cols;

    private PrintStream out = System.out;
    
    IDLineWriter(boolean   hideIndicator,
                 boolean   hideSource,
                 String    separator,
                 String    missingValueSymbol,
                 Integer[] columnLengths) {
        hideI = hideIndicator;
        hideS = hideSource;
        sep   = separator;
        na    = missingValueSymbol;
        cols  = columnLengths;
    }

    PrintStream setStream(PrintStream stream) {
    	PrintStream old = out;
    	out = stream;
    	return old;
    }

    public void processIDs(String                sourceKey,
                           String[]              targetDbs,
                           boolean[]             toId,
                           List<Set<DatabaseId>> keys) {
        if (!hideS) out.print(sourceKey);
        if (!hideI) {
            if (!hideS) out.print(sep);
            out.print((keys == null) ? '-' : '+');
        }
        
        boolean isFirst = hideS && hideI;
        for (int t=0; t<targetDbs.length; t++) {
            int c = 0;
            if (keys != null) for (DatabaseId id : keys.get(t)) {
                if ((cols[t] != null) && (c >= cols[t].intValue()))
                    break;
                if (isFirst)
                    isFirst = false;
                else
                    out.print(sep);
                out.print(toId[t] ? id.getId() : id.getLabel());
                c++;
            }
            if (cols[t] != null) for (; c<cols[t].intValue(); c++) {
                if (isFirst)
                    isFirst = false;
                else
                    out.print(sep);
                out.print(na);
            }
        }
        out.println();
    }

}
