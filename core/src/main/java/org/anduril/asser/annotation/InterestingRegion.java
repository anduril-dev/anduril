package org.anduril.asser.annotation;

/**
 * Represents a chromosome region with a start and end position.
 * 
 * @author Marko Laakso
 */
public class InterestingRegion extends GOTarget {

    private String chromosome;
    private long   start;
    private long   end;

    /** Undefined region */
    public InterestingRegion() {
        // Shall be initialized using the accessories
    }

    public InterestingRegion(String location) {
    	int cPos = location.indexOf(':');
    	if (cPos < 0)
    		throw new IllegalArgumentException("No chromosome delimiter: "+location);
    	chromosome = location.substring(0,cPos);
    	int hPos = location.indexOf('-', cPos);
    	if (hPos < 0) {
    		// No base pair delimiter
            start = Long.parseLong(location.substring(cPos+1));
            end   = start;
        } else {
            start = Long.parseLong(location.substring(cPos+1, hPos));
            end   = Long.parseLong(location.substring(hPos+1));
        }
    }

    public String getChromosome() {
        return chromosome;
    }

    public void setChromosome(String chromosome) {
        this.chromosome = chromosome;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return chromosome+": "+start+'-'+end;
    }

}
