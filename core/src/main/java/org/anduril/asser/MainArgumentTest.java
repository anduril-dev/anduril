package org.anduril.asser;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.MainArgument}.
 * 
 * @author Marko Laakso
 */
public class MainArgumentTest {

	@Test
	public void testDefaults() {
		MainArgument arg = new MainArgument(new String[] {"i"}, "An optional parameter", new String[] {"123"});
		assertEquals(123, arg.getInt());
	}

	@Test
	public void testParsing() {
		final String S = "size";
		final String K = "k";
		Map<String, MainArgument> args = new HashMap<String, MainArgument>();
		MainArgument.add(S, S, "Length of the item", args);
		MainArgument.add("v", null, "Use verbose mode", (String)null, args);
		MainArgument.add(K, "key", "The key thing", "5", args);
		assertFalse(args.get(S).isUsed());
		assertEquals(5, args.get(K).getInt());
		
		String[] argv = new String[] {"-size", "3", "something", "-v", "-k", "34"};
	    String[] left = MainArgument.parseInput(argv, args, 1, 1);
	    assertTrue(Arrays.deepEquals(new String[]{"something"}, left));
		assertEquals("3", args.get(S).getValue());
		assertEquals(3,   args.get(S).getInt());
		assertTrue(args.get(S).isUsed());
		assertEquals(34, args.get(K).getInt());
	}

}
