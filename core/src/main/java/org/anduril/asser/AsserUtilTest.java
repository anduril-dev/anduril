package org.anduril.asser;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.AsserUtil}.
 * 
 * @author Marko Laakso
 */
public class AsserUtilTest {

    @Test
    public void testMin() {
        final double DELTA = 0.0000001;
        assertEquals(Double.NaN, AsserUtil.min(),              DELTA);
        assertEquals(0.1,        AsserUtil.min(0.1),           DELTA);
        assertEquals(0.1,        AsserUtil.min(0.3, 0.1, 0.2), DELTA);
    }

    @Test
    public void testMax() {
        final double DELTA = 0.0000001;
        assertEquals(Double.NaN, AsserUtil.max(),               DELTA);
        assertEquals(0.1,        AsserUtil.max(0.1),            DELTA);
        assertEquals(0.1,        AsserUtil.max(0.0, 0.1, -0.2), DELTA);
    }

	@Test
	public void testRound() {
		assertEquals("1.",             AsserUtil.round(0.6, 0));
		assertEquals("0.1",            AsserUtil.round(0.06, 1));
		assertEquals("0.0",            AsserUtil.round(0.04, 1));
		assertEquals("123456789.0123", AsserUtil.round(123456789.0123456789, 4));
		assertEquals("NA",             AsserUtil.round(Double.NaN, 4));
		assertEquals("NA",             AsserUtil.round(Double.NEGATIVE_INFINITY, 4));
		assertEquals("NA",             AsserUtil.round(Double.POSITIVE_INFINITY, 4));
	}

	@Test
	public void testReplaceAll() {
		StringBuffer b = new StringBuffer("FooFooBarFoo");
		AsserUtil.replaceAll(b, "Bar", "FAR");
		assertEquals("FooFooFARFoo", b.toString());
		AsserUtil.replaceAll(b, "Foo", "ZooM");
		assertEquals("ZooMZooMFARZooM", b.toString());
		AsserUtil.replaceAll(b, "oo", "");
		assertEquals("ZMZMFARZM", b.toString());
	}

    @Test
    public void testExpandJointValues() {
        final String SEP = ",";

        String[] a1 = new String[] { "A", "B" };
        String[] a2 = new String[] { };
        String[] a3 = new String[] { "A", "B,C", null, "D" };
        
        assertSame(a1, AsserUtil.expandJointValues(a1, SEP));
        assertSame(a2, AsserUtil.expandJointValues(a2, SEP));
        assertSame(a3, AsserUtil.expandJointValues(a3, "."));
        assertArrayEquals(new String[] { "A", "B", "C", null, "D" },
                          AsserUtil.expandJointValues(a3, SEP));
    }

    @Test
    public void testSplit() {
        assertArrayEquals(new String[] { "A", "B", "C", "", "D" },
                          AsserUtil.split(" A, B ,C,,D ,"));
        assertArrayEquals(new String[] { "A", "B", "C", "", "D" },
                          AsserUtil.split(" A; B ;C;;D ;", ";"));
    }

    @Test
    public void testStrictSplit() {
        assertArrayEquals(new String[] { "A", "Bb", "C", "", "D", "", "", "" },
                          AsserUtil.strictSplit(" A, Bb ,C,,D , ,,", ","));
        assertArrayEquals(new String[] { "", "A", "Bb", "C" },
                AsserUtil.strictSplit("--A--Bb--C", "--"));
    }

}
