package org.anduril.asser.db;

import java.util.List;

/**
 * Object conversion functions to access the database objects from Java.
 * 
 * @author Marko Laakso
 */
public final class TypeConverter {

	/** No instantiation for the library class **/
	private TypeConverter() {}

	/**
	 * Converts all sorts of objects to primitive integers.
	 */
	static public int toInt(Object obj) {
		if (obj instanceof Number)
			return ((Number)obj).intValue();
		else
			return Integer.parseInt(obj.toString());
	}

	static public int[] toIntArray(Object[] obj) {
		int[] array = new int[obj.length];
		for (int i=0; i<array.length; i++) {
			array[i] = toInt(obj[i]);
		}
		return array;
	}

    static public Integer toInteger(Object obj) {
        if (obj instanceof Integer)
            return (Integer)obj;
        return toInt(obj);
    }

    static public Integer[] toIntegerArray(Object[] obj) {
        Integer[] array = new Integer[obj.length];
        for (int i=0; i<array.length; i++) {
            array[i] = toInteger(obj[i]);
        }
        return array;
    }

	/**
	 * Converts all sorts of objects to primitive long integers.
	 */
	static public long toLong(Object obj) {
		if (obj instanceof Number)
			return ((Number)obj).longValue();
		else
			return Long.parseLong(obj.toString());
	}

	/**
	 * Converts all sorts of objects to primitive Booleans.
	 */
	static public boolean toBoolean(Object obj) {
		if (obj instanceof Boolean)
			return ((Boolean)obj).booleanValue();
		else
			return Boolean.parseBoolean(obj.toString());
	}

	/**
	 * Converts all sorts of objects to primitive floating points.
	 */
    static public float toFloat(Object obj) {
        if (obj instanceof Number)
            return ((Number)obj).floatValue();
        else
            return Float.parseFloat(obj.toString());
    }

    static public float[] toFloatArray(Object[] obj) {
        float[] array = new float[obj.length];
        for (int i=0; i<array.length; i++) {
            array[i] = toFloat(obj[i]);
        }
        return array;
    }

	/**
	 * Converts all sorts of objects to primitive floating points
	 * with the double precision.
	 */
    static public double toDouble(Object obj) {
        if (obj instanceof Number)
            return ((Number)obj).doubleValue();
        else
            return Double.parseDouble(obj.toString());
    }

    static public double[] toDoubleArray(Object[] obj) {
        double[] array = new double[obj.length];
        for (int i=0; i<array.length; i++) {
            array[i] = toDouble(obj[i]);
        }
        return array;
    }

	/**
	 * Extracts the given column from the matrix that is represented as a
	 * list of arrays of rows.
	 */
	static public Object[] columnOf(List<Object[]> matrix, int col) {
		Object[] column = new Object[matrix.size()];
		int      i      = 0;
		for (Object[] row : matrix) {
			column[i++] = row[col];
		}
		return column;
	}

}