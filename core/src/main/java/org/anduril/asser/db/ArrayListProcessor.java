package org.anduril.asser.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Converts result sets to a list of object arrays representing the rows.
 */
public class ArrayListProcessor implements ResultSetProcessor {

    private List<Object[]> retValue;

    @Override
    public void readResults(ResultSet rs) throws SQLException {
        ResultSetMetaData desc = rs.getMetaData();
        int               cols = desc.getColumnCount();
        Object[]          tuple;

        if (retValue == null) retValue = new LinkedList<Object[]>();
        while (rs.next()) {
            tuple = new Object[cols];
            for (int i=0; i<tuple.length; i++) {
                tuple[i] = rs.getObject(i+1);
            }
            retValue.add(tuple);
        }
    }
    
    public List<Object[]> getArray() {
    	List<Object[]> array = retValue;
    	retValue = null;
    	return array;
    }

}