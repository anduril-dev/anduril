package org.anduril.asser.db;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.io.CSVParser;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;
import org.anduril.component.Tools;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This Anduril component executes the given SQL select statements
 * and prints its result to the output CSV file.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 2.20
 */
public class SQLSelectComponent extends SkeletonComponent {

    @Override
    protected ErrorCode runImpl(CommandFile cf) throws IOException,
                                                       SQLException {
        File    sqlFile = cf.getInput("query");
        File    conFile = cf.getInput("connection");
        File    argFile = cf.getInput("queryParams");

        String sqlIn;
        if (sqlFile == null) {
            sqlIn = cf.getParameter("defaultQuery");
        } else {
            sqlIn = Tools.readFile(sqlFile);
        }
        String sql = sqlIn;

        JDBCConnection con;
        if (conFile == null) {
            con = new JDBCConnection();
        } else {
            con = new JDBCConnection(conFile);
        }
        cf.writeLog("Target database is "+con.getURL());

        ResultSet2CSV processor = new ResultSet2CSV(cf.getOutput("table"));
        if (argFile == null) { // No query parameters
            con.doSelect(sql, processor);
        } else {               // Some query parameters
            CSVParser args = new CSVParser(argFile);
            if (args.hasNext()) { // Has some queries
                String       colP      = cf.getParameter("columns").trim();
                boolean[]    lists     = null;
                StringBuffer sqlBuffer = null;
                int[]        cols      = null;
                String[]     input     = null;
                if (!colP.isEmpty()) {
                   cols  = AsserUtil.indicesOf(AsserUtil.split(colP), args.getColumnNames(), true, true);
                   input = new String[cols.length];
                }
                int[] listColPos = listPositions(sql, cf.getParameter("listCols"), args.getColumnNames(), cols);
                if (listColPos != null) {
                	lists     = new boolean[listColPos.length];
                	sqlBuffer = new StringBuffer(sql.length()*2);
                }
                while (args.hasNext()) {
                   if (cols == null) {
                      input = args.next();
                   } else {
                      String[] line = args.next();
                      for (int i=0; i<cols.length; i++) {
                          input[i] = line[cols[i]];
                      }
                   }
                   if (listColPos != null) {
                	   boolean hasLists = false;
                	   for (int i=0; i<listColPos.length; i++) {
                		   if ((listColPos[i] >= 0) && (input[i] != null) && (input[i].indexOf(',') >= 0)) {
                			  // cf.writeLog(input[i]+" in pieces for the position "+listColPos[i]+'.');
                			  hasLists = true;
                			  lists[i] = true;
                		   } else {
                			  lists[i] = false;
                		   }
                	   }
                	   if (hasLists) {
                		   input = expandQuery(input, lists, listColPos, sqlIn, sqlBuffer);
                		   sql   = sqlBuffer.toString();
                		   // cf.writeLog(sql);
                	   } else {
                		   sql = sqlIn;
                	   }
                   }
                   con.doSelect(sql, processor, (Object[])input);
                }
            } else {              // Query set is empty
                processor.prepareOutput(con.getMetaData(sql));
            }
            args.close();
        }
        processor.close();

        con.close();
        return ErrorCode.OK;
    }

    static private String[] expandQuery(String[]     original,
    		                            boolean[]    isList,
    		                            int[]        positions,
    		                            String       sqlTemplate,
    		                            StringBuffer sqlBuffer) {
    	sqlBuffer.setLength(0);
    	int          lastPos = 0;
    	List<String> buffer  = new ArrayList<String>(100);
    	for (int i=0; i<isList.length; i++) {
    		if (isList[i]) {
    			String[] values = AsserUtil.split(original[i]);
    			for (String value : values) {
    				buffer.add(value);
    			}
    			sqlBuffer.append(sqlTemplate.substring(lastPos, positions[i]+1));
    			for (int c=1; c<values.length; c++) {
    				sqlBuffer.append(",?");
    			}
    		    lastPos = positions[i]+1;
    		} else {
    			buffer.add(original[i]);
    		}
    	}
    	sqlBuffer.append(sqlTemplate.substring(lastPos, sqlTemplate.length()));
    	return buffer.toArray(new String[buffer.size()]);
    }

    static private int[] listPositions(String   sql,
    		                           String   listColP,
    		                           String[] colNames,
    		                           int[]    colOrder) {
    	if (listColP.isEmpty()) return null;

    	String[] cols;
    	if (colOrder == null) {
    		cols = colNames;
    	} else {
    		cols = new String[colOrder.length];
    		for (int i=0; i<cols.length; i++) {
    			cols[i] = colNames[colOrder[i]];
    		}
    	}

    	String[] listColN  = AsserUtil.split(listColP);
    	int[]    listOrder = AsserUtil.indicesOf(listColN, cols, true, true);
    	for (int i=0; i<listColN.length; i++) {
    		listColN[i] = cols[listOrder[i]];
    	}
    	listOrder = AsserUtil.indicesOf(cols, listColN, true, false);
    	int posCount = 0;
    	int wait     = -1;
    	for (int p=0; p<sql.length(); p++) {
    		char c = sql.charAt(p);
    		if (wait == -1) {
    		    if (c == '"' || c == '\'') {
    		       // Start ignoring...
    		       wait = c;
    		    } else
	    		if (c == '?') {
	    			if (listOrder[posCount] >=0) {
	    				listOrder[posCount] = p;
	    			}
	    			posCount++;
	    		}
    		} else {
    		    if (c == wait) {
    		       // Stop ignoring...
    		       wait = -1;
    		    }
    		}
    	}
    	if (posCount != cols.length)
           throw new IllegalArgumentException("Total of "+posCount+" assignments found for "+cols.length+" parameters.");
    	return listOrder;
    }

    /**
     * @param argv Anduril arguments
     */
    public static void main(String[] argv) {
        new SQLSelectComponent().run(argv);
    }

}