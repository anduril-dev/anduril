package org.anduril.asser.db;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Common interface for the objects reading database result sets.
 */
public interface ResultSetProcessor {

    /**
     * This method is called with a prepared result set that may be
     * read. The set may be left open.
     */
    public void readResults(ResultSet rs) throws SQLException;

}
