package org.anduril.asser.db;

import java.io.*;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * This class provides a generic SQL query framework that can be used to
 * fetch objects from a JDBC database.
 *  
 * @author Marko Laakso
 */
public class JDBCConnection {

    private String     user;
    private String     password;
	private String     url;
	private int        timeout;
	private boolean    recycle;
	private Connection connection;

	/** Default name for the connection properties **/
    static public final String FILE_DB_CFG            = "database.properties";

	static public final String CFG_DATABASE_URL       = "database.url";
    static public final String CFG_DATABASE_USER      = "database.user";
    static public final String CFG_DATABASE_PASSWORD  = "database.password";
	static public final String CFG_DATABASE_TIMEOUT   = "database.timeout";
	static public final String CFG_DATABASE_RECYCLING = "database.recycle";
	static public final String CFG_DATABASE_DRIVER    = "database.driver";

	public JDBCConnection(String  jdbcUrl,
			              int     timeoutLimit,
			              boolean connectionRecycling,
			              String  driver) throws SQLException {
		try { Class.forName(driver).newInstance(); }
		catch (Exception e) {
			throw new SQLException("Unable to connect to the database: "+jdbcUrl, e);
		}
		url     = jdbcUrl;
		timeout = timeoutLimit;
		recycle = connectionRecycling;
	}

	/**
	 * Creates a new connection that is based on the configurations
	 * given in {@link #FILE_DB_CFG}.
	 */
	public JDBCConnection() throws IOException, SQLException {
		this(FILE_DB_CFG);
	}

	/**
	 * Creates a new connection that is based on the configuration properties
	 * of the given file.
	 */
	public JDBCConnection(String cfgFile) throws IOException, SQLException {
    	this(ClassLoader.getSystemClassLoader().getResourceAsStream(cfgFile), cfgFile);
	}

	/**
	 * Creates a new connection that is based on the configuration properties
	 * of the given file.
	 */
	public JDBCConnection(File cfgFile) throws IOException, SQLException {
    	this(new FileInputStream(cfgFile), cfgFile.getAbsolutePath());
	}

	/**
	 * Creates a new connection that is based on the configuration properties
	 * of the given file stream.
	 *
	 * @param resource A human readable name for the input source (used in error messages)
	 */
	public JDBCConnection(InputStream in, String resource) throws IOException, SQLException {
    	if (in == null)
    		throw new FileNotFoundException(resource);

    	ResourceBundle cfg = new PropertyResourceBundle(in);
    	in.close();

        if (cfg.containsKey(CFG_DATABASE_USER))     user     = cfg.getString(CFG_DATABASE_USER);
        if (cfg.containsKey(CFG_DATABASE_PASSWORD)) password = cfg.getString(CFG_DATABASE_PASSWORD);
    	url     = cfg.getString(CFG_DATABASE_URL);
    	recycle = Boolean.parseBoolean(cfg.getString(CFG_DATABASE_RECYCLING));
    	try { timeout = Integer.parseInt(cfg.getString(CFG_DATABASE_TIMEOUT)); }
    	catch (NumberFormatException e) {
            throw new NumberFormatException("Invalid "+CFG_DATABASE_TIMEOUT+" value in "+ // NOPMD
    				                        resource+": "+cfg.getString(CFG_DATABASE_TIMEOUT));
    	}

		try { Class.forName(cfg.getString(CFG_DATABASE_DRIVER)).newInstance(); }
		catch (Exception e) {
			throw new SQLException("Unable to connect to the database: "+url, e);
		}
	}

	/**
	 * Returns the database connection URL in use.
	 */
	public String getURL() {
		return url;
	}

	/**
	 * Retrieves a single value from the database.
	 */
	public Object fetchValue(String    sql,
			                 boolean   mandatory,
			                 Object... params) throws SQLException {
		Object[] row = fetchObject(sql, mandatory, params);
		
		if (!mandatory && (row == null))
			return null;
		if (row.length != 1)
			throw new IllegalStateException("Invalid number of result attributes: "+row.length);
		if (row[0] == null)
			throw new IllegalStateException("This mandatory value is not defined.");
		return row[0];
	}

	/**
	 * Retrieves a single tuple from the database. 
	 */
	public Object[] fetchObject(String    sql,
			                    boolean   mandatory,
			                    Object... params) throws SQLException {
		List<Object[]> list = fetchObjects(sql, params);

		if (!mandatory && list.isEmpty())
			return null;
		if (list.size() != 1)
			throw new IllegalStateException("Invalid number of results ("+list.size()+") for parameters: "+Arrays.toString(params));
		return list.get(0);
	}

	/**
	 * Executes a generic SQL query and returns the corresponding tuples as a list. 
	 * 
	 * @param  sql          SQL query
	 * @param  params       Query parameters for the given SQL expression
	 * @return              List of return tuples
	 * @throws SQLException If the execution of the database query fails.
	 */
	public List<Object[]> fetchObjects(String    sql,
			                           Object... params) throws SQLException {
        ArrayListProcessor processor = new ArrayListProcessor();

        doSelect(sql, processor, params);

		return processor.getArray();
	}

    /**
     * Executes a generic SQL query and returns the corresponding tuples as a list. 
     *
     * @param  sql          SQL query
     * @param  params       Query parameters for the given SQL expression
     * @param  processor    Result set listener that will read the results
     * @throws SQLException If the execution of the database query fails.
     */
    public void doSelect(String             sql,
                         ResultSetProcessor processor,
                         Object...          params) throws SQLException {
        Connection        con = null;
        PreparedStatement ps  = null;
        ResultSet         rs  = null;

        synchronized (this) {
            try {
                con = openConnection();
                ps  = con.prepareStatement(sql);
                for (int p=0; p<params.length; p++) {
                    ps.setObject(p+1, params[p]);
                }
                rs = ps.executeQuery();

                // Call processor...
                processor.readResults(rs);
            }
            catch (SQLException e) {
                close();
                throw e;
            }
            finally {
                if (rs  != null)             rs.close();
                if (ps  != null)             ps.close();
                if (con != null && !recycle) con.close();
            }
        }
    }

    /**
     * Compile the given SQL. 
     *
     * @param  sql          SQL query
     * @throws SQLException If the execution of the database query fails.
     */
    public ResultSetMetaData getMetaData(String sql) throws SQLException {
        Connection        con = null;
        PreparedStatement ps  = null;

        synchronized (this) {
            try {
                con = openConnection();
                ps  = con.prepareStatement(sql);
                return ps.getMetaData();
            }
            catch (SQLException e) {
                close();
                throw e;
            }
            finally {
                if (ps  != null)             ps.close();
                if (con != null && !recycle) con.close();
            }
        }
    }

	/**
	 * Releases the underlying database connection. This method should be
	 * called after the last query. 
	 */
	public void close() {
	  	synchronized (this) {
			try {
				if (connection != null)
					connection.close();
				connection = null;
			}
			catch (Exception e) {
				e.printStackTrace();
			}
	  	}
	}

	private Connection openConnection() throws SQLException {
		DriverManager.setLoginTimeout(timeout);

		@SuppressWarnings("resource")
		Connection con = recycle ? connection : null; // NOPMD
		if (con == null) try {
			con = DriverManager.getConnection(url, user, password);
			if (recycle) { connection = con; }
		} catch (SQLException e) {
			throw new SQLException("Cannot create database connection to "+getURL()+'.',
					               e.getSQLState(),
					               e.getErrorCode(),
					               e);
		}
		con.setAutoCommit(true);
		return con;
	}

}