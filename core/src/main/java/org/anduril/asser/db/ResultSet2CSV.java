package org.anduril.asser.db;

import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Writes the given result sets to a CSVWriter (tab delimited) file.
 */
public class ResultSet2CSV implements ResultSetProcessor {

    private final OutputStream outStream;

    private boolean useQuotes = true;

    private CSVWriter out;

    public ResultSet2CSV(File targetfile) throws IOException {
        outStream = new FileOutputStream(targetfile);
    }

    public ResultSet2CSV(OutputStream stream) {
        outStream = stream;
    }

    /**
     * This flag determines whether the output columns are surrounded with quotes.
     */
    public void setUseQuotes(boolean value) {
        useQuotes = value;
    }

    @Override
    public void readResults(ResultSet rs) throws SQLException {
        if (out == null) {
            prepareOutput(rs.getMetaData());
        }
        int cols = out.getColumnCount();

        while (rs.next()) {
            for (int i=0; i<cols; i++) {
                out.write(rs.getString(i+1), useQuotes);
            }
        }
    }

    public void prepareOutput(ResultSetMetaData desc) throws SQLException {
        int      cols   = desc.getColumnCount();
        String[] cNames = new String[cols];

        for (int i=0; i<cols; i++) {
            cNames[i] = desc.getColumnLabel(i+1);
        }
        out = new CSVWriter(cNames, outStream, CSVParser.DEFAULT_DELIMITER, useQuotes);
    }

    /**
     * Close output file after the last statement.
     */
    public void close() {
        if (out != null) {
            out.close();
            out = null;
        }
    }

}