package org.anduril.asser.io;

import org.anduril.asser.ArgumentEncoding;
import org.anduril.asser.AsserUtil;
import org.anduril.asser.MainArgument;
import org.anduril.asser.Version;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * This iterator can be used to scan lines of a comma separated file.
 * The column separator can be customized.
 */
public class CSVParser implements Iterator<String[]>, Iterable<String[]> {

	/** The default delimiter for the columns **/
    static public final String DEFAULT_DELIMITER = "\t";

	/** The default symbol for missing values **/
    static public final String DEFAULT_MISSING_VALUE_SYMBOL = "NA";

    private boolean skipComments;

    /** Missing value symbol */
    private String symbolNA;

    /** Column delimiter symbol */
    private String delimeter;

    /** Internal buffer for the previous line fetched from the input source */
    private String lineBuf;

    /** Current line number for the input */
    private int line;

	/** Input source for the tabular data */
    private BufferedReader in;

    /** Column names */
	private String[] columns;

    /** Quotation flags for the column values */
    private boolean[] quotes;

    /** Accept rows with too few columns and complete them with missing values. */
    private boolean fillRows = false;

	/**
	 * Create a new parser for the given CSV file.
     * This instance will skips no lines from the beginning of the file.
     * Comment lines are skipped automatically.
	 *
	 * @param  filename    Name of the target file.
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(String filename) throws IOException {
		this(new File(filename), DEFAULT_DELIMITER, 0, DEFAULT_MISSING_VALUE_SYMBOL, true);
	}

	/**
	 * Create a new parser for the given CSV file.
     * Comment lines are skipped automatically.
	 *
	 * @param  filename    Name of the target file.
	 * @param  skip        Skips the given number of lines from the beginning of the file.
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(String filename, int skip) throws IOException {
		this(new File(filename), DEFAULT_DELIMITER, skip, DEFAULT_MISSING_VALUE_SYMBOL, true);
	}

	/**
	 * Create a new parser for the given CSV file.
     * This instance will skips no lines from the beginning of the file.
     * Comment lines are skipped automatically.
	 *
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(File file) throws IOException {
		this(file, DEFAULT_DELIMITER, 0, DEFAULT_MISSING_VALUE_SYMBOL, true);
	}

	/**
	 * Create a new parser for the given CSV file.
     * This instance will skips no lines from the beginning of the file.
	 *
	 * @param  file        A reference to the target file.
	 * @param  comments    Skip comment lines
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(File file, boolean comments) throws IOException {
		this(file, DEFAULT_DELIMITER, 0, DEFAULT_MISSING_VALUE_SYMBOL, comments);
	}

	/**
	 * Create a new parser for the given CSV file.
     * Comment lines are skipped automatically.
	 *
	 * @param  file        A reference to the target file.
	 * @param  skip        Skips the given number of lines from the beginning of the file.
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(File file, int skip) throws IOException {
		this(file, DEFAULT_DELIMITER, skip, DEFAULT_MISSING_VALUE_SYMBOL, true);
	}

	/**
	 * Create a new parser for the given CSV file.
	 *
	 * @param  file        A reference to the target file.
	 * @param  delim       A delimited character for the columns.
	 * @param  skip        Skips the given number of lines from the beginning of the file.
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(File file, String delim, int skip) throws IOException {
		this(file, delim, skip, DEFAULT_MISSING_VALUE_SYMBOL, true);
	}

	/**
	 * Create a new parser for the given CSV file.
     * Comment lines are skipped automatically.
	 *
	 * @param  file        A reference to the target file.
	 * @param  delim       A delimited character for the columns.
	 * @param  skip        Skips the given number of lines from the beginning of the file.
	 * @param  na          Missing value symbol (values to be replaced with nulls)
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(File file, String delim, int skip, String na) throws IOException {
		this(file, delim, skip, na, true);
	}

    /**
     * Create a new parser for the given CSV file.
     *
     * @param  file        A reference to the target file.
     * @param  delim       A delimited character for the columns.
     * @param  skip        Skips the given number of lines from the beginning of the file.
     * @param  na          Missing value symbol (values to be replaced with nulls)
     * @param  comments    Skip comment lines
     * @throws IOException If the column headers cannot be read
     */
    public CSVParser(File    file,
                     String  delim,
                     int     skip,
                     String  na,
                     boolean comments) throws IOException {
        this(new FileReader(file), delim, skip, na, comments, true);
    }

    /**
     * Create a new parser for the given CSV file.
     *
     * @param  in          Input source for the tabular data
     * @param  delim       A delimited character for the columns.
     * @param  skip        Skips the given number of lines from the beginning of the file.
     * @param  na          Missing value symbol (values to be replaced with nulls)
     * @param  comments    Skip comment lines
     * @throws IOException If the column headers cannot be read
     */
    public CSVParser(InputStream in,
                     String      delim,
                     int         skip,
                     String      na,
                     boolean     comments) throws IOException {
        this(new InputStreamReader(in), delim, skip, na, comments, true);
    }

	/**
	 * Create a new parser for the given CSV file.
	 *
	 * @param  reader      Input source for the tabular data
	 * @param  delim       A delimited character for the columns.
	 * @param  skip        Skips the given number of lines from the beginning of the file.
	 * @param  na          Missing value symbol (values to be replaced with nulls)
	 * @param  comments    Skip comment lines
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(Reader  reader,
			         String  delim,
			         int     skip,
			         String  na,
			         boolean comments) throws IOException {
		this(reader, delim, skip, na, comments, true);
	}

	/**
	 * Create a new parser for the given CSV file.
	 *
	 * @param  reader      Input source for the tabular data
	 * @param  delim       A delimited character for the columns.
	 * @param  skip        Skips the given number of lines from the beginning of the file.
	 * @param  na          Missing value symbol (values to be replaced with nulls)
	 * @param  comments    Skip comment lines
	 * @param  hasHeader   True if the first line after skip contains column names.
	 * @throws IOException If the column headers cannot be read
	 */
	public CSVParser(Reader  reader,
			         String  delim,
			         int     skip,
			         String  na,
			         boolean comments,
			         boolean hasHeader) throws IOException {
	    if (delim.isEmpty()) throw new IllegalArgumentException("Empty delimiter!");

		symbolNA     = na;
		delimeter    = delim;
		in           = new BufferedReader(reader);
		skipComments = comments;

        for (int i=0; i<skip; i++) {
            if (in.readLine() == null) // Skip one line
		       throw new EOFException("There were only "+(i-1)+" lines to skip");
        }
	    line = skip;
        nextLine();
        String[] resBuffer = new String[1];
	    if (hasHeader) {
			if (lineBuf == null)
			   throw new EOFException("No column headers");
			List<String> cols = new LinkedList<String>();
			for (int lPos=0; lPos<=lineBuf.length(); ) {
	            lPos = readEntry(lineBuf, delim, null, resBuffer, 0, lPos, line);
	            if (resBuffer[0].equals(symbolNA)) {
	                cols.add(null);
	            } else {
			        cols.add(resBuffer[0]);
	            }
			}
			columns = cols.toArray(new String[cols.size()]);
			nextLine();
	    } else {
	    	int colCount = 0;
	    	if (lineBuf != null) for (int lPos=0; lPos<=lineBuf.length(); ) {
	            lPos = readEntry(lineBuf, delim, null, resBuffer, 0, lPos, line);
			    colCount++;
			}
	    	columns = new String[colCount];
	    }
        quotes  = new boolean[columns.length];
	}

	/**
	 * Adjust to the next input line.
	 */
	private void nextLine() throws IOException {
		while ((lineBuf=in.readLine()) != null) {
	        line++;
	        if (!AsserUtil.onlyWhitespace(lineBuf) &&
	        	!(skipComments && (lineBuf.charAt(0)=='#'))) {
	        	break;
	        }
		}
	}

	/**
	 * Defines if the rows with too few columns can be accepted and completed with missing values.
	 */
	public void setFillRows(boolean enabled) {
	    fillRows = enabled;
	}

    /**
     * Tells if the rows with too few columns can be accepted and completed with missing values.
     */
	public boolean isFillRows() {
	    return fillRows;
	}

	/**
	 * True value activates the skipping of comments lines starting with
	 * hash signs.
	 */
	public void setSkipComments(boolean skip) {
		skipComments = skip;
	}

	/**
	 * Tells if the parser ignores lines starting with hash signs.
	 */
	public boolean isSkipComments() { return skipComments; }

	/**
	 * Returns the current column delimiter symbol.
	 */
	public String getColumnDelimiter() {
		return delimeter;
	}

	/**
	 * Returns the current symbol for missing values.
	 */
	public String getNASymbol() {
		return symbolNA;
	}

	/**
	 * Set symbol for missing values.
	 * @param na Missing value symbol or null to disable the missing value conversion.
	 */
	public void setNASymbol(String na) {
		symbolNA = na;
	}

	/**
	 * Current line of the input file.
	 */
    public int getLineNumber() {
      return line;
    }

    /**
     * Number of columns in the file.
     */
    public int getColumnCount() {
      return columns.length;
    }

    /**
     * Column labels.
     */
    public String[] getColumnNames() {
      return columns;
    }

    /**
     * Returns the index of the column with the given name.
     */
    public int getColumnIndex(String name) {
    	return AsserUtil.indexOf(name, columns, true, true);
    }

    /**
     * Returns the index of the column with the given name or -1 if the
     * column is not available.
     *
     * @param mandatory Throw an exception if the key is not found
     */
    public int getColumnIndex(String name, boolean mandatory) {
    	return AsserUtil.indexOf(name, columns, true, mandatory);
    }

    /**
     * Tells if the value of the input column was surrounded by quotation marks.
     * 
     * @param column Column index of interest
     */
    public boolean hasQuotations(int column) {
        return quotes[column];
    }

    /**
     * Iterate over the remaining rows.
     */
    @Override
    public Iterator<String[]> iterator() {
        return this;
    }

    /**
     * Tells if the file has more rows.
     */
    @Override
    public boolean hasNext() {
      return (lineBuf != null);
    }

    /**
     * Returns a vector of all columns of the next row.
     */
    @Override
    public String[] next() {
      int      lPos     = 0;
      int      lLen     = lineBuf.length();
      String[] delivery = new String[columns.length];

      try {
	      int i=0;
	      for (; i<delivery.length; i++) {
	          if (lPos >= lLen) {
		         if ((i == delivery.length-1) && lineBuf.endsWith(delimeter)) {
		            delivery[i] = symbolNA.isEmpty() ? null : AsserUtil.EMPTY_STRING;
		            break;
		         }
		         if (fillRows) {
		             break;
		         } else {
	                 throw new IllegalStateException("Only "+i+'/'+delivery.length+" columns at line "+line);
		         }
		      }
	          lPos = readEntry(lineBuf, delimeter, quotes, delivery, i, lPos, line);
	          if ((symbolNA != null) && symbolNA.equals(delivery[i]))
	        	  delivery[i] = null;
	      }
      }
      finally {
	      try { nextLine(); }
	      catch (IOException e) {
	    	  throw new RuntimeException(e);
	      }
      }
      return delivery;
    }

    @Override
	public void remove() {
		throw new UnsupportedOperationException("read only access provided");
	}

    /**
     * Close the input resource.
     */
	public void close() {
      try { in.close(); }
      catch (IOException e) {
    	  // We may ignore these errors.
    	  System.err.println("***** Cannot close a CSV file: "+e.getLocalizedMessage());
      }
    }

	/**
	 * Fetch one column from the given input line.
     *
     * @param  input  Line that contains the column of interest
     * @param  delim  Column delimiter
     * @param  quotes This vector is populated for the quotation status of the columns
     * @param  output Output entry is stored into the corresponding column of this vector
     * @param  qIndex Column index
     * @param  begin  Character index for the starting position of the column
     * @param  line   Line number for the error messages
     * @return New starting position for the next column
	 */
    static int readEntry(String    input,
                         String    delim,
                         boolean[] quotes,
                         String[]  output,
                         int       qIndex,
                         int       begin,
                         int       line) {
       int start = begin;
       int dLen  = delim.length();
       int end   = input.indexOf(delim, start);
       if (end < 0) end = input.length();

       boolean hasQuote = (start < end) && (input.charAt(start) == '\"');
       if (hasQuote) {
           if (end < start+2) end = input.indexOf(delim, end+1);
           while (input.charAt(end-1) != '\"') {
               if (end == input.length())
            	  throw new IllegalStateException("Premature end of item at line "+line+" (column "+(qIndex+1)+"): "+input.substring(start));
               end = input.indexOf(delim, end+dLen);
               if (end < 0) end = input.length();
           }
           start++;
           end--;
           output[qIndex] = input.substring(start, end);
           if (output[qIndex].indexOf("\\\"") > 0)
               output[qIndex] = output[qIndex].replaceAll("\\\\\"", "\"");
       } else {
           output[qIndex] = input.substring(start, end);
       }
       if (quotes != null) quotes[qIndex] = hasQuote;
       return hasQuote ? end+dLen+1 : end+dLen;
    }

    static public void main(String[] argv) {
        Map<String, MainArgument> args = new HashMap<String, MainArgument>();
        MainArgument argNA     = MainArgument.add(  "NA", "symbol", "Missing value symbol", DEFAULT_MISSING_VALUE_SYMBOL, args);
        MainArgument argNoDocs = MainArgument.add(   "c",           "Do not skip comment lines", args);
        MainArgument argDelim  = MainArgument.add(   "d", "char",   "Column delimiter", "\\t", args);
        MainArgument argHideS  = MainArgument.add(   "h",           "Do not print column statistics", args);
        MainArgument argHideA  = MainArgument.add(   "H",           "Do not print any statistics", args);
        MainArgument argSkip   = MainArgument.add("skip", "n",      "Skip n lines from the beginning of each file", "0", args);
        String[] files;
        try { files = MainArgument.parseInput(argv, args, 1, -1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            System.err.println();
            printSynopsis(args);
            System.exit(-1); return;
        }

        DecimalFormat naFormat = new DecimalFormat("###000000 ");
        for (String filename : files) {
            System.out.println("Analysing "+filename+"...");
            CSVParser parser = null;
            long      time   = System.currentTimeMillis();
            boolean[] quot;
            int[]     nas;
            try {
                parser = new CSVParser(new File(filename),
                                       ArgumentEncoding.decode(argDelim.getValue()),
                                       argSkip.getInt(),
                                       ArgumentEncoding.decode(argNA.getValue()),
                                       !argNoDocs.isUsed());
                nas  = new int[parser.getColumnCount()];
                quot = new boolean[nas.length];
                for (String[] line : parser) {
                    for (int i=0; i<nas.length; i++) {
                        if (line[i] == null)  nas[i]++;
                        if (parser.quotes[i]) quot[i] = true;
                    }
                }
                parser.close();
            } catch (Exception e) {
            	if (parser == null) {
            		System.err.println("Parser construction failed for "+filename+':');
            	} else {
            		System.err.println("Parser failed at line "+(parser.getLineNumber()-1)+':');
            	}
                e.printStackTrace();
                continue;
            }
            if (argHideA.isUsed()) continue;
            time = System.currentTimeMillis()-time;
            System.out.println("- reading time "+(time/1000.0)+" seconds");
            System.out.println("- total of "+nas.length+" columns and "+
                               (parser.getLineNumber()-1)+" rows");
            if (!argHideS.isUsed()) {
               System.out.println("- \"    NAs name");
               for (int i=0; i<nas.length; i++) {
                   System.out.println((quot[i] ? "  \" " : "    ")+
                                      naFormat.format(nas[i])+
                                      parser.columns[i]);
               }
            }
        }
    }

    /**
     * Print a list of command line arguments to the standard error stream.
     */
    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("CSVValidator v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("-----------------------------------");
        System.err.println("validateCSV [options] files...");
        MainArgument.printArguments(args, System.err);
    }

}
