package org.anduril.asser.io;

import java.io.*;

/**
 * A convenient writer for comma separated files with column names.
 * This class writer takes care of column separators, field quotations and
 * the creation of new lines.
 */
public class CSVWriter {

	private PrintWriter out;
	private String      delimiter;
	private String[]    cols;
	private int         phase;
	private int         line;
	private String      na = "NA";

	/**
	 * Create a new CSV file using the {@link org.anduril.asser.io.CSVParser#DEFAULT_DELIMITER default delimiter}.
	 *
	 * @param  columns     Column names
	 * @param  filename    Name of the file
	 * @throws IOException If the file cannot be created
	 */
	public CSVWriter(String[] columns,
			         String   filename) throws IOException {
		this(columns, new FileOutputStream(filename), CSVParser.DEFAULT_DELIMITER, true);
	}

    /**
     * Create a new CSV file using the {@link org.anduril.asser.io.CSVParser#DEFAULT_DELIMITER default delimiter}.
     *
     * @param  columns      Column names
     * @param  filename     Name of the file
     * @param  useQuotation This flag can be used to enable quotation marks around column names
     * @throws IOException  If the file cannot be created
     */
    public CSVWriter(String[] columns,
                     String   filename,
                     boolean  useQuotation) throws IOException {
        this(columns, new FileOutputStream(filename), CSVParser.DEFAULT_DELIMITER, useQuotation);
    }

	/**
	 * Create a new CSV file using the {@link org.anduril.asser.io.CSVParser#DEFAULT_DELIMITER default delimiter}.
	 *
	 * @param  columns     Column names
	 * @param  file        Target file
	 * @throws IOException If the file cannot be created
	 */
	public CSVWriter(String[] columns,
			         File     file) throws IOException {
		this(columns, new FileOutputStream(file), CSVParser.DEFAULT_DELIMITER, true);
	}

    /**
     * Create a new CSV file using the {@link org.anduril.asser.io.CSVParser#DEFAULT_DELIMITER default delimiter}.
     *
     * @param  columns      Column names
     * @param  file         Target file
     * @param  useQuotation This flag can be used to enable quotation marks around column names
     * @throws IOException  If the file cannot be created
     */
    public CSVWriter(String[] columns,
                     File     file,
                     boolean  useQuotation) throws IOException {
        this(columns, new FileOutputStream(file), CSVParser.DEFAULT_DELIMITER, useQuotation);
    }

	/**
	 * Create a new CSV file using the given delimiter.
	 *
	 * @param  columns     Column names
	 * @param  filename    Name of the file
	 * @param  delim       Column delimiter
	 * @throws IOException If the file cannot be created
	 */
	public CSVWriter(String[] columns,
			         String   filename,
			         String   delim) throws IOException {
		this(columns, new FileOutputStream(filename), delim, true);
	}

	/**
	 * Create a new CSV file using the given delimiter.
	 *
	 * @param  columns      Column names
	 * @param  file         Target file
	 * @param  delim        Column delimiter
	 * @param  useQuotation This flag can be used to enable quotation marks around column names
	 * @throws IOException  If the file cannot be created
	 */
	public CSVWriter(String[] columns,
			         File     file,
			         String   delim,
			         boolean  useQuotation) throws IOException {
		this(columns, new FileOutputStream(file), delim, useQuotation);
	}

	/**
	 * Start writing to the given stream.
	 *
	 * @param columns Column names
	 */
	public CSVWriter(String[]     columns,
	                 OutputStream output) {
		this(columns, output, CSVParser.DEFAULT_DELIMITER, true);
	}

    /**
     * Write to the given destination.
     * 
     * @param columns Column names
     * @param output  Output destination
     * @param delim   Column delimiter character
     */
    public CSVWriter(String[]     columns,
                     OutputStream output,
                     String       delim) {
        this(columns, output, delim, true);
    }

	/**
	 * Write to the given destination.
	 * 
	 * @param columns      Column names
	 * @param output       Output destination
	 * @param delim        Column delimiter character
     * @param useQuotation This flag can be used to enable quotation marks around column names
	 */
	public CSVWriter(String[]     columns,
			         OutputStream output,
			         String       delim,
                     boolean      useQuotation) {
		delimiter = delim;
		cols      = columns;
		phase     = 0;
		line      = 1;
		out       = new PrintWriter(output);

		for (int i=0; i<columns.length; i++) {
			if (i>0)
				out.append(delim);
			String col = columns[i];
            if (col == null)
               throw new IllegalArgumentException("Column name is null at index: "+i);
            if (useQuotation) {
			   col = col.replaceAll("\"", "\"");
			   out.append('\"').append(col).append('\"');
            } else {
               out.append(col);
            }
		}
		line++;
		out.print('\n'); // Use UNIX line feeds only!
	}

	/**
	 * Write to the given destination without a line that would describe the column names.
	 *
	 * @param columns      Column count
	 * @param output       Output destination
	 * @param delim        Column delimiter character
	 */
	public CSVWriter(int          columns,
			         OutputStream output,
			         String       delim) {
		delimiter = delim;
		cols      = new String[columns];
		phase     = 0;
		line      = 1;
		out       = new PrintWriter(output);
		
		for (int i=0; i<cols.length; i++) {
			cols[i] = "column"+(i+1);
		}
	}

	/**
	 * Set a value symbol for skipped columns.
	 */
	public void setMissingValue(String symbol) {
		na = symbol;
	}

    /**
     * Returns the current missing value symbol.
     */
	public String getMissingValue() {
		return na;
	}

    /**
     * Number of columns in the file.
     */
    public int getColumnCount() {
      return cols.length;
    }

	/**
	 * Returns the column number for the column that is to be written.
	 */
	public int getColumn() {
		return (phase+1);
	}

    /**
     * Column labels.
     */
    public String[] getColumnNames() {
      return cols;
    }

    /**
     * Return the number of the current line.
     * The column headers appear at line one.
     */
	public int getLineNumber() {
		return line;
	}

    /**
     * Write one column.
     * 
     * @return True if a line is completed
     */
    public boolean write(boolean value) {
        return write(String.valueOf(value), false);
    }

    /**
     * Write one column.
     * 
     * @return True if a line is completed
     */
    public boolean write(char value) {
        return write(String.valueOf(value), false);
    }

	/**
	 * Write one column.
	 * 
	 * @return True if a line is completed
	 */
	public boolean write(long value) {
		return write(String.valueOf(value), false);
	}

	/**
	 * Write one column.
	 * 
	 * @return True if a line is completed
	 */
	public boolean write(double value) {
		String v = (Double.isNaN(value) || Double.isInfinite(value)) ?
				   na : String.valueOf(value); 
		return write(v, false);
	}

	/**
	 * Write one column.
	 * 
	 * @return True if a line is completed
	 */
	public boolean write(String value) {
		return write(value, true);
	}

	/**
	 * Add one empty column.
	 *
	 * @return True if a line is completed
	 */
	public boolean skip() {
		return write(null, false);
	}

	/**
	 * Add several empty columns.
	 *
	 * @return True if a line is completed
	 */
	public boolean skip(int colCount) {
		boolean lf = false;
		for (int i=0; i<colCount; i++) {
		    lf = write(null, false) || lf;
		}
		return lf;
	}

	/**
	 * Write one column.
	 *
	 * @param entry        Value to be written
	 * @param useQuotation This flag enables quotation marks around the given value
	 * @return             True if a line is completed
	 */
	public boolean write(String entry, boolean useQuotation) {
		synchronized (this) {

		if (phase > 0)
			out.append(delimiter);
		if (entry == null) {
			out.append(na);
		} else {
			String col = entry;
			if (useQuotation) {
				col = col.replaceAll("\"", "\"");
				out.append('\"').append(col).append('\"');					
			} else {
				if (col.indexOf(delimiter) >= 0)
					throw new IllegalArgumentException("There is a column separator in '"+entry+"'.");
				out.append(col);
			}
		}

		phase++;
		if (phase >= cols.length) {
			out.print('\n'); // Use UNIX line feeds only!
			phase = 0;
			line++;
			return true; 
		}
		return false;

		}
	}

	/**
	 * Write one row.
	 *
	 * @param entries      Values of each cell
	 * @param useQuotation This flag enables quotation marks around the given value
	 */
	public void writeRow(boolean useQuotation, String... entries) {
		synchronized (this) {

		if (phase != 0)
		   throw new IllegalStateException("Expecting content for "+
				                           cols[phase]+
				                           ". This is not the first column of the row.");
		if (entries.length != cols.length)
			throw new IllegalArgumentException("Input contains "+entries.length+
					                           " value(s) but the file has "+cols.length+
					                           " column(s).");
		for (String entry : entries) {
			write(entry, useQuotation);
		}

		}
	}

	/**
	 * Write one row with quotations around each cell.
	 *
	 * @param entries Values of each cell.
	 */
	public void writeRow(String... entries) {
		writeRow(true, entries);
	}

	/**
     * Close the output stream.
     */
	public void close() {
		out.close();
		if (phase != 0)
			throw new IllegalStateException("Line "+line+" has only "+phase+" out of "+cols.length+" columns.");
	}

	/**
	 * Flush the underlying output buffer so that the current content is send forward.
	 */
	public void flush() {
		out.flush();
	}

}
