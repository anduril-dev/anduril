package org.anduril.asser.io;

import org.junit.Test;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.io.CSVParser}.
 *
 * @author Marko Laakso
 */
public class CSVParserTest {

    static private final String TEST_FILE_ANDURIL = "org.anduril/asser/io/pipeline.csv";
    static private final String TEST_FILE_SIMPLE  = "org.anduril/asser/io/simple.csv";

    @Test
    public void testSimpleFile() throws IOException {
        CSVParser parser = new CSVParser(ClassLoader.getSystemResourceAsStream(TEST_FILE_SIMPLE),
                                         CSVParser.DEFAULT_DELIMITER,
 								         0,
								         CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
								         true);

        // Read header
        assertEquals(4, parser.getColumnCount());
        String[] cols = parser.getColumnNames();
        assertEquals(parser.getColumnCount(), cols.length);
        assertEquals(2, parser.getLineNumber());
        assertEquals("ID",     cols[0]);
        assertEquals("value1", cols[1]);
        assertEquals("value2", cols[2]);
        assertEquals("value3", cols[3]);

        // Read content
        assertTrue(parser.hasNext());
        cols = parser.next();
        assertEquals(3, parser.getLineNumber());
        assertEquals("i1",  cols[0]); assertTrue (parser.hasQuotations(0));
        assertEquals("v11", cols[1]); assertTrue (parser.hasQuotations(1));
        assertEquals("v12", cols[2]); assertFalse(parser.hasQuotations(2));
        assertEquals("v13", cols[3]); assertTrue (parser.hasQuotations(3));

        assertTrue(parser.hasNext());
        cols = parser.next();
        assertEquals("i2",  cols[0]); assertTrue (parser.hasQuotations(0));
        assertEquals("v21", cols[1]); assertTrue (parser.hasQuotations(1));
        assertEquals("v22", cols[2]); assertFalse(parser.hasQuotations(2));
        assertEquals("v23", cols[3]); assertTrue (parser.hasQuotations(3));

        assertFalse(parser.hasNext());
        parser.close();
    }

	@Test
	public void testPipelineFile() throws IOException {
		CSVParser parser = new CSVParser(ClassLoader.getSystemResourceAsStream(TEST_FILE_ANDURIL),
				                         CSVParser.DEFAULT_DELIMITER,
				                         0,
				                         CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
				                         true);
		
		// Read header
		assertEquals(3, parser.getColumnCount());
		String[] cols = parser.getColumnNames();
		assertEquals(parser.getColumnCount(), cols.length);
		assertEquals(4, parser.getLineNumber());
		assertEquals("ID",     cols[0]);
		assertEquals("value1", cols[1]);
		assertEquals("value2", cols[2]);

		// Read content
		assertTrue(parser.hasNext());
		cols = parser.next();
		assertEquals(5, parser.getLineNumber());
		assertEquals("i1",  cols[0]); assertFalse(parser.hasQuotations(0));
		assertEquals("v11", cols[1]); assertTrue (parser.hasQuotations(1));
		assertEquals("v12", cols[2]); assertFalse(parser.hasQuotations(2));
		
		assertTrue(parser.hasNext());
		cols = parser.next();
		assertEquals(6, parser.getLineNumber());
		assertEquals("i2",  cols[0]); assertFalse(parser.hasQuotations(0));
		assertEquals("v21", cols[1]); assertFalse(parser.hasQuotations(1));
		assertEquals("v22", cols[2]); assertFalse(parser.hasQuotations(2));

		assertTrue(parser.hasNext());
		try {
			cols = parser.next();
			fail("No error although the input was too short!");
	    }
		catch (IllegalStateException e) {
			String msg = e.getMessage();
			assertTrue(msg.indexOf('6') > 0);
		}
		assertEquals(7, parser.getLineNumber());
		
		assertTrue(parser.hasNext());
		cols = parser.next();
		assertEquals(8, parser.getLineNumber());
		assertEquals("i4",  cols[0]); assertTrue (parser.hasQuotations(0));
		assertEquals("v41", cols[1]); assertTrue (parser.hasQuotations(1));
		assertEquals("v42", cols[2]); assertTrue (parser.hasQuotations(2));

		assertTrue(parser.hasNext());
		cols = parser.next();
		assertEquals(9, parser.getLineNumber());
		assertEquals("i5",  cols[0]); assertFalse(parser.hasQuotations(0));
		assertEquals("",    cols[1]); assertFalse(parser.hasQuotations(1));
		assertEquals("v52", cols[2]); assertFalse(parser.hasQuotations(2));

		assertTrue(parser.hasNext());
		cols = parser.next();
		assertEquals(10, parser.getLineNumber());
		assertEquals("",    cols[0]); assertFalse(parser.hasQuotations(0));
		assertEquals("v61", cols[1]); assertFalse(parser.hasQuotations(1));
		assertEquals("v62", cols[2]); assertFalse(parser.hasQuotations(2));

		assertTrue(parser.hasNext());
		cols = parser.next();
		assertEquals(10, parser.getLineNumber());
		assertEquals("i7",  cols[0]); assertFalse(parser.hasQuotations(0));
		assertEquals("v71", cols[1]); assertFalse(parser.hasQuotations(1));
		assertEquals("",    cols[2]); assertFalse(parser.hasQuotations(2));

		assertFalse(parser.hasNext());
        parser.close();
	}

	@Test
	public void testReadEntry() {
	  String    delim  = "\t";
      boolean[] quotes = new boolean[2];
      String[]  res    = new String[2];

      CSVParser.readEntry("foo",          delim,   null, res, 0, 0, 123); // NOPMD
      assertEquals("foo",      res[0]);

      CSVParser.readEntry("foo",          delim, quotes, res, 0, 0, 123);
      assertEquals("foo",      res[0]);
      assertFalse(quotes[0]);

      CSVParser.readEntry("\"foo\"",      delim, quotes, res, 0, 0, 123);
      assertEquals("foo",      res[0]);
      assertTrue(quotes[0]);

      CSVParser.readEntry("\"f\\\"oo\"",  delim, quotes, res, 0, 0, 123);
      assertEquals("f\"oo",    res[0]);
      assertTrue(quotes[0]);

      CSVParser.readEntry("\"foo\tbar\"", delim, quotes, res, 1, 0, 123);
      assertEquals("foo\tbar", res[1]);
      assertTrue(quotes[1]);

      CSVParser.readEntry("foo\tbar",     delim, quotes, res, 1, 0, 123);
      assertEquals("foo",      res[1]);
      assertFalse(quotes[1]);

      CSVParser.readEntry("\"\tfoo bar\t\"", delim, quotes, res, 0, 0, 123);
      assertEquals("\tfoo bar\t", res[0]);
      assertTrue(quotes[0]);

       CSVParser.readEntry("\"\"", delim, quotes, res, 0, 0, 123);
      assertEquals("", res[0]);
      assertTrue(quotes[0]);

	}

    @Test
    public void testForEach() throws IOException {
		CSVParser parser = new CSVParser(ClassLoader.getSystemResourceAsStream(TEST_FILE_ANDURIL),
                                         CSVParser.DEFAULT_DELIMITER,
						                 0,
						                 CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
						                 true);
        int rowC = 0;

        for (String[] line : parser) {
            rowC++;
            if (rowC == 2) try {
                assertEquals("i2", line[0]);
                line = parser.next();
                fail("No error although the input was too short!");
            }
            catch (IllegalStateException e) {
                rowC++; // Line skip
            }
        }
        assertEquals(7, rowC);
        parser.close();
    }

    @Test
    public void testNoHeader() throws IOException {
    	CSVParser parser = new CSVParser(new InputStreamReader(ClassLoader.getSystemResourceAsStream(TEST_FILE_SIMPLE)),
    			                         CSVParser.DEFAULT_DELIMITER,
    			                         0,
    			                         CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
    			                         false,
    			                         false);

        // Make sure that column names are missing
        assertEquals(4, parser.getColumnCount());
        String[] cols = parser.getColumnNames();
        assertEquals(parser.getColumnCount(), cols.length);
        assertEquals(1, parser.getLineNumber());
        assertNull(cols[0]);
        assertNull(cols[1]);
        assertNull(cols[2]);
        assertNull(cols[3]);

        // Read header as content
        cols = parser.next();
        assertEquals("ID",     cols[0]);
        assertEquals("value1", cols[1]);
        assertEquals("value2", cols[2]);
        assertEquals("value3", cols[3]);
        assertEquals(2, parser.getLineNumber());

		assertTrue(parser.hasNext());
		cols = parser.next();
        assertEquals(3, parser.getLineNumber());
        assertEquals("i1",  cols[0]); assertTrue (parser.hasQuotations(0));
        assertEquals("v11", cols[1]); assertTrue (parser.hasQuotations(1));
        assertEquals("v12", cols[2]); assertFalse(parser.hasQuotations(2));
        assertEquals("v13", cols[3]); assertTrue (parser.hasQuotations(3));

    	parser.close();
    }

    @Test
    public void testNoHeaderEmpty() throws IOException {
    	CSVParser parser = new CSVParser(new CharArrayReader(new char[0]),
    			                         CSVParser.DEFAULT_DELIMITER,
    			                         0,
    			                         CSVParser.DEFAULT_MISSING_VALUE_SYMBOL,
    			                         false,
    			                         false);

        // Make sure that column names are missing
        assertEquals(0, parser.getColumnCount());
        String[] cols = parser.getColumnNames();
        assertEquals(parser.getColumnCount(), cols.length);
        assertEquals(0, parser.getLineNumber());
        assertFalse(parser.hasNext());

    	parser.close();
    }

}