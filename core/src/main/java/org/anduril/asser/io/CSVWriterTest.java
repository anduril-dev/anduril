package org.anduril.asser.io;

import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.io.CSVWriter}.
 *
 * @author Marko Laakso
 */
public class CSVWriterTest {

	@Test
	public void testWriteTable() {
	  String[]              columns = new String[] { "H1", "H2" }; 
	  ByteArrayOutputStream buffer  = new ByteArrayOutputStream(500);
	  CSVWriter             out     = new CSVWriter(columns, buffer);
	  
	  assertEquals(2, out.getLineNumber());
	  assertEquals(1, out.getColumn());
	  out.write(123);
	  assertEquals(2, out.getColumn());
	  out.write(456.0);
	  assertEquals(3, out.getLineNumber());
	  assertEquals(1, out.getColumn());
	  out.write("foo");
	  out.write("bar");
	  assertEquals(4, out.getLineNumber());
	  assertEquals(1, out.getColumn());
	  out.write("", false);
	  out.write("", true);
	  assertEquals(5, out.getLineNumber());
	  assertEquals(1, out.getColumn());

	  out.close();
	  assertEquals("\"H1\"\t\"H2\"\n"+
			       "123\t456.0\n"+
			       "\"foo\"\t\"bar\"\n"+
			       "\t\"\"\n",
			       buffer.toString());
	}

	@Test
	public void testWriteRow() {
	  String[]              columns = new String[] { "H1", "H2" }; 
	  ByteArrayOutputStream buffer  = new ByteArrayOutputStream(500);
	  CSVWriter             out     = new CSVWriter(columns, buffer);

	  out.writeRow(false, "123", "456.0");
	  assertEquals(3, out.getLineNumber());
	  assertEquals(1, out.getColumn());
	  out.writeRow("foo", "bar");
	  assertEquals(4, out.getLineNumber());
	  assertEquals(1, out.getColumn());
	  out.write("", false);
	  assertEquals(2, out.getColumn());
	  try {
		  out.writeRow(columns);
		  fail("No error although the previous line was not completed!");
	  }
	  catch (IllegalStateException e) {
          String msg = e.getMessage();
          assertTrue(msg.startsWith("Expecting content for H2."));
      }
	  assertEquals(2, out.getColumn());
	  out.write("", true);
	  try {
		  out.writeRow("justme");
		  fail("No error although the given input lacks columns!");
	  }
	  catch (IllegalArgumentException e) {
          assertEquals("Input contains 1 value(s) but the file has 2 column(s).",
					   e.getMessage());
      }
	  assertEquals(5, out.getLineNumber());
	  assertEquals(1, out.getColumn());

	  out.close();
	  assertEquals("\"H1\"\t\"H2\"\n"+
			       "123\t456.0\n"+
			       "\"foo\"\t\"bar\"\n"+
			       "\t\"\"\n",
			       buffer.toString());
	}

}
