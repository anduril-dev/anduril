package org.anduril.asser.io;

import java.io.*;
import java.util.*;

/**
 * FASTA sequence file. 
 */
public class FastaFile implements Iterable<byte[]> {

	private final List<String> names = new ArrayList<String>();
	private final List<byte[]> seqs  = new ArrayList<byte[]>();

    /**
     * Create an empty file. 
     */
    public FastaFile() {
        // Empty file with no sequencies
    }

	/**
	 * Get index for the given sequence name.
	 */
	public int getIndex(String name) {
		return names.indexOf(name);
	}

	/**
	 * Get the name of the sequence for the given sequence index.
	 */
	public String getName(int index) {
		return names.get(index);
	}

	/**
	 * Get the actual sequence data for the given sequence index.
	 */
	public byte[] getSequence(int index) {
		return seqs.get(index);
	}

	/**
	 * Get the actual sequence data for the given sequence name.
	 */
	public byte[] getSequence(String name) {
		int index = getIndex(name);
		if (index < 0)
			throw new IllegalArgumentException("No such sequence: "+name);
		return seqs.get(index);
	}

	/**
	 * Returns the number of sequences in this file.
	 */
	public int size() {
		return seqs.size();
	}

	/**
	 * Adds a new sequence into the file.
	 */
	public void addSequence(String name, byte[] seq) {
		names.add(name);
		seqs.add(seq);
	}

    public void remove(int index) {
        names.remove(index);
        seqs.remove(index);
    }

    public Iterator<byte[]> iterator() {
        return new Iterator<byte[]>() {
            int i = 0;
            public boolean hasNext() {
                return (i<size());
            }
            public byte[] next() {
                return getSequence(i++);
            }
            public void remove() {
                FastaFile.this.remove(--i);
            }
        };
    }

	/**
	 * Read all sequences from the given file.
	 */
	static public FastaFile read(String filename) throws IOException {
		return read(filename, null);
	}

    /**
     * Read all sequences from the given file.
     */
    static public FastaFile read(File file) throws IOException {
        return read(file, null);
    }

    /**
     * Read only some sequences from the given file.
     * 
     * @param filename     Name of the FASTA file
     * @param seqInterest  A set of sequence names to be read
     * @return             Collection of selected sequences
     * @throws IOException If the file cannot be read
     */
    static public FastaFile read(String      filename,
                                 Set<String> seqInterest) throws IOException {
        return read(new File(filename), seqInterest);
    }

	/**
	 * Read only some sequences from the given file.
	 * 
	 * @param file         A reference for the FASTA file
	 * @param seqInterest  A set of sequence names to be read
	 * @return             Collection of selected sequences
	 * @throws IOException If the file cannot be read
	 */
	static public FastaFile read(File        file,
			                     Set<String> seqInterest) throws IOException {
		BufferedReader        in    = new BufferedReader(new FileReader(file));
		ByteArrayOutputStream bbuf  = new ByteArrayOutputStream();
		FastaFile             fasta = new FastaFile();
		String                name  = null;
		String                line;

		while ((line=in.readLine()) != null) {
			line = line.trim();
			if (line.length() < 1)
				continue;
			if (line.charAt(0) == '>') {
				if (name != null) {
					fasta.addSequence(name, bbuf.toByteArray());
					bbuf.reset();
				}
				StringTokenizer descParser = new StringTokenizer(line.substring(1));
				name = descParser.nextToken();
				if ((seqInterest != null) && !seqInterest.contains(name)) {
				   name = null;
				   continue;
				}
			} else {
			    if (name != null) bbuf.write(line.getBytes());
			}
		}
		if (name != null) {
			fasta.addSequence(name, bbuf.toByteArray());
		}

		in.close();
		return fasta;
	}

    /**
     * Save content of this instance to the given file. 
     * 
     * @param  filename    Name of the FASTA file
     * @throws IOException If the file cannot be written
     */
    public void write(String filename) throws IOException {
        write(new File(filename));
    }

    /**
     * Save content of this instance to the given file. 
     * 
     * @param  file        Target FASTA file
     * @throws IOException If the file cannot be written
     */
    public void write(File file) throws IOException {
        PrintWriter out = new PrintWriter(file);
        write(out);
        out.close();
    }

    /**
     * Print content of this instance with the given writer. 
     * 
     * @throws IOException If the file cannot be written
     */
    public void write(PrintWriter out) throws IOException {
        int size = size();
        for (int i=0; i<size; i++) {
            String name = getName(i);
            byte[] seq  = getSequence(i);
            int    s    = 0;
            int    e;
            String line;

            out.print('>');
            out.println(name);

            while (true) {
                e    = Math.min(s+69, seq.length-1);
                line = new String(seq, s, e-s+1);
                out.println(line);
                if (e == seq.length-1)
                    break;
                s = e+1;
            }
        }
    }

}
