package org.anduril.asser.io;

import org.junit.Test;

import java.text.Format;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.io.CSVCleaner}.
 *
 * @author Marko Laakso
 */
public class CSVCleanerTest {

    static private final String[] O_COLS = { "foo1", "foo2", "foo3" }; // NOPMD

    @Test
    public void testRenameColumns() {
        String[] res;

        res = CSVCleaner.renameColumns(O_COLS, "");
        assertArrayEquals(new String[] { "foo1", "foo2", "foo3" }, res);
        assertSame(O_COLS, res);

        res = CSVCleaner.renameColumns(O_COLS, "foo2=fooKaksi");
        assertArrayEquals(new String[] { "foo1", "fooKaksi", "foo3" }, res);

        res = CSVCleaner.renameColumns(O_COLS, "foo2=foo3, foo1=foo");
        assertArrayEquals(new String[] { "foo", "foo3", "foo3" }, res);

        res = CSVCleaner.renameColumns(O_COLS, "foo3=foo1, foo1=foo3,foo2=foo2");
        assertArrayEquals(new String[] { "foo3", "foo2", "foo1" }, res);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testUnknownColumn() {
        CSVCleaner.renameColumns(O_COLS, "foo=foo1");
    }

    @Test
    public void testPrepareNumberFormat() {
        Format[] f = CSVCleaner.prepareNumberFormat(O_COLS, "foo2=#0.0\nfoo1=0.###E0 m/s");
        assertSame(3, f.length);
        assertEquals("1.205E1 m/s", f[0].format(12.05));
        assertEquals("5.2",         f[1].format(5.15));
        assertEquals("5.0",         f[1].format(5.05)); // RoundingModel.HALF_EVEN
        assertNull(f[2]);
    }

    @Test
    public void testPrepareSearchReplaces() {
    	final String[] EMPTY = new String[]{"",""};
    	String[][]     rules = CSVCleaner.prepareSearchReplaces(O_COLS, "foo3\nA\nB\nfoo1\n\t\n ");
    	assertSame(3, rules.length);
    	assertArrayEquals(new String[]{"\t"," "}, rules[0]);
    	assertNull(rules[1]);
    	assertArrayEquals(new String[]{"A","B"}, rules[2]);

    	rules = CSVCleaner.prepareSearchReplaces(O_COLS, "foo2\n\n");
    	assertSame(3, rules.length);
    	assertArrayEquals(EMPTY, rules[1]);

    	rules = CSVCleaner.prepareSearchReplaces(O_COLS, "foo1\n\n\nfoo3\n\n");
    	assertSame(3, rules.length);
    	assertArrayEquals(EMPTY, rules[0]);
    	assertNull(rules[1]);
    	assertArrayEquals(EMPTY, rules[2]);
    }

}