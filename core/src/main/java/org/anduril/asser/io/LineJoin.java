package org.anduril.asser.io;

import org.anduril.asser.ArgumentEncoding;
import org.anduril.asser.MainArgument;
import org.anduril.asser.Version;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * This program can be used to merge lines of two or more input files.
 * 
 * @author Marko Laakso (Marko.Laakso@Helsinki.FI)
 * @since  {@link org.anduril.asser.Version Version} 1.64
 */
public final class LineJoin {

	/** No instantiation for this library class **/
	private LineJoin() {}
	
	static public void join(String[]    filenames,
	                        PrintStream out,
			                boolean     checkID,
			                String      delim,
			                boolean     skipEmpty) throws IOException {
		BufferedReader[] ins = new BufferedReader[filenames.length];
		
		for (int i=0; i<filenames.length; i++) {
		   ins[i] = new BufferedReader(new FileReader(filenames[i]));
		}

		String line;
		String id = null;
		while ((line=ins[0].readLine()) != null) {
		  if (skipEmpty) {
		     line = line.trim();
		     if (line.length() < 1)
		        continue;
		  }
		  if (checkID) {
		     id = line.substring(0, line.indexOf(delim));
		  }
		  out.print(line);
		  for (int i=1; i<ins.length; i++) {
		      do {
		         line = ins[i].readLine();
		         if (line == null)
		            throw new EOFException("Premature end of "+filenames[i]+".");
		         if (skipEmpty)
		            line = line.trim();
		      } while (skipEmpty && (line.length()<1));
		      if (checkID) {
		         int    dPos = line.indexOf(delim);
		         String cID  = line.substring(0, dPos);
		         if (!id.equals(cID))
		            throw new IOException(filenames[i]+" refers to "+cID+" when "+filenames[0]+" has "+id+".");
		         line = line.substring(dPos+delim.length());
		      }
		      out.print(delim);
		      out.print(line);
		  }
		  out.println();
		}

		for (int i=0; i<ins.length; i++) {
		   while ((line=ins[i].readLine()) != null) {
		     line = line.trim();
		     if (line.length() > 0) {
		        System.err.println("Warning: "+filenames[i]+" has lines that were ignored");
		        break;
		     }
		   }
		   try { ins[i].close(); }
		   catch (IOException e) {
		      e.printStackTrace();
		   }
		}
	}
	
	/**
	 * Application executable.
	 */
	static public void main(String[] argv) {
		Map<String, MainArgument> args = new HashMap<String, MainArgument>();
		MainArgument argUseId = MainArgument.add("cutIDs",    "Compare first word of each line and remove replicates", args);
		MainArgument argTrim  = MainArgument.add("trim",      "Trim lines and skip the empty ones", args);
		MainArgument argDelim = MainArgument.add("d", "char", "Word delimiter", "\\t", args);
		String[] files;
        try { files = MainArgument.parseInput(argv, args, 2, -1); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
	        System.err.println();
	        printSynopsis(args);
            return;
        }
        String delim = ArgumentEncoding.decode(argDelim.getValue());

        try {
          join(files,
               System.out,
               argUseId.isUsed(),
               delim,
               argTrim.isUsed());
        }
        catch (IOException e) {
          System.err.println("Cannot join: "+Arrays.toString(files));
          e.printStackTrace();
        }
	}

    /**
     * Print a list of command line arguments to the standard error stream.
     */
    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("LineJoin v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("-------------------------------");
        System.err.println("LineJoin [options] file1 file2 ...");
        MainArgument.printArguments(args, System.err);
    }

}
