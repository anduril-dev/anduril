package org.anduril.asser.io;

import org.anduril.asser.ArgumentEncoding;
import org.anduril.asser.AsserUtil;
import org.anduril.asser.text.TextUtil;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This component can be used to simplify Anduril outputs.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.69
 */
public class CSVCleaner extends SkeletonComponent {

    static private final String ALL_SYMBOL = "*";

    static public final String INPUT_CSV         = "in";
    static public final String OUTPUT_CSV        = "out";
    static public final String PARAM_NO_QUOTES   = "skipQuotes";
    static public final String PARAM_COLUMNS     = "columnsOut";
    static public final String PARAM_COLS_IN     = "columnsIn";
    static public final String PARAM_AUTORENAME  = "autoRename";
    static public final String PARAM_RENAME      = "rename";
    static public final String PARAM_NA_OUT      = "naSymbol";
    static public final String PARAM_NA_IN       = "naIn";
    static public final String PARAM_DELIM_OUT   = "delimSymbol";
    static public final String PARAM_DELIM_IN    = "delimIn";
    static public final String PARAM_ROW_SKIP    = "rowSkip";
    static public final String PARAM_HEADER_DROP = "dropHeader";
    static public final String PARAM_ROW_FILL    = "fillRows";
    static public final String PARAM_TRIM        = "trim";
    static public final String PARAM_N_FORMAT    = "numberFormat";
    static public final String PARAM_REPLACE     = "replace";

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
    	String    columnsIn = cf.getParameter(PARAM_COLS_IN).trim();
        CSVParser in        = new CSVParser(new FileReader(cf.getInput(INPUT_CSV)),
                                            ArgumentEncoding.decode(cf.getParameter(PARAM_DELIM_IN)),
                                            cf.getIntParameter(PARAM_ROW_SKIP),
                                            ArgumentEncoding.decode(cf.getParameter(PARAM_NA_IN)),
                                            true,
                                            columnsIn.isEmpty());
        String[] skipQ    = AsserUtil.split(cf.getParameter(PARAM_NO_QUOTES));
        String[] colOut   = AsserUtil.split(cf.getParameter(PARAM_COLUMNS));
        String   rename   = cf.getParameter(PARAM_RENAME);
        String   uniqColD = cf.getParameter(PARAM_AUTORENAME);
        boolean  showCols = !cf.getBooleanParameter(PARAM_HEADER_DROP);
        boolean  fillRows = cf.getBooleanParameter(PARAM_ROW_FILL);
        boolean  trim     = cf.getBooleanParameter(PARAM_TRIM);

        in.setFillRows(fillRows);

        int[]    cols;
        String[] colIn = columnsIn.isEmpty() ? in.getColumnNames() : AsserUtil.split(columnsIn);
        if (!uniqColD.isEmpty()) {
        	TextUtil.makeUnique(colIn, uniqColD);
        }
        if ((colOut.length == 1) && ALL_SYMBOL.equals(colOut[0])) {
            cols = new int[colIn.length];
            for (int i=0; i<cols.length; i++) {
                cols[i] = i;
            }
            colOut = colIn;
        } else {
            cols = AsserUtil.indicesOf(colOut, colIn, true, true);
        }

        Format[] nFormats = prepareNumberFormat(colOut, cf.getParameter(PARAM_N_FORMAT));

        String[][] replaces = prepareSearchReplaces(colOut, cf.getParameter(PARAM_REPLACE));

        boolean[] quotas = new boolean[colOut.length];
        boolean   colQ   = false;
        if ((skipQ.length == 1) && skipQ[0].isEmpty()) {
            // Use quotations for all columns
            colQ = true;
        } else
        if ((skipQ.length == 1) && ALL_SYMBOL.equals(skipQ[0])) {
            Arrays.fill(quotas, true);
        } else {
            for (int i : AsserUtil.indicesOf(skipQ, colOut, true, true)) {
                quotas[i] = true;
            }
        }

        CSVWriter out;
        if (showCols) {
        	colOut = renameColumns(colOut, rename);
        	out = new CSVWriter(colOut,
                                cf.getOutput(OUTPUT_CSV),
                                ArgumentEncoding.decode(cf.getParameter(PARAM_DELIM_OUT)),
                                colQ);
        } else {
        	out = new CSVWriter(colOut.length,
        			            new FileOutputStream(cf.getOutput(OUTPUT_CSV)),
        			            ArgumentEncoding.decode(cf.getParameter(PARAM_DELIM_OUT)));
        }
        out.setMissingValue(ArgumentEncoding.decode(cf.getParameter(PARAM_NA_OUT)));
        for (String[] row : in) {
            for (int i=0; i<cols.length; i++) {
                String value = row[cols[i]];
                if (value != null) {
                   if (replaces[i] != null) { value = value.replaceAll(replaces[i][0], replaces[i][1]); }
                   if (trim)                { value = value.trim(); }
                   if (nFormats[i] != null) { value = nFormats[i].format(Double.parseDouble(value)); }
                }
                out.write(value, !quotas[i]);
            }
        }
        in.close();
        out.close();

        return ErrorCode.OK;
    }

    /**
     * Generate search replace rules for the output columns.
     *
     * @param colNames Column names of the input data
     * @param inputDef A line feed separated list of column names, replaces and replacements.
     */
    static public String[][] prepareSearchReplaces(String[] colNames,
                                                   String   inputDef) {
        String[][] rules = new String[colNames.length][];

        int s = 0;
        for (int i=inputDef.indexOf('\n',s); i>=s; i=inputDef.indexOf('\n',s)) {
        	String col = inputDef.substring(s,i);
        	s = i+1;
        	i = inputDef.indexOf('\n', s);
        	if (i < s) throw new IllegalArgumentException("Column "+col+" has no replacement key.");
        	String key = inputDef.substring(s,i);
        	s = i+1;
        	i = inputDef.indexOf('\n', s);
        	if (i < s) i = inputDef.length();
        	String value = inputDef.substring(s,i);
        	s = i+1;

        	int c = AsserUtil.indexOf(col, colNames, true, true);
            rules[c] = new String[] { key, value };
        }
        return rules;
    }

    /**
     * Generate number formats for the output columns.
     *
     * @param colNames Column names of the input data
     * @param formats  A line feed separated list of column names and their number formats
     */
    static public Format[] prepareNumberFormat(String[] colNames,
                                               String   formats) {
        Format[]             rules   = new Format[colNames.length];
        Map<String,String>   tokens  = AsserUtil.parseMap(formats, "\n");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US); // US English is CSBL standard for Anduril outputs

        for (Entry<String,String>t : tokens.entrySet()) {
            int    c = AsserUtil.indexOf(t.getKey(), colNames, true, true);
            rules[c] = new DecimalFormat(t.getValue(), symbols);
        }
        return rules;
    }

    /**
     * Renames the columns that are listed in the given list of replacement rules.
     *
     * @param cols  Original column names
     * @param rules A comma separated list of oldname=newname pairs
     * @return      Final column names
     */
    static public String[] renameColumns(String[] cols, String rules) {
        String[] terms = AsserUtil.split(rules);

        if ((terms.length == 1) && (terms[0].length() == 0))
            return cols;
        String[] colMe = cols.clone();
        for (String rule : terms) {
            int i = rule.indexOf('=');
            if (i<1)
               throw new IllegalArgumentException("Invalid renaming rule: '"+rule+'\'');
            String oldN = rule.substring(0,i).trim();
            String newN = rule.substring(i+1).trim();
            i = AsserUtil.indexOf(oldN, cols, true, true);
            colMe[i] = newN;
        }
        return colMe;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new CSVCleaner().run(argv);
    }

}
