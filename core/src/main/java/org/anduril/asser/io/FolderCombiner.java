package org.anduril.asser.io;

import org.anduril.asser.AsserUtil;
import org.anduril.component.*;

import java.io.File;
import java.io.IOException;

/**
 * This Anduril component copies files from its input folders to
 * its output folder.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 2.47
 */
public class FolderCombiner extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        boolean  overwrite = cf.getBooleanParameter("overwrite");
        boolean  keyNames  = cf.getBooleanParameter("keysAsNames");
        String   exclude   = cf.getParameter("exclude");
        File     outputDir = cf.getOutput("out");
        String[] roots     = AsserUtil.split(cf.getParameter("asDirs"));

        if (!outputDir.exists() && !outputDir.mkdirs())
            throw new IOException("Cannot create output folder: "+outputDir.getCanonicalPath());
        File[] content = outputDir.listFiles();
        if (content.length > 0) {
            for (File f : content) {
                cf.writeError("Output folder contains: "+f.getCanonicalPath());
            }
            return ErrorCode.OUTPUT_IO_ERROR;
        }

        // Process folders...
        IndexFile folders = IndexFile.read(cf.getInputArrayIndex("in"));
        for (int port=1; port<10; port++) {
            String key   = "in"+port;
            File   input = cf.getInput(key);
            if (input != null) {
               folders.add(key, input);
            }
        }
        for (String f : folders) {
            File id = folders.getFile(f);
            File od = (AsserUtil.indexOf(id.getName(), roots, true, false) < 0) ?
                      outputDir :
                      new File(outputDir, keyNames ? f : id.getName());
            Tools.copyDirectory(id, od, overwrite, exclude);
        }

        // Process files...
        IndexFile files = IndexFile.read(cf.getInputArrayIndex("files"));
        if (!keyNames) {
           IndexFile clone = new IndexFile();
           for (String f : files) {
               File file = files.getFile(f);
               clone.add(file.getName(), file);
           }
           files = clone;
        }
        for (int port=1; port<10; port++) {
            File input = cf.getInput("file"+port);
            if (input != null) {
               String fname = cf.getParameter("fname"+port);
               if ((fname == null) || fname.isEmpty()) {
                  fname = input.getName();
               }
               files.add(fname, input);
            }
        }
        for (String f : files) {
            File   input = files.getFile(f);
            File outFile = new File(outputDir, f);
            if (!overwrite && outFile.exists())
              	throw new IOException("Cannot copy "+input.getCanonicalPath()+" to "+
               			              outFile.getCanonicalPath()+" as it exists already.");
            Tools.copyFile(input, outFile);
        }

        return ErrorCode.OK;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new FolderCombiner().run(argv);
    }

}
