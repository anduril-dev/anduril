package org.anduril.asser.io;

import org.anduril.asser.sequence.SNPCall;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.io.HelistinOutputReader}.
 *
 * @author Marko Laakso
 */
public class HelistinOutputReaderTest {

    @Test
    public void testGenotypeFile() throws IOException {
        InputStream          file   = ClassLoader.getSystemResourceAsStream("org.anduril/asser/io/genotypes.snpr");
        HelistinOutputReader parser = new HelistinOutputReader(file);

        // Read header
        String[] cols = parser.getColumnNames();
        assertEquals(1, cols.length);
        assertEquals(1, parser.getColumnCount());
        assertEquals("rs12690344", cols[0]);

        assertTrue(parser.hasMoreMarkers());
        assertEquals("60370", parser.getSample());
        assertEquals(SNPCall.AG, parser.nextMarker());

        assertTrue(parser.hasMoreMarkers());
        assertEquals("60420", parser.getSample());
        assertEquals(SNPCall.AG, parser.nextMarker());

        assertTrue(parser.hasMoreMarkers());
        assertEquals("60430", parser.getSample());
        SNPCall[] markers = parser.markersLeft();
        assertEquals(1, markers.length);
        assertEquals(SNPCall.AG, markers[0]);
        markers = parser.markersLeft();
        assertEquals(0, markers.length);

        assertTrue(parser.hasMoreMarkers());
        assertEquals("66250", parser.getSample());
        assertEquals(SNPCall.GG, parser.nextMarker());

        assertTrue(parser.hasMoreMarkers());
        assertEquals("66340", parser.getSample());

        // Skip genotypes of the previous sample
        assertTrue(parser.hasMoreMarkers());
        assertEquals("66360", parser.nextSample());
        assertEquals(SNPCall.GG, parser.nextMarker());

        // Trust in having more data
        assertEquals("91490", parser.getSample());
        assertEquals(SNPCall.AA, parser.nextMarker());

        assertTrue(parser.hasMoreMarkers());
        assertEquals("91620", parser.getSample());
        assertEquals(SNPCall.AA, parser.nextMarker());

        assertTrue(parser.hasMoreMarkers());
        assertEquals("9690", parser.getSample());
        assertEquals(SNPCall.AA, parser.nextMarker());

        assertFalse(parser.hasMoreMarkers());
        parser.close();
    }

}
