package org.anduril.asser.io;

import org.anduril.asser.sequence.SNPCall;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * File parser for SNPHelistin genotype outputs.
 *
 * @author Marko Laakso
 */
public final class HelistinOutputReader {

	private final String[] markers;

	private final Scanner in;

	private int markerPos;
	
	private String sample;

	public HelistinOutputReader(String filename) throws IOException {
		this(new Scanner(new File(filename)));
	}

	public HelistinOutputReader(File file) throws IOException {
		this(new Scanner(file));
	}

	public HelistinOutputReader(InputStream stream) throws IOException {
		this(new Scanner(stream));
	}

	private HelistinOutputReader(Scanner reader) throws IOException {
		in = reader;

		StringTokenizer line = new StringTokenizer(in.nextLine());
		if (!line.hasMoreTokens())
			throw new EOFException("Sample column is missing");
		line.nextToken(); // Skip sample column
		List<String> cols = new LinkedList<String>();
		while (line.hasMoreTokens()) {
			cols.add(line.nextToken());
		}
		markers   = cols.toArray(new String[cols.size()]);
        markerPos = markers.length;
	}

	public int getColumnCount() {
		return markers.length;
	}

	public String[] getColumnNames() {
		return markers;
	}

	public String getSample() {
		return sample;
	}

	public String nextSample() {
		if ((markerPos < markers.length) && in.hasNext()) {
			in.nextLine(); // Skip rest of the markers
        }
		markerPos = 0;
        if (in.hasNext()) {
            sample = in.next();
        } else {
            sample = null;
        }
		return sample;
	}

	public boolean hasMoreMarkers() {
        if (markerPos >= markers.length) {
           nextSample();
        }
		return in.hasNext();
	}

	public SNPCall nextMarker() {
		SNPCall call = SNPCall.parse(in.next());
		markerPos++;
        if (markerPos >= markers.length) {
           nextSample();
        }
		return call;
	}

	public SNPCall[] markersLeft() {
		SNPCall[] calls = new SNPCall[markers.length-markerPos];

		for (int i=0; i<calls.length; i++) {
			calls[i] = SNPCall.parse(in.next());
		}
        markerPos = markers.length;
		return calls;
	}

	public void close() {
		in.close();
	}

}