package org.anduril.asser.io;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * File parser for text mappings between word aliases like IDs.
 * 
 * @since  {@link org.anduril.asser.Version Version} 1.21
 */
public class KeyMapParser {

	private String commentSign = "#";

	private String keySeparator;
	
	private boolean includeKey = false;
	
	private PrintStream warningWriter = System.err;

	public KeyMapParser() {
		this(null);
	}

	public KeyMapParser(String separator) {
		keySeparator = separator;
	}
	
	public String getCommentSign() {
		return commentSign;
	}

	public void setCommentSign(String sign) {
		commentSign = sign;
	}

	public void setIncludeKey(boolean value) {
		includeKey = value;
	}
	
	/**
	 * Set the output device for the warning messages or null to disable warnings.
	 */
	public void setWarningStream(PrintStream stream) {
		warningWriter = stream;
	}
	
	public Map<String, String> read(String filename) throws IOException {
		FileInputStream in = new FileInputStream(filename);
		try { return read(in, 32); }
		finally { in.close(); }
	}

	public Map<String, String> read(String filename, int initialCapacity) throws IOException {
		FileInputStream in = new FileInputStream(filename);
		try { return read(in, initialCapacity); }
		finally { in.close(); }
	}

    public Map<String, String> read(InputStream stream, int initialCapacity) throws IOException {
    	final float         CAP    = 0.75f;
    	BufferedReader      in     = new BufferedReader(new InputStreamReader(stream));
		Map<String, String> map    = new HashMap<String, String>((int)(initialCapacity*CAP+1), CAP);
		boolean             failed = false;
		String              line;
		String              prev;
		String              curr;
		
		while ((line=in.readLine()) != null) {
			line = line.trim();
			if ((line.length() < 1) || line.startsWith(commentSign))
				continue;
			StringTokenizer tok = (keySeparator == null) ? new StringTokenizer(line) : new StringTokenizer(line, keySeparator);
			String          key = tok.nextToken();
			if (includeKey) {
				prev = map.put(key, key);
				if (prev != null) {
					if (warningWriter != null) // NOPMD
					   warningWriter.println("Error: The key ("+key+") was already as "+prev+".");
					failed = true;
				}
			}
			while (tok.hasMoreTokens()) {
				curr = tok.nextToken();
				prev = map.put(curr, key);
				if (prev != null) {
					if (warningWriter != null)
	 			       warningWriter.println("Error: The previous mapping ("+prev+") of '"+curr+"' has been replaced with "+key+".");
					failed = true;
				}
				
			}
		}
		if (failed)
		   throw new IOException("Invalid input format.");
		return map;
	}

}
