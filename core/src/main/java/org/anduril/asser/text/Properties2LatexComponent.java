package org.anduril.asser.text;

import org.anduril.asser.AsserUtil;
import org.anduril.component.LatexTools;
import org.anduril.core.engine.DynamicError;
import org.anduril.core.engine.Engine;
import org.anduril.core.network.AbstractComponentInstance;
import org.anduril.core.network.Component;
import org.anduril.core.network.SourceLocation;

import java.io.*;
import java.util.*;

/**
 * This component generates a LaTeX fragment that describes the content
 * of the given {@link java.util.Properties} files.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.81
 */
public class Properties2LatexComponent extends AbstractComponentInstance {

    static public final String OUTPUT_FOLDER = "report";

    static public final String PARAM_SECTION      = "section";
    static public final String PARAM_SECTION_TYPE = "sectionType";
    static public final String PARAM_MERGE        = "merge";
    static public final String PARAM_COL1_DIM     = "keyWidth";
    static public final String PARAM_COL2_DIM     = "valueWidth";
    static public final String PARAM_HIDE         = "hide";

    public Properties2LatexComponent(String[]        name,
    		                          SourceLocation src,
    		                          Component      component) {
        super(name, src, component);
    }

    @Override
    public boolean isExecutable() {
        return false;
    }

    @Override
    protected File getDefaultOutputFile(String portName, File executionRoot) {
        return getRegularOutputFile(portName, executionRoot);
    }

    @Override
    public void launch(Engine engine) {
        File             root      = engine.getExecutionDirectory().getRoot();
        File             outputDir = getOutputFile(OUTPUT_FOLDER, root);
        boolean          merge     = Boolean.parseBoolean(getParameterValue(PARAM_MERGE).toString());
        String           section   = getParameterValue(PARAM_SECTION).toString().trim();
        String[]         hide      = AsserUtil.split(getParameterValue(PARAM_HIDE).toString());
        List<Properties> props     = new ArrayList<Properties>();
        List<String>     names     = new ArrayList<String>();
        Properties       loader    = null;
        int[]            tDims;
        InputStream      in;
        PrintWriter      out;

        tDims = new int[] { Integer.parseInt(getParameterValue(PARAM_COL1_DIM).toString()),
                            Integer.parseInt(getParameterValue(PARAM_COL2_DIM).toString()) };

        for (int i=0; i<10; i++) {
            String name = "props"+i;
            File   iFile = getInputFile(name, root);
            if (iFile == null)
                continue;
            if (!merge || (loader == null)) {
                loader = new Properties();
                props.add(loader);
            }
            names.add(LatexTools.quote(getInConnection(getComponent().getInPort(name)).getFrom().getName()));
            try {
              in = new FileInputStream(iFile);
              loader.load(in);
              in.close();
            } catch (IOException e) {
                engine.addError(new DynamicError("IOException while reading: "+iFile.getAbsolutePath(), this, e));
            }
        }

        if (!outputDir.exists() && !outputDir.mkdirs()) {
            engine.addError(new DynamicError("Cannot create output folder: "+outputDir.getAbsolutePath(), this));
            return;
        }

        try { out = new PrintWriter(new FileWriter(new File(outputDir, LatexTools.DOCUMENT_FILE))); }
        catch (IOException e) {
            engine.addError(new DynamicError("Cannot write output file", this, e));
            return;
        }

        if (section.length() > 0) {
           out.println(String.format("\\%s{%s}\\label{%s}",
                       getParameterValue(PARAM_SECTION_TYPE),
                       section,
                       getName()));
        }

        int      inCount = props.size();
        String[] titles  = merge ? names.toArray(new String[names.size()]) : new String[1];
        for (int i=0; i<inCount; i++) {
            if (!merge) {
                titles[0] = names.get(i);
            }
            printProperties(out, props.get(i), titles, hide, tDims);
        }
        out.close();
    }

    static private void printProperties(PrintWriter out,
                                        Properties  props,
                                        String[]    names,
                                        String[]    hide,
                                        int[]       tDims) {
        List<String> keys = new ArrayList<String>(props.stringPropertyNames());

        Collections.sort(keys);
        if (names.length > 1) {
            Arrays.sort(names);
            out.append("The following table shows a joint set of properties from: ");
            for (int i=0; i<names.length; i++) {
                if (i > 0) {
                    out.append(", ");
                    if (i == names.length-1)
                        out.append("and ");
                }
                out.print(names[i]);
            }
            out.println(".\n");
        } else {
            out.append("The following table shows the properties of \\large{\\textbf{")
               .append(names[0])
               .println("}} component.\n");
        }
        out.println(String.format("\\begin{tabular}{p{%dcm}p{%dcm}}", tDims[0], tDims[1]));
        out.println("\\textbf{property}&\\textbf{value}\\\\\\hline{}");
        int size = keys.size();
        for (int i=0; i<size; i++) {
            String key = keys.get(i);
            if (AsserUtil.indexOf(key, hide, true, false) >= 0)
                continue;
            String value = props.getProperty(key);
            out.append("\\texttt{").append(LatexTools.quote(key));
            out.print("}&");
            out.append(TextUtil.addLineWraps(value));
            if (i < size-1) {
                out.println("\\tabularnewline{}%");
            } else {
                out.println("\\tabularnewline\\hline{}");
                break;
            }
        }
        out.println("\\end{tabular}\n\n");
    }

}
