package org.anduril.asser.text;

import org.junit.Test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link org.anduril.asser.text.TextUtil}.
 * 
 * @author Marko Laakso
 */
public class TextUtilTest {

    @Test
    public void testAddLineWrapDefaults() {
        final String SHORT_TEXT = "12345/6789";

        // LatexTools.quote() should not create a new one.
        //assertSame(SHORT_TEXT, TextUtil.addLineWraps(SHORT_TEXT));
        assertEquals(SHORT_TEXT, TextUtil.addLineWraps(SHORT_TEXT));

        assertEquals("\\raggedright{\\mbox{12345}/\\hspace{0pt}\\mbox{67890}}", TextUtil.addLineWraps("12345/67890"));
        assertEquals("\\raggedright{\\mbox{12345}/\\hspace{0pt}\\mbox{67890} \\mbox{12345}/\\hspace{0pt}\\mbox{67890}}", TextUtil.addLineWraps("12345/67890 12345/67890"));
        assertEquals("\\raggedright{\\mbox{12345}/\\hspace{0pt}\\mbox{6789}/}", TextUtil.addLineWraps("12345/6789/"));
        assertEquals("\\raggedright{12345 67890 54321}", TextUtil.addLineWraps("12345 67890 54321"));
        assertEquals("\\raggedright{12345 / 67890}", TextUtil.addLineWraps("12345 / 67890"));
        assertEquals("\\raggedright{12345 \\mbox{0}/\\hspace{0pt}\\mbox{1234567890}}", TextUtil.addLineWraps("12345 0/1234567890"));
        assertEquals("\\raggedright{12345 /\\hspace{0pt}\\mbox{1234567890}}", TextUtil.addLineWraps("12345 /1234567890"));
        assertEquals("\\raggedright{12345 /12345}", TextUtil.addLineWraps("12345 /12345"));
        assertEquals("\\raggedright{\\mbox{1234567890}/ 67890}", TextUtil.addLineWraps("1234567890/ 67890"));
    }

    @Test
    public void testAddLineWrapParams() {
        assertEquals("\\raggedright{\\mbox{12345}\\hspace{0pt}/\\mbox{67890}}",
                     TextUtil.addLineWraps("12345/67890", "/", 10, false));
        assertEquals("\\raggedright{12345 /\\mbox{1234567890}}",
                TextUtil.addLineWraps("12345 /1234567890", "/", 10, false));
        assertEquals("\\raggedright{\\mbox{12345}/\\hspace{0pt}\\mbox{6789}}",
                TextUtil.addLineWraps("12345/6789", "/", 9, true));
        assertEquals("\\raggedright{\\mbox{1234}5\\hspace{0pt}\\mbox{/6789}}",
                TextUtil.addLineWraps("12345/6789", "5", 9, true));
    }

    @Test
    public void testFormat() {
        DecimalFormat format = (DecimalFormat)NumberFormat.getInstance(Locale.UK);
        format.applyPattern("#0.00");
        assertEquals("1.23", TextUtil.format("1.23",  format));
        assertEquals("1.00", TextUtil.format("1",     format));
        assertEquals("3.21", TextUtil.format("3.214", format));
    }

    @Test
    public void testFirstSentenceOf() {
        final String MISSING = "missing";
        assertEquals("",
        		     TextUtil.firstSentenceOf("", 10, MISSING));
        assertEquals("",
   		             TextUtil.firstSentenceOf("", 5, MISSING));
        assertEquals("This is a sentence.",
   		             TextUtil.firstSentenceOf("This is a sentence. This is the next one.", 50, MISSING));
        assertEquals("This is...",
		             TextUtil.firstSentenceOf("This is a sentence. This is the next one.", 10, MISSING));
        assertEquals("This is a...",
	                 TextUtil.firstSentenceOf("This is a sentence. This is the next one.", 11, MISSING));
        assertEquals(MISSING,
		             TextUtil.firstSentenceOf(null, 50, MISSING));
        assertEquals("0.123 is a floating point number.",
	                 TextUtil.firstSentenceOf("0.123 is a floating point number.", 50, MISSING));
	    assertEquals("01...",
	                 TextUtil.firstSentenceOf("0123456789", 5, null));
	    assertEquals("01...",
	                 TextUtil.firstSentenceOf("01234 56789", 5, null));
	    assertEquals("0...",
                     TextUtil.firstSentenceOf("0 12 34 56789", 5, null));
    }

    @Test
    public void textMakeUnique() {
    	String[] keys;

        keys = new String[]{null, null, null};
        TextUtil.makeUnique(keys, "");
        assertArrayEquals(new String[]{"null", "null1", "null2"}, keys);

    	keys = new String[]{"A", "A", "A-1", "A-3", "A"};
    	TextUtil.makeUnique(keys, "-");
    	assertArrayEquals(new String[]{"A", "A-2", "A-1", "A-3", "A-4"}, keys);

    	keys = new String[]{};
    	TextUtil.makeUnique(keys, "");
    	assertArrayEquals(new String[]{}, keys);

    	keys = new String[]{"A"};
    	TextUtil.makeUnique(keys, "-");
    	assertArrayEquals(new String[]{"A"}, keys);

    	keys = new String[]{"A", "A", "A1", "A3", "A"};
    	TextUtil.makeUnique(keys, "");
    	assertArrayEquals(new String[]{"A", "A2", "A1", "A3", "A4"}, keys);
    }

}