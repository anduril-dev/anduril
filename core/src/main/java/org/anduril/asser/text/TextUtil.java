package org.anduril.asser.text;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.io.CSVParser;
import org.anduril.component.LatexTools;

import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.StringTokenizer;

/**
 * Contains utilities for plain text and LaTeX formatting. 
 */
public final class TextUtil {

    private TextUtil() {
        // No instantiation
    }

    /** List of characters that may indicate line wrap points **/
    static public final String DEFAULT_LINE_WRAPS = ",./_(){}&#!?%=:;-+*\\|";

    /** List of common capital letters that may be used as line wrap points */
    static public final String CAPITAL_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * Converts a given text to an escaped LaTeX string that will support
     * line wraps for long words such as file names of URLs.
     */
    static public String addLineWraps(String text) {
        return addLineWraps(text, DEFAULT_LINE_WRAPS, 10, true);
    }

    /**
     * Converts a given text to an escaped LaTeX string that will support
     * line wraps for long words such as file names of URLs.
     *
     * @param text       Text to be converted into a LaTeX string
     * @param symbols    List of characters that may indicate a wrap point
     * @param safeLength If the length of the text is below this limit then the original text is preserved
     * @param postBreak  Add break after the delimiter character
     */
    static public String addLineWraps(String  text,
                                      String  symbols,
                                      int     safeLength,
                                      boolean postBreak) {
        int length = text.length();
        if (length <= safeLength)
            return LatexTools.quote(text);

        StringBuffer    buf  = new StringBuffer(5*length+14);
        StringTokenizer toks = new StringTokenizer(text);

        buf.append("\\raggedright{");
        while (toks.hasMoreTokens()) {
            String word = toks.nextToken();
            if (word.length() <= safeLength) {
                buf.append(LatexTools.quote(word));
            } else {
                splitWord(word, buf, symbols, postBreak);
            }
            if (toks.hasMoreTokens())
                buf.append(' ');
        }
        buf.append('}');
        return buf.toString();
    }

    static private void splitWord(String       word, //NOPMD
                                  StringBuffer buf,
                                  String       symbols,
                                  boolean      postBreak) {
        boolean phase = (symbols.indexOf(word.charAt(0)) >= 0);
        if (phase && !postBreak) {
            buf.append(LatexTools.quote(word.substring(0,1)));
            word  = word.substring(1);
            phase = (symbols.indexOf(word.charAt(0)) >= 0);
        }

        StringTokenizer toks = new StringTokenizer(word, symbols, true);
        while (toks.hasMoreTokens()) {
            String key = LatexTools.quote(toks.nextToken()); 
            if (phase) {
                if (!postBreak)
                    buf.append("\\hspace{0pt}");
                buf.append(key);
                if (postBreak && toks.hasMoreTokens())
                    buf.append("\\hspace{0pt}");
            } else {
                buf.append("\\mbox{").append(key).append('}');
            }
            phase = !phase;
        }        
    }

    /**
     * Use the given number format to represent the given numeric string value.
     * Missing values (null/'NA') are shown as
     * {@link org.anduril.asser.io.CSVParser#DEFAULT_MISSING_VALUE_SYMBOL}.
     */
    static public String format(String value, Format format) {
        if (value == null)
            return CSVParser.DEFAULT_MISSING_VALUE_SYMBOL;
        String text = value.trim();
        if ("NA".equals(text))
           return CSVParser.DEFAULT_MISSING_VALUE_SYMBOL;
        if (format instanceof NumberFormat)
           return format.format(Double.parseDouble(text));
        if (format instanceof MessageFormat)
            return format.format(new String[]{text});
        return format.format(text);
    }

    /**
     * Returns the beginning of the text. 
     *
     * @param text      Original input test.
     * @param maxLength Maximum length of the output. Too long outputs are force to fit into this space.
     *                  This argument should be greater than five.
     * @param def       Default text for the missing inputs.
     */
    static public String firstSentenceOf(String text, int maxLength, String def) {
    	if (maxLength < 5) throw new IllegalArgumentException("Expecting too short outputs ("+maxLength+"<5).");

    	String output = (text==null) ? def : text;
    	int    length = output.length();
    	int    pos;

    	for (pos=0; pos<length-1; pos++) {
    		char c = output.charAt(pos); 
    		if (((c=='.')||(c=='!')||(c=='?')) &&
    		    Character.isWhitespace(output.charAt(pos+1))) {
    			output = output.substring(0, pos+1);
    			length = output.length();
    			break;
    		}
    	}
    	if (length > maxLength) {
    		pos = maxLength-3;
    		for (int p=maxLength-2; p>0; p--) {
    			if (Character.isWhitespace(output.charAt(p))) {
    				pos = p;
    				break;
    			}
    		}
    		output = output.substring(0, pos)+"...";
    	}
    	return output;
    }

    /**
     * Prepares the values of the given array so that they become unique.
     * For example a hyphen (-) delimiter converts
     * {a, a, a-1, a-3, a} to {a, a-2, a-1, a-3, a-4}.
     *
     * @param array List of values to be modified
     * @param delim Delimiter between the counter and the original value
     */
    static public void makeUnique(String[] array, String delim) {
        final String NULL_LABEL = "null";

        if ((array.length > 0) && (array[0] == null)) array[0] = NULL_LABEL;

    	for (int i=1; i<array.length; i++) {
    	    if (array[i] == null) array[i] = NULL_LABEL;
    		boolean uniq = true;
    		for (int a=0; a<i; a++) {
    			if (array[a].equals(array[i])) {
    				uniq = false;
    				break;
    			}
    		}
    		if (uniq) continue;
    		int    c = 0;
    		int    p;
    		String trial;
    		do {
    			c++;
    			trial = array[i]+delim+c;
    			p = AsserUtil.indexOf(trial, array, true, false);
    		} while (p >= 0);
    		array[i] = trial;
    	}
    }

}