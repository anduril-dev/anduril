package org.anduril.asser;

/**
 * Version information for the asser.jar project.
 */
public final class Version {

	/** No instantiation **/
	private Version() {}

	/** Returns a string presentation of the version. **/
	static public String getVersion() {
		return "4.00";
	}

	/** Prints version number into the standard output stream. **/
	static public void main(String[] args) {
		System.out.println(getVersion());
	}

	/** Compares the given version number against the current version. **/
	static public int compare(String ver) {
		return compare(getVersion(), ver);
	}
	
	/** Compares the second version number against the first one. **/
	static public int compare(String ver1, String ver2) {
		float v1 = Float.parseFloat(ver1);
		float v2 = Float.parseFloat(ver2);
		return (int)((v2-v1)*1000);
	}

}
