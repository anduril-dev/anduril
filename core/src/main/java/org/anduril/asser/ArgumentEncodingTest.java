package org.anduril.asser;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link org.anduril.asser.ArgumentEncodingTest}.
 * 
 * @author Marko Laakso
 */
public class ArgumentEncodingTest {

    @Test
    public void testDecode() {
        assertEquals("\\",  ArgumentEncoding.decode("\\\\"));
        assertEquals("\t",  ArgumentEncoding.decode("\t"));
        assertEquals("\t",  ArgumentEncoding.decode("\\t"));
        assertEquals("\\t", ArgumentEncoding.decode("\\\\t"));
        assertEquals("AA",  ArgumentEncoding.decode("A\\e\\eA"));
    }

    @Test
    public void testEscapes() {
        for (ArgumentEncoding elem: ArgumentEncoding.values()) {
            assertEquals('A'+elem.value+ 'A', ArgumentEncoding.decode('A'+  elem.symbol+'A'));
            assertEquals('B'+elem.symbol+'B', ArgumentEncoding.decode("B\\"+elem.symbol+'B'));
        }
    }

}
