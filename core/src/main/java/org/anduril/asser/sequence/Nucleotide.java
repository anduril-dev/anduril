package org.anduril.asser.sequence;

/**
 * Enumeration of biological nucleotides that are responsible for the information
 * encoding in DNA or RNA molecules.
 *
 * @since {@link org.anduril.asser.Version Version} 1.22
 */
public enum Nucleotide {

	A("adenosine"),
	C("cytosine"),
	G("guanine"),
	T("thymine"),
	U("uracil");

	private String fullName;

	private Nucleotide(String fName) {
		fullName = fName;
	}
	
	public String getFullName() {
		return fullName;
	}

	static public Nucleotide toNucleotide(char c) {
		switch (c) { // NOPMD
		  case 'a': case 'A': return A;
		  case 'c': case 'C': return C;
		  case 'g': case 'G': return G;
		  case 't': case 'T': return T;
		  case '-':           return null;
		  default: throw new IllegalArgumentException("Invalid nucleotide symbol: "+c);
		}
	}

}
