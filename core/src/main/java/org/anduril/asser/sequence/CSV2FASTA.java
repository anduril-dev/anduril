package org.anduril.asser.sequence;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.FastaFile;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.File;
import java.io.IOException;

/**
 * This component takes sequences from the column of the given
 * CSV file and writes them into a FASTA file.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 2.14
 */
public class CSV2FASTA extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        File   in             = cf.getInput("in");
        String sequenceColumn = cf.getParameter("seqCol");
        String idColumn       = cf.getParameter("idCol");
        String compN          = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);

        FastaFile fasta = readSequenceCSV(in, sequenceColumn, idColumn, compN);
        fasta.write(cf.getOutput("out"));
        return ErrorCode.OK;
    }

    /**
     * Produces a FASTA representation for the sequences obtained from the given column of the
     * input CSV file. Tries to use 'id' column for the sequence identifiers. Identifiers like
     * seqN (where N is an incremented number starting from 0) are used if the identifiers are
     * not found.
     *
     * @param file           File pointer to the source CSV
     * @param sequenceColumn Column name for the source CSV pointing to the sequence information
     * @throws IOException   If the input file cannot be read
     */
    static public FastaFile readSequenceCSV(File   file,
    		                                String sequenceColumn) throws IOException {
    	return readSequenceCSV(file, sequenceColumn, "id", "seq");
    }

    /**
     * Produces a FASTA representation for the sequences obtained from the given column of the
     * input CSV file. 
     *
     * @param file           File pointer to the source CSV
     * @param sequenceColumn Column name for the source CSV pointing to the sequence information
     * @param idColumn       Column name for the source CSV pointing to the sequence identifiers.
     *                       If this column does not exist then the identifiers are generated automatically.
     * @param idPrefix       This prefix is used to label automatically generated sequences
     * @throws IOException   If the input file cannot be read
     */
    static public FastaFile readSequenceCSV(File   file,
    		                                String sequenceColumn,
    		                                String idColumn,
    		                                String idPrefix) throws IOException {
        FastaFile fasta  = new FastaFile();
        CSVParser in     = new CSVParser(file);
        int       seqCol = AsserUtil.indexOf(sequenceColumn, in.getColumnNames(), true, true);
        int       idCol  = AsserUtil.indexOf(idColumn,       in.getColumnNames(), true, false);
        int       idSeq  = 0;
        String    name;
        byte[]    seq;

        for (String[] row : in) {
            if (row[seqCol] == null)
               continue;

            if (idCol < 0) {
                name = idPrefix+idSeq++;
            } else {
                name = row[idCol];
                if (name == null)
                    name = idPrefix+idSeq++;
            }
            seq = row[seqCol].getBytes();
            fasta.addSequence(name, seq);
        }

        return fasta;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new CSV2FASTA().run(argv);
    }

}
