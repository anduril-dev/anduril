package org.anduril.asser.sequence;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link org.anduril.asser.sequence.SequenceUtil}.
 * 
 * @author Marko Laakso
 */
public class SequenceUtilTest {
	
	@Test
	public void testIndexOf() {
		byte[] seq1 = "AACTAA".getBytes();
		byte[] seq2 = "CT".getBytes();
		
		assertEquals( 2, SequenceUtil.indexOf(seq1, seq2));
		assertEquals(-1, SequenceUtil.indexOf(seq2, seq1));
	}

	@Test
	public void testStopCodon() {
		byte[] seq1 = "AAACTGGAAT".getBytes();
		byte[] seq2 = "TTTGGAATAACTAAAT".getBytes();

		assertEquals(0, SequenceUtil.stopCodon(0, "TAA".getBytes(), true));
		assertEquals(0, SequenceUtil.stopCodon(0, "TAG".getBytes(), true));
		assertEquals(0, SequenceUtil.stopCodon(0, "TGA".getBytes(), true));
		assertEquals(0, SequenceUtil.stopCodon(0, "TTA".getBytes(), false));
		assertEquals(0, SequenceUtil.stopCodon(0, "CTA".getBytes(), false));
		assertEquals(0, SequenceUtil.stopCodon(0, "TCA".getBytes(), false));

		assertEquals(-1, SequenceUtil.stopCodon(0,  seq1, true));
		assertEquals(-1, SequenceUtil.stopCodon(0,  seq1, false));
		assertEquals(-1, SequenceUtil.stopCodon(0,  seq2, true));
		assertEquals( 7, SequenceUtil.stopCodon(1,  seq2, true));
		assertEquals(10, SequenceUtil.stopCodon(13, seq2, false));
	}
	
	@Test
	public void testComplement() {
		assertEquals("TAGC",   SequenceUtil.complement("ATCG"));
		assertEquals("??",     SequenceUtil.complement("?O"));
		assertEquals("YRSWMK", SequenceUtil.complement("RYSWKM"));
		assertEquals("VHDB",   SequenceUtil.complement("BDHV"));
	}
	
	@Test
	public void testReverse() {
		assertEquals("TTAGCCA", SequenceUtil.reverse("ACCGATT"));
		assertEquals("TTGCCA",  SequenceUtil.reverse("ACCGTT"));
	}

	@Test
	public void testFindRepeats() {
		byte[]      seq1 = "AAATAAAATAAAAATAAAATAAA".getBytes();
		byte[]      seq2 = "TTTCTTTTTTTTTTTTCTTTTTTTTTTCT".getBytes();
		List<int[]> ans;

		ans = SequenceUtil.findRepeats(seq1, 0, seq1.length, 6);
		assertTrue(ans.isEmpty());

		ans = SequenceUtil.findRepeats(seq1, 10, seq1.length, 5);
		assertTrue(ans.isEmpty());

		ans = SequenceUtil.findRepeats(seq1, 9, seq1.length, 5);
		assertEquals(1, ans.size());
		assertArrayEquals(new int[] {9,13}, ans.get(0));
		
		ans = SequenceUtil.findRepeats(seq1, 0, seq1.length, 3);
		assertEquals(5, ans.size());
		assertArrayEquals(new int[] {0,2}, ans.get(0));
		assertArrayEquals(new int[] {4,7}, ans.get(1));
		
		ans = SequenceUtil.findRepeats(seq2, 2, seq2.length, 6);
		assertEquals(2, ans.size());
		assertArrayEquals(new int[] {4,15}, ans.get(0));
		assertArrayEquals(new int[] {17,26}, ans.get(1));
	}
	
}
