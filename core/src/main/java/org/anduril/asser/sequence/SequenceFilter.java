package org.anduril.asser.sequence;

import org.anduril.asser.io.FastaFile;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.IOException;
import java.util.Iterator;

/**
 * This Anduril component takes in a FASTA file and selects
 * sequences that satisfy the filtering criteria.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 2.31
 */
public class SequenceFilter extends SkeletonComponent {

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        int       minL  = cf.getIntParameter("minLength");
        int       maxL  = cf.getIntParameter("maxLength");
        double    maxN  = cf.getDoubleParameter("maxN");
        boolean   ratio = cf.getBooleanParameter("relative");
        int       maxNL = cf.getIntParameter("maxNSeq");
        FastaFile seqs  = FastaFile.read(cf.getInput("sequences"));

        if (maxNL < 0) {
            maxNL = Integer.MAX_VALUE;
        }
        int origSize = seqs.size();

        Iterator<byte[]> iter = seqs.iterator();
        while (iter.hasNext()) {
            byte[] seq = iter.next();
            if ((seq.length < minL) ||
                ((maxL >= 0) && (seq.length > maxL))) {
               iter.remove();
               continue;
            }
            int sN = 0;
            int tN = 0;
            int lN;
            if (maxN >= 0) {
                lN = ratio ? (int)(seq.length*maxN) : (int)maxN;
            } else {
                lN = Integer.MAX_VALUE;
            }
            for (int x=0; x<seq.length; x++) {
                if (seq[x] != 'N') {
                    sN = 0;
                    continue;
                }
                sN++;
                tN++;
                if ((sN > maxNL) || (tN > lN)) {
                    iter.remove();
                    break;
                }
            }
        }

        if (origSize == seqs.size())
            cf.writeLog("All input sequences were accepted.");
        else
            cf.writeLog("The original set of "+origSize+" sequences was reduced to "+seqs.size()+" sequences.");
        seqs.write(cf.getOutput("selection"));
        return ErrorCode.OK;
    }

    /**
     * Executes this component from the command line.
     *
     * @param argv Pipeline arguments
     */
    static public void main(String[] argv) {
        new SequenceFilter().run(argv);
    }

}
