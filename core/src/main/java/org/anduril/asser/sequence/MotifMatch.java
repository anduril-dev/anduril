package org.anduril.asser.sequence;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.MainArgument;
import org.anduril.asser.Version;
import org.anduril.asser.db.TypeConverter;
import org.anduril.asser.io.FastaFile;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * This class represents a sequence match with an associated score.
 */
public class MotifMatch {

	private final int     start;
	private final int     end;
	private final boolean forward;
	private final double  score;
	
	public MotifMatch(int     startLocus,
	                  int     endLocus,
	                  boolean originalStrand,
			          double  scoreValue) {
		start   = startLocus;
		end     = endLocus;
		forward = originalStrand;
		score   = scoreValue;
	}

	public int getFirstLocus() { return start; }

	public int getLastLocus() { return end; }
	
	public boolean isOriginalStrand() { return forward; }

	public double getScore() { return score; }

    /**
     * Returns the well scoring alignments for the motif.
     *
     * @param seq        Input sequence
     * @param motif      Query motif that is compared against the input sequence
     * @param gap        Null or (start index, min length, max length)
     * @param bg         Background probabilities of the nucleotides
     * @param pseudo     Pseudocount for the scoring function
     * @param scoreLimit Minimum score that is considered as a match
     */
	static public MotifMatch[] findMotifs(byte[]       seq, // NOPMD
			                              SeqProfile[] motif,
			                              int[]        gap,
			                              SeqProfile   bg,
			                              double       pseudo,
			                              double       scoreLimit,
			                              boolean      includeReverseStrand,
                                          boolean      relative) {
		if (seq.length < motif.length)
			throw new IllegalArgumentException("The motif is longer than the sequence.");
		List<MotifMatch> matches = new LinkedList<MotifMatch>();

        double     sLimit  = scoreLimit;
		double[][] coTable = new double[motif.length][];
		boolean    strand  = true;                      // Original strand
        int        eos     = seq.length-motif.length+1; // End of sequence
        while (strand || includeReverseStrand) {
            patternInit(strand, coTable, motif, bg, pseudo);
            if (relative) {
                sLimit = relativeScore(coTable, scoreLimit);
            }
			for (int i=0; i<eos; i++) { // Sequence loop
			    if (gap == null) {
			       double sco = countScore(seq, i, 0, motif.length-1, coTable);
				   if (sco >= sLimit)
					  matches.add(new MotifMatch(i, i+motif.length-1, strand, sco));
				} else {
				   // Count common start
				   double scoS = countScore(seq, i, 0, gap[0]-1, coTable);
				   // Count all possible tails after the gap
				   for (int gl=gap[1]; gl<=gap[2] && i+gl < eos; gl++) {
				       double sco = scoS + countScore(seq, i+gl, gap[0], motif.length-1, coTable);
				       if (sco >= sLimit)
					      matches.add(new MotifMatch(i, i+motif.length-1+gl, strand, sco));
				   }
				}
			}
			if (includeReverseStrand) {
				if (!strand) break; // Both strands completed!
				seq = SequenceUtil.takeReverseComplementCopy(seq, 0, seq.length);
			}
			strand = false;
        }
		return matches.toArray(new MotifMatch[matches.size()]);
	}

    static protected double relativeScore(double[][] table, double factor) {
        double min = 0;
        double max = 0;

        for (int i=0; i<table.length; i++) {
            if (table[i] == null) continue;
            min += AsserUtil.min(table[i]);
            max += AsserUtil.max(table[i]);
        }
        return (max-min)*factor+min;
    }

    static private void patternInit(boolean      strand,
                                    double[][]   coTable,
                                    SeqProfile[] motif,
                                    SeqProfile   bg,
                                    double       pseudo) {
        if (bg != null) {
            for (int j=0; j<motif.length; j++) {
                if (motif[j] == null) // Skip this locus
                    continue;
                coTable[j] = new double[] {
                  Math.log((motif[j].pA+pseudo)/(strand ? bg.pA : bg.pT)),
                  Math.log((motif[j].pC+pseudo)/(strand ? bg.pC : bg.pG)),
                  Math.log((motif[j].pG+pseudo)/(strand ? bg.pG : bg.pC)),
                  Math.log((motif[j].pT+pseudo)/(strand ? bg.pT : bg.pA))
                };
            }
        }
    }

	/**
     * Compares the given fragment of the score matrix against
     * the given location of the sequence.
     *
     * @param seq     Nucleotide sequence
     * @param sStart  Index of the first nucleotide to be compared
     * @param mStart  Index of the first nucleotide used from the motif
     * @param mEnd    Index of the last nucleotide used from the motif
     * @param coTable Score table representing the motif
     */
	static protected double countScore(byte[]     seq,
	                                   int        sStart,
	                                   int        mStart,
	                                   int        mEnd,
	                                   double[][] coTable) {
	    double sco = 0;
		for (int j=mStart; j<=mEnd; j++) { // Motif loop
			if (coTable[j] == null) // Skip this locus
				continue;
			double co;
			switch (seq[sStart+j]) {
			  case 'A': co = coTable[j][0]; break;
			  case 'C': co = coTable[j][1]; break;
			  case 'G': co = coTable[j][2]; break;
			  case 'T': co = coTable[j][3]; break;
			  case 'N': continue; // Skip masked regions.
			  case 'R': co = (coTable[j][0]+coTable[j][2])/2; break; // A|G
			  case 'Y': co = (coTable[j][1]+coTable[j][3])/2; break; // C|T
			  case 'S': co = (coTable[j][1]+coTable[j][2])/2; break; // C|G
			  case 'W': co = (coTable[j][0]+coTable[j][3])/2; break; // A|T
			  case 'K': co = (coTable[j][2]+coTable[j][3])/2; break; // G|T
			  case 'M': co = (coTable[j][0]+coTable[j][1])/2; break; // A|C
			  case 'B': co = (coTable[j][1]+coTable[j][2]+coTable[j][3])/3; break; // C|G|T
			  case 'D': co = (coTable[j][0]+coTable[j][2]+coTable[j][3])/3; break; // A|G|T
			  case 'H': co = (coTable[j][0]+coTable[j][1]+coTable[j][3])/3; break; // A|C|T
			  case 'V': co = (coTable[j][0]+coTable[j][1]+coTable[j][2])/3; break; // A|C|G			  
			  default: throw new IllegalArgumentException("Unknown nucleotide: "+(char)seq[sStart+j]);
			}
			sco += co;
		}
        return sco;
	}

	/**
	 * Matches the given motif profile against a set of FASTA sequences.
	 */
	public static void main(String[] argv) {
		Map<String, MainArgument> args = new HashMap<String, MainArgument>();
		MainArgument argReverse    = MainArgument.add("rev",    "Include reverse strand", args);
        MainArgument argRelativ    = MainArgument.add("relat",  "Define score limit as percentages of max versus min scores", args);
		MainArgument argVerbose    = MainArgument.add("v",      "Verbose output", args);
		MainArgument argShowSeq    = MainArgument.add("seq",    "Show matching sequence", args);
        MainArgument argScore      = MainArgument.add("score",  "value",  "Score limit", "1.0", args);
        MainArgument argPseudo     = MainArgument.add("pseudo", "v",      "Pseudocount for nucleotides", "0.04", args);
        MainArgument argFileMotif  = MainArgument.add("m",      "file",   "Motif definition file", args);
        MainArgument argAbsMotif   = MainArgument.add("abs",    "subseq", "Motif definition sequence", args);
        MainArgument argFileSeqs   = MainArgument.add("s",      "file",   "Sequence file in FASTA format", args);
        MainArgument argSeqId      = MainArgument.add("id",     "seqID",  "Sequence identifier for one sequence of interest", args);
        MainArgument argFileIds    = MainArgument.add("ids",    "file",   "File of sequence identifiers of interest", args);
        MainArgument argGap        = MainArgument.add("g",  new String[] {"l","min","max"}, "Motif gap: location minimum_length maximum_length", args);
        MainArgument argBackground = MainArgument.add("bg", new String[] {"A","C","G","T"}, "Background frequencies of the nucleotides", args);
        try { MainArgument.parseInput(argv, args, 0, 0); }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
	        System.err.println();
	        printSynopsis(args);
            return;
        }
		if (!(argFileMotif.isUsed() || argAbsMotif.isUsed()) || 
		    !argFileSeqs.isUsed()) {
		   printSynopsis(args);
		   return;
		}

        double scoreLimit;
        try { scoreLimit = Double.parseDouble(argScore.getValue()); }
        catch (NumberFormatException e) {
           System.err.println("Invalid score limit: "+argScore.getValue());
           return;
        }

        double pseudo;
        try { pseudo = Double.parseDouble(argPseudo.getValue()); }
        catch (NumberFormatException e) {
           System.err.println("Invalid pseudocount: "+argPseudo.getValue());
           return;
        }

        Set<String> intSeqs = null;
        if (argFileIds.isUsed()) {
        	intSeqs = new HashSet<String>();
        	Scanner scan;
        	try { scan = new Scanner(new File(argFileIds.getValue())); }
        	catch (IOException e) {
        		System.err.println("Cannot read sequence identifiers from "+argFileIds.getValue()+": "+
        				           e.toString());
        		return;
        	}
        	while (scan.hasNext()) {
        		intSeqs.add(scan.next());
        	}
        	scan.close();
        }
        if (argSeqId.isUsed()) {
           if (intSeqs == null)
              intSeqs = new TreeSet<String>();
           intSeqs.add(argSeqId.getValue());
        }

		FastaFile fasta;
		int       seqCount;
		if (argVerbose.isUsed()) { System.err.println("Loading FASTA input..."); }
		try { fasta = FastaFile.read(argFileSeqs.getValue(), intSeqs); }
		catch (IOException e) {
		   System.err.println("Cannot read sequences from "+argFileSeqs.getValue()+":");
		   System.err.println(e);
		   return;
		}
		seqCount = fasta.size();
		if (argVerbose.isUsed()) { System.err.println("- "+seqCount+" sequences loaded"); }

        int[] gap = null;
        if (argGap.isUsed()) {
		    try { gap = TypeConverter.toIntArray(argGap.getValues()); }
		    catch (NumberFormatException e) {
		    	System.err.println("Invalid syntax of the motif gap: "+Arrays.toString(argGap.getValues()));
		    	return;
		    }
        }

        SeqProfile[] motif;
        if (argAbsMotif.isUsed()) {
            try { motif = SeqProfile.toMotif(argAbsMotif.getValue()); }
            catch (Exception e) {
              System.err.println("Invalid motif sequence: "+argAbsMotif.getValue());
              return;
            }
        } else {
			if (argVerbose.isUsed()) { System.err.println("Loading motif matrix..."); }
			try { motif = SeqProfile.readMotif(argFileMotif.getValue()); }
			catch (IOException e) {
			   System.err.println("Cannot read the motif from "+argFileMotif.getValue()+":");
			   System.err.println(e);
			   return;
			}
		}
		if (argVerbose.isUsed()) {
		   System.err.print("- Motif ("+SeqProfile.toConcensus(motif, gap)+") length is ");
		   if (gap == null)
			   System.err.println(motif.length);
		   else
			   System.err.println((gap[1]==gap[2]) ? motif.length+gap[1] : (motif.length+gap[1])+"-"+(motif.length+gap[2]));
		}
		if (gap != null) {
           if (gap[0] < 1 || gap[0] >= motif.length) { System.err.println("Invalid gap position: "+gap[0]);        return; }
           if (gap[1] > gap[2])                      { System.err.println("Invalid gap size: "+gap[1]+"-"+gap[2]); return; }
        }

		// Background distribution
		SeqProfile bg;
		if (argBackground.isUsed()) {
			String[] values = argBackground.getValues();
		    try {
		    	bg = new SeqProfile(Double.parseDouble(values[0]),
		    			            Double.parseDouble(values[1]),
		    			            Double.parseDouble(values[2]),
		    			            Double.parseDouble(values[3]),
		    			            true);
		    }
		    catch (NumberFormatException e) {
		    	System.err.println("Invalid syntax of the background frequencies: "+Arrays.toString(argBackground.getValues()));
		    	return;
		    }
		} else {
		    if (argVerbose.isUsed()) { System.err.println("Counting nucleotide frequencies..."); }
			bg = SequenceUtil.nucleotideFrequencies(fasta);
			if (argVerbose.isUsed()) { System.err.println("- bg: "+bg); }
		}

        if (argVerbose.isUsed()) { System.err.println("Processing sequencies..."); }
        int     hitS     = 0;
        int     hitC     = 0;
		boolean showSeq  = argShowSeq.isUsed();
		boolean revStrd  = argReverse.isUsed();
        boolean relative = argRelativ.isUsed();
		for (int si=0; si<seqCount; si++) {
		    String       name    = fasta.getName(si);
		    byte[]       seq     = fasta.getSequence(si);
		    MotifMatch[] matches;

		    try { matches = findMotifs(seq, motif, gap, bg, pseudo, scoreLimit, revStrd, relative); }
		    catch (IllegalArgumentException e) {
		    	throw new IllegalArgumentException("Cannot process the sequence #"+si+" ("+name+')', e);
		    }

		    if (matches.length < 1)
		       continue;
		    hitS++;
		    System.out.print(name);
		    System.out.print('\t');
		    System.out.print(matches.length);
		    System.out.print('\t');
		    for (int m=0; m<matches.length; m++) {
		        hitC++;
		        if (showSeq) {
		        	System.out.print("\n\t"+(matches[m].forward ? 'f' : 'b')+matches[m].start+"-"+matches[m].end+"\t(");
		        	if (matches[m].forward) {
		        		System.out.write(seq, matches[m].start, matches[m].end-matches[m].start+1);
		        	} else {
		        		int    sI   = seq.length-matches[m].end-1;
		        		int    eI   = seq.length-matches[m].start;
		        		byte[] seqS = SequenceUtil.takeReverseComplementCopy(seq, sI, eI-sI);
		        		System.out.write(seqS, 0, seqS.length);
		        	}
		        	System.out.print(")\t");
		        	System.out.print(matches[m].score);
		        } else {
			        if (m > 0)
				        System.out.print(',');
			        if (revStrd)
			            System.out.print(matches[m].forward ? 'f' : 'b');
				    System.out.print(matches[m].start+"-"+matches[m].end);
		        }
		    }
		    System.out.println();
		}

		if (argVerbose.isUsed()) {
		   System.err.println("Total of "+hitS+" sequences matched the motif "+hitC+" times.");
		   System.err.println("Done!");
		}
	}

    /**
     * Print a list of command line arguments to the standard error stream.
     */
    static private void printSynopsis(Map<String, MainArgument> args) {
        System.err.println("MotifMatch v."+Version.getVersion()+" by Marko Laakso");
        System.err.println("---------------------------------");
        System.err.println("MotifMatch [options]");
        MainArgument.printArguments(args, System.err);
    }

}
