package org.anduril.asser.sequence;

import static org.anduril.asser.sequence.Nucleotide.*;

/**
 * This enumeration represents single nucleotide polymorphism (SNP) calls.
 */
public enum SNPCall {

	/** Measured but failed SNP */
    FAILED(null, null, 0x7E),
	AA(A,A,0x00), AC(A,C,0x01), AG(A,G,0x02), AT(A,T,0x03), /** Null allele */ AN(A,    null,0x04),
                  CC(C,C,0x05),	CG(C,G,0x06), CT(C,T,0x07), /** Null allele */ CN(C,    null,0x08),
        	                    GG(G,G,0x09), GT(G,T,0x0A), /** Null allele */ GN(G,    null,0x0B),
	        	                              TT(T,T,0x0C), /** Null allele */ TN(T,    null,0x0D),
	                                             /** Homozygous null allele */ NN(null, null,0x0E);

    /** Representation of markers that with no measurements */
    static public final String SYMBOL_MISSING_VALUE       = "NA";
    /** Representation of measured but failed markers */
    static public final String SYMBOL_EXPERIEMENT_FAILURE = "ERR";

    /** Nucleotides of both chromosomes */
    private final Nucleotide[] nucs;

    /** Binary code for the serialization */
    private final byte code;

    /** Textual representation of the nucleotide calls */
    private final String label;

    private SNPCall(Nucleotide n1, Nucleotide n2, int c) {
    	nucs  = new Nucleotide[] { n1, n2 };
        code  = (byte)c;
        label = (n1==null?"-":n1.name())+(n2==null?"-":n2.name());
    }

    /**
     * Tells if the SNP is homozygous.
     */
	public boolean isHomozygous() {
		return (this == AA || this == CC || this == GG || this == TT);
	}

	public Nucleotide[] getNucleotides() {
		return nucs;
	}

    /**
     * Get a permanent binary code for the SNPCall.
     */
    static public byte getCode(SNPCall call) {
        if (call == null) {
            return 0x7F;
        }
        return call.code;
    }

    /**
     * Decode the given binary code to its original SNPCall.
     */
    static public SNPCall parse(byte code) {
        switch (code) {
          case 0x00: return AA;
          case 0x01: return AC;
          case 0x02: return AG;
          case 0x03: return AT;
          case 0x04: return AN;
          case 0x05: return CC;
          case 0x06: return CG;
          case 0x07: return CT;
          case 0x08: return CN;
          case 0x09: return GG;
          case 0x0A: return GT;
          case 0x0B: return GN;
          case 0x0C: return TT;
          case 0x0D: return TN;
          case 0x0E: return NN;
          case 0x7E: return FAILED;
          case 0x7F: return null;
          default: throw new IllegalArgumentException("Unknown byte code: "+code);
        }
    }

	static public SNPCall parse(String name) {
		return parse(name, SYMBOL_MISSING_VALUE, SYMBOL_EXPERIEMENT_FAILURE);
	}

	static public SNPCall parse(String name,
			                    String na,
			                    String err) {
		char       c1, c2;
		Nucleotide a, b;
		int        l = name.length();

		if (name.equals(na))  return null;
		if (name.equals(err)) return FAILED;
		if (l == 2) {
	        c1 = name.charAt(0);
	        c2 = name.charAt(1);
		} else
	    if (l == 3) {
	        c1 = name.charAt(0);
	        c2 = name.charAt(2);
	    } else
			throw new IllegalArgumentException("Invalid length of the SNP call: "+name.length());
		a = Nucleotide.toNucleotide(c1);
		b = Nucleotide.toNucleotide(c2);
		return toSNPCall(a, b);
	}

	static public String toString(SNPCall call) {
		return toString(call, null, SYMBOL_MISSING_VALUE, SYMBOL_EXPERIEMENT_FAILURE);
	}

	static public String toString(SNPCall call, String separator) {
		return toString(call, separator, SYMBOL_MISSING_VALUE, SYMBOL_EXPERIEMENT_FAILURE);
	}

	static public String toString(SNPCall call, String separator, String NA, String ERR) {
		if (call == null)           return NA;
		if (call == SNPCall.FAILED) return ERR;
		if (separator == null) {
           return call.label;
        } else {
           Nucleotide[] n = call.getNucleotides();
		   return (n[0]==null?"-":n[0].name())+separator+(n[1]==null?"-":n[1].name());
        }
	}

	static public SNPCall toSNPCall(Nucleotide a, Nucleotide b) { // NOPMD
		final String ERR_NUCLEOTIDE = "Invalid nucleotide: ";

		if (b == null && a != null) {
			Nucleotide tmp = a;
			a = b;
			b = tmp;
		}
		if (a == null) {
			if (null         == b) return NN;
			if (Nucleotide.A == b) return AN;
			if (Nucleotide.C == b) return CN;
			if (Nucleotide.G == b) return GN;
			if (Nucleotide.T == b) return TN;
			throw new IllegalArgumentException(ERR_NUCLEOTIDE+b);
		}
		switch (a) { // NOPMD
  		  case A: switch (b) {
  		          case A: return AA;
  		          case C: return AC;
  		          case G: return AG;
  		          case T: return AT;
  		          default: throw new IllegalArgumentException(ERR_NUCLEOTIDE+b);
  		  }
  		  case C: switch (b) {
  		          case A: return AC;
  		          case C: return CC;
  		          case G: return CG;
  		          case T: return CT;
  		          default: throw new IllegalArgumentException(ERR_NUCLEOTIDE+b);
  		  }
  		  case G: switch (b) {
  		          case A: return AG;
  		          case C: return CG;
  		          case G: return GG;
  		          case T: return GT;
  		          default: throw new IllegalArgumentException(ERR_NUCLEOTIDE+b);
  		  }
  		  case T: switch (b) {
  		          case A: return AT;
  		          case C: return CT;
  		          case G: return GT;
  		          case T: return TT;
  		          default: throw new IllegalArgumentException(ERR_NUCLEOTIDE+b);
  		  }
  		  default: throw new IllegalArgumentException(ERR_NUCLEOTIDE+a);
		}
	}

}
