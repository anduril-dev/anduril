package org.anduril.asser.sequence;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link org.anduril.asser.sequence.MotifMatch}.
 * 
 * @author Marko Laakso
 */
public class MotifMatchTest {

	@Test
	public void testFindMotif() {
		byte[]       seq = "GGACACGTTTACGT".getBytes();
		int[]        gap = new int[] {2,2,2};
		MotifMatch[] hits;
		SeqProfile   bg    = new SeqProfile(0.25,0.25,0.25,0.25,false);
		SeqProfile[] motif = new SeqProfile[4];
		motif[0] = new SeqProfile(1,0,0,0,false);
		motif[1] = new SeqProfile(0,1,0,0,false);
		motif[2] = new SeqProfile(0,0,1,0,false);
		motif[3] = new SeqProfile(0,0,0,1,false);

		hits = MotifMatch.findMotifs(seq, motif, null, bg, 0.04, -8, false, false);
		assertEquals(seq.length-motif.length+1, hits.length);

		hits = MotifMatch.findMotifs(seq, motif, null, bg, 0.04, 0.5, false, false);
		assertEquals( 2, hits.length);
		assertEquals( 4, hits[0].getFirstLocus());
		assertEquals( 7, hits[0].getLastLocus());
		assertEquals(10, hits[1].getFirstLocus());
		assertEquals(13, hits[1].getLastLocus());

		// Full match ACGT
		hits = MotifMatch.findMotifs(seq, motif, gap, bg, 0.04, 3, false, false);
		assertEquals("ACnnGT", SeqProfile.toConcensus(motif, gap));
		assertEquals( 1, hits.length);
		assertEquals( 2, hits[0].getFirstLocus());
		assertEquals( 7, hits[0].getLastLocus());

		// Partial match of ACTT
		hits = MotifMatch.findMotifs(seq, motif, gap, bg, 0.04, 2, false, false);
		assertEquals( 2, hits.length);
		assertEquals( 4, hits[1].getFirstLocus());
		assertEquals( 9, hits[1].getLastLocus());
	}

	@Test
	public void testCountScore() {
		byte[]     seq   = "ACGTRYSWKMBDHVN".getBytes();
        double[][] motif = new double[][] {
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 },
        };

        // A
        assertEquals(1.0, MotifMatch.countScore(seq,  0, 0, 0, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq, -1, 1, 1, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq, -2, 2, 2, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq, -3, 3, 3, motif), 0.00001);

        // C
        assertEquals(0.0, MotifMatch.countScore(seq,  1, 0, 0, motif), 0.00001);
        assertEquals(1.0, MotifMatch.countScore(seq,  0, 1, 1, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq, -1, 2, 2, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq, -2, 3, 3, motif), 0.00001);

        // G
        assertEquals(0.0, MotifMatch.countScore(seq,  2, 0, 0, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  1, 1, 1, motif), 0.00001);
        assertEquals(1.0, MotifMatch.countScore(seq,  0, 2, 2, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq, -1, 3, 3, motif), 0.00001);

        // T
        assertEquals(0.0, MotifMatch.countScore(seq,  3, 0, 0, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  2, 1, 1, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  1, 2, 2, motif), 0.00001);
        assertEquals(1.0, MotifMatch.countScore(seq,  0, 3, 3, motif), 0.00001);

        // R (=A|G)
        assertEquals(0.5, MotifMatch.countScore(seq,  4, 0, 0, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  3, 1, 1, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  2, 2, 2, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  1, 3, 3, motif), 0.00001);
        
        // Y (=C|T)
        assertEquals(0.0, MotifMatch.countScore(seq,  5, 0, 0, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  4, 1, 1, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  3, 2, 2, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  2, 3, 3, motif), 0.00001);

        // S (=C|G)
        assertEquals(0.0, MotifMatch.countScore(seq,  6, 0, 0, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  5, 1, 1, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  4, 2, 2, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  3, 3, 3, motif), 0.00001);

        // W (=A|T)
        assertEquals(0.5, MotifMatch.countScore(seq,  7, 0, 0, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  6, 1, 1, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  5, 2, 2, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  4, 3, 3, motif), 0.00001);

        // K (=G|T)
        assertEquals(0.0, MotifMatch.countScore(seq,  8, 0, 0, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  7, 1, 1, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  6, 2, 2, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  5, 3, 3, motif), 0.00001);

        // M (=A|C)
        assertEquals(0.5, MotifMatch.countScore(seq,  9, 0, 0, motif), 0.00001);
        assertEquals(0.5, MotifMatch.countScore(seq,  8, 1, 1, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  7, 2, 2, motif), 0.00001);
        assertEquals(0.0, MotifMatch.countScore(seq,  6, 3, 3, motif), 0.00001);
        
        // B (=C|G|T)
        assertEquals(0.0, MotifMatch.countScore(seq, 10, 0, 0, motif), 0.00001);
        assertEquals(0.3, MotifMatch.countScore(seq,  9, 1, 1, motif), 0.05);
        assertEquals(0.3, MotifMatch.countScore(seq,  8, 2, 2, motif), 0.05);
        assertEquals(0.3, MotifMatch.countScore(seq,  7, 3, 3, motif), 0.05);

        // D (=A|G|T)
        assertEquals(0.3, MotifMatch.countScore(seq, 11, 0, 0, motif), 0.05);
        assertEquals(0.0, MotifMatch.countScore(seq, 10, 1, 1, motif), 0.00001);
        assertEquals(0.3, MotifMatch.countScore(seq,  9, 2, 2, motif), 0.05);
        assertEquals(0.3, MotifMatch.countScore(seq,  8, 3, 3, motif), 0.05);

        // H (=A|C|T)
        assertEquals(0.3, MotifMatch.countScore(seq, 12, 0, 0, motif), 0.05);
        assertEquals(0.3, MotifMatch.countScore(seq, 11, 1, 1, motif), 0.05);
        assertEquals(0.0, MotifMatch.countScore(seq, 10, 2, 2, motif), 0.00001);
        assertEquals(0.3, MotifMatch.countScore(seq,  9, 3, 3, motif), 0.05);

        // V (=A|C|G)
        assertEquals(0.3, MotifMatch.countScore(seq, 13, 0, 0, motif), 0.05);
        assertEquals(0.3, MotifMatch.countScore(seq, 12, 1, 1, motif), 0.05);
        assertEquals(0.3, MotifMatch.countScore(seq, 11, 2, 2, motif), 0.05);
        assertEquals(0.0, MotifMatch.countScore(seq, 10, 3, 3, motif), 0.00001);
	}

    @Test
    public void testRelativeScore() {
        double[][] matrix = new double[][] {
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 },
                { 0.2, 0.2, 0.2, 0.3 }
        };
        assertEquals(4.30, MotifMatch.relativeScore(matrix, 1.0), 0.00001);
        assertEquals(0.20, MotifMatch.relativeScore(matrix, 0.0), 0.00001);
        assertEquals(2.25, MotifMatch.relativeScore(matrix, 0.5), 0.00001);
    }

    @Test
    public void testRelativeScoreWithGaps() {
        double[][] matrix = new double[][] {
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                null,
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 },
                { 0.2, 0.2, 0.2, 0.3 }
        };
        assertEquals(4.30, MotifMatch.relativeScore(matrix, 1.0), 0.00001);
        assertEquals(0.20, MotifMatch.relativeScore(matrix, 0.0), 0.00001);
        assertEquals(2.25, MotifMatch.relativeScore(matrix, 0.5), 0.00001);
    }

}
