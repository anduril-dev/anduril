package org.anduril.asser.sequence;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Represents a nucleotide distribution for a sequence loci.
 */
public class SeqProfile {

	/** File symbol for missing frequencies **/
	static public final String GAP_SYMBOL = "Gap";

	// Nucleotide distribution (probabilities of each base) 
	public final double pA;
	public final double pC;
	public final double pG;
	public final double pT;

	public SeqProfile(double  a,
			          double  c,
			          double  g,
			          double  t,
			          boolean scale) {
		double sum = a+c+g+t;
		if (scale) {
			sum = 1/sum;
		} else {
			if (Math.abs(1-sum) > 0.00001)
				throw new IllegalArgumentException("The sum of probabilities is "+sum+".");
		}
		pA = a*sum;
		pC = c*sum;
		pG = g*sum;
		pT = t*sum;
	}

	public SeqProfile(Nucleotide nuc) {
		pA = (nuc == Nucleotide.A) ? 1 : 0;
		pC = (nuc == Nucleotide.C) ? 1 : 0;
		pG = (nuc == Nucleotide.G) ? 1 : 0;
		pT = (nuc == Nucleotide.T) ? 1 : 0;
	}

	@Override
	public String toString() {
		return "A="+pA+" C="+pC+" G="+pG+" T="+pT;
	}

	/**
	 * Generates a motif sequence where each loci has a certain nucleotide.
	 */
	static public SeqProfile[] toMotif(String sequence) {
		int          length = sequence.length();
		SeqProfile[] motif  = new SeqProfile[length];

		for (int i=0; i<length; i++) {
			motif[i] = new SeqProfile(Nucleotide.toNucleotide(sequence.charAt(i)));
		}
		return motif;
	}

	/**
	 * Generates a nucleotide sequence where each loci is occupied by the
	 * most probable base.
	 */
	static public String toConcensus(SeqProfile[] profile) {
		return toConcensus(profile, null);
	}

	/**
	 * Generates a nucleotide sequence where each loci is occupied by the
	 * most probable base. The sequence may contain a gap that is denoted by
	 * n-characters or like [1-3n] if the length of the gap may vary.
	 *
	 * @param gap {start index, min length, max length} 
	 */
	static public String toConcensus(SeqProfile[] profile,
			                         int[]        gap) {
		byte[] gRep = null;
		int    d    = 0;
		if (gap != null) {
			if (gap[1] == gap[2]) {
			   gRep = new byte[gap[1]];
			   Arrays.fill(gRep, (byte)'n');
			} else {
			   gRep = ("["+gap[1]+'-'+gap[2]+"n]").getBytes();
			}
			d = gRep.length;
		}

		byte[] seq  = new byte[profile.length + d];
		d = 0;
		for (int i=0; i<profile.length; i++) {
			if ((gap != null) && (gap[0]==i)) {
				System.arraycopy(gRep, 0, seq, i, gRep.length);
				d = gRep.length;
			}
			if (profile[i] == null) {
				seq[i+d] = 'n';
				continue;
			}
			double p = profile[i].pA;
			seq[i+d] = 'A';
			if (profile[i].pC > p) { p = profile[i].pC; seq[i+d] = 'C'; }
			if (profile[i].pG > p) { p = profile[i].pG; seq[i+d] = 'G'; }
			if (profile[i].pT > p) { p = profile[i].pT; seq[i+d] = 'T'; }
		}
		return new String(seq);
	}

    /**
     * Stores the given motif sequence into a text file.
     *
     * @param  motif       Probability matrix to be written
     * @param  file        Output file
     * @throws IOException If the writing fails
     */
    static public void writeMotif(SeqProfile[] motif,
                                  File         file) throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(file));
        
        out.println("A\tC\tG\tT");
        for (SeqProfile locus : motif) {
            out.print  (locus.pA); out.append('\t');
            out.print  (locus.pC); out.append('\t');
            out.print  (locus.pG); out.append('\t');
            out.println(locus.pT);
        }
        out.close();
    }

    /**
     * Reads a sequence of nucleotide distributions from the given file.
     * The gaps are represented as nulls and the length of the resulting
     * motif will match the number of loci defined within the file.
     */
    static public SeqProfile[] readMotif(String filename) throws IOException {
        return readMotif(new File(filename));
    }

    /**
     * Reads a sequence of nucleotide distributions from the given file.
     * The gaps are represented as nulls and the length of the resulting
     * motif will match the number of loci defined within the file.
     */
	static public SeqProfile[] readMotif(File file) throws IOException {
		Scanner          in          = new Scanner(file);
		List<SeqProfile> nucleotides = new LinkedList<SeqProfile>();
		
		if (!"A".equals(in.next()) ||
			!"C".equals(in.next()) ||
			!"G".equals(in.next()) ||
		    !"T".equals(in.next())) {
			in.close();
			throw new IOException("Invalid file format.");
		}
		while (in.hasNext()) {
			try {
				String tmpA = in.next();
				if (GAP_SYMBOL.equalsIgnoreCase(tmpA))
					nucleotides.add(null);
				else
					nucleotides.add(new SeqProfile(
							Double.parseDouble(tmpA),
							Double.parseDouble(in.next()),
							Double.parseDouble(in.next()),
							Double.parseDouble(in.next()),
							true
					));
			}
			catch (NumberFormatException e) {
				in.close();
				throw new IOException("Invalid probability value: "+e.getMessage()); // NOPMD
			}
		}
		
		in.close();
		return nucleotides.toArray(new SeqProfile[nucleotides.size()]);
	}

}
