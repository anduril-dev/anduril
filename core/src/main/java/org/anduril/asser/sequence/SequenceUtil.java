package org.anduril.asser.sequence;

import org.anduril.asser.io.FastaFile;

import java.util.LinkedList;
import java.util.List;

/**
 * Miscellaneous routines for sequence manipulation.
 */
public final class SequenceUtil {

	private SequenceUtil() {}

	static public byte[] takeReverseComplementCopy(byte[] seq, int start, int length) {
		byte[] copy = new byte[length];
		for (int i=0; i<length; i++) {
			copy[copy.length-i-1] = seq[start+i];
		}
		complement(copy);
		return copy;
	}

	static public String reverse(String seq) {
		byte[] res = seq.getBytes();
		reverse(res);
		return new String(res);
	}

	static public void reverse(byte[] seq) {
		for (int i=0; i<seq.length/2; i++) {
			int  j   = seq.length-i-1;
			byte tmp = seq[i];
			seq[i]   = seq[j];
			seq[j]   = tmp;
		}
	}

    static public List<int[]> findRepeats(byte[]  seq,
    			 					      int     start, // NOPMD
    		                              int     end,   // NOPMD
    		                              int     limit) {
    	if (start > end) {
    		//throw new IllegalArgumentException(start+">"+end);
    		int tmp = start;
    		end     = start;
    		start   = tmp;
    	}

    	byte        nuc     = '-';
    	int         length  = 0;
    	List<int[]> regions = new LinkedList<int[]>();

    	for (int i=start; i<end; i++) {
    		if (seq[i] == nuc) {
    			length++;
    		} else {
        		if (length >= limit) {
        			regions.add(new int[] {i-length, i-1});
        		}
    			nuc    = seq[i];
    			length = 1;
    		}
    	}
		if (length >= limit) {
			regions.add(new int[] {end-length, end});
		}
		return regions;
    }

    /**
     * Returns the starting position of the first match of the given subsequence
     * or -1 if the sequence does not include the key.
     */
    static public int indexOf(byte[] sequence, byte[] key) {
    	in_loop:
    	for (int i=0; i<sequence.length-key.length; i++) {
    		for (int j=0; j<key.length; j++) {
    			if (sequence[i+j] != key[j])
    				continue in_loop;
    		}
    	    return i;
    	}
    	return -1;
    }

    /**
     * Finds the next stop codon from the given sequence.
     *
     * @param start    Beginning index for the search
     * @param sequence Nucleotide sequence
     * @param ori      Orientation of the strand (is forward)
     * @return         Index of the first base of the stop codon or -1 if no codon is found.
     */
    static public int stopCodon(int     start,
    		                    byte[]  sequence,
    		                    boolean ori) {
    	int limit;
    	int i;

    	if (ori) {
    		limit = (sequence.length-start)%3;
    	    limit = sequence.length - (limit==0 ? 0 : 3-limit);
    	} else {
    		limit = start%3;
    	}

    	for (i=start; ori ? i<limit : i>=0; i+=(ori ? 3 : -3)) {
    		if (ori) {
    			if (sequence[i]=='T' && (
        			(sequence[i+1]=='A' && (sequence[i+2]=='A'   ||
        					                sequence[i+2]=='G')) ||
        			(sequence[i+1]=='G' &&  sequence[i+2]=='A')))
    				return i;
    		} else {
    			if (((sequence[i]=='T' && (sequence[i+1]=='T'   ||
    				                       sequence[i+1]=='C')) ||
                   	 (sequence[i]=='C' &&  sequence[i+1]=='T'))  &&
                   	sequence[i+2]=='A')
        			return i;
    		}
    	}
    	return -1;
    }

    /**
     * Replaces all nucleotides with their counterparts on the other strand.
     */
    static public String complement(String seq) {
		byte[] res = seq.getBytes();
		complement(res);
		return new String(res);
    }
 
    /**
     * Replaces all nucleotides with their counterparts on the other strand.
     */
	static public void complement(byte[] seq) {
		for (int i=0; i<seq.length; i++) {
			switch (seq[i]) {
				case 'A': seq[i]='T'; break;
				case 'T': seq[i]='A'; break;
				case 'G': seq[i]='C'; break;
				case 'C': seq[i]='G'; break;
				case 'R': seq[i]='Y'; break;
				case 'Y': seq[i]='R'; break;
				case 'S': seq[i]='S'; break;
				case 'W': seq[i]='W'; break;
				case 'K': seq[i]='M'; break;
				case 'M': seq[i]='K'; break;
				case 'B': seq[i]='V'; break;
				case 'D': seq[i]='H'; break;
				case 'H': seq[i]='D'; break;
				case 'V': seq[i]='B'; break;
				case 'N': break; // Masked nucleotide
				default:  seq[i]='?';
			}
		}
	}

	/**
	 * Calculate relative frequencies of each nucleotide.
	 * This method will utilize all sequences of the given file.
	 */
	static public SeqProfile nucleotideFrequencies(FastaFile fasta) {
		int a = 0;
		int c = 0;
		int g = 0;
		int t = 0;

		for (int s=0; s<fasta.size(); s++) {
			for (byte b : fasta.getSequence(s)) {
				switch (b) {
				  case 'A': a++; break;
				  case 'C': c++; break;
				  case 'G': g++; break;
				  case 'T': t++; break;
				  default: continue;
				}
			}
		}
		return new SeqProfile(a, c, g, t, true);
	}

}
