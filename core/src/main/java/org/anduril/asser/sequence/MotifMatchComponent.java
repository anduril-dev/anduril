package org.anduril.asser.sequence;

import org.anduril.asser.AsserUtil;
import org.anduril.asser.io.CSVParser;
import org.anduril.asser.io.CSVWriter;
import org.anduril.asser.io.FastaFile;
import org.anduril.component.CommandFile;
import org.anduril.component.ErrorCode;
import org.anduril.component.SkeletonComponent;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * This component can be used to match a set of motifs against the given DNA sequencies.
 *
 * @author Marko Laakso
 * @since  {@link org.anduril.asser.Version Version} 1.75
 */
public class MotifMatchComponent extends SkeletonComponent {

    static public final String INPUT_MOTIFS     = "motifs";
    static public final String INPUT_SEQUENCE   = "sequence";
    static public final String INPUT_SELECTION  = "seqSelection";
    static public final String INPUT_BACKGROUND = "bgIn";
    static public final String INPUT_EXCLUDES   = "exclude";

    static public final String OUTPUT_HITS       = "hits";
    static public final String OUTPUT_FREQS      = "frequencies";
    static public final String OUTPUT_BACKGROUND = "bgOut";

    static public final String PARAM_SCORE          = "score";
    static public final String PARAM_RELATIVE       = "relativeScore";
    static public final String PARAM_REVERSE        = "reverse";
    static public final String PARAM_F_POS          = "forwardPos";
    static public final String PARAM_PSEUDO         = "pseudo";
    static public final String PARAM_BACKGROUND     = "background";
    static public final String PARAM_GAP            = "gap";
    static public final String PARAM_DROP_EXTENSION = "dropExt";

    protected ErrorCode runImpl(CommandFile cf) throws IOException {
        String       myName     = cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME);
        double       scoreLimit = cf.getDoubleParameter(PARAM_SCORE);
        double       pseudo     = cf.getDoubleParameter(PARAM_PSEUDO);
        boolean      useRelatS  = cf.getBooleanParameter(PARAM_RELATIVE);
        boolean      useRev     = cf.getBooleanParameter(PARAM_REVERSE);
        boolean      useFPos    = cf.getBooleanParameter(PARAM_F_POS);
        boolean      dropExt    = cf.getBooleanParameter(PARAM_DROP_EXTENSION);
        String       gapDef     = cf.getParameter(PARAM_GAP).trim();
        Set<String>  seqSelect  = loadIDs(cf.getInput(INPUT_SELECTION));
        File         motifs     = cf.getInput(INPUT_MOTIFS);
        File[]       mFiles     = motifs.listFiles();
        List<String> mNames     = new ArrayList<String>(mFiles.length);
        FastaFile    fasta      = FastaFile.read(cf.getInput(INPUT_SEQUENCE), seqSelect);
        int          seqCount   = fasta.size();
        String       bgS        = cf.getParameter(PARAM_BACKGROUND).trim();
        int[][]      freqs      = new int[mFiles.length][];
        int[]        gap        = null;
        Set<String>  excludes   = null;
        byte[]       seq;
        SeqProfile[] motif;
        SeqProfile   bg;
        MotifMatch[] matches;
        String       mName;

        if (gapDef.length() > 0) {
          StringTokenizer toks = new StringTokenizer(gapDef);
          try {
              gap = new int[] { Integer.parseInt(toks.nextToken()),
                                Integer.parseInt(toks.nextToken()),
                                Integer.parseInt(toks.nextToken()) };
          } catch (RuntimeException e) {
              cf.writeError("Invalid syntax of the motif gap: "+gapDef);
              return ErrorCode.PARAMETER_ERROR;
          }
        }

        if (cf.inputDefined(INPUT_EXCLUDES)) {
            excludes = loadIDs(cf.getInput(INPUT_EXCLUDES));
        }

        if (cf.inputDefined(INPUT_BACKGROUND)) {
            bg = loadBackground(cf.getInput(INPUT_BACKGROUND));
        } else if (bgS.length() > 1) {
            String[] bgTmp = bgS.split(",");
            if (bgTmp.length != 4)
                throw new IllegalArgumentException("Syntax error in background frequencies: "+bgS);
            bg = new SeqProfile(Double.parseDouble(bgTmp[0]),
                                Double.parseDouble(bgTmp[1]),
                                Double.parseDouble(bgTmp[2]),
                                Double.parseDouble(bgTmp[3]),
                                true);
        } else {
            bg = SequenceUtil.nucleotideFrequencies(fasta);
            cf.writeLog("Estimated background frequencies are: "+bg);
        }
        saveBackground(cf.getOutput(OUTPUT_BACKGROUND), bg, myName);

        CSVWriter hits = new CSVWriter(new String[] {"motif", "sequence", "strand", "start", "end", "match", "score"},
                                       cf.getOutput(OUTPUT_HITS),
                                       false);
        mNames.add("sequence");
        Arrays.sort(mFiles);
        for (int f=0; f<mFiles.length; f++) {
            File motifF = mFiles[f];
            mName = motifF.getName();
            if (motifF.isDirectory()) {
                cf.writeLog("Ignoring motif folder: "+mName);
                continue;
            }
            if (dropExt) {
                int extPos = mName.lastIndexOf('.');
                if (extPos > 0)
                   mName = mName.substring(0, extPos);
            }
            if ((excludes != null) && excludes.contains(mName)) {
                cf.writeLog("Skipping "+mName);
                continue;
            }
            cf.writeLog("Processing "+mName);
            mNames.add(mName);
            motif    = SeqProfile.readMotif(motifF);
            freqs[f] = new int[seqCount];
            for (int i=0; i<seqCount; i++) {
                seq = fasta.getSequence(i);
                if (seq.length < motif.length) continue;
                try { matches = MotifMatch.findMotifs(seq, motif, gap, bg, pseudo, scoreLimit, useRev, useRelatS); }
                catch (IllegalArgumentException e) {
                	cf.writeError("Cannot process sequence #"+i+" ("+fasta.getName(i)+"): "+e.getMessage());
                	hits.close();
                	System.out.println(new String(seq));
                	return ErrorCode.INVALID_INPUT;
                }
                freqs[f][i] = matches.length;
                for (MotifMatch hit : matches) {
                    int    start = hit.getFirstLocus();
                    int    end   = hit.getLastLocus();
                    int    strand;
                    String mSeq;
                    if (hit.isOriginalStrand()) {
                        strand = 1;
                        mSeq = new String(seq, start, end-start+1);
                    } else {
                        strand = -1;
                        int sI = seq.length-end-1;
                        int eI = seq.length-start;
                        mSeq = new String(SequenceUtil.takeReverseComplementCopy(seq, sI, eI-sI));
                        if (useFPos) {
                            start = sI;
                            end   = eI;
                        }
                    }
                    hits.write(mName);
                    hits.write(fasta.getName(i));
                    hits.write(strand);
                    hits.write(start);
                    hits.write(end);
                    hits.write(mSeq);
                    hits.write(AsserUtil.round(hit.getScore(), 3));
                }
            }
        }
        hits.close();

        CSVWriter freqOut = new CSVWriter(mNames.toArray(new String[mNames.size()]),
                                          cf.getOutput(OUTPUT_FREQS),
                                          false);
        for (int s=0; s<seqCount; s++) {
            freqOut.write(fasta.getName(s));
            for (int m=0; m<freqs.length; m++) {
                if (freqs[m] != null) {
                    freqOut.write(freqs[m][s]);
                }
            }
        }
        freqOut.close();

        return ErrorCode.OK;
    }

    static private SeqProfile loadBackground(File file) throws IOException {
        File[]       motifs = file.listFiles();
        SeqProfile[] bg     = null;

        for (File m : motifs) {
            if (m.isDirectory())
                continue;
            if (bg != null)
                throw new IllegalStateException("There are more than one motifs in: "+file.getAbsolutePath());
            bg = SeqProfile.readMotif(m);
            if (bg.length != 1)
                throw new IllegalArgumentException("Expected one instead of "+
                                                   bg.length+
                                                   " nucleotides in "+
                                                   m.getAbsolutePath()+".");
        }
        if (bg == null)
            throw new IllegalStateException("No motifs in: "+file.getAbsolutePath());
        return bg[0];
    }

    static private void saveBackground(File       dir,
                                       SeqProfile bg,
                                       String     name) throws IOException {
        dir.mkdirs();

        File file = new File(dir, name+"Bg.motif");
        SeqProfile.writeMotif(new SeqProfile[] { bg }, file);
    }

    static private Set<String> loadIDs(File file) throws IOException {
        if (file == null)
            return null;
        Set<String> ids = new HashSet<String>();
        CSVParser   in  = new CSVParser(file);
        while (in.hasNext()) {
            ids.add(in.next()[0]);
        }
        return ids;
    }

    /**
     * @param argv Pipeline arguments
     */
    public static void main(String[] argv) {
        new MotifMatchComponent().run(argv);
    }

}
