/*
 Anduril microarray documentation search.
 Author: ville.rantanen@helsinki.fi 2011/02
*/

function onLoad() {
/*
 Moves the cursor focus to the search box.
 Makes a predefined search if found.
*/
    searchExit();
    searchFocus();
    predefined_search();
}

function jumpToResult(event) {
    var key=event.keyCode || event.which;
    if (key==13){
        if (document.getElementById('searchBox').value.length>1) {
            var link=doSearch();
            if (link.length>0) {
                searchExit();
                top.frames['AndurilMain'].location.href=link;
                top.frames['AndurilMain'].focus();
            }
        }
    }   
}

function searchFocus() {
    /*
      Focuses on the search box, and selects the text
    */
    top.frames['AndurilNav'].focus();
    var id=document.getElementById('searchBox');
    if (!id.focused) {
        id.focus();
        id.select();
    }
    id.focused=true;
}

function searchEnter() {
/*
 Starts the search if string length exceeds 1. Also clears up the resultBox
 at any input.
*/
    fold_all();
    if (document.getElementById('searchBox').value.length>1) {
        doSearch();
    }
    else {
        document.getElementById('resultBox').innerHTML = "";
    }
}
function searchExit() {
/*
 Marks the element being blurred.
*/
    document.getElementById('searchBox').focused=false;
}
function doSearch(nth) {
/*
  Fetch link by looping over list types, names and search strings separated by space.
  Both strings must match (OR).
  Writes the resulting link string in the resultBox.
*/
    if (document.getElementById('searchBox').value.length<1) { return ""; }
    var resultString='';
    var needleArray=document.getElementById('searchBox').value.toLowerCase().split(" ");
    var found=true;
    var result=[];
    var resultlink=[];
    var newstr='';
    var descstart=0; // index where the description results begin
    if (!nth) { nth=0; }
    /* Find the needle from component/type/category names only */
    for (typekey in searchArray) {
        for (namekey in searchArray[typekey]) {
            found=true;
            for (needle in needleArray) {
                if (needleArray[needle].length>1) {
                    found=found && (namekey.toLowerCase().indexOf(needleArray[needle]) != -1 );
                }
            }
            if (found) {
                result.push(result_string(typekey,namekey,result.length));
                resultlink.push(result_URL(typekey,namekey));
            }
        }
    }
    if (result.length>0) { resultString+='<div class="result-head">Names:</div>'; }
    descstart=result.length;
    /* Find the needle from the descriptions and add them to result if not found already.
       Descriptions should be lower case already. */
    for (typekey in searchArray) {
        for (namekey in searchArray[typekey]) {
            found=true;
            for (needle in needleArray) {
                if (needleArray[needle].length>1) {
                    found=found && (searchArray[typekey][namekey].indexOf(needleArray[needle]) != -1 );
                }
            }
            if (found) {
                newstr = result_string(typekey,namekey,result.length);
                newurl = result_URL(typekey,namekey)
                /* Check the result isn't already found */
                for (row in resultlink) {
                    if (newurl==resultlink[row]) { found=false; }
                }
                if (found) {
                    result.push(newstr);
                    resultlink.push(newurl);
                }
            }
        }
    }
    for (row in result) {
        if (row==descstart) { resultString+='<div class="result-head">Descriptions:</div>'; }
        resultString+=result[row];
    }
    if (resultString.length==0) {
        resultString='<div class="result-item">No search results.</div>';
    }
    document.getElementById('resultBox').innerHTML = resultString;
    if (result.length<nth+1) { return }
    return resultlink[nth];
}
function component_link(component) {
/*
  Return link of exact component name 
*/
    for (name in searchArray['components']) {
        if (name.toLowerCase()==component.toLowerCase()) {
            return result_URL('components',name)
        }
    }
    return ""
}
function unfold(type) {
/*
  Writes a list type e.g. component list in to resultBox
  Changes bullet between + and -
*/
    var foldString='';
    var unfold=true;
    if (document.getElementById(type+'-fold').innerHTML=='-') {
        unfold=false;
    }
    document.getElementById('resultBox').innerHTML = foldString;
    fold_all();
    if (unfold) {
        for (namekey in searchArray[type]) {
            foldString+=result_string(type,namekey,99);
        }
    document.getElementById('resultBox').innerHTML = foldString;
    document.getElementById(type+'-fold').innerHTML = '-';
    }
}
function fold_all() {
/*
  Changes all -:s to +:s in the menu.
*/
    for (typekey in searchArray) {
        document.getElementById(typekey+'-fold').innerHTML = '+';
    }    
}
function predefined_search() {
    /*
      Creates a predefined search based on URL?q=string
      Works only on http protocol, not on local files.
    */
    if (location.protocol=='file:') {
        return;
    }
    var query;
    if (top.location.search != "") {   
        query = top.location.search.substr(1).split("=");
        if ((query[0]=="q") & query.length>1 & query[1].length>1) {
            document.getElementById('searchBox').value=unescape(query[1]);
            var link=doSearch();
            if (link.length>0) {
                top.frames['AndurilMain'].location.href=link;
                top.frames['AndurilMain'].focus();
            }
        }
        if ((query[0]=="c") & query.length>1 & query[1].length>1) {
            link=component_link(unescape(query[1]));
            if (link.length>0) {
                top.frames['AndurilMain'].location.href=link;
                top.frames['AndurilMain'].focus();
            }
        }
    }
}
function print_short_help() {
    var str='<div class="result-item"><li/>Type your search in the text box. <li/>Press "h" for more help'+
    '</div>';
    document.getElementById('resultBox').innerHTML = str+document.getElementById('resultBox').innerHTML;
}
function get_long_help() {
    /* Returns an object containing the long help text */
    var el = document.createElement('div'); 
    el.setAttribute("id", 'helpWindow');
    el.setAttribute("class", 'result-help');
    if (location.protocol=='file:') { 
        var str='<a href="javascript:void(0)" onclick="show_help();">Close</a>';
    } else {
        var str='<a href="javascript:void(0)" onclick="destroyParent(this)">Close</a>';
    }
    str+='<ul><li/>Type your search in the text box. '+
            '<li/>Your query will be searched in component names and descriptions. '+
            '<li/>Pressing enter takes you to the first result. '+
            '<li/>All results are links to corresponding pages.'+
            '<li/>Keyboard shortcuts:'+
            '<ul><li/>Numbers [1-9] show a numbered result page.'+
            '<li/>[s] takes you back to the search field.'+
            '<li/>[e] shows the example usage (copy with ctrl-c).'+
            '<li/>[t] expand test case parameters.'+
            '<li/>[h] shows this help.'+
            '</ul>'+
            '<li/>A predefined search encoded in the URL: '+
                'http://hostname/path/to/index.html?q=[KeyWord]'+
                '<ul><li/>The search will take you to the first match automatically.'+
                '<li/>Works only if using web server (URL starts with http://)';
    if (location.protocol!='file:') { 
        str+='<li/>Example: <a href="'+top.location.origin+top.location.pathname+'?q=csv" target="_BLANK">'+
                top.location.origin+top.location.pathname+'?q=csv</a>';
    } else {
        el.style.top="0px";
        el.style.left="0px";
    }
    str+='</ul><li/>Refer to component name directly: '+
                'http://hostname/path/to/index.html?c=[bundle.component]';
    if (location.protocol!='file:') { 
        str+='<li/>Example: <a href="'+top.location.origin+top.location.pathname+'?c=builtin.INPUT" target="_BLANK">'+
                top.location.origin+top.location.pathname+'?c=builtin.INPUT</a>';
    }
    str+='</ul></ul>';
    el.innerHTML=str;
    return el
}
function result_string(typekey,namekey,nId) {
    /* Returns a HTML string that will be added to the result list */
    if (nId >= 0 && nId< 9) { var nText='<span class="small">('+String(nId+1)+')</span>&nbsp;'; } else { var nText=""; }
    var nameStr=namekey;
    if (typekey=='components' && namekey.indexOf(".")>-1) {
        var nameSplit=namekey.split(".",2);
        nameStr='['+nameSplit[0].charAt(0).toUpperCase()+'] '+
            nameSplit[1];
    }
    return '<div class="result-item" title="'+typekey+':'+namekey+'">'+
        '<img class="result-icon" src="images/'+imagemap[typekey]+'"/>'+nText+
        '<a href="'+result_URL(typekey,namekey)+'" target="AndurilMain">'+nameStr+'</a></div>\n';
}
function result_URL(typekey,namekey) {
    /* Returns a URL string of the search result */
    if (typekey=='components' && namekey.indexOf(".")>-1) {
        namekey=namekey.split(".")[1];
    }
    return typekey+'/'+namekey+'/index.html';
}
function show_help() {
    /* Creates a DIV in the body containing the long help text */ 
    if (document.getElementById('searchBox').focused) { return }
    if (location.protocol=='file:') { 
        if (document.getElementById('helpWindow')) { 
            var el=document.getElementById('helpWindow')
            el.parentNode.removeChild(el);
        } else {
            el=get_long_help();
            document.body.appendChild(el);
            el.scrollIntoView(true);
        }
        return
    }
    if (top.frames['AndurilMain'].document.getElementById('helpWindow')) {
        // erase help window if exists 
        var el=top.frames['AndurilMain'].document.getElementById('helpWindow')
        el.parentNode.removeChild(el);
    } else {
        // create help window if does not exist
        el=get_long_help();
        top.frames['AndurilMain'].document.body.appendChild(el);
        el.scrollIntoView(true);
    }
    return
}

