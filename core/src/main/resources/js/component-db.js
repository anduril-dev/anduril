

#macro (componentDB $components)
#foreach ($comp in $components)
'$comp.bundle.$comp.name':'#if ($comp.doc) $comp.doc.toLowerCase().replaceAll("\r\n","").replaceAll("\r|\n","").replaceAll("\s{2,}"," ").replaceAll("\'","\\'").replaceAll("<.*?>","").trim() #else  #end',
#end
#end

searchArray['components'] = {
#componentDB($components)
#componentDB($functions)
};

