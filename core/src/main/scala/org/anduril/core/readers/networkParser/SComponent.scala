package org.anduril.core.readers.networkParser

import org.anduril.core.engine.NetworkEvaluator
import org.anduril.core.network._

object NetworkHandler {
    var networkInstance : Network = _
    var repository : Repository = _

    def getRepository() = repository

    def setRepository(aRepository: Repository) = {
      repository = aRepository
    }

//    def getComponent(name: String): Component = {
//      repository.getComponent(name)
//    }

    def setNetwork(network : Network) = {
   	  networkInstance = network
    }

    def getNetwork() = {
      networkInstance
    }
}

object EvaluatorHandler {

  var networkEvaluator : org.anduril.core.engine.NetworkEvaluator = _

  def setNetworkEvaluator(neval: NetworkEvaluator) = networkEvaluator = neval

  def getNetworkEvaluator() = networkEvaluator
}