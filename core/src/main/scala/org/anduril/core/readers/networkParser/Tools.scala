package org.anduril.core.readers.networkParser

import org.anduril.core.network.{PresentationType, DataType}
import org.anduril.runtime.Component
import org.anduril.runtime.Port

object Tools {
    def invokeComponentByName(componentClass: String, args: scala.collection.Map[String, Any]): Component = {
        val cls = Class.forName(componentClass)
        // TODO: error handling
        val constructor = cls.getConstructor(Class.forName("scala.collection.Map"))
        val inst = constructor.newInstance(args)
        inst.asInstanceOf[Component]
    }

    def invokeComponentByNameJava(componentClass: String, args: java.util.Map[String, AnyVal]): Component = {
        import scala.collection.JavaConversions._
        val argsScala = scala.collection.mutable.Map[String, Any]()
        for (key <- args.keySet()) {
            argsScala(key) = args.get(key)
        }
        invokeComponentByName(componentClass, argsScala)
    }

    def getPortByName(component: Component, portName: String): Port = {
        val method = component.getClass.getMethod(portName)
        val result = method.invoke(component)
        result.asInstanceOf[Port]
    }

    def invokeINPUT(path: String): Port = {
        val args = Map("path" -> path)
        val comp = invokeComponentByName("anduril.builtin.INPUT", args)
        getPortByName(comp, "out")
    }

    def invokeOUTPUT(port: Port): Port = {
        val args = Map("in" -> port)
        val comp = invokeComponentByName("anduril.builtin.OUTPUT", args)
        getPortByName(comp, "out")
    }

    def addDataTypeToRepository(typeName: String, parentTypeName: String, presentationType: String, fileExtension: String): DataType = {
        val repository = NetworkHandler.getRepository()
        val existingType = repository.getDataType(typeName)
        if (existingType == null) {
            val parent = if (parentTypeName == null) {
                null
            } else {
                repository.getDataType(parentTypeName)
            }

            val dataType = new DataType(typeName, parent, null,
                PresentationType.valueOf(presentationType), null, fileExtension, null, false)
            NetworkHandler.getRepository().addDataType(dataType)
            dataType
        } else {
            existingType
        }
    }

}
