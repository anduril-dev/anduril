package org.anduril

import java.io.File

import org.anduril.core.readers.networkParser._
import org.anduril.core.utils.{Logger, Language}
import org.anduril.runtime.table.{Table, Row}

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.LinkedHashMap
import scala.collection.mutable.ListBuffer
import scala.language.implicitConversions
import scala.reflect.ClassTag

/**
  * Anduril runtime support package.
  */
package object runtime {

    val logger = Logger.getLogger("<runtime>")
    def msgToString(msg: Any) = if (msg!=null) msg.toString else "null"
    def info(msg: Any) = logger.info(msgToString(msg))
    def warning(msg: Any) = logger.warning(msgToString(msg))
    def error(msg: Any) = {
        logger.error(msgToString(msg))
        throw new Exception(msgToString(msg))
    }

    @deprecated("Use error(msg) instead", "2016-04-20")
    def severe(msg: Any) = logger.error(msg.toString)

    /**
      * Return the value of an environment variable. Raise an error
      * if the environment variable is not found and no default is
      * provided.
      *
      * @param envVar Name of the environment variable.
      * @param default Default value that is returned if the environment
      *                variable is not found. If this is null, then a
      *                missing variable is treated as an error.
      * @return Value of the environment variable.
      */
    def getEnv(envVar: String, default: String = null): String = {
        val result = sys.env.getOrElse(envVar, default)
        if (result == null) {
            sys.error(envVar + " not defined and no default was provided.")
        }
        result
    }

    /**
      * Return true if given environment variable exists, false otherwise.
      *
      * @param envVar Name of the environment variable.
      */
    def existsEnv(envVar: String): Boolean = {
        sys.env.contains(envVar)
    }

    /** Convert component with one out-port into port. */
    implicit def convertPort(c: Product1[Port]): Port = {
        if (c == null) null
        else c._1
    }

    /** Convert a string to a StringInput instance with the content
      * of the string.
      */
    implicit def stringToPort(content: String): Port = {
        val args = Map("content" -> content)
        val comp = Tools.invokeComponentByName("anduril.builtin.StringInput", args)
        Tools.getPortByName(comp, "out")
    }

    /**
      * Convert an iterable to an Anduril array using makeArray.
      */
    implicit def iterableToArray(iterable: Iterable[Any]): Port = {
        makeArray(iterable)
    }

    /**
      * Extract (key, port) pairs from an iterable that can contain:
      * (1) ports, (2) component instances with exactly one outport,
      * (3) key-value pairs, (4) other iterables (including maps),
      * for which recursion is used. For (1) and (2), resulting keys
      * are based on numeric index, and for (3), the key is used.
      */
    private def extractKeyPortPairs(ports: Iterable[Any]): Seq[(String, Port)] = {
        val keyPortList = ArrayBuffer[(String, Port)]()

        for ((value, index) <- ports.zipWithIndex) {
            if (value.isInstanceOf[Iterable[Any]]) {
                keyPortList.appendAll(extractKeyPortPairs(value.asInstanceOf[Iterable[Any]]))
            } else {
                val (key, port) = value match {
                    case port: Port =>
                        ((index+1).toString, port)
                    case component: Product1[_] =>
                        ((index+1).toString, component._1.asInstanceOf[Port])
                    case (k, v) =>
                        val port = v match {
                            case port: Port =>
                                port
                            case component: Product1[_] =>
                                component._1.asInstanceOf[Port]
                        }
                        (k.toString, port)
                    case invalid => throw new IllegalArgumentException("Can not construct array from: "+invalid)
                }
                port.component._keep = true
                keyPortList.append((key, port))
            }
        }

        keyPortList
    }

    /**
     * Construct an Anduril array from a sequence of ports, components
     * or name-value pairs.
     *
     * @param ports Sequence of (1) ports, (2), components with
     *              exactly one output port, or (3) key-value pairs,
     *              e.g., "key1" -> port.
     *              Arguments of different types can be mixed.
     * @return An array port.
     */
    def makeArray(ports: Any*): Port = {

        val keyPortList = extractKeyPortPairs(ports)

        val ARRAY_CONSTRUCTOR_PORTS = org.anduril.core.network.Port.ARRAY_CONSTRUCTOR_PORTS
        val ARRAY_COMBINER_PORTS = org.anduril.core.network.Port.ARRAY_COMBINER_PORTS
        val ARRAY_OUTPUT_PORT = "out"

        /* Group ports into arrays of fixed length. */
        val groupedPorts: Iterator[Seq[(String, Port)]] = keyPortList.grouped(ARRAY_CONSTRUCTOR_PORTS)

        val pendingArrays = ArrayBuffer[Component]()

        /* Create ArrayConstructor instances and add them to pendingArrays list. */
        for (portList <- groupedPorts) {
            val args = scala.collection.mutable.Map[String,Any]()
            for (((key, port), index) <- portList.zipWithIndex) {
                args("file" + (index+1)) = port
                args("key" + (index+1)) = key
            }
            val comp = Tools.invokeComponentByName("anduril.builtin.ArrayConstructor", args)
            pendingArrays.append(comp)
        }

        /* Create ArrayCombiner instances from pendingArray and add the new instances to the
         * cleared pendingArray list. */
        while (pendingArrays.size > 1) {
            val groupedArrays = pendingArrays.grouped(ARRAY_COMBINER_PORTS).toArray
            pendingArrays.clear()

            for (componentList <- groupedArrays) {
                val args = scala.collection.mutable.Map[String,Any]()
                for ((component, index) <- componentList.zipWithIndex) {
                    args("array" + (index+1)) = component(ARRAY_OUTPUT_PORT)
                }
                val comp = Tools.invokeComponentByName("anduril.builtin.ArrayCombiner", args)
                pendingArrays.append(comp)
            }
        }

        if (pendingArrays.isEmpty) {
            null
        } else {
            pendingArrays(0)(ARRAY_OUTPUT_PORT)
        }
    }

    /**
     * Convert a Scala mutable Map onto Anduril array by invoking makeArray.
      *
      * @param args Any scala.collection.mutable.Map .
     * @return An array port.
     */
    def map2Array(args: scala.collection.mutable.Map[_,_]): Port = makeArray(args.toSeq:_*)
    def map2Array(args: scala.collection.immutable.Map[_,_]): Port = makeArray(args.toSeq:_*)

    /**
     * Add language specific escape sequences.
     *
     * @param text A String to be reformatted.
     * @param targetLanguage A language onto which the text is specialised (escaped).
     *                       One of html, latex, scala, url.
     * @return A formatted String.
     */
    def quote(text: String, targetLanguage: String): String = Language.valueOf(targetLanguage.toLowerCase).quote(text)

    /**
      * Add language specific escape sequences.
      *
      * @param text A String to be reformatted.
      * @param targetLanguage A language onto which the text is specialised (escaped).
      *                       One of html, latex, scala, url.
      * @return A formatted String.
      */
    def quote(text: String): String = Language.valueOf("scala").quote(text)

    /**
     * A mutable.ListBuffer wrapper that infers names of inserted component instances
     * and provides stack functionality.     
     */
    @deprecated("Use NamedSeq instead", "2016-04-08")
    case class Chain[T]( var _name: String, values: T* ) extends Iterable[T] {

        val chain: ListBuffer[T] = new ListBuffer[T]()
        for (it <- values) +=(it)

        override def iterator = chain.iterator

        override def toString() = _name +": " + chain.mkString("<# ",", "," #>")

        override def size() = chain.size

        override def isEmpty = size() == 0

        override def nonEmpty = size() > 0

        def collect[U <: T : ClassTag] = chain.collect { case v: U => v }

        def apply[U <: T : ClassTag]() = collect[U]

        def apply(i: Int) = chain(i-1)

        def rename(name: String) = _name = name

        def contains(value: T) = chain.contains(value)

        def add(value: T) = +=(value)

        def append(value: T) = +=(value)

        def remove(value: T) = chain -= value

        def update(n: Int, value: T) = chain.update(n, value)

        def push(value: T) = chain.prepend(value)

        def pop() = chain.remove(0)

        def peek() = chain.head

        def +=(value: T) {
            value match {
                case v: Component =>
                    if ( v._componentInstance.isAutoGeneratedName() ) {
                        val ciName = v._componentInstance.getName()
                        v._componentInstance.rename(_name+"_"+ciName)
                        v._componentInstance.setAutoGeneratedName(false)
                    }
                case p: Port =>
                    if ( p.componentInstance.isAutoGeneratedName() ) {
                        p.componentInstance.rename(_name+"_"+p.name)
                        p.componentInstance.setAutoGeneratedName(false)
                    }
                case r: Record[_] =>
                    r.rename( _name + "_" + r._name )
                case c: Chain[_] =>
                    c.rename( _name + "_" + c._name )
                case _ =>
            }
            chain += value
        }

        def ++=(ch: Chain[T]) = for ( c <- ch ) chain += c
        def ++=(values: Iterable[T]) = for ( v <- values ) chain += v

        def print = println(this.toString)
    }

    /**
     * A mutable.LinkedHashMap wrapper that infers names of component instances inserted.
     */
    @deprecated("Use NamedMap instead", "2016-04-08")
    case class Record[T]( var _name: String, pairs: (String,T)* ) extends Iterable[(String,T)] {

        val rec = new LinkedHashMap[String,T]
        for ( (key,value) <- pairs) +=(key, value)

        override def iterator = rec.iterator

        override def toString() = s"Record ${_name}\n" + { for ((key,value) <- rec) yield s"  $key -> $value\n" }.mkString

        override def size() = rec.size

        override def isEmpty = size() == 0

        override def nonEmpty = size() > 0

        def collect[U: ClassTag] = rec.collect { case (key, value: U) => key -> value }

        def apply[U: ClassTag]() = collect[U]

        def update(key: String, value: T) = +=(key, value)

        def apply(key: String) = rec get key getOrElse sys.error(s"Key $key not found.")

        def rename(name: String) = _name = name

        def contains(key: String) = rec.contains(key)

        def add(key: String, value: T) = +=(key, value)

        def remove(key: String) = rec -= key

        def keys() = rec.keys

        def nKey(i: Int) = {
            val xs = keys().toSeq
            xs(i)
        }

        def values() = rec.values

        def nValue(i: Int) = {
            val xs = values().toSeq
            xs(i)
        }

        def +=(key: String, value: T) {
            value match {
                case v: Component =>
                    // Do not rename existing instance name
                    if ( v._componentInstance.isAutoGeneratedName() ) {
                        v._componentInstance.rename(_name+"_"+key)
                        v._componentInstance.setAutoGeneratedName(false)
                    }
                case p: Port =>
                    // Do not rename existing instance name
                    if ( p.componentInstance.isAutoGeneratedName() ) {
                        p.componentInstance.rename(_name+"_"+key)
                        p.componentInstance.setAutoGeneratedName(false)
                    }
                case r: Record[_] =>
                    r.rename(_name + "_" + key + "_" + r._name )
                case c: Chain[_] =>
                    c.rename( _name + "_" + key + "_" + c._name )
                case _ =>
            }
            rec += (key -> value)
        }

        def ++=(r: Record[T]) = for ( (key,value) <- r ) +=(key, value)
        def ++=(values: Iterable[(String,T)]) = for ( (key,value) <- values ) +=(key,value)

        def print = println(this.toString)

        // TODO: nested Records
        def toArray() = makeArray(rec.toSeq:_*)

        def rec2String(valueSep: String = "\t", itemSep: String = "\n", keys: Boolean = true, values: Boolean = true) = {
            val strRec = for { (key,value) <- rec
                               k = if (keys) key else ""
                               v = if (values) value.toString else ""
            } yield k + valueSep + v

            strRec.mkString(itemSep)
        }
    }

    /**
     * Convert Anduril array to Anduril Record.
      *
      * @param _name Name set to the resulting Record for name
                    prepending to component instances filenames.
     * @param array Anduril array port to be converted.
     * @return a Record.
     */
    @deprecated("Use NamedMap instead", "2016-04-08")
    def array2Rec(_name: String, array: Port) = {
        val pairs = arrayKeys(array).map ( k => (k, array(k)) )
        Record( _name, pairs.toSeq: _*)
    }
    
    /**
     * Convert Anduril Record to Anduril Array.
      *
      * @param rec An Anduril Record to be converted.
     * @return an Array Port.
     */
    @deprecated("Use NamedMap instead", "2016-04-08")
    def rec2Array[T](rec: Record[T]) = makeArray(rec.rec.toSeq:_*)

    @deprecated("Use NamedMap instead", "2016-04-08")
    def rec2Array[N <: Component](rec: Record[N], portName: String) = makeArray((for ((k,v) <- rec) yield (k,v(portName))).toSeq:_*)

    @deprecated("Use NamedSeq instead", "2016-04-08")
    def chain2Array[T](chain: Chain[T]) = makeArray(chain.chain.toSeq:_*)

    @deprecated("Use NamedSeq instead", "2016-04-08")
    def array2Chain(_name: String, array: Port): Chain[Any] = {
        val values = for ( (_, value) <- new ArrayIndex(array.contentArray) ) yield value
        Chain[Any]( _name, values.toSeq: _*)
    }

    /**
     * Extract keys from array.
      *
      * @param array An array.
     * @return A Chain[String] with array keys.
      */
    @deprecated("Use NamedSeq instead", "2016-04-08")
    def arrayKeys( array: Port ): Chain[String] = {
        val keys = for ( (key,file) <- new ArrayIndex(array.contentArray) ) yield key 
        Chain[String](array.name+"Keys", keys.toSeq: _*)
    }

    /** Iteration over a CSV file produced dynamically. */
    def iterCSV(port: Port): Iterable[Row] = {
        Table.reader(port.content, Table.FormatCSV)
    }

    /** Iteration over a static CSV file. */
    def iterCSV(file: File): Iterable[Row] = {
        Table.reader(file, Table.FormatCSV)
    }

    /** Iteration over a static CSV file. */
    def iterCSV(filename: String): Iterable[Row] = {
        iterCSV(new File(filename))
    }

    /* Iteration over files and folders in a given file or folder. */
    case class iterFolder( dir: File ) extends Traversable[(String,File)] {
      override def foreach[U](f: ((String,File)) => U): Unit = {
        if (dir.isFile) Map(dir.getName -> dir)
        else {
            val valid = dir.listFiles.filterNot(f => f.getName.startsWith(".") || f.getName.startsWith("_")).sorted
            valid.foreach(file => f ( (file.getName, file) ) )
        }
      }
    }

    object iterFolder {    
      def apply(port: Port): iterFolder = iterFolder( port.content )
      def apply(dirname: String): iterFolder = iterFolder( new File(dirname) )
    }

    /**
      * Iterate over a dynamic Anduril file array.
      */
    def iterArray(port: Port) = new ArrayIndex(port.contentArray())

    /**
      * Push a name to the explicit call stack. This affects how component
      * instances are named. If a component would get name "A-B" without
      * calling pushName, it will be named "X-A-B" after calling pushName("X").
      * @param name Identifier
      */
    def pushName(name: String): Unit = {
        Component.pushName(name)
    }

    /**
      * Pop the most recent name from the explicit call stack.
      * @return The most recent item.
      */
    def popName(): String = {
        Component.popName()
    }

    /**
      * Evaluate a code block in an environment that adds the provided name
      * to the names of created component instances. If a component
      * would get name "A-B" without withName, it will be named "X-A-B"
      * when evaluated inside withName("X").
      * @param name Name that is pushed to the explicit call stack for
      *             the duration of body evaluation.
      * @param body Code block.
      * @return The value returned by body.
      */
    @IgnoreName
    def withName[T](name: String)(body: => T): T = {
        pushName(name)
        try {
            val result: T = body
            result
        } finally {
            popName()
        }
    }

}
