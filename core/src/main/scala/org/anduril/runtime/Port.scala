package org.anduril.runtime

import java.io.File

import org.anduril.core.Version
import org.anduril.core.network._
import org.anduril.core.readers.networkParser.{Tools, NetworkHandler, EvaluatorHandler}

final class Port(val name: String, owner: Component, ci: ComponentInstance, val isForced: Boolean=false) {
    val component = owner
    val componentInstance = ci
    val networkPort = ci.getComponent().getOutPort(name)

    override def toString() = s"$networkPort ( $owner )"

    def textRead() = {
        try {
            io.Source.fromFile(this.content).getLines.mkString("\n")
        }
        catch {
            case e: java.nio.charset.MalformedInputException => 
                val msg = s"Non text port $name."
                throw new IllegalArgumentException(msg)
        }
    }

    def textRows() = {
        try {
            io.Source.fromFile(this.content).getLines.length
        }
        catch {
            case e: java.nio.charset.MalformedInputException => 
                val msg = s"Non text port $name."
                throw new IllegalArgumentException(msg)
        }
    }

    /** Disable type checking at both Scala and workflow level. */
    def force(): Port = {
        new Port(name, owner, ci, true)
    }

    @deprecated("This method is no longer needed", "2016-04-25")
    def forceSoft(): Port = {
        this
    }

    /** Return a file object to the dynamic contents of the port.
      * Calling this method invokes the execution of the workflow
      * constructed so far.
      * For array ports, this function returns the directory
      * corresponding to the array; usually, [contentArray]
      * should be used instead to obtain the array index file.
      */
    def content(): File = {
        val evaluator = EvaluatorHandler.getNetworkEvaluator()
        val componentInstancePort = new ComponentInstancePort(ci, this.networkPort)
        evaluator.getDynamicOutputFile(componentInstancePort, ci.getSourceLocation())
    }
    
    /** For array ports, return a file object pointing to the
      * dynamically constructed array index of the port.
      * Calling this method invokes the execution of the workflow
      * constructed so far.
      */
    def contentArray(): File = {
        content() // Ensure content is available
        val root =  EvaluatorHandler.getNetworkEvaluator().getExecutionRoot
        componentInstance.getOutputArrayIndex(this.networkPort, root)
    }

    /** Extract a port element from an Anduril array.
      * @param arrayElementName Array key used for extraction.
      * @throws IllegalArgumentException If the port is not an array.
      */
    def apply(arrayElementName: String, _name: String = null): Port = {
        if (componentInstance.getPortIsArray(networkPort) == ArrayStatus.FALSE) {
            val msg = "%s.%s: Can not access array elements: port is not an array".format(
                componentInstance.getName, name)
            throw new IllegalArgumentException(msg)
        }

        // TODO: use constants instead of literals
        val args = scala.collection.mutable.Map("array" -> this, "key1" -> arrayElementName)
        if (_name != null) {
            args("_name") = _name
        }
        val comp = Tools.invokeComponentByName("anduril.builtin.ArrayExtractor", args)
        Tools.getPortByName(comp, "file1")
    }
}
