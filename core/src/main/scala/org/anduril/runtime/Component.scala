package org.anduril.runtime

import java.io.File

import org.anduril.core.network.annotations.Annotation
import org.anduril.core.network.launcher._
import org.anduril.core.network._
import org.anduril.core.readers.naming.StackNameFinder
import org.anduril.core.readers.networkParser.NetworkHandler

import scala.collection.mutable
import scala.collection.JavaConversions._
import scala.util.control.Breaks._
import scala.util.Try

abstract class Component(val componentName: String, version: String, mainDirectory: File) {
    type NComponent = org.anduril.core.network.Component

    val _componentInstance: ComponentInstance

    private val outPorts = mutable.HashMap[String,Port]()

    override def toString() = _componentInstance.toString

    _loadBundleDataTypes()

    protected def _addOutPort(portName: String, port: Port): Unit = {
        this.outPorts(portName) = port
    }

    def apply(outPortName: String): Port = {
        this.outPorts.get(outPortName) match {
            case Some(port) => port.asInstanceOf[Port]
            case None => throw new IllegalArgumentException("out port name " + outPortName + 
                " in component " + componentName + " not found.")
            // TODO: return only port's array port, if name not one of the ports.
        }
    }

    def canEqual(that: Any): Boolean = that.isInstanceOf[Component]

    def _bind(bindFrom: Component): Unit = {
        bindFrom._componentInstance.connectTo(_componentInstance)
    }
    def _bind(bindFrom: Port): Unit = {
        bindFrom.componentInstance.connectTo(_componentInstance)
    }

    @deprecated("Use _custom(\"cpu\") instead", "2016-04-25")
    def _cpu: Int = _custom("cpu").toInt

    @deprecated("Use _custom(\"cpu\") = ... instead", "2016-04-25")
    def _cpu_=(cpu: Int): Unit = {
        _custom("cpu") = cpu.toString
    }

    @deprecated("Use _custom(\"doc\") instead", "2016-04-25")
    def _doc: String = _custom("doc")

    @deprecated("Use _custom(\"doc\") = ... instead", "2016-04-25")
    def _doc_=(doc: String): Unit = {
        _custom("doc") = doc
    }

    def _enabled: Boolean = _componentInstance.getInstanceAnnotation(Annotation.ENABLED).asInstanceOf[Boolean]
    def _enabled_=(enabled: Boolean): Unit = {
        _componentInstance.setInstanceAnnotation(Annotation.ENABLED, enabled)
    }

    def _execute: String = _componentInstance.getInstanceAnnotation(Annotation.EXECUTE).asInstanceOf[String]
    def _execute_=(execute: String): Unit = {
        _componentInstance.setInstanceAnnotation(Annotation.EXECUTE, execute)
    }

    def _custom: mutable.Map[String, String] = {
        scala.collection.JavaConversions.mapAsScalaMap(_componentInstance.getCustomAnnotation())
    }

    def _filename(outputPortName: String, filename: String): Unit = {
        val port: OutPort = _componentInstance.getComponent.getOutPort(outputPortName)
        if (port == null) {
            val msg = "Can not set annotation: %s does not have output port %s".format(
                componentName, outputPortName)
            throw new IllegalArgumentException(msg)
        }
        _componentInstance.setOutPortAnnotation(Annotation.FILENAME, port, filename)
    }

    @deprecated("Use _custom(\"host\") instead", "2016-04-25")
    def _host: String = _custom("host")

    @deprecated("Use _custom(\"host\") = ... instead", "2016-04-25")
    def _host_=(host: String): Unit = {
        _custom("host") = host
    }

    def _keep: Boolean = _componentInstance.getInstanceAnnotation(Annotation.KEEP).asInstanceOf[Boolean]
    def _keep_=(keep: Boolean): Unit = {
        _componentInstance.setInstanceAnnotation(Annotation.KEEP, keep)
    }

    @deprecated("Use _custom(\"memory\") instead", "2016-04-25")
    def _memory: Int = _custom("memory").toInt

    @deprecated("Use _custom(\"memory\") = ... instead", "2016-04-25")
    def _memory_=(memory: Int): Unit = {
        _custom("memory") = memory.toString
    }

    def _name: String = _componentInstance.getSimpleName
    def _fullName: String = _componentInstance.getName
    def _sourceLocation: String = _componentInstance.getSourceLocation.toString

    def _priority: Int = _componentInstance.getInstanceAnnotation(Annotation.PRIORITY).asInstanceOf[Int]
    def _priority_=(priority: Int): Unit = {
        _componentInstance.setInstanceAnnotation(Annotation.PRIORITY, priority)
    }

    def _require(inputPortName: String, require: Boolean): Unit = {
        val port: InPort = _componentInstance.getComponent.getInPort(inputPortName)
        if (port == null) {
            val msg = "Can not set annotation: %s does not have input port %s".format(
                componentName, inputPortName)
            throw new IllegalArgumentException(msg)
        }
        _componentInstance.setInPortAnnotation(Annotation.REQUIRE, port, require)
    }

    @deprecated("Use _custom(KEY) = VALUE instead", "2016-04-25")
    def _userDefined: String = _custom("userdefined")

    @deprecated("Use _custom(KEY) = VALUE instead", "2016-04-25")
    def _userDefined_=(userDefined: String): Unit = {
        _custom("userDefined") = userDefined
    }

    /**
     * Fill type parameters, input and output ports, parameters
     * and launchers of the component object. This callback is invoked
     * once for each component, before it is added to the repository.
     */
    protected def _fillComponentInterface(component: NComponent, repository: Repository): Unit

    /**
     * Load the data types of the bundle for the component, if
     * they are not already loaded.
     */
    protected def _loadBundleDataTypes(): Unit

    protected def _getComponent(repository: Repository): NComponent = {
        val repositoryComp = repository.getComponent(componentName)
        if (repositoryComp == null) {
            val component = new NComponent(componentName, null, version, null, null, null, null, null)
            component.setMainDirectory(mainDirectory)
            _fillComponentInterface(component, repository)
            component.addSpecialPorts(repository)
            repository.addComponent(component)
            component
        } else {
            repositoryComp
        }
    }

    protected def _makeName(): (Seq[String], Boolean) = {
        val nameTokens: Seq[String] = Component.getStack().reverse ++ StackNameFinder.getInstance().getStackName()

        if (nameTokens.isEmpty) {
            val generatedTokens = Seq(NetworkHandler.getNetwork().generateInstanceName(componentName))
            (generatedTokens, true)
        } else {
            if (nameTokens.last == null) {
                val lastName = NetworkHandler.getNetwork().generateInstanceName(componentName)
                val newTokens = Seq.concat(nameTokens.take(nameTokens.size-1), Seq(lastName))
                val filteredTokens = newTokens.filter(n => n != null && !n.startsWith("_") && n != "apply")
                (filteredTokens, true)
            } else {
                val filteredTokens = nameTokens.filter(n => n != null && !n.startsWith("_") && n != "apply")
                (filteredTokens, false)
            }
        }
    }

    protected def _makeComponentInstance(overrideName: String): ComponentInstance = {
        val repository = NetworkHandler.getRepository()
        val component = _getComponent(repository)

        val (fullName, isAutoGenerated) = _makeName()
        val sourceLocation = StackNameFinder.getInstance().getSourceLocation()

        // The new component instance.
        val ci = component.makeComponentInstance(fullName.toArray, sourceLocation)
        if (isAutoGenerated) {
            ci.setAutoGeneratedName(true)
        }
        if (overrideName != null) {
            ci.rename(overrideName)
        }

        ci
    }

    protected def _addComponentInstance(ci: ComponentInstance): Unit = {        

        // Rename parents instead of ci. For Inline component calls
        // we want the outermost (child) component to get the assigned 
        // instance name.
        var still_renaming = true
        while ( still_renaming ) {
            var network = NetworkHandler.getNetwork()
            breakable { // Loop over the network to find existing names
                for ( c <- network if ci.getName() == c.getName() ) {
                      // Same name is found in the network, check for inline parents                      
                    for ( my_in <- ci.getInPortConnections(); c_out <- c.getOutPortConnections() )                            
                        if  (c_out == my_in) {
                            // c is parent of ci !  rename c, instead of ci.
                            val to_cp=my_in.getToPort()
                            val newName = network.generateInstanceName(c.getSimpleName()+"_"+to_cp.getName()+"_"+c.getComponent.getName()+"_")
                            Try ( c.rename(newName) )
                            // After each rename, scan the network again from scratch
                            still_renaming=true
                            break
                        }
                } // for,if,for,if,for
                still_renaming=false
        }} // breakable, while
        val network = NetworkHandler.getNetwork()
        try {
            network.addComponentInstance(ci)
        } catch {
            case e: IllegalArgumentException =>
                val origName = ci.getSimpleName()
                val newName = network.generateInstanceName(ci.getSimpleName()+"_"+ci.getComponent().getName()+"_")
                ci.rename(newName)
                if ( Try ( network.addComponentInstance(ci) ).toOption != None ) return
                // Could not add; Propagate the exception
                ci.rename(origName)
                network.addComponentInstance(ci)
        }
    }

    protected def _makeLauncher(launcherType: String, launcherArgs: java.util.Map[String, String], component: NComponent): Launcher = {
        launcherType match {
            case "bash" => new BashLauncher(launcherType, launcherArgs, component)
            case "java" => new JavaLauncher(launcherType, launcherArgs, component)
            case "r" => new RLauncher(launcherType, launcherArgs, component)
            case "python" => new PythonLauncher(launcherType, launcherArgs, component)
            case "python3" => new Python3Launcher(launcherType, launcherArgs, component)
            case "perl" => new PerlLauncher(launcherType, launcherArgs, component)
            case "matlab" => new MatlabLauncher(launcherType, launcherArgs, component)
            case "octave" => new OctaveLauncher(launcherType, launcherArgs, component)
            case "lua" => new LuaLauncher(launcherType, launcherArgs, component)
            case "scala" =>
                val javaLauncher = new JavaLauncher(launcherType, launcherArgs, component)
                javaLauncher.setIsScalaLauncher(true)
                javaLauncher
        }
    }

    protected def _extractFunctionResult(resultObj: AnyRef, fieldName: String, fieldPosition: Int): Port = {
        resultObj match {
            case p: Product =>
                p.productElement(fieldPosition-1).asInstanceOf[Port]
            case m: scala.collection.Map[_, _] =>
                val typedMap = m.asInstanceOf[scala.collection.Map[String, Port]]
                typedMap(fieldName).asInstanceOf[Port]
            case s: scala.collection.Seq[_] =>
                s(fieldPosition-1).asInstanceOf[Port]
            case port: Port if (fieldPosition == 1) =>
                port.asInstanceOf[Port]
            case x =>
                throw new IllegalArgumentException("%s: can not extract output port %s from %s".format(
                    componentName, fieldName, x))
        }
    }
}

object Component {
    private val explicitNameStack = scala.collection.mutable.Stack[String]()

    def getStack(): Seq[String] = {
        explicitNameStack.toSeq
    }

    def pushName(name: String): Unit = {
        explicitNameStack.push(name)
    }

    def popName(): String = {
        explicitNameStack.pop()
    }
}
