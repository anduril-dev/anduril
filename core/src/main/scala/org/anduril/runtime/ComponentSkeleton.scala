package org.anduril.runtime

import java.io.File
import org.anduril.component.{CommandFile,SkeletonComponent}
import org.anduril.component.{ErrorCode => JavaErrorCode}

/**
Base class for Anduril components written in Scala. The class provides access
to command file properties, including parameters, input and output ports, and
metadata.

Components need to define the [[run]] method that implements the functionality
of the component and returns a status code that indicates success or failure.
It is convenient to define the component as a Scala singleton object.

In the following example component, we first fetch the values of two parameters
(one string and one double). We validate that param1 starts with "x" and that
param2 is greater than 5. We fetch file names of an input port and an output
port and then do the processing (not shown). Upon successful execution, the
error code [[ErrorCode.OK]] is returned.
{{{
import org.anduril.runtime.{ComponentSkeleton,ErrorCode}
object TestComponent extends ComponentSkeleton {
    override def run(): ErrorCode = {
        val param1 = paramS("param1", _.matches("x.*"))
        val param2 = paramD("param2", _ > 5)
        val input1 = input("input1")
        val output1 = output("output1")
        // Process
        ErrorCode.OK
    }
}
}}}
*/
abstract class ComponentSkeleton {
    private class SkeletonAdapter(comp: ComponentSkeleton) extends SkeletonComponent {
        override protected def runImpl(cf: CommandFile): JavaErrorCode = {
            comp.commandFile = cf
            try {
                val status: ErrorCode = comp.run()
                JavaErrorCode.getErrorCode(status.code)
            } catch {
                case e: ParameterValidationError =>
                    cf.writeError(e.getMessage)
                    JavaErrorCode.PARAMETER_ERROR
            }
        }
    }

    private class ParameterValidationError(parameterName: String)
        extends Exception("Parameter %s: invalid value".format(parameterName))

    private var commandFile: CommandFile = null

    /**
     * Return the command file object that contains values for
     * parameters, ports and metadata. It is usually not necessary
     * to directly access this object.
     */
    def getCommandFile(): CommandFile = this.commandFile

    /**
     * Return the filename of given input port. For array data types,
     * this returns the directory that contains the array.
     * @param inPortName Name of input port.
     * @return The filename, or null if the port doesn't exist
     * 	or is not connected.
     */
    def input(inPortName: String): File = {
        this.commandFile.getInput(inPortName)
    }

    /**
     * Return the filename of given input port, or None if the
     * port doesn't exist or is not connected. For array data types,
     * this returns the directory that contains the array.
     * @param inPortName Name of input port.
     * @return The filename, or None if the port doesn't exist
     * 	or is not connected.
     */
    def inputOpt(inPortName: String): Option[File] = {
        this.commandFile.getInput(inPortName) match {
            case null => None
            case f => Some(f)
        }
    }

    /**
     * Return true if the input port exists, it is connected
     * and the input file exists.
     * @param inPortName Name of input port.
     */
    def inputDefined(inPortName: String): Boolean = {
        this.commandFile.inputDefined(inPortName)
    }

    /**
     * Return a metadata value. This is the contents
     * of the metadata.NAME line, where NAME is the
     * metadata name given.
     */
    def metadata(metadataName: String): String = {
        this.commandFile.getMetadata(metadataName)
    }

    /**
     * Return the filename of given output port. For array data types,
     * this returns the directory that contains the array.
     * @param outPortName Name of output port.
     * @return The filename, or null if the port doesn't exist
     * 	or is not connected.
     */
    def output(outPortName: String): File = {
        this.commandFile.getOutput(outPortName)
    }

    private def paramGeneric[T](paramName: String, value: T, validator: (T => Boolean)): T = {
        if (validator != null) {
            validator(value) match {
                case true =>
                case false => throw new ParameterValidationError(paramName)
            }
        }
        value
    }

    /**
     * Return the value of given parameter as a boolean.
     * @param paramName Name of parameter.
     * @throws IllegalArgumentException If the parameter
     *  has no value or is not a valid boolean.
     */
    def paramB(paramName: String): Boolean = {
        this.commandFile.getBooleanParameter(paramName)
    }

    /**
     * Return the value of given parameter as a double.
     * @param paramName Name of parameter.
     * @param validator If non-null, this is a function
     *  that checks the value of the parameter and returns
     *  true if the value is valid. If the function returns
     *  false, execution of the component immediately halts
     *  and a message is printed to the error log.
     * @throws IllegalArgumentException If the parameter
     *  has no value.
     * @throws NumberFormatException If the value is not
     *  a legal double.
     */
    def paramD(paramName: String, validator: (Double => Boolean) = null): Double = {
        paramGeneric(paramName, this.commandFile.getDoubleParameter(paramName), validator)
    }

    /**
     * Return the value of given parameter as an integer.
     * @param paramName Name of parameter.
     * @param validator If non-null, this is a function
     *  that checks the value of the parameter and returns
     *  true if the value is valid. If the function returns
     *  false, execution of the component immediately halts
     *  and a message is printed to the error log.
     * @throws IllegalArgumentException If the parameter
     *  has no value.
     * @throws NumberFormatException If the value is not
     *  a legal integer.
     */
    def paramI(paramName: String, validator: (Int => Boolean) = null): Int = {
        paramGeneric(paramName, this.commandFile.getIntParameter(paramName), validator)
    }

    /**
     * Return the value of given parameter as a string.
     * Return null if the parameter does not exist.
     * @param paramName Name of parameter.
     * @param validator If non-null, this is a function
     *  that checks the value of the parameter and returns
     *  true if the value is valid. If the function returns
     *  false, execution of the component immediately halts
     *  and a message is printed to the error log.
     */
    def paramS(paramName: String, validator: (String => Boolean) = null): String = {
        paramGeneric(paramName, this.commandFile.getParameter(paramName), validator)
    }

    /**
     * Read the array index file of given array input port.
     * @param inPortName Name of array input port.
     * @throws IllegalArgumentException If the input port is empty.
     */
    def readArrayInput(inPortName: String): ArrayIndex = {
        val indexFile = this.commandFile.getInputArrayIndex(inPortName)  
        if (indexFile == null || !indexFile.exists) {
            throw new IllegalArgumentException("Input port is not defined or contents are missing: "+inPortName);
        }
        new ArrayIndex(indexFile)
    }

    /**
     * Returns a folder that may be used for temporary intermediates
     * of the component instance. This folder is automatically created
     * by this method and it is removed by the hosting engine after the
     * execution of the instance.
     */
    def tempDir(): File = {
        this.commandFile.getTempDir()
    }

    /**
     * Write the array index to an output array port.
     * @param index Array index.
     * @param outPortName Name of array output port.
     */
    def writeArrayOutput(index: ArrayIndex, outPortName: String): Unit = {
        index.write(this, outPortName)
    }

    /**
     * Write a message to error stream.
     * @param message Error message. It may contain newlines.
     */
    def writeError(message: String): Unit = {
        this.commandFile.writeError(message)
    }

    /**
     * Write a message to log stream.
     * @param message Log message. It may contain newlines.
     */
    def writeLog(message: String): Unit = {
        this.commandFile.writeLog(message)
    }

    /**
     * Perform the actions of the component. This method is
     * overriden by concete components.
     * @return Exit status that indicates whether execution was
     *  successful ([[ErrorCode.OK]]) or an error occurred. For
     *  errors, various error codes are available, including
     *  a generic error ([[ErrorCode.ERROR]]).
     */
    def run(): ErrorCode

    /**
     * Main entry point for the component. Usually not called manually.
     * @param args Command line arguments. The first argument
     *  must be a path to the command file.
     */
    def main(args: Array[String]) = {
        val adapter = new SkeletonAdapter(this)
        adapter.run(args)
    }
}

object TestComponent extends ComponentSkeleton {
    override def run(): ErrorCode = {
        val index = new ArrayIndex(new File("_index"))
        index("newelem") = new File("newelem.txt")
        index.remove("elem2")
        for ((key, file) <- index) println((key, file))
        index.write(new File("newindex"))
        ErrorCode.OK
    }
}
