package org.anduril.runtime.table.io

import org.anduril.asser.io
import org.anduril.asser.io.CSVParser
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.FileOutputStream
import java.io.OutputStream
import org.anduril.runtime.table.{Row,Table,TableWriter}

/**
Base class for CSV (tab-delimited) readers.

@constructor Initialize table.
@param input Input file.
@param hasHeader If true, the first line (after skipped lines) of
 the file contains the column names.
@param missingValue String for missing values.
@param skipRegexp If non-null, this is a regular expression
 that is used for skipping lines before the actual table content.
 Lines are skipped as long as they match this pattern.
@param delimiter Field delimiter.
@param comments If true, lines beginning with a hash (#) are
 skipped before the actual content.
*/
class CSVTable(
        input: File,
        hasHeader: Boolean = true,
        missingValue: String = "NA",
        delimiter: Char = '\t',
        skipRegexp: String = null,
        comments: Boolean = false)
        extends Table(input.getName()) {
    
    private val reader = new BufferedReader(new FileReader(input))
    if (skipRegexp != null) {
        reader.mark(4096)
        var line = reader.readLine()
        while (line != null && line.matches(skipRegexp)) {
            reader.mark(4096)
            line = reader.readLine()
        }
        reader.reset()
    }

    private val parser = new CSVParser(this.reader,
            delimiter.toString, 0, missingValue, comments, hasHeader)
    
    this.parser.getColumnNames().foreach(createColumn(_))
    private val numCSVColumns = this.numColumns
    
    protected override def produceRow(row: Row): Boolean = {
        if (!this.parser.hasNext()) return false
        val curRow = this.parser.next()
        row.reset()
        for (col <- this.columns if col.creationIndex < this.numCSVColumns) {
            row(col.index) = curRow(col.creationIndex)
        }
        true
    }
}

/**
Base class for CSV (tab-delimited) writers.

@constructor Initialize writer.
@param columns Output column names.
@param target Output stream.
@param withHeader If true, column names are written before main content.
@param quotes If true, string values are quoted.
@param missingValue String for missing values.
@param delimiter Field delimiter
@param preHeader If non-null, this string is written to the output
 before other content. The string should contain a newline if one is
 needed.
*/
class CSVWriter(
        columns: Seq[String],
        target: OutputStream,
        withHeader: Boolean = true,
        quotes: Boolean = false,
        missingValue: String = "NA",
        delimiter: Char = '\t',
        preHeader: String = null
        ) extends TableWriter(columns) {

    if (preHeader != null) target.write(preHeader.getBytes())

    private val writer =
        if (withHeader) new io.CSVWriter(
                columns.toArray, target, delimiter.toString,
                quotes)
        else new io.CSVWriter(
                columns.size, target, delimiter.toString)
    writer.setMissingValue(missingValue)
    
    override def write(row: Row): Unit = {
        row.foreach { s => this.writer.write(s, quotes)}
    }

    override def close(): Unit = {
        this.writer.close()
    }
}
