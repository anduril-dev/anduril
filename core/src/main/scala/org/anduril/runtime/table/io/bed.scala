package org.anduril.runtime.table.io

import java.io.File
import java.io.OutputStream

class BEDTable(input: File)
    extends CSVTable(input, false, ".", '\t', "(track|browser).*", true) {
    renameColumn(0, "chrom")
    renameColumn(1, "chromStart")
    renameColumn(2, "chromEnd")
    if (this.numColumns >= 4) renameColumn(3, "name")
    if (this.numColumns >= 5) renameColumn(4, "score")
    if (this.numColumns >= 6) renameColumn(5, "strand")
}

class BEDWriter(columns: Seq[String], target: OutputStream)
	extends CSVWriter(columns, target, false, false, ".", '\t') {
    assert(columns.size >= 3)
}
