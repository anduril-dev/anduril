package org.anduril.runtime.table.processor

import org.anduril.runtime.table.Row
import org.anduril.runtime.table.Table

final class ModifyColumnString(table: Table, columnName: String,
        value: (String, Row) => String) extends Function1[Row, Unit] {
    val columnIndex = table.getColumn(columnName)
    
    def apply(row: Row): Unit = {
        val curVal = row(this.columnIndex)
        if (curVal != null) {
        	row(this.columnIndex) = value(curVal, row)
        }
    }
}

final class ModifyColumnInt(table: Table, columnName: String,
        value: (Int, Row) => Int) extends Function1[Row, Unit] {
    val columnIndex = table.getColumn(columnName)
    
    def apply(row: Row): Unit = {
        val curVal = row.intOpt(this.columnIndex)
        if (curVal.isDefined) {
        	row(this.columnIndex) = value(curVal.get, row)
        }
    }
}

final class ModifyColumnDouble(table: Table, columnName: String,
        value: (Double, Row) => Double) extends Function1[Row, Unit] {
    val columnIndex = table.getColumn(columnName)
    
    def apply(row: Row): Unit = {
        val curVal = row.doubleOpt(this.columnIndex)
        if (curVal.isDefined) {
        	row(this.columnIndex) = value(curVal.get, row)
        }
    }
}
