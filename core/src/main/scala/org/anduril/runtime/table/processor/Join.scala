package org.anduril.runtime.table.processor

import org.anduril.runtime.table.Column
import org.anduril.runtime.table.Row
import org.anduril.runtime.table.Table
import scala.collection.mutable.HashMap

final class Join(t1: Table, t2: Table, key1: Seq[Column], key2: Seq[Column])
	extends Function1[Row, Unit] {
   
        assert(key1.size == key2.size)

	private val beginCreationIndex = t1.getColumnCreationCount()
	private val endCreationIndex = beginCreationIndex + t2.getColumnCreationCount()
	
	private val column1To2Map = new HashMap[Column,Column]()
	private val column2To1Map = new HashMap[Column,Column]()
	
	for (col2 <- t2.columns) {
		val otherCol =
			if (t1.hasColumn(col2.name)) t2.name + '_' + col2.name
			else col2.name
		val col1 = t1.createColumn(otherCol)
		this.column1To2Map(col1) = col2
		this.column2To1Map(col2) = col1
	}

        private def keyFunc1(row: Row): Seq[String] = {
            key1.map(row(_))
        }

        private def keyFunc2(row: Row): Seq[String] = {
            key2.map(row(_))
        }

	lazy val table2Map = t2.toMapG(keyFunc2)
    
	override def apply(table1Row: Row): Unit = {
	    val key = keyFunc1(table1Row)
	    val table2Row =
                if (key.forall(_ != null)) this.table2Map.get(key)
                else None
	    for (col1 <- t1.columns) {
	        if (col1.creationIndex >= this.beginCreationIndex &&
	                col1.creationIndex < this.endCreationIndex) {
                    table1Row(col1.index) = table2Row match {
                        case None => null
                        case Some(row2) =>
                            val col2 = this.column1To2Map(col1)
                            row2(col2)
                    }
	        }
	    }
	}
	
	def map1to2(column1: Column): Column = {
	    this.column1To2Map(column1)
	}

	def map2to1(column2: Column): Column = {
	    this.column2To1Map(column2)
	}
}
