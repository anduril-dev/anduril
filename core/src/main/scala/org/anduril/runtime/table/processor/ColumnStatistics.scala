package org.anduril.runtime.table.processor

import org.anduril.runtime.table.Column
import org.anduril.runtime.table.Row
import org.anduril.runtime.table.Table
import org.apache.commons.math3.stat.descriptive
import org.apache.commons.math3.stat.descriptive.SummaryStatistics
import java.util.Arrays
import org.apache.commons.collections.primitives.ArrayDoubleList
import org.apache.commons.math3.stat.descriptive.rank.Median
import org.apache.commons.math3.stat.descriptive.rank.Percentile
import org.apache.hadoop.metrics2.util.Quantile
import org.apache.hadoop.metrics2.util.SampleQuantiles

final class ColumnStatistics(
        val column: Column,
        val quantileLimit: Int,
        val quantileEpsilon: Double) extends Function1[Row, Unit] {
	private val summary = new SummaryStatistics()
	private var collected =
	    if (quantileLimit > 0) new ArrayDoubleList(1024)
	    else null
	private var approxQuantile: Option[SampleQuantiles] = None
	private var exactQuantile: Option[Percentile] = None
	private var numMissing = 0L
	
	override def apply(row: Row): Unit = {
	    if (row(this.column) != null) {
	    	val value = row.double(this.column)
	    	this.summary.addValue(value)

	    	if (row.rowNumber < this.quantileLimit) {
	    		this.collected.add(value)
	    	} else if (quantileEpsilon > 0) {
	    	    getApproxQuantileObj.insert(value)
	    	}
	    } else {
	        this.numMissing += 1
	    }
	}
	
	override def toString(): String = {
	    "N=%d min=%g max=%g mean=%g sd=%g NA=%d".format(
	            n(),
	            min(),
	            max(),
	            mean(),
	            sd(),
	            missing()
	    )
	}
	
	def geometricMean(): Double = this.summary.getGeometricMean()
	def max(): Double = this.summary.getMax()
	def mean(): Double = this.summary.getMean()
	def min(): Double = this.summary.getMin()
	def missing(): Long = this.numMissing
	def n(): Long = this.summary.getN()
	def sd(): Double = this.summary.getStandardDeviation()
	def sum(): Double = this.summary.getSum()
	def variance(): Double = this.summary.getVariance()
	
	private def getExactQuantileObj(): Percentile = {
	    this.exactQuantile match {
	        case None =>
	        	val array = this.collected.toArray()
	        	Arrays.sort(array)
	        	this.exactQuantile = Some(new Percentile())
	        	this.exactQuantile.get.setData(array)
	        	this.exactQuantile.get
	        case Some(x) => x
	    }
	}
	
	private def getApproxQuantileObj(): SampleQuantiles = {
	    this.approxQuantile match {
	        case Some(x) => x
	        case None =>
	            val quantiles = Array(0, 0.1, 0.25, 0.5, 0.75, 0.9, 1)
	            val quantileObjs = quantiles.map(new Quantile(_, quantileEpsilon))
	        	this.approxQuantile = Some(new SampleQuantiles(quantileObjs))
	        	this.collected.toArray().foreach(this.approxQuantile.get.insert((_)))
	        	this.collected = null
	        	this.approxQuantile.get
	    }
	}
	
	def quantile(quantile: Double): Double = {
	    assert(quantile >= 0 && quantile <= 1)
	    if (n() < this.quantileLimit) {
	        getExactQuantileObj.evaluate(quantile*100.0)
	    } else {
	        this.approxQuantile match {
	            case None =>
	                throw new IllegalArgumentException("Can not computed median: row limit exceeded")
	            case Some(x) => 
	                x.insertBatch()
	                x.query(quantile)
	        }
	    }
	}
	
	def median(): Double = quantile(0.5)
}
