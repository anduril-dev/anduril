package org.anduril.runtime.table

import java.util.Arrays

/**
Data structure for accessing values in a row in a [[Table table]]. The table defines a
list of columns, which are accessed using names (like a map) or indexes (like a
sequence). This class provides the contents for the row. Values can be accessed
as strings (apply), integers (int, intOpt) or doubles (double, doubleOpt).
Columns can also be assigned new values (update).

The underlying table object is available as [[Row.table]]. Column names are available
as [[Row.columnNames]] for convenience.

==Example: Iterating over a CSV file==

In this example, we iterate over the rows of the following tab-delimited file.
See [[Table Table]] for further examples.

Tab-delimited file (people.csv):

{{{
Name      Height     Mass
Alice     1.68       56
Bob       1.83       75
Charlie   1.75       95
Eve       1.65       65
}}}

Code:

{{{
import org.anduril.runtime.table.Table

for (row <- Table.reader("people.csv")) {
    println(row.columnNames)
    println(row.rowNumber) // Starts from 0

    val name = row("Name")
    val nameUsingPosition = row(0)
    assert(name == nameUsingPosition)

    val height = row.double("Height")
    val mass = row.int("Mass")

    println("%s is %.2f m tall and weighs %d kg".format(name, height, mass))
}
}}}

@constructor Create a new row that is bound to the given table. Values in all
 columns are initially undefined.
*/
final class Row(val table: Table)
    extends IndexedSeq[String] with Cloneable with Ordered[Row] {

    /*
    Internally, values are stored both as strings as doubles. Integer values are
    obtained from the doubles. A value in a column can be in one of three states:
    (1) undefined (e.g., after calling reset(), (2) defined but missing (null), or
    (3) defined and non-missing (a regular string or number).
    */

    /** Number of columns in the row. */
    override val length = table.numColumns

    /** Return names of columns. */
    def columnNames: IndexedSeq[String] = table.columnNames

    private val strings = new Array[String](length)
    private val doubles = new Array[Double](length)
    
    private val stringsDefined = new Array[Boolean](length)
    private val doublesDefined = new Array[Boolean](length)

    private var curRowNum: Int = 0

    private def parseInt(s: String): Int = java.lang.Integer.parseInt(s, 10)

    private def parseDouble(s: String): Double = {
        if (s == null) Double.NaN
        else java.lang.Double.parseDouble(s)
    }

    private def doubleToString(d: Double): String = {
        if (java.lang.Double.isNaN(d)) null
        else d.toString
    }
    
    /** Return the current row number, starting from 0. */
    def rowNumber: Int = curRowNum

    /** Assign the current row number. */
    def setRowNumber(rowNumber: Int) = {
        assert(rowNumber >= 0)
        this.curRowNum = rowNumber
    }

    /** Increase the current row number by one. */
    def incRowNumber() = this.curRowNum += 1
    
    /** Clone the row contents into a new row object.
      * Calling this method is necessary when a permanent
      * row object is needed. */
    override def clone(): Row = {
        val newRow = new Row(table)
        this.strings.copyToArray(newRow.strings)
        this.doubles.copyToArray(newRow.doubles)
        this.stringsDefined.copyToArray(newRow.stringsDefined)
        this.doublesDefined.copyToArray(newRow.doublesDefined)
        newRow.curRowNum = this.curRowNum
        newRow
    }

    override def compare(other: Row): Int = {
        if (this.table != other.table) {
            return java.lang.Integer.compare(this.table.hashCode(), other.table.hashCode())
        }

        /* While loop for speed */
        var i = 0
        while (i < this.length) {
            if (this.doublesDefined(i) && other.doublesDefined(i)) {
                val cmp = java.lang.Double.compare(this.doubles(i), other.doubles(i))
                if (cmp != 0) return cmp
            } else {
                /* Nulls last */
                val s1 = this.strings(i)
                val s2 = other.strings(i)
                if (s1 == null) {
                    if (s2 != null) return 1
                } else if (s2 == null) {
                    return -1
                } else {
                    val cmp = s1.compareTo(s2)
                    if (cmp != 0) return cmp
                }
            }
            i += 1
        }

        0
    }

    override def equals(other: Any): Boolean = {
        if (other.isInstanceOf[Row]) {
            compare(other.asInstanceOf[Row]) == 0
        } else {
            false
        }
    }

    override def hashCode(): Int = {
        var hash = 0
        var i = 0
        while (i < this.length) {
            if (this.doublesDefined(i)) {
                hash ^= new java.lang.Double(this.doubles(i)).hashCode()
            } else {
                val s = this.strings(i)
                if (s != null) hash ^= s.hashCode()
            }
            i += 1
        }
        hash
    }

    /** Return the contents of given column as a string.
      * Null is returned for missing values.
      * @param column Column index, 0-based.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset(). */
    override def apply(column: Int): String = {
        if (this.stringsDefined(column)) {
            this.strings(column)
        } else if (this.doublesDefined(column)) {
            this.stringsDefined(column) = true
            var str = doubleToString(this.doubles(column))
            if (str != null && str.endsWith(".0")) {
                str = str.substring(0, str.size-2)
            }
            this.strings(column) = str
            str
        } else {
            throw new IllegalStateException("Value is not defined at column "+column)
        }
    }
    
    /** Return the contents of given column as a string.
      * Null is returned for missing values.
      * @param column Column name.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset(). */
    def apply(column: String): String = apply(table.getColumn(column).index)
    
    /** Return the contents of given column as a string.
      * Null is returned for missing values.
      * @param column Column object.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset(). */
    def apply(column: Column): String = {
        apply(column.index)
    }
    
    /** Return the contents of given column as an integer.
      * @param index Column index, 0-based.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid integer, including when it is a missing
      *  value. */
    def int(index: Int): Int = {
        if (this.doublesDefined(index)) {
            this.doubles(index).toInt
        } else if (this.stringsDefined(index)) {
            val num = parseInt(this.strings(index))
            this.doubles(index) = num
            this.doublesDefined(index) = true
            num
        } else {
            throw new IllegalStateException("Value is not defined at column "+index)
        }
    }
    
    /** Return the contents of given column as an integer.
      * @param column Column object.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid integer, including when it is a missing
      *  value. */
    def int(column: Column): Int = int(column.index)

    /** Return the contents of given column as an integer.
      * @param column Column name.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid integer, including when it is a missing
      *  value. */
    def int(column: String): Int = int(table.getColumn(column).index)

    /** Return the contents of given column as an integer, or
      * as None for missing values.
      * @param column Column index, 0-based.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid integer. */
    def intOpt(index: Int): Option[Int] = {
        if (this.doublesDefined(index)) {
            Some(this.doubles(index).toInt)
        } else if (this.stringsDefined(index)) {
            val value = this.strings(index)
            if (value == null) None
            else {
                val num = parseInt(this.strings(index))
                this.doubles(index) = num
                this.doublesDefined(index) = true
                Some(num)
            }
        } else {
            throw new IllegalStateException("Value is not defined at column "+index)
        }
    }

    /** Return the contents of given column as an integer, or
      * as None for missing values.
      * @param column Column name.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid integer. */
    def intOpt(column: String): Option[Int] = {
        intOpt(table.getColumn(column).index)
    }

    /** Return the contents of given column as a double.
      * Double.NaN is returned for missing values.
      * @param column Column index, 0-based.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid double. */
    def double(index: Int): Double = {
        if (this.doublesDefined(index)) {
            this.doubles(index)
        } else if (this.stringsDefined(index)) {
        val str = this.strings(index)
            val num = parseDouble(str)
            this.doublesDefined(index) = true
            this.doubles(index) = num
            num
        } else {
            throw new IllegalStateException("Value is not defined at column "+index)
        }
    }
    
    /** Return the contents of given column as a double.
      * Double.NaN is returned for missing values.
      * @param column Column object.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid double. */
    def double(column: Column): Double = double(column.index)

    /** Return the contents of given column as a double.
      * Double.NaN is returned for missing values.
      * @param column Column name.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid double. */
    def double(column: String): Double = double(table.getColumn(column).index)

    /** Return the contents of given column as a double, or None
      * for missing values.
      * @param column Column index, 0-based.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid double. */
    def doubleOpt(index: Int): Option[Double] = {
        if (this.doublesDefined(index)) {
            Some(this.doubles(index))
        } else if (this.stringsDefined(index)) {
            val value = this.strings(index)
            if (value == null) None
            else {
                val num = parseDouble(this.strings(index))
                this.doubles(index) = num
                this.doublesDefined(index) = true
                Some(num)
            }
        } else {
            throw new IllegalStateException("Value is not defined at column "+index)
        }
    }

    /** Return the contents of given column as a double, or None
      * for missing values.
      * @param column Column name.
      * @throws IllegalStateException If the value at the column
      *  is not defined, e.g., after calling reset().
      * @throws NumberFormatException If the value at the column
      *  is not a valid double. */
    def doubleOpt(column: String): Option[Double] = {
        doubleOpt(table.getColumn(column).index)
    }
    
    /** Assign the value of a string column.
      * @param column Column index, 0-based.
      * @param value New value. */
    def update(column: Int, value: String): Unit = {
        this.strings(column) = value
        this.stringsDefined(column) = true
        this.doublesDefined(column) = false
    }
    
    /** Assign the value of a string column.
      * @param column Column name.
      * @param value New value. */
    def update(column: String, value: String): Unit = {
        update(table.getColumn(column).index, value)
    }

    /** Assign the value of an integer column.
      * @param column Column index, 0-based.
      * @param value New value. */
    def update(column: Int, value: Int): Unit = {
        this.doubles(column) = value
        this.stringsDefined(column) = false
        this.doublesDefined(column) = true
    }

    /** Assign the value of an integer column.
      * @param column Column name.
      * @param value New value. */
    def update(column: String, value: Int): Unit = {
        update(table.getColumn(column).index, value)
    }
    
    /** Assign the value of a double column.
      * @param column Column index, 0-based.
      * @param value New value. */
    def update(column: Int, value: Double): Unit = {
        this.doubles(column) = value
        this.stringsDefined(column) = false
        this.doublesDefined(column) = true
    }

    /** Assign the value of a double column.
      * @param column Column name.
      * @param value New value. */
    def update(column: String, value: Double): Unit = {
        update(table.getColumn(column).index, value)
    }
    
    /** Undefine the values in all columns. After
      * this, all apply, int and double methods are
      * undefined until new data values are defined. */
    def reset() = {
        Arrays.fill(this.stringsDefined, false)
        Arrays.fill(this.doublesDefined, false)
    }
    
    /** Assign values to a set of columns in a batch operation.
      * @param values New string values
      * @param fromIndex Column index from which to start assignment,
      *  0-based. */
    def setData(values: Seq[String], fromIndex: Int) = {
        values.copyToArray(this.strings, fromIndex)
        Arrays.fill(this.stringsDefined, fromIndex, fromIndex+values.size, true)
    }
}
