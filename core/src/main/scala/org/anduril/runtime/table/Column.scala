package org.anduril.runtime.table

import scala.language.implicitConversions

/**
Representation of a column in a table. The identity of column object is
permanent, i.e., does not change when columns are moved or renamed.
*/
final class Column(_creationIndex: Int, private var _index: Int, private var _name: String) {
    assert(_creationIndex >= 0)
    
    /** Return an internal integer that is always unchanged
      * and denotes the creation order of columns in a given table. */
    def creationIndex() = _creationIndex

    /** Return the current 0-based index of the column. */
    def index() = this._index

    /** Return the current name of the column. */
    def name() = this._name
    
    override def toString(): String = {
        "Column %s (index=%d, creation=%d)".format(name, index, creationIndex)
    }
    
    /** Return true if the column name matches the given
      * regular expression. */
    def matches(regex: String): Boolean = {
        this._name.matches(regex)
    }
    
    private[table] def setIndex(index: Int): Unit = {
        this._index = index
    }
    
    private[table] def setName(name: String): Unit = {
        this._name = name
    }
}

object Column {
    /** Implicitly convert a Column object to a string (column name). */
    implicit def columnToString(column: Column): String = column.name
}
