package org.anduril.runtime.table.processor

import org.anduril.runtime.table.{Row,Table}

final class AddColumnString(table: Table, columnName: String,
        value: Row => String) extends Function1[Row, Unit] {
    val column = table.createColumn(columnName)
    
    def apply(row: Row): Unit = {
        row(this.column) = value(row)  
    }
}

final class AddColumnInt(table: Table, columnName: String,
        value: Row => Int) extends Function1[Row, Unit] {
    val column = table.createColumn(columnName)
    
    def apply(row: Row): Unit = {
        row(this.column) = value(row)  
    }
}

final class AddColumnDouble(table: Table, columnName: String,
        value: Row => Double) extends Function1[Row, Unit] {
    val column = table.createColumn(columnName)
    
    def apply(row: Row): Unit = {
        row(this.column) = value(row)  
    }
}

final class AddColumnStringOpt(table: Table, columnName: String,
        value: Row => Option[String]) extends Function1[Row, Unit] {
    val column = table.createColumn(columnName)
    
    def apply(row: Row): Unit = {
        row(this.column) = value(row) match {
            case None => null
            case Some(x) => x
        }
    }
}

final class AddColumnIntOpt(table: Table, columnName: String,
        value: Row => Option[Int]) extends Function1[Row, Unit] {
    val column = table.createColumn(columnName)
    
    def apply(row: Row): Unit = {
        val curVal: Option[Int] = value(row)
        curVal match {
            case None => row(column) = null
            case Some(x) => row(column) = x
        }
   }
}

final class AddColumnDoubleOpt(table: Table, columnName: String,
        value: Row => Option[Double]) extends Function1[Row, Unit] {
    val column = table.createColumn(columnName)
    
    def apply(row: Row): Unit = {
        val curVal: Option[Double] = value(row)
        curVal match {
            case None => row(column) = null
            case Some(x) => row(column) = x
        }
   }
}
