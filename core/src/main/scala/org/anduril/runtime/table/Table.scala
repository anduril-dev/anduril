package org.anduril.runtime.table

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.LinkedHashSet

/*
TODO
Histogram
Group by
GFF, BAM/SAM
Overlap queries with index
Statistics tools (t-test, other tests, FDR; where?)
Expand / collapse rows
GZ files; bzip (Apache Commons Compress)
Faster CSVParser 
*/

/**
Base class for reading and modifying rectangular tables composed of rows and
columns. The content for the table may originate from various file formats, but
different formats are accessed in a similar manner. Currently, the file formats
supported are CSV (tab-delimited), BED and VCF.

A table is composed of an iterable list of [[Row rows]], each of which has the
same number of columns. Columns can be accessed using a name or an index
(0-based).  Contents of rows can be read as strings, integers or doubles. Rows
can also contain missing values. Normally, the data type of a column is
unchanged between successive rows, although this is not enforced by the class.

Tables can be iterated in two ways. Using [[foreach]], a manual for-loop can be
written. A more convenient access method, however, is setting up row processors
to the table object and then calling [[readAll]] or write. A row processor is
an action that modifies or accesses the contents of a row during an iteration.
Row processors are available for filtering rows, adding and modifying row
contents, joins between tables, collecting statistics, and arbitrary
transformations.

Before iteration is started, the schema of the table can be modified by
renaming, moving and removing columns. This is useful when the table is written
to an output file. Columns are represented either using indexes, names or
[[Column column objects]]. The latter are useful when columns are renamed or
moved, because the identity of column objects is permanent (except when columns
are removed).

Normally, Table instances are created using the [[Table$ Table]] companion object.

==Example: Iterating over a CSV file==

In this example, we iterate over the rows of a CSV file and access the columns
of each row. Our example data set consists of heights and masses of people
stored in a tab-delimited file (people.csv):

{{{
Name      Height     Mass
Alice     1.68       56
Bob       1.83       75
Charlie   1.75       95
Eve       1.65       65
}}}

We use [[Row Row]] accessors to access row values as strings, doubles or ints.

{{{
import org.anduril.runtime.table.Table

val table = Table.reader("people.csv")

// Prints "Name", "Height" and "Mass"
println(table.columnNames)

for (row <- table) {
    val name = row("Name")
    val height = row.double("Height")
    val mass = row.int("Mass")
    println("%s is %.2f m tall and weighs %d kg".format(name, height, mass))
}
}}}

Instead of column names, we can alternatively use 0-based column indexes.
Also, if we don't need to access table attributes prior to iterating, we can
stream line the code as follows:

{{{
for (row <- Table.reader("people.csv")) {
    val name = row(0)
    val height = row.double(1)
    val mass = row.int(2)
    println("%s is %.2f m tall and weighs %d kg".format(name, height, mass))
}
}}}

==Example: Filtering and modifying a CSV file==

In this example, we use the people.csv file above and modify the rows by adding
the body mass index (BMI) for each person.

In the code, we have three phases, of which the second is central. We rename
the "Name" column, move the position of the mass column, add a BMI column, and
remove input rows where height is below 1.66 meters.

{{{
import org.anduril.runtime.table.Table

// Phase 1: set up a Table reader with an automatically inferred format
val table = Table.reader("people.csv")

// Phase 2: arrange columns and add row processors
table.renameColumn("Name", "FirstName")
table.moveColumn("Mass", 1)
table.addColumnD("BodyMassIndex",
    r => r.double("Mass") / math.pow(r.double("Height"), 2))
table.filter(r => r.double("Height") > 1.66)

// Phase 3: iterate over rows and write result into output
// Alternatively, we could iterate manually with:
// for (row <- table) println(row)
val writer = Table.writer(table, System.out, Table.FormatCSV)
table.write(writer)
}}}

This prints:
{{{
FirstName  Mass      Height     BodyMassIndex
Alice      56        1.68       19.841269841269845
Bob        75        1.83       22.395413419331717
Charlie    95        1.75       31.020408163265305
}}}
*/
class Table(_name: String) extends Traversable[Row] with Iterable[Row] {
    private val index2Column = new ArrayBuffer[Column]()
    private val name2Column = new HashMap[String,Column]()
    private var columnCreationCount = 0
    private var nameVar = _name

    private type ProcessorType = Function1[Row, Unit]
    
    private var filterFunc: Row => Boolean = null
    private var abortFunc: Row => Boolean = null
    private val processors = new ArrayBuffer[ProcessorType]()

    /* Overridable methods ********************************************/
    
    protected def produceRow(row: Row): Boolean = false

    /* Basic **********************************************************/
    
    final def name() = this.nameVar
    final def setName(name: String) = this.nameVar = name

    /* Column handling ************************************************/
    
    /** Return the number of columns. */
    final def numColumns: Int = this.index2Column.size

    /** Return the number of columns that have been added to the table. */
    final def getColumnCreationCount(): Int = this.columnCreationCount

    /** Return the sequence of active columns. */
    final def columns: IndexedSeq[Column] = this.index2Column

    /** Return names of columns. */
    final def columnNames: IndexedSeq[String] = this.index2Column.map(_.name)
    
    /** Return a column by name if it is exists, or None otherwise.
      * @param name Column name. */
    final def getColumnOpt(name: String): Option[Column] = {
        this.name2Column.get(name)
    }

    /** Return a column by name.
      * @throws IllegalArgumentException If the column does not exist. */
    final def getColumn(name: String): Column = {
        this.name2Column.getOrElse(name,
                throw new IllegalArgumentException(
                    "Column not found in %s: %s (columns are: %s...)".format(
                    this.nameVar, name,
                    this.index2Column.take(5).map(_.name).mkString(","))))
    }

    /** Return a column by index.
      * @param index Column index, 0-based.
      * @throws IllegalArgumentException If the column does not exist. */
    final def getColumn(index: Int): Column = {
        try {
            this.index2Column(index)
        } catch {
            case e: IndexOutOfBoundsException =>
                throw new IllegalArgumentException("Column not found: "+index)
        }
    }
    
    /** Return true if a column with the name exists. */
    final def hasColumn(name: String): Boolean = {
        getColumnOpt(name).isDefined
    }
    
    /** Raw method for creating a column without defining any
      * content for it. Content must be created manually. Usually,
      * it is more convenient to use addColumnX.
      * @param name Name of the new column */
    final def createColumn(name: String): Column = {
        val creationIndex = this.columnCreationCount
        this.columnCreationCount += 1
        val index = this.index2Column.size
        val finalName = if (name == null) index.toString else name
        
        val column = new Column(creationIndex, index, finalName)
        this.index2Column.append(column)
        this.name2Column.put(finalName, column)
        column
    }

    /** Rename a column in the given index.
      * @param column Column index, 0-based.
      * @param newName New name for the column. */
    final def renameColumn(column: Int, newName: String): Unit = {
        val columnObj = getColumn(column)
        this.name2Column.remove(columnObj.name)
        columnObj.setName(newName)
        this.name2Column.put(newName, columnObj)
    }
    
    /** Rename a column.
      * @param column Current (old) name of the column.
      * @param newName New name for the column. */
    final def renameColumn(column: String, newName: String): Unit = {
    	renameColumn(getColumn(column).index, newName)
    }

    final private def resetColumnIndices(fromIndex: Int): Unit = {
        val pairs = this.index2Column.view.zipWithIndex.drop(fromIndex)
        pairs.foreach { case (c, i) => c.setIndex(i) }
    }
    
    /** Remove the column in the given position. After this
      * call, any references to the column are invalid.
      * @param column Column index, 0-based. */
    final def removeColumn(column: Int): Unit = {
        val columnObj = getColumn(column)
        this.index2Column.remove(columnObj.index)
        this.name2Column.remove(columnObj.name)
        resetColumnIndices(columnObj.index)
        columnObj.setIndex(-1)
    }
    
    /** Remove the column with the given name. After this
      * call, any references to the column are invalid.
      * @param column Column name. */
    final def removeColumn(column: String): Unit = {
        removeColumn(getColumn(column).index)
    }

    /** Remove all columns whose names match a regular expression.
      * @param regexp Regular expression for matching column names. */ 
    final def removeColumnsMatch(regexp: String): Unit = {
        val columns = this.name2Column.keys.filter(_.matches(regexp))
        columns.foreach { col => removeColumn(col) }
    }

    /** Move a column to a new index.
      * @param column Current (old) index of the column, 0-based.
      * @param newIndex New index, 0-based. */
    final def moveColumn(column: Int, newIndex: Int): Unit = {
        val columnObj = getColumn(column)
        val oldIndex = columnObj.index
        if (columnObj.index >= 0) {
            this.index2Column.remove(columnObj.index)
        }
        val finalIndex = Math.min(newIndex, this.index2Column.size)
        this.index2Column.insert(finalIndex, columnObj)
        columnObj.setIndex(finalIndex)
        resetColumnIndices(Math.min(oldIndex, finalIndex))
    }
    
    /** Move a column to a new index.
      * @param column Column name.
      * @param newIndex New index, 0-based. */
    final def moveColumn(column: String, newIndex: Int): Unit = {
        moveColumn(getColumn(column).index, newIndex)
    }
    
    /** Retain only those columns whose names are in the given
      * list. Other columns are removed.
      * @param columnNames Sequence of included column names. */
    final def includeColumns(columnNames: Traversable[String]) = {
        val columns = columnNames.view.map(c => getColumn(c))
        val exclude = this.index2Column.diff(columns.toSeq)
        exclude.foreach(col => removeColumn(col.index))
        resetColumnIndices(0)
    }

    /** Retain only those columns whose names match the given
      * regular expression. Other columns are removed.
      * @param regexp Regular expression matching included column names. */
    final def includeColumnsMatch(regexp: String) = {
        includeColumns(this.name2Column.keys.filter(_.matches(regexp)))
    }
    
    /* Basic processors ***********************************************/
    
    /** Raw method for registering a row processor. */
    final def addProcessor(processor: Row => Unit) = {
        this.processors.append(processor)
    }
    
    /** Add a row processor that filters (skips) rows
      * that do not match the given condition. If this
      * method is called multiple times, the filters
      * are combined using AND, i.e., all filter functions
      * must return true for the row to be processed.
      * @param func Function that inspects the contents
      *  of a row and returns true if the row should be
      *  accepted, and false if is should be skipped. */
    final override def filter(func: Row => Boolean) = {
        if (this.filterFunc == null) {
            this.filterFunc = func
        } else {
            val oldFunc = this.filterFunc
            this.filterFunc = (r: Row) => oldFunc(r) && func(r)
        }
        this
    }

    /** Add a row processor that aborts the iteration
      * of the table if the condition is true. If this
      * method is called multiple times, the abort functions
      * are combined using OR, i.e., iteration is aborted
      * if any condition is true. Aborting does not throw
      * an exception.
      * @param func Function that inspects the contents
      *  of a row and returns true if table iteration
      *  should be aborted immediately. */
    final def abort(func: Row => Boolean) = {
        if (this.abortFunc == null) {
            this.abortFunc = func
        } else {
            val oldFunc = this.abortFunc
            this.abortFunc = (r: Row) => oldFunc(r) || func(r)
        }
        this
    }
   
    /** Add a row processor that performs arbitrary
      * transformations to a row. This is a fallback processor
      * that is used when no specific processor is available.
      * @param transformer Function that takes a row as parameter
      *  and returns nothing. It may do arbitrary operations on
      *  the row. */
    final def transform(transformer: Row => Unit) = {
        addProcessor(transformer)
    }
    
    /* Add and modify columns *****************************************/
    
    /** Row processor for adding a new string column with a function to
      * create content.
      * @param columnName Name of the new column.
      * @param value Function that returns a new value for the column.
      *  It may inspect other columns in the current row, which is given
      *  as parameter. The function may return null, which is the missing
      *  value. */
    final def addColumnS(columnName: String, value: Row => String): Column = {
   		val proc = new processor.AddColumnString(this, columnName, value)
   		addProcessor(proc)
   		proc.column
    }
    
    /** Row processor for adding a new integer column with a function to
      * create content.
      * @param columnName Name of the new column.
      * @param value Function that returns a new value for the column.
      *  It may inspect other columns in the current row, which is given
      *  as parameter. Is is not possible to return a missing value. */
    final def addColumnI(columnName: String, value: Row => Int): Column = {
   		val proc = new processor.AddColumnInt(this, columnName, value)
   		addProcessor(proc)
   		proc.column
    }
    
    /** Row processor for adding a new double column with a function to
      * create content.
      * @param columnName Name of the new column.
      * @param value Function that returns a new value for the column.
      *  It may inspect other columns in the current row, which is given
      *  as parameter. The function may return Double.NaN, which is the missing
      *  value. */
    final def addColumnD(columnName: String, value: Row => Double): Column = {
   		val proc = new processor.AddColumnDouble(this, columnName, value)
   		addProcessor(proc)
   		proc.column
    }

    /** Row processor for modifying the value of an existing string column.
      * @param columnName Name of the existing column.
      * @param value Function that returns a replacement value for the column.
      *  It may inspect the old value (first argument) and other columns in the
      *  current row. The function may return null, which is the missing value. */
    final def modifyColumnS(columnName: String, value: (String, Row) => String): Unit = {
   		val proc = new processor.ModifyColumnString(this, columnName, value)
   		addProcessor(proc)
    }

    /** Row processor for modifying the value of an existing integer column.
      * @param columnName Name of the existing column.
      * @param value Function that returns a replacement value for the column.
      *  It may inspect the old value (first argument) and other columns in the
      *  current row. It is not possible to return a missing value. */
    final def modifyColumnI(columnName: String, value: (Int, Row) => Int): Unit = {
   		val proc = new processor.ModifyColumnInt(this, columnName, value)
   		addProcessor(proc)
    }
    
    /** Row processor for modifying the value of an existing double column.
      * @param columnName Name of the existing column.
      * @param value Function that returns a replacement value for the column.
      *  It may inspect the old value (first argument) and other columns in the
      *  current row. The function may return Double.NaN, which is the missing value. */
    final def modifyColumnD(columnName: String, value: (Double, Row) => Double): Unit = {
   		val proc = new processor.ModifyColumnDouble(this, columnName, value)
   		addProcessor(proc)
    }
    
    /* Collect and statistics *****************************************/
    
    /** Row processor that collects the string values of a column
      * into an array during iteration. The array may contains nulls.
      * @param column Name of the existing column to be collected.
      * @return A function that returns the array when called
      *  with no arguments after iteration. */
    final def collectS(column: String): () => Array[String] = {
        val proc = new processor.CollectString(getColumn(column))
        addProcessor(proc)
        () => proc.toArray
    }

    /** Row processor that collects the integer values of a column
      * into an array during iteration.
      * @param column Name of the existing column to be collected.
      * @return A function that returns the array when called
      *  with no arguments after iteration. */
    final def collectI(column: String): () => Array[Int] = {
        val proc = new processor.CollectInt(getColumn(column))
        addProcessor(proc)
        () => proc.toArray()
    }

    /** Row processor that collects the double values of a column
      * into an array during iteration. The array may contain
      * Double.NaNs.
      * @param column Name of the existing column to be collected.
      * @return A function that returns the array when called
      *  with no arguments after iteration. */
    final def collectD(column: String): () => Array[Double] = {
        val proc = new processor.CollectDouble(getColumn(column))
        addProcessor(proc)
        () => proc.toArray
    }
    
    /** Row processor that collects numerical statistics on a
      * column during iteration.
      * @param column Name of the existing column. */
    final def statistics(column: String,
            quantileLimit: Int = 10000, quantileEpsilon: Double = 0.001):
    		processor.ColumnStatistics = {
        val index = getColumn(column)
        val proc = new processor.ColumnStatistics(index, quantileLimit, quantileEpsilon)
        addProcessor(proc)
        proc
    }

    /* Joins **********************************************************/

    /** Row processor that joins this table with the other table during
      * iteration of this table using multi-column keys.
      * The other table is read into memory before iteration and thus the
      * smaller table should be given as other.
      * Joining adds the columns from the other table to the
      * current table. Duplicate columns are renamed. Joining rows is
      * done by comparing the string values of key columns.
      * @param other Table to be joined to this table
      * @param keyColumnsThis List of columns in this table that contain
      *  key values.
      * @param keyColumnsOther List of columns in the other table that contain
      *  key values. */
    final def join(other: Table, keyColumnsThis: Seq[String], keyColumnsOther: Seq[String]):
                    processor.Join = {
        val columnsThis = keyColumnsThis.map(getColumn(_))
        val columnsOther = keyColumnsOther.map(other.getColumn(_))
        val joinObj = new processor.Join(this, other, columnsThis, columnsOther) 
        addProcessor(joinObj)
        for (col2 <- columnsOther) {
            val col1 = joinObj.map2to1(col2)
            removeColumn(col1)
        }
        joinObj
    }

    /** Row processor that joins this table with the other table during
      * iteration of this table using single-column keys.
      * The other table is read into memory before iteration and thus the
      * smaller table should be given as other.
      * Joining adds the columns from the other table to the
      * current table. Duplicate columns are renamed. Joining rows is
      * done by comparing the string values of key columns.
      * @param other Table to be joined to this table
      * @param keyColumnThis The column in this table that contains
      *  key values.
      * @param keyColumnOther The column in the other table that contains
      *  key values. */
    final def join(other: Table, keyColumnThis: String, keyColumnOther: String):
                    processor.Join = {
        join(other, Seq(keyColumnThis), Seq(keyColumnOther))
    }
	
    /* Iterating ******************************************************/
    
    /** Basic method for iterating over the rows of the table.
      * Often, this method is not called directly.
      * Note: for performance reasons, the method always produces
      * the same row object, with contents updated for the current
      * row. Call row.clone() to get a permanent copy. */
    final override def foreach[U](f: Row => U): Unit = {
        val curRow = new Row(this)

        while (produceRow(curRow) &&
                (this.abortFunc == null || !this.abortFunc(curRow))) {
            if (this.filterFunc == null || this.filterFunc(curRow)) {
                    this.processors.foreach(_(curRow))
                    f(curRow)
            }
            curRow.incRowNumber()
        }
    }

    final override def iterator: Iterator[Row] = {
        this.toIterator
    }

    /** Iterate over the rows of the table, running row processors
      * on each row. */
    final def readAll(): Unit = foreach { r => }

    /** Write all the rows to a file. Row processors are executed
      * as usual.
      * @param target Output file
      * @param format File format, one of Table.FormatX constants. */
    final def write(target: File, format: String = Table.FormatAuto): Unit = {
        val writer = Table.writer(this, target, format)
        foreach(writer.write(_))
        writer.close()
    }

    /** Write all the rows to a file. Row processors are executed
      * as usual.
      * @param target Output file
      * @param format File format, one of Table.FormatX constants. */
    final def write(target: OutputStream, format: String): Unit = {
        val writer = Table.writer(this, target, format)
        foreach(writer.write(_))
        writer.close()
    }

    /** Write all the rows to a table writer. Row processors are executed
      * as usual.
      * @param writer Table writer */
    final def write(writer: TableWriter): Unit = {
        foreach(writer.write(_))
        writer.close()
    }

    /** Read the table into an in-memory array. Row processors are executed
      * as usual. */
    final def toArray(): Array[Row] = {
        val array = new ArrayBuffer[Row]()
        foreach { r => array.append(r.clone) }
        array.toArray
    }

    /** Read the table into an in-memory map from custom keys to rows.
      * Row processors are executed as usual.
      * @param keyFunc Function that produces a key value from a row.
      *  If there are rows with duplicate keys, the last is included. */
    final def toMapG[K](keyFunc: Row => K): scala.collection.Map[K, Row] = {
        val map = new HashMap[K, Row]()
        foreach { r => map.put(keyFunc(r), r.clone) }
        map
    }

    /** Read the table into an in-memory map from key column values to rows.
      * Row processors are executed as usual.
      * @param keyColumn Name of a column that contains key values.
      *  If there are rows with duplicate keys, the last is included. */
    final def toMap(keyColumn: String): scala.collection.Map[String, Row] = {
        val columnIndex = getColumn(keyColumn)
        val map = new HashMap[String, Row]()
        foreach { r => map.put(r(columnIndex), r.clone) }
        map
    }

    /** Read the table into an in-memory map from custom keys to rows using
      * an aggregate function for duplicate keys.
      * Row processors are executed as usual.
      * @param keyFunc Function that produces a key value from a row.
      * @param aggregate Function that collapses a sequence of rows
      *  with identical keys to a single row. */
    final def toMapAggregateG[K](keyFunc: Row => K, aggregate: IndexedSeq[Row] => Row):
                    scala.collection.Map[K, Row] = {
        val arrayMap = new HashMap[K, ArrayBuffer[Row]]()
        for (row <- this) {
            val key = keyFunc(row)
            val array = arrayMap.getOrElseUpdate(key, new ArrayBuffer[Row]())
            array.append(row.clone)
        }
        
        val resultMap = new HashMap[K, Row]()
        resultMap.sizeHint(arrayMap.size)
        for ((key, array) <- arrayMap) {
            resultMap(key) = aggregate(array)
        }
        resultMap
    }

    /** Read the table into an in-memory map from key column values to rows using
      * an aggregate function for duplicate keys.
      * Row processors are executed as usual.
      * @param keyColumn Name of a column that contains key values.
      * @param aggregate Function that collapses a sequence of rows
      *  with identical keys to a single row. */
    final def toMapAggregate(keyColumn: String, aggregate: IndexedSeq[Row] => Row):
                    scala.collection.Map[String, Row] = {
        val columnIndex = getColumn(keyColumn)
        toMapAggregateG(r => r(columnIndex), aggregate)
    }

    /** Read the table into an in-memory set of rows that contains no duplicate
      * rows and is in the same order as original rows. Implemented using
      * LinkedHashSet. */
    final def toSet(): scala.collection.mutable.Set[Row] = {
        val set = LinkedHashSet[Row]()
        foreach(r => set.add(r.clone))
        set
    }

    /** Read the table into a sorted in-memory array. */
    final def sorted(): Array[Row] = {
        toArray().sorted
    }

    def apply(columnName: String) = getColumn(columnName)
}

/**
Convenience object for creating table readers and writers, and containing
various constants.
*/
object Table {
    /** Constant for the automatic file format, in which format is
      * inferred from file name. */
    val FormatAuto: String = "auto"

    /** Constant for the BED file format. */
    val FormatBED: String = "bed"

    /** Constant for tab-separated file format. */
    val FormatCSV: String = "csv"

    /** Constant for variant call format (VCF). */
    val FormatVCF: String = "vcf"

    /** Names of the first six columns for BED. */
    val ColumnsBED6 = Seq("chrom", "chromStart", "chromEnd", "name", "score", "strand")

    /** Standard names of the first eight columns for VCF. */
    val ColumnsVCF = Seq("CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO")

    private val extensions = Map(
            "bed" -> FormatBED,
            "csv" -> FormatCSV,
            "maf" -> FormatCSV, /* Mutation Annotation Format */
            "tsv" -> FormatCSV,
            "txt" -> FormatCSV,
            "vcf" -> FormatVCF
            )
            
    private def normalizeFormat(file: File, format: String): String = {
        format.toLowerCase match {
            case FormatAuto =>
                val filename = file.getName
                val pos = filename.lastIndexOf('.')
                if (pos > 0) {
                    val ext = filename.substring(pos+1).toLowerCase()
                    val inferredFormat = extensions.get(ext)
                    inferredFormat match {
                        case Some(fmt) => fmt
                        case None => new IllegalArgumentException(
                            "Can not determine file format automatically: "+filename)
                    }
                    return inferredFormat.get
                }
                throw new IllegalArgumentException(
                                "Can not determine file format automatically: "+filename)
            case fmt @ (FormatBED | FormatCSV) => fmt
            case _ => throw new IllegalArgumentException("Unknown format: "+format)
        }
    }

    /** Create a table reader from a file.
      * @param source Table file
      * @param format Format of the table file, as one of the FormatX
      *  constants. If FormatAuto is given, the format is inferred from
      *  the file name. */
    def reader(source: File, format: String = FormatAuto): Table = {
      normalizeFormat(source, format) match {
        case FormatBED => new io.BEDTable(source)
        case FormatCSV => new io.CSVTable(source)
        case FormatVCF => new io.VCFTable(source)
        case _ => throw new IllegalArgumentException("Unknown format")
      }
    }

    /** Create a table reader from a file name. File format is autodetected.
      * @param sourcePath Table file name
      */
    def reader(sourcePath: String): Table = {
        reader(new File(sourcePath))
    }

    /** Create a table writer based on a list of column names.
      * @param columns Column names for the output table.
      * @param target Stream for writing the table.
      * @param format Format of the output table, as one of the FormatX
      *  constants. FormatAuto is not supported in this variant. */
    def writer(columns: Seq[String], target: OutputStream, format: String): TableWriter = {
        format match {
            case FormatBED => new io.BEDWriter(columns, target)
            case FormatCSV => new io.CSVWriter(columns, target)
            case FormatVCF => new io.VCFWriter(columns, target)
            case _ => throw new IllegalArgumentException("Invalid format: "+format)
        }
    }

    /** Create a table writer based on a list of column names.
      * @param columns Column names for the output table.
      * @param target File for writing the table.
      * @param format Format of the output table, as one of the FormatX
      *  constants. If FormatAuto is given, the format is inferred from
      *  the file name. */
    def writer(columns: Seq[String], target: File, format: String = FormatAuto): TableWriter = {
        writer(columns, new FileOutputStream(target), normalizeFormat(target, format))
    }

    /** Create a table writer based on a template table that defines
      * output columns.
      * @param template Template table from which columns are taken.
      * @param target Stream for writing the table.
      * @param format Format of the output table, as one of the FormatX
      *  constants. FormatAuto is not supported in this variant. */
    def writer(template: Table, target: OutputStream, format: String): TableWriter = {
        writer(template.columnNames, target, format)
    }

    /** Create a table writer based on a template table that defines
      * output columns.
      * @param template Template table from which columns are taken.
      * @param target File for writing the table.
      * @param format Format of the output table, as one of the FormatX
      *  constants. If FormatAuto is given, the format is inferred from
      *  the file name. */
    def writer(template: Table, target: File, format: String): TableWriter = {
        writer(template.columnNames, new FileOutputStream(target), format)
    }

}
