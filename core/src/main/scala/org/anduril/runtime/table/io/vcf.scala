package org.anduril.runtime.table.io

import java.io.File
import java.io.OutputStream
import org.anduril.runtime.table.Table

class VCFTable(input: File)
    extends CSVTable(input, true, ".", '\t', "##.*", false) {
    renameColumn(0, "CHROM") /* Remove # from column name */
}

class VCFWriter(columns: Seq[String], target: OutputStream)
	extends CSVWriter(columns, target, true, false, ".", '\t', "##fileformat=VCFv4.1\n#") {
    assert(columns.size >= 8)
    for ((col, i) <- Table.ColumnsVCF.zipWithIndex) {
        assert(columns(i) == col,
            "VCF column at position %d must be named %s".format(i+1, col))
    }
}
