package org.anduril.runtime.table.processor

import scala.collection.mutable.ArrayBuffer
import org.anduril.runtime.table.Column
import org.anduril.runtime.table.Row
import org.anduril.runtime.table.Table
import org.apache.commons.collections.primitives.ArrayIntList
import org.apache.commons.collections.primitives.ArrayDoubleList

final class CollectString(column: Column) extends Function1[Row, Unit] {
    private val data = new ArrayBuffer[String](1024)
    
	def apply(row: Row): Unit = {
	    this.data.append(row(column))
	}
    
    def toArray(): Array[String] = this.data.toArray
}

final class CollectInt(column: Column) extends Function1[Row, Unit] {
    private val data = new ArrayIntList(1024)
    
	def apply(row: Row): Unit = {
        if (row(column) != null) {
        	this.data.add(row.int(column))    
        }
	}
    
    def toArray(): Array[Int] = this.data.toArray()
}

final class CollectDouble(column: Column) extends Function1[Row, Unit] {
    private val data = new ArrayDoubleList(1024)
    
	def apply(row: Row): Unit = {
        if (row(column) != null) {
        	this.data.add(row.double(column))
        } else {
        	this.data.add(Double.NaN)
	}
	}
    
    def toArray(): Array[Double] = this.data.toArray()
}
