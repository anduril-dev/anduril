package org.anduril.runtime.table

/**
Base class for table writers.
*/
abstract class TableWriter(columnNames: Seq[String]) {
    private val table = new Table("output")
    columnNames.map(table.createColumn(_))

    /** Return a row that can be used for generating new
      * values to the output table. The row contains the columns
      * configured for this writer. After obtaining the row,
      * assign its contents and write to the output using write(). */
    val row = new Row(this.table)

    /** Return the list of column objects set up for this
      * table. */
    val columns: IndexedSeq[Column] = this.table.columns

    /** Write the given row to this table. */
    def write(row: Row): Unit

    /** Close the output table. This must be called after
      * all rows are written. */
    def close(): Unit
}
