package org.anduril.runtime

/**
Anduril component error codes, returned to the Anduril
core. Actual values are in the [[ErrorCode$ companion object]].
*/
case class ErrorCode(val code: Int) {}

object ErrorCode {
    /** Successful execution. */
    val OK = new ErrorCode(0)

    /** Generic failed execution. */
    val ERROR = new ErrorCode(1)

    /** No command file was given as command line argument. */
    val NO_COMMAND_FILE = new ErrorCode(20)

    /** The command file could not be read. */
    val COMMAND_FILE_IO_ERROR = new ErrorCode(21)

    /** An input file could not be read. */
    val INPUT_IO_ERROR = new ErrorCode(22)

    /** The error stream could not be written to. */
    val INVALID_ERRORSTREAM = new ErrorCode(23)

    /** A parameter is missing or has an invalid value. */
    val PARAMETER_ERROR = new ErrorCode(24)

    /** An uncaught exception was thrown during component execution. */
    val UNCAUGHT_EXCEPTION = new ErrorCode(25)

    /** An input file is badly formatted or semantically invalid. */
    val INVALID_INPUT = new ErrorCode(26)

    /** The command file contains syntax errors. */
    val INVALID_COMMAND_FILE = new ErrorCode(27)

    /** An output port cannot be written. */
    val OUTPUT_IO_ERROR = new ErrorCode(30)
}
