package org.anduril.runtime

/**
  * LinkedHashMap that has a top level name and can provide generated
  * names for component instances added to the map.
  */
class NamedMap[T](var name: String, elems: (String, T)*) extends scala.collection.mutable.LinkedHashMap[String, T] {

    super.++(elems)

    def rename(newName: String): Unit = {
        this.name = newName
    }

    def apply(key: Int): T = {
        super.apply(key.toString)
    }

    def update(key: Int, value: T): Unit = {
        super.update(key.toString, value)
    }

    override def +=(elem: (String, T)): NamedMap.this.type = {
        val key: String = elem._1
        val value: T = elem._2

        /* Set instance name if not set already. */
        value match {
            case c: Component =>
                if ( c._componentInstance.isAutoGeneratedName()  ) {
                    c._componentInstance.rename(name+"_"+key)
                }
            case p: Port =>
                if ( p.componentInstance.isAutoGeneratedName ) {
                    p.componentInstance.rename(name+"_"+key)
                }
            case m: NamedMap[T] =>
                /* Replace value with a map whose name is a composite */
                val compositeName = this.name + "_" + key
                m.rename(compositeName)
            case _ =>
            // Do nothing
        }

        super.+=((key, value))

        this
    }
}

object NamedMap {
    def apply[T](name: String, elems: (String, T)*): NamedMap[T] = {
        new NamedMap[T](name, elems: _*)
    }
}
