package org.anduril.runtime

import java.io.File
import scala.collection.mutable.Map
import org.anduril.component.IndexFile;

/**
Represents an Anduril array index: a mapping from string keys to file names.
The mapping maintains the insertion order of keys. The index can be read from a
file in the constructor and written to a file using write. Other than these
features, this class implements a regular mutable map.

==Example==

In the following example, we read an existing index from _index, add an element
to it and remove an existing element. Then, we iterate over the index and write
it to a new file.

Contents of existing index (_index):
{{{
Key      File
elem1    element1.txt
elem2    element2.txt
}}}

Code:
{{{
val index = new ArrayIndex(Some(new File("_index")))
index("newelem") = new File("newelem.txt")
index.remove("elem2")
for ((key, file) <- index) println((key, file))
index.write(new File("newindex"))
}}}

Result (newindex):
{{{
Key      File
elem1    element1.txt
newelem  newelem.txt
}}}

@constructor Initialize either an empty array or one read from a file.
@param indexFile If None, the array will be initially empty. Otherwise,
 the index is read from the file.
*/
class ArrayIndex(indexFile: File = null) extends Map[String, File] {
    private class PairIterator extends Iterator[(String, File)] {
        private val iter = index.iterator()

        def hasNext: Boolean = {
            iter.hasNext
        }

        def next(): (String, File) = {
            val key = iter.next()
            (key, index.getFile(key))
        }
    }

    private val index = indexFile match {
        case null => new IndexFile()
        case f: File => IndexFile.read(f)
    }

    def get(key: String): Option[File] = {
        this.index.getFile(key) match {
            case null => None
            case f: File => Some(f)
        }
    }

    def iterator: Iterator[(String, File)] = {
        new PairIterator()
    }

    def += (kv: (String, File)) = {
        val key = kv._1
        val file = kv._2
        this.index.add(key, file)
        this
    }

    def -= (key: String) = {
        this.index.remove(key)
        this
    }

    override def empty = {
        new ArrayIndex(null)
    }

    override def size: Int = {
        this.index.size()
    }

    override def foreach[U](f: ((String, File)) => U): Unit = {
        val iter = iterator
        while (iter.hasNext) f(iter.next())
    }

    /**
     * Writes the index to given file.
     * @param file Output array index file.
     * @throws IOException When I/O error occurs.
     */
    def write(file: File): Unit = {
        index.write(file)
    }

    /**
     * Writes the index to given output array port.
     * @param component Component instance representation.
     * @param outPortName Name of the output array port.
     * @throws IOException When I/O error occurs.
     */
    def write(component: ComponentSkeleton, outPortName: String): Unit = {
        index.write(component.getCommandFile(), outPortName)
    }
}
