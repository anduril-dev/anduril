package util

import java.io.File
import java.nio.file.Files
import org.anduril.core.utils.IOTools
import org.scalatest._
import scala.collection.mutable.StringBuilder
import scala.sys.process._
import scala.io.Source

/** Base class for all tests that need a work (temporary)
  * directory and access to the testing directory. */
abstract class BaseTest extends FunSpec {

    protected def workDirectory(): File = {
        EnvironmentHelper.tempDir
    }

    protected def test(testName: String)(func: File => Unit): Unit = {
        val testDirectory = new File(workDirectory(), testName)
        IOTools.rmdir(testDirectory, true)
        testDirectory.mkdirs()
        it(testName)(func(testDirectory))
    }

    protected def ignore(testName: String)(func: File => Unit): Unit = {
        super.ignore(testName){}
    }
}

object EnvironmentHelper {

    val tempDir = Files.createTempDirectory("anduril-sbt-test-").toFile()
    tempDir.deleteOnExit()

}