package engine

import org.scalatest._

import org.anduril.core.network.Network
import org.anduril.core.engine.{CurrentState, Engine}

import anduril.techtest._
import org.anduril.runtime._

class StateInitTest extends NetworkTestBase[(Boolean, Boolean, Boolean, Boolean, String, Boolean, Boolean)] {

    type ConfigType = (Boolean, Boolean, Boolean, Boolean, String, Boolean, Boolean)

    override def numRounds = 2

    override def title = "StateInitTest: test state initialization with a->b workflow using 2 rounds"

    override def formatTestCase(config: ConfigType): String = {
        val (bFail1, aChanged2, bChanged2, aKeep, bExecute, aUptodate2, bUptodate2) = config
        "b.fail1=%5s a.changed2=%5s b.changed2=%5s a.keep=%5s b.execute=%7s a.uptodate2=%5s b.uptodate2=%5s".format(
            bFail1, aChanged2, bChanged2, aKeep, bExecute, aUptodate2, bUptodate2)
    }

    override def configs = Seq(
    //   b       a         b         a       b         a            b
    //   fail1   changed2  changed2  keep    execute   uptodate(2)  uptodate(2)
        (false,  false,    false,    false,  Always,   false,       false),
        (false,  false,    false,    false,  Changed,  true,        true),
        (false,  false,    false,    false,  Once,     true,        true),

        (false,  false,    false,    true,   Always,   true,        false),
        (false,  false,    false,    true,   Changed,  true,        true),
        (false,  false,    false,    true,   Once,     true,        true),

        (false,  false,    true,     false,  Always,   false,       false),
        (false,  false,    true,     false,  Changed,  false,       false),
        (false,  false,    true,     false,  Once,     true,        true),

        (false,  false,    true,     true,   Always,   true,        false),
        (false,  false,    true,     true,   Changed,  true,        false),
        (false,  false,    true,     true,   Once,     true,        true),


        (false,  true,     false,    false,  Always,   false,       false),
        (false,  true,     false,    false,  Changed,  false,       false),
        (false,  true,     false,    false,  Once,     false,       true),

        (false,  true,     false,    true,   Always,   false,       false),
        (false,  true,     false,    true,   Changed,  false,       false),
        (false,  true,     false,    true,   Once,     false,       true),

        (false,  true,     true,     false,  Always,   false,       false),
        (false,  true,     true,     false,  Changed,  false,       false),
        (false,  true,     true,     false,  Once,     false,       true),

        (false,  true,     true,     true,   Always,   false,       false),
        (false,  true,     true,     true,   Changed,  false,       false),
        (false,  true,     true,     true,   Once,     false,       true),

    //   b       a         b         a       b         a            b
    //   fail1   changed2  changed2  keep    execute   uptodate(2)  uptodate(2)
        (true,   false,    false,    false,  Always,   true,        false), // a is up-to-date because b failed and a was not cleaned
        (true,   false,    false,    false,  Changed,  true,        false),
        (true,   false,    false,    false,  Once,     true,        false),

        (true,   false,    false,    true,   Always,   true,        false),
        (true,   false,    false,    true,   Changed,  true,        false),
        (true,   false,    false,    true,   Once,     true,        false),

        (true,   false,    true,     false,  Always,   true,        false),
        (true,   false,    true,     false,  Changed,  true,        false),
        (true,   false,    true,     false,  Once,     true,        false),

        (true,   false,    true,     true,   Always,   true,        false),
        (true,   false,    true,     true,   Changed,  true,        false),
        (true,   false,    true,     true,   Once,     true,        false),


        (true,   true,     false,    false,  Always,   false,       false),
        (true,   true,     false,    false,  Changed,  false,       false),
        (true,   true,     false,    false,  Once,     false,       false),

        (true,   true,     false,    true,   Always,   false,       false),
        (true,   true,     false,    true,   Changed,  false,       false),
        (true,   true,     false,    true,   Once,     false,       false),

        (true,   true,     true,     false,  Always,   false,       false),
        (true,   true,     true,     false,  Changed,  false,       false),
        (true,   true,     true,     false,  Once,     false,       false),

        (true,   true,     true,     true,   Always,   false,       false),
        (true,   true,     true,     true,   Changed,  false,       false),
        (true,   true,     true,     true,   Once,     false,       false)
        )

    override def populateNetwork(network: Network, round: Int, config: ConfigType): Boolean = {
        val (bFail1, aChanged2, bChanged2, aKeep, bExecute, aUptodate2, bUptodate2) = config

        val aDummy = if (round == 1 || !aChanged2) 0 else 1
        val bDummy = if (round == 1 || !bChanged2) 0 else 1
        val bFail = (round == 1 && bFail1)

        val a = C1(dummy=aDummy)
        a._keep = aKeep

        val b = C2(a, dummy=bDummy, fail=bFail)
        b._execute = bExecute

        !bFail
    }

    override def checkBeforeExecution(engine: Engine, network: Network, round: Int, config: ConfigType): Unit = {
        val (bFail1, aChanged2, bChanged2, aKeep, bExecute, aUptodate2, bUptodate2) = config
        val nodeA = findComponentInstance(network, "a")
        val nodeB = findComponentInstance(network, "b")
        val outFileA = getOutPortFile(nodeA, "out1", engine)
        val outFileB = getOutPortFile(nodeB, "out1", engine)

        round match {
            case 1 =>
                assertResult(CurrentState.NO, "Is node 'a' up-to-date before round "+round) { engine.getCurrentState(nodeA) }
                assertResult(CurrentState.NO, "Is node 'b' up-to-date before round "+round) { engine.getCurrentState(nodeB) }
            case 2 =>
                val expectedA = if (aUptodate2) CurrentState.YES else CurrentState.NO
                val expectedB = if (bUptodate2) CurrentState.YES else CurrentState.NO
                assertResult(expectedA, "Is node 'a' up-to-date before round "+round) { engine.getCurrentState(nodeA) }
                assertResult(expectedB, "Is node 'b' up-to-date before round "+round) { engine.getCurrentState(nodeB) }

                val expectOutputAPresent = aKeep || bFail1
                assertResult(expectOutputAPresent, "Is output file of 'a' present after round 1") { outFileA.exists() }
                assertResult(true, "Is output file of 'b' present after round 1") { outFileB.exists() }
        }
    }
}
