package engine

import org.anduril.core.network.Network
import org.anduril.core.engine.{ActiveState, CurrentState, Engine}

import anduril.techtest._
import org.anduril.runtime._

class DisabledNodeTest extends NetworkTestBase[(Boolean, Boolean, Boolean, CurrentState, ActiveState, CurrentState, ActiveState)] {

    type ConfigType = (Boolean, Boolean, Boolean, CurrentState, ActiveState, CurrentState, ActiveState)

    override def numRounds = 3

    override def title = "DisabledNodeTest: test _enabled annotation in a network with a single node using 3 rounds"

    override def formatTestCase(config: ConfigType): String = {
        val (disabled1, disabled2, changed2, current2, active2, current3, active3) = config
        "disabled1=%5s disabled2=%5s changed2=%5s current2=%3s active2=%8s current3=%3s active3=%3s".format(
            disabled1, disabled2, changed2, current2, active2, current3, active3)
    }

    override def configs = Seq(
    //   disabled1  disabled2  changed2  current2          active2               current3          active3
        (false,     false,     false,    CurrentState.YES, ActiveState.YES,      CurrentState.YES, ActiveState.YES),
        (false,     false,     true,     CurrentState.NO,  ActiveState.YES,      CurrentState.YES, ActiveState.YES),
        (false,     true,      false,    CurrentState.YES, ActiveState.DISABLED, CurrentState.YES, ActiveState.YES),
        (false,     true,      true,     CurrentState.NO,  ActiveState.DISABLED, CurrentState.NO,  ActiveState.YES),

        (true,      false,     false,    CurrentState.NO,  ActiveState.YES,      CurrentState.YES, ActiveState.YES),
        (true,      false,     true,     CurrentState.NO,  ActiveState.YES,      CurrentState.YES, ActiveState.YES),
        (true,      true,      false,    CurrentState.NO,  ActiveState.DISABLED, CurrentState.NO,  ActiveState.YES),
        (true,      true,      true,     CurrentState.NO,  ActiveState.DISABLED, CurrentState.NO,  ActiveState.YES)
        )

    override def populateNetwork(network: Network, round: Int, config: ConfigType): Boolean = {
        val (disabled1, disabled2, changed2, current2, active2, current3, active3) = config

        val dummy = if (round>=2 && changed2) 1 else 0
        val disabled = round match {
            case 1 => disabled1
            case 2 => disabled2
            case _ => false }

        val a = C1(dummy=dummy)
        a._enabled = !disabled

        true
    }

    override def checkBeforeExecution(engine: Engine, network: Network, round: Int, config: ConfigType): Unit = {
        val (disabled1, disabled2, changed2, current2, active2, current3, active3) = config
        val node = findComponentInstance(network, "a")

        round match {
            case 1 =>
                // No checks
            case 2 =>
                assertResult(current2, "Is node 'a' up-to-date in round "+round) { engine.getCurrentState(node) }
                assertResult(active2,  "Is node 'a' active in round "+round)     { engine.getActiveState(node) }
            case 3 =>
                assertResult(current3, "Is node 'a' up-to-date in round "+round) { engine.getCurrentState(node) }
                assertResult(active3,  "Is node 'a' active in round "+round)     { engine.getActiveState(node) }
        }
    }

}
