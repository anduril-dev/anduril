package engine

import java.io.File
import org.scalatest._
import scala.collection.JavaConversions._

import org.anduril.core.commandline.BaseCommand
import org.anduril.core.engine.NetworkEvaluator
import org.anduril.core.network.{Bundle, ComponentInstance, Network, Repository, StaticError}
import org.anduril.core.engine.{CurrentState, DynamicError, Engine, NetworkEvaluator}
import org.anduril.core.readers.networkParser.NetworkHandler
import org.anduril.core.utils.IOTools

import anduril.builtin._

/**
 * Base class for tests that populate, execute and inspect a network
 * using components from the techtest bundle. The basic execution cycle
 * is: for the current round, populate the network (populateNetwork),
 * inspect engine state before execution (checkBeforeExecution), execute
 * network, and inspect engine state after execution (checkAfterExecution).
 * This is repeated for numRounds times. The cycle is implemented by
 * this base class, and subclasses override the methods they need.
 * It is possible to test multiple scenarios using configs; each config
 * object describes a variation of the test.
 */
abstract class NetworkTestBase[T] extends util.BaseTest {

    val Always = "always"
    val Changed = "changed"
    val Once = "once"

    private val executionDirectory = workDirectory()

    /** Return the number of network executions. */
    def numRounds: Int

    /** Return config objects, each of which describes one test scenario.
      * Config objects can be used to modify network populating and engine
      * checking. */
    def configs: Seq[T]

    /** Return a single label describing the test case. */
    def title: String

    /** Format the current test scenario. This is shown in addition to test title. */
    def formatTestCase(config: T): String

    def runChecksIfExpectFailure: Boolean = false

    /** Populate network. Return true if successful execution is
      * expected this round, false is there are expected to be
      * errors.
      * @param network The network into which component instances
      *  are inserted. This network is already defined as the
      *  target for Scala-based components, so it is sufficient
      *  to invoke functions such as C1().
      * @param round Current execution round, between 1 and numRounds
      *  inclusive.
      * @param config Object describing the current test scenario.
      */
    def populateNetwork(network: Network, round: Int, config: T): Boolean

    /** Check engine state before execution of the network. 
      * The default implementation does nothing. */
    def checkBeforeExecution(engine: Engine, network: Network, round: Int, config: T): Unit = {}

    /** Check engine state after execution of the network.
      * The default implementation does nothing. */
    def checkAfterExecution(engine: Engine, network: Network, round: Int, config: T): Unit = {}

    def findComponentInstance(network: Network, simpleName: String): ComponentInstance = {
        for (ci <- network.getMembers) {
            if (ci.getSimpleName() == simpleName) return ci
        }
        throw new IllegalArgumentException("No component instance with name %s was found".format(simpleName))
    }

    def getOutPortFile(componentInstance: ComponentInstance, portName: String, engine: Engine): File = {
        componentInstance.getOutputFile(
            componentInstance.getComponent().getOutPort(portName),
            engine.getExecutionDirectory().getRoot())
    }

    def getC3InPortName(typeName: String): String = {
        typeName match {
            case "A" => "in1"
            case "B" => "in2"
            case "C" => "in3"
            case "D" => "in4"
            case "E" => "in5"
            case err => throw new IllegalArgumentException("Invalid type: "+err)
        }
    }

    def getC3OutPortName(typeName: String): String = {
        typeName match {
            case "A" => "out1"
            case "B" => "out2"
            case "C" => "out3"
            case "D" => "out4"
            case "E" => "out5"
            case err => throw new IllegalArgumentException("Invalid type: "+err)
        }
    }

    private def executeNetwork(network: Network, evaluator: NetworkEvaluator, round: Int, config: T, expectSuccess: Boolean): Unit = {
        if (expectSuccess) {
            val engine = evaluator.makeEngine(network)
            checkBeforeExecution(engine, network, round, config)
            evaluator.execute(engine)
            checkAfterExecution(engine, network, round, config)
        } else {
            var engine: Engine = null
            try {
                engine = evaluator.makeEngine(network)
                if (runChecksIfExpectFailure) {
                    checkBeforeExecution(engine, network, round, config)
                }
                evaluator.execute(engine)
                fail("Expected failed execution")
            } catch {
                case e @ (_ : StaticError | _: DynamicError) => {
                    println(e) // OK
                    if (runChecksIfExpectFailure && engine != null) {
                        checkAfterExecution(engine, network, round, config)
                    }
                }
            }
        }
    }

    describe(title) {
        for (config <- configs) {
            it(formatTestCase(config)) {
                for (round <- 1 to numRounds) {
                    println("### %s: round %d".format(formatTestCase(config), round))
                    val evaluator = new NetworkEvaluator(this.executionDirectory, NetworkTestBase.repository)
                    evaluator.setReadStateFile(round > 1)
                    val network = new Network(NetworkTestBase.repository)
                    NetworkHandler.setNetwork(network)
                    val expectSuccess = populateNetwork(network, round, config)
                    executeNetwork(network, evaluator, round, config, expectSuccess)
                }

                println("Clearing %s".format(this.executionDirectory))
                IOTools.rmdir(this.executionDirectory, true)
            }
        }
    }
}

object NetworkTestBase {
    private val repository = new Repository()
    NetworkHandler.setRepository(this.repository)

    for (bundleDir: File <- repository.findAllBundles()) {
        val tempBundle = new Bundle(bundleDir, bundleDir.getName())
        if (tempBundle.isCompiled()) {
            repository.loadBundleFromJAR(bundleDir)
        } else {
            val msg = "Warning: bundle not compiled: %s".format(bundleDir)
            throw new RuntimeException(msg)
        }
    }
}
