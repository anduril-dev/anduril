package engine

import org.anduril.core.network.Network
import org.anduril.core.engine.Engine

import anduril.techtest._
import org.anduril.runtime._

class TypeInferenceSplitTest extends NetworkTestBase[(String, String, String)] {

    type ConfigType = (String, String, String)

    override def numRounds = 1

    override def title = "TypeInferenceSplitTest: test type inference in three-node split network, having a->b and a->c ('a' generic)"

    override def formatTestCase(config: ConfigType): String = {
        val (typeB, typeC, typeA) = config
        "b.type=%s c.type=%s a.result=%s".format(
            typeB, typeC, typeA)
    }

    override def configs = Seq(
    //    b.type  c.type  a.type
        ("A",    "A",     "A"),
        ("A",    "B",     "B"),
        ("A",    "C",     "C"),
        ("A",    "D",     "D"),
        ("A",    "E",     null),

        ("B",    "A",     "B"),
        ("B",    "B",     "B"),
        ("B",    "C",     null),
        ("B",    "D",     "D"),
        ("B",    "E",     null),

        ("C",    "A",     "C"),
        ("C",    "B",     null),
        ("C",    "C",     "C"),
        ("C",    "D",     null),
        ("C",    "E",     null),

        ("D",    "A",     "D"),
        ("D",    "B",     "D"),
        ("D",    "C",     null),
        ("D",    "D",     "D"),
        ("D",    "E",     null),

        ("E",    "A",     null),
        ("E",    "B",     null),
        ("E",    "C",     null),
        ("E",    "D",     null),
        ("E",    "E",     "E")
    )

    override def populateNetwork(network: Network, round: Int, config: ConfigType): Boolean = {
        val (typeB, typeC, typeA) = config
        val portB = getC3InPortName(typeB)
        val portC = getC3InPortName(typeC)

        val a = C1()
        val b = C3(Map(portB -> a.out1))
        val c = C3(Map(portC -> a.out1))

        val expectSuccess = (typeA != null)
        expectSuccess
    }

    override def checkBeforeExecution(engine: Engine, network: Network, round: Int, config: ConfigType): Unit = {
        val (typeB, typeC, typeA) = config
        val nodeA = findComponentInstance(network, "a")

        val actualTypeA = nodeA.getGenericType(nodeA.getComponent().getTypeParameter("T"))
        assertResult(typeA, "Assigned type parameter for 'a'" ) { actualTypeA.getName() }
    }
}
