package engine

import org.anduril.core.network.Network
import org.anduril.core.engine.Engine

import anduril.techtest._
import org.anduril.runtime._

class TypeInferenceLinearTest extends NetworkTestBase[(String, String, String)] {

    type ConfigType = (String, String, String)

    override def numRounds = 1

    override def title = "TypeInferenceLinearTest: test type inference in three-node linear network a->b->c ('b' generic)"

    override def formatTestCase(config: ConfigType): String = {
        val (typeA, typeC, typeB) = config
        "a.type=%s c.type=%s b.result=%s".format(
            typeA, typeC, typeB)
    }

    override def configs = Seq(
    //    a.type  c.type  b.type
        ("A",    "A",     "A"),
        ("A",    "B",     null),
        ("A",    "C",     null),
        ("A",    "D",     null),
        ("A",    "E",     null),

        ("B",    "A",     "A"),
        ("B",    "B",     "B"),
        ("B",    "C",     null),
        ("B",    "D",     null),
        ("B",    "E",     null),

        ("C",    "A",     "A"),
        ("C",    "B",     null),
        ("C",    "C",     "C"),
        ("C",    "D",     null),
        ("C",    "E",     null),

        ("D",    "A",     "A"),
        ("D",    "B",     "B"),
        ("D",    "C",     null),
        ("D",    "D",     "D"),
        ("D",    "E",     null),

        ("E",    "A",     null),
        ("E",    "B",     null),
        ("E",    "C",     null),
        ("E",    "D",     null),
        ("E",    "E",     "E")
    )

    override def populateNetwork(network: Network, round: Int, config: ConfigType): Boolean = {
        val (typeA, typeC, typeB) = config
        val outPortA = getC3OutPortName(typeA)
        val inPortC = getC3InPortName(typeC)

        val a = C3()
        val b = C1(in1=a(outPortA))
        val c = C3(Map(inPortC -> b.out1))

        val expectSuccess = (typeB != null)
        expectSuccess
    }

    override def checkBeforeExecution(engine: Engine, network: Network, round: Int, config: ConfigType): Unit = {
        val (typeA, typeC, typeB) = config
        val nodeB = findComponentInstance(network, "b")

        val actualTypeB = nodeB.getGenericType(nodeB.getComponent().getTypeParameter("T"))
        assertResult(typeB, "Assigned type parameter for 'b'" ) { actualTypeB.getName() }
    }
}
