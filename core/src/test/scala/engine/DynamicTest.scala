package engine

import scala.collection.JavaConversions._

import org.anduril.core.network.Network
import org.anduril.core.engine.{ActiveState, CurrentState, Engine}

import anduril.techtest._
import org.anduril.runtime._

class DynamicTest extends NetworkTestBase[Boolean] {

    type ConfigType = Boolean

    override def numRounds = 2

    override def title = "DynamicTest: test dynamic execution in workflow a1->a2->a3, b, c->c2 with breakpoints after {a1,b,c} and {a2}"

    override def formatTestCase(config: ConfigType): String = {
        "c.fail = %s".format(config)
    }

    override def configs = Seq(false, true)

    override def runChecksIfExpectFailure = true

    override def populateNetwork(network: Network, round: Int, config: ConfigType): Boolean = {
        val cFail = config

        // Stage 0
        val a1 = C1(ensureStage=0)
        val b = C1(ensureStage=0)
        b._keep = false
        val c = C1(ensureStage=0, fail=cFail)

        a1.content() // Dynamic breakpoint

        // Stage 1
        val a2 = C2(a1, ensureStage=1)
        val c2 = C1(c, ensureStage=1)

        a2.content() // Dynamic breakpoint

        // Stage 2
        val a3 = C2(a2, ensureStage=2)

        val expectSuccess = !cFail
        expectSuccess
    }

    override def checkAfterExecution(engine: Engine, network: Network, round: Int, config: ConfigType): Unit = {
        val cFail = config

        val nodeA1 = findComponentInstance(network, "a1")
        val nodeA2 = findComponentInstance(network, "a2")
        val nodeA3 = findComponentInstance(network, "a3")
        val nodeB  = findComponentInstance(network, "b")
        val nodeC  = findComponentInstance(network, "c")
        val nodeC2  = findComponentInstance(network, "c2")
        val outFileA1 = getOutPortFile(nodeA1, "out1", engine)
        val outFileA2 = getOutPortFile(nodeA2, "out1", engine)
        val outFileA3 = getOutPortFile(nodeA3, "out1", engine)
        val outFileB  = getOutPortFile(nodeB,  "out1", engine)
        val outFileC  = getOutPortFile(nodeC,  "out1", engine)

        assertResult(CurrentState.YES, "Is node 'a1' up-to-date after round "+round) { engine.getCurrentState(nodeA1) }
        assertResult(CurrentState.YES, "Is node 'a2' up-to-date after round "+round) { engine.getCurrentState(nodeA2) }
        assertResult(CurrentState.YES, "Is node 'a3' up-to-date after round "+round) { engine.getCurrentState(nodeA3) }
        assertResult(CurrentState.YES, "Is node 'b' up-to-date after round "+round) { engine.getCurrentState(nodeB) }
        assertResult(if (cFail) CurrentState.NO else CurrentState.YES, "Is node 'c' up-to-date after round "+round) { engine.getCurrentState(nodeC) }
        assertResult(if (cFail) CurrentState.NO else CurrentState.YES, "Is node 'c2' up-to-date after round "+round) { engine.getCurrentState(nodeC2) }

        assertResult(true,  "Is output file of 'a1' present") { outFileA1.exists() }
        assertResult(true,  "Is output file of 'a2' present") { outFileA2.exists() }
        assertResult(true,  "Is output file of 'a3' present") { outFileA3.exists() }
        assertResult(false, "Is output file of 'b' present")  { outFileB.exists() }
        assertResult(true,  "Is output file of 'c' present")  { outFileC.exists() }

        assertResult(2, "Stage of last engine") { engine.getStage() }
        val failed2 = engine.getFailed()
        assertResult(0, "No components should fail at last round") { failed2.size() }

        val engine1 = engine.getPreviousStageEngine()
        assertResult(1, "Stage of next to last engine") { engine1.getStage() }

        val engine0 = engine1.getPreviousStageEngine()
        assertResult(0, "Stage of initial engine") { engine0.getStage() }
        val failed0 = engine0.getFailed()
        assertResult(if (cFail) 1 else 0, "First round failures") { failed0.size() }
    }
}
