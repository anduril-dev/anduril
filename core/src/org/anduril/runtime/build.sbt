// Makefile for Simple Build Tool (http://www.scala-sbt.org/), version 0.10+
// Compile JAR file: sbt package
// Continuous compilation: sbt ~package
// Build API: sbt doc
//
// Environment requirements:
// - $ANDURIL_HOME is defined
// - $ANDURIL_HOME/microarray points to the microarray bundle

name := "anduril-scala"

scalaVersion := "2.11.7"

scalaSource in Compile := baseDirectory(_ / "src")

javaSource in Compile := baseDirectory(_ / "src")

scalacOptions in (Compile, doc) ++= Opts.doc.title("Anduril Scala Runtime")

scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-Xlint")

unmanagedClasspath in Compile +=
    file(System.getenv("ANDURIL_HOME")) / "lang/scala/lib/commons-primitives-1.0.jar"

unmanagedClasspath in Compile +=
    file(System.getenv("ANDURIL_HOME")) / "lang/scala/lib/commons-math3-3.2.jar"

unmanagedClasspath in Compile +=
    file(System.getenv("ANDURIL_HOME")) / "anduril.jar"

crossTarget := baseDirectory

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "." + artifact.extension
}
