#!/bin/bash

DOCKER_USER_ID=${USER_ID:-9001}
USERNAME=user

useradd --shell /bin/bash -u $DOCKER_USER_ID -o -c "" -m -p "" $USERNAME
groupadd admin > /dev/null
adduser $USERNAME admin > /dev/null
export HOME=/home/$USERNAME
exec /usr/sbin/gosu $USERNAME "$@"
