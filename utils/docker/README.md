# Docker images

To run Anduril with Docker:

- If you mount folders in the docker container:
    - Pass `-e USER_ID=$( id -u )` to give the docker user your id.
    - Create the folders ahead. Docker creates folders with root permissions only.
      i.e. `mkdir results && docker run -v $( pwd )/results:/results ...`


