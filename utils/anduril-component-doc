#!/usr/bin/env python
import os,sys,string,re

if os.path.exists(os.path.expandvars('$ANDURIL_HOME/lang/python')):
    sys.path.append(os.path.expandvars('$ANDURIL_HOME/lang/python'))
if os.path.exists('/usr/share/anduril/lang/python'):
    sys.path.append('/usr/share/anduril/lang/python')

try:
    from anduril.bundlerepository import *
    from anduril.generators.script import *
    from anduril.generators.documentation import *
    from anduril.utils import get_console_size as get_size
except ImportError,e:
    print('Could not import Anduril modules.')
    print('Set your $ANDURIL_HOME! {:}'.format(os.path.expandvars('$ANDURIL_HOME')))
    print(e)
    sys.exit(1)

__description__="""
Tool to search components, and print component documentation with markdown syntax.
"""
__version__="20150126"

def options():
    ''' Setup the command line options '''
    from argparse import ArgumentParser
    parser=ArgumentParser(description=__description__)
    parser.add_argument("-v",action='version', version=__version__)
    parser.add_argument("-k",action='store', dest='key',default=None,
                      help="Search for a word in component names or documentation.")
    parser.add_argument("-l",action='store_true', dest='list',
                      help="List all the component names")
    parser.add_argument("-s",action='store_true', dest='short',default=False,
                      help="Do not display documentation strings (short syntax)")
    parser.add_argument("-u",action='store_true', dest='usage',default=False,
                      help="Print example usage of the component instead of documentation.")
    parser.add_argument("-x",action='store_true', dest='xclip',default=False,
                      help="Copy example usage to clipboard with xclip command")
    parser.add_argument("-b",action='store', dest='bundle',default=None,
                      help="Narrow down search to specific bundle.")
    parser.add_argument("component",type=str,action="store",default=None,
                      help="Component name.",nargs="?")
    options=parser.parse_args()
    if options.component==None and any((options.usage,options.xclip)):
        parser.print_help()
        print("\nMissing component name")
        sys.exit(1)
    if options.component==None and not any((options.key,options.list)):
        parser.print_help()
        print("\nMissing component name")
        sys.exit(1)
    if options.xclip:
        options.usage=True
    if options.component != None and "." in options.component:
        parts=options.component.split(".")
        if len(parts)!=3:
            print("\nComponent full name not following format:  anduril.bundle.component")
            sys.exit(1)
        options.component=parts[2]
        options.bundle=parts[1]
    return options      

def sanitize_doc(d,w):
    """ Return a shortened version of a string without control characters """
    return re.sub(' +', ' ', d.translate(string.maketrans("\n\t\r", "   "))).strip()[0:w]

def search_matching_or_exit():
    if not opt.component:
        raise Exception("No component defined")
    print("Component {:} not found! Perhaps you were looking for:".format(opt.component))
    matches=repo.search_component_close_match(opt.component)
    if opt.component.lower() == matches[0].lower():
        (c,b)=repo.get_component(matches[0])
        return (c,b)
    print(", ".join(matches))
    sys.exit(1)
    

repo=Repository.from_env()
opt=options()
try: 
    screen_width=int(get_size()[1])
except:
    screen_width=80
    
if opt.key:
    for b in sorted(repo.get_bundle_names()):
        if b in ('techtest','testsystem'):
            continue
        if opt.bundle==None or opt.bundle==b:
            matches=repo.get_bundle(b).search_component(opt.key)
            if len(matches)==0:
                continue
            matches.sort(key=lambda x: x[0])
            for m in matches:
                if opt.short:
                    doc=""
                else:
                    doc=sanitize_doc(repo.get_bundle(b).get_component(m[0]).get_doc().encode('utf-8'),screen_width-39)
                print("{:10} {:20} {:} {:}".format(b,m[0],m[1],doc))
if opt.list:
    for b in sorted(repo.get_bundle_names()):
        if b in ('techtest','testsystem'):
            continue
        if opt.bundle==None or opt.bundle==b:
            names=repo.get_bundle(b).get_components().keys()
            if len(names)==0:
                continue
            names.sort()
            for n in names:
                if opt.short:
                    doc=""
                else:
                    doc=sanitize_doc(repo.get_bundle(b).get_component(n).get_doc().encode('utf-8'),screen_width-34)
                print("{:10} {:20} {:}".format(b,n,doc))
if opt.component:
    if opt.bundle:
        if opt.component in repo.get_bundle(opt.bundle).get_components():
            c=repo.get_bundle(opt.bundle).get_component(opt.component)
            b=opt.bundle
        else:
            (c,b)=search_matching_or_exit()
    else:
        if opt.component in [c[1] for c in repo.get_all_component_names()]:
            (c,b)=repo.get_component(opt.component)
        else:
            (c,b)=search_matching_or_exit()

    if not opt.usage:
        print(component_markdown(c,opt.short,b).encode('utf-8'))
    if opt.usage:
        print("\n"+component_usage(c))
    if opt.xclip:
        copy_to_clipboard(component_usage(c).lstrip())


