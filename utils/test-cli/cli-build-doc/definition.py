class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'build-doc --component doc-destination -b builtin -b testsystem'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileExists(output_directory, 'doc-destination/index.html')
        self.assertFileExists(output_directory, 'doc-destination/components/INPUT/index.html')
        self.assertFileExists(output_directory, 'doc-destination/components/Add/index.html')
        self.assertFileExists(output_directory, 'doc-destination/types/BinaryFile/index.html')
        self.assertFileExists(output_directory, 'doc-destination/types/Number/index.html')
