class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'test -b testsystem -c LibSumSquare -c Add -d myexecute'

    def extra_env(self):
    	return { 'ANDURIL_TESTSYSTEM_LIBSUMSQUARE_FAIL': 'fail' }

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertRegexpMatches(stdout, 'environment variable ANDURIL_TESTSYSTEM_LIBSUMSQUARE_FAIL')
        self.assertRegexpMatches(stdout, 'Failed to load components')
        self.assertRegexpMatches(stdout, 'Add/testcases/case1')
