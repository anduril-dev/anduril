class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'test-workflow -b testsystem -w network01_basic -d myexecute'

    def extra_env(self):
        return { 'ANDURIL_TESTSYSTEM_ADDMATRIX_FAIL': 'fail' }

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertRegexpMatches(stdout, 'environment variable ANDURIL_TESTSYSTEM_ADDMATRIX_FAIL')
        self.assertRegexpMatches(stdout, 'network01_basic')
        self.assertNotRegexpMatches(stdout, 'network03_dynamic')
