class Test(CLITest):
    def command(self, command_prefix):
        return [
            command_prefix + 'run workflow.scala -d myexecute'
        ]

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileMatches(output_directory, 'myexecute/_state', 'result')
        self.assertFileMatches(output_directory, 'myexecute/output/result-sum', '^3[.]0$')
        self.assertFileMatches(output_directory, 'myexecute/_state', 'result')
        self.assertRegexpMatches(stdout, 'x1')
        self.assertRegexpMatches(stdout, 'x2')
        self.assertRegexpMatches(stdout, 'result')        
