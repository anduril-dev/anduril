import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object Test {
	val x1 = INPUT("x1")
	x1.content // dynamic breakpoint
	val x2 = INPUT("x2")
	val result = Add(x1, x2)
	OUTPUT(result.sum)
}