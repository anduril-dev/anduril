class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'build-doc --component doc-destination -b invalidbundle'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertNotFileExists(output_directory, 'doc-destination')
        self.assertRegexpMatches(stdout, 'invalidbundle')
