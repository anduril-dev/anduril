class Test(CLITest):
    def command(self, command_prefix):
        return [
            command_prefix + 'run invalid.jar -d execute'
        ]

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertNotFileExists(output_directory, 'execute')
        self.assertNotFileExists(output_directory, 'invalid.jar')
