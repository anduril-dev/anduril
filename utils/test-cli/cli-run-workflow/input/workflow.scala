import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object Test {
	val x1 = INPUT("x1")
	val x2 = INPUT("x2")
	val result = Add(x1, x2)
	OUTPUT(result.sum)
}