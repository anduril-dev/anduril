class Test(JARTest):
    def command(self, command_prefix):
        return command_prefix + 'test -b testsystem -c AddMatrix -t case02_rowvector_zero -d myexecute'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileExists(output_directory, 'myexecute/AddMatrix/case02_rowvector_zero')
        self.assertNotFileExists(output_directory, 'myexecute/AddMatrix/case01_regular2by3')
        self.assertNotFileExists(output_directory, 'myexecute/Add')
        self.assertNotFileExists(output_directory, 'myexecute/LibSumSquare')
        self.assertRegexpMatches(stdout, 'AddMatrix/testcases/case02_rowvector_zero')
        self.assertNotRegexpMatches(stdout, 'AddMatrix/testcases/case01_regular2by3')
