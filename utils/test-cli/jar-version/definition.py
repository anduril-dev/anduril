class Test(JARTest):
    def command(self, command_prefix):
        return command_prefix + '--version'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertRegexpMatches(stdout, '^Anduril version .*')
