class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'test -b testsystem -c AddMatrix -d myexecute'

    def extra_env(self):
    	return { 'ANDURIL_TESTSYSTEM_ADDMATRIX_FAIL': 'fail' }

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertRegexpMatches(stdout, 'environment variable ANDURIL_TESTSYSTEM_ADDMATRIX_FAIL')
        self.assertRegexpMatches(stdout, 'AddMatrix/testcases/case01_regular2by3')
        self.assertRegexpMatches(stdout, 'AddMatrix/testcases/case02_rowvector_zero')
        self.assertRegexpMatches(stdout, 'AddMatrix/testcases/case03_invalid')
