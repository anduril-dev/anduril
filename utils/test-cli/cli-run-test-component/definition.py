class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'test -b testsystem -d myexecute'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileExists(output_directory, 'myexecute/Add/case1')
        self.assertFileExists(output_directory, 'myexecute/LibSumSquare/case2')
        self.assertRegexpMatches(stdout, 'Add/testcases/case1')
        self.assertRegexpMatches(stdout, 'LibSumSquare/testcases/case2')
