class Test(CLITest):
    def command(self, command_prefix):
        return [
            command_prefix + 'run workflow.scala -d execute'
        ]

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertFileExists(output_directory, 'execute')
        self.assertRegexpMatches(stdout, 'missinginput')
