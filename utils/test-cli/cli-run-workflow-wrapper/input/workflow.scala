import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object Test {
	val x1 = INPUT("x1")
	val x2 = INPUT("x2")

	val result = Add(x1, x2)
    result._cpu = 42
    result._host = "myhost"
    result._memory = 500
    result._userDefined = "custom_value"
    result._custom("extra key") = "extra_value"

	OUTPUT(result.sum)
}