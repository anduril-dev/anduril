class Test(CLITest):
    def command(self, command_prefix):

        import os.path
        wrapper = os.path.join(os.path.abspath(self.test_case_directory), 'mywrapper.sh')

        return [
            command_prefix + 'run workflow.scala -d myexecute --wrapper "bash %s"' % wrapper
        ]

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileMatches(output_directory, 'myexecute/_state', 'result')
        self.assertFileMatches(output_directory, 'myexecute/output/result-sum', '^3[.]0$')

        self.assertRegexpMatches(stdout, 'CPU = 42')
        self.assertRegexpMatches(stdout, 'HOST = myhost')
        self.assertRegexpMatches(stdout, 'MEMORY = 500')
        self.assertRegexpMatches(stdout, 'USERDEFINED = custom_value')
        self.assertRegexpMatches(stdout, 'EXTRA_KEY = extra_value')
        self.assertRegexpMatches(stdout, 'COMPONENT STATUS 0')
