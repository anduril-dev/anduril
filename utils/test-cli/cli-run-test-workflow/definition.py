class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'test-workflow -b testsystem -d myexecute'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileExists(output_directory, 'myexecute/testsystem/network01_basic/add/sum')
        self.assertFileExists(output_directory, 'myexecute/testsystem/network03_dynamic/sum2/sum.csv')
        self.assertFileExists(output_directory, 'myexecute/testsystem/network06_libfunction/sumSquare1-sumSquare/sum')
        self.assertRegexpMatches(stdout, 'network01_basic')
        self.assertRegexpMatches(stdout, 'network03_dynamic')
        self.assertRegexpMatches(stdout, 'network06_libfunction')
