class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'build clitestbundle'

    def extra_env(self):
        return { 'ANDURIL_BUNDLES': '.' }

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 0)
        self.assertFileExists(output_directory, 'clitestbundle/clitestbundle.jar')
