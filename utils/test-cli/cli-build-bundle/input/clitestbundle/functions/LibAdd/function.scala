def LibAdd(x: Number, y: Number, skip: Boolean): Number = {
    val result = Add(x, y)
    result.sum
}
