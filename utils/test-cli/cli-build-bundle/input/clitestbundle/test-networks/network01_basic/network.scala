import anduril.builtin._
import anduril.testsystem._
import org.anduril.runtime._

object network01 {

    info("Test workflow")

    val in1 = INPUT(path="in1")
    val in2 = INPUT(path="in2")
    val result = Add(in1, in2)
    OUTPUT(result.sum)

}