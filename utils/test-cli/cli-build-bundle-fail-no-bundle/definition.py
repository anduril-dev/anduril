class Test(CLITest):
    def command(self, command_prefix):
        return command_prefix + 'build invalidbundle'

    def check(self, status, output_directory, stdout):
        self.assertEquals(status, 1)
        self.assertNotFileExists(output_directory, 'invalidbundle')
        self.assertRegexpMatches(stdout, 'invalidbundle')
