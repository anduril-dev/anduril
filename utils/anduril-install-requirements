#!/usr/bin/env python

import os,sys
#import subprocess
#import tempfile

if os.path.exists(os.path.expandvars('$ANDURIL_HOME/lang/python')):
    sys.path.append(os.path.expandvars('$ANDURIL_HOME/lang/python'))
if os.path.exists('/usr/share/anduril/lang/python'):
    sys.path.append('/usr/share/anduril/lang/python')

try:
    from anduril.bundlerepository import *
    from anduril.generators.script import *
    from anduril.utils import *
 
except ImportError,e:
    print('Could not import Anduril modules.')
    print('Set your $ANDURIL_HOME! {:}'.format(os.path.expandvars('$ANDURIL_HOME')))
    print(e)
    sys.exit(1)

__description__="""
Run installers for component requirements.

ex.   anduril-install-requirements -b tools CSVJoin
"""
__version__="20150204"

def search_matching_or_exit(component):
    if not component:
        raise Exception("No component defined")
    print("Component {:} not found! Perhaps you were looking for:".format(component))
    matches=repo.search_component_close_match(component)
    print(", ".join(matches))
    sys.exit(1)

def options():
    ''' Setup the command line options '''
    from argparse import ArgumentParser
    parser=ArgumentParser(description=__description__)
    parser.add_argument("-v",action='version', version=__version__)
    parser.add_argument("-b",action='store', dest='bundle',default=None,
                      help="Specify specific bundle.")
    parser.add_argument("-r",action='store_true', dest='run',default=False,
                      help="Run installers, instead of printing installation commands.")
    parser.add_argument("-s",action='store_true', dest='sudo',default=False,
                      help="Run/print installers with sudo command.")
    parser.add_argument("component",type=str,action="store",
                      help="Component name. 'all' for all components")
    options=parser.parse_args()
    if options.component.lower()=="all":
        options.component="all"
    return options

opt=options()
repo=Repository.from_env()
if not opt.component=="all":
    if opt.bundle:
        if opt.component in repo.get_bundle(opt.bundle).get_components():
            c=repo.get_bundle(opt.bundle).get_component(opt.component)
            b=opt.bundle
        else:
            search_matching_or_exit(opt.component)
    else:
        if opt.component in [c[1] for c in repo.get_all_component_names()]:
            (c,b)=repo.get_component(opt.component)
        else:
            search_matching_or_exit(opt.component)


    print("# ===================\n# == "+opt.component+"\n# ===================")
    install_requirements(c, opt.sudo, not opt.run)
else:
    if opt.bundle:
        for cname in sorted(repo.get_bundle(opt.bundle).get_components()):
            c=repo.get_bundle(opt.bundle).get_component(cname)
            b=opt.bundle
            print("# ===================\n# == "+b+": "+cname+"\n# ===================")
            install_requirements(c, opt.sudo, not opt.run)
    else:
        for cname in [c[1] for c in repo.get_all_component_names()]:
            (c,b)=repo.get_component(cname)
            print("# ===================\n# == "+b+": "+cname+"\n# ===================")
            install_requirements(c, opt.sudo, not opt.run)
