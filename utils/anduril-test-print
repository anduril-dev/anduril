#!/usr/bin/env python
import os,sys,textwrap

if os.path.exists(os.path.expandvars('$ANDURIL_HOME/lang/python')):
    sys.path.append(os.path.expandvars('$ANDURIL_HOME/lang/python'))
if os.path.exists('/usr/share/anduril/lang/python'):
    sys.path.append('/usr/share/anduril/lang/python')

try:
    from anduril.bundlerepository import *
    from anduril.generators.script import *
    from anduril.generators.documentation import *
except ImportError,e:
    print('Could not import Anduril modules.')
    print('Set your $ANDURIL_HOME! {:}'.format(os.path.expandvars('$ANDURIL_HOME')))
    print(e)
    sys.exit(1)

__description__="""
Print testcase pipeline to stdout

ex.   anduril-test-print -b tools CSVJoin CSVFilter
"""
__version__="20161003"

def options():
    ''' Setup the command line options '''
    from argparse import ArgumentParser
    parser=ArgumentParser(description=__description__)
    parser.add_argument("-v",action='version', version=__version__)
    parser.add_argument("-b",action='store', dest='bundle',
                      help="Specify bundle.")
    parser.add_argument("-c",action='store', dest='cases', default="all",
                      help="Test case name, or all for everything.  [%(default)s]")
    parser.add_argument("--disabled",action='store_true', dest='disabled', default=False,
                      help="Print disabled test cases.  [%(default)s]")
    parser.add_argument("-d",action='store', dest='execution',default="execute",
                      help="Specify execution folder.  [%(default)s]")
    parser.add_argument("--log",action='store', dest='log',default="log",
                      help="Specify log folder.  [%(default)s]")
    parser.add_argument("component",type=str,action="store",nargs='?',
                      help="Component name. If left out, print test cases for all components in the bundle.")
    options=parser.parse_args()
    if not options.bundle:
        parser.print_help()
        print("Define bundle! (-b)")
        sys.exit(1)
    return options

def case_fails(testcase):
    if testcase['fail']:
        return "(Testcase should FAIL) "
    return ""

opt=options()
repo=Repository.from_env()
bundle=repo.get_bundle(opt.bundle)

header=["#!/usr/bin/env anduril", 
        "//$OPT -d "+opt.execution
        ]
header.append("import org.anduril.runtime._")
header.append("import anduril."+opt.bundle+"._")
for dep in bundle.get_depends():
    header.append("import anduril."+dep+"._")
if opt.bundle!="builtin":
    header.append("import anduril.builtin._")
header.append("object runTest_"+opt.bundle+" { \n")
footer="}"

if opt.component:
    components=[opt.component]
else:
    components=sorted(bundle.get_components().keys())
compNames=bundle.get_components().keys()
testcode=[]
for c in components:
    if c not in compNames:
        print("No component '%s' found"%(c,))
        sys.exit(1)
    testcases=bundle.get_component(c).get_testcases()
    if testcases.get_names()==None:
        continue
    for t in testcases.get_names():
        this_case=testcases.get_case(t)
        if not opt.disabled:
            if this_case['disabled']:
                continue
        if opt.cases=="all":
            testcode.append( "\n".join([
              "    /** Testcase: %s/%s %s*/"%(c,t,case_fails(this_case)),
              testcase(bundle, c, t),
              ""
              ]))

if (len(testcode)==0):
    print("No testcases found with testcase name '%s'"%(opt.cases,))
    sys.exit(1)


print "\n".join([
            "\n".join(header),
            "\n".join(testcode),
            footer 
            ])
        

