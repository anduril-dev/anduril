# Anduril #

[Anduril](http://anduril.org) is a workflow platform for analyzing large data sets.

Anduril provides [facilities](http://www.anduril.org/anduril/bundles/all/doc/) for analyzing high-thoughput data in biomedical research, and the platform is fully [extensible](http://anduril.org/userguide/extend/overview/) by third parties. Ready-made tools support data visualization, DNA/RNA/ChIP-sequencing, DNA/RNA microarrays, cytometry and image analysis.

Workflows are [constructed](http://anduril.org/userguide/workflows/hello-world/) using Scala 2.11 and executed in parallel using a workflow engine optimized for iterative development. External libraries (e.g., R, Python) and command-line tools can be integrated into workflows. Anduril can be deployed locally or in a [Linux cluster](http://anduril.org/userguide/install/cluster/).

Anduril development began in 2007 and the platform has been used for
analyzing several large molecular biology data sets.

## Download and Install intructions
Check our [wiki](https://bitbucket.org/anduril-dev/anduril/wiki/Home)

## License

Anduril is developed by Systems Biology Laboratory, University of Helsinki, Finland.

Anduril 2 (current version) is available under the 
[BSD 2-clause license](https://bitbucket.org/anduril-dev/anduril/raw/anduril2/doc/LICENSE).

Anduril 1 (legacy version) is available under the GNU General Public License v2 (GPL 2).

## Citing

Please cite this publication when you use Anduril.

* Kristian Ovaska, Marko Laakso et al. [Large-scale data integration
framework provides a comprehensive view on glioblastoma multiforme.](http://genomemedicine.com/content/2/9/65)
Genome Medicine 2010, 2(9):65.

