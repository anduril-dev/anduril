#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import sys,os
from datetime import datetime
import re,signal

VERSION=20160422

K= '30'
R= '31'
G= '32'
Y= '33'
B= '34'
M= '35'
C= '36'
W= '37'
S= '1'
E= '0'
BR= '41'
CLR = '\033[2J'
SAVE = '\033[s'
LOAD = '\033[u'
CLRLN = '\033[K'
CLRBLN = '\033[1K'
DOWN = '\033[1B'
STATUSLINE='ANDURiL'

def c(attribs):
    ''' ANSI colorizer '''
    return '\033['+';'.join(attribs)+'m'

def cc(attribs):
    ''' ANSI colorizer with checkup '''
    if not options.colors:
        return ''
    else:
        return c(attribs)

def pos(y,x):
    ''' ANSI absolute position set '''
    return "\033["+str(y)+";"+str(x)+"H"

counter_match={
    'COMPONENT-STARTED':re.compile('\[INFO ([^ ]+)\] Executing [^ ]+ \(([^ ]+)\) \(SOURCE ([^\)]+)\) \((COMPONENT-STARTED)\).*'),
    'COMPONENT-FINISHED-OK':re.compile('\[INFO ([^ ]+)\] .* \((COMPONENT-FINISHED-OK)\).*'),
    'COMPONENT-FINISHED-ERROR':re.compile('\[INFO ([^ ]+)\] .* \((COMPONENT-FINISHED-ERROR)\).*'),
    'READY-QUEUE':re.compile('\[INFO [^ ]+\] .* \(READY-QUEUE ([^ ]+)\).*'),
    'CALLBACK':re.compile('\[INFO [^ ]+\] .* \(CALLBACK\).*'),
    'slurm':re.compile('\[LOG ([^ ]+)\] srun: Node ([^ ,]+).*')
}

color_match={#'line_ends':(re.compile('$'),c.END),
    'COMPONENT-STARTED':(re.compile('(COMPONENT-STARTED)'),c([E,G])+'\\1'+c([E])),
    'COMPONENT-FINISHED-OK':(re.compile('(COMPONENT-FINISHED-OK)'),c([E,G])+'\\1'+c([E])),
    'COMPONENT-FINISHED-ERROR':(re.compile('(COMPONENT-FINISHED-ERROR)'),c([E,R])+'\\1'+c([E])),
    'COMPONENT-CONFIGURATION-CHANGED':(re.compile('(COMPONENT-CONFIGURATION-CHANGED)'),c([W,S])+'\\1'+c([E])),
    'CALLBACK':(re.compile('\((CALLBACK)\)'),'({}\\1{})'.format(c([C,S]),c([E]))),
    'warning':(re.compile('\[(WARNING)\]'),'[{}\\1{}]'.format(c([Y,S]),c([E]))),
    'info':(re.compile('\[(INFO) '),'[{}\\1  {}'.format(c([W,S]),c([E]))),
    'warn':(re.compile('\[(WARN) '),'[{}\\1  {}'.format(c([Y,S]),c([E]))),
    'severe':(re.compile('\[(SEVERE) '),'[{}\\1{}'.format(c([R,S]),c([E]))),
    'error':(re.compile('\[(ERROR) ([^\]]+)\](.*)'),'[{}\\1{} \\2]{}\\3{}'.format(c([E,BR]),c([E]),c([R,S]),c([E]))),
    'log':(re.compile('\[(LOG) '),'[\\1   '),
    'done_ok':(re.compile('(Done. No errors occurred.)'),c([G,S])+'\\1'+c([E])),
    'done_err':(re.compile('(Done. The following.*)'),c([R,S])+'\\1'+c([E])),
    'slurmnode':(re.compile('(.*)(srun:)( Node )([^ ,]*)(.*)'),'\\1\\2\\3'+c([E,G])+'\\4'+c([E])+'\\5'),
}
error_match={
    #'stderr':re.compile('.*STDERR.*'),
    'error':re.compile('.*\[ERROR.*'),
}
queue_match=re.compile('(.*)(READY queue[:]*)(.*)')
remove_match={
    'execute':(re.compile('\[INFO [^ ]+\] Executing .*'),'','shows when a component is executed'),  
    'queue':(re.compile('\[INFO [^ ]+\] Current ready queue: .*'),'','show ready queue'),
    'success':(re.compile('\[INFO [^ ]+\] Component finished with success.*'),'','successful component execution'),
    'failure':(re.compile('\[INFO [^ ]+\] Component finished with failure.*'),'','failed component execution'),
    'clearing':(re.compile('\[INFO [^ ]+\] Clearing directory .*'),'','directory clearing'),
    'callback':(re.compile('\[INFO [^ ]+\] Accessing dynamic contents .*'),'','restarts of the engine due to for loops'),
    'keep':(re.compile('\[INFO [^ ]+\] Removing unnecessary output: .*'),'','removal of unnecessary output (from _keep=false)'),
    'log':(re.compile('^\[LOG .*'),'','Output from components'),
    'warn':(re.compile('^\[WARN .*'),'','All WARN lines'),
    'slurm':(re.compile('^\[LOG [^ ]+\] srun: .*'),'','slurm verbose messages (anything with "srun:")'),
}

def colorize(string):
    ''' colorizes a string based on color_match '''
    if not options.colors:
        return string
    for c in color_match:
        string=color_match[c][0].sub(color_match[c][1],string)
    return string

def count_queue(string,oldlength):
    ''' counts the QUEUE ready messages '''
    if not counter_match['READY-QUEUE'].match(string):
        return oldlength
    else:
        length_str=counter_match['READY-QUEUE'].sub('\\1',string)
        length=int(length_str)
        
        return length

def count_running(string, running):
    ''' Counts the running executions '''
    if counter_match['COMPONENT-STARTED'].match(string):
        instance=counter_match['COMPONENT-STARTED'].sub('\\1',string).strip()
        component=counter_match['COMPONENT-STARTED'].sub('\\2 l:\\3',string).strip()
        if component.startswith("anduril."):
            component=component[8:]
        running.append((instance,datetime.now(),component,''))
    if counter_match['COMPONENT-FINISHED-OK'].match(string):
        instance=counter_match['COMPONENT-FINISHED-OK'].sub('\\1',string).strip()
        running=remove_running(running,instance)
    if counter_match['COMPONENT-FINISHED-ERROR'].match(string):
        instance=counter_match['COMPONENT-FINISHED-ERROR'].sub('\\1',string).strip()
        running=remove_running(running,instance)
    if counter_match['slurm'].match(string):
        instance=counter_match['slurm'].sub('\\1',string).strip()
        node=counter_match['slurm'].sub('\\2',string).strip()
        for e in enumerate(running):
            if e[1][0]==instance:
                running[e[0]]=(running[e[0]][0], running[e[0]][1], running[e[0]][2], node)
                break

    return running

class EndProgram( Exception ):
    ''' Nice way of exiting the program '''
    pass

def get_host():
    ''' Get the current system host name '''
    try:
        import socket
        return "@"+socket.gethostname()
    except:
        return ""

def get_load():
    ''' Get the current system load '''
    try:
        return "%.2f" % os.getloadavg()[0]
    except:
        return "NA"

def get_next_status(s):
    ''' Animate status row '''
    for i,c in enumerate(s):
        if c==c.lower():
            break
    if i==len(s)-1:
        i=0
    else:
        i+=1
    return s.replace(c,c.upper()).replace(s[i],s[i].lower())

def get_time_difference(time1,time2):
    ''' returns the time difference in seconds '''
    time_diff=time1-time2
    try:
        return time_diff.total_seconds()
    except:
        # for older version of python
        return time_diff.seconds

def get_timestamp(time_start):
    ''' Get the human readable time since start '''
    time_diff=get_time_difference(datetime.now(),time_start)
    return humanize_time(time_diff)

def humanize_time(secs):
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    if hours < 24:
        return '%02d:%02d:%02d' % (hours, mins, secs)
    else:
        days, hours = divmod(hours, 24)
        return '%dd %02d:%02d:%02d' % (days, hours, mins, secs)

def insert_multiple_strings(s, ins, pos_list):
    ''' Insert string at multiple positions '''
    offset=0
    pos_list.sort()
    for p in pos_list:
        new_p=p+offset
        s=s[:new_p] + ins + s[new_p:]
        offset+=len(ins)
    return s

def match_error(string):
    ''' finds errors based on error_match '''
    matches=False
    for c in error_match:
        matches=error_match[c].match(string)
        if matches:
            return matches
    return matches
    
def match_remove(string):
    ''' applies the cleaning regexps based on remove_match '''
    for c in options.noshowlist:
        string=remove_match[c][0].sub(remove_match[c][1],string)
    return string

def print_stats(errorlist,stats):
    ''' Prints logged errors, and the status line '''
    if not options.status:
        return
    sys.stdout.write(SAVE)
    e=0
    sys.stdout.write(pos(e+1,0)+' '+CLRLN)
    for e in range(len(errorlist)):
        sys.stdout.write(pos(e+1,0)+errorlist[e]+CLRLN)
    e+=1
    if options.title:
        sys.stdout.write(pos(e+1,0)+'-= '+options.title+' =-'+CLRLN)
    else:
        sys.stdout.write(pos(e+1,0)+CLRLN)
    sys.stdout.write("\x1b]2;"+stats['STATUSLINE']+" "+get_timestamp(stats['time'])+"\x07")
    sys.stdout.write(pos(e+2,0)+stats['STATUSLINE'])
    sys.stdout.write(' OK: '+str(stats['executed'])+\
                     ' Fail: '+str(stats['fails'])+\
                     ' Queue: '+str(stats['queue'])+\
                     ' Callbacks: '+str(stats['callbacks'])+\
                     ' Time: '+get_timestamp(stats['time'])+\
                     ' Load: '+get_load()+stats['host']+\
                     CLRLN)

    if options.status_long:
        stats_lengths=[0,0]
        for ex in stats['running']:
            if stats_lengths[0] < len(ex[0]):
                stats_lengths[0] = len(ex[0])
            if stats_lengths[1] < len(ex[2]):
                stats_lengths[1] = len(ex[2])
        for ex in enumerate(stats['running']):
            sys.stdout.write(
                    ('{:}({:>2}) {:} {:<'+str(stats_lengths[0])+'} {:<'+str(stats_lengths[1])+'} {:}{:}').format(
                      pos(e+3+ex[0],0),
                      str(ex[0]+1),
                      get_timestamp(ex[1][1]),
                      ex[1][0],
                      ex[1][2],
                      ex[1][3],
                      CLRLN
                    ))
            
    else:
        sys.stdout.write(pos(e+3,0)+'Executing instances ('+str(len(stats['running']))+\
                     '): '+', '.join([r[0] for r in stats['running']])+CLRLN)
    sys.stdout.write(DOWN+CLRBLN+CLRLN)            
    sys.stdout.write(LOAD)
    

def print_final_stats(errorlist,stats,options):
    ''' Prints logged errors, and the status line, as the last entry on the screen '''
    if not options.status:
        return
    sys.stdout.write(pos(3000,1))
    if len(errorlist)>0:
        sys.stdout.write('{} {}Saved error messages:{} (first {}) {}\n'.format(
                                25*'=', cc([R]), cc([E]),
                                str(options.error_rows),
                                25*'='))
        for line in errorlist:
            sys.stdout.write(line)
    else:
        sys.stdout.write(cc([G,S])+'No errors logged'+cc([E])+'\n')
    sys.stdout.write(' OK: '+str(stats['executed'])+\
                     ' Fail: '+str(stats['fails'])+\
                     ' Callbacks: '+str(stats['callbacks'])+\
                     ' Time: '+get_timestamp(stats['time'])+\
                     ' Host: '+stats['host']+\
                     CLRLN+'\n')

def readinput(lf,foo=True):
    try:
        line=lf.readline()
        return line
    except:
        return "CleanerTimeout"

def remove_running(running,string):
    ''' Remove list item matching to string '''
    for e in enumerate(running):
        if e[1][0]==string:
            running.pop(e[0])
            break
    return running
        
def setup_options():
    ''' Setup the command line options '''
    from argparse import ArgumentParser
    import argparse

    parser=ArgumentParser(description='''
Tool to clean up and colorize the output of Anduril.
Example:   anduril run yourscript.scala --pipe %(prog)s 
You can tap in to an existing log with:
  tail -f -n +0 log/_global  | %(prog)s''',formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-e",type=int,action='store', dest='error_rows',default=10,
                      help="How many error rows to show. default: %(default)s")
    parser.add_argument("--timer",'-t',action="store_true",dest="timestamp",default=False,
                        help="Print timer on each line")
    parser.add_argument("--no-colors",'--nc',action="store_false",dest="colors",default=True,
                        help="Disable colored output")
    parser.add_argument("--no-status",'--ns',action="store_false",dest="status",default=True,
                        help="Disable status window. Enables scrolling back of the output.")
    parser.add_argument("--long-status",'--ls',action="store_true",dest="status_long",default=False,
                        help='''Show long version of status window. Shows the node where an
instance is running, if using --slurm-args %%v''')
    
    filter_list="\n".join([("* ")+": ".join([x, remove_match[x][2]]) for x in sorted(remove_match)])
    parser.add_argument('--show','-s',type=str,action='store', dest='showlist',
                        default="execute,failure,log,warn",
                      help='''Labels to display in the log feed. A comma separated list of keywords:'''+
                      filter_list+'''

* all: shortcut to show all of the feed
* default: %(default)s
    ''')
    parser.add_argument("--title",type=str,action="store",default=False,dest='title',
                      help="Title for this run")
    parser.add_argument("logfile",type=str,action="store",default="",nargs="?",
                      help="Log file to read, uses stdin automatically")
    parser.add_argument("--version",action='version', version=VERSION)
    options=parser.parse_args()
    options.showlist=options.showlist.split(',')
    options.showlist=[i.strip().lower() for i in options.showlist]
    return options

def timeouthandler(sig,frame):
    raise IOError("Input timeout")

options=setup_options()
if options.showlist[0]=='all':
    options.showlist=[sl for sl in remove_match]
options.noshowlist=[sl for sl in remove_match if sl not in options.showlist]
banner_path_name=os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),'..','doc','logo')
BANNER=""
if options.colors:
    banner_full_path=os.path.join(os.path.abspath(banner_path_name),'seasonal.ans')
else:
    banner_full_path=os.path.join(os.path.abspath(banner_path_name),'seasonal.asc')

if os.path.exists(banner_full_path):
    BANNER=open(banner_full_path,'r').read()

errorlist=[]
stats={'executed':0, 'fails':0, 
       'queue':0, 'callbacks':0,
       'time':datetime.now(), 
       'running':[], 
       'STATUSLINE':STATUSLINE,
       'host':get_host()}

if (not sys.stdin.isatty()):
    logfile=sys.stdin
else:
    logfile=open(options.logfile,'r')

old_timestamp=datetime.now()
if options.status:
    sys.stdout.write(CLR)
sys.stdout.write(BANNER)
print_stats(errorlist,stats)
while 1:
    try:
        sys.stdout.flush()
        # set a 3 second timeout for the line read.
        signal.signal(signal.SIGALRM, timeouthandler)
        signal.alarm(3)
        line=readinput(logfile)
        if not line:
            raise EndProgram
        new_timestamp=datetime.now()
        if get_time_difference(new_timestamp,old_timestamp)>1:
            old_timestamp=new_timestamp
            stats['STATUSLINE']=get_next_status(stats['STATUSLINE'])
        # if timeout returns a special string, in this case we re-read
        if line=="CleanerTimeout":
            print_stats(errorlist,stats)
            continue
        if counter_match['COMPONENT-FINISHED-OK'].match(line):
            stats['executed']+=1
        if counter_match['COMPONENT-FINISHED-ERROR'].match(line):
            stats['fails']+=1
        if counter_match['CALLBACK'].match(line):
            stats['callbacks']+=1
        stats['queue']=count_queue(line,stats['queue'])
        stats['running']=count_running(line,stats['running'])
        # store only maximum number of error lines
        if len(errorlist)<options.error_rows:
            if match_error(line):
                errorlist.append(colorize(line))
        #~ # end processing when log is closed
        #~ if line.startswith('Log closed on ') and '; user ' in line:
            #~ raise EndProgram
        # remove content we don't want to see
        line=match_remove(line)
        # if line empty, read next
        if line.strip()=="":
            continue
        # colorize the line
        colored=colorize(line)
        if options.timestamp:
            sys.stdout.write('['+get_timestamp(stats['time'])+']')
        if len(colored)>10000:
            colored="%s %s>>>%s\n\r"%(colored[0:10000],cc(['7','33']),cc([E]))
        sys.stdout.write(colored)
        print_stats(errorlist,stats)
        
    except (EndProgram,KeyboardInterrupt):
        if stats['callbacks']>20:
            sys.stdout.write('Pipeline contains many callbacks ('+str(stats['callbacks'])+'), potentially slowing down the run.\n')
        print_final_stats(errorlist,stats,options)
        sys.stdout.write(cc([E]))
        sys.stdout.flush()
        sys.exit(0)    
    except IOError:
        pass



